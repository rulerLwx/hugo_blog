---
title: "关于我"
date: 2020-07-09T10:00:10+08:00
draft: true
---

# 我的技能

- 熟悉Spring、SpringMVC、SpringBoot、MyBatis、JPA
- 熟悉Oracle、MySQL数据库，熟练编写sql
- 熟悉Linux常用命令
- 熟悉JSP、Servlet、Html、JavaScript、jQuery
- 掌握Spring Cloud、Docker
- 掌握Redis
- 掌握WebService、Shiro
- 掌握Tomcat、Nginx的配置、部署
- 掌握相关工具：Eclipse、IDEA、Maven、Git、SVN


