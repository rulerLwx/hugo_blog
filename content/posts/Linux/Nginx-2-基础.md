---
title: "Nginx-2"
date: 2017-08-01T10:02:07+08:00
draft: true
tags: [ ""]
categories: ["Linux"]
---


# 学习资料

# 什么是负载均衡

增加服务器数量，将请求分发到各个服务器上，将原先请求集中到单个服务上的情况改为将请求分发到多个服务上


# 什么是动静分离

为了加快网站的解析速度，可以将**动态页面和静态页面**由不同的服务器来解析，加快解析速度，降低单个服务器压力

单个Tomcat：Html/js/css（静态）、JSP/Servlet（静态）

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708173659.png)

多个Tomcat：静态页面放在一个Tomcat，动态页面放在一个Tomcat

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708173720.png)



# 什么是正向代理、反向代理

区别在于代理的对象不一样：

- 正向代理代理的对象是客户端
- 反向代理代理的对象是服务端

比如，翻墙软件是正向代理



# 反向代理

## 示例一：浏览器输入`www.123.com`，跳转到linux系统Tomcat主页

图示：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708173805.png)

1）准备工作

安装Tomcat

防火墙开放端口号

2）在windows的hosts文件配置映射

```
192.168.12.133 www.123.com
```

3）docker中的nginx

- 宿主机IP：192.168.12.133，端口映射情况：8081 -> 172.17.0.2:80，8888 -> 172.17.0.3:8080
- Nginx容器IP：172.17.0.2
- Tomcat容器IP：172.17.0.3

```
    ...
    server{
        listen 80;
        server_name 172.17.0.2;
        location / {
                proxy_pass http://172.17.0.3:8080;
        }
    }
```

4）测试

重启Nginx容器

win10浏览器访问：http://www.123.com:8081/

本来是nginx页面，将跳转到tomcat首页

5）总结

核心配置 proxy_pass

参考：https://www.bilibili.com/video/av68136734?p=9



## 示例二

用docker实现

- 宿主机IP：192.168.12.133
- 宿主机端口映射情况：
    - 8081 -> 172.17.0.2:80
    - 8888 -> 172.17.0.3:8080
    - 8889 -> 172.17.0.4:8080

效果：
- Nginx监听端口：9001
- 访问 http://192.168.12.133:9001/edu 直接跳转到 172.17.0.3:8080
- 访问 http://192.168.12.133:9001/vod 直接跳转到 172.17.0.4:8080

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708173853.png)

**总结**：需要在相应的tomcat中创建edu项目（或说是文件夹）

**得出结论**：nginx只负责请求，不改变后面的路径；若要改变，则用rewrite ——20191121



# 负载均衡

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708173911.png)


