---
title: "CentOS-生产环境"
date: 2017-08-01T10:05:01+08:00
draft: true
tags: [ ""]
categories: ["Linux"]
---

# 生产环境服务器变慢，诊断思路和性能评估

## top-整个系统

`top`命令，按`q`退出，按`h`进入帮助页面，按`f`进入字段介绍页面

主要关注的字段：
- %CPU
- %MEM
- `load average: 0.10, 0.04, 0.04`
 
`uptime`命令是`top`命令精简版

什么是`load average`？它记录了一分钟、五分钟、以及十五分钟的系统平均负载

可参考：https://blog.csdn.net/ztf312/article/details/80342234

## free-内存

查看内存
- `free`
- `free -g`
- `free -m` 以 m 为单位
- 

## df-磁盘

查看硬盘剩余空间`df -h`

## 其它命令

- pidstat 
- vmstat 查看cpu
- iostat 查看磁盘IO
- ifstat 查看网络IO

# 如果生产环境出现CPU占用过高，你的分析思路和定位

1）选用`top`命令找出cpu占用最高的

2）`ps -ef`或`jps -l`进一步定位，得知是怎样的一个后台**进程**给我们惹事

3）定位到具体线程或代码

4）将需要的**线程ID**转换成16进制（英文小写）

5）jstack 进程ID | grep tid(16进制线程ID小写英文)-A60


