---
title: "Nginx-3"
date: 2017-08-01T10:02:08+08:00
draft: true
tags: [ ""]
categories: ["Linux"]
---


# rewrite

## 语法

官方文档：http://nginx.org/en/docs/http/ngx_http_rewrite_module.html

```
Syntax:	rewrite regex replacement [flag];
Default:	—
Context:	server, location, if
```

flag有

- last	本条规则匹配完成后继续向下匹配新的location URI规则
- break	本条规则匹配完成后终止，不在匹配任何规则
- redirect	返回302临时重定向，地址栏改变
- permanent	返回301永久重定向，地址栏改变

参考：https://www.cnblogs.com/brianzhu/p/8624703.html



## last与break区别

示例代码：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708174033.png)

参考：https://www.bilibili.com/video/av55805198?p=67

总结：
- last，匹配到路径后，会按照新的路径请求一次nginx
- break，匹配到路径后，不会按照新的路径请求一次nginx，而是会去root目录寻找文件；如果在location中，则用break，则不是last。
 



