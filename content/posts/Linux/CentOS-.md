---
title: "CentOS-"
date: 2017-08-01T10:01:01+08:00
draft: true
tags: [ ""]
categories: ["Linux"]
---


# 安装CentOS

## 准备资料

![centos7-1](https://oscimg.oschina.net/oscnet/8fcc8906887806e8dc873cb9ec209886499.jpg "centos7-1")

## 创建一台祼机电脑

![centos7-3](https://oscimg.oschina.net/oscnet/48a8b9a042bb3e5b985c9cee91136fe1542.jpg "centos7-3")
![centos7-4](https://oscimg.oschina.net/oscnet/ec38ee7139b926fdf9249e7f498baa0404a.jpg "centos7-4")
![centos7-5](https://oscimg.oschina.net/oscnet/6153f4a81f175e8d7e5c00cddf7eee00ccd.jpg "centos7-5")
![centos7-6](https://oscimg.oschina.net/oscnet/358f951d552d6ec00da9b397022fddc6ddf.jpg "centos7-6")
![centos7-7](https://oscimg.oschina.net/oscnet/88ac7b56d1fc3b3da220b22cc6e06f252a3.jpg "centos7-7")
![centos7-8](https://oscimg.oschina.net/oscnet/b0fff9a7c0bbd9965144e3b673948659e8d.jpg "centos7-8")
![centos7-9](https://oscimg.oschina.net/oscnet/3818701f4c4201cc477f410b5c26f207254.jpg "centos7-9")
![centos7-10](https://oscimg.oschina.net/oscnet/397cb268ba4b985f984ebfe372c737310a2.jpg "centos7-10")
![centos7-11](https://oscimg.oschina.net/oscnet/ba356aae0cbb2f085d8145a5ce77e65711a.jpg "centos7-11")
![centos7-12](https://oscimg.oschina.net/oscnet/a09cfdd76ffa4c7e0f95f3cb004a78470d6.jpg "centos7-12")
![centos7-13](https://oscimg.oschina.net/oscnet/969ebc91457a655d3654dd9419c6928b2b9.jpg "centos7-13")
![centos7-14](https://oscimg.oschina.net/oscnet/41a52d547c7257777a499d202c01dd3f8a2.jpg "centos7-14")

## 安装操作系统

分区大小：  
![centos7-2](https://oscimg.oschina.net/oscnet/e09d2685a1ca4b94a5cf508d7f1dfb4017c.jpg "centos7-2")

交换分区：  
![centos7-15](https://oscimg.oschina.net/oscnet/17497f169ca8469da122d11edec567be7fc.jpg "centos7-15")

为什么要交换分区？

在硬件技术不发达的时候，把硬盘的一部分空间开辟出来当内存来使用，内存中很长时间也不使用的数据放到该交换分区里边运行，交换分区大小建议是内存的2倍。

设置启动磁盘分区，可以保障电脑的正常开关机。  
![centos7-16](https://oscimg.oschina.net/oscnet/53d4af4eb98e236068d8cc77b12881766f1.jpg "centos7-16")

![centos7-17](https://oscimg.oschina.net/oscnet/45f3dd2b936e59233f7d6124214bcebdc4c.jpg "centos7-17")
![centos7-18](https://oscimg.oschina.net/oscnet/6a839dd5e3cb9af98347fc1870293cbab69.jpg "centos7-18")
![centos7-19](https://oscimg.oschina.net/oscnet/396bf0caf5527c251320039a2ba7f352688.jpg "centos7-19")
![centos7-20](https://oscimg.oschina.net/oscnet/5dcc71cd13c9d84c6d3ba3e2f70517d6b05.jpg "centos7-20")
![centos7-21](https://oscimg.oschina.net/oscnet/e20570f3afbbd00f5efc6517f91316cfeb3.jpg "centos7-21")
![centos7-22](https://oscimg.oschina.net/oscnet/5827439e294644934e3c9857832a6290650.jpg "centos7-22")

# 系统分区原理

Windows:  
![centos7-23](https://oscimg.oschina.net/oscnet/2de96a66b0802f9a1c3a0bab31062963791.jpg "centos7-23")

Linux:  
![centos7-24](https://oscimg.oschina.net/oscnet/b47867f3ff7517bd09b1d43f1d2e4e37d03.jpg "centos7-24")

# 根目录介绍

## /bin

Binary的简写，存放二进制命令。许多“指令”对应的可“执行程序文件”的目录，如 ls、pwd、init等。

## /sbin

Super binary，超级的二进制。该目录文件对应指令都是“root”用户可以执行的指令，如init。

## /usr

Unix system resource（unix系统资源文件目录）。该目录类似win系统的c:/Program File目录，该目录经常用于安装各种软件。

软件安装完毕会形成对应的指令，该指令对应的可执行程序文件存放在以下目录  
1）/usr/bin  
许多“指令”对应的“可执行程序文件”目录  
2）/usr/sbin  
root用户执行的指令，对应的“可执行程序文件”目录

## /dev

Device	系统硬件设备目录（linux系统所有的硬件都通过文件表示）。
如：/dev/cdrom	是光驱  
/dev/sda	是第一块scsi硬盘

## /home

用户的“家目录”

给系统每增加一个普通用户时，都会在该目录为该用户设置一个home目录，家目录名字默认与当前用户名字一致，用户对家目录拥有绝对的最高权限。

## /root

该目录是root管理员的家目录，root用户登录系统时会首先进入该目录。

## /proc

内存映射目录，该目录可以查看系统的相关硬件信息

## /var

variable，可变的，易变的。

该目录存储的文件经常会发生变动（增、删、改），经常用于部署项目程序文件。如：`/var/www/shop`，`/var/www/book`。

## /boot

系统启动核心目录，用于存储系统启动文件

## /etc

系统主要配置文件目录

- /etc/passwd，用于存储用户信息，格式是：用户名：密码：UID：GID：用户全名：home目录：shell
- /etc/group，用于存储级别信息

## /lib

Library，系统资源文件类库目录

## /selinux

Secure enhanced	linux安全增强型linux

对系统形成保护，会对给系统安装软件时有干扰作用。

# 用户与组别

Linux系统对用户、组别、被操作的文件有独特的设置：用户与组别对应、组别与被操作的文件对应。  
![centos7-25](https://oscimg.oschina.net/oscnet/51b0d3b7446e56cc90e083f211f8c2d0178.jpg "centos7-25")
***
![centos7-26](https://oscimg.oschina.net/oscnet/874023439ac962be7d29d900015e9e64068.jpg "centos7-26")
![centos7-27](https://oscimg.oschina.net/oscnet/519da7d1510f18dba44de48509fecf68407.jpg "centos7-27")

# VI编辑器

![centos7-28](https://oscimg.oschina.net/oscnet/a2c4c6809cc3efdbdac83c7899f15e649ae.jpg "centos7-28")

## 命令模式

### 移动光标

```t
字符级：
	上下左右键
	上（k） 下（j） 左（h） 右（l）
单词级：
	w：word移动到下个单词的首字母
	e：end移动到下个（本）单词的尾字母
	b：before移动到上个（本）单词的首字母
行级：
	$：行尾
	0（数字）：行首
段落级（翻屏）：
	{：上个（本）段落首部
	}：下个（本）段落尾部
屏幕级（不翻屏）：
	H：屏幕首部
	L：屏幕尾部
文档级：
	G：文档尾部
	1G：文档第1行
	NG：文档第n行
```

### 删除内容

```t
dd：	删除光标所在行
2dd：	包括当前行在内，向后删除2行内容
ndd：	包括当前行在内，向后删除n行内容

按x键：删除光标所在字符
c + w：从光标所在位置删除到单词结尾，并进入编辑模式
```

### 复制内容

```t
y：		在使用v模式选定了某一块的时候，复制选定块到缓冲区用；
yy：		复制光标所在行
2yy：	包括当前行在内，向后复制2行内容
nyy：	包括当前行在内，向后复制n行内容

按v选好内容后，按y是复制内容，按d是剪切内容。

p：		对复制好的内容进行粘贴
```

### 其它操作

```t
u：undo撤消
J：合并上下两行
r：单个字符替换
.点：重复执行上次最近的指令
```

## 编辑模式

进入编辑模式的四种方式：
```
a：光标向后移动一位
I：光标和所在字符不发生任何变化
o：新起一行
s：删除光标所在字符
```

## 尾行模式

```t
:q		//quite退出编辑器
:w		//wirite对修改后的内容进行保存
:wq		//write quite 保存修改并退出
:q!		//强制退出

:w!		//强制保存
:wq!		//强制保存并退出

:set number 或 nu			//设置行号
:set nonumber  或 nonu	//取消行号

:/内容/ 或 /内容		//查找指定内容，
					//按小写n键查找下一个，
					//大写n查找上一个
:数字    //跳转到指定行

字符串替换：将countq替换为count2
:s/count1/count2			//首先会替换光标所在行的第一个适配字符串
:s/count1/count2/g			//替换光标所在行所有适配字符串
:%s/count1/count2/g		//替换所有适配字符串
```

# 权限

权限：用户针对文件是否拥有读、写、执行的权限。

权限分类：  
本身划分：读 Read、写 Write、执行 execute；  
针对用户划分：主人User、同组用户Group、其它用户组Other。

![centos7-29](https://oscimg.oschina.net/oscnet/20c5bfc79aefa43dcd533272596896643b1.jpg "centos7-29")

权限有3-3-3结构，对应主人、同组、其它组的RWX权限。

# 管道pipe

前面的输出作为后面的输入。
linux中很多指令都可以作管道来使用，如：grep head tail wc ls等等。
```c
> ls -l | wc
> grep sbin passwd | wc
> ls -l | head -20 | tail -5		//查看第16-20个文件/目录
```

# 软链接&硬链接

## 软链接

软链接：相当于windows中的快捷方式。

作用：可以对硬盘空间进行合理分配。

具体设置：`>ln -s 源文件 软件链接`。

软链接和源文件不在一个目录时，在命令中，“源文件”要是绝对路径，否则是个无效的软链接。如 `> ln -s a.txt /var/b.txt`，则 /var 下的b.txt文件是无效的。

建立软链接后，对软链接的所有操作，就是对源文件的操作。

注意事项：  
1）软链接本身占用一定的空间，用于保存文件地址。因此，跟源文件不在一个目录的软链接要比在同一个目录的软链接占用空间大。  
![centos7-30](https://oscimg.oschina.net/oscnet/975ff953f701142efd079f5fd232840ccee.jpg "centos7-30")  
2）软链接不但可以对文件建立链接，还可以对目录建立链接。  
3)源文件被删除后，软链接不可以用（变成红色），源文件恢复后，软链接又可用。

## 硬链接

定义：系统里面文件的名称（引用）就是硬链接。给文件增加名称（引用）的过程就是创建“硬链接”（一个文件有多个名字，它们都是同一个文件实体的硬链接）。

命令：`> ln [-d] 源文件 硬链接`。

![centos7-31](https://oscimg.oschina.net/oscnet/9eb136b34f36a48a029ba5009b892cfe453.jpg "centos7-31")

同一个文件实体如果有多个名称（引用），即使其中其中一个名称被删除了，也不影响其它名称的使用（该文件实体依然存在）。

如果一个文件实体只有一个名称，执行rm删除文件实体时，只是删除文件名字而已，系统的垃圾回收机制会定期检查该文件是否存在名称引用，如果不存在，就当垃圾收走。

![centos7-32](https://oscimg.oschina.net/oscnet/1e43566933a80e061bb7bb06e0a50fc9122.jpg "centos7-32")

查看索引号码：`> ls -i 	//查看文件索引号码`。

`> ln -d a.txt /var/b.txt	//创建硬链接，源文件不用写绝对路径`

![centos-33](https://oscimg.oschina.net/oscnet/9606f9f3ba5de766572310ed52ebf602b4b.jpg "centos-33")

新建的文件夹，为什么有两个硬链接？

答：
这是文件夹本身的索引号：  
![centos-34](https://oscimg.oschina.net/oscnet/0ab7c0d8193a06023e76813dd2cfd09673d.jpg "centos-34")  
这是sz文件夹中的内容：  
![centos-35](https://oscimg.oschina.net/oscnet/67bdd8c4643854d8b810427ad04505beab0.jpg "centos-35")  
. 表示引用它自身  
.. 表示sz的上一级目录的索引号，如下  
![centos-36](https://oscimg.oschina.net/oscnet/7b28ebda9472baa77cf889de830437ff612.jpg "centos-36")  
也就是说目录结构是：tar>sz>. ..

硬链接注意事项：  
1）“> ln [-d] 源文件 硬链接”命令中的源文件，不用写绝对路径  
2）硬链接只能对普通文件建立链接，不能对目录建立  
3）同一个源文件的所有硬链接文件必须 在同一个硬盘、同一个分区里边

好处：  
1、防止重要文件被误删除  
2、如果有多人需要操作同一个文件，就给他们创建很多硬链接即可（如百度网盘）

## 软&硬比较

不同点：  
软链接是快捷方式，可以用在硬盘空间合理分配上  
硬链接就是文件的名字，一个文件的许多名字互为硬链接

相同点：  
百万都是针对同一个目录进行操作

# 任务调度指令

定义：规定系统在指定的时间完成指定的任务的过程。

命令：  
`> crontab -e		//编辑任务高度指令`  
`> crontab -l		//查看任务高度指使`

![centos-37](https://oscimg.oschina.net/oscnet/1640138ddea4dcf1f525458209a324b68f3.jpg "centos-37")

执行 crontab -e 指令后，会出现编辑框，如下：  
示例：  
![centos-38](https://oscimg.oschina.net/oscnet/4632460f4dcaafbc854f7fb68402407958f.jpg "centos-38")

示例中的命令：（将目录列表打印到一个文本文档）  
![centos-39](https://oscimg.oschina.net/oscnet/325202b6a25751d08cbf922fbf3283f840a.jpg "centos-39")

# 设置文件主人和组别及递归效果

```c
change owner
> chown 主人 filename
> chown 主人.组别 filename
> chown .组别 filename
> chown -R 主人:组别 dir 		//递归方式设置目录主人和级别

> chmod -R 765 dir				//递归方式设置目录的权限
```

# 配置网络

## 虚拟机网络配置

vmware以NAT模式链接，因此要配置链接的ip、网关

![centos-40](https://oscimg.oschina.net/oscnet/b0054e0a25a7698df56920f2aff8f9101fc.jpg "centos-40")

## 配置centos网络

```t
1）配置网络：
vi /etc/sysconfig/network-scripts/ifcfg-ens33	# 添加如下内容
TYPE=Ethernet
BOOTPROTO=static
IPADDR0=192.168.12.132
PREFIX0=24
GATEWAY0=192.168.12.2
DNS1=192.168.12.2
DNS2=8.8.8.8
DEFROUTE=yes
PEERDNS=yes
PEERROUTES=yes
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_PEERDNS=yes
IPV6_PEERROUTES=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=ens33
UUID=dde7804b-22b7-4a38-ad6c-aeeefd44bb86
DEVICE=ens33
ONBOOT=yes

注意：GATEWAY0=192.168.12.2 要跟vmware中的网关一致
（如果vmware网络以桥接模式连接，则网关要跟当前网段的网关一致，？？？）


经实践，只需要配置这几项就行

DEVICE=eth0
HWADDR=00:0C:29:69:69:0E
TYPE=Ethernet
UUID=e0b1cef5-fbd7-4b76-b89d-f034ce407f7
NM_CONTROLLED=yes

# 以下几项是重点
ONBOOT=yes
BOOTPROTO=static
IPADDR=192.168.12.133
GATEWAY=192.168.12.2
DNS1=192.168.12.2
DNS2=8.8.8.8

2）重启网络：
># service network restart
```

网络最简配置 ——20190710

```
TYPE=Ethernet
BOOTPROTO=static
DEVICE=ens33
ONBOOT=yes
IPADDR=192.168.12.133
GATEWAY=192.168.12.2
DNS1=192.168.12.2
DNS2=8.8.8.8
```


# 光驱挂载

## 概述

目的是从光驱中读取数据。

前提是，要让光驱连接电脑，并通电，且把光盘放入光驱中（以便读取它的数据），如下图：  
![centos-41](https://oscimg.oschina.net/oscnet/5eba9a9d4965816a8da5f6d402ce14ce5ab.jpg "centos-41")

光驱使用，其挂载是手动的，步骤如下：  
1）创建一个普通目录  
2）找到光驱硬件设备（/dev/cdrom）  
3）使得普通目录与光驱设备进行联系（挂载）

下图是光驱（硬件）位置，且是个软链接：  
![centos-42](https://oscimg.oschina.net/oscnet/e0f57766877e0120d5be84ae2cf3fc3ec0a.jpg "centos-42")

## 命令

```c
mount 硬件 挂载目录（普通目录）		//挂载动作
> mount /dev/cdrom /home/jinnan/rom	//把光驱挂载到rom目录
umount 硬件或挂载点					//挂载动作
> umount /dev/cdrom					//（硬件）卸载光驱
> umount /home/jinnan/rom				//（挂载点）卸载光驱
> eject								//弹出光盘，对应“已连接”处不打勾
```

# Linux系统软件安装

## 二进制码软件安装

二进制码安装：把从网络下载的“二进制码”软件从安装包复制到系统指定目录的过程。

### rpm方式

该方式软件安装本质与win系统软件安装方式一致，就是把软件包里边的二进制代码文件复制到系统指定目录（C:/Program Files）的过程。  
优点：软件安装方便、快速  
缺点：软件的各个组成部分非常固定，不灵活。需要手动解决依赖关系。

示例：安装FTP：File Transfer protocol（文件传输协议）

```c
rpm方式安装vsftpd软件：
> rpm -ivh 软件包命名			//安装软件
> rpm -q	软件包名（完整）	//query查看是否有安装
> rpm -e 软件包名（完整）		//卸载软件
> rpm -qa						//query all 查看系统里所有以rpm方式安装的软件
> rpm -qa | grep ftpd（部分名字）//模糊查看是否安装ftpd软件

软件包全名 = 软件包名 + 软件版本 + 支持的系统 + 支持的CPU + 文件后缀
如：firefox-45.4.0-1.el7.centos.x86_64.rpm
（有时候文件文件后缀会省略）

启动FTP服务：
> service vsftpd start/stop/restart		//启动ftp服务
> ps -A | grep ftp					//查看是否启动了ftp服务

测试本机与linux的ftp端口通不通（使用windows的telnet）
如果不通，
># setup			//通过界面方式关掉Linux防火墙
```

### yum智能方式

该方式类似360软件管理里边的“一键安装”，较智能。  
该方式的条件：  
1）可以上上网；  
2）通过配置把软件放到指定位置

好处：方便，一键安装，无需考虑软件依赖。

`> yum install phps`

## 源码安装

该软件安装本质：从网络下载下来的软件，内部文件内容都是源码内容。

![centos-43](https://oscimg.oschina.net/oscnet/be06ee7e290523bc5d1c63194081164d0a8.jpg "centos-43")

软件安装的时候：  
1）把“源码内容”文件编译为“二进制码”文件  
2）再把编译后的二进制代码文件复制到系统指定目录。  
优点：运行效率高、执行速度快，软件内部各个组成部分可以灵活配置（例如PHP里边有gd/xml/jpeg/png等各个部分组成，都可以灵活选取）  
缺点：安装稍麻烦

源码的安装之前，一般要先安装其依赖软件。
todo...

## 两种安装方式的比较

二进制码安装方式与源码安装方式的比较：  
1）软件安装后使用的的用户非常少（如：公司内部人使用的ftp、root管理员使用的gcc），就采用二进制码安装方式。  
2）软件安装完毕使用者非常多、非常巨大（php、apache、mysql），就采用源码编译方式安装。

# 开机启动

CentOS设置服务开机启动的两种方法：  
1、利用 chkconfig 来配置启动级别  
chkconfig –-add postfix  
注意：此种方式的前提是要拷贝要启动的程序文件到 /etc/init.d目录，如mysql、redis_cli

2、修改 /etc/rc.d/rc.local 这个文件

参考：
http://blog.csdn.net/educast/article/details/49558945