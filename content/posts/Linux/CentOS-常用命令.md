---
title: "CentOS-常用命令"
date: 2017-08-01T10:01:02+08:00
draft: true
tags: [ ""]
categories: ["Linux"]
---

# 系统相关

## sudo

这些的前提是要通过sudo的配置文件/etc/sudoers来进行授权


## 快捷键

xShell或Putty编辑器中使用的快捷键  
1）清屏：CTRL + L  
2）删除光标左边的输入内容：CTRL + U  
3）删除光标右边的输入内容：CTRL + K  
4）退出登录：CTRL + D  
5）杀掉进程：CTRL + C  
6）停止进程：CTRL + Z，恢复进程：fg  
7）锁住屏幕：CTRL + S，恢复屏幕：CTRL + Q

## 查看指令可设置的参数

`> man 指令`

总结：查看手册时，synopsis是命令的基本格式，option是可选操作。其中option描述中“--参数描述”是对前面命令字母进行解释，如果“--参数描述”后面空格后有单词，则此单词是此参数的入参。

如`> man usermod `，  
1）它的synopsis是这样的：usermod [options] LOGIN，其中LOGIN（用户名）是必须；  
2）option中-d, --home HOME_DIR，作如下解释：  
-d：参数名称  
--home：对-d的解释，说明是家目录  
HOME_DIR：是-d参数的必填参数，是个目录


## 系统信息

```sh
> uname -a	//查看当前Linux系统版本
> $ cat /etc/issue 	// 查看发行版本

cat /etc/centos-release
lsb_release -a //适用于所有的linux，包括Redhat、SuSE、Debian等发行版，但是在debian下要安装lsb

> lscpu //查看cpu信息
```


## 时间

```sh
> date		//查看时间
> # date -s "2017-07-16 01:38:30"		//设置时间（root用户)
```



## 其它常用指令

```shell
> df -lh		//查看系统分区情况

> wherei java	//查看应用程序安装在哪里

> wget -c http://xxx.xx.xxx.tar.gz	//???

> alias 	//查看命令别名列表

histtory #查看当前用户使用过后历史命令
```

## 管道pipe

前面的输出作为后面的输入。linux中很多指令都可以作管道来使用，如：grep head tail wc ls等等。

```c
> ls -l | wc
> grep sbin passwd | wc
> ls -l | head -20 | tail -5		//查看第16-20个文件/目录
```

## 任务调度指令

```
> crontab -e		//编辑任务高度指令
> crontab -l		//查看任务高度指使
```

## 磁盘相关

```c
># fdisk -l		// linux 系统分区信息
># df -h		//分区使用情况

du -sh * | sort -n //查看当前目录下的文件大小
```

参考：https://www.cnblogs.com/QuestionsZhang/p/10329483.html

## 防火墙

centos7

```sh
service firewalld status # 查看防火墙状态

systemctl stop firewalld.service #停止firewall
systemctl disable firewalld.service #禁止firewall开机启动
systemctl enable firewalld.service #启用firewall开机启动

service iptable stop #关闭防火墙，TO CHECK...

sudo firewall-cmd --query-port=3306/tcp 	# 查看端口是否开放
sudo firewall-cmd --add-port=3306/tcp		# 临时开启3306端口
sudo firewall-cmd --permanent --zone=public --add-port=3306/tcp	# 永久开启3306端口
```

开放端口

```shell
# firewall-cmd --add-service=http --permanent
# firewall-cmd --add-port=8888/tcp --permanent
# firewall-cmd --reload 重启防火墙

# firewall-cmd --list-all 查看开放的端口号
```


red hat 6.5

```sh
#查看是否开启：
service iptables status
#关闭方法：
service iptables stop
```

查看防火墙规则 ——20190713

```sh
[root@localhost ~]# iptables -t nat -S
-P PREROUTING ACCEPT
-P INPUT ACCEPT
-P OUTPUT ACCEPT
-P POSTROUTING ACCEPT
-N DOCKER
-A PREROUTING -m addrtype --dst-type LOCAL -j DOCKER
-A OUTPUT ! -d 127.0.0.0/8 -m addrtype --dst-type LOCAL -j DOCKER
-A POSTROUTING -s 172.22.0.0/24 ! -o br-2795741963a5 -j MASQUERADE
-A POSTROUTING -s 172.18.0.0/16 ! -o br-e3e5cd5694b2 -j MASQUERADE
-A POSTROUTING -s 172.17.0.0/16 ! -o docker0 -j MASQUERADE
-A DOCKER -i br-2795741963a5 -j RETURN
-A DOCKER -i br-e3e5cd5694b2 -j RETURN
-A DOCKER -i docker0 -j RETURN
```




## 网络

查看所有的ip：`>$ ip a`

开启/关闭网络
```c
># ifdown <设备名>
># ifup <设备名>

>$ curl --head https://www.baidu.com 	# 查看请求头
```

修改hosts文件：

```shell
vim /etc/hosts
```

查看IP

```shell
ifconfig

ip addr show
```



## CPU、内存运行情况

`> top`

## 环境变量

用户局部变量：`~/.bash_profile`

全局变量：`/etc/profile`


java环境变量：
```t
JAVA_HOME=xxx/xxx/xxx
PATH = $PATH:$JAVA_HOME/bin
export PATH
```

## yum

```shell
yum install xxx
yum remove xxx
yum history
yum history info 数字

yum repolistj #查看有哪些镜像源
vim /etc/yum.repos.d/nginx.repo #添加yum源，参考《Nginx-1》
```

- CentOS 镜像：https://developer.aliyun.com/mirror/centos?spm=a2c6h.13651102.0.0.53322f701HLPB8

```
1. 备份
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
2. 下载新的 CentOS-Base.repo 到 /etc/yum.repos.d/
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
```



# 进程、端口相关

查看进程：

```sh
$ ps aux | grep redis # 查看 redis 进程

$ ps -ef | grep ora

$ ps -ef | grep 6379

$ sp -A		# 查看系统活跃进程

```
ps -ef 输出结果的具体含义：https://blog.csdn.net/lg632/article/details/52556139

https://blog.csdn.net/qq_30505673/article/details/85142311

---

结束进程：

```shell
kill -9 pid	 # 杀死pid进程

pkill -9 redis-server

killall 进程名
killall nginx
```

---

端口

```
$ netstat -ntlp 	# 查看端口占用

ss -ntlp
netstat -anp | grep portno
$ netstat  -anp  | grep orc
netstat –apn | grep 8080
```


# 文件相关

## 列出文件

```c
> ls		//list 列出目录
> ls -l	//list -list 以“详细信息”查看目录
>ls -a	//list -all 查看目录“全部”（包括隐藏文件）信息
>ls -al	//list -all list 查看目录“全部”（包括隐藏文件），以“详细信息”展示
> ls 目录	//查看指定目录下有什么文件

> # pwd //当前所在目录

>ll

> du -h 文件	//文件或目录占据磁盘的空间（block块默认=4k）
```

## 查找文件

```c
>which 指令	//查看一个指令对应的执行程序文件在哪
如：which ls		//查看ls指令在哪个目录

> grep 关键字 filename		//将文本中指定的信息匹配出来

1）根据名称查找文件
find 查找目录 选项 选项值 选项 选项值...
> find / -name passwd	//“递归遍历”/根目录及其内部深层目录，查找passwd文件
>find / -name “pas*”	//模糊查找名称以pas开关的文件
>find / -name “*pas*”	//模糊查找名称包含pas的文件


2）限制查找的目录层次 -maxdepth -mindepth
-maxdepth	限制查找的最深目录
-mindepth	限制查找的最浅目录
>find / -maxdepth 4 -name passwd 
>find / -maxdepth 4 -mindepth 3 -name passwd //在目录3和4层次之间查找

3）根据大小查找文件
-sise +/-数字
+：大于某个范围
-：小于某个范围
单位大小：
-size 5	//单位是“512字节”		5*512字节
-size 10c	//单位是“字节”			10字节
-size 3k	//单位是“千字节”		3*1024字节
-size 6M	//单位是M				6*1024*1024字节
> find ./ -size 14c		//查找当前目录大小等于14字节的文件
>find / 
4）
```

## 操作目录&文件

`> cd ~	或 cd	//切换到用户家目录`

### 创建目录

```c
> mkdir	目录名字	//创建目录
> mkdir -p newdir/newdir/newdir	//递归方式创建连续目录
>mkdir dir/newdir			//不用-p参数
> mkdir -p dir/newdr/newdir	//使用-p参数
```

### 移动目录&文件

```c
> mv dir1 dir2			//把dir1移动到dir2目录下
>mv dir1/dir2 dir3		//把dir2移动到dir3目录下
>mv dir1/dir2 dir3/dir4	//把dir2移动到dir4目录下
>mv dir1/dir2 ./		//把dir2移动到当前目录下
```

### 改名目录&文件

```c
>mv dir1	newdir		//把目录dir1改名为newdir

把“移动”和“改名”合并
>mv dir1	newdir		//把dir1移动到当前目录，并改名为newdir
> mv dir1/dir2 dir3		//把dir2移动到dir3目录下，并改名为原名
> mv dir1/dir2 dir3/newdir	//把dir2移动到dir3目录下，并改名为newdir
>mv dir1/dir2 dir3/dir4	//把dir2移动到dir4目录下，并改名为原名
>mv dir1/dir2 dir3/dir4/newdir	//把dir2移动到dir4目录下，并改名为newdir
```

### 复制目录&文件

```c
文件复制
> cp file1 dir/file2		//把fiel1复制到dir下，并命名为file2
> cp file1 dir			//把fiel1复制到dir下，并命名为原名
> cp dir1/file1 dir2/newfile	//把file1复制到dir2下，并命名为newfile
> scp local_file remote_username@remote_ip:remote_folder	//从本地linux机上复制文件到另一linux机上 

目录复制（需要设置-r[recursive]参数，忽略目录层次）
> cp -r dir1 dir2				//把dir1复制到dir2下，并改为原名
> cp -r dir1/dir2 dir3/dir4		//将dir2复制到dir4下，并改为原名
> cp -r dir1/dir2 dir3/dir4/newdir		//将dir2复制到dir4下，并改为newdir
> scp -r local_folder remote_username@remote_ip:remote_folder	//从本地linux机上复制文件到另一linux机上

复制文件夹中的内容到另一个文件夹，用.
docker cp 5d897b0067c0:/etc/nginx/. /lwx/nginx/conf
```

### 删除目录&文件

```c
>rm 文件
>rm -r 目录			//递归删除
>rm -rf 文件/目录		//r[recursive]、f[force]，递归并强制删除文件/目录

小心：
> rm -rf /
```

### 查看文件内容

```c
> cat filename			//打印文件内容到终端
> more filename		//通过回车逐行查看，
					//默认从第一行开始查看
					//不支持回看
					//q	退出查看
> less filename		//通过“上下左右”键查看文件的各个部分
					//支持回看
					//q 退出查看
> head -n filename		//查看文件前 n 行内容
> tail-n filename		//查看文件最末尾 n 行内容


> wc	 filename			//查看文件有多少行，多少句话，多少大小（字节）

wc  -l : 统计行
wc  -c: 统计字节数
wc  -m:统计字符数，不能与-c同时使用
wc  -w:统计字数
wc  -L:打印最长长度
```

### 创建文件

```c
> touch filename			//在当前目录创建filename文件
> touch dir1/filename		//在dir1目录创建filename文件
```

### 给文件追加内容

```c
> echo 内容 > 文件名称	//将“内容”以【覆盖写】方式追加到文件中
> echo 内容 >> 文件名称	//将“内容”以【追加】方式追加到文件中
（如果文件不存在，则会创建文件，并写入内容）
（“内容”可以不用引号）

> ls -al >/>> filename
> cat file1 >/>> fie2
```

### 解压&压缩

```c
tar文件
> tar -zcvf /home/test/xxxx.tar.gz tar		//压缩，将当前目录下的tar目录/文件
											//打包并压缩到/home/test目录下，
											//并命名为 xxxx.tar.gz
> tar -zxvf 压缩包.tar.gz					//解压，压缩包.tar.gz 包到当前目录

bz2文件
> tar jcvf 文件名.tar.bz2 目标文件		//压缩：将当前目录下的xxx目录/文件
									//打包并压缩到/home/test目录下，
									//并命名为 xxxx.tar.bz2
> tar jxvf 压缩包.tar.bz2					//解压

zip文件
> zip （新）文件名.zip 待压缩的目标文件	//压缩
> unzip 文件
```

### 两台linux之间传文件

将前置机（linux）中的tomcat上传到应用服务器（linux）  
`scp /home/platform-xy/apache-tomcat-7.0.73.tar.gz root@10.131.53.126:/opt/tomcat-platform`

拷贝文件夹：

```sh
scp -r redis-3.0.0/ 192.168.12.133:/usr/local
```


# 用户相关

## 切换用户

```c
> su - 或 su - root		//向root用户切换
> exit	//退回到原用户

> su 用户名	//普通用户切换

（注意：多次使用su指令，会造成用户的“叠加”，所以 su与exit最好匹配使用。）

> whoami		//查看当前用户是谁

>sudo su
```

## 用户操作

配置文件：/etc/passwd

```c
># useradd 用户名			//添加一个用户（密码呢？），同时创建一个同名组别
># useradd -g 级别编号 username		//添加一个用户，避免创建一个同名组别
># useradd -g 级别编号 -u 用户编号 -d 家目录 username
># useradd -g 1002 xiaogang

># usermod username		//修改用户
># usermod -g 组编号 -u 用户编号 -d 家目录 -l 新用户名 当前用户名

># userdel username		//删除用户
># userdel -r username		//删除用户，并删除相应的家目录

># passwd 用户名		//给用户设置密码，回车后，会让你输入要设置的密码的

（只有root用户有权限操作）

查看Linux某用户属于哪个组
id  user
groups user
```

## 组别操作

配置文件：/etc/group

```c
># groupadd goupname		//添加一组别
># groupadd music	
># groupadd movie
># groupadd php

># groupmod goupname	//修改一组别
># groupmod -g gid -n 新名字 groupname

># groupdel goupname		//删除一组别，如果组下有用户，则禁止删除

（只有root用户有权限操作）
```

## 文件主人和组别及递归效果

```c
change owner
> chown 主人 filename
> chown 主人.组别 filename
> chown .组别 filename
> chown -R 主人.组别 dir 		//递归方式设置目录主人和级别

> chmod -R 765 dir				//递归方式设置目录的权限
```

# 权限相关

## 设置文件&目录权限

### 字母相对方式

针对一个组另设置权限，其它组别权限没有变化，称为“相对方式”权限设置。

```c
> chmod u+rwx filename		//给filename文件的主人增加“读、写、执行”权限
> chmod g-rx filename		//给filename文件的同级用户删除“读、执行”权限

chmod u+/-rwx,g+/-rwx,o+/-rwx filename
说明：
1）每个单元“+”，“-”只能使用一个
2）可以同时给一个组或多个组设置权限，组别之间使用“，”分割
3）每个单元的权限可以是“rwx”中的一个或多个
> chmod u+w,g-rx,o+rw filename		//给filename文件主人增加写权限
									//同级删除读、执行权限
									//其它组增加读、写权限
> chmod u+w,u-x filename

chmod +/- rwx filename		//无视具体级别，统一给全部的组设置权限
> chmod +rw filename 

（普通用户即可操作，不一定要是root用户）
```

### 数字绝对方式

r读：4  
w写：2  
x执行：1

具体数字含义如下：  
0：没有权限  
1：执行  
2：写  
3：写、执行  
4：读  
5：读、执行  
6：读、写  
7：读、写、执行

chmod ABC filename	//ABC分别代表主人、同组、其它组用户的数字权限  
`> chmod 753 filename`

### 两种方式的比较

1）修改的权限相对较少的时候，使用字母方式（可以对个别权限设置）  
2）修改的权限相对较多的时候，使用数字方式（可以对全部权限设置）

## 权限的应用

文件：  
1）读：是否可以查看文件内容  
2）写：是否可以修改该文件  
（如果不让写，可以使用强制写操作，为不让写，可以把上级目录设为不可读、不可写）  
3）执行：二进制文件 或 批量批量文件（shell）

目录：  
1）读：是否可以查看该目录内部的文件信息  
2）写：是否可以给该目录创建、删除文件  
3）执行：指定用户是否可以cd进入该目录

# 安装软件

## 二进制方式

### rpm方式

```c
> rpm -ivh 软件包命名			//安装软件
> rpm -q	软件包名（完整）	//query查看是否有安装
> rpm -e 软件包名（完整）		//卸载软件
> rpm -qa						//query all 查看系统里所有以rpm方式安装的软件
> rpm -qa | grep ftpd（部分名字）//模糊查看是否安装ftpd软件
> rpm -qi xxx 					//安装的软件的信息和说明


> rpm -Uvh xxx
```

### yum方式

```c
> yum install phps

>yum remove xxxx

配置yum源连接超时时间：
># vim /etc/yum.conf
添加 timeout=120		# 120 seconds
```

## 源码方式

todo...

# 挂载光驱

```c
mount 硬件 挂载目录（普通目录）		//挂载动作
> mount /dev/cdrom /home/jinnan/rom	//把光驱挂载到rom目录
umount 硬件或挂载点					//挂载动作
> umount /dev/cdrom					//（硬件）卸载光驱
> umount /home/jinnan/rom				//（挂载点）卸载光驱
> eject								//弹出光盘
（root用户）
```

# VI编辑器

![vi](https://oscimg.oschina.net/oscnet/a2c4c6809cc3efdbdac83c7899f15e649ae.jpg "vi")

## 命令模式

### 移动光标

```t
字符级：
	上下左右键
	上（k） 下（j） 左（h） 右（l）
单词级：
	w：word移动到下个单词的首字母
	e：end移动到下个（本）单词的尾字母
	b：before移动到上个（本）单词的首字母
行级：
	$：行尾
	0（数字）：行首
段落级（翻屏）：
	{：上个（本）段落首部
	}：下个（本）段落尾部
屏幕级（不翻屏）：
	H：屏幕首部
	L：屏幕尾部
文档级：
	G：文档尾部
	1G：文档第1行
	NG：文档第n行
```

### 删除内容

dd：	删除光标所在行  
2dd：	包括当前行在内，向后删除2行内容  
ndd：	包括当前行在内，向后删除n行内容  

按x键：删除光标所在字符  
c + w：从光标所在位置删除到单词结尾，并进入编辑模式

### 复制内容

y：		在使用v模式选定了某一块的时候，复制选定块到缓冲区用；  
yy：		复制光标所在行  
2yy：	包括当前行在内，向后复制2行内容  
nyy：	包括当前行在内，向后复制n行内容

按v选好内容后，按y是复制内容，按d是剪切内容。

p：		对复制好的内容进行粘贴

### 其它操作

u：undo撤消  
J：合并上下两行  
r：单个字符替换  
.点：重复执行上次最近的指令

## 编辑模式

进入编辑模式的四种方式：  
a：光标向后移动一位  
I：光标和所在字符不发生任何变化  
o：新起一行  
s：删除光标所在字符

## 尾行模式

:q		//quite退出编辑器  
:w		//wirite对修改后的内容进行保存  
:wq		//write quite 保存修改并退出  
:q!		//强制退出

:w!		//强制保存  
:wq!		//强制保存并退出

:set number 或 nu			//设置行号  
:set nonumber  或 nonu	//取消行号

:/内容/ 或 /内容		//查找指定内容，
//按小写n键查找下一个，  
//大写n查找上一个  
:数字	//跳转到指定行

字符串替换：将countq替换为count2  
:s/count1/count2			//首先会替换光标所在行的第一个适配字符串  
:s/count1/count2/g			//替换光标所在行所有适配字符串  
:%s/count1/count2/g		//替换所有适配字符串