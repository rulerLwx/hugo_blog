---
title: "Docker-4-我的dockerfile"
date: 2017-08-01T10:03:04+08:00
draft: true
tags: [ ""]
categories: ["Linux"]
---

# docker_centos7安装jdk

构建镜像时，`ADD`命令中的主机文件必须跟dockerfile文件在同一个目录！

https://www.cnblogs.com/sparkdev/p/9573248.html

单单安装jdk

文件目录：
```
[test@localhost temp_software]$ ll
-rw-r--r--. 1 root  root        162 6月  23 22:55 centos_jdk
-rw-r--r--. 1 root  root  195132576 6月  23 21:27 jdk-8u251-linux-x64.tar.gz
```

dockerfile
```
FROM centos:7
MAINTAINER xxx.com
WORKDIR /usr
RUN mkdir jdk
ADD jdk-8u251-linux-x64.tar.gz /usr/jdk/
ENV JAVA_HOME=/usr/jdk/jdk1.8.0_251
ENV PATH=/sbin:$JAVA_HOME/bin:$PATH

CMD /bin/bash
```

构建：`# docker build -f centos_jdk -t centos_jdk .`

启动：`# docker run --name centos_jdk -it centos_jdk`


# 发布web服务

基于上面已经安装了jdk/tomcat的centos，发布web服务

思想是：创建容器时，使用-v参数进行映射

## dockerfile

```
FROM         centos
MAINTAINER    bs@126.com

#设置工作访问时候的WORKDIR路径，登录落脚点
ENV MYPATH /usr/local
WORKDIR $MYPATH

#把java与tomcat添加到容器中
ADD jdk-8u251-linux-x64.tar.gz /usr/local
ADD apache-tomcat-9.0.17.tar.gz /usr/local

#安装vim编辑器
RUN yum -y install vim

#配置java与tomcat环境变量
ENV JAVA_HOME /usr/local/jdk1.8.0_251
ENV CLASSPATH $JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
ENV CATALINA_HOME /usr/local/apache-tomcat-9.0.17
ENV CATALINA_BASE /usr/local/apache-tomcat-9.0.17
ENV PATH $PATH:$JAVA_HOME/bin:$CATALINA_HOME/lib:$CATALINA_HOME/bin

#容器运行时监听的端口
EXPOSE  8080

#启动时运行tomcat
# ENTRYPOINT ["/usr/local/apache-tomcat-9.0.17/bin/startup.sh" ]
CMD ["/usr/local/apache-tomcat-9.0.17/bin/catalina.sh","run"]
#CMD /usr/local/apache-tomcat-9.0.17/bin/startup.sh && tail -F /usr/local/apache-tomcat-9.0.17/bin/logs/catalina.out
```

## 创建容器

```
docker run --name centos_jdk_tomcat_war -p 8080:8080 -d -v /home/test/webjar/DockerTest:/usr/local/apache-tomcat-9.0.17/webapps/DockerTest --privileged=true centos_jdk_tomcat
```

关键点：
- 使用`-v`进行目录映射
- `--privileged=true`使用此参数才能访问到页面，参数不要放在最后，会当成CMD命令从而覆盖掉其它CMD命令

注意：

`/home/test/webjar/DockerTest:/usr/local/apache-tomcat-9.0.17/webapps/DockerTest`，其中`/home/test/webjar/DockerTest`中的`DockerTest`是一个从项目DockerTest.war解压后的目录，不是文件

访问镜像中的tomcat：http://192.168.12.132:8080/DockerTest/

图示：
![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708164312.png)