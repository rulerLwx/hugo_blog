---
title: "Nginx-4-FAQ"
date: 2017-08-01T10:02:09+08:00
draft: true
tags: [ ""]
categories: ["Linux"]
---

# FAQ


## alias与root的区别

解释一：

```
# 如果一个请求的URI是/t/a.html时，web服务器将会返回服务器上的/www/root/html/t/a.html的文件。
location ^~ /t/ {
	root /www/root/html/;
}

# 如果一个请求的URI是/t/a.html时，web服务器将会返回服务器上的/www/root/html/new_t/a.html的文件。
# 注意这里是new_t，因为alias会把location后面配置的路径丢弃掉，把当前匹配到的目录指向到指定的目录。
location ^~ /t/ {
	alias /www/root/html/new_t/;
}

```

参考：https://blog.csdn.net/asahinokawa/article/details/87702491

解释二：

与root一样，alias也是配置资源文件路径的，但是alias是location后的路径以别名的方式替换目标路径的指定部分，比如如下配置：

```
location /conf {
  alias /usr/local/nginx/conf;
}
```

此时如果一个请求为/conf/index.html，那么其前缀/conf将会与当前location匹配，并且会将alias参数替换请求uri中匹配的部分，也就是转换后的uri为/usr/local/nginx/conf/index.html

参考：https://my.oschina.net/zhangxufeng/blog/3120456#h4_15

总结：
- root是基于一个根目录，进行接连，找到对应的文件
- alias不是，是替换 ——20191120



## nginx程序的热更新是如何做的？

执行 重载配置文件：nginx -s reload

具体细节 todo...


## 隐藏nginx版本号？

在http块中加入`server_tokens  off;`

- 修改版本号

    在安装nginx之前，修改源码src/core/nginx.h：
```
#define nginx_version         8053
#define NGINX_VERSION      "10.0"
#define NGINX_VER          "jh/" NGINX_VERSION
```

参考：[修改或隐藏Nginx的版本号][2]

- 如何平滑升/降级？

    todo....

- nginx的变量问题"$1"是如何定义的？

$1表示前面（）之内的内容



## nginx语法高亮

参考博客：https://blog.csdn.net/qq_31725371/article/details/84348817

自己去找 nginx.vim ：https://www.vim.org/scripts/script.php?script_id=1886

```shell
# cd 
# mkdir -p .vim/syntax
# cd .vim/syntax/
# wget https://www.vim.org/scripts/download_script.php?src_id=19394 -O nginx.vim
# cd ..
# vim filetype.vim
```

添加以下内容

```
au BufRead,BufNewFile /etc/nginx/*,/usr/local/nginx/conf/* if &ft == '' | setfiletype nginx | endif
```

以上是在root用户根目录下创建 .vim/syntax，只对root用户有效。其它用户可以在根目录下同样这样配置即可。



## ip_hash

nginx中的的ip_hash机制：https://blog.csdn.net/weixin_42075590/article/details/80631439



## nginx中 $1,$2,$3是什么?

答：表示小括号中匹配的内容，一个小括号中的内容是一个$，依次为$1、$2、$3...

参考：https://www.bilibili.com/video/av55251610?p=55

官方示例：

```
server {
    ...
    rewrite ^(/download/.*)/media/(.*)\..*$ $1/mp3/$2.mp3 last;
    rewrite ^(/download/.*)/audio/(.*)\..*$ $1/mp3/$2.ra  last;
    return  403;
    ...
}
```

http://nginx.org/en/docs/http/ngx_http_rewrite_module.html#rewrite


---

Nginx中，set $para $1，$1表示路径中正则表达式匹配的第一个参数。https://blog.csdn.net/cbmljs/article/details/86573248

nginx正则相关变量$1,$2,$3使用注意：https://blog.csdn.net/weixin_34162695/article/details/87639639