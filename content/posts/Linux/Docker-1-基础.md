---
title: "Docker-1-基础"
date: 2017-08-01T10:02:01+08:00
draft: true
tags: [ ""]
categories: ["Linux"]
---


# 什么是Docker？

官网：https://www.docker.com/

开源：https://github.com/docker

Docker是一个开源的应用容器引擎，支持将软件编译成一个镜像，然后在镜像中各种做好配置，将镜像发布出去，其它使用者直接使用这个镜像。容器启动是非常讯速的。类似windows里面的ghost操作系统，安装好后什么都有了。

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708161745.png)

## Docker核心概念

Docker核心概念：

- docker主机（Host）：一个物理或虚拟的机器用于执行Docker守护进程和容器（安装了Docker程序的机器，可以是Linux或windows）
- docker客户端（Client）：客户端通过命令或其它工具使用Docker API（）与Docker的守护进程通信
- docker仓库（Registry）：用来保存打包好的镜像，可以理解为代码控制中的代码仓库。Docker Hub（https://hub.docker.com）提供了庞大的镜像集合供使用。
- docker镜像（Images）：用于创建Docker容器的模板
- docker容器（Container）：镜像启动后的实例称为一个容器；容器是一个独立运行的一个或一组应用

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708161843.png)

Docker使用步骤：

1）安装Docker

2）去Docker仓库找到这个软件对应的镜像

3）使用Docker运行这个镜像，这个镜像就会生成一个Docker容器

4）对容器的启动停止就是对软件的启动停止

## 安装Docker（Linux）

1）环境：CentOS7、

Docker要求CentOS内核版本高于3.10，如果版本过低，可以用`yum update`升级内核版本

2）安装

```shell
yum update #更新内核到3.10以上

yum install docker

systemctl start docker #启动docker

systemctl enable/disable docker #设为/关闭开机启动

systemctl stop docker #停止docker
```

启动失败

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708161941.png)

解决办法：`vim /etc/sysconfig/docker`，设置`--selinux-enabled=false`

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708161957.png)

参考了：https://blog.csdn.net/kavito/article/details/81200665

总结：将centos7的内核升级至最新后再安装docker可以减少很多错误，另外还需要禁用sell linux。

```shell
[root@eureka1 test]# systemctl enable docker
Created symlink from /etc/systemd/system/multi-user.target.wants/docker.service to /usr/lib/systemd/system/docker.service.

```
peter老师：3423941094

## 安装Docker（Windows）

```
Docker Desktop requires Windows 10 Pro or Enterprise version 14393 to run.
```


# 常用操作

## 一、镜像操作

操作 | 命令 | 说明
---|---|---
检索 | docker search xxx，如：docker search redis | 检索镜像，作用不大，还不如到官网上检索 https://hub.docker.com/
拉取 | docker pull 镜像名:tag | :tag是可选的，tag表示标签，多为软件版本，默认为latest
列表 | docker images | 查看所有本地镜像
删除 | docker rmi image-id  | 删除指定的本地镜像
清理镜像 | docker image prune | 清除一些遗留的临时镜像，释放空间
- | docker commit  -m="提交的描述信息" -a="作者" 容器ID 要创建的目标镜像名:[TAG] | 提交容器副本使之成为一个新的镜像

怎么知道有哪些可用的tag?官网上搜索会提示有哪些可用的tags https://hub.docker.com/

---

解决镜像下载慢的问题：

1）登录阿里云 https://www.aliyun.com/ ，查看镜像加速地址：https://chohcg90.mirror.aliyuncs.com

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708162036.png)

2）`vim /etc/docker/daemon.json`，修改如下：

```
{
"registry-mirrors": ["https://chohcg90.mirror.aliyuncs.com"]
}
```

参考：https://www.cnblogs.com/kevin7234/p/10617734.html

---

将镜像提交到阿里云

1）登录阿里云，创建镜像仓库

2）本地上传镜像到刚刚创建的镜像仓库

创建镜像仓库后，上面有文档参考



## 二、容器操作

软件镜像（QQ安装程序）---》运行镜像---》产生一个容器（正在运行的QQ）

操作 | 命令 | 说明
---|---|---
运行 | docker run --name container-name -d image-name <br> 如：docker run -name myredis -d redis | --name：自定义容器名 <br> -d：后台运行 <br> image-name：指定镜像模板
列表 | docker ps（查看运行中的容器） | docker ps -a 可以查看所有容器
停止 | docker stop/kill container-name/container-id | 停止当前运行的容器
启动 | docker start/restart container-name/container-id | 启动容器
删除 | docker rm container-id <br> docker rm -f $(docker ps -q) 删除全部在运行的容器 <br> docker rm -f $(docker ps -a -q) 删除所有容器 | 删除指定的容器
端口映射 | -p 6379:6379 <br> 如：docker run -d -p 6379:6379 --name myredis redis | -p：主机端口（映射到）容器内部的端口
容器日志 | docker logs container-name/container-id [OPTIONS] | -t 加入时间戳 <br> -f 跟随最新的日志打印 <br> --tail x(x是数字) 显示最后多少行
查看容器内运行的进程 | docker top 容器id | 
查看容器内部细节 | docker inspect 容器id | 查看容器的内部细节，包括IP、网桥
进入正在运行的容器 <br> 并以命令行交互 | docker exec -it 容器ID bashShell <br> `docker container exec -it nginx1 /bin/bash` <br> docker attach 容器ID | 区别：<br> attach 直接以命令交互方式进入容器 <br> exec 不用进入容器却能执行容器的命令
从容器拷贝文件到主机上 | docker cp 容器ID:容器内路径 目标主机路径 |
更多命令 | 

---

容器启动命令 docker run，格式：docker run [OPTIONS] IMAGE [COMMAND] [ARG...]

OPTIONS，有些是一个减号，有些是两个减号

- --name = "给容器起个名字"
- -d 后台运行，并返回容器ID
- -i 以交互模式运行容器，通常与 -t 同时使用
- -t 为容器重新分配一个伪输入终端，通常与 -i 同时使用
- -P 随机端口映射
- -p 指定端口映射

    - ip:hostPort:containerPort
    - ip::containerPort
    - hostPort:containerPort，这个常用
    - containerPort

例如在docker上启动ctntos

```shell
docker run -it 9f38484d220f
```

退出centos

- exit，完全退出并关闭容器
- 快捷键：ctrl + P + Q，暂时退出但不关闭容器

重新进入centos

- 方式一：

```shell
docker exec -t 容器ID /bin/bash
```

- 方式二：

```shell
docker attach 容器ID
```

`docker exec -t 容器ID ls -l /tmp`可以直接执行容器中的命令而不用进入容器中的命令行界面，“隔山打牛”。

---

docker ps [OPTIONS]

- -a 所有历史记录的容器
- -l 上一次打开的容器
- -n x(x是数字)，最近打开过的x个容器
- -q 查出正在运行的容器ID，批量删除容器：docker rm -f $(docker ps -q) ——20190704


## 文件拷贝

- 从宿主机拷贝文件到容器中：docker cp 需要拷贝的文件或目录 容器名称:容器目录
- 从容器拷贝文件到宿主机中：docker cp 容器名称:容器目录 宿主机目录

## 目录挂载

-v 参数

目录挂载是为了方便在宿主机编辑文件，然后会同步到容器上，程序运行，最终是按容器中的目录 ——20191031


# 镜像原理

这是金鱼背上的一个集装箱

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708162120.png)

相关概念：

- UnionFS（联合文件系统）
- bootfs，
- rootfs，在bootfs之上，包含的就是典型的Linux中的/dev,/proc,/bin,/etc等标准目录和文件，rootfs就是各种不同的操作系统发行版本，如ubuntu、centos。

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708162143.png)


# 数据卷

数据卷：对容器数据的持久化，持久化到主机上，容器与主机之间的数据共享。

## -v 参数

命令：

```
docker run -it -v /宿主机绝对路径目录:/容器内目录 镜像名或id
```

命令（带权限）
```
docker run -it -v /宿主机绝对路径目录:/容器内目录:ro 镜像名或id
```

- 默认是可读可写
- :ro 只读，容器只能读，主机可读可写


总结：`-v`除了目录映射外，还可以文件映射，即：-v /宿主机绝对路径文件:/容器内的文件，如nginx.conf ——20191121



## DockerFile

1）在主机根目录下新建mydocker目录并进入

```shell
# cd /
# mkdir /mydocker
# cd /mydocker/
```

2）在Dockerfile中使用 VOLUME 指令来给镜像添加一个或多个数据卷

```shell
# vim DockerFile
```

内容如下：

```shell
FROM centos
VOLUME ["/dataVolumeContainer1","/dataVolumeContainer2"]
CMD echo "finished,-----------success1"
CMD /bin/bash
```

3）build生成新的镜像

```shell
# docker build -f /mydocker/DockerFile -t zzyy/centos .
```

4）运行刚刚生成的容器容器

```shell
# docker run -it zzyy/centos
```

在根目录下可以看到多个两个卷

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708162206.png)

5）备注

如果出现容器卷不能写，出现cannot open directory:Permission denied

解决办法：在挂载目录后多加一个 --privileged=true 参数


# 容器数据卷

## 是什么

命名的容器挂载数据卷，其它容器通过容器通过挂载这个（父容器）实现数据共享，挂载数据卷的容器称之为数据卷容器。

一句话：活动硬盘上挂活动硬盘，实现数据的传递依赖。

## 数据共享

上面已经创建了一个 zzyy/centos 镜像，并且有了两个数据卷 dataVolumeContainer1、dataVolumeContainer2

```shell
[root@localhost ~]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
zzyy/centos         latest              e8c61129663a        9 hours ago         202 MB
...

[root@localhost ~]# docker run -it --name dc01 zzyy/centos
[root@3da5bea8d5d8 /]# ll
total 12
-rw-r--r--.   1 root root 12082 Mar  5 17:36 anaconda-post.log
lrwxrwxrwx.   1 root root     7 Mar  5 17:34 bin -> usr/bin
drwxr-xr-x.   2 root root     6 Jul  4 16:19 dataVolumeContainer1
drwxr-xr-x.   2 root root     6 Jul  4 16:19 dataVolumeContainer2
drwxr-xr-x.   5 root root   360 Jul  4 16:19 dev
drwxr-xr-x.   1 root root    66 Jul  4 16:19 etc
...
[root@3da5bea8d5d8 /]# cd dataVolumeContainer2/                     // 在这个容器的这个目录，创建一个文件
[root@3da5bea8d5d8 dataVolumeContainer2]# touch dc01_add.txt

ctrl + p + q

[root@localhost ~]# docker run -it --name dc02 --volumes-from dc01 zzyy/centos
[root@61be3fe9262b /]# ll
total 12
-rw-r--r--.   1 root root 12082 Mar  5 17:36 anaconda-post.log
lrwxrwxrwx.   1 root root     7 Mar  5 17:34 bin -> usr/bin
drwxr-xr-x.   2 root root     6 Jul  4 16:19 dataVolumeContainer1
drwxr-xr-x.   2 root root    26 Jul  4 16:20 dataVolumeContainer2
drwxr-xr-x.   5 root root   360 Jul  4 16:28 dev
...
[root@61be3fe9262b /]# cd dataVolumeContainer2
[root@61be3fe9262b dataVolumeContainer2]# ll
total 0
-rw-r--r--. 1 root root 0 Jul  4 16:20 dc01_add.txt                 // 这个文件是从 dc01容器继承过来的
[root@61be3fe9262b dataVolumeContainer2]# 
[root@61be3fe9262b dataVolumeContainer2]# touch dc02_add.txt

ctrl + p + q

[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
61be3fe9262b        zzyy/centos         "/bin/sh -c /bin/bash"   11 minutes ago      Up 11 minutes                           dc02
3da5bea8d5d8        zzyy/centos         "/bin/sh -c /bin/bash"   19 minutes ago      Up 19 minutes                           dc01

[root@localhost ~]# docker run -it --name dc03 --volumes-from dc01 zzyy/centos
[root@d541e9e3b6a2 /]# cd dataVolumeContainer2
[root@d541e9e3b6a2 dataVolumeContainer2]# ll
total 0
-rw-r--r--. 1 root root 0 Jul  4 16:20 dc01_add.txt
-rw-r--r--. 1 root root 0 Jul  4 16:38 dc02_add.txt                 // 也有前两个容器的文件
[root@d541e9e3b6a2 dataVolumeContainer2]# 
[root@d541e9e3b6a2 dataVolumeContainer2]# touch dc03_add.txt

ctrl + p + q

[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
d541e9e3b6a2        zzyy/centos         "/bin/sh -c /bin/bash"   5 minutes ago       Up 5 minutes                            dc03
61be3fe9262b        zzyy/centos         "/bin/sh -c /bin/bash"   19 minutes ago      Up 19 minutes                           dc02
3da5bea8d5d8        zzyy/centos         "/bin/sh -c /bin/bash"   28 minutes ago      Up 28 minutes                           dc01
[root@localhost ~]# docker attach dc01
[root@3da5bea8d5d8 dataVolumeContainer2]# ll
total 0
-rw-r--r--. 1 root root 0 Jul  4 16:20 dc01_add.txt
-rw-r--r--. 1 root root 0 Jul  4 16:38 dc02_add.txt
-rw-r--r--. 1 root root 0 Jul  4 16:47 dc03_add.txt                 // dc01容器有子容器添加的文件
[root@3da5bea8d5d8 dataVolumeContainer2]# 

```

整理：dc02、dc03的父类都是dc01，子到父，父到子都可以传递文件。

如果此时将父容器dc01删掉，dc02和dc03之间数据还是共享的，也就是文件是共享的，数据卷的生命周期一直持续到没有容器使用它为止。


# DockerFile

## 是什么

Dockerfile是用来构建Docker镜像的构建文件，是由一系列命令、参数构成的脚本。

三个步骤

1）编写DockerFile文件

2）docker build

3）docker run

文件长什么样子的？以centos6.8为例

```dockerfile
FROM scratch
MAINTAINER The CentOS Project <cloud-ops@centos.org>
ADD c68-docker.tar.xz /
LABEL name="CentOS Base Image" \
    vendor="CentOS" \
    license="GPLv2" \
    build-date="2016-06-02"

# Default command
CMD ["/bin/bash"]
```

## 构建过程解析

DockerFile内容基础知识：

- 每条保留字指令都必须为大写字母且后面跟随至少一个参数
- 指令按照从上到下顺序执行
- #表示注解
- 每条指令都会创建一个新的镜像层，并对镜像进行提交

执行大致流程

1. 从基础镜像运行一个容器
2. 执行一条指令并对容器作出修改
3. 执行 docker commit 提交一个新的镜像层
4. 基于刚刚提交的镜像运行一个新的容器
5. 执行下一条指令直至所有指令都执行完成
 
![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708162249.png)

## 体系结构（保留字指令）

- FROM
- MAINTAINER
- RUN
- EXPOSE
- WORKDIR
- ENV
- ADD
- COPY
- VOLUME
- CMD
- ENTRYPOINT
- ONBUILD

解释

- FROM，基础镜像，当前镜像是基于哪个镜像的
- MAINTAINER，镜像维护者的姓名、邮箱
- RUN，容器构建时需要执行的命令
- EXPOSE，当前容器对外暴露的端口
- WORKDIR，指定在容器创建后，终端默认登录进来的工作目录，落脚点
- ENV，用来在构建镜像过程中设置环境变量，定义变量后，后面的指令就可以使用$来获取定义的变量

```dockerfile
ENV MY_PATH /usr/mytest
...
WORKDIR $MY_PATH
```

- ADD，将宿主机目录下的文件拷贝进镜像且ADD命令会自动处理URL和解压tar压缩包
- COPY，类似ADD，只是拷贝，从<源路径>拷贝到<目标路径>
    - COPY src dest
    - COPY ["src","dest"]
- VOLUME，容器数据卷，用于数据保存、待久化，VOLUME ["/dataVolumeContainer1","/dataVolumeContainer2"]
- CMD，指定一个容器启动时运行的命令，可以有多个CMD命令，但只有最后一个生效，CMD会被 docker run 之后的参数替换
- ENTRYPOINT，指定一个容器启动时运行的命令，跟CMD一样，都是在指定容器启动程序及参数，区别是：CMD命令可能被替换，而ENTRYPOINT可以追加命令
- ONBUILD，在构建一个被继承的DockerFile时运行命令，父镜像在被子继承后父镜像的onbuild被触发


## 案例

### 自定义mycentos

让精简版的centos具有以下功能：

1. 登录后的默认路径
2. vim编辑器
3. 支持ifconfig查看网络

步骤：

1）在宿主机根目录下的mydocker目录下创建一个DockerFile

```shell
cd /
cd /mydocker
vim DockerFile2
```

内容如下：

```dockerfile
FROM centos
MAINTAINER zzyy<zzyy167@126.com>

ENV MYPATH /usr/local
WORKDIR $MYPATH

RUN yum -y install vim
RUN yum -y install net-tools

EXPOSE 80

CMD echo $MYPATH
CMD echo "success......ok"
CMD /bin/bash
```

2）构建

```shell
docker build -f /mydocker/DockerFile2 -t mycentos:1.3 .
```

注意：最后有个点`.`代表当前路径

3）运行

```shell
docker run -it mycentos:1.3
```

默认进入 /usr/local 目录，也能使用 vim/ifconfig 命令了

4）列出镜像变更历史

```shell
docker history 镜像ID

docker history 8911244c3084
```


### CMD/ENTRYPOINT命令案例

cmd/entrypoint都是指定一个容器启动时运行的命令

区别：
- CMD，指定一个容器启动时运行的命令，可以有多个CMD命令，但只有最后一个生效，CMD会被 docker run 之后的参数替换
- ENTRYPOINT，指定一个容器启动时运行的命令，跟CMD一样，都是在指定容器启动程序及参数，区别是：CMD命令可能被替换，而ENTRYPOINT可以追加命令


### 自定义的tomcat9

dockerfile

```shell
FROM         centos
MAINTAINER    zzyy<zzyybs@126.com>
#把宿主机当前上下文的c.txt拷贝到容器/usr/local/路径下
COPY c.txt /usr/local/cincontainer.txt
#把java与tomcat添加到容器中
ADD jdk-8u171-linux-x64.tar.gz /usr/local/
ADD apache-tomcat-9.0.8.tar.gz /usr/local/
#安装vim编辑器
RUN yum -y install vim
#设置工作访问时候的WORKDIR路径，登录落脚点
ENV MYPATH /usr/local
WORKDIR $MYPATH
#配置java与tomcat环境变量
ENV JAVA_HOME /usr/local/jdk1.8.0_171
ENV CLASSPATH $JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
ENV CATALINA_HOME /usr/local/apache-tomcat-9.0.8
ENV CATALINA_BASE /usr/local/apache-tomcat-9.0.8
ENV PATH $PATH:$JAVA_HOME/bin:$CATALINA_HOME/lib:$CATALINA_HOME/bin
#容器运行时监听的端口
EXPOSE  8080
#启动时运行tomcat
# ENTRYPOINT ["/usr/local/apache-tomcat-9.0.8/bin/startup.sh" ]
# CMD ["/usr/local/apache-tomcat-9.0.8/bin/catalina.sh","run"]
CMD /usr/local/apache-tomcat-9.0.8/bin/startup.sh && tail -F /usr/local/apache-tomcat-9.0.8/bin/logs/catalina.out
```

启动命令：`# docker run --name centos_jdk_tomcat -p 8080:8080 -d  centos_jdk_tomcat`

注意：
- 此处需以压缩包方式上传，`ADD jdk-8u171-linux-x64.tar.gz /usr/local/`，否则出现启动文件找不到问题
- 如果文件夹方式，`ADD jdk1.8.0_251 /usr/local/`，则其复制是`jdk1.8.0_251文件夹下`的文件，而不是`jdk1.8.0_251及其目录下`的文件


# FAQ

## Docker容器开机自动启动

在使用docker run启动容器时，使用--restart参数来设置：

```shell
# docker run -m 512m --memory-swap 1G -it -p 58080:8080 --restart=always --name bvrfis --volumes-from logdata mytomcat:4.0 /root/run.sh 
```

--restart具体参数值详细信息：

- no -  容器退出时，不重启容器；
- on-failure - 只有在非0状态退出时才从新启动容器；
- always - 无论退出状态是如何，都重启容器；

参考：https://blog.csdn.net/menghuanbeike/article/details/79261828

# 附件

## Docker相关书

https://blog.csdn.net/qq_40083727/article/details/88676382

https://blog.csdn.net/icannotdebug/article/details/82352825