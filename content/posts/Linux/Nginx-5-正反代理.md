---
title: "Nginx-5-正反代理"
date: 2017-08-01T10:02:10+08:00
draft: true
tags: [ ""]
categories: ["Linux"]
---


# 代理

## 正反代理

- 正向代理
- 反向代理

正向代理

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708175025.png)

反向代理

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708175105.png)

代理的区别在于代理的对象不一样：

- 正向代理代理的对象是客户端
- 反向代理代理的对象是服务端

比如，翻墙软件是正向代理

## 语法

官方文档：

- http://nginx.org/en/docs/http/ngx_http_proxy_module.html#proxy_pass
- http://nginx.org/en/docs/http/ngx_http_upstream_module.html

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708175124.png)

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708175143.png)

https://www.xuliangwei.com/

