---
title: "Docker-3-常用软件安装"
date: 2017-08-01T10:03:02+08:00
draft: true
tags: [ ""]
categories: ["Linux"]
---


# Redis

1）拉取镜像

```shell
docker pull redis:5
```

2）启动容器

最简单的启动

```shell
docker run --name redis01 -p 6379:6379 -d redis:5 redis-server
```

复杂的启动：自定义配置文件

使用 `-v` 设置文件存储位置、redis配置文件位置，注意：此处映射的都是目录，不是文件（redis.conf是目录，不是文件）

事先创建`/lwx/redis/redis.conf`文件，再映射，就映射的是文件，而不是目录 ——20191122

```shell
docker run -p 6379:6379 --name redis01 -v /lwx/redis/data:/data  -v /lwx/redis/conf/redis.conf:/usr/local/etc/redis/redis.conf -d redis:5 redis-server /usr/local/etc/redis/redis.conf --appendonly yes
```

参考官网：https://hub.docker.com/_/redis

> 总结：docker 上的 redis 默认没有配置文件（也找不到），需要我们映射后添加自己的配置文件
>
> 参考：https://cloud.tencent.com/developer/news/440264


3）在宿主机的 /zzyyuse/myredis/conf/redis.conf 目录下新建 redis.conf配置文件

```shell
vim /zzyyuse/myredis/conf/redis.conf/redis.conf
```

配置的内容可以从其它已安装的redis机器中拷贝过来，做适当的修改，不绑定IP等等


4）运行redis客户端

```shell
docker exec -it 容器id redis-cli
```



# MySQL

1）拉取镜像

```shell
# docker pull mysql:5.5
```

2）启动MySQL

需指定密码、端口映射

简单启动

```shell
docker run --name mysql01 -e MYSQL_ROOT_PASSWORD=123456 -d -p 3306:3306 mysql:5.5
```

复杂启动

```shell
docker run -p 3306:3306 --name mysql -v /zzyyuse/mysql/conf:/etc/mysql/conf.d -v /zzyyuse/mysql/logs:/logs -v /zzyyuse/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql:5.5
```

3）配置文件

在相应的目录（/zzyyuse/mysql/conf）添加配置文件

4）连接mysql

```shell
docker exec -it MySQL容器ID /bin/bash

[root@localhost mydocker]# docker exec -it 0d702f321502  /bin/bash
root@0d702f321502:/# mysql -uroot -p
```

5）外部连接上mysql

使用SQLyog/Workbench等连接上mysql

当然了，防火墙要关掉或开放3306端口

Docker mysql 把数据存储在本地目录：https://blog.csdn.net/hxpjava1/article/details/78271794

更高级的设置：https://hub.docker.com/_/mysql



# oracle-11g

安装 xe 版本的oracle

官方文档：https://hub.docker.com/r/oracleinanutshell/oracle-xe-11g

1）拉取镜像

```shell
docker pull oracleinanutshell/oracle-xe-11g
```

文件比较大，2.13 GB

2）运行镜像

```shell
docker run -d -p 49161:1521 -p 8080:8080 -e ORACLE_ALLOW_REMOTE=true oracleinanutshell/oracle-xe-11g
```

映射一个目录，用来放表空间？？

```

```


3）本地（宿主机）连接

```shell
[root@localhost mydocker]# docker exec -it b0ff72d71b17 bash
root@b0ff72d71b17:/# sqlplus

SQL*Plus: Release 11.2.0.2.0 Production on Fri Jul 5 15:52:42 2019

Copyright (c) 1982, 2011, Oracle.  All rights reserved.

Enter user-name: system
Enter password: 

Connected to:
Oracle Database 11g Express Edition Release 11.2.0.2.0 - 64bit Production

SQL> 
```

4）运程连接

在pl/sql等工具，使用以下信息连接

```tex
hostname: localhost
port: 49161
sid: xe
username: system
password: oracle
```

如

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708163945.png)

或者

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708163956.png)

以前企业版的连接是`192.168.12.132:1521/orcl`，而这里不写或用`/xe` ——自己的实践20190705

另外可以参考：

https://www.cnblogs.com/caiyt/p/9962559.html


5）其它

企业版本的安装参考：

https://hub.docker.com/r/jaspeen/oracle-11g

https://github.com/jaspeen/oracle-11g

6）示例——导入德生的 sisp 和 sisp_public

oracle xe 默认的表空间位置：/u01/app/oracle/oradata/XE/users.dbf，可以执行`SELECT * FROM Dba_Data_Files;`来查看表空间默认位置

1）创建表空间

pl/sql 连接上容器中的数据库后，可以在 pl/sql 中直接执行命令

修改路径为默认位置 

```sql
CREATE TABLESPACE SISP_TBS
         DATAFILE  '/u01/app/oracle/oradata/XE/SISP_TBS.DBF'
         SIZE 1024M
         AUTOEXTEND ON
         NEXT 32M MAXSIZE UNLIMITED
         EXTENT MANAGEMENT LOCAL;

CREATE TEMPORARY TABLESPACE SISP_TMP_TBS
         TEMPFILE '/opt/sisp_data/SISP_TMP_TBS.DBF'
         SIZE 1024M
         AUTOEXTEND ON
         NEXT 32M MAXSIZE UNLIMITED
         EXTENT MANAGEMENT LOCAL;

CREATE TABLESPACE SISP_PUBLIC_TBS
         DATAFILE  '/opt/sisp_data/SISP_PUBLIC_TBS.DBF'
         SIZE 1024M
         AUTOEXTEND ON
         NEXT 32M MAXSIZE UNLIMITED
         EXTENT MANAGEMENT LOCAL;

CREATE TEMPORARY TABLESPACE SISP_PUBLIC_TMP_TBS
         TEMPFILE '/opt/sisp_data/SISP_PUBLIC_TMP_TBS.DBF'
         SIZE 1024M
         AUTOEXTEND ON
         NEXT 32M MAXSIZE UNLIMITED
         EXTENT MANAGEMENT LOCAL;
```


2）创建用户

```sql
CREATE USER  sisp   IDENTIFIED BY  sisp
 DEFAULT TABLESPACE SISP_TBS
 TEMPORARY TABLESPACE SISP_TMP_TBS
 PROFILE DEFAULT
 QUOTA  UNLIMITED ON SISP_TBS;
 
GRANT  CONNECT         TO  sisp   WITH ADMIN OPTION;
GRANT  resource        TO  sisp  ; 
GRANT  CREATE SESSION  TO  sisp ;

grant  debug connect session to  sisp ;
 
GRANT CREATE DATABASE LINK TO sisp ;
GRANT CREATE PUBLIC DATABASE LINK TO  sisp ;

grant drop PUBLIC DATABASE LINK TO  sisp;
grant create SYNONYM  to sisp;
grant create view to  sisp;
grant create materialized view to sisp;
grant create job to sisp;
grant create trigger to sisp;
grant create any directory to sisp;
grant create sequence to sisp;
grant dba to sisp;

ALTER USER sisp  TEMPORARY TABLESPACE SISP_TMP_TBS;
ALTER USER sisp  QUOTA UNLIMITED ON   SISP_TBS;

-- drop user &user_name cascade;
----------------------SISP_PUBLIC----------------------------------------------

CREATE USER  sisp_public   IDENTIFIED BY  sisp_public
 DEFAULT TABLESPACE SISP_PUBLIC_TBS
 TEMPORARY TABLESPACE SISP_PUBLIC_TMP_TBS
 PROFILE DEFAULT
 QUOTA  UNLIMITED ON SISP_PUBLIC_TBS;
 
GRANT  CONNECT         TO  sisp_public   WITH ADMIN OPTION;
GRANT  resource        TO  sisp_public  ; 
GRANT  CREATE SESSION  TO  sisp_public ;

grant  debug connect session to  sisp_public ;
 
GRANT CREATE DATABASE LINK TO sisp_public ;
GRANT CREATE PUBLIC DATABASE LINK TO  sisp_public ;

grant drop PUBLIC DATABASE LINK TO  sisp_public;
grant create SYNONYM  to sisp_public;
grant create view to  sisp_public;
grant create materialized view to sisp_public;
grant create job to sisp_public;
grant create trigger to sisp_public;
grant create any directory to sisp_public;
grant create sequence to sisp_public;
grant dba to sisp_public;

ALTER USER sisp_public  TEMPORARY TABLESPACE SISP_TMP_TBS;
ALTER USER sisp_public  QUOTA UNLIMITED ON   SISP_TBS;

-- drop user &sisp_public cascade
```

3）导入 dmp

在 docker 上启动容器，将 dmp 上传到容器目录中，这里是 /opt 

执行命令：

```shell
root@ce1231e36282:/opt# imp sisp/sisp@xe file=./sisp_71_20150820.dmp full=y
root@ce1231e36282:/opt# imp sisp_public/sisp_public@xe file=./sisp_public_71_20150820.dmp full=y
```



# Nginx

```shell
docker run --name nginx01 -p 80:80 -v /some/content:/usr/share/nginx/html:ro -d nginx:stable
```

指定配置文件目录、静态文件目录、日志目录的安装，TODO...

思路：先安装一个简易的、默认的nginx容器，目的是从这个容器中拿到nginx的默认配置

步骤一：复制nginx.conf，复制到事先创建好的目录
```
$ docker run --name tmp-nginx-container -d nginx
$ docker cp tmp-nginx-container:/etc/nginx/nginx.conf /lwx/nginx/nginx.conf
$ docker rm -f tmp-nginx-container
```

参考：https://www.runoob.com/docker/docker-install-nginx.html

步骤二：创建容器，`-v /lwx/nginx/nginx.conf:/etc/nginx/nginx.conf:ro`是文件映射，不是目录

```shell
docker run --name nginx01 -p 8081:80 -v /lwx/nginx/nginx.conf:/etc/nginx/nginx.conf:ro -v /lwx/nginx/html:/usr/share/nginx/html:ro -d nginx:stable
```

要事先将`nginx.conf`配置文件复制到`/lwx/nginx/`下，这样用`-v`映射的就是文件，而不是目录 ——20191120

**总结：** 配置文件映射后，在配置文件中写的路径不是宿主机的路径，而是容器中的路径，将宿主机的路径嵌入容器中来写 ——20191031

还是复制原生的配置文件比较靠谱，不能随便拷贝其它的配置，例如拷贝下面的，将启动不想来：

```
user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    #include /etc/nginx/conf.d/*.conf;
    server {
        listen       80;
        server_name  localhost;
        #charset koi8-r;
        #access_log  logs/host.access.log  main;

        location / {
                ssi on;
        	ssi_silent_errors on;
        	ssi_types text/shtml;
            	root           /usr/share/nginx/html; #重点
            	index          index.html; #重点，可以在前面加上相对路径
        }
    }
}
```


# rabbitmq

参考：https://www.cnblogs.com/yufeng218/p/9452621.html


```shell
docker pull rabbitmq:3.7.17-management

docker run -d --name rabbitmq3.7.17 -p 5672:5672 -p 15672:15672 -v /myrabit/data:/var/lib/rabbitmq --hostname myRabbit -e RABBITMQ_DEFAULT_VHOST=my_vhost  -e RABBITMQ_DEFAULT_USER=admin -e RABBITMQ_DEFAULT_PASS=admin rabbitmq:3.7.17-management
```

关于`docker run`的说明：

- -d 后台运行容器；
- --name 指定容器名；
- -p 指定服务运行的端口（5672：应用访问端口；15672：控制台Web端口号）；
- -v 映射目录或文件；
- --hostname  主机名（RabbitMQ的一个重要注意事项是它根据所谓的 “节点名称” 存储数据，默认为主机名）；
- -e 指定环境变量；（RABBITMQ_DEFAULT_VHOST：默认虚拟机名；RABBITMQ_DEFAULT_USER：默认的用户名；RABBITMQ_DEFAULT_PASS：默认用户名的密码）

使用浏览器打开web管理端：http://Server-IP:15672



# Tomcat

```
docker run -d --name tomcat01 -p 8888:8080 tomcat:9
```



# 其它常用软件镜像

```shell
docker pull wnameless/oracle-xe-11g
docker pull mongo
docker pull redis:2.8.21
docker pull cloudesire/activemq
docker pull rabbitmq
docker pull rabbitmq:3-management
```

有些镜像可以在阿里云上搜索

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708164055.png)


# FAQ

## 已经执行了 docker run 后的容器，如何修改配置，如：添加或修改目录映射？

- 方法一：docker update --xxx 容器ID
- 方法二：直接修改容器配置文件：/var/lib/docker/containers/xxxxxxx/hostconfig.json

总结：方法一，参数有限；方法二，我觉得不好修改 ——20191031

参考：
- https://blog.csdn.net/chouzhou9701/article/details/86725203
- https://www.cnblogs.com/zhuochong/p/10070516.html

