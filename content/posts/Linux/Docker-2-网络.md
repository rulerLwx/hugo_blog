---
title: "Docker-2-网络"
date: 2017-08-01T10:03:01+08:00
draft: true
tags: [ ""]
categories: ["Linux"]
---


# 容器网络

## 如何查看容器的IP

命令：`docker inspect 容器名`

```
    ....

    "Networks": {
        "bridge": {
            "IPAMConfig": null,
            "Links": null,
            "Aliases": null,
            "NetworkID": "c167bb0d8d95a088e76ccfff410704298be4f666f7a288a14c2498bc145a1edb",
            "EndpointID": "83687cf0e4565af8c9eda1427b4c208bc09a2c9f9ae498d53ca849933ce54905",
            "Gateway": "172.17.0.1",
            "IPAddress": "172.17.0.3",
            "IPPrefixLen": 16,
            "IPv6Gateway": "",
            "GlobalIPv6Address": "",
            "GlobalIPv6PrefixLen": 0,
            "MacAddress": "02:42:ac:11:00:03"
        }
    }
    ....

```


## Docker原生网络

学习资料：https://www.bilibili.com/video/av55976953/?p=13

docker安装后默认自动创建3种网络：bridge/host/none

```shell
[root@localhost ~]# docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
58214525774f        bridge              bridge              local
dd0e78f25340        host                host                local
1534922da50b        none                null                local
```

docker 安装时会创建一个名为 docker0 的 Linux bridge，新建的容器会自动桥接到这个接口

```shell
[root@localhost ~]# ip addr show docker0
3: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:f8:e0:bb:ac brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:f8ff:fee0:bbac/64 scope link 
       valid_lft forever preferred_lft forever

```

新建的容器的IP会依次自动递增，如 172.17.0.2/16，172.17.0.3/16

1）bridge模式（默认）

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708162911.png)

2）host模式

要使用 host 网络模式，则需要在容器创建时指定：--network=host

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708162947.png)

此时在容器中使用 ifconfig 得到的网络跟宿主机的一样

3）none模式

关键点：禁用网络

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708163008.png)

## 自定义网络

docker 提供了3种自定义网络驱动：

- bridge
- overlay
- macvlan

此处的 bridge，类似于默认的bridge网络模式，但增加了一些新的功能；overlay、macvlan用于创建主机网络

建议使用自定义的网络来控制哪些容器可以相互通信，还可以DNS解析容器名称到IP地址

如何创建自定义网络？

1）创建自定义网桥

```shell
[root@localhost ~]# docker network create -d bridge my_net1
e3e5cd5694b2cc3b0d0345a7564b9d200300d5a8196301973d53fd38814e6a24
```

-d bridge 表示指定使用的驱动，默认是 bridge，因此可以不写

还可以自定义网段：在创建时指定参数 --subnet、--geteway

```shell
[root@localhost ~]# docker network create -d bridge --subnet 172.22.0.0/24 --gateway 172.22.0.1  my_net2
2795741963a53d10206568ca7688b1ed2d7df1873f877014c7f160b8f9791e4f
[root@localhost ~]# docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
58214525774f        bridge              bridge              local
dd0e78f25340        host                host                local
e3e5cd5694b2        my_net1             bridge              local
2795741963a5        my_net2             bridge              local
1534922da50b        none                null                local
```

2）创建一个容器，并使用上面的 my_net1 网桥

```shell
[root@localhost ~]# docker run --name vm1 -it --network my_net1 centos
```

查看网桥详细信息

```shell
[root@localhost ~]# docker network inspect my_net1
[
    {
        "Name": "my_net1",
        "Id": "e3e5cd5694b2cc3b0d0345a7564b9d200300d5a8196301973d53fd38814e6a24",
        "Created": "2019-07-12T19:27:21.239734462+08:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.18.0.0/16",
                    "Gateway": "172.18.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Containers": {
            "179545cecca5294bbd5f778ebb533d12179169eef0b041054d742122597e4018": {
                "Name": "vm1",
                "EndpointID": "e6e6b308a2720d88a287709c2801a4ff45d08ddfc0245a963b7bae1a0f2c50ff",
                "MacAddress": "02:42:ac:12:00:02",
                "IPv4Address": "172.18.0.2/16",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {}
    }
]
```

另外，对于自定义网段、网关的 my_net2，创建容器时还可使用`--ip`参数指定IP（my_net1不支持，因为不是自定义网段、网关）

```shell
[root@localhost ~]# docker run --name vm2 -it --network my_net2 --ip 172.22.0.10 centos
```

在同一网桥上（如my_net2 ）容器是互通的

桥接到不同网桥上容器，彼此是不能通信的；docker在设计上就是要隔离不同的network。如 172.22.0.10 是ping不通 172.18.0.2。

如何让不同网桥上的容器通信呢：使用`docker network connect `命令为vm1添加一块my_net2的网卡

```shell
[root@localhost ~]# docker network connect my_net2 vm1
```

这样vm1就能ping通vm2，但此时vm2是拼不能vm1的 ——20190713实践过，需要同样添加网卡才行：

```shell
[root@localhost ~]# docker network connect my_net1 vm2
```

## 容器间通信

1）容器之间通信除了使用IP通信外，还可以使用容器名称通信

- docker从1.10开始，内嵌了一个DNS server
- **dns解析功能必须在自定义网络中使用**，docker原生的3种网络不能使用
- 启动容器时使用 --name 参数指定容器名称

在vm2容器中拼vm1：

```shell
[root@7ba00cd3fe5a /]# ping vm1
PING vm1 (172.18.0.2) 56(84) bytes of data.
64 bytes from vm1.my_net1 (172.18.0.2): icmp_seq=1 ttl=64 time=0.053 ms
64 bytes from vm1.my_net1 (172.18.0.2): icmp_seq=2 ttl=64 time=0.168 ms
```

2）Joined

两台容器共享一个网络栈（类似于主机模式），两个容器可以使用Localhost快速高效地通信

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708163100.png)

3）--link

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708163112.png)

4）容器是如何访问外网的

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708163123.png)

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708163223.png)


## 跨主机容器通信——macvlan方式

跨主机网络解决方案：

- docker原生的 overlay、macvlan
- 第三方的 flannel、weave、calico

众多网络方案是如何与docker集成在一起的：

- libnetwork，docker容器网络库
- CNM(Container Network Model)这个模型对容器网络进行了抽象
 
![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708163257.png)

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708163307.png)

使用 macvlan实现两台主机之间的容器通信：

1）

先清理掉多余的网桥：

```shell
[root@localhost ~]# docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
e3ca980cb0de        bridge              bridge              local
dd0e78f25340        host                host                local
e3e5cd5694b2        my_net1             bridge              local
2795741963a5        my_net2             bridge              local
1534922da50b        none                null                local
[root@localhost ~]# docker network prune 
WARNING! This will remove all networks not used by at least one container.
Are you sure you want to continue? [y/N] y
Deleted Networks:
my_net1
my_net2

[root@localhost ~]# docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
e3ca980cb0de        bridge              bridge              local
dd0e78f25340        host                host                local
1534922da50b        none                null                local
```

准备两台主机，并各自添加一个新的网卡

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708163331.png)

2）打开混杂模式

为两台主机新添加的网卡打开混杂模式

```shell

```

3）

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708163401.png)

4）

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708163416.png)


## 网络命令-汇总

```shell
# docker network ls
# docker network rm # 删除网桥
# docker network inspect x # 查看网桥信息
# docker network prune # 删除不是docker默认创建的网桥
# docker network create -d bridge my_net1
# docker network connect my_net2 vm1
# docker network disconnect my_net2 vm1

# ip addr show docker0
# brctl show

```


## docker0

```shell
[root@localhost network-scripts]# ifconfig
docker0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 172.17.0.1  netmask 255.255.0.0  broadcast 0.0.0.0
        inet6 fe80::42:3aff:feb3:e49  prefixlen 64  scopeid 0x20<link>
        ether 02:42:3a:b3:0e:49  txqueuelen 0  (Ethernet)
        RX packets 32  bytes 2176 (2.1 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 8  bytes 656 (656.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
...
```

1）安装网桥工具

```shell
yum install bridge-utils
```

2）查看本机上的网桥

```shell
[root@localhost ~]# brctl show
bridge name	bridge id		STP enabled	interfaces
docker0		8000.02423ab30e49	no	
```

修改docker0地址

```
# ifconfig docker0 192.168.12.150 netmask 255.255.255.0
```
