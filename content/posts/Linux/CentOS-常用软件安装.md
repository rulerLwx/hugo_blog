---
title: "CentOS-常用软件安装"
date: 2017-08-01T10:01:03+08:00
draft: true
tags: [ ""]
categories: ["Linux"]
---

# Java

## 卸载open-jdk

如果系统安装了open-jdk，需先卸载。

1）查看系统是否安装了open-jdk `rpm -qa | grep java`

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708160816.png)

2）用yum方式卸载 `# yum remove java-1.8.0-openjdk-headless.x86_64`

## 安装在当前用户下

假设用户为tset

1）新建Java文件夹`/home/test/Java`

2）复制`jdk-8u201-linux-x64.tar.gz`到`/home/test/Java`

3）进入Java文件夹，解压`tar -zxvf jdk-8u201-linux-x64.tar.gz`，删除tar文件`rm jdk-8u201-linux-x64.tar.gz`

4）配置环境变量

编辑`/home/test`下的`.bash_profile`，在文件末尾添加两行配置：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708161132.png)

## 安装在root用户下

root用户的jdk一般安装在`/usr/java`目录

root用户的环境变量配置，编辑`/etc/profile`，在末尾添加配置（跟test用户的配置相同）

PS：若root用户没安装jdk，在root用户下`java -version`也能查看到test用户安装的jdk，TODO...




# Tomcat

## 安装

前提：安装Tomcat之前，必须检查是否配置了Java环境（JAVA_HOME），Tomcat版本跟Java版位数（32 or 64）要相同。

下载tar包，放到相应目录，解压、运行即可。


## 配置开机启动

1）`vim /etc/rc.d/rc.local`增加如下配置

```
export JAVA_HOME=/home/test/Java/jdk1.8.0_201
/home/test/Tomcat/apache-tomcat-9.0.17/bin/startup.sh start
```

如下：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708161152.png)

2）将rc.local修改为可执行 `chmod 777 /etc/rc.d/rc.local`

参考：https://www.cnblogs.com/jiuyue-flamen/p/6818022.html




# Redis

## 传智播客-PHP2015-Redis视频教程

创建redis目录，上传文件并解压

```
# mkdir /home/jinnan/rdtar

# cd /home/jinnan/rdtar
# tar zxvf redis-2.6.14.tar.gz
```

执行make命令

```
# cd /home/jinnan/rdtar/redis-2.6.14
# make
```

创建/usr/local/redis运行目录，并给其copy两个执行文件、一个配置文件到此目录

```
# mkdir /usr/local/redis
# cp redis.conf /usr/local/redis
# cd src
# cp redis-cli redis-server /usr/local/redis
```

修改redis.conf配置文件，设置后台启动redis

```
deamonize yes
```

带着redis.conf配置文件，在后台启动redis服务

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708161212.png)


此处没有设置redis开机启动，参考 数据库/Redis.md




# MySQL

## 安装（Generic Binaries）

- 下载：mysql-5.5.57-linux-glibc2.12-x86_64.tar.gz
- 解压：tar xzvf mysql-5.5.57-linux-glibc2.12-x86_64.tar.gz
- mv mysql-5.5.57-linux-glibc2.12-x86_64 /usr/local/mysql -r
- 创建组和用户：`groupadd mysql`,`useradd -r -g mysql mysql`
- 安装数据库

```sh
cd /usr/local/mysql     # 进入安装mysql软件目录
chown -R mysql:mysql ./ # 修改当前目录拥有者为mysql用户
./scripts/mysql_install_db --user=mysql     # 安装数据库
chown -R root:root ./       # 修改当前目录拥有者为root用户
chown -R mysql:mysql data   # 修改当前data目录拥有者为mysql用户

cp support-files/mysql.server /etc/init.d/mysql     # 添加开机启动脚本
chkconfig --add mysql       # 添加开机启动服务，将执行上面的脚本，，，这是开机自启的重点

修改mysql启动时的参数：
-----------------------------------------------
[root@localhost software]# vim /etc/my.cnf
[mysqld]
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock

[mysqld_safe]
# log-error=/var/log/mariadb/mariadb.log    # 这是原来的配置
# pid-file=/var/run/mariadb/mariadb.pid     # 这是原来的配置
log-error=/var/log/mysql/mysql.log      # 修改后的配置，不修改的话，mysql用户
                                        # 访问不到 /var/log/mariadb/mariadb.log，因此不能启动数据库实例
pid-file=/var/run/mysql/mysql.pid       # 修改后的配置

!includedir /etc/my.cnf.d
-----------------------------------------------

另外，要建立一个软链接，防止异常：Can't connect to local MySQL server through socket '/tmp/mysql.sock' (2)
ls -s /var/lib/mysql/mysql.sock /tmp/mysql.sock

service mysql start/stop     # 手动启动数据库服务
ps -ef|grep mysql       # 查看mysql数据库进程是否已启动

```

- ./bin/mysqladmin -u root password '密码'    # 修改数据库用户密码 ，或 alter user system identified by 123456;
- ln -s /usr/local/mysql/bin/mysql /usr/local/bin/mysql     # 将mysql客户端放到默认路径
- mysql -uroot -p   # 登录数据库，验证是否安装成功

注意：/etc/my.cnf.d 可以配置很多参数。可以参看官网或《CentOS7系统管理与运维实战》。

参考的资料：

- [Linux下安装mysql][1]
- [Can't connect to local MySQL server through socket '/tmp/mysql.sock' (2)][2]
- [Linux下Mysql自启动][3]

## 远程连接

- ~~修改 /etc/my.cnf，增加 bind-address = 0.0.0.0~~

- 登录mysql，并进行如下设置

```sh
mysql> grant all privileges  on *.* to root@'%' identified by "root";
Query OK, 0 rows affected (0.01 sec)

mysql> flush privileges;
Query OK, 0 rows affected (0.00 sec)

```

- 关闭linux的防火墙或修改防火墙配置

```sh
开启防火墙：
1、查看端口开启情况，若之前没有配过会显示 no 说明 3306 端口未开放，反之 yes 说明已开放直接可用 Mysql 客户端远程访问！
$ sudo firewall-cmd --query-port=3306/tcp  
no

2、临时性开启 3306 端口
$ sudo firewall-cmd --add-port=3306/tcp  
success 

3、永久性开启 3306 端口
$ sudo firewall-cmd --permanent --zone=public --add-port=3306/tcp  
success  
$ sudo firewall-cmd --reload #重新加载配置  
success  
[john@localhost ~]$ sudo firewall-cmd --zone=public --list-all #查看添加结果  
public (default, active)  
  interfaces: eth0  
  sources:   
  services: dhcpv6-client ssh  
  ports: 3306/tcp 22/tcp  
  masquerade: no  
  forward-ports:   
  icmp-blocks:   
  rich rules: 
```

以上防火墙设置只对 CentOS7.

注意：`firewall-cmd`是一个完整的命令，而不是 firewall cmd.

删除一个端口：
`sudo firewall-cmd --permanent --zone=public --remove-port=3306/tcp`，`firewall-cmd --reload #重新加载配置`。

参考：[ Mysql 安装(Using Generic Binaries)][4] 之 Mysql远程连接 与 防火墙开启

[1]:http://www.cnblogs.com/fnlingnzb-learner/p/5830622.html
[2]: http://blog.csdn.net/ixidof/article/details/5958904/
[3]: http://www.cnblogs.com/fnlingnzb-learner/p/5832917.html
[4]: http://blog.csdn.net/johnnycode/article/details/40783553



# 正则表达式终端测试工具

```shell
# yum install -y pcre-tools
```

如何使用？
- re：输入正则表达式，//表示开头和结束
- data：测试的数据

```
[root@localhost ~]# pcretest 
PCRE version 8.32 2012-11-30

  re> /(\d+)\.(\d+)\.(\d+)\.(\d+)/
data> 192.168.12.133
 0: 192.168.12.133
 1: 192
 2: 168
 3: 12
 4: 133
```

参考：https://www.bilibili.com/video/av55251610?p=55


# 其它

```
yum -y install vim
yum -y install net-tools
yum -y install psmisc

yum -y install git

yum install -y bash-completion #命令自动补齐，安装后，需要退出编辑器后重进就正常了

yum -y install initscripts # bash: ip command not found

yum install -y wget
```
