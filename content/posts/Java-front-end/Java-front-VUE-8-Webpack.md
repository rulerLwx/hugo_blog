---
title: "Java-front-VUE-8-Webpack"
date: 2018-06-20T11:08:02+08:00
draft: true
categories: ["Java-front-end"]
---

# npm

npm（node package manage）

常用命令：
- npm 初始化：`npm init -y`
- 将 npm 更新到最新版本：`npm install npm@latest`
- 安装 jquery ：`npm install jquery [-g]` 或指定版本号 `npm i jquery@3.0.1  [-g]`
- 卸载 jquery ：`npm uninstall jquery [-g]`
- 更新 juqery ：`npm update jquery`
- 查看全局安装了哪些包（模块）：`npm list -g --depth 0`
- 查看**全局**或**项目**中模块安装的路径：`npm root [-g]`
- 模块信息（名称、版本号、依赖关系、Repo）：`npm view <name> [package.json属性名称]`

参考：https://www.jianshu.com/p/7ea13d57638b

免费的 CDN 服务：https://www.bootcdn.cn/

# nrm

什么是 nrm ？

nrm（npm registry manager ）是 npm 的镜像源管理工具，提供了一些最常用的 NPM 包镜像地址，例如淘宝镜像，能够让我们快速的切换安装包时候的服务器地址；

安装 nrm 之前，先安装 npm ，官网：https://github.com/npm/cli ，下载地址：https://nodejs.org/en/download/

如：在 windows 中安装 node-v10.16.3-x64.msi ，

```tex
C:\Users\lwx>npm i nrm -g
npm WARN deprecated coffee-script@1.7.1: CoffeeScript on NPM has moved to "coffeescript" (no hyphen)
C:\Users\lwx\AppData\Roaming\npm\nrm -> C:\Users\lwx\AppData\Roaming\npm\node_modules\nrm\cli.js
+ nrm@1.2.1
added 489 packages from 840 contributors in 148.807s

C:\Users\lwx>
```

环境变量（自动）配置好后，进行以下步骤：

1. 运行`npm i nrm -g`全局安装`nrm`包；
2. 使用`nrm ls`查看当前所有可用的镜像源地址以及当前所使用的镜像源地址；
3. 使用`nrm use npm`或`nrm use taobao`切换不同的镜像源地址；

> 注意： nrm 只是单纯的提供了几个常用的 下载包的 URL地址，并能够让我们在 这几个 地址之间，很方便的进行切换，但是，我们每次装包的时候，使用的 装包工具，都是  npm ——《vue教程-黑马-205集完整版》

# cnpm

cnpm 是装包工具，安装 npm 后，可以安装 cnpm ：`npm i cnpm -g`


# 概述

## 网页中常见的静态资源？

- JS
    - .js  .jsx  .coffee  .ts（TypeScript  类 C# 语言）
+ CSS
    - .css  .less   .sass  .scss
+ Images
    - .jpg   .png   .gif   .bmp   .svg
+ 字体文件（Fonts）
    - .svg   .ttf   .eot   .woff   .woff2
+ 模板文件
    - .ejs   .jade  .vue【这是在webpack中定义组件的方式，推荐这么用】

## 静态资源多了以后有什么问题？

1. 网页加载速度慢， 因为 我们要发起很多的二次请求；
2. 要处理错综复杂的依赖关系


## 如何解决上述两个问题

1. 合并、压缩、精灵图、图片的Base64编码
2. 可以使用之前学过的requireJS、也可以使用webpack可以解决各个包之间的复杂依赖关系；

## 什么是webpack?

webpack 是前端的一个项目构建工具，它是基于 Node.js 开发出来的一个前端工具；

![](https://gitee.com/leafsing/pic/raw/master/img/20200709020551.png)

webpack 能做什么？
- 处理 JS 文件的相互依赖关系
- 处理 JS 浏览器兼容问题（高级语法转换为低级语法）

## 如何完美实现上述的2种解决方案

1. 使用Gulp， 是基于 task 任务的；
2. 使用Webpack， 是基于整个项目进行构建的；

+ 借助于webpack这个前端自动化构建工具，可以完美实现资源的合并、打包、压缩、混淆等诸多功能。
+ 根据官网的图片介绍webpack打包的过程
+ webpack 官网：https://webpack.js.org


## webpack安装的两种方式

1. 运行`npm i webpack -g`全局安装webpack，这样就能在全局使用webpack的命令，另外安装`npm i webpack-cli -g`
2. 在项目根目录中运行`npm i webpack --save-dev`安装到项目依赖中

本处教程安装的是 webpack 版本 4.41.0


# 入门-示例

命令行窗口，先执行`npm init -y`初始化`package.json`文件，然后安装依赖，如 jquery，最后执行 webpack 打包

- `npm init -y`
- `npm i jquery -S`
- `webpack .\src\main.js -o .\dist\bundle.js --mode=development`

项目结构：

```
webpack
    dist
        bundle.js
    node_modules
        jquery
    src
        css
        images
        js
        index.html
        main.js
    package.json
```

index.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>webpack基本使用</title>
    <!--这里导入的是 webpack 编译后的文件-->
    <script src="../dist/bundle.js"></script>
</head>
<body>
<ul>
    <li>这是第1个li</li>
    <li>这是第2个li</li>
    <li>这是第3个li</li>
    <li>这是第4个li</li>
    <li>这是第5个li</li>
</ul>
</body>
</html>
```

需要使用 webpack 编译 main.js

```js
// main.js 是项目的 JS 入口文件

// 1、导入 jQuery
// 用 $ 来接收 jquery ， import .. from .. 是 es6 导入模块语法
import $ from 'jquery';

$(function () {
    $('li:odd').css('backgroundColor', 'lightblue');
    $('li:even').css('backgroundColor', function () {
        return '#' + '8fd19e'
    });
})
```

# webpack 最基本的配置文件

在项目根目录，新建一个`webpack.config.js`文件，内容如下：

```js
const path = require('path');

// 通过 node 中的模块操作，向外暴露一个配置对象
module.exports = {
    // 手动指定入口、出口
    entry: path.join(__dirname, './src/main.js'),// 要打包哪个文件
    output: {
        path: path.join(__dirname, './dist'),// 输出到哪个目录
        filename: 'bundle.js'// 文件名称
    }
};
```

然后，在控制台输入`webpack`即可将 js 编译至相应的目录

# webpack-dev-server 的基本使用

目的：让 webpack 自动监听代码的改动，自动部署，不用手动执行`webpack`命令

- 安装：`npm i webpack-dev-server -D`，安装到项目本地开发依赖
- 


