---
title: "Java-front-VUE-7-案例"
date: 2018-06-20T10:08:02+08:00
draft: true
categories: ["Java-front-end"]
---

# 名称案例-监听输入框变化

## 使用 keyup 事件实现

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>名称案例-keyup</title>
    <script src="../js/vue.js"></script>
</head>
<body>
    <div id="app">
        <input type="text" v-model="firstname" @keyup="getFullName"/>+
        <input type="text" v-model="lastname" @keyup="getFullName"/>=
        <input type="text" v-model="fullname"/>
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{
                firstname:'',
                lastname:'',
                fullname:''
            },
            methods:{
                getFullName(){
                    this.fullname = this.firstname + "-" + this.lastname;
                }
            }
        })
    </script>
</body>
</html>
```

效果：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709020255.png)


## 使用 watch 实现

在 vm 中使用 watch 属性，监听 data 中指定数据的变化，触发相应的函数

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>名称案例-watch</title>
    <script src="../js/vue.js"></script>
</head>
<body>
    <div id="app">
        <input type="text" v-model="firstname"/>+
        <input type="text" v-model="lastname" />=
        <input type="text" v-model="fullname"/>
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{
                firstname:'',
                lastname:'',
                fullname:''
            },
            methods:{},
            watch:{
                // firstname 与 'firstname' 等同，什么时候用 '' ？当名称有 - 时
                // 函数自带有两个入参
                firstname: function (newVal,oldVal) {
                    this.fullname = newVal + "-" + this.lastname;
                },
                lastname: function (newVal) {
                    this.fullname = this.firstname + "-" + newVal;
                }
            }
        })
    </script>
</body>
</html>
```

总结：keyup 方式与 watch 方式相比，似乎 keyup 方式写起来更简单；但 watch 方式有特有的用法，如监听路由地址变化

### watch 监听路由地址变化

watch 还可以监听非 dom 元素的变化

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>watch-监视路由地址变化</title>
    <script src="../js/vue.js"></script>
    <script src="../js/vue-router.js"></script>
</head>
<body>
    <div id="app">
        <router-link to="/login">登录</router-link>
        <router-link to="/register">注册</router-link>

        <router-view></router-view>
    </div>
    <script>
        var login = {
            template:'<h2>这是登录组件</h2>'
        }
        var register = {
            template:'<h2>这是注册组件</h2>'
        }
        var router = new VueRouter({
            routes:[
                {path:'/login',component:login},
                {path:'/register',component: register},
                {path:'/',redirect:'/login'}
            ]
        });
        var vm = new Vue({
            el:'#app',
            data:{},
            methods:{},
            router,
            watch:{
                // this.$route.path
                '$route.path': function (newVal, oldVal) {
                    console.log(newVal + "---" + oldVal);
                }
            }
        })
    </script>
</body>
</html>
```

## 使用 computed 实现

- 在 vm 中使用 computed 属性，本质是一个方法，只不过我们直接将方法名称当属性用，并不会当作方法来使用
- 在 function 内部所引用到的 data 中的数据发生变化，就会重新计算这个值，不变化则不计算

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>称案例-使用computed</title>
    <script src="../js/vue.js"></script>
</head>
<body>
    <div id="app">
        <input type="text" v-model="firstname"/>+
        <input type="text" v-model="lastname" />=
        <!--这个 fullname 属性并没有在 data 中定义-->
        <input type="text" v-model="fullname"/>
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{
                firstname:'',
                lastname:''
            },
            methods:{},
            computed:{
                'fullname':function () {
                    return this.firstname + '-' + this.lastname;
                }
            }
        })
    </script>
</body>
</html>
```

## keyup/watch/computed 比较

1. `computed`属性的结果会被缓存，除非依赖的响应式属性变化才会重新计算。主要当作属性来使用；
2. `methods`方法表示一个具体的操作，主要书写业务逻辑；
3. `watch`一个对象，键是需要观察的表达式，值是对应回调函数。主要用来监听某些特定数据的变化，从而进行某些具体的业务逻辑操作；可以看作是`computed`和`methods`的结合体；
