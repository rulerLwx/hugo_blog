---
title: "Java-front-VUE-1"
date: 2018-06-20T10:02:02+08:00
draft: true
tags: [ ]
categories: ["Java-front-end"]
---


# 概述

官网：
- 中文：https://cn.vuejs.org/
- 英文：https://vuejs.org/

## 什么是 vue

vue 是渐进式 JavaScript 框架。 vue 不提倡操作 dom。javascript/jquery 是经常操作 dom 的。

## 框架和库的区别

- 框架：是一套完整的解决方案；对项目的侵入性较大，项目如果需要更换框架，则需要重新架构整个项目。
- 库（插件）：提供某一个小功能，对项目的侵入性较小，如果某个库无法完成某些需求，可以很容易切换到其它库实现需求
    - 从Jquery 切换到 Zepto
    - 从 EJS 切换到 art-template

## MVC 与 前端中的 MVVM 之间的区别

MVC解释:
![image](E6FEBB460DDB4D0B99B524312DD6EC4A)

---

MVVM 的解释：
![image](6BE30BC3BEA443BCA9AAD08A71271989)

总结：VM 是 MVVM 思想的核心。


# 入门

## 第一个程序

```html
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <!-- 1. 导入Vue的包 -->
  <script src="./lib/vue-2.4.0.js"></script>
</head>

<body>
  <!-- 将来 new 的Vue实例，会控制这个 元素中的所有内容 -->
  <!-- Vue 实例所控制的这个元素区域，就是我们的 V  -->
  <div id="app">
    <p>{{ msg }}</p>
  </div>

  <script>
    // 2. 创建一个Vue的实例
    // 当我们导入包之后，在浏览器的内存中，就多了一个 Vue 构造函数
    //  注意：我们 new 出来的这个 vm 对象，就是我们 MVVM中的 VM调度者
    var vm = new Vue({
      el: '#app',  // 表示，当前我们 new 的这个 Vue 实例，要控制页面上的哪个区域
      // 这里的 data 就是 MVVM中的 M，专门用来保存 每个页面的数据的
      data: { // data 属性中，存放的是 el 中要用到的数据
        msg: '欢迎学习Vue' // 通过 Vue 提供的指令，很方便的就能把数据渲染到页面上，程序员不再手动操作DOM元素了【前端的Vue之类的框架，不提倡我们去手动操作DOM元素了】
      }
    })
  </script>
</body>
</html
```

注意：`el`是英文，不是英文`e`+数字`1`。

参考《vue教程-黑马-205集完整版》

总结：
- 定义一个 div ，所有需要 vue 操作的元素都需要包含在 div 区域内，否则不起作用。**初学者常犯的错误**。
- 2.x版本之后，不要给 body 标签加 id="app"。
- 如果要访问 data 中的数据，或要访问 methods 中的方法，必须带上 this ——20190922


## v-cloak/v-text/v-html

解决闪烁问题

1）v-cloak

```html
><body>
    <div id="app">
        <p v-clock>{{msg}} </p>
        <h4 v-text="msg"></h4>
        <div v-html="msg2"></div>
    </div>
    <script>
        new Vue({
            el:'#app',
            data:{
                msg:'123',
                msg2:'<h1>我是H1</h1>'
            }
        })
    </script>
</body
```
v-cloak 只插值，不覆盖内容

2）v-text

3）v-html

## v-bind

`v-bind:` 用于属性绑定

- `><input type="button" value="点击" v-bind:title="mytitle"/>`
- `<input type="button" value="点击" v-bind:title="mytitle + 123"/>` 可以拼接
- `<input type="button" value="点击" :title="mytitle"/>` 

`v-bind:`可以简写为`:`


## v-on

```html
<body>
    <div id="app">
        <p v-clock>{{msg}} </p>
        <h4 v-text="msg"></h4>
        <div v-html="msg2"></div>
        <input type="button" value="点击" v-bind:title="mytitle"/>
        <input type="button" value="按钮" v-on:click="show"/>
    </div>
    <script>
        new Vue({
            el:'#app',
            data:{
                msg:'123',
                msg2:'<h1>我是H1</h1>',
                mytitle:'自定义的title'
            },
            methods:{
                show:function(){
                    alert("hello")
                }
            }
        })
    </script>
</body>
```

`<input type="button" value="按钮" v-on:click="show"/>`

注意：不是`v-on:onclick="show"`

`v-on:`可以缩写成`@`


## 跑马灯效果

知识点：

1）didiao(){}是使用了es6的写法，省略了function

```java
  methods:{
      lang:function(){
          alert("hello");
      },
      didiao(){
          alert("didiao");
      }
  }
```

2）注意`this`指针指向

```js
    methods:{
        lang(){
        // 此处的 _this = this 指向 vm 实例
            var _this = this;
            setInterval(function () {
                var start = _this.msg.substr(0, 1);
                var end = _this.msg.substr(1);
                _this.msg = end + start;
            },400)
        }
        lang(){
        //下面是es6的写法，等同于上面 _this 的写法
        //此时的指针指向外面的 vm 实例
            setInterval(() => {
                var start = this.msg.substr(0, 1);
                var end = this.msg.substr(1);
                this.msg = end + start;
            },400)
        }
    }
```

注意：在 VM实例中，如果想要获取 data 上的数据，或者 想要调用 methods 中的 方法，必须通过 this.数据属性名  或  this.方法名 来进行访问，这里的this，就表示 我们 new 出来的  VM 实例对象

思路分析：
1. 给 【浪起来】 按钮，绑定一个点击事件   v-on   @
2. 在按钮的事件处理函数中，写相关的业务逻辑代码：拿到 msg 字符串，然后 调用 字符串的 substring 来进行字符串的截取操作，把 第一个字符截取出来，放到最后一个位置即可；
3. 为了实现点击下按钮，自动截取的功能，需要把 2 步骤中的代码，放到一个定时器中去；

代码实现：

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>跑马灯效果</title>
    <script src="../js/vue.js"></script>
</head>
<body>
    <div id="app">
        <input value="浪起来" type="button" @click="lang"/>
        <input value="低调" type="button" @click="stop"/>
        <h4>{{msg}}</h4>
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{
                msg: '猥琐发育，别浪~~~',
                intervalId:null
            },
            methods:{
                lang(){
                    if (this.intervalId != null) {
                        return;
                    }
                    // 每次调用setInterval()方法，都返回ID
                    this.intervalId = setInterval(() => {
                        var start = this.msg.substr(0, 1);
                        var end = this.msg.substr(1);
                        this.msg = end + start;
                    },400);
                },
                stop(){
                    clearInterval(this.intervalId);
                    // 停止后，需将intervalId置为null
                    this.intervalId = null;
                }
            }
        })
    </script>
</body>
</html>
```

## 事件修饰符

+ .stop       阻止冒泡
+ .prevent    阻止默认事件
+ .capture    添加事件侦听器时使用事件捕获模式
+ .self       只当事件在该元素本身（比如不是子元素）触发时触发回调
+ .once       事件只触发一次

1）.stop

事件冒泡：里面（层）的事件传递给外面（层）

![image](2CE5BBC8730945309C4CEB14183299D6)

使用.stop停止冒泡 `<input type="button" value="戳它" @click.stop="btnHandler">`

```html
<body>
    <div id="app">
        <div class="inner" @click="divHandler">
            <input type="button" value="戳它" @click.stop="btnHandler">
        </div>
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{},
            methods:{
                divHandler(){
                    console.log("这是触发了 divHandler 的点击事件")
                },
                btnHandler(){
                    console.log("这是触发了 btnHandler 的点击事件")
                }
            }
        })
    </script>
</body>
```

2）.prevent

```html
    <div id="app">
        <a href="https://www.baidu.com" @click.prevent="linkClick">有问题，去百度</a>
    </div>
```

```
var vm = new Vue({
    el:'#app',
    data:{},
    methods:{
        linkClick(){
            console.log("这是触发了 linkClick 的点击事件");
            alert("prevent")
        }
    }
})
```

注意：事件一定要交给 vue 来管理才行，也就是，事件所发生区域一定要在`el:'#app'`这个区域内！！！

3）.capture

事件从 外层 到 里层 触发事件

与“事件冒泡”顺序相反

4）.self 

只当事件在该元素本身（比如不是子元素）触发时触发回调

5）.once       

事件只触发一次

---

问：`.self`和`.stop`的区别？

答：`.self`只会阻止自己身上的冒泡行为，并不会真正阻止冒泡行为


## v-model

原理：在 Vue 中，已经实现了数据的双向绑定（M 和 V 之间），每当我们修改了 data 中的数据，vue 会默认监听到数据的改动，自动将最新的数据应用到页面上；页面上修改的数据也会自动应用到 data 中。

- `v-model=""` 可以双向数据绑定
- `v-bind:`只能实现数据的单向绑定，从 M 自动绑定到 V

注意：`v-model=""`**只能应用在表单元素**中


示例：
```html
<body>
    <div id="app">
        <h4>{{msg}}</h4>
        <input type="text" v-model="msg">
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{
                msg:'我们都是好学生'
            },
            methods:{}
        })
    </script>
</body>
```
效果图如下：
![image](A232702B32D54BCB9387D71FF38090B8)

示例，简易计算器的实现：

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>v-model实现简易计算器</title>
    <script src="../js/vue.js"></script>
</head>
<body>
    <div id="app">
        <input type="text" v-model="n1">
        <select v-model="opt">
            <option value="+">+</option>
            <option value="-">-</option>
            <option value="*">*</option>
            <option value="/">/</option>
        </select>
        <input type="text" v-model="n2">
        <input type="button" value="=" @click="calc"/>
        <input type="text" v-model="result"/>
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{
                n1:'0',
                opt: '+',
                n2:'0',
                result:'0'
            },
            methods:{
                calc(){
                    switch (this.opt) {
                        case "+":
                            // 输入校验，todo...
                            this.result = parseInt(this.n1) + parseInt(this.n2);
                            break;
                        case "-":
                            this.result = parseInt(this.n1) - parseInt(this.n2);
                            break;
                        case "*":
                            this.result = parseInt(this.n1) * parseInt(this.n2);
                            break;
                        case "/":
                            this.result = parseInt(this.n1) / parseInt(this.n2);
                            break;
                        default:
                            break;
                    }
                }
            }
        })
    </script>
</body>
</html>
```
效果如下：

![image](8A2E4783D00F4E4EB7D118D3FA830D7F)


## 在Vue 中使用样式

示例代码：
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>vue中使用样式-class</title>
    <script src="../js/vue.js"></script>
    <style>
        .red {
            color: red;
        }
        .thin {
            font-weight: 200;
        }
        .italic{
            font-style: italic;
        }
        .active{
            letter-spacing: 0.5em;
        }
    </style>
</head>
<body>
    <div id="app">
        <!--以前的class样式使用方法-->
        <h1 class="red thin">这是一个很大很大的H1</h1>
        <!--vue方式，数组-->
        <h1 :class="['red','thin']">这是一个很大很大的H1</h1>
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{},
            methods:{}
        })
    </script>
</body>
</html>
```

### 使用class样式

语法：`v-bind:class=""`，或缩写成 `:class=""`

1. 数组
    ```
    <h1 :class="['red', 'thin']">这是一个邪恶的H1</h1>
    ```
2. 数组中使用三元表达式
    ```
    <h1 :class="['red', 'thin', isactive?'active':'']">这是一个邪恶的H1</h1>
    ```

isactive 是 data 中的一个布尔值

3. 数组中嵌套对象
    ```
    <h1 :class="['red', 'thin', {'active': isactive}]">这是一个邪恶的H1</h1>
    ```

4. 直接使用对象
    ```
    <h1 :class="{red:true, italic:true, active:true, thin:true}">这是一个邪恶的H1</h1>
    ```

### 使用内联样式

语法：`v-bind:style=""`，或缩写成 `:style=""`

1. 直接在元素上通过 `:style` 的形式，书写样式对象
    ```
    <h1 :style="{color: 'red', 'font-size': '40px'}">这是一个善良的H1</h1>
    ```

2. 将样式对象，定义到 `data` 中，并直接引用到 `:style` 中
 
    + 在data上定义样式：
    ```
    data: {
            h1StyleObj: { color: 'red', 'font-size': '40px', 'font-weight': '200' }
    }
    ```
    + 在元素中，通过属性绑定的形式，将样式对象应用到元素中：
    ```
    <h1 :style="h1StyleObj">这是一个善良的H1</h1>
    ```

3. 在 `:style` 中通过数组，引用多个 `data` 上的样式对象
    + 在data上定义样式：
    ```
    data: {
            h1StyleObj: { color: 'red', 'font-size': '40px', 'font-weight': '200' },
            h1StyleObj2: { fontStyle: 'italic' }
    }
    ```
    + 在元素中，通过属性绑定的形式，将样式对象应用到元素中：
    ```
    <h1 :style="[h1StyleObj, h1StyleObj2]">这是一个善良的H1</h1>
    ```

## v-for

1）遍历数组

```html
<body>
    <div id="app">
        <p v-for="item in list">{{item}}</p>
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{
                list:[1,2,3,4,5,6]
            },
            methods:{}
        })
    </script>
</body>
```
页面效果：

```html
1
2
3
4
5
6
```

2）循环数组，并拿到索引值

```html
<body>
    <div id="app">
        <p v-for="(item,i) in list">索引：{{i}}，值：{{item}}</p>
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{
                list:[1,2,3,4,5,6]
            },
            methods:{}
        })
    </script>
</body>
```

页面效果：
```
索引：0，值：1
索引：1，值：2
索引：2，值：3
索引：3，值：4
索引：4，值：5
索引：5，值：6
```

3）循环对象数组

```html
<body>
    <div id="app">
        <p v-for="user in list">{{user.id}},{{user.name}}</p>
        <!--拿到索引-->
        <p v-for="(user,i) in list">{{user.id}},{{user.name}},索引：{{i}}</p>
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{
                list:[
                    {id:1,name:'zs1'},
                    {id:2,name:'zs2'},
                    {id:3,name:'zs3'},
                    {id:4,name:'zs4'}
                ]
            },
            methods:{}
        })
    </script>
</body>
```

4）遍历对象属性

```html
<body>
    <div id="app">
        <!--(val,key)，对象属性的值在前面，名称在后面-->
        <p v-for="(val,key) in user">{{val}},{{key}}</p>
        <!--对象的索引-->
        <p v-for="(val,key,i) in user">{{val}},{{key}},索引：{{i}}</p>
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{
                user:{
                    id:1,
                    name:'托尼·波斯顿',
                    gender:'男'
                }
            },
            methods:{}
        })
    </script>
</body>
```

5）循环数字

```html
<body>
    <div id="app">
        <!--数字从 1 开始-->
        <p v-for="count in 10">{{count}}</p>
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{}
        })
    </script>
</body>
```

6）绑定 key

指定唯一个字符/数字为key

```html
<body>
    <div id="app">
        <label>
            ID：<input type="text" v-model="id">
        </label>
        <label>
            Name：<input type="text" v-model="name">
        </label>
        <input type="button" value="添加" @click="add"/>
        <!--绑定key，指定唯一的字符或数字为key-->
        <p v-for="item in list" :key="item.id">
            <input type="checkbox">{{item.id}}---{{item.name}}
        </p>
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{
                id:'',
                name:'',
                list:[
                    {id:1,name:'李斯'},
                    {id:2,name:'赢政'},
                    {id:3,name:'越高'},
                    {id:4,name:'韩非'},
                    {id:5,name:'荀子'}
                ]
            },
            methods:{
                add(){
                    // 判断重复添加，todo...
                    this.list.push({id:this.id,name:this.name})
                }
            }
        })
    </script>
</body>
```
效果如下：

![image](C27E8BC3D75646EEA89BDB88AAB0FCE5)

## v-if 和 v-show

- v-if 每次都会重新删除或创建新元素，有较高的切换性能消耗
- v-shwo 则不会，只是切换了元素的 display:none 样式，有较高的初始渲染消耗

总结：
- 如果元素经常切换，最好使用 v-show
- 如果元素可能永远不会被显示出来给用户看，最好使用 v-if


```html
<body>
    <div id="app">
        <input type="button" value="切换" @click="flag=!flag"/>
        <h3 v-if="flag">这是用v-if控制的元素</h3>
        <h3 v-show="flag">这是用v-show控制的元素</h3>
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{
                flag:true
            },
            methods:{}
        })
    </script>
</body>
```

@click="flag=!flag" 是一种更简单的写法，也可以用事件控制来改变 flag 值

## 总结-变量（表达式）还是值

- 有冒号的，是变量（表达式），如：:name="name"
- 没有冒号的，是普通的值，如：name="ls" ——20191004
