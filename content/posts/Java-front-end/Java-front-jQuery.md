---
title: "Java-front-JQuery"
date: 2018-06-01T10:03:02+08:00
draft: true
tags: [ ]
categories: ["Java-front-end"]
---

# 什么是 jQuery

jQuery 是 JavaScript 的一个库，目的是简化 JavaScript 的开发。

JavaScript 的常见程序库有哪些？
- jQuery
- Prototype
- Dojo
- Ext JS
- Moo Tools 

参考：《锋利的jQuery第2版》


# jQuery与JavaScript对象的互换

- 约定：jQuery对象在变量前加上$
```
    var $varible = jQuery对象;

    var variable = DOM对象;
```
- jQuery对象转成DOM对象：
```
    有两种办法，即[index]和get[index].

    1) jQuery对象是类似数组的对象
        var $cr = $("#cr");
        var cr = $cr[0];

    2) jQuery本身提供的yy
        var $cr = $("#cr");
        var cr = $cr.get(0);
```
- DOM对象转换成jQuery对象
```
    只要用$()把DOM对象包装起来：
    var cr = document.getElementById("cr");
    var $cr = $(cr);
```

# jQuery 的写法

```js
	// 1.第一种写法
    $(document).ready(function () {
        // alert("hello lnj");
    });

    // 2.第二种写法
    jQuery(document).ready(function () {
        // alert("hello lnj");
    });

    // 3.第三种写法(推荐)
    $(function () {
        // alert("hello lnj");
    });

    // 4.第四种写法
    jQuery(function () {
        alert("hello lnj");
    });
```

参考：https://blog.csdn.net/qq_43645678/article/details/93380482#2__jQuery_29


# jQuery - AJAX

## 1. load() 方法

jQuery load() 方法是简单但强大的 AJAX 方法。
load() 方法从服务器加载数据，并把返回的数据放入被选元素中。

```
    语法：$(selector).load(URL,data,callback);

    -必需的 URL 参数规定您希望加载的 URL。
    -可选的 data 参数规定与请求一同发送的查询字符串键/值对集合。
    -可选的 callback 参数是 load() 方法完成后所执行的函数名称。

    $("#div1").load("demo_test.txt");
```

## 2. $.get()
```
    语法：$.get(URL,callback);

    -必需的 URL 参数规定您希望请求的 URL。
    -可选的 callback 参数是请求成功后所执行的函数名

    $("button").click(function(){
      $.get("demo_test.asp",function(data,status){
        alert("Data: " + data + "\nStatus: " + status);
      });
    });
```

## 3. $.post()

```
    语法：$.post(URL,data,callback);

    -必需的 URL 参数规定您希望请求的 URL。
    -可选的 data 参数规定连同请求发送的数据。
    -可选的 callback 参数是请求成功后所执行的函数名。

    $("button").click(function(){
      $.post("demo_test_post.asp",
      {
        name:"Donald Duck",
        city:"Duckburg"
      },
      function(data,status){
        alert("Data: " + data + "\nStatus: " + status);
      });
    });
```

# jQuery Validate

github:https://github.com/jquery-validation/jquery-validation

todo..


# Q & A

## 测试页面是否加载 jquery成功：

```
    <script type="text/javascript">
        $(function(){
            alert("hello jquery");
        })
    </script>
```

## $()会在文档加载完成时执行

js中`(function(){}())`、`(function(){})()`、`$(function(){})`之间的区别

https://blog.csdn.net/stpice/article/details/80586444

##  $()与$(document) 区别

$(document)是一个选择器，选中的是整个html所有元素的集合。

$(function(){..})等于document.ready 事件，参考上面的笔记`jQuery的写法`的第三种

所有事件函数都写在 ready 中：

```html
<head>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
      $("button").click(function(){
        $("p").hide();
      });
      //事件函数依次写在下面
      
    });
</script>
</head>
```
