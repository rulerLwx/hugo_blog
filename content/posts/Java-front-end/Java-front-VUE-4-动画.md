---
title: "Java-front-VUE-4-动画"
date: 2018-06-20T10:05:02+08:00
draft: true
categories: ["Java-front-end"]
---

# 动画-单元素/组件的过渡

动画能够提高用户的体验，帮助用户更好的理解页面中的功能

## 原理

在进入/离开的过渡中，会有 6 个 class 切换：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709014341.png)

- *-active 可以理解为时间段（2个），其它为时间点（4个）
- v-enter/v-leave-to 样式相同，v-enter-to/v-leave 样式相同

## 入门-自定义样式实现动画

步骤一：使用`transition`标签封装动画元素或组件

```html
    <div id="app">
        <!--需求：点击按钮，让 h3 显示或隐藏-->
        <input type="button" value="toggle" @click="flag=!flag"/>
        <transition>
            <h3 v-if="flag">这是一个H3</h3>
        </transition>
    </div>
```

步骤二：自定义两组样式，来控制 transition 内部元素实现动画

```html
    <style type="text/css">
        /*进入至离开，两个时间点*/
        .v-enter,
        .v-leave-to{
            opacity: 0;
            transform: translateX(150px);
        }
        /*两个时间段*/
        .v-enter-active,
        .v-leave-active{
            transition: all 1s;
        }
    </style>
```

对于这些在过渡中切换的类名来说，如果你使用一个没有名字的 <transition>，则 v- 是这些类名的默认前缀。

如果你使用了 <transition name="my-transition">，那么 v-enter 会替换为 my-transition-enter。这样可以不同组的变化 ——20190927

官方文档：https://cn.vuejs.org/v2/guide/transitions.html#%E8%BF%87%E6%B8%A1%E7%9A%84%E7%B1%BB%E5%90%8D

知识点：
- opacity（不透明性）
    - 0：完全不可见
    - 1：完全可见


## 使用第三方库实现动画-animate.css

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>动画-animate.css</title>
    <script src="../js/vue.js"></script>
    <link rel="stylesheet" href="../css/animate.css">
    <style>

    </style>
</head>
<body>
<div id="app">
    <input type="button" value="toggle" @click="flag=!flag"/>
    <transition enter-active-class="animated bounceIn" leave-active-class="animated bounceOut">
        <h3 v-if="flag">这是一个H3</h3>
    </transition>
</div>
<script>
    var vm = new Vue({
        el:'#app',
        data:{
            flag:true
        },
        methods:{}
    })
</script>
</body>
</html>
```

还可以设置动画时间有长短，参考官方手册：https://github.com/daneden/animate.css


## 使用钩子函数实现动画

```html
<transition
  v-on:before-enter="beforeEnter"
  v-on:enter="enter"
  v-on:after-enter="afterEnter"
  v-on:enter-cancelled="enterCancelled"

  v-on:before-leave="beforeLeave"
  v-on:leave="leave"
  v-on:after-leave="afterLeave"
  v-on:leave-cancelled="leaveCancelled"
>
  <!-- ... -->
</transition>
```

```
        methods:{
            beforeEnter(el){},
            enter(el,done){},
            afterEnter(el){}
        }
```

https://cn.vuejs.org/v2/guide/transitions.html#JavaScript-%E9%92%A9%E5%AD%90

总结：
- 钩子函数的第一个参数 el ，是指要执行动画的那个 DOM 元素，是一个原生的 JS 对象
- beforeEnter(el) 表示动画进入之前，动画尚未开始，用于设置元素开始动画之前的样式
- enter(el,done) 最后要记得调用 done() 函数
- afterEnter(el)

> 当只用 JavaScript 过渡的时候，在 enter 和 leave 中必须使用 done 进行回调。否则，它们将被同步调用，过渡会立即完成。

其实 done 指的就是 afterEnter 这个函数，是引用 afterEnter 。——vue教程-黑马-205集完整版 p50

示例，小球动画：

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>钩子函数-小球动画</title>
    <script src="../js/vue.js"></script>
    <style>
        .ball {
            width: 15px;
            height: 15px;
            border-radius: 50%;
            background-color: red;
        }
    </style>
</head>
<body>
    <div id="app">
        <input type="button" value="快到碗里来" @click="flag=!flag"/>
        <transition
            @before-enter="beforeEnter"
            @enter="enter"
            @after-enter="afterEnter"
        >
            <div class="ball" v-show="flag"></div>
        </transition>
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{
                flag:false
            },
            methods:{
                beforeEnter(el){
                    el.style.transform = "translate(0,0)";
                },
                enter(el,done){
                    el.offsetWidth;// 如果不写这句话，没有动画效果；只要是任一个 el.offsetXXX 都行
                    el.style.transform = "translate(150px,450px)";
                    el.style.transition = 'all 1.5s ease';
                    done();
                },
                afterEnter(el){
                    // 将 flag 置为 false ，跳过下半场动画
                    this.flag = !this.flag;
                }
            }
        })
    </script>
</body>
</html>
```

问：为什么每次点击按钮，小球都从开始位置执行动画？

答：因为在 afterEnter 函数中，将 flag 置为 false ，跳过下半场动画动。画从头开始执行了，也就是从 beforeEnter 开始重新执行。

# 列表动画

- 如果需要过度的元素，是通过 v-for 渲染出来的，不能使用 transition ，需要使用 transition-group 包裹；而且，需要为每个元素设置 :key 属性
- `.v-move{transition: all 0.6s ease;}` 与 `.v-leave-active{position: absolute}` 一般配合使用，实现渐渐地漂上来的效果
- `<transition-group appear>`：增加 appear 属性可以实现数据初始化时的“入场”效果
- `<transition-group appear tag="ul">`：通过 tag 属性指定将 transition 渲染成 ul 元素，默认是 span 元素。


```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>列表动画</title>
    <script src="../js/vue.js"></script>
    <style>
        li {
            border: 1px dashed #999999;
            margin: 5px;
            line-height: 35px;
            padding-left: 5px;
            font-size: 12px;
            width: 100%; /*这里要写100%*/
        }
        li:hover{
            background-color: pink;
            transition: all 0.8s ease;
        }
        .v-enter,
        .v-leave-to {
            opacity: 0;
            transform: translateY(80px);
        }
        .v-enter-active,
        .v-leave-active {
            transition: all 0.6s ease;
        }
        /* .v-move{transition: all 0.6s ease;} 与 .v-leave-active{position: absolute} 一般配置使用，实现渐渐地漂上来的效果*/
        .v-move {
            transition: all 0.6s ease;
        }
        .v-leave-active {
            position: absolute
        }

    </style>
</head>
<body>
    <div id="app">
        <div>
            ID:<input type="text" v-model="id"/>
            Name:<input type="text" v-model="name"/>
            <input type="button" @click="add" value="添加"/>
        </div>
        <!--如果需要过度的元素，是通过 v-for 渲染出来的，不能使用 transition ，需要使用 transition-group 包裹-->
        <!--而且，需要为每个元素设置 :key 属性-->
        <transition-group appear tag="ul">
            <li v-for="(item,i) in list" :key="item.id" @click="del(i)">
                {{item.id}} --- {{item.name}}
            </li>
        </transition-group>
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{
                id: '',
                name: '',
                list:[
                    {id:1,name:'赵高'},
                    {id:2,name:'秦桧'},
                    {id:3,name:'严嵩'},
                    {id:4,name:'魏忠贤'}
                ]
            },
            methods:{
                add() {
                    this.list.push({id: this.id, name: this.name});
                    this.id = this.name = '';
                },
                del(i) {
                    // 从数组中删除元素
                    // i 是元素索引
                    this.list.splice(i, 1);
                }
            }
        })
    </script>
</body>
</html>
```
