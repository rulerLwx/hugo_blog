---
title: "Java-front-VUE-3-vue-resource"
date: 2018-06-20T10:04:02+08:00
draft: true
categories: ["Java-front-end"]
---

# vue-resource

除了 vue-resource 之外，还可以使用 `axios` 的第三方包实现数据的请求

## 什么是 vue-resource

官网：https://github.com/pagekit/vue-resource

## 如何使用

vue-resource 依赖于 vue，导包的时候，先导 vue.js，再导 vue-resource.js。顺序不能乱。

导包
```html
    <script src="../js/vue.js"></script>
    <script src="../js/vue-resource.js"></script>
```

使用
```js
// in a Vue instance
this.$http.get('/someUrl', [config]).then(successCallback, errorCallback);
```

successCallback 是必须传的，errorCallback 可选

调用方法格式：

List of shortcut methods:
- get(url, [config])
- head(url, [config])
- delete(url, [config])
- jsonp(url, [config])
- post(url, [body], [config])
- put(url, [body], [config])
- patch(url, [body], [config])

参考：https://github.com/pagekit/vue-resource/blob/develop/docs/http.md

入门示例：

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>vue-resource</title>
    <script src="../js/vue.js"></script>
    <!--注意导包顺序，先 vue.js，再 vue-resource.js-->
    <script src="../js/vue-resource.js"></script>
    <link rel="stylesheet" href="../css/bootstrap-3.3.7.css">
</head>
<body>
    <div id="app">
        <input type="button" value="Get请求" @click="getInfo"/>
        <input type="button" value="Post请求" @click="postInfo"/>
        <input type="button" value="JSONP请求" @click="jsonpInfo"/>
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{},
            methods:{
                getInfo(){
                    this.$http.get('http://127.0.0.1:8080/vue/get').then(function (result) {
                        console.log(result.data)
                    })
                },
                postInfo(){
                    // 第三个参数设置为 emulateJSON: true
                    this.$http.post('http://127.0.0.1:8080/vue/post', {}, {emulateJSON: true}).then(function (result) {
                        console.log(result);
                    });
                },
                jsonpInfo(){
                    this.$http.jsonp('http://127.0.0.1:8080/vue/jsonp').then(result => {
                        console.log("result:"+result)
                    })
                }
            }
        })
    </script>
</body>
</html>
```

后台代码`Controller`

```java
package com.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/vue")
public class VueTestController {
    @RequestMapping("/get")
    @CrossOrigin
    public String testGet() {
        System.out.println("==========testGet==========");
        return "index";
    }

    @RequestMapping("/post")
    @ResponseBody
    @CrossOrigin
    public Map<String, String> testPost(){
        Map<String, String> map = new HashMap<>();
        map.put("a", "a");
        map.put("b", "b");
        return map;
    }

    @RequestMapping("/jsonp")
    @ResponseBody
    public String testJsonp(HttpServletRequest req) {
        String callback = req.getParameter("callback");
        System.out.println("----callback----:"+callback);
        return callback+"()";// 返回格式要正确，如 _jsonp1q42uhqydsk({...})，括号里面要是一个 json 对象
    }
}
```

后台接口使用`@CrossOrigin`修饰，避免了“跨域”问题，见附件

## 使用 vue-resource 改造商品列表

- 初始化数据（$http.get/post()）尽量在生命周期的 create 阶段 ——实践过，要在 beforeMount 才行，
- 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>品牌列表案例</title>
    <script src="../js/vue.js"></script>
    <script src="../js/vue-resource.js"></script>
    <link rel="stylesheet" href="../css/bootstrap-3.3.7.css">
</head>
<body>
    <div id="app">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">添加品牌</h3>
            </div>
            <div class="panel-body form-inline">
                <!--<label>ID:<input type="text" class="form-control" v-model="id"></label>-->
                <label>Name:<input type="text" class="form-control" v-model="name" @keyup.enter="add"></label>
                <input type="button" value="添加" class="btn btn-primary" @click="add"/>
                <label>搜索名称关键字：<input type="text" class="form-control" v-model="keywords" v-focus v-color="'red'"/> </label>
            </div>
        </div>
        <table class="table table-bordered table-hover table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Ctime</th>
                <th>Operation</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="item in search(keywords)" :key="item.id">
                <td>{{item.id}}</td>
                <td>{{item.name}}</td>
                <td>{{item.ctime | dateFormat('yyyy-mm-dd')}}</td>
                <td><a href="" @click.prevent="del(item.id)">删除</a></td>
            </tr>
            </tbody>
        </table>
    </div>
    <script>
        // 全局-过滤器-时间格式化
        // pattern="" 默认赋值为""
        Vue.filter('dateFormat', function (dateStr, pattern="") {
            var dt = new Date(dateStr);
            var y = dt.getFullYear();
            var m = (dt.getMonth() + 1).toString().padStart(2,0);
            var d = dt.getDate().toString().padStart(2,0);
            if (pattern.toLowerCase() == "yyyy-mm-dd") {
                return `${y}-${m}-${d}`;// esc 下面那个键
            } else {
                var h = dt.getHours().toString().padStart(2,0);
                var min = dt.getMinutes().toString().padStart(2,0);
                var s = dt.getSeconds().toString().padStart(2,0);
                return `${y}-${m}-${d} ${h}:${min}:${s}`;
            }
        });
        Vue.directive('focus',{
            // 第一个参数 el：表示被绑定了指令的那个元素，是一个原生 js 对象
            bind:function (el) {

            },
            inserted:function (el) {
                el.focus();
            },
            update:function () {

            }
        });
        Vue.directive('color', {
            bind: function (el,binding) {
                // 内联样式
                // el.style.color = 'red';// 不应该写死，参数应该从页面传来
                el.style.color = binding.value;
            }
        });
        var vm = new Vue({
            el: '#app',
            data: {
                name: '',
                keywords: '',
                list: [
                    {id: 1, name: '奔驰', ctime: new Date()},
                    {id: 2, name: '宝马', ctime: new Date()}
                ]
            },
            // 初始化列表数据
            beforeMount(){
                this.getAllList();
            },
            methods: {
                getAllList(){
                  this.$http.get('http://www.liulongbin.top:3005/api/getprodlist').then(result => {
                      var result = result.body;
                      if (result.status === 0) {
                          this.list = result.message;
                      } else {
                          alert("获取数据失败")
                      }
                  })
                },
                add() {
                    this.$http.post('http://www.liulongbin.top:3005/api/addproduct',
                        {name:this.name},// id 不用传，后台自动生成
                        {emulateJSON: true})
                        .then(result =>{
                            if (result.body.status === 0) {
                                // 添加完成后，查出所有数据回显到表单
                                this.getAllList();
                                this.name = '';
                            }else{
                                alert("添加失败");
                            }
                        })
                },
                // 删除
                del(id) {
                    this.$http.get('http://www.liulongbin.top:3005/api/delproduct/' + id).then(result => {
                        if (result.body.status == 0) {
                            // 刷新页面数据
                            this.getAllList();
                        }else {
                            alert("删除失败");
                        }
                    })
                },
                search(keywords) {
                    var newList = [];
                    this.list.forEach(item => {
                        if (item.name.indexOf(keywords) != -1) {
                            newList.push(item);
                        }
                    });
                    return newList;
                }
            }
        });
    </script>
</body>
</html>
```

### 配置数据接口根域名

分全局配置和私有配置

```js
// 全局配置
Vue.http.options.root = 'http://www.liulongbin.top:3005/';
Vue.http.options.emulateJSON = true;// 全局启用 emulateJSON，这样 $http.post()中就不用写了
```

参考：https://github.com/pagekit/vue-resource/blob/develop/docs/config.md

注意：调用的时候，使用相对路径，即，前面不用 `/`，正确：`this.$http.get('api/getprodlist')`



# 附件-异常

## No 'Access-Control-Allow-Origin' header is present on the requested resource

前端页面请求后台接口时，

![](https://gitee.com/leafsing/pic/raw/master/img/20200709013844.png)

解决办法：后端 controller 添加 @CrossOrigin 注解

```java
@CrossOrigin
@Controller
@RequestMapping("/vue")
public class VueTestController {
    @RequestMapping("/get")
    public String testGet() {
        System.out.println("==========testGet==========");
        return "index";
    }
}
```
参考：https://blog.csdn.net/qq_37647296/article/details/89401499

## Cross-Origin Read Blocking (CORB)

前端发送 jsonp 请求的时候：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709013928.png)



# 跨域问题

## 为什么出现“跨域”问题

因为浏览器有 同源策略，是浏览器搞的鬼。是浏览器根据服务器返回的http头来判断的（参考：https://www.v2ex.com/t/203693）

但 同源策略 是浏览器最基本的安全机制。

所谓同源（即指在同一个域）就是两个页面具有相同的协议（protocol），主机（host）和端口号（port），缺一不可。

## 如何跨域

### 方法一：jsonp

- 总结：在页面中引入一段`<script src="http://localhost:91?callback=f">`代码，其中`f`是回调函数名称；后台接口返回的数据可能是：`f([{},{},...])`。
![](https://gitee.com/leafsing/pic/raw/master/img/20200709014005.png)
- 缺点：只能是 get 请求。——20190925

学习资料：https://www.bilibili.com/video/av26167402

示例，效果图：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709014022.png)

代码：

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>跨域问题</title>
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
        }
        ul{
            list-style-type: none;/*列表前面的 * 号去掉*/
        }
        img,input{
            display: block;
            margin: 30px auto;
        }
        #search{
            width: 550px;
            height: 30px;
            margin-bottom: 0;/*下边距*/
        }
        #sugList {
            display: none;
            width: 552px;
            margin: auto;
            border: 1px solid #000;
            border-top: 0;
        }
        #sugList ul li {
            line-height: 30px;
            text-indent: 20px;
        }
        #sugList ul li:hover {
            background: #333;
            color: #fff;
        }
    </style>
</head>
<body>
    <img src="../img/baidu.gif" width="260" alt="baidu">
    <input id="search" type="text">
    <div id="sugList">
        <ul>
            <!--<li>aa</li>
            <li>aa</li>
            <li>aa</li>
            <li>aa</li>
            <li>aa</li>-->
        </ul>
    </div>
    <script>
        var search = document.getElementById("search");
        var suglist = document.getElementById("sugList");
        var ul = document.querySelector("#sugList ul");
        search.onkeyup = function search() {
            var sugScript = document.createElement("script");
            sugScript.src = "https://www.baidu.com/sugrec?prod=pc&wd=" + this.value + "&cb=getSug";
            document.body.appendChild(sugScript);
        }
        function getSug(result) {
            var data = result.g;
            if(data != undefined){
                ul.innerHTML = "";// 删除原来的列表数据
                suglist.style.display = "block";
                data.forEach(function (el) {
                    var li = document.createElement("li");
                    li.innerText = el.q;//  el.q 是取出数据（要调试才知道）
                    ul.appendChild(li);

                    // 开始搜索
                    li.onclick = function () {
                        window.location.href = "https://www.baidu.com/s?wd=" + this.innerText;
                    }
                })
            }else{
                suglist.style.display = "none";
            }
        }
    </script>
</body>
</html>
```

代码在：https://github.com/wolf-lea/heima-vue/blob/master/day02/00.%E8%B7%A8%E5%9F%9F%E9%97%AE%E9%A2%98.html#L57


总结——**jsonp的原理**：只有通过 ajax 方式时才会有跨域问题，而通过 href 或 src 请求的 .js/css/img 等文件时，不会存在跨域。


### 方法二：CROS

在后台服务器端 response 对象上增加响应头`Access-Control-Allow-Origin`：

```
res.header("Access-Control-Allow-Origin","*");
```

在 Spring 4.x 中提供了 `@CrossOrigin` 注解，可以注解在类或方法上，原理也是添加响应头。


学习资料：https://www.bilibili.com/video/av60490895


### 方法三：nginx 反向代理

使用 nginx 反向代理



## 跨域参考文章
- https://www.jianshu.com/p/7ce1f8c7a04c
- https://blog.csdn.net/weixin_33743880/article/details/91423231
- https://blog.csdn.net/qq_38128179/article/details/84956552
- https://segmentfault.com/a/1190000015597029?utm_source=tag-newest
