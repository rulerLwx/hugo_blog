---
title: "Java-front-VUE-5-组件"
date: 2018-06-20T10:06:02+08:00
draft: true
categories: ["Java-front-end"]
---

# 组件

什么是组件：不同的组件划分不同的功能，需要什么样的功能就调用相应的组件。

- 模块化：从代码逻辑角度划分，方便代码分层开发，保证每个功能模块职能单一
- 组件化：从 UI 界面角度划分，方便 UI 组件重用

## 全局定义组件

官方文档：https://cn.vuejs.org/v2/guide/components-registration.html

### 方式一

```html
<body>
    <div id="app">
        <my-com1></my-com1>
    </div>
    <script>
        var com1 = Vue.extend({
            template:'<h3>这是使用 Vue.extend 创建的组件</h3>'
        });
        Vue.component('myCom1', com1);// myCom1：组件名称，com1：组件模板对象
        var vm = new Vue({
            el:'#app',
            data:{},
            methods:{}
        })
    </script>
</body>
```

- 使用`Vue.component('myCom1', com1);`命名的时候，可以使用驼峰，使用组件的时候，大写改成小写并在前面加 -。如 myCom1 ，调用：<my-com1></my-com1>
- 如果没有使用驼峰（全部小写），就用原名

另外，可以不用 com1 这个中间变量，直接将创建模板的代码放入 component 的第二个参数中。

### 方式二

```html
<body>
    <div id="app">
        <my-com1></my-com1>
    </div>
    <script>
        // 要有一个根元素来包裹，如 div
        Vue.component('myCom1', {
            template:'<div><h3>这是直接创建的组件</h3><span>112</span></div>'
        });
        var vm = new Vue({
            el:'#app',
            data:{},
            methods:{}
        })
    </script>
</body>
```

缺点：'' 里面的 html 元素写起来不方便，没有提示

### 方式三（推荐）

```html
<body>
    <div id="app">
        <!--使用定义的组件-->
        <my-com3/>
    </div>
    <!--在 app 外部定义一个组件-->
    <template id="tmp1">
        <div>
            <h1>这是通过 template 标签，在外部定义的组件</h1>
            <h4>不错，好用</h4>
        </div>
    </template>
    <script>
        Vue.component('myCom3', {
            template:'#tmp1' // 引用被控制的 #app 之外的 template
        });
        var vm = new Vue({
            el:'#app',
            data:{},
            methods:{}
        })
    </script>
</body>
```

### 总结

归根到底，组件创建只有一种 Vue.component('',{})，能变的只是第二参数 


## 定义私有组件

使用 components:{}，跟 data:{} 同级

```html
<body>
    <div id="app">
        <login></login>
    </div>
    <template id="tmp1">
        <div>
            <h5>全局组件h5</h5>
            <h1>全局组件h1</h1>
        </div>
    </template>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{},
            methods:{},
            components: {
                login:{
                    // template:'<h1>私有组件 h1</h1>'
                    template:'#tmp1'
                }
            }
        })
    </script>
</body>
```

## 总结

自定义组件，就是自定义一个标签，然后使用。

- 全局组件使用 Vue.component('',{})
- 私有组件使用 var com1 = {}


## 组件中的 data 和 methods

- 组件中可以有自己的 data 属性，但必须是一个 function 且返回一个对象
- 组件返回的对象最好是独立的，而不是公用一个全局对象。因为全局对象，所有数据共享，存在数据安全，**这是为什么要返回一个 {} 的原因**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>组件中的-data/methods-计数器</title>
    <script src="../js/vue.js"></script>
    <script src="../js/vue-resource.js"></script>
</head>
<body>
    <div id="app">
        <counter></counter>
        <hr/>
        <counter></counter>
        <hr/>
        <counter></counter>
    </div>
    <!--计数器组件-->
    <template id="temp1">
        <div>
            <input type="button" value="+1" @click="increment"/>
            <h3>{{count}}</h3>
        </div>
    </template>
    <script>
        // 定义一个对象
        // var dataObj = {count:0};// 组件不能用全局的变量，这是为什么要返回一个 {} 的原因
        Vue.component('counter', {
            template: '#temp1',
            data: function () {
                return {count: 0}
            },
            methods: {
                increment() {
                    this.count++;
                }
            }
        });
        var vm = new Vue({
            el:'#app',
            data:{},
            methods:{}
        })
    </script>
</body>
</html>
```

效果图：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709014759.png)

总结：在组件方法中直接使用 this 访问 data 中的 属性，如 this.count 。

## 组件切换

### 方式一

缺点：只能在两个组件间切换

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>两组件间切换</title>
    <script src="../js/vue.js"></script>
</head>
<body>
    <div id="app">
        <a href="" @click.prevent="flag=true">登录</a>
        <a href="" @click.prevent="flag=false">注册</a>

        <!--这里的 flag 不用取非-->
        <login v-if="flag"></login>
        <register v-else="flag"></register>
    </div>

    <script>
        Vue.component('login',{
            template:'<h3>登录组件</h3>'
        });
        Vue.component('register',{
            template:'<h3>注册组件</h3>'
        });
        var vm = new Vue({
            el:'#app',
            data:{
                flag:true
            },
            methods:{}
        })
    </script>
</body>
</html>
```

### 方式二（推荐）

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>组件切换方式二</title>
    <script src="../js/vue.js"></script>
</head>
<body>
    <div id="app">
        <a href="" @click.prevent="comName='login'">登录</a>
        <a href="" @click.prevent="comName='register'">注册</a>
        <!--component是一个占位符， is 是指定要展示的组件名称-->
        <component :is="comName"></component>
    </div>

    <script>
        Vue.component('login',{
            template:'<h3>登录组件</h3>'
        });
        Vue.component('register',{
            template:'<h3>注册组件</h3>'
        });
        var vm = new Vue({
            el:'#app',
            data:{
                comName:'login'
            },
            methods:{}
        })
    </script>
</body>
</html>
```

## 组件切换-动画

- 使用 transition 标签将组件包裹起来
- 写四个 css 类样式
- 在 transition 中使用 model 属性
    - out-in
    - in-out

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>组件切换-动画</title>
    <script src="../js/vue.js"></script>
    <style>
        .v-enter,
        .v-leave-to {
            opacity: 0;
            transform: translateX(150px);
        }
        .v-enter-active,
        .v-leave-active {
            transition: all 0.6s ease;
        }
    </style>
</head>
<body>
    <div id="app">
        <a href="" @click.prevent="comName='login'">登录</a>
        <a href="" @click.prevent="comName='register'">注册</a>
        <transition mode="out-in">
            <component :is="comName"></component>
        </transition>
    </div>

    <script>
        Vue.component('login',{
            template:'<h3>登录组件</h3>'
        });
        Vue.component('register',{
            template:'<h3>注册组件</h3>'
        });
        var vm = new Vue({
            el:'#app',
            data:{
                comName:'login'
            },
            methods:{}
        })
    </script>
</body>
</html>
```

## 父组件向子组件传值

- 子组件默认访问不到父组件中 data 中的数据
- 父组件在引用子组件时，可以能过**属性绑定**（v-bind）将 data 中的数据传给子组件，子组件需要在 props 数组中声明属性
- 子组件中 props 中所有的数据都是父组件传来的（父组件通过 v-bind 传值）
- 但，子组件中的 data 中的数据是自身私有的，比如，通过 vue-resource 请求回来的数据
- 子组件 props 中的数据只读，data 中的数据可读可写

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>组件-父组件向子组件传值</title>
    <script src="../js/vue.js"></script>
</head>
<body>
    <div id="app">
        <com1 v-bind:parentmsg="msg"></com1>
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{
                msg:'123-父组件中的msg'
            },
            methods:{},
            components:{
                // 子组件默认访问不到父组件中 data 中的数据
                com1:{
                    // 子组件需要声明属性，用于接收父组件的传值
                    props:['parentmsg'],
                    template:'<h1>这是一个子组件--- {{parentmsg}}</h1>'
                }
            }
        })
    </script>
</body>
</html>
```

vm 相当于一个父组件，components 相当于一个子组件


## 子组件通过事件调用向父组件传值

- 父组件向子组件传值，使用的是 v-on
- 子组件向父组件传值，是通过子组件的事件向父组件传值

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>组件-子组件通过事件调用向父组件传值</title>
    <script src="../js/vue.js"></script>
</head>
<body>
    <div id="app">
        <com2 @func123="show"></com2>
    </div>
    <template id="temp1">
        <div>
            <h1>这是子组件</h1>
            <input type="button" value="子组件按钮-调用父组件事件" @click="myclick"/>
        </div>
    </template>

    <script>
        // 定义一个字面量类型的组件模板对象
        // 子组件
        var com2 = {
            template:'#temp1',
            data:function () {
                return {
                    sunmsg:{name:'小头儿子',age:6}
                }
            },
            methods: {
                myclick(){
                    // 从第2个参数开始，可以传多个参数
                    this.$emit('func123',this.sunmsg);
                }
            }
        }
        var vm = new Vue({
            el:'#app',
            data:{
                msgFromSun:null
            },
            methods:{
                show(data){
                    this.msgFromSun = data;
                    console.log(this.msgFromSun)
                }
            },
            components: {
                com2,
            }
        })
    </script>
</body>
</html>
```

## 案例-评论列表

- 使用 var com1 = { template:'#temp1'} 定义组件，更好理解，js 代码会更好看
- 一个组件相当于一个小小的 vm ，**有其独立的** data/method/filters...
- 父子组件参数的传递：此案例没有传参，参考上一节

知识点：
- JSON.parse() 转换成 json
- JSON.stringify() 转成字符串
- (localStorage.getItem('a') || '[]') = []
- 数组添加元素方法：push/unshift，一个添加在结尾，一个添加在开头

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>组件-案例-评论列表</title>
    <script src="../js/vue.js"></script>
    <!--收入 css 时，不要忘记 rel 这个属性-->
    <link rel="stylesheet" href="../css/bootstrap.css"/>
</head>
<body>
    <div id="app">
        <commonbox @func="loadComments"></commonbox>
        <ul class="list-group">
            <li class="list-group-item" v-for="item in list" :key="item.id">
                {{ item.content }}
                <span class="badge">评论人：{{ item.user }}</span>
            </li>
        </ul>
    </div>
    <template id="temp1">
        <div>
            <div class="form-group">
                <label>评论人：</label>
                <input type="text" class="form-control" v-model="user">
            </div>
            <div class="form-group">
                <label>评论内容：</label>
                <textarea class="form-control" v-model="content"></textarea>
            </div>
            <div class="form-group">
                <input type="button" value="发表评论" class="btn btn-primary" @click="postComment">
            </div>
        </div>
    </template>
    <script>
        var commonbox = {
            template: '#temp1',
            data: function () {
                return {
                    user: '',
                    content: ''
                }
            },
            methods: {
                postComment() {
                    var comment = {id: Date.now(), user: this.user, content: this.content};
                    // 从 localStorage 中获取所有评论，如果获取的数据为 null ，则返回 []
                    // 原理： (localStorage.getItem('a') || '[]') = []
                    var list = JSON.parse(localStorage.getItem('cmts') || '[]');
                    list.unshift(comment);
                    // 重新保存最新的评论数据
                    localStorage.setItem('cmts', JSON.stringify(list));

                    this.user = this.content = '';
                    //调用父组件的方法
                    this.$emit('func');
                }
            }
        };
        var vm = new Vue({
            el:'#app',
            data:{
                list: [
                    {id: Date.now(), user: '李白', content: '天生我才必有用'}
                ]
            },
            methods:{
                // 从本地的 localStorage 中加载评论列表
                loadComments(){
                    var list = JSON.parse(localStorage.getItem('cmts') || '[]');
                    this.list = list;
                }
            },
            components:{
                commonbox
            },
            // 首次或刷新页面时，加载评论列表
            created(){
                this.loadComments();
            }
        })
    </script>
</body>
</html>
```

![](https://gitee.com/leafsing/pic/raw/master/img/20200709014859.png)

总结：
- localStorage 的使用
- 子组件调用父组件的方法
- JSON 的使用
- bootstrap 样式的使用
