---
title: "Java-front-CSS"
date: 2018-06-01T10:01:02+08:00
draft: true
tags: [ ]
categories: ["Java-front-end"]
---

# 概述

层叠样式表(Cascading Style Sheets)是一种用来表现HTML或XML等文件样式的计算机语言。

# css 与 html 结合的方式

## 方式一：style属性

```html
<div style="background-color: #e1ff4b;color: #e1ff4b"></div>
```

## 方式二：style标签

一般定义在head标签中

```html
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style type="text/css">
        div{
            background-color: bisque;color: red
        }
    </style>
</head>
<body>

<header>
    <div >这是一个div</div>
    <div >这是一个div2</div>
</header>
```

## 方式三：@import

```html
    <style type="text/css">
        @import "div.css";
    </style>
```

在 div.css 中也可以使用 `@import "div.css";`


## 方式四：<link>标签

```html
<body>
...
    <link rel="stylesheet" href="div.css">
...
</body>
```

**千万不要忘记** rel="stylesheet" 这个属性 ——20190928

css样式优先级：由上到下，由外到内，总之：后加载的为主。


# css 选择器

## html标签名选择器

## class选择器

指标签中使用的 class 属性

```html
    <style type="text/css">
        /*不指定容器*/
        .haha{
            
        }
        /*指定容器*/
        div.haha{
            
        }
    </style>
```

## id选择器

```html
    <style type="text/css">
        /*不指定容器*/
        #haha{
            
        }
        /*指定容器*/
        div#haha{
            
        }
    </style>
```

优先级高低：标签选择器 < 类选择器 < id选择器 < style属性

## 扩展选择器

1）关联选择器

html

```
<span><b>这是加粗文字</b></span>
```

css

```
span b {
    background-color: bisque;
    color: red
}
```

2）组合选择器

```
.haha,div b {
    
}
```

不同选择器之间有`,`隔开


3）伪元素选择器

伪元素用于向某些选择器设置特殊效果。如超链接访问前、鼠标悬停、点击、访问后等效果。

```html
    <style>
        a:link{
            
        }
        a:hover{

        }
        a:active{
            
        }
        a:visited{
            
        }
    </style>
```


# 布局

## 盒子模型

![](https://gitee.com/leafsing/pic/raw/master/img/20200709011609.png)
