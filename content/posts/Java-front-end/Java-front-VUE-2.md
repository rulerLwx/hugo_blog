---
title: "Java-front-VUE-2"
date: 2018-06-20T10:03:02+08:00
draft: true
categories: ["Java-front-end"]
---

# 事件

## 事件函数传参

在 Vue 中，使用事件绑定机制，为元素指定处理函数时，如果加了小括号，就可以给函数传参

```html
<a href="" @click.prevent="del(item.id)">删除</a>
```


# 品牌列表案例

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>品牌列表案例</title>
    <script src="../js/vue.js"></script>
    <link rel="stylesheet" href="../css/bootstrap-3.3.7.css">
</head>
<body>
    <div id="app">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">添加品牌</h3>
            </div>
            <div class="panel-body form-inline">
                <label>ID:<input type="text" class="form-control" v-model="id"></label>
                <label>Name:<input type="text" class="form-control" v-model="name"></label>
                <input type="button" value="添加" class="btn btn-primary" @click="add"/>
                <label>搜索名称关键字：<input type="text" class="form-control" v-model="keywords"/> </label>
            </div>
        </div>
        <table class="table table-bordered table-hover table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Ctime</th>
                <th>Operation</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="item in search2(keywords)" :key="item.id">
                <td>{{item.id}}</td>
                <td>{{item.name}}</td>
                <td>{{item.ctime}}</td>
                <td><a href="" @click.prevent="del2(item.id)">删除</a></td>
            </tr>
            </tbody>
        </table>
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{
                id:'',
                name:'',
                keywords:'',
                list:[
                    {id:1,name:'奔驰',ctime:new Date()},
                    {id:2,name:'宝马',ctime:new Date()},
                    {id:3,name:'现代',ctime:new Date()}
                ]
            },
            methods:{
                add(){
                    this.list.push({id: this.id, name: this.name, ctime: new Date()});
                    this.id = this.name = '';
                },
                // 删除方式一
                del(id){
                    // some() 用于循环数组
                    this.list.some((item,i) => {
                        if (item.id == id) {
                            // 调用数组的 splice() 方法删除元素
                            this.list.splice(i, 1);
                            return true;//返回 true，终止这个数组的后续循环
                        }
                    });
                },
                // 删除方式二
                del2(id){
                    var index = this.list.findIndex(item => {
                        if (item.id == id) {
                            return true;
                        }
                    });
                    this.list.splice(index, 1);
                },
                search(keywords){
                    var newList = [];
                    this.list.forEach(item => {
                        if (item.name.indexOf(keywords) != -1) {
                            newList.push(item);
                        }
                    });
                    return newList;
                    // 为什么 keywords='' 时，返回的是完整的 list ？
                    // 看示例："abc".indexOf('') = 0
                    // 因为 不等于 -1，所有都加入新的数组中
                },
                search2(keywords){
                    // 注意：forEach some filter findIndex 这些都属于数组的新方法
                    // 都会对数组的每一项进行遍历，执行相关操作
                    // filter() 是 es6 提供的方法
                    var newList = this.list.filter(item =>{
                        if (item.name.includes(keywords)) {
                            return 1;// 随便返回一个值，即：有返回值即可
                            // return true;
                        }
                    });
                    return newList;
                }
            }
        })
    </script>
</body>
</html>
```

- ES6提供的新方法`String.prototype.includes(要包含的字符串)`，包含返回 true，否则返回 false
- 

# 过滤器

Vue.js 允许你自定义过滤器，**可被用作一些常见的文本格式化**。过滤器可以用在两个地方：**mustache 插值和 v-bind 表达式**。过滤器应该被添加在 JavaScript 表达式的尾部，由“管道”符指示：

- 在双花括号中：`{{ message | 过滤器名称 }}`
- 在 v-bind 中：`<div v-bind:id="rawId | 过滤器名称"></div>`

## 全局过滤器

在**创建 Vue 实例之前**定义全局过滤器。全局的过滤器**可以应用于不同的 vm 实例**，即：共享。

示例：

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>过滤器</title>
    <script src="../js/vue.js"></script>
</head>
<body>
    <div id="app">
        <p>{{msg|msgFormat('aa','bb')}}</p>
    </div>
    <script>
        // 第一个参数规定是过滤器 {{msg|msgFormat('aa','bb')}} “管道”前面的值：msg
        // 之后的0个或多个是过滤器入参：msgFormat('aa','bb')
        Vue.filter("msgFormat", function (msg, arg1, arg2) {
            return msg.replace(/单纯/g, arg1 + arg2);
        });
        var vm = new Vue({
            el:'#app',
            data:{
                msg:'曾经，我也是单纯的少年，单纯的我，傻傻的问，谁是世界上最单纯的人'
            },
            methods:{}
        })
    </script>
</body>
</html>
```

过滤器可以串联：{{ message | filterA | filterB }}


## 私有过滤器

一个页面可以有多个 vm 实例，在 vm 实例里面定义的过滤器就是私有过滤器

```js
    var vm2 = new Vue({
       el:'#app2',
       data: {},
       methods:{},
       filters:{
           dateFormat:function (dateStr,pattern="") {
               
           }
       }
    });
```

当全局过滤器和局部过滤器重名时，会采用局部过滤器，“就近原则”。

知识点：
在es6中，可以使用 
- String.padStart(maxLength: number,fillString?: string)
- String.padEnd(maxLength: number,fillString?: string) 

在字符串的前面或后面补充给定的字符，例如，时间，分钟是个位数时，前面补0：`var min = dt.getMinutes().toString().padStart(2,0);`

解释：总长度2，不足2位时，前面补0，如 06:03:08


# 按键修饰符

## 常用按键修饰符

例如，按 Enter 键时触发“添加”操作方法：`<input type="text" class="form-control" v-model="name" @keyup.enter="add">`

Vue 提供了绝大多数常用的按键码的别名：
- .enter
- .tab
- .delete (捕获“删除”和“退格”键)
- .esc
- .space
- .up
- .down
- .left
- .right

## 自定义按键修饰

通过 **全局**（跟 new Vue 同级） config.keyCodes 对象自定义按键修饰符别名：

Vue.config.keyCodes.f1 = 112; // 可以使用 `v-on:keyup.f1`

对应的键盘码值参考《附件-相关文章》


# 自定义全局指令

使用`Vue.directive()`定义全局指令

注意：vue 中所有的指令，在调用时都以 v- 开头

## 示例

```html
<input type="text" class="form-control" v-model="keywords" v-focus v-color="'red'"/> 
```

```js
    // 全局的定义
    // 光标锁定
    Vue.directive('focus',{
        // 第一个参数 el：表示被绑定了指令的那个元素，是一个原生 js 对象
        bind:function (el) {
    
        },
        inserted:function (el) {
            el.focus();
        },
        update:function () {
    
        }
    });
    // 字体颜色
    Vue.directive('color', {
        bind: function (el,binding) {
            // 内联样式
            // el.style.color = 'red';// 不应该写死，参数应该从页面传来
            el.style.color = binding.value;
        }
    });
```

这三个方法有对应的执行时机，系统内置的（规定好了的），参考下一节


## 勾子函数

- `bind:function(el){}` 每当指令绑定到元素上时（这个时候还没有插入到dom中），会执行 bind 函数，只执行1次
- `inserted:function(el){}` 元素插入到 dom 中时，触发1次
- `updated:function(){el}` 当 VNode 时，可能会触发多冷

如果有参数，那么，**第一个参考永远是 el，表示被绑定指令的那个元素，el 是原生 js 对象**，可以执行原生的 js 方法。

参考官网：https://cn.vuejs.org/v2/guide/custom-directive.html

总结：
- bind 的执行时机比 inserted 早
- 跟样式相关的操作，一般可以在 bind 中执行
- 跟 js 相关的操作，一般可以在 inserted 中执行

## 勾子函数参数

```
    Vue.directive('color', {
        bind: function (el,binding) {
            el.style.color = binding.value;
            console.log(binding.value)// red
            console.log(binding.expression)// 'red'
        }
    });
```

常用的勾子函数参数、属性：
- el 第一个参数
- binding 第二个参数
    - value 属性
    - expression 属性

参考官网：https://cn.vuejs.org/v2/guide/custom-directive.html#%E9%92%A9%E5%AD%90%E5%87%BD%E6%95%B0%E5%8F%82%E6%95%B0

总结：理解参数的原理，参数的位置不能变，但，名字可以改。如：第二个参数可以写成 binding123456 。


## 私有-自定义指令


```
    <div id="app2">
        <label v-fontweight="'initial'">这是一行文字</label>
    </div>
    <script>
        var vm2 = new Vue({
           el:'#app2',
           data: {},
           methods:{},
           filters:{
           },
            directives:{
               'fontweight':{
                   bind: function (el, binding) {
                       el.style.fontWeight = binding.value;
                   }
               }
            }
        });
    </script>
```

## 指令函数简写

在 bind 和 update 时，可以简写（暗示 inserted 不能简写？？？）

全局指令

```js
    Vue.directive('color-swatch', function (el, binding) {
      el.style.backgroundColor = binding.value
    })
```


私有指令
```html
    <div id="app2">
        <label v-fontweight="'initial'" v-fontsize="'30px'">这是一行文字</label>
    </div>
    <script>
        var vm2 = new Vue({
           el:'#app2',
           data: {},
           methods:{},
           filters:{
               dateFormat:function (dateStr,pattern="") {
                   dateStr.padStart()
               }
           },
            directives:{
               'fontweight':{
                   bind: function (el, binding) {
                       el.style.fontWeight = binding.value;
                   }
               },
                // 指令简写，只应用在 bind/update 阶段
                'fontsize':function (el,binding) {
                   console.log(binding.value)
                    el.style.fontSize = parseInt(binding.value) + 'px';
                }
            }
        });
    </script>
```

知识点：parseInt()方法可以去掉 px ，而只剩下数字。也就是说，v-fontsize="'30px'" 与 v-fontsize="'30'" 等价 ——20190924

参考：https://cn.vuejs.org/v2/guide/custom-directive.html#%E5%87%BD%E6%95%B0%E7%AE%80%E5%86%99


# vue 生命周期

## 什么是生命周期

从Vue实例创建、运行、到销毁期间，总是伴随着各种各样的事件，这些事件，统称为生命周期！

换句话说，生命周期就是事件。事件有创建和销毁。另一种，生命周期钩子 = 生命周期函数 = 生命周期事件

## 生命周期函数分类

分三类：

- 创建期间的生命周期函数：
    + beforeCreate：实例刚在内存中被创建出来，此时，还没有初始化好 data 和 methods 属性
    + created：实例已经在内存中创建OK，此时 data 和 methods 已经创建OK，此时还没有开始 编译模板
    + beforeMount：此时已经完成了模板的编译，但是还没有挂载到页面中
    + mounted：此时，已经将编译好的模板，挂载到了页面指定的容器中显示
- 运行期间的生命周期函数：
    + beforeUpdate：状态更新之前执行此函数， 此时 data 中的状态值是最新的，但是界面上显示的 数据还是旧的，因为此时还没有开始重新渲染DOM节点
    + updated：实例更新完毕之后调用此函数，此时 data 中的状态值 和 界面上显示的数据，都已经完成了更新，界面已经被重新渲染好了！
- 销毁期间的生命周期函数：
    + beforeDestroy：实例销毁之前调用。在这一步，实例仍然完全可用。
    + destroyed：Vue 实例销毁后调用。调用后，Vue 实例指示的所有东西都会解绑定，所有的事件监听器会被移除，所有的子实例也会被销毁。

总结：
- 以上生命周期，**针对的是 vue 实例**，而不是某个具体事件
- 如果要调用 methods 中的方法或操作 data 中的数据，最早只能在 created 中操作
- 如果要操作页面上的 dom 节点，最早要在 mounted 中进行

![](https://gitee.com/leafsing/pic/raw/master/img/20200709013251.png)

官网参考：https://cn.vuejs.org/v2/guide/instance.html#%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F%E5%9B%BE%E7%A4%BA

## 生命周期的应用

**vue 实例**的生命周期怎么使用？写在哪些地方？

```html
<body>
    <div id="app">
    </div>
    <script>
        var vm = new Vue({
            el:'#app',
            data:{},
            methods:{},
            beforeCreate(){
                console.log("beforeCreate");
            },
            created(){
                console.log("created()")
            },
            beforeMount(){},
            mounted(){},
            updated(){},
            beforeDestroy(){},
            destroyed(){}
        })
    </script>
</body>
```

总结：vue 实例的生命周期方法跟 el/data/methods 等，同级。


# 附件-相关文章
1. [vue.js 1.x 文档](https://v1-cn.vuejs.org/)
2. [vue.js 2.x 文档](https://cn.vuejs.org/)
3. [String.prototype.padStart(maxLength, fillString)](http://www.css88.com/archives/7715)
4. [js 里面的键盘事件对应的键码](http://www.cnblogs.com/wuhua1/p/6686237.html)
5. [pagekit/vue-resource](https://github.com/pagekit/vue-resource)
6. [navicat如何导入sql文件和导出sql文件](https://jingyan.baidu.com/article/a65957f4976aad24e67f9b9b.html)
7. [贝塞尔在线生成器](http://cubic-bezier.com/#.4,-0.3,1,.33)