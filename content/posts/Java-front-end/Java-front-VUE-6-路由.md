---
title: "Java-front-VUE-6-路由"
date: 2018-06-20T10:07:02+08:00
draft: true
categories: ["Java-front-end"]
---


# ref-获取元素、组件引用

目的：不用像`document.getElementById('#id')`一样操作dom

想获取哪个元素或标签，就在哪个元素上加上 `ref="唯一的名字"`，然后`this.$refs.xx.值`

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ref-获取dom元素和组件</title>
    <script src="../js/vue.js"></script>
    <link rel="stylesheet" href="../css/bootstrap.css"/>
</head>
<body>
    <div id="app">
        <input type="button" value="获取元素" @click="getElement" ref="mybtn"/>
        <h3 id="myh3" ref="myh3">这是一个H3</h3>

        <!--子组件-->
        <login ref="mylogin"></login>
    </div>
    <template id="temp1">
        <h1 ref="sunh1">这是一个登录组件</h1>
    </template>
    <script>
        var com1 = {
            template: '#temp1',
            data: function () {
                return {
                    msg:'sum msg'
                }
            },
            methods: {
                show(){
                    console.log("子组件 show()")
                }
            }
        }
        var vm = new Vue({
            el:'#app',
            data:{},
            methods:{
                getElement(){
                    // 访问元素（值）
                    console.log(this.$refs.myh3.innerText);
                    // 访问子组件的数据、方法
                    console.log(this.$refs.mylogin.msg);
                    this.$refs.mylogin.show();
                    
                    // 父组件可以拿到子组件元素
                    console.log(this.$refs.mylogin.$refs.sunh1.innerText);
                }
            },
            components:{
                login: com1
            }
        })
    </script>
</body>
</html>
```

在父组件中获取父组件的元素、子组件的 data/method。在子组件中的元素一样可以定义 ref。

思路：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709015121.png)

总结：使用 ref 的方式比从父组件传递方法给子组件的方法更容易使用。——20190929

代码：https://github.com/wolf-lea/heima-vue/blob/master/day04/04.ref-%E8%8E%B7%E5%8F%96dom%E5%85%83%E7%B4%A0%E5%92%8C%E7%BB%84%E4%BB%B6.html


# 路由

官网：
- https://github.com/vuejs/vue-router
- https://router.vuejs.org/zh/

## 什么是路由

- 后端路由：前端的所有资源都对应后台的一个 URL 地址，请求时，通过后台进行分发
- 前端路由：**单页面**内容之间跳转，锚点，通过 hash(#号) 实现

## 基本使用

安装方式跟 vue-resource 一样

- 导入 vue-router.js 后，在 window 全局对象中，就有了一个路由构造函数 VueRouter
- new 路由对象的时候，可以传递一个配置**对象**，也叫【路由匹配规则】

步骤：
1. 导包
2. 创建模板对象
3. new VueRouter ，写路由规则
4. 将 vueroute 跟 vm 关联
5. 在 #app 中使用 router-view 占位符，在路径前加上 # 号（使用 a 标签时要加 #，使用 router-link 标签则不用）

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>路由-基本使用</title>
    <!--导入包，注意顺序-->
    <script src="../js/vue.js"></script>
    <script src="../js/vue-router.js"></script>
</head>
<body>
    <div id="app">
        <a href="#/login">登录</a>
        <a href="#/register">注册</a>
        <!-- vue-router 提供的占位符，根据路由规则，匹配到的元素就会展示在这里-->
        <router-view></router-view>
    </div>
    <script>
        // 模板对象
        var login = {
            template:'<h2>这是登录组件</h2>'
        }
        var register = {
            template:'<h2>这是注册组件</h2>'
        }
        var vueRouter = new VueRouter({
            // 路由匹配规则
            routes:[
                // 有两个固定属性，path：路由地址，component：对应的组件对象（不是名称）
                {path:'/login',component:login}, // 注意，如果定成 component:'login' ，是错误的
                {path:'/register',component: register}
            ]
        });
        var vm = new Vue({
            el:'#app',
            data:{},
            methods:{},
            // 将路由对象关联到 vm
            router:vueRouter
        })
    </script>
</body>
</html>
```

另外，可以使用 router-link 标签代替上面的 a 标签，还可以通过 tag="" 指定渲染成什么标签，如 

```html
        <router-link to="/login" tag="span">登录2</router-link>
        <router-link to="/register">注册2</router-link>
```

## 路由重定向

可以在路由规则里使用重定向，跳转到指定页面

```html
        var vueRouter = new VueRouter({
            routes:[
                {path:'/login',component:login}, 
                {path:'/register',component: register},
                {path:'/',redirect:'/login'} // 重定向
            ]
        });
```

## 选中的路由高亮显示

方法一：重写系统默认的 css 类即可

思路：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709015724.png)

重写 css 类：
```html
    <style>
        .router-link-active {
            color: red;
            font-weight: 800;
            font-size: 80px;
            font-style: italic;
            text-decoration: underline;
        }

    </style>
```

效果：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709015814.png)

方法二：自定义 css 类名称

默认 css **类的名称**是可以修改的，参考：https://router.vuejs.org/zh/api/#active-class

```js
        var vueRouter = new VueRouter({
            routes:[],
            linkActiveClass:myactive // myactive 将会替换上面的 .router-link-active
        });
```

## 路由-动画

步骤：
1. 用 transition 将占位符包裹起来
2. 写四个 css 类

```html
        <transition mode="out-in">
            <router-view></router-view>
        </transition>
```

## 路由传参-通过query获取get请求参数

- 在使用 router-link 标签 to 属性时，可以在请求地址后面加上请求参数，如：`<router-link to="/login?id=10">登录</router-link>`
- 使用`this.$route.query.xx`拿到 get 请求参数的值，`this` 关键字可以省略

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>路由规则中定义参数</title>
    <script src="../js/vue.js"></script>
    <script src="../js/vue-router.js"></script>
</head>
<body>
    <div id="app">
        <router-link to="/login?id=10&name=zs">登录</router-link>
        <router-link to="/register">注册</router-link>

        <router-view></router-view>
    </div>
    <script>
        var login = {
            template:'<h2>这是登录组件：{{this.$route.query.id}} --- {{$route.query.name}}</h2>',
            // 组件生命周期钩子函数
            // 可以打印 this.$route ，据此提供思路来取值
            created(){
                console.log(this.$route.query.id);
            }
        }
        var register = {
            template:'<h2>这是注册组件</h2>'
        }
        var vueRouter = new VueRouter({
            routes:[
                {path:'/login',component:login},
                {path:'/register',component: register}
            ]
        });
        var vm = new Vue({
            el:'#app',
            data:{},
            methods:{},
            router:vueRouter// 如果属性名、属性值都是 router，可以简写成 router
        })
    </script>
</body>
</html>
```

## 路由传参-通过params方式（restful）

restful 风格传参

1. 路由规则的路径的后面跟`/：xx`
2. 在 router-link 标签 to 属性路径后面带上 rest 风格的参数
3. 通过 this.$route.params.xx 取得参数值

思路：打印 this.$route（`console.log(this.$route)`） ，观察 params 属性

![](https://gitee.com/leafsing/pic/raw/master/img/20200709015848.png)

示例代码：

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>路由传参-params</title>
    <script src="../js/vue.js"></script>
    <script src="../js/vue-router.js"></script>
</head>
<body>
    <div id="app">
        <router-link to="/login/10">登录</router-link>
        <router-link to="/register/03/ls">注册</router-link>

        <router-view></router-view>
    </div>
    <script>
        var login = {
            template:'<h2>这是登录组件 {{$route.params.id}}</h2>',
        }
        var register = {
            template:'<h2>这是注册组件 {{$route.params.id}}---{{$route.params.name}}</h2>'
        }
        var vueRouter = new VueRouter({
            routes:[
                {path:'/login/:id',component:login},
                {path:'/register/:id/:name',component: register}
            ]
        });
        var vm = new Vue({
            el:'#app',
            data:{},
            methods:{},
            router:vueRouter,
            created(){
                console.log(this.$route)
                console.log(this.$route.params.id)
            }
        })
    </script>
</body>
</html>
```

## 路由嵌套-children

组件里面有子组件，点击子组件，父组件不消失

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>children-路由嵌套</title>
    <script src="../js/vue.js"></script>
    <script src="../js/vue-router.js"></script>
</head>
<body>
    <div id="app">
        <router-link to="/account">Account 组件</router-link>

        <router-view></router-view>
    </div>
    <template id="temp1">
        <div>
            <h1>这是 Account 组件</h1>
            <!--这里要加上 /account 前缀-->
            <router-link to="/account/login">登录</router-link>
            <router-link to="/account/register">注册</router-link>
        </div>
    </template>
    <script>
        var account = {
            template:'#temp1',
        }
        var login = {
            template:'<h3>登录</h3>',
        }
        var register = {
            template:'<h3>注册</h3>',
        }
        var router = new VueRouter({
            routes:[
                {
                    path:'/account',
                    component:account,
                    children:[
                        // path 不加斜线
                        {path:'login',component:login},
                        {path:'register',component:register}
                    ]
                }
            ]
        });
        var vm = new Vue({
            el:'#app',
            data:{},
            methods:{},
            router:router
        })
    </script>
</body>
</html>
```

效果图：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709015930.png)


## 路由-命名视图-实现经典布局

思路：
- 给 router-view 标签命名
- 在路由规则中使用 components 属性（注意，比 component 多了 s）
- 然后使用 css 控制 router-view 的布局

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>命名视图-实现经典布局</title>
    <script src="../js/vue.js"></script>
    <script src="../js/vue-router.js"></script>
    <style>
        html,body {
            margin: 0;
            padding: 0;
        }
        h1 {
            margin: 0;
            padding: 0;
            font-size: 16px;
        }
        .header {
            background-color: orange;
            height: 80px;
        }
        .container {
            display: flex;
            height: 600px;
        }
        .left {
            background-color: lightgreen;
            flex: 2;
        }
        .main {
            background-color: lightpink;
            flex: 8;
        }
    </style>
</head>
<body>
    <div id="app">
        <router-view></router-view>
        <div class="container">
            <router-view name="left"></router-view>
            <router-view name="main"></router-view>
        </div>
    </div>
    <script>
        var header = {
            template:'<h1 class="header">这是 HEADER</h1>'
        }
        var leftBox = {
            template:'<h1 class="left">这是 leftBox 侧边栏</h1>'
        }
        var mainBox = {
            template:'<h1 class="main">这是 mainBox 主体区域</h1>'
        }
        var router = new VueRouter({
            routes:[
                {
                    path:'/',
                    components:{
                        default: header,
                        left: leftBox,
                        main:mainBox
                    }
                }
            ]
        });
        var vm = new Vue({
            el:'#app',
            data:{},
            methods:{},
            router
        })
    </script>
</body>
</html>
```

效果图：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709020010.png)


## 附件-相关文章

- [URL中的hash（井号）](http://www.cnblogs.com/joyho/articles/4430148.html)
