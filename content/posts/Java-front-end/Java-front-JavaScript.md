---
title: "Java-front-JavaScript"
date: 2018-06-01T10:02:02+08:00
draft: true
tags: [ ]
categories: ["Java-front-end"]
---

# ECMAScript & JavaScript

>ECMAScript是一种由Ecma国际（前身为欧洲计算机制造商协会,英文名称是European Computer Manufacturers Association）通过ECMA-262标准化的脚本程序设计语言。这种语言在万维网上应用广泛，它往往被称为JavaScript或JScript，但实际上后两者是ECMA-262标准的实现和扩展。----[百度百科][3]

>查看w3school的[javascript对象][2]手册 ，会有很大帮助。

JavaScript 是 ECMAScript 规范的一种实现。

# 基础知识

## js脚本位置
-  HTML中的脚本必须位于`<script>`和`</script>` 之间，那些老旧的实例可能会在 `<script>` 标签中使用 type="text/javascript"。现在已经不必这样做了。JavaScript 是所有现代浏览器以及 HTML5 中的默认脚本语言。

- 脚本可被放置在HTML页面的`<body>`和`<head>`中，或者同时存在于两个部分中。通常的做法是把函数放入 `<head>` 部分中，或者放在页面底部。这样就可以把它们安置到同一处位置，不会干扰页面的内容。

- 把脚本放在body中和head中，有什么区别吗？

>- 在HTML body部分中的JavaScripts会在页面加载的时候被执行。
>- 在HTML head部分中的JavaScripts会在被调用的时候才执行。
>---- [Js放在head和body中的区别][1]

## js数据类型

字符串、数字、布尔、数组、对象、Null、Undefined。

- JavaScript 只有一种数字类型。数字可以带小数点，也可以不带。
- 极大或极小的数字可以通过科学（指数）计数法来书写

```javascript
    var y=123e5;      // 12300000
    var z=123e-5;     // 0.00123
```

- 数组

```javascript
    var cars=new Array();
    cars[0]="Audi";
    cars[1]="BMW";
    cars[2]="Volvo";
    或
    var cars=new Array("Audi","BMW","Volvo");
    或
    var cars=["Audi","BMW","Volvo"];
```

数组下标是基于零的，所以第一个项目是 [0]，第二个是 [1]，以此类推

- 对象

对象由花括号分隔。在括号内部，对象的属性以名称和值对的形式 (name : value) 来定义。属性由逗号分隔：

```javascript
    var person={firstname:"Bill", lastname:"Gates", id:5566};
    或
    var person={
        firstname : "Bill",
        lastname  : "Gates",
        id        :  5566
    };
```

对象属性有两种寻址方式：

```javascript
    name=person.lastname;
    name=person["lastname"];
```

- 声明变量类型

也可以采用声明式来创建数据类型：

```javascript
    var carname=new String;
    var x=      new Number;
    var y=      new Boolean;
    var cars=   new Array;
    var person= new Object;
```

## 比较 & 运算符

- 比较运算符,...
- 逻辑运算符,...
- 条件运算符，`variablename=(condition)?value1:value2 `

## If...Else  & Switch 语句

```js
    if (条件)
      {
        当条件为 true 时执行的代码
      }
    else
      {
        当条件不为 true 时执行的代码
      }

      或：

    if (条件 1)
      {
        当条件 1 为 true 时执行的代码
      }
    else if (条件 2)
      {
        当条件 2 为 true 时执行的代码
      }
    else
      {
        当条件 1 和 条件 2 都不为 true 时执行的代码
      }

    switch(n)
    {
        case 1:
          执行代码块 1
          break;
        case 2:
          执行代码块 2
          break;
        default:
          n 与 case 1 和 case 2 不同时执行的代码
    }
```

## For & While 循环

```js
    for (var i=0;i<cars.length;i++)
    {
        document.write(cars[i] + "<br>");
    }
    或
    var person={fname:"John",lname:"Doe",age:25};
    for (x in person)
    {
      txt=txt + person[x];
    }

    while (条件)
    {
      需要执行的代码
    }
    或
    do
    {
      需要执行的代码
    }
    while (条件);
```

-break 语句用于跳出循环。  
-continue 用于跳过循环中的一个迭代。

**JavaScript 标签**:如需标记 JavaScript 语句，请在语句之前加上冒号

```js
    语法：
    label:
    语句

    break labelname;
    continue labelname;

-continue 语句（带有或不带标签引用）只能用在循环中。
-break 语句（不带标签引用），只能用在循环或 switch 中。
-通过标签引用，break 语句可用于跳出任何 JavaScript 代码块
```

## 异常处理

```js
    语法：
    try
    {
        //在这里运行代码
    }
    catch(err)
    {
        //在这里处理错误
    }

```

代码片段：

```html
<!DOCTYPE html>
<html>
<head>
<script>
    var txt="";
    function message()
    {
    try
      {
      adddlert("Welcome guest!");
      }
    catch(err)
      {
      txt="There was an error on this page.\n\n";
      txt+="Error description: " + err.message + "\n\n";
      txt+="Click OK to continue.\n\n";
      alert(txt);
      }
    }
</script>
</head>

<body>
<input type="button" value="View message" onclick="message()">
</body>

</html>
```

如果把 throw 与 try 和 catch 一起使用，那么您能够控制程序流，并生成自定义的错误消息。

```js
    语法：
    throw exception
```

```javascript
<script>
    function myFunction()
    {
        try
        {
          var x=document.getElementById("demo").value;
          if(x=="")    throw "empty";
          if(isNaN(x)) throw "not a number";
          if(x>10)     throw "too high";
          if(x<5)      throw "too low";
        }
        catch(err)
        {
          var y=document.getElementById("mess");
          y.innerHTML="Error: " + err + ".";
        }
    }
<script>
```

可以自定义异常哦！

## 增加 & 删除 元素

- 如需向 HTML DOM 添加新元素，您必须首先创建该元素（元素节点），然后向一个已存在的元素追加该元素。

```html
<div id="div1">
<p id="p1">这是一个段落</p>
<p id="p2">这是另一个段落</p>
</div>

<script>
var para=document.createElement("p");
var node=document.createTextNode("这是新段落。");
para.appendChild(node);

var element=document.getElementById("div1");
element.appendChild(para);
</script>
```

- 如需删除 HTML 元素，您必须首先获得该元素的父元素

```js
    var child=document.getElementById("p1");
    child.parentNode.removeChild(child);
```

# 对象

## JS 对象

>JavaScript 中的所有事物都是对象：字符串、数字、数组、日期，等等,对象是拥有属性和方法的数据。

实际上，`var txt = "Hello";`就已经创建了一个 JavaScript 字符串对象,此对象拥有相应的属性和方法，如`txt.length = 5`,`txt.indexOf()`方法。

创建一个对象有两种方式：

- 直接声明法，如 var txt = "Hello";
- new Object() 法，如

```js
    person=new Object();
    person.firstname="Bill";
    person.lastname="Gates";
    person.age=56;
    person.eyecolor="blue";
```

-访问对象的属性

```js
    语法：
    objectName.propertyName

    如：
    var message="Hello World!";
    var x=message.length;
```

-访问对象的方法

```js
    语法：
    objectName.methodName()

    如：
    var message="Hello world!";
    var x=message.toUpperCase();    
```

---



### 自定义对象

创建自定义对象有三种不同的方法：

- ① 定义并创建对象的实例
- ② 使用函数来定义对象，然后创建新的对象实例
- ③ Object.create()方法

每一种创建方式继承的原型对象都不同：

- ① 对象直接量：原型为Object.prototype。
- ② new 构造函数：原型为构造函数的prototype属性。
- ③ Object.create()：原型为传入的第一个参数，若第一个参数为null，以Object.prototype为原型。---[JavaScript 自定义对象][5]

#### 创建直接的实例

```js
    person=new Object();
    person.firstname="Bill";
    person.lastname="Gates";
    person.age=56;
    person.eyecolor="blue";
    或
    person={firstname:"John",lastname:"Doe",age:50,eyecolor:"blue"};
```

#### 使用对象构造器

使用函数来构造对象

```js
    function person(firstname,lastname,age,eyecolor)
    {
        this.firstname=firstname;
        this.lastname=lastname;
        this.age=age;
        this.eyecolor=eyecolor;
    }
```

一旦您有了对象构造器，就可以创建新的对象实例，就像这样：

```js
    var myFather=new person("Bill","Gates",56,"blue");
    var myMother=new person("Steve","Jobs",48,"green");
```

向已有对象添加新属性：

```js
    person.firstname="Bill";
    person.lastname="Gates";
    person.age=56;
    person.eyecolor="blue";
```

把方法添加到对象（在构造器函数内部定义对象的方法）：

```javascript
    function person(firstname,lastname,age,eyecolor)
    {
        this.firstname=firstname;
        this.lastname=lastname;
        this.age=age;
        this.eyecolor=eyecolor;

        this.changeName=changeName;

        function changeName(name)
        {
            this.lastname=name;
        }
    }
```

*注意*：
JavaScript 是面向对象的语言，但 JavaScript 不使用类。
在 JavaScript 中，不会创建类，也不会通过类来创建对象（就像在其他面向对象的语言中那样）。
JavaScript 基于 prototype，而不是基于类的。

#### prototype & _proto_ 属性

>prototype 属性使您有能力向对象添加属性和方法。

```javascript
    <script type="text/javascript">

        function employee(name,job,born)
        {
            this.name=name;
            this.job=job;
            this.born=born;
        }

        var bill=new employee("Bill Gates","Engineer",1985);

        employee.prototype.salary=null;
        bill.salary=20000;

        document.write(bill.salary);

    <script>
```

>1. __proto__是每个对象都有的一个属性，而prototype是函数才会有的属性。
>2. __proto__指向的是当前对象的原型对象，而prototype指向的，是以当前函数作为构造函数构造出来的对象的原型对象。---[知乎][4]

顶层的原型是Object.prototype。

参看:[JavaScript 自定义对象][5],[js中__proto__和prototype的区别和关系][4]

*因此，通过设置prototype可以实现js之间的继承关系。*

#### constructor 属性

>constructor 属性返回对创建此对象的 xxx 函数的引用。

```javascript
    <script type="text/javascript">

        var test=new Date();

        if (test.constructor==Array)
        {
            document.write("This is an Array");
        }
        if (test.constructor==Boolean)
        {
            document.write("This is a Boolean");
        }
        if (test.constructor==Date)
        {
            document.write("This is a Date");
        }
        if (test.constructor==String)
        {
            document.write("This is a String");
        }

    <script>
```

## Browser 对象

### window

Window 对象表示一个浏览器窗口或一个框架。在客户端 JavaScript 中，Window 对象是全局对象，所有的表达式都在当前的环境中计算。

例如，可以只写 document，而不必写 window.document。

### document

## HTML DOM 对象
## HTML 对象



# 函数

>函数是由事件驱动的或者当它被调用时执行的可重复使用的代码块。

## 语法

```js
    语法：
    function functionname()
    {
        这里是要执行的代码
    }
    或
    function myFunction(var1,var2)
    {
        这里是要执行的代码
    }
```

function是关键词。函数可以有返回值，用return返回。

## 变量 & 生命期

- 函数局部变量：声明在函数内部的变量（使用var）是局部变量，函数运行结束，局部变量就会被删除。
- 全局变量：声明在函数外的变量（使用var）是全局变量，页面关闭后，全局变量就被删除。
- 另外，赋值给未声明的变量（没有使用var），不管是函数内还是外，该变量将被自动作为全局变量。

document.write() 仅仅向文档输出写内容。如果在文档已完成加载后执行 document.write("我的第一段 JavaScript")，整个 HTML 页面将被覆盖。



# 事件

## onload & onunload

- onload 和 onunload 事件会在用户进入或离开页面时被触发。
- onload 事件可用于检测访问者的浏览器类型和浏览器版本，并基于这些信息来加载网页的正确版本。
- onload 和 onunload 事件可用于处理 cookie。

```js
    window.onload = function () {}
```

## onchange 

- onchange 事件常结合对输入字段的验证来使用

## onmouseover & onmouseout

onmouseover 和 onmouseout 事件可用于在用户的鼠标移至 HTML 元素上方或移出元素时触发函数

## onmousedown & onmouseup & onclick

当点击鼠标按钮时，会触发 onmousedown 事件，当释放鼠标按钮时，会触发 onmouseup 事件，最后，当完成鼠标点击时，会触发 onclick 事件。

## 疑问

- onchange onclick执行的先后顺序
- typeof




# this

```js
JS 里的 this，有以下特点：

1、在 function 内部被创建
2、指向调用时所在函数所绑定的对象（拗口）
3、this 不能被赋值，但可以被 call/apply  改变


以前用 this 时经常担心，不踏实，你不知道它到底指向谁？ 这里把它所有用到的地方列出

this 和构造器
this 和对象
this 和函数
全局环境的 this
this 和 DOM/事件
this 可以被 call/apply 改变
me/self/that/_this 暂存 this
ES5 中新增的 bind 和 this
ES6 箭头函数(arrow function) 和 this

```

参考：[JavaScript中知而不全的this][7]，对比参考：[深入理解javascript原型和闭包（10）——this][8]

```js
面试题：
var x = 10;
function func() {
    alert(this.x)
}
var obj = {
    x: 20,
    fn: function() {
        alert(this.x)
    }
}
var fn = obj.fn
func() // 10
fn() // 10

永远记住这一点：判断 this 指向谁，看执行时而非定义时，只要函数(function)没有绑定在对象上调用，它的 this 就是 window。

```

用原型链来理解？？？

总之，**this 指向调用时所在函数所绑定的对象**.这句话适用于this的所有场景。

## 原型 & 闭包

>__proto__是每个对象都有的一个属性，而prototype是函数才会有的属性。
__proto__指向的是当前对象的原型对象，而prototype指向的，是以当前函数作为构造函数构造出来的对象的原型对象。---[知乎][4]

函数的这个prototype的属性值是一个对象（属性的集合，再次强调！），它默认的只有一个叫做constructor的属性，指向这个函数本身。

_proto_ & prototype 区别：

![proto_protype区别](1F7FD6F620FF48D095BC431D3D6AD918)

**Instanceof的判断规则**是：沿着A的__proto__这条线来找，同时沿着B的prototype这条线来找，如果两条线能找到同一个引用，即同一个对象，那么就返回true。如果找到终点还未重合，则返回false。

instanceof表示的就是一种继承关系，或者原型链的结构。

在一段js代码拿过来真正一句一句运行之前，浏览器已经做了一些“准备工作”，其中就包括对变量的声明，而不是赋值。变量赋值是在赋值语句执行的时候进行的。

我们总结一下，在“准备工作”中完成了哪些工作：

- 变量、函数表达式——变量声明，默认赋值为undefined；
- this——赋值；
- 函数声明——赋值；

这三种数据的准备情况我们称之为“执行上下文”或者“执行上下文环境”。

在函数中this到底取何值，是在函数真正被调用执行的时候确定的，函数定义的时候确定不了。

下面的this:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script type="text/javascript">
        function testOnclick(obj) {
            console.log(obj);           //<input type="button" value="点击我" onclick="testOnclick(this)" />
            console.log(obj.type);      //button
        }
    </script>

</head>
<body>
    <form>
        <input type="button" value="点击我" onclick="testOnclick(this)" />
    </form>
</body>
</html>
```

参考：[深入理解javascript原型和闭包（完结）][6]

## Ajax

[1]:[http://www.imooc.com/wiki/view?pid=143]
[2]: http://www.w3school.com.cn/jsref/dom_obj_window.asp
[3]: https://baike.baidu.com/item/ECMAScript/1889420?fr=aladdin
[4]: https://www.zhihu.com/question/34183746
[5]: http://www.cnblogs.com/polk6/p/4492757.html
[6]: http://www.cnblogs.com/wangfupeng1988/p/3977924.html
[7]: http://www.cnblogs.com/snandy/p/4773184.html
[8]: http://www.cnblogs.com/wangfupeng1988/p/3988422.html


# 正则表达式

## 验证数字

![](https://gitee.com/leafsing/pic/raw/master/img/20200709012406.png)

原文：http://colbybobo.iteye.com/blog/1769993

![](https://gitee.com/leafsing/pic/raw/master/img/20200709012509.png)

# Q & A

## 判断当前浏览器

```
function getOs() { 
    var OsObject = "";  
    if (isIE = navigator.userAgent.indexOf("MSIE") != -1) {return "MSIE";}
    if (isFirefox = navigator.userAgent.indexOf("Firefox") != -1) {return "Firefox";  }
    if (isChrome = navigator.userAgent.indexOf("Chrome") != -1) {return "Chrome";}
    if (isSafari = navigator.userAgent.indexOf("Safari") != -1) {return "Safari";}
    if (isOpera = navigator.userAgent.indexOf("Opera") != -1) {return "Opera";}
}
```
