---
title: "Java-framework-SpringBoot-4"
date: 2019-08-20T11:14:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---

# 自定义拦截器

参考 springmvc 拦截器的自定义配置

1）实现HandlerInterceptor接口，自定义一个拦截器

```java
public class LoginHandlerInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
        Object user = request.getSession().getAttribute("loginUser");
        //没有登录
        if (user == null) {
            //转发到登录页
            request.setAttribute("msg","请先登录");
            request.getRequestDispatcher("/index.html").forward(request,response);
            return false;
        }else{
            return true;
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object o, Exception e) throws Exception {

    }
}
```

2）在java配置文件中添加拦截器

```java
    //所有的WebMvcConfigurerAdapter组件都会一起起作用
    @Bean
    public WebMvcConfigurerAdapter webMvcConfigurerAdapter(){
        WebMvcConfigurerAdapter adapter = new WebMvcConfigurerAdapter(){
            @Override
            public void addViewControllers(ViewControllerRegistry registry) {

                registry.addViewController("/").setViewName("login");
                registry.addViewController("/index.html").setViewName("login");
                registry.addViewController("/main.html").setViewName("dashboard");
            }

            //添加一个拦截器
            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                //静态资源，*.css,*.js
                //springboot已经做好的静态资源映射，我们不用配置
                registry.addInterceptor(new LoginHandlerInterceptor()).addPathPatterns("/**")
                .excludePathPatterns("/","/index.html","/user/login");
            }
        };
        return adapter;
    }
```

# RestFul

首先参考springmvc的restful怎么做的，《Java-framework-SpringMVC-1》