---
title: "Java-framework-SpringBoot-2"
date: 2019-08-20T11:12:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---


# 文件上传

不需要导入额外的包，只需要有 spring-boot-starter-web 依赖即可。

页面、控制类写法同SpriingMVC，参考《Java-framework-SpringMVC-2》。

Spring Boot需要在application.properties文件配置上传文件大小：

```
spring.servlet.multipart.max-file-size=300MB
spring.servlet.multipart.max-request-size=300MB
```

# 数据检验：hibernate-validate

> 跟 Spring MVC 数据校验一致，参考《Java-framework-SpringMVC-2》之表单校验。
>
> 也可以使用全局异常类来处理校验返回的结果
>
> 参考：https://github.com/keeperLee/miaosha_1/tree/master/src/main/java/com/imooc/miaosha/exception


1）在实体类字段上添加注解校验规则

```java
public class Users {
	@NotBlank(message="用户名不能为空") //非空校验
	@Length(min=2,max=6,message="最小长度为2位，最大长度为6位")
	private String name;
	@NotEmpty
	private String password;
	@Min(value=15)
	private Integer age;
	@Email
	private String email;
	
	//set/get...
}
```

2）在controller方法中使用@Valid和BindingResult

```
@Controller
public class UsersController {

	@RequestMapping("/addUser")
	public String showPage(@ModelAttribute("aa") Users users){
		return "add";
	}

	@RequestMapping("/save")
	public String saveUser(@ModelAttribute("aa") @Valid Users users,BindingResult result){
		if(result.hasErrors()){
			return "add";
		}
		System.out.println(users);
		return "ok";
	}
}
```
BindingResult会将校验结果以`users`作为key，对应的校验结果文本（message）作为value进行返回。
    
key遵循驼峰命名规则，即 Users 类对应 users。

如果想为传递的对象更改名称，可以使用@ModelAttribute("aa")这表示当前传递的对象的key为aa。

那么我们在页面中获取该对象的key也需要修改为aa

3） 页面

```
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>添加用户</title>
</head>
<body>
	<form th:action="@{/save}" method="post">
		用户姓名：<input type="text" name="name"/><font color="red" th:errors="${aa.name}"></font><br/>
		用户密码：<input type="password" name="password" /><font color="red" th:errors="${aa.password}"></font><br/>
		用户年龄：<input type="text" name="age" /><font color="red" th:errors="${aa.age}"></font><br/>
		用户邮箱：<input type="text" name="email" /><font color="red" th:errors="${aa.email}"></font><br/>
		<input type="submit" value="OK"/>
	</form>
</body>
</html>
```

几个注解规则解释：

- @NotBlank: 判断字符串是否为null 或者是空串(**去掉首尾空格**)。
- @NotEmpty: 判断字符串是否null 或者是空串。
- @Length: 判断字符的长度(最大或者最小)
- @Min: 判断数值最小值
- @Max: 判断数值最大值
- @Email: 判断邮箱是否合法


# 异常处理

SpringBoot 提供了五种异常处理方式

- 自定义错误页面
- @ExceptionHandle 注解处理异常
- @ControllerAdvice + @ExceptionHandler 注解处理异常
- 配置 SimpleMappingExceptionResolver 处理异常
- 自定义 HandlerExceptionResolver 类处理异常

跟 Spring MVC 结合起来一直学习。


## 自定义错误页面

SpringBoot 默认的已经提供了一套处理异常的机制。

一旦程序中出现了异常SpringBoot 会像/error 的 url 发送请求。在springBoot 中提供了一个叫 BasicExceptionController 来处理/error 请求，然后跳转到 src/main/resources/templates 目录下的 error.html。

error.html 就是我们自定义的错误页面。


## @ExceptionHandle 注解处理异常

同 Spring MVC，参考《Java-framework-SpringMVC-2》之异常处理。

## @ControllerAdvice + @ExceptionHandler 注解处理异常

同 Spring MVC，参考《Java-framework-SpringMVC-2》之异常处理。


## SimpleMappingExceptionResolver 处理异

同 Spring MVC，参考《Java-framework-SpringMVC-2》之异常处理。

其实就是将 xml 配置转为“零配置”方式。

```java
/**
 * 通过SimpleMappingExceptionResolver做全局异常处理
 */
@Configuration
public class GlobalException {
	
	/**
	 * 该方法必须要有返回值。返回值类型必须是：SimpleMappingExceptionResolver
	 */
	@Bean
	public SimpleMappingExceptionResolver getSimpleMappingExceptionResolver(){
		SimpleMappingExceptionResolver resolver = new SimpleMappingExceptionResolver();
		
		Properties mappings = new Properties();
		
		/**
		 * 参数一：异常的类型，注意必须是异常类型的全名
		 * 参数二：视图名称
		 */
		mappings.put("java.lang.ArithmeticException", "error1");
		mappings.put("java.lang.NullPointerException","error2");
		
		//设置异常与视图映射信息的
		resolver.setExceptionMappings(mappings);
		
		return resolver;
	}
}
```

这种方式的缺点：无法传递异常信息到页面。


## 自定义 HandlerExceptionResolver 类处理异常

实现 HandlerExceptionResolver 接口

```java
/**
 * 通过实现HandlerExceptionResolver接口做全局异常处理
 */
@Configuration
public class GlobalException implements HandlerExceptionResolver {

	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		ModelAndView mv = new ModelAndView();
		//判断不同异常类型，做不同视图跳转
		if(ex instanceof ArithmeticException){
			mv.setViewName("error1");
		}
		
		if(ex instanceof NullPointerException){
			mv.setViewName("error2");
		}
		mv.addObject("error", ex.toString());
		
		return mv;
	}
}
```

## 示例、博客、demo

https://www.cnblogs.com/hongdada/p/9179053.html



# Spring Boot Test

依赖

```xml
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
    </dependency>
```

测试类

```java
/**
 * SpringBoot测试类
 *@RunWith:启动器 
 *SpringJUnit4ClassRunner.class：让junit与spring环境进行整合
 *
 *@SpringBootTest(classes={App.class})有两层含义：
 * 1,当前类为springBoot的测试类
 * 2,加载SpringBoot启动类。启动springBoot
 *
 *junit与spring整合 @Contextconfiguartion("classpath:applicationContext.xml")
 */
@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest(classes={App.class})
public class UserServiceTest {

	@Autowired
	private UserServiceImpl userServiceImpl;
	
	@Test
	public void testAddUser(){
		this.userServiceImpl.addUser();
	}
}
```

# 热部署

SprigBoot 的热部署方式分为两种

- SpringLoader 插件
- DevTools 工具

## SpringLoader 方式

方式一：以maven 插件方式

```xml
    <!-- springloader插件 -->
    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <dependencies>
                    <dependency>
                        <groupId>org.springframework</groupId>
                        <artifactId>springloaded</artifactId>
                        <version>1.2.5.RELEASE</version>
                    </dependency>
                </dependencies>
            </plugin>
        </plugins>
    </build>
```

热部署操作：

1. 正常启动 Spring Boot 项目
1. 修改后台代码后，通过执行 maven 命令 spring-boot:run 进行热部署

缺点：1）只对后台代码做部署处理，对页面无能为力；2）以后台进程方式运行，占用端口，需手动关闭进程

结论：不用这种方式。


方式二：将 springloaded 的jar包放到lib目录

结论：不好用。


## DevTools 方式

1）SpringLoader 与 DevTools 的区别：

- SpringLoader：SpringLoader 在部署项目时使用的是热部署的方式。
- DevTools：DevTools 在部署项目时使用的是重新部署的方式

2）使用 DevTools

```xml
    <dependencies>
        <!-- DevTools的坐标 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <optional>true</optional>
        </dependency>
    </dependencies>
```

后台、页面修改后，项目会自动重新部署，不需要手动执行任务命令。（eclipse）

但在 IntelliJ 中要做一些配置才行

参考：https://www.cnblogs.com/MrSi/p/9540730.html，https://blog.csdn.net/qq_31293575/article/details/80654132


# thymeleaf

## 引擎原理

所有的模板引擎都是这个原理，只是模板的语法不同

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708100715.png)

## 导入依赖

修改版本，thymeleaf3跟thymeleaf-layout-dialect2以上搭配使用

参考：https://docs.spring.io/spring-boot/docs/1.5.21.RELEASE/reference/htmlsingle/#howto-use-thymeleaf-3

```
<properties>
    <thymeleaf.version>3.0.2.RELEASE</thymeleaf.version>
    <thymeleaf-layout-dialect.version>2.1.1</thymeleaf-layout-dialect.version>
</properties>
```

引入依赖

```xml
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-thymeleaf</artifactId>
        </dependency>
    </dependencies>
```

## thymeleaf默认配置

SpringBoot在自动配置时已经对thymeleaf做了默认的配置，

参考：org.springframework.boot.autoconfigure.thymeleaf.ThymeleafProperties

```java
@ConfigurationProperties(prefix = "spring.thymeleaf")
public class ThymeleafProperties {

	private static final Charset DEFAULT_ENCODING = Charset.forName("UTF-8");

	private static final MimeType DEFAULT_CONTENT_TYPE = MimeType.valueOf("text/html");

	public static final String DEFAULT_PREFIX = "classpath:/templates/";

	public static final String DEFAULT_SUFFIX = ".html";

```

## thymeleaf语法

官网：https://www.thymeleaf.org/

使用thymeleaf第一步：导入命名空间`xmlns:th="http://www.thymeleaf.org`，如下：

```xml
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>thymeleaf测试</title>
</head>
<body>

</body>
</html>
```

th:text;改变当前元素里面的文本内容；

th:任意html属性；可替换原生属性的值。

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708100747.png)

图片内容来自：https://www.thymeleaf.org/doc/tutorials/3.0/usingthymeleaf.html#attribute-precedence

可以用哪些表达式？

```
Simple expressions:
    Variable Expressions: ${...}
    Selection Variable Expressions: *{...}：配合 th:object 使用
    Message Expressions: #{...}：国际化
    Link URL Expressions: @{...}：定义URL
    Fragment Expressions: ~{...}：
Literals
    Text literals: 'one text', 'Another one!',…
    Number literals: 0, 34, 3.0, 12.3,…
    Boolean literals: true, false
    Null literal: null
    Literal tokens: one, sometext, main,…
Text operations:
    String concatenation: +
    Literal substitutions: |The name is ${name}|
Arithmetic operations:
    Binary operators: +, -, *, /, %
    Minus sign (unary operator): -
Boolean operations:
    Binary operators: and, or
    Boolean negation (unary operator): !, not
Comparisons and equality:
    Comparators: >, <, >=, <= (gt, lt, ge, le)
    Equality operators: ==, != (eq, ne)
Conditional operators:
    If-then: (if) ? (then)
    If-then-else: (if) ? (then) : (else)
    Default: (value) ?: (defaultvalue)
Special tokens:
    No-Operation: _
```

参考：https://www.thymeleaf.org/doc/tutorials/3.0/usingthymeleaf.html#standard-expression-syntax


## 如何使用webjar

官网：https://www.webjars.org/

```html
	<!-- Bootstrap core CSS -->
	<link href="asserts/css/bootstrap.min.css" th:href="@{/webjars/bootstrap/4.0.0/css/bootstrap.css}" rel="stylesheet">
	<!-- Custom styles for this template -->
	<link href="asserts/css/signin.css" th:href="@{/asserts/css/signin.css}" rel="stylesheet">
```

`@{/asserts/css/signin.css}`表示 resources/static目录下的 asserts目录 ——20190818



# 国际化

1）编写国际化文件

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708100812.png)

2）配置ResourceBundleMessageSource

SpringBoot自动配置好了ResourceBundleMessageSource

查看`org.springframework.boot.autoconfigure.context.MessageSourceAutoConfiguration`源码，

可以看到`@ConfigurationProperties(prefix = "spring.messages")`

```java
@ConfigurationProperties(prefix = "spring.messages")
public class MessageSourceAutoConfiguration {

	private static final Resource[] NO_RESOURCES = {};

	/**
	 * Comma-separated list of basenames (essentially a fully-qualified classpath
	 * location), each following the ResourceBundle convention with relaxed support for
	 * slash based locations. If it doesn't contain a package qualifier (such as
	 * "org.mypackage"), it will be resolved from the classpath root.
	 */
	private String basename = "messages";
```

因此，我们只需要在application.properties文件中配置以spring.message开头的配置即可

```
spring.messages.basename=i18n.login
```

3）页面获取国际化值

前台模板使用thymeleaf，使用 `th:text="#{login.btn}"`取值

```html
<h1 class="h3 mb-3 font-weight-normal" th:text="#{login.tip}">Please sign in</h1>
```

---

疑问：如何实现：页面点击“中文”，使用中文的国际化；页面点击“EN”使用英文的国际化？如图：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708100835.png)

1）页面标签写法

```html
	<a class="btn btn-sm" th:href="@{/index.html(l='zh_CN')}">中文</a>
	<a class="btn btn-sm" th:href="@{/index.html(l='en_US')}">English</a>
```

2）自定义一个LocaleResolver

```java
package com.demo.component;

import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * 自定义的国际化解析器
 *
 * 实现了：页面点击“中文”，使用中文的国际化；页面点击“EN”使用英文的国际化
 *
 * Created by lwx on 2019/6/9.
 */
public class MyLocalResolver implements LocaleResolver {
    @Override
    public Locale resolveLocale(HttpServletRequest rq) {
        //http://localhost:8080/index.html?l=zh_CN
        String l = rq.getParameter("l");
        Locale locale = Locale.getDefault();
        if (!StringUtils.isEmpty(l)) {
            String[] arr = l.split("_");
            locale = new Locale(arr[0],arr[1]);
        }
        return locale;
    }

    @Override
    public void setLocale(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Locale locale) {

    }
}
```

他的原理是改变Locale的值。

3）在java配置文件中配置成一个bean

```java
    @Bean
    public LocaleResolver localeResolver(){
        return new MyLocalResolver();
    }
```

参考代码：https://github.com/wolf-lea/spring-boot-v1520-demo/tree/master/31-spring-boot-atguigu-crud

# Spring Boot 启动配置原理

教程参考：https://ke.qq.com/course/403245?taid=3394046366459693


## 如何修改Spring Boot的默认配置

1）SpringBoot在自动配置很多组件时，都会用@ConditionalOnxxx来判断有没有用户自己配置的（@Bean,@Component），如果有就用用户配置的，如果没有才自动配置；如果有些组件有多个（如ViewResolver），则将用户配置和默认配置组合起来；

2）在SpringBoot中会有非常多的xxxConfigurer帮助我们进行扩展配置

