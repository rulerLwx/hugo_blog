---
title: "Java-framework-RabbitMQ-1"
date: 2019-08-09T11:02:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---


# 安装RabbitMQ（一）

## 1、安装Erlang

RabbitMQ需要Erlang环境，Erlang(['ə:læŋ])是一种通用的面向并发的编程语言，它由瑞典电信设备制造商爱立信所辖的
CS-Lab 开发，目的是创造一种可以应对大规模并发活动的编程语言和运行环境

```sh
cd /usr/local/src/
mkdir rabbitmq
cd rabbitmq

wget https://packages.erlang-solutions.com/erlang-solutions-1.0-1.noarch.rpm
rpm -Uvh erlang-solutions-1.0-1.noarch.rpm

rpm --import http://packages.erlang-solutions.com/rpm/erlang_solutions.asc

yum install erlang

erl -version # 查看是否安装成功
```

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708082012.png)

## 2、安装RabbitMQ Server

安装环境：CentOS7、RabbitMQ－Server：3.7.15

```sh
cd /usr/local/src/rabbitmq
# wget http://www.rabbitmq.com/releases/rabbitmq-server/v3.5.1/rabbitmq-server-3.5.1-1.noarch.rpm
wget https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.7.15/rabbitmq-server-3.7.15-1.el7.noarch.rpm
# rpm --import http://www.rabbitmq.com/rabbitmq-signing-key-public.asc
rpm --import https://github.com/rabbitmq/signing-keys/releases/download/2.0/rabbitmq-release-signing-key.asc
yum install rabbitmq-server-3.7.15-1.el7.noarch.rpm
```

rqbbitmq跟centos版本有关系

参考了：https://www.rabbitmq.com/install-rpm.html#downloads

## 3、启动RabbitMQ

```sh
1.配置为守护进程随系统自动启动，root权限下执行:
	chkconfig rabbitmq-server on
2.启动rabbitMQ服务
	/sbin/service rabbitmq-server start
```

可能遇到启动失败，

参考：https://stackoverflow.com/questions/25816918/not-able-to-start-rabbitmq-server-in-centos-7-using-systemctL

mq版本问题：https://ask.csdn.net/questions/694983

## 4、安装Web管理界面插件

```sh
1.安装命令
	rabbitmq-plugins enable rabbitmq_management
2.安装成功后会显示如下内容
	The following plugins have been enabled:
	  mochiweb
	  webmachine
	  rabbitmq_web_dispatch
	  amqp_client
	  rabbitmq_management_agent
	  rabbitmq_management
	Plugin configuration has changed. Restart RabbitMQ for changes to take effect.
```

## 5、设置RabbitMQ远程ip登录

这里我们以创建个oldlu帐号，密码123456为例，创建一个账号并支持远程ip访问。

```
1.创建账号
	rabbitmqctl add_user oldlu 123456
2.设置用户角色
	rabbitmqctl  set_user_tags  oldlu  administrator
3.设置用户权限
	rabbitmqctl set_permissions -p "/" oldlu ".*" ".*" ".*"
4.设置完成后可以查看当前用户和角色(需要开启服务)
	rabbitmqctl list_users
```
	
浏览器输入：serverip:15672。其中serverip是RabbitMQ-Server所在主机的ip。


# 安装 RabbitMQ（二）：docker

参考《Linux-Docker-1》


# RabbitMQ 界面操作

## 添加用户

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708082758.png)

## virtual hosts

virtual hosts 相当于 mysql 的 db ，权限操作。

每个VirtualHost相当于一个相对独立的 RabbitMQ 服务器，每个VirtualHost之间是相互隔离的。exchange、queue、message不能互通。

名称一般以 / 开头 

1）创建

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708082854.png)

2）授权给用户

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708082917.png)


# 为什么用MQ？

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708082945.png)

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708083017.png)

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708083111.png)

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708083132.png)

# 消息队列基础

# 入门示例

官方示例：https://github.com/spring-projects/spring-boot/tree/master/spring-boot-samples/spring-boot-sample-amqp

1）创建springboot项目

2）依赖

```xml
	<dependency>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-amqp</artifactId>
	</dependency>
```

3）application.properties

```properties
spring.application.name=spring-cloud-mq

spring.rabbitmq.host=192.168.12.132
spring.rabbitmq.port=5672
spring.rabbitmq.username=oldlu
spring.rabbitmq.password=123456
```

4）启动类

```java
@SpringBootApplication
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
```

5）队列、发送者、接收者

```java
package com.demo;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 创建消息队列
 *
 * Created by lwx on 2019/6/16.
 */
@Configuration
public class QueueConfig {
    //注意：Queue是 org.springframework.amqp.core.Queue;
    @Bean
    public Queue createQueue(){

        return new Queue("hello-queue");
    }
}

```

```java
@Component
public class Sender {

    @Autowired
    private AmqpTemplate rabbitAmqpTemplate;

    public void send(String msg) {
        /**
         * 参数一：消息队列名称
         * 参数二：消息
         */
        this.rabbitAmqpTemplate.convertAndSend("hello-queue",msg);
    }
}
```

```
@Component
public class Receiver {

    /**
     * 接收消息，采用消息队列监听机制
     */
    @RabbitListener(queues = {"hello-queue"})
    public void process(String msg){
        System.out.println("receiver:"+msg);
    }
}
```

6）测试

```java
@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class QueueTest {

    @Autowired
    private Sender sender;

    @Test
    public void test1(){
        this.sender.send("hello rabbitmq");
    }
}
```

注：这只是一个简单示例，其实没什么用


# RabbitMQ原理图

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708083236.png)

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708083250.png)

1. Message
消息。消息是不具名的，它由消息头、消息体组成。消息体是不透明的，而消息头则由
一系列可选属性组成，这些属性包括：routing-key(路由键)、priority(相对于其他消息的优先权)、delivery-mode(指出消息可能持久性存储)等。
2. Publisher
消息的生产者。也是一个向交换器发布消息的客户端应用程序。
3. Consumer
消息的消费者。表示一个从消息队列中取得消息的客户端应用程序。
4. Exchange
交换器。用来接收生产者发送的消息并将这些消息路由给服务器中的队列。
三种常用的交换器类型
    1. direct(发布与订阅完全匹配)
    2. fanout(广播)
    3. topic(主题，规则匹配)
5. Binding
绑定。用于消息队列和交换器之间的关联。一个绑定就是基于路由键将交换器和消息
队列连接起来的路由规则，所以可以将交换器理解成一个由绑定构成的路由表。
6. Queue
消息队列。用来保存消息直到发送给消费者。它是消息的容器，也是消息的终点。一
个消息可投入一个或多个队列。消息一直在队列里面，等待消费者链接到这个队列将其取
走。
7. Routing-key
路由键。RabbitMQ 决定消息该投递到哪个队列的规则。
队列通过路由键绑定到交换器。
消息发送到MQ 服务器时，消息将拥有一个路由键，即便是空的，RabbitMQ 也会将其
和绑定使用的路由键进行匹配。
如果相匹配，消息将会投递到该队列。
如果不匹配，消息将会进入黑洞。
8. Connection
链接。指rabbit 服务器和服务建立的TCP 链接。
9. Channel 信道。
    1. Channel 中文叫做信道，是TCP 里面的虚拟链接。例如：电缆相当于TCP，信道是
    一个独立光纤束，一条TCP 连接上创建多条信道是没有问题的。
    2. TCP 一旦打开，就会创建AMQP 信道。
    3. 无论是发布消息、接收消息、订阅队列，这些动作都是通过信道完成的。
10. Virtual Host
虚拟主机。表示一批交换器，消息队列和相关对象。虚拟主机是共享相同的身份认证
和加密环境的独立服务器域。每个vhost 本质上就是一个mini 版的RabbitMQ 服务器，拥有
自己的队列、交换器、绑定和权限机制。vhost 是AMQP 概念的基础，必须在链接时指定，
RabbitMQ 默认的vhost 是/
11. Borker
表示消息队列服务器实体。

交换器和队列的关系：

交换器是通过路由键和队列绑定在一起的，如果消息拥有的路由键跟队列和交换器的
路由键匹配，那么消息就会被路由到该绑定的队列中。
也就是说，消息到队列的过程中，消息首先会经过交换器，接下来交换器在通过路由
键匹配分发消息到具体的队列中。
路由键可以理解为匹配的规则。

RabbitMQ 为什么需要信道？为什么不是TCP 直接通信？

1. TCP 的创建和销毁开销特别大。创建需要3 次握手，销毁需要4 次分手。
2. 如果不用信道，那应用程序就会以TCP 链接Rabbit，高峰时每秒成千上万条链接
会造成资源巨大的浪费，而且操作系统每秒处理TCP 链接数也是有限制的，必定造成性能
瓶颈。
3. 信道的原理是一条线程一条通道，多条线程多条通道同用一条TCP 链接。一条TCP
链接可以容纳无限的信道，即使每秒成千上万的请求也不会成为性能的瓶颈。

# RabbitMQ交换器讲解

三种常用的交换器类型
- direct(发布与订阅完全匹配)
- topic(主题，规则匹配)
- fanout(广播)

## Direct交换器

### 需求

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708083327.png)

### 代码实现

1）创建两个工程，生产者、消费者

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708083406.png)

2）修改全局配置文件

消费者（Consumer）

```
spring.application.name=spring-cloud-mq

spring.rabbitmq.host=192.168.12.132
spring.rabbitmq.port=5672
spring.rabbitmq.username=oldlu
spring.rabbitmq.password=123456

#自定义交换器名称
mq.config.exchange=log.direct

#自定义info队列名称、路由键
mq.config.queue.info=log.info
mq.config.queue.info.routing.key=log.info.routing.key

#自定义error队列名称、路由键
mq.config.queue.error=log.error
mq.config.queue.error.routing.key=log.error.routing.key
```

生产者（Provider）

```
spring.application.name=spring-cloud-mq

spring.rabbitmq.host=192.168.12.132
spring.rabbitmq.port=5672
spring.rabbitmq.username=oldlu
spring.rabbitmq.password=123456

#自定义交换器名称
mq.config.exchange=log.direct

#自定义info/error路由键
mq.config.queue.info.routing.key=log.info.routing.key
mq.config.queue.error.routing.key=log.error.routing.key
```

3）Consumer下的两个消费器

```java
/**
 * 接收info级别的消息处理器
 *
 * Created by lwx on 2019/6/16.
 */
@Component
@RabbitListener(
        bindings =@QueueBinding(
                value = @Queue(value = "${mq.config.queue.info}",autoDelete ="true"),//配置队列名称、是否是一个可删除的临时队列
                exchange = @Exchange(value = "${mq.config.exchange}",type = ExchangeTypes.DIRECT),//配置交换器名称、指定交换器类型
                key = "${mq.config.queue.info.routing.key}"
        )
)
public class InfoReceiver {

    @RabbitHandler
    public void process(String msg){
        System.out.println("info.........receiver:"+msg);
    }
}
```

```java
/**
 * 接收error级别的消息处理器
 *
 * Created by lwx on 2019/6/16.
 */
@Component
@RabbitListener(
        bindings =@QueueBinding(
                value = @Queue(value = "${mq.config.queue.error}",autoDelete ="true"),//配置队列名称、是否是一个可删除的临时队列
                exchange = @Exchange(value = "${mq.config.exchange}",type = ExchangeTypes.DIRECT),//配置交换器名称、指定交换器类型
                key = "${mq.config.queue.error.routing.key}"
        )
)
public class ErrorReceiver {

    @RabbitHandler
    public void process(String msg){
        System.out.println("error.......receiver:"+msg);
    }
}
```

4）Provider生产者

```java
/**
 * 消息发送者
 *
 * Created by lwx on 2019/6/16.
 */
@Component
public class Sender {

    @Autowired
    private AmqpTemplate rabbitAmqpTemplate;

    @Value("${mq.config.exchange}")
    private String exchange;

    @Value("${mq.config.queue.info.routing.key}")
    private String routingkey;

    public void send(String msg) {
        /**
         * 参数一：交换器名称
         * 参数二：路由键
         * 参数三：消息
         */
        this.rabbitAmqpTemplate.convertAndSend(this.exchange,this.routingkey,msg);
    }
}
```

5）测试

步骤一：启动消费者项目的引导类

步骤二：运行生产者的测试代码

```java
@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class QueueTest {

    @Autowired
    private Sender sender;

    @Test
    public void test1() throws InterruptedException {
        while (true) {
            Thread.sleep(1000);
            this.sender.send("hello rabbitmq");
        }
    }
}
```

在消费者项目的控制台上看到输出：

```
info.........receiver:hello rabbitmq
...
```

## Topic交换器

### 需求

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708083440.png)

### 代码实现

创建两个项目

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708083501.png)

1）全局配置文件

consumer

```
spring.application.name=spring-cloud-mq

spring.rabbitmq.host=192.168.12.132
spring.rabbitmq.port=5672
spring.rabbitmq.username=oldlu
spring.rabbitmq.password=123456

#自定义交换器名称
mq.config.exchange=log.topic

#自定义info队列名称
mq.config.queue.info=log.info

#自定义error队列名称
mq.config.queue.error=log.error

#自定义log队列名称
mq.config.queue.logs=log.all
```

provider

```
spring.application.name=spring-cloud-mq

spring.rabbitmq.host=192.168.12.132
spring.rabbitmq.port=5672
spring.rabbitmq.username=oldlu
spring.rabbitmq.password=123456

#自定义交换器名称
mq.config.exchange=log.topic
```

2）编写provider

```java
/**
 * 消息发送者
 *
 * 用户服务
 *
 * Created by lwx on 2019/6/16.
 */
@Component
public class UserSender {

    @Autowired
    private AmqpTemplate rabbitAmqpTemplate;

    @Value("${mq.config.exchange}")
    private String exchange;

    public void send(String msg) {
        /**
         * 参数一：交换器名称
         * 参数二：路由键
         * 参数三：消息
         */
        this.rabbitAmqpTemplate.convertAndSend(this.exchange,"user.log.debug","user.log.debug...."+msg);
        this.rabbitAmqpTemplate.convertAndSend(this.exchange,"user.log.info","user.log.info...."+msg);
        this.rabbitAmqpTemplate.convertAndSend(this.exchange,"user.log.warn","user.log.warn...."+msg);
        this.rabbitAmqpTemplate.convertAndSend(this.exchange,"user.log.error","user.log.error...."+msg);
    }
}
```

```java
/**
 * 消息发送者
 *
 * 商品服务
 *
 * Created by lwx on 2019/6/16.
 */
@Component
public class ProductSender {

    @Autowired
    private AmqpTemplate rabbitAmqpTemplate;

    @Value("${mq.config.exchange}")
    private String exchange;

    public void send(String msg) {
        /**
         * 参数一：交换器名称
         * 参数二：路由键
         * 参数三：消息
         */
        this.rabbitAmqpTemplate.convertAndSend(this.exchange,"product.log.debug","product.log.debug...."+msg);
        this.rabbitAmqpTemplate.convertAndSend(this.exchange,"product.log.info","product.log.info...."+msg);
        this.rabbitAmqpTemplate.convertAndSend(this.exchange,"product.log.warn","product.log.warn...."+msg);
        this.rabbitAmqpTemplate.convertAndSend(this.exchange,"product.log.error","product.log.error...."+msg);
    }
}
```

```java
/**
 * 消息发送者
 *
 * 订单服务
 *
 * Created by lwx on 2019/6/16.
 */
@Component
public class OrderSender {

    @Autowired
    private AmqpTemplate rabbitAmqpTemplate;

    @Value("${mq.config.exchange}")
    private String exchange;

    public void send(String msg) {
        /**
         * 参数一：交换器名称
         * 参数二：路由键
         * 参数三：消息
         */
        this.rabbitAmqpTemplate.convertAndSend(this.exchange,"order.log.debug","order.log.debug...."+msg);
        this.rabbitAmqpTemplate.convertAndSend(this.exchange,"order.log.info","order.log.info...."+msg);
        this.rabbitAmqpTemplate.convertAndSend(this.exchange,"order.log.warn","order.log.warn...."+msg);
        this.rabbitAmqpTemplate.convertAndSend(this.exchange,"order.log.error","user.log.error...."+msg);
    }
}
```

3）编写consumer

```java
/**
 * 接收info级别的消息处理器
 *
 * Created by lwx on 2019/6/16.
 */
@Component
@RabbitListener(
        bindings =@QueueBinding(
                value = @Queue(value = "${mq.config.queue.info}",autoDelete ="true"),//配置队列名称、是否是一个可删除的临时队列
                exchange = @Exchange(value = "${mq.config.exchange}",type = ExchangeTypes.TOPIC),//配置交换器名称、指定交换器类型
                key = "*.log.info"
        )
)
public class InfoReceiver {

    @RabbitHandler
    public void process(String msg){
        System.out.println("info.........receiver:"+msg);
    }
}
```

```java
/**
 * 接收error级别的消息处理器
 *
 * Created by lwx on 2019/6/16.
 */
@Component
@RabbitListener(
        bindings =@QueueBinding(
                value = @Queue(value = "${mq.config.queue.error}",autoDelete ="true"),//配置队列名称、是否是一个可删除的临时队列
                exchange = @Exchange(value = "${mq.config.exchange}",type = ExchangeTypes.TOPIC),//配置交换器名称、指定交换器类型
                key = "*.log.error"
        )
)
public class ErrorReceiver {

    @RabbitHandler
    public void process(String msg){
        System.out.println("error.......receiver:"+msg);
    }
}
```

```java
/**
 * 接收所有日志级别的消息处理器
 *
 * Created by lwx on 2019/6/16.
 */
@Component
@RabbitListener(
        bindings =@QueueBinding(
                value = @Queue(value = "${mq.config.queue.logs}",autoDelete ="true"),//配置队列名称、是否是一个可删除的临时队列
                exchange = @Exchange(value = "${mq.config.exchange}",type = ExchangeTypes.TOPIC),//配置交换器名称、指定交换器类型
                key = "*.log.*"
        )
)
public class LogsReceiver {

    @RabbitHandler
    public void process(String msg){
        System.out.println("all.........receiver:"+msg);
    }
}
```

4）测试

步骤一：运行consumer引导类

步骤二：运行provider测试方法

```java
@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class QueueTest {

    @Autowired
    private UserSender userSender;

    @Autowired
    private ProductSender productSender;

    @Autowired
    private OrderSender orderSender;

    @Test
    public void test1() throws InterruptedException {
        this.userSender.send("userSender...........hello rabbitmq");
        this.productSender.send("productSender...........hello rabbitmq");
        this.orderSender.send("orderSender...........hello rabbitmq");
    }
}
```

## Fanout交换器

### 需求

原来耦全方式：
![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708083540.png)

使用mq后：
![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708083559.png)

### 代码实现

1）创建两个项目

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708083617.png)

2）修改全局配置文件

consumer

```
spring.application.name=spring-cloud-mq

spring.rabbitmq.host=192.168.12.132
spring.rabbitmq.port=5672
spring.rabbitmq.username=oldlu
spring.rabbitmq.password=123456

#自定义交换器名称
mq.config.exchange=order.fanout

#自定义队列名称
mq.config.queue.sms=order.sms
mq.config.queue.push=order.push
```

provider
```
spring.application.name=spring-cloud-mq

spring.rabbitmq.host=192.168.12.132
spring.rabbitmq.port=5672
spring.rabbitmq.username=oldlu
spring.rabbitmq.password=123456

#自定义交换器名称
mq.config.exchange=order.fanout
```

广播模式，所有的队列都会接收到消息，所有不需要路由键

3）编写consumer，两个消息处理器

```java
/**
 * sms消息处理器
 *
 * @QueueBinding
 *      value 绑定队列名称
 *      exchange 路由器
 *      key 路由键
 *
 * FANOUT模式不需要路由键
 *
 * Created by lwx on 2019/6/16.
 */
@Component
@RabbitListener(
        bindings =@QueueBinding(
                value = @Queue(value = "${mq.config.queue.sms}",autoDelete ="true"),//配置队列名称、是否是一个可删除的临时队列
                exchange = @Exchange(value = "${mq.config.exchange}",type = ExchangeTypes.FANOUT)//配置交换器名称、指定交换器类型
        )
)
public class SmsReceiver {

    @RabbitHandler
    public void process(String msg){
        System.out.println("sms.........receiver:"+msg);
    }
}
```

```java
/**
 * push消息处理器
 *
 * Created by lwx on 2019/6/16.
 */
@Component
@RabbitListener(
        bindings =@QueueBinding(
                value = @Queue(value = "${mq.config.queue.push}",autoDelete ="true"),//配置队列名称、是否是一个可删除的临时队列
                exchange = @Exchange(value = "${mq.config.exchange}",type = ExchangeTypes.FANOUT)//配置交换器名称、指定交换器类型
        )
)
public class PushReceiver {

    @RabbitHandler
    public void process(String msg){
        System.out.println("push.......receiver:"+msg);
    }
}
```

4）编写provider

```
@Component
public class Sender {

    @Autowired
    private AmqpTemplate rabbitAmqpTemplate;

    @Value("${mq.config.exchange}")
    private String exchange;

    public void send(String msg) {
        /**
         * 参数一：交换器名称
         * 参数二：路由键
         * 参数三：消息
         *
         * fanout模式，参数二传""
         */
        this.rabbitAmqpTemplate.convertAndSend(this.exchange,"",msg);
    }
}
```

注意：fanout模式时，参数二传""

5）测试

步骤一：启动consumer引导类

步骤二：启动provider测试类

```java
@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class QueueTest {

    @Autowired
    private Sender sender;

    @Test
    public void test1(){
        this.sender.send("hello rabbitmq");
    }
}
```
