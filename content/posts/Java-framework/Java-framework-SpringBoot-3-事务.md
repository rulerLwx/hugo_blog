---
title: "Java-framework-SpringBoot-3-事务"
date: 2019-08-20T11:13:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---

# 开启事务

在配置类中使用@EnableTransactionManagement开启事务

```java
@SpringBootApplication
@EnableTransactionManagement
public class AppConfig {
    public static void main(String[] args) {
        
    }
}
```

但，SpringBoot自动开启了注解事务的支持，**无须显式使用@EnableTransactionManagement开启事务**

——《SpringBoot实战》汪云飞，p303

# 事务行为

@Transactional可注解在类、方法上，如果两者都用了，以方法的为准。——《SpringBoot实战》汪云飞，p300

使用@Transactional时，可指定如下属性：

- isolation：指定事务隔离级别。默认为底层事务的隔离级别。
- noRollbackFor()：指定遇到特定异常时强制不回滚事务。
- noRollbackForClassName()：指定遇到特定的多个异常时强制不回滚事务。该属性可以指定多个异常类名。
- propagation()：指定事务传播行为。
- readOnly：指定事务是否只读，默认false。
- rollbackFor()：定遇到特定异常时强制回滚事务。
- rollbackForClassName()：指定遇到特定的多个异常时强制回滚事务。该属性可以指定多个异常类名。
- timeout()：指定事务的超时时长。

总结：看源码，就知道要配置的值是哪些东西了。

Spring Date JPA 对所有的默认方法都开启了事务支持，且查询类事务默认启用 readOnly=true 。因为它在类上使用@Transactional(readOnly=true)，而在和save、delete相关的操作重写了@Transaction属性，readOnly默认是false。

