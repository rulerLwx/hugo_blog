---
title: "Java-framework-Spring-7-事务"
date: 2019-08-15T11:09:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---


# 事务

## 基本概念

什么是事务？

事务有很严格的定义，它必须同时满足4个条件：原子性（Atomicity）、一致性（Consistency）、隔离性（Isolation）、持久性（Durability）。

- 原子性：指一个事务必须被视为一个不可分割的最小工作单元（要么全做，要么全不做）。
- 一致性：指事务将数据库从一种状态转变为下一种一致的状态。如违反唯一性约束时（即操作失败），系统自动撤销事务，返回初始化的状态。
- 隔离性：还可称为并发控制、可串行人、锁等，当多个用户并发访问数据库时，数据库为每个用户开启的事务，不能被其它事务的操作数据所干扰。
- 持久性：事务一旦提交，其所做的修改就会永久保存到数据库中。


声明式事务基于Spring AOP实现。

Java EE应用的传统事务有两种策略：全局事务和局部事务。
全局事务由应用服务器管理，需要底层服务器的JTA支持。

Spring事务策略是通过PlatformTransactionManager接口体现的，系统只要使用不同的PlatformTransactionManager实现类即可。

即使使用容器管理的JTA，代码也依然无须执行JNDI查找，无须与特定的JTA资源耦合在一起，通过配置文件，JTA资源传给PlatformTransactionManager的实现类。因此，程序的代码可在JTA事务管理和非JTA事务管理之间轻松切换。

换句话说，Spring既支持全局事务，也支持局部事务，就看应用的底层是否支持了。

PlatformTransactionManager接口有如下方法：

- public void rollback(TransactionStatus arg0) throws TransactionException 
- public TransactionStatus getTransaction(TransactionDefinition arg0)
- public void commit(TransactionStatus arg0) throws TransactionException

TransactionDefinition接口定义了一个事务规则，该接口必须指定如下几个属性值：

- 事务隔离：当前事务与其它事务的隔离程序。
- 事务传播：如果一个事务上下文已经存在，有几个选项可指定该事务性方法的执行行为。
- 事务超时：事务的最长持续时间。
- 只读状态：只读事务不修改任务数据。在某些情况下，只读事务是非常有用的优化。

不同的PlatformTransactionManager的实现类：

1）JDBC：org.springframework.jdbc.datasource.DataSourceTransactionManager

2）JTA：org.springframework.transaction.jta.JtaTransactionManager （分布式事务）

3）Hibernate：org.springframework.orm.hibernate4.HibernateTransactionManager

4）JPA：JpaTransactionManager

5）JDO：JdoTransactionManager —— 《SpringBoot实战》汪云飞，p298

Spring提供如下两种事务管理方式：

1）编程式事务管理：即使使用Spring的编程式事务，程序也可直接获取容器中的transactionManager Bean，该Bean总是PlatformTransactionManager的实例，所以可能通过该接口提供的三个方法来开始事务，提交事务和回滚事务。
（通过TransactionTemplate类操作事务更简捷）

2）声明式事务管理：分为XML配置方式、Annotation注解方式。



## XML配置事务

1、引入tx:命名空间：

```
xmlns:tx="http://www.springframework.org/schema/tx"
http://www.springframework.org/schema/tx 
http://www.springframework.org/schema/tx/spring-tx-4.0.xsd
```

2、<tx:advice.../>元素除了需要transaction-manager属性指定事务管理器之外，还需要一个<tx:attributes.../>子元素，该子元素又可包含多个<tx:method.../>子元素。

<tx:method.../>子元素可配置如下几个属性：

- name：指定对哪些方法起作用。
- propagation：事务传播属性。
- isolation：事务隔离属性。
- timeout：事务超时属性。
- read-only：只读属性。
- rollback-for：对指定异常回滚。
- no-rollback-for：对指定异常不回滚。

其中propagation的事务传播行为有如下几种：

- MANDATORY：要求调用该方法的线程必须处于事务环境中，否则抛异常。
- NESTED：即使执行该方法的线程已牌事务环境中，也依然启动新的事务，方法嵌套在事务里执行。
- NEVER：不允许调用该方法的线程处于事务环境中，否则抛异常。
- NOT_SUPPORTED：如果调用该方法的线程处于事务环境中，则先暂停当前事务，然后执行该方法。
- REQUIRED：要求在事务环境中执行该方法。如果当前执行线程已处于事务环境中，则直接调用；如果当前执行线程不处于事务环境中，则启动新的事务后执行该方法。
- REQUIRES_NEW：要求在新的事务环境中执行。如果当前执行线程已经处于事务环境中，则先暂停当前事务，启动新事务后执行该方法；如果当前调用线程不处于事务环境中，则启动新的事务后执行该方法。
- SUPPORTS：如果当前执行线程处于事务环境中，则使用当前事务，否则不使用事务。


示例：

```
public class NewsDaoImpl implements NewsDao{
	private DataSource ds;
	public void setDs(DataSource ds){
		this.ds = ds;
	}
	public void insert(String title, String content){
		JdbcTemplate jt = new JdbcTemplate(ds);
		jt.update("insert into news_inf"+ " values(null , ? , ?)", title , content);
		// 两次插入的数据违反唯一键约束
		jt.update("insert into news_inf"+ " values(null , ? , ?)", title , content);
		// 如果没有事务控制，则第一条记录可以被插入
		// 如果增加事务控制，将发现第一条记录也插不进去。
	}
}
```

```
<?xml version="1.0" encoding="GBK"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="http://www.springframework.org/schema/beans"
	xmlns:p="http://www.springframework.org/schema/p"
	xmlns:aop="http://www.springframework.org/schema/aop"
	xmlns:tx="http://www.springframework.org/schema/tx"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
	http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
	http://www.springframework.org/schema/aop
	http://www.springframework.org/schema/aop/spring-aop-4.0.xsd
	http://www.springframework.org/schema/tx
	http://www.springframework.org/schema/tx/spring-tx-4.0.xsd">

	<bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource"
		destroy-method="close"
		p:driverClass="com.mysql.jdbc.Driver"
		p:jdbcUrl="jdbc:mysql://localhost/spring"
		p:user="root"
		p:password="32147"
		p:maxPoolSize="40"
		p:minPoolSize="2"
		p:initialPoolSize="2"
		p:maxIdleTime="30"/>

	<bean id="transactionManager" 
		class="org.springframework.jdbc.datasource.DataSourceTransactionManager"
		p:dataSource-ref="dataSource"/>

	<bean id="newsDao" class="org.crazyit.app.dao.impl.NewsDaoImpl" p:ds-ref="dataSource"/>

	<tx:advice id="txAdvice" transaction-manager="transactionManager">
		<tx:attributes>
			<tx:method name="get*" read-only="true"/>
			<tx:method name="*" isolation="DEFAULT" propagation="REQUIRED" timeout="5"/>
		</tx:attributes>
	</tx:advice>

	<aop:config>
		<aop:pointcut id="myPointcut" expression="execution(* org.crazyit.app.dao.impl.*Impl.*(..))"/>
		<aop:advisor advice-ref="txAdvice" pointcut-ref="myPointcut"/>
	</aop:config>
</beans>
```

总结：事务中的切面类是Spring定义好了的（以前都是咱自己用@Aspect定义的）。



## Annotation配置事务

@Transactional注解，可用于类（对类下的所有方法有效）、方法上。

使用@Transactional时，可指定如下属性：

- isolation：指定事务隔离级别。默认为底层事务的隔离级别。
- noRollbackFor()：指定遇到特定异常时强制不回滚事务。
- noRollbackForClassName()：指定遇到特定的多个异常时强制不回滚事务。该属性可以指定多个异常类名。
- propagation()：指定事务传播行为。
- readOnly：指定事务是否只读。
- rollbackFor()：定遇到特定异常时强制回滚事务。
- rollbackForClassName()：指定遇到特定的多个异常时强制回滚事务。该属性可以指定多个异常类名。
- timeout()：指定事务的超时时长。

总结：看源码，就知道要配置的值是哪些东西了。


配置步骤：

1）在需要使用事务的类或方法上使用@Transactional注解

2）在beans.xml文件中增加两个配置：

```
<bean class="org.springframework.jdbc.datasource.DataSourceTransactionManager" 
	id="transactionManager" p:dataSource-ref="dataSource"/>
<tx:annotation-driven transaction-manager="transactionManager"/>
```

示例：

```
public interface NewsDao {
	public void insert(String title, String content);
}
```

```
public class NewsDaoImpl implements NewsDao {
	private DataSource ds;
	public void setDs(DataSource ds){
		this.ds = ds;
	}
	
	@Transactional(propagation=Propagation.REQUIRED ,isolation=Isolation.DEFAULT , timeout=5)
	public void insert(String title, String content){
		JdbcTemplate jt = new JdbcTemplate(ds);
		jt.update("insert into news_inf"+ " values(null , ? , ?)", title , content);
		// 两次插入的数据违反唯一键约束
		jt.update("insert into news_inf"+ " values(null , ? , ?)", title , content);
		// 如果没有事务控制，则第一条记录可以被插入
		// 如果增加事务控制，将发现第一条记录也插不进去。
	}
}
```

```
<?xml version="1.0" encoding="GBK"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="http://www.springframework.org/schema/beans"
	xmlns:p="http://www.springframework.org/schema/p"
	xmlns:aop="http://www.springframework.org/schema/aop"
	xmlns:tx="http://www.springframework.org/schema/tx"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
	http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
	http://www.springframework.org/schema/aop
	http://www.springframework.org/schema/aop/spring-aop-4.0.xsd
	http://www.springframework.org/schema/tx
	http://www.springframework.org/schema/tx/spring-tx-4.0.xsd">

	<bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource"
		destroy-method="close"
		p:driverClass="com.mysql.jdbc.Driver"
		p:jdbcUrl="jdbc:mysql://localhost/spring"
		p:user="root"
		p:password="32147"
		p:maxPoolSize="40"
		p:minPoolSize="2"
		p:initialPoolSize="2"
		p:maxIdleTime="30"/>

	<bean id="newsDao" class="org.crazyit.app.dao.impl.NewsDaoImpl" p:ds-ref="dataSource"/>

	<bean id="transactionManager" 
		class="org.springframework.jdbc.datasource.DataSourceTransactionManager"
		p:dataSource-ref="dataSource"/>
		
	<tx:annotation-driven transaction-manager="transactionManager"/>

</beans>
```

```
public class SpringTest{
	public static void main(String[] args){
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		NewsDao dao = (NewsDao)ctx.getBean("newsDao" , NewsDao.class);
		dao.insert("疯狂Java" , "轻量级Java EE企业应用实战");
	}
}
```

# FAQ

## 只读事务 @Transactional(readOnly = true)

如果你一次执行单条查询语句，则没有必要启用事务支持，数据库默认支持SQL执行期间的读一致性； 

如果你一次执行多条查询语句，例如统计查询，报表查询，在这种场景下，多条查询SQL必须保证整体的读一致性，否则，在前条SQL查询之后，后条SQL查询之前，数据被其他用户改变，则该次整体的统计查询将会出现读数据不一致的状态，此时，应该启用事务支持。

参考：https://blog.csdn.net/xingqibaing/article/details/82838146