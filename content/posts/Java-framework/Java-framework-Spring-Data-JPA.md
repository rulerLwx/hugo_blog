---
title: "Java-framework-Spring-Data-JPA"
date: 2019-08-17T11:10:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---

# 概述

Spring Data JPA 底层使用 Hibernate JPA 来实现（所以**要导入 Hibernate 相关包**）。

Spring Data JPA 技术特点：只需要定义接口并继承 Spring Data JPA 提供的接口就行，不需要写接口实现类。

# 官方示例

官方示例代码：https://github.com/spring-projects/spring-data-examples

PS：官方示例并没有导入 Hibernate 相关包，但自己的示例中要导入。


# 博客

https://www.cnblogs.com/dreamroute/p/5173896.html

https://www.cnblogs.com/zeng1994/p/7575606.html


# 入门示例

## 步骤一：导入依赖

父pom，使用了BOM，子模块相关项不用写版本

```xml
    <properties>
        <java.version>1.8</java.version>
        <spring.version>4.3.24.RELEASE</spring.version>
        <spring-data.version>Ingalls-SR22</spring-data.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-framework-bom</artifactId>
                <version>${spring.version}</version>
                <scope>import</scope>
                <type>pom</type>
            </dependency>
            <dependency>
                <groupId>org.springframework.data</groupId>
                <artifactId>spring-data-releasetrain</artifactId>
                <version>${spring-data.version}</version>
                <scope>import</scope>
                <type>pom</type>
            </dependency>
        </dependencies>
    </dependencyManagement>
```

子模块：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>spring-data-demo</artifactId>
        <groupId>com.demo</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>spring-data-jpa</artifactId>

    <properties>
        <hibernate.version>5.0.12.Final</hibernate.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.data</groupId>
            <artifactId>spring-data-jpa</artifactId>
        </dependency>
        <dependency>
            <groupId>com.mchange</groupId>
            <artifactId>c3p0</artifactId>
            <version>0.9.5.4</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
        </dependency>
        <!--hibernate-->
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-core</artifactId>
            <version>${hibernate.version}</version>
        </dependency>
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-entitymanager</artifactId>
            <version>${hibernate.version}</version>
        </dependency>
        <!--mysql drive-->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.19</version>
        </dependency>
    </dependencies>

</project>
```

注意版本的要求：http://hibernate.org/orm/documentation/getting-started/

```txt
Hibernate 5.2 and later versions require at least Java 1.8 and JDBC 4.2.
Hibernate 5.1 and older versions require at least Java 1.6 and JDBC 4.0.
```



## 步骤二：Spring配置文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context" xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:jpa="http://www.springframework.org/schema/data/jpa"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context https://www.springframework.org/schema/context/spring-context.xsd http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd http://www.springframework.org/schema/data/jpa http://www.springframework.org/schema/data/jpa/spring-jpa.xsd">

    <context:property-placeholder location="classpath:jdbc.properties"/>
    <!-- 配置c3p0 数据库连接池-->
    <bean id="dataSource"
          class="com.mchange.v2.c3p0.ComboPooledDataSource">
        <property name="jdbcUrl" value="${jdbc.mysql.url}"/>
        <property name="driverClass" value="${jdbc.mysql.driverClass}"/>
        <property name="user" value="${jdbc.mysql.user}"/>
        <property name="password" value="${jdbc.mysql.password}"/>
    </bean>
    <!-- Spring 整合JPA 配置EntityManagerFactory-->
    <bean id="entityManagerFactory"
          class="org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean">
        <property name="dataSource" ref="dataSource"/>
        <property name="jpaVendorAdapter">
            <bean class="org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter">
                <!-- hibernate 相关的属性的注入-->
                <!-- 配置数据库类型-->
                <property name="database" value="MYSQL"/>
                <!-- 正向工程自动创建表-->
                <property name="generateDdl" value="true"/>
                <!-- 显示执行的SQL -->
                <property name="showSql" value="true"/>
            </bean>
        </property>
        <!-- 扫描实体的包-->
        <property name="packagesToScan">
            <list>
                <value>com.demo.pojo</value>
            </list>
        </property>
    </bean>
    <!-- 配置Hibernate 的事务管理器-->
    <bean id="transactionManager" class="org.springframework.orm.jpa.JpaTransactionManager">
        <property name="entityManagerFactory" ref="entityManagerFactory"/>
    </bean>
    <!-- 配置开启注解事务处理-->
    <tx:annotation-driven transaction-manager="transactionManager"/>
    <!-- 配置springIOC 的注解扫描-->
    <context:component-scan base-package="com.demo"/>
    <!-- Spring Data JPA 的配置-->
    <!-- base-package：扫描dao 接口所在的包-->
    <jpa:repositories base-package="com.demo.dao"/>
</beans>
```

```properties
jdbc.mysql.driverClass=com.mysql.jdbc.Driver
jdbc.mysql.url=jdbc:mysql://localhost:3306/test
jdbc.mysql.user=root
jdbc.mysql.password=root
```

## 步骤三：写DAO和实体类

```java
public interface UserDao extends JpaRepository<User,Integer> {

    /**
     * JpaRepository<T,D>，T表示对应哪个实体，D表示实体的id类型
     */
}
```

```java
@Entity
@Table(name="user")
public class User implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;

    //omitted for set/get/toString
}
```


## 步骤四：测试

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class UsersDaoImplTest {
    @Autowired
    private UserDao usersDao;
    /**
     * 添加用户
     */
    @Test
    @Transactional// 在测试类对于事务提交方式默认的是回滚。
    @Rollback(false)//取消自动回滚
    public void testInsertUsers(){
        User user = new User();
        user.setName("张三丰");
        this.usersDao.save(user);
    }
}
```

# 底层原理

## JpaRepository 接口继承结构

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708092347.png)

org.springframework.data.repository.CrudRepository.save()方法有增加、更新两重意思。


## DAO接口不写实现的原理

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class UsersDaoImplTest {
    @Autowired
    private UserDao usersDao;

    @Test
    public void test(){
        //org.springframework.data.jpa.repository.support.SimpleJpaRepository@2ab2710
        System.out.println(this.usersDao);
        //class com.sun.proxy.$Proxy29
        System.out.println(this.usersDao.getClass());
    }
}
```

UserDao 接口是 SimpleJpaRepository 的代理。

# Repository 接口

Repository 接口是Spring Data JPA 中为我们提供的所有接口中的顶层接口

Repository 提供了两种查询方式的支持

- 1）基于方法名称命名规则查询
- 2）基于@Query 注解查询


## 1）基于方法名称命名规则查询

命名规则：findBy(关键字)+属性名称(属性名称的首字母大写)+查询条件(首字母大写)

关键字 | 方法命名 | sql where 字句
---|---|---
And | findByNameAndPwd | where name= ? and pwd =?
Or | findByNameOrSex | where name= ? or sex=?
Is,Equal | findById,findByIdEquals | where id= ?
Between | findByIdBetween | where id between ? and ?
LessThan | findByIdLessThan | where id < ?
LessThanEqual | findByIdLessThanEquals | where id <= ?
GreaterThan | findByIdGreaterThan | where id > ?
GreaterThanEqual | findByIdGreaterThanEquals | where id > = ?
After | findByIdAfter | where id > ?
Before | findByIdBefore | where id < ?
IsNull | findByNameIsNull | where name is null
isNotNull,Not | findByNameNotNull | where name is not
Like | findByNameLike | where name like ?
NotLike | findByNameNotLike | where name not like ?
StartingWith | findByNameStartingWith | where name like '?%'
EndingWith | findByNameEndingWith | where name like '%?'
Containing | findByNameContaining | where name like '%?%'
OrderBy | findByIdOrderByXDesc | where id=? order by x desc
Not | findByNameNot | where name <> ?
In | findByIdIn(Collection<?> c) | where id in (?)
NotIn | findByIdNotIn(Collection<?> c) | where id not in (?)
True | findByAaaTue | where aaa = true
False | findByAaaFalse | where aaa = false
IgnoreCase | findByNameIgnoreCase | where UPPER(name)=UPPER(?)

参考：https://docs.spring.io/spring-data/jpa/docs/1.11.22.RELEASE/reference/html/#jpa.query-methods.query-creation

示例：

```java
//省……
```

总之一句话，DAO 接口继承 Repository 接口，然后在 DAO 接口中写的方法名要遵循命名规则


## 2）基于 @Query 注解查询

官方示例：https://docs.spring.io/spring-data/jpa/docs/1.11.22.RELEASE/reference/html/#jpa.query-methods.at-query

我的示例：

```java
public interface UserDao extends Repository<User, Integer> {
    //JPQL
    @Query(value = "select u from User u where u.name = ?1")
    List<User> queryUserByName(String name);

    //SQL
    @Query(value = "select * from user where name = ? ",nativeQuery = true)
    List<User> queryUserByName2(String name);
}
```

```java
    @Test
    public void test1(){
        List<User> userList = usersDao.queryUserByName("王五");
        for (User u : userList) {
            System.out.println(u.getName());
        }
    }
```

## 3）基于 @Query 注解执行更新、删除操作

```java
public interface UserDao extends Repository<User, Integer> {
    //update
    @Modifying
    @Query("update User u set u.name = ?1 where u.id = ?2")
    int updateUserName(String newName, Integer id);

    //delete
    @Modifying
    @Query("delete from User u where u.id = ?1")
    int deleteUser(Integer id);
}
```

```java
    //更新用户
    @Test
    @Transactional// 在测试类对于事务提交方式默认的是回滚。
    @Rollback(false)//取消自动回滚
    public void test2(){
        this.usersDao.updateUserName("李想", 46);
    }

    //删除
    @Test
    @Transactional// 在测试类对于事务提交方式默认的是回滚。
    @Rollback(false)//取消自动回滚
    public void test3(){
        this.usersDao.deleteUser(46);
    }
```


官方示例：https://docs.spring.io/spring-data/jpa/docs/1.11.22.RELEASE/reference/html/#jpa.modifying-queries

# CrudRepository 接口

## CRUD 操作

```java
public interface UserDao extends CrudRepository<User, Integer> {

}
```


```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class UsersDaoImplTest {
    @Autowired
    private UserDao usersDao;

    /**
     * 添加单条数据
     *
     * 使用CrudRepository，不用添加事务，因为它已经添加
     */
    @Test
    public void test1(){
        User user = new User();
        user.setName("张飞");
        this.usersDao.save(user);
    }

    /**
     * 批量添加
     *
     * JDK中的集合都实现了 Iterable 接口，所以可以将集合作为入参
     */
    @Test
    public void test2(){
        User user = new User();
        user.setName("张小飞");

        User user1 = new User();
        user1.setName("王二虎");

        ArrayList<User> list = new ArrayList<>();
        list.add(user);
        list.add(user1);

        this.usersDao.save(list);
    }

    /**
     * 查找单条数据
     */
    @Test
    public void test3(){
        User user = this.usersDao.findOne(50);
        System.out.println(user);
    }

    /**
     * 查找多条数据
     */
    @Test
    public void test4(){
        List<User> list = (List) this.usersDao.findAll();
        for (User u : list) {
            System.out.println(u);
        }
    }

    /**
     * 更新方式一
     */
    @Test
    public void test5(){
        User user = this.usersDao.findOne(48);
        user.setName("王小小");
        this.usersDao.save(user);
    }

    /**
     * 更新方式二
     */
    @Test
    @Transactional
    @Rollback(false)
    public void test6(){
        User user = this.usersDao.findOne(49);//持久化状态
        user.setName("王小五");
        //this.usersDao.save(user);//不需要执行保存，但在测试类中要开事务
    }
}
```

# PagingAndSortingRepository 接口

PagingAndSortingRepository 接口对表中的所有数据执行分页、排序操作，不是对一部分操作，而是所有数据。

## 分页

```java
public interface UserDao extends PagingAndSortingRepository<User, Integer> {

}
```

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class UsersDaoImplTest {
    @Autowired
    private UserDao usersDao;

    @Test
    public void test(){
        int page = 0;//当前页，索引从0开始
        int size = 5;//每页条数
        Pageable pageable = new PageRequest(page,size);
        Page<User> p = this.usersDao.findAll(pageable);
        System.out.println("总条数："+p.getTotalElements());
        System.out.println("总页数："+p.getTotalPages());
        List<User> userList = p.getContent();
        for (User u : userList) {
            System.out.println(u);
        }
    }
}
```


## 排序

```java
    /**
     * 对单列排序
     */
    @Test
    public void test2(){
        //sort对象封装了排序规则和指定排序字段（对象的属性）
        //direction：排序规则，升序、降序
        //properties：根据哪个字段来排序
        Sort sort = new Sort(Sort.Direction.ASC, "id");
        List<User> list = (List<User>) this.usersDao.findAll(sort);
        for (User u : list) {
            System.out.println(u);
        }
    }

    /**
     * 对多列排序
     */
    @Test
    public void test3(){
        //sort对象封装了排序规则和指定排序字段（对象的属性）
        //direction：排序规则，升序、降序
        //properties：根据哪个字段来排序
        Sort.Order order1 = new Sort.Order(Sort.Direction.ASC, "id");
        Sort.Order order2 = new Sort.Order(Sort.Direction.DESC, "name");
        Sort sort = new Sort(order1,order2);
        List<User> list = (List<User>) this.usersDao.findAll(sort);
        for (User u : list) {
            System.out.println(u);
        }
    }
```


# JpaRepository 接口

JpaRepository 接口的最大作用是对父接口的返回值做适配处理（很多返回值是 List ）

开发中推荐继承 JpaRepository 接口，而不是继承上面的父接口。


# JpaSpecificationExecutor 接口

JpaSpecificationExecutor 接口的主要作用：完成多条件查询，并且支持分页与排序

不能单独使用，需要结合其它接口来使用

```java
public interface UserDao extends JpaRepository<User, Integer>,JpaSpecificationExecutor<User> {

}
```

## 单条件查询

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class UsersDaoImplTest {
    @Autowired
    private UserDao userDao;

    /**
     * 单条件查询
     *
     *需求：根据用户姓名查询数据
     */
    @Test
    public void test1(){
        Specification<User> spec = new Specification<User>() {
            /**
             * @param root：根对象，封装了查询条件的对象
             * @param criteriaQuery：定义了一个基本的查询.一般不使用
             * @param criteriaBuilder：创建一个查询条件
             *
             * @return Predicate:定义了查询条件
             */
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Predicate pre = criteriaBuilder.equal(root.get("name"), "王五");
                return pre;
            }
        };
        List<User> list = this.userDao.findAll(spec);
        for (User u : list) {
            System.out.println(u);
        }
    }
}
```

## 多条件查询

```java
    /**
     * 多条件查询方式一
     *
     * 需求：使用用户姓名以及年龄查询数据
     */
    @Test
    public void test2(){
        Specification<User> spec = new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder cb) {
                List<Predicate> list = new ArrayList<>();
                list.add(cb.equal(root.get("name"),"王五"));
                list.add(cb.equal(root.get("age"),24));//此时条件之间是没有任何关系的。
                Predicate[] arr = new Predicate[list.size()];
                return cb.and(list.toArray(arr));
            }
        };
        List<User> list = this.userDao.findAll(spec);
        for (User u : list) {
            System.out.println(u);
        }
    }

    /**
     * 多条件查询方式二
     *
     * 需求：使用用户姓名或者年龄查询数据
     */
    @Test
    public void test3(){
        Specification<User> spec = new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder cb) {
                return cb.or(cb.equal(root.get("name"),"王五"),cb.equal(root.get("age"), 25));
            }
        };
        List<User> list = this.userDao.findAll(spec);
        for (User u : list) {
            System.out.println(u);
        }
    }
```


## 分页

```java
    /**
     * 分页
     *
     * 需求：查询王姓用户，并且做分页处理
     */
    @Test
    public void test4(){
        Specification<User> spec = new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder cb) {
                return cb.like(root.get("name").as(String.class),"王%");
            }
        };
        //分页
        Pageable pageable = new PageRequest(0, 2);
        Page<User> page = this.userDao.findAll(spec, pageable);
        System.out.println("总条数："+page.getTotalElements());
        System.out.println("总页数："+page.getTotalPages());
        List<User> list = page.getContent();
        for (User u : list) {
            System.out.println(u);
        }
    }
```


## 排序

```java
    /**
     * 排序
     *
     * 需求：查询数据库中王姓的用户，并且根据用户id 做倒序排序
     */
    @Test
    public void test5(){
        Specification<User> spec = new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder cb) {
                return cb.like(root.get("name").as(String.class),"王%");
            }
        };
        //排序
        Sort sort = new Sort(Sort.Direction.DESC,"id");
        List<User> list = this.userDao.findAll(spec, sort);
        for (User u : list) {
            System.out.println(u);
        }
    }
```

## 分页与排序

```java
    /**
     * 分页与排序
     *
     * 需求：查询数据库中王姓的用户，做分页处理，并且根据用户id 做倒序排序
     */
    @Test
    public void test6(){
        //排序等定义
        Sort sort = new Sort(Sort.Direction.DESC,"id");
        //分页的定义
        Pageable pageable = new PageRequest(0,5, sort);
        //查询条件
        Specification<User> spec = new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> root,
                                         CriteriaQuery<?> query, CriteriaBuilder cb) {
                return cb.like(root.get("name").as(String.class),"王%");
            }
        };
        Page<User> page = this.userDao.findAll(spec, pageable);
        System.out.println("总条数："+page.getTotalElements());
        System.out.println("总页数："+page.getTotalPages());
        List<User> list = page.getContent();
        for (User user : list) {
            System.out.println(user);
        }
    }
```

## 总结

在上面示例中，Specification 中的参数都是写死的，不符合实际。在实际开发中参数由 DAO 入参传入，类似：

```java
public List<WeChatGzUserInfoEntity> findByCondition(Date minDate, Date maxDate, String nickname){
        List<WeChatGzUserInfoEntity> resultList = null;
        Specification querySpecifi = new Specification<WeChatGzUserInfoEntity>() {
            @Override
            public Predicate toPredicate(Root<WeChatGzUserInfoEntity> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

                List<Predicate> predicates = new ArrayList<>();
                if(null != minDate){
                    predicates.add(criteriaBuilder.greaterThan(root.get("subscribeTime"), minDate));
                }
                if(null != maxDate){
                    predicates.add(criteriaBuilder.lessThan(root.get("subscribeTime"), maxDate));
                }
                if(null != nickname){
                    predicates.add(criteriaBuilder.like(root.get("nickname"), "%"+nickname+"%"));
                }
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
        resultList =  this.weChatGzUserInfoRepository.findAll(querySpecifi);
        return resultList;
    }
```

参考：http://www.cnblogs.com/derry9005/p/6282571.html



# 自定义 Repository 接口

目的：上面的5个接口不能满足我们的需求时，就自定义 Repository 接口。

怎么做呢？

1）创建自定义接口

```java
public interface UserRepository {
    public User findUserById(Integer id);
}
```

2）使用自定义接口

  使用上面自定义的接口

```java
public interface UserDao extends JpaRepository<User, Integer>,JpaSpecificationExecutor<User>,UserRepository {

}
```

3）创建接口实现类

  实现的接口是 UserDao，即实现将要交给 JPA 产生代理的接口；接口命名规则：以 Impl 结尾。
  
  `implements UserRepository` 其实就是实现 UserDao 接口，因为其它5个接口不用我们实现。

```java
public class UserDaoImpl implements UserRepository {

    @PersistenceContext(name="entityManagerFactory")
    EntityManager em;

    @Override
    public User findUserById(Integer id) {
        System.out.println("MyRepository.......");
        return em.find(User.class,id);
    }
}
```


4）测试

  测试跟前5个接口没有区别

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class UsersDaoImplTest {
    @Autowired
    private UserDao userDao;

    @Test
    public void test(){
        User user = this.userDao.findUserById(10);
        System.out.println(user);
    }
}
```

参考自己的代码：spring-data-jpa-myrepository


# 关联映射操作

## 一对一

需求：用户与角色的一对一的关联关系

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708092455.png)

在 User 端维护外键。


## 一对多

需求：从角色到用户的一对多关联关系

梳理关系：一个角色可以赋给多个用户，多个用户可以拥有一个角色（一个用户只能拥有一个角色） —— 20190520。

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708092514.png)

在多的一方（User）维护外键


##  多对多

需求：一个角色可以拥有多个菜单，一个菜单可以分配多个角色。多对多的关联关系。

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708092533.png)

中间表在哪边配都一样

`@ManyToMany(cascade = CascadeType.PERSIST,fetch = FetchType.EAGER)` 开启关联操作、关闭延迟加载

---

更多知识可以阅读《Spring Data JPA从入门到精通》等书