---
title: "Java-framework-MyBatis-2-关联查询"
date: 2019-08-05T11:03:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---

# 关联查询

## 商品订单数据模型

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708080942.png)

## 一对一查询

案例：查询所有订单信息，关联查询下单用户信息。

注意：因为一个订单信息只会是一个人下的订单，所以从查询订单信息出发关联查询用户信息为一对一查询。如果从用户信息出发查询用户下的订单信息则为一对多查询，因为一个用户可以下多个订单。

### 方法一：使用resultType

sql语句：

```sql
SELECT 
  orders.*,
  user.username,
  user.address
FROM
  orders,
  user 
WHERE orders.user_id = user.id
```


- 定义po类

    此po类中包括了订单信息和用户信息，包括上边sql查询出来的所有字段，
    
    OrdersCustom类继承Orders类后OrdersCustom类包括了Orders类的所有字段，只需要定义用户的信息字段即可。

```java
    public class OrdersCustom extends Orders {
    
    	private String username;// 用户名称
    	private String address;// 用户地址
    
        //ommitted get/set...
```


- Mapper.xml

```xml
	<select id="findOrdersList" resultType="cn.itcast.mybatis.po.OrdersCustom">
    	SELECT
    	orders.*,
    	user.username,
    	user.address
    	FROM
    	orders,	user
    	WHERE orders.user_id = user.id 
	</select>
```

- Mapper接口

```java
    public List<OrdersCustom> findOrdersList() throws Exception;
```

- 测试

```java
    Public void testfindOrdersList()throws Exception{
		//获取session
		SqlSession session = sqlSessionFactory.openSession();
		//获限mapper接口实例
		UserMapper userMapper = session.getMapper(UserMapper.class);
		//查询订单信息
		List<OrdersCustom> list = userMapper.findOrdersList();
		System.out.println(list);
		//关闭session
		session.close();
	}
```

定义专门的po类作为输出类型，其中定义了sql查询结果集所有的字段。此方法较为简单，企业中使用普遍


### 方法二：使用resultMap

sql语句

```sql
    SELECT 
      orders.*,
      user.username,
      user.address
    FROM
      orders,
      user 
    WHERE orders.user_id = user.id
```

- 定义po类

    在Orders类中加入User属性，user属性中用于存储关联查询的用户信息，因为订单关联查询用户是一对一关系，所以这里使用单个User对象存储关联查询的用户信息。

- Mapper.xml

    这里resultMap指定userordermap

```xml
    <select id="findOrdersListResultMap" resultMap="userordermap">
    	SELECT
    	orders.*,
    	user.username,
    	user.address
    	FROM
    	orders,	user
    	WHERE orders.user_id = user.id 
	</select>
```

- 定义resultMap

    需要关联查询映射的是用户信息，使用association将用户信息映射到订单对象的用户属性中

```xml
    <resultMap type="cn.itcast.mybatis.po.Orders" id="userordermap">
        <!-- 这里的id，是mybatis在进行一对一查询时将user字段映射为user对象时要使用，必须写 -->
        <id property="id" column="id"/>
        <result property="user_id" column="user_id"/>
        <result property="number" column="number"/>
        <association property="user" javaType="cn.itcast.mybatis.po.User">
            <!-- 这里的id为user的id，如果写上表示给user的id属性赋值 -->
            <id property="id" column="user_id"/>
            <result property="username" column="username"/>
            <result property="address" column="address"/>
        </association>
    </resultMap>
```

    association：表示进行关联查询单条记录
    property：表示关联查询的结果存储在cn.itcast.mybatis.po.Orders的user属性中
    javaType：表示关联查询的结果类型
    <id property="id" column="user_id"/>：查询结果的user_id列对应关联对象的id属性，这里是<id />表示user_id是关联查询对象的唯一标识。
    <result property="username" column="username"/>：查询结果的username列对应关联对象的username属性。

- Mapper接口

```java
    public List<Orders> findOrdersListResultMap() throws Exception;
```

- 测试

```java
    Public void testfindOrdersListResultMap()throws Exception{
		//获取session
		SqlSession session = sqlSessionFactory.openSession();
		//获限mapper接口实例
		UserMapper userMapper = session.getMapper(UserMapper.class);
		//查询订单信息
		List<Orders> list = userMapper.findOrdersList2();
		System.out.println(list);
		//关闭session
		session.close();
	}
```

- 总结

    使用 association 完成关联查询，将关联查询信息映射到pojo对象中。




## 一对多查询

案例：查询所有订单信息及订单下的订单明细信息。

订单信息与订单明细为一对多关系。

使用resultMap实现

sql语句

```sql
    SELECT 
      orders.*,
      user.username,
      user.address,
      orderdetail.id orderdetail_id,
      orderdetail.items_id,
      orderdetail.items_num
    FROM
      orders,user,orderdetail
    
    WHERE orders.user_id = user.id 
    AND orders.id = orderdetail.orders_id
```

- 定义po类

    在Orders类中加入User属性。
    
    在Orders类中加入List<Orderdetail> orderdetails属性

- Mapper.xml

```xml
    <select id="findOrdersDetailList" resultMap="userorderdetailmap">
    	SELECT
    	orders.*,
    	user.username,
    	user.address,
    	orderdetail.id orderdetail_id,
    	orderdetail.items_id,
    	orderdetail.items_num
    	FROM orders,user,orderdetail
    	WHERE orders.user_id = user.id
    	AND orders.id = orderdetail.orders_id
	</select>
```

- 定义resultMap

```xml
    <resultMap type="cn.itcast.mybatis.po.Orders" id="userorderdetailmap">
        <id property="id"column="id"/>
        <result property="user_id" column="user_id"/>
        <result property="number" column="number"/>
        <association property="user" javaType="cn.itcast.mybatis.po.User">
            <id property="id" column="user_id"/>
            <result property="username" column="username"/>
            <result property="address" column="address"/>
        </association>
        <collection property="orderdetails" ofType="cn.itcast.mybatis.po.Orderdetail">
        	<id property="id" column="orderdetail_id"/>
        	<result property="items_id" column="items_id"/>
        	<result property="items_num" column="items_num"/>
        </collection>
    </resultMap>
```

另一种写法：

```xml
    <!--使用 extends 属性，重用了上面一对一关联查询时定义好的 resultmap -->
    <resultMap type="cn.itcast.mybatis.po.Orders" id="userorderdetailmap" extends="userordermap">
    <collection property="orderdetails" ofType="cn.itcast.mybatis.po.Orderdetail">
    	<id property="id" column="orderdetail_id"/>
    	<result property="items_id" column="items_id"/>
    	<result property="items_num" column="items_num"/>
    </collection>
    </resultMap>
```


- Mapper接口

```java
    public List<Orders>findOrdersDetailList () throws Exception;
```

- 测试

```java
    Public void testfindOrdersDetailList()throws Exception{
		//获取session
		SqlSession session = sqlSessionFactory.openSession();
		//获限mapper接口实例
		UserMapper userMapper = session.getMapper(UserMapper.class);
		//查询订单信息
		List<Orders> list = userMapper.findOrdersDetailList();
		System.out.println(list);
		//关闭session
		session.close();
	}
```




## 多对多查询

- 需求

    查询用户购买的商品信息

sql

```sql
    SELECT
    	orders.*, 
    	USER.username,
    	USER.address,
    	orderdetail.id orderdetail_id,
    	orderdetail.items_id,
    	orderdetail.items_num,
    	items.name items_name,
    	items.detail items_detail
    FROM
    	orders,
    	USER,
    	orderdetail,
    	items
    WHERE
    	orders.user_id = USER .id
    AND orders.id = orderdetail.orders_id
    AND orderdetail.items_id = items.id
```

- 定义po

    在User中添加List<Orders> orders 属性，在Orders类中加入List<Orderdetail> orderdetails属性

- resultMap

    需要关联查询映射的信息是：订单、订单明细、商品信息
    
    订单：一个用户对应多个订单，使用collection映射到用户对象的订单列表属性中
    
    订单明细：一个订单对应多个明细，使用collection映射到订单对象中的明细属性中
    
    商品信息：一个订单明细对应一个商品，使用association映射到订单明细对象的商品属性中。

```xml
	 <resultMap type="cn.itcast.mybatis.po.User" id="userOrderListResultMap">
	 	<id column="user_id" property="id"/>
		<result column="username" property="username"/>
	 	<collection property="orders" ofType="cn.itcast.mybatis.po.Orders">
	      <id  column="id" property="id"/>
	      <result property="number" column="number"/>
         	<collection property="orderdetails" ofType="cn.itcast.mybatis.po.Orderdetail">
         	 	<id  column="orderdetail_id" property="id"/>
          		<result property="ordersId" column="id"/>
         		<result property="itemsId" column="items_id"/>
          		<result property="itemsNum" column="items_num"/>
          		<association property="items" javaType="cn.itcast.mybatis.po.Items">
    	  			 <id column="items_id" property="id"/>
    	   			 <result column="items_name" property="name"/>
    	   			 <result column="items_detail" property="detail"/>
    			</association>
       		</collection>
	   </collection>
	 </resultMap>
```


## 延迟加载

需要查询关联信息时，使用mybatis延迟加载特性可有效的减少数据库压力，首次查询只查询主要信息，关联信息等用户获取时再加载。


### 延迟加载开关

在mybatis核心配置文件中配置

设置项 |	描述 |	允许值 |默认值
--- | --- |--- |---
lazyLoadingEnabled | 全局性设置懒加载。如果设为‘false’，则所有相关联的都会被初始化加载。 |	true <br/> false  |	false
aggressiveLazyLoading  | 当设置为‘true’的时候，懒加载的对象可能被任何懒属性全部加载。<br/> 否则，每个属性都按需加载。 |	true <br/>  false  |	true

例：

```xml
<settings>
		<setting name="lazyLoadingEnabled" value="true"/>
		<setting name="aggressiveLazyLoading" value="false"/>
</settings>
```


### 一对一查询延迟加载

查询订单信息，关联查询用户信息。

默认只查询订单信息，当需要查询用户信息时再去查询用户信息。

- 定义po类

    在Orders类中加入User属性。

- Mapper.xml

```
    <select id="findOrdersList3" resultMap="userordermap2">
    	SELECT
    	orders.*
    	FROM
    	orders
    </select>
```

- 定义resultMap

    看点：`select="findUserById"`

```
    <!-- 订单信息resultmap -->
    <resultMap type="cn.itcast.mybatis.po.Orders" id="userordermap2">
        <id property="id" column="id"/>
        <result property="user_id" column="user_id"/>
        <result property="number" column="number"/>
        <association property="user" javaType="cn.itcast.mybatis.po.User" select="findUserById" column="user_id"/>
    </resultMap>
```

- Mapper接口

```
    Public void testfindOrdersList3()throws Exception{
		//获取session
		SqlSession session = sqlSessionFactory.openSession();
		//获限mapper接口实例
		UserMapper userMapper = session.getMapper(UserMapper.class);
		//查询订单信息
		List<Orders> list = userMapper.findOrdersList3();
		System.out.println(list);
		//开始加载，通过orders.getUser方法进行加载
        for(Orders orders:list){
			System.out.println(orders.getUser());
		}
		//关闭session
		session.close();
	}
```

- 测试

```
    Public void testfindOrdersList3()throws Exception{
		//获取session
		SqlSession session = sqlSessionFactory.openSession();
		//获限mapper接口实例
		UserMapper userMapper = session.getMapper(UserMapper.class);
		//查询订单信息
		List<Orders> list = userMapper.findOrdersList3();
		System.out.println(list);
		//开始加载，通过orders.getUser方法进行加载
        for(Orders orders:list){
			System.out.println(orders.getUser());
		}
		//关闭session
		session.close();
	}
```

- 延迟加载的思考

不使用mybatis提供的延迟加载功能是否可以实现延迟加载？

实现方法：

针对订单和用户两个表定义两个mapper方法。

1、订单查询mapper方法

2、根据用户id查询用户信息mapper方法

默认使用订单查询mapper方法只查询订单信息。

当需要关联查询用户信息时再调用根据用户id查询用户信息mapper方法查询用户信息。



### 一对多延迟加载

一对多延迟加载的方法同一对一延迟加载，在collection标签中配置select内容。

### 小结

- 作用

	当需要查询关联信息时再去数据库查询，默认不去关联查询，提高数据库性能。**只有resultMap支持延迟加载**设置。

- 场合

	当只有部分记录需要关联查询其它信息时，此时可按需延迟加载，需要关联查询时再向数据库发出sql，以提高数据库性能。

	当全部需要关联查询信息时，此时不用延迟加载，直接将关联查询信息全部返回即可，可使用resultType或resultMap完成映射。

