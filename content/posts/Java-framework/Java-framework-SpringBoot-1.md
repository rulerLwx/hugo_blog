---
title: "Java-framework-SpringBoot-1"
date: 2019-08-20T11:11:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---

# 知识点

## Demo

目录结构：

![](https://oscimg.oschina.net/oscnet/2ce460e38e10ee52024dbf08aa91cf00c78.jpg)

maven：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.itheima</groupId>
    <artifactId>springboot</artifactId>
    <version>1.0-SNAPSHOT</version>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.1.3.RELEASE</version>
    </parent>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>

</project>
```

引导类：

```java
@SpringBootApplication  //表明当前类是SpringBoot的引导类
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
```

controller：

```java
@RestController
@RequestMapping("/springmvc")
public class HelloController {

    @RequestMapping("/hello")
    public String sayHello(){
        return "hello spring boot";
    }
}
```

验证：运行Application类的main方法，在浏览器访问：http://localhost:8080/springmvc/hello

---

疑问：导入依赖时，`spring-boot-dependencies`与`spring-boot-start-parent`的区别？

总结：在真实的企业级项目，我们可能会有自己的父项目，不想依赖Spring提供的父项目

参考：https://www.cnblogs.com/yaowen/p/8622507.html

示例：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>Test202001</artifactId>
        <groupId>org.example</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>SpringBootTest</artifactId>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <!-- Import dependency management from Spring Boot -->
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>2.1.3.RELEASE</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
</project>
```

---

## 默认配置

- SpringBoot默认加载Application所在包（根包）及其子包中的类。
- SpringBoot配置：resources下的application.properties文件；
- @EnableWebMvc 开启一些默认配置，如ViewResolver或MessageConverter。

## 关闭某一项自动配置

如果我们不需要Spring Boot自动配置，想关闭某一项的自动配置，该如何设置呢？

比如：我们不想自动配置Redis，想手动配置

```java
@SpringBootApplication(exclude = {RedisAutoConfiguration.class  })
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class,args);
    }
}
```

其它的类推


## 条件注解

例：org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration是Redis的自动配置

![](https://oscimg.oschina.net/oscnet/4c8ecc9a9694f458b0c2a5b7484419fae33.jpg)
![](https://oscimg.oschina.net/oscnet/30d118c03f548983cb1d5013c006ca7dbea.jpg)
![](https://oscimg.oschina.net/oscnet/16c46208d9d780b1fd279e3f4c3ff692abb.jpg)



## 访问静态资源

学习资料：

- https://ke.qq.com/webcourse/index.html#cid=403245&term_id=100480897&taid=3443636058859309&type=1024&vid=c1432lg3pfz

### 所有的 /webjars/**

所有的 /webjars/**，都去 classpath:/META-INF/resources/webjars/jquery/ 找资源

即：localhost:8080/webjars/jquery/3.3.1/jquery.js，直接访问静态资源 ——20190813

```xml
        <dependency>
            <groupId>org.webjars</groupId>
            <artifactId>jquery</artifactId>
            <version>3.3.1</version>
        </dependency>
```

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708100311.png)


###  /**（访问任何路径）

~~如果进入SpringMVC的规则为/时，Spring Boot的默认静态资源的路径为：~~

默认的静态资源位置：

spring.resources.static-locations=

- classpath:/META-INF/resources/,
- classpath:/resources/,
- classpath:/static/,
- classpath:/public/

例如，在resource下的static目录下文件可以通过浏览器路径直接访问

如：static/img/a.jpg，可以通过 http://localhost:8080/img/a.jpg 访问

将静态资源放置到webapp下的static目录中也可通过地址访问

![](https://oscimg.oschina.net/oscnet/2414626b61b9dc12db095a842436e7fb13a.jpg)

测试：

![](https://oscimg.oschina.net/oscnet/234852832aa8ec0f61e0c93db7355965cc8.jpg)


## 自定义消息转化器

自定义消息转化器，只需要在@Configuration的类中添加消息转化器的@bean加入到Spring容器，就会被Spring Boot自动加入到容器中

```java
    @Bean
    public StringHttpMessageConverter stringHttpMessageConverter(){
        StringHttpMessageConverter converter  = new StringHttpMessageConverter(Charset.forName("UTF-8"));
        return converter;
    }
```


## 自定义SpringMVC的配置

有些时候我们需要自已配置SpringMVC而不是采用默认，比如说增加一个拦截器，这个时候就得通过继承WebMvcConfigurerAdapter然后重写父类中的方法进行扩展。

```java
import java.nio.charset.Charset;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration //申明这是一个配置
public class MySrpingMVCConfig extends WebMvcConfigurerAdapter{

    // 自定义拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        HandlerInterceptor handlerInterceptor = new HandlerInterceptor() {
            @Override
            public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
                    throws Exception {
                System.out.println("自定义拦截器............");
                return true;
            }
            
            @Override
            public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                    ModelAndView modelAndView) throws Exception {
                
            }
            
            @Override
            public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
                    Exception ex) throws Exception {
            }
        };
        registry.addInterceptor(handlerInterceptor).addPathPatterns("/**");
    }

    // 自定义消息转化器的第二种方法
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        StringHttpMessageConverter converter  = new StringHttpMessageConverter(Charset.forName("UTF-8"));
        converters.add(converter);
    }

}
```
注意：这个类上不能有@EnableWebMvc，否则就相当于所有的MVC都是我们自己配置，跟自己搭建SSM框架没区别。

参考：https://docs.spring.io/spring-boot/docs/1.5.21.RELEASE/reference/htmlsingle/#boot-features-spring-mvc-auto-configuration


## 异常处理

定义一个异常类让SpringBoot扫描到即可。

```java
@ControllerAdvice
public class GlobleExceptionApp {

    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public String runtimeException(){
        return "出现异常，相当于以前的AOP拦截";
    }
}
```

## 热部署

加入依赖包：

```xml
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
        </dependency>
```

启动项目，文件修改后按ctrl+f9重新加载即可实现热部署。

## 打war包单独发布

1）将工程打包方式改为`<packaging>war</packaging>`

2）将spring-boot-starter-tomcat的范围设置为`provided`

```xml
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-tomcat</artifactId>
            <scope>provided</scope>
        </dependency>
```

3）设置启动配置：集成`SpringBootServletInitializer`，然后重写`configure`，将Spring Boot的入口类设置进去。

```java
public class ServletInitializer extends SpringBootServletInitializer{
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(App.class);
    }
}
```

此入口类跟app类放同级目录

注意：访问时带上项目名称，http://localhost:8080/SpringBootTest-1.0-SNAPSHOT/test/hello

---

其原理是：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708100346.png)

参考：https://ke.qq.com/webcourse/index.html#cid=403245&term_id=100480897&taid=3443850807224109&type=1024&vid=q1432g6c5v3

---

spring boot 项目打包时，连依赖的jar包一起打包，需要添加一个插件 ——20190623

```xml
	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</build>
```


# 全局配置文件：.properties/yml

## 文件位置与优先级

全局的配置文件application.properties/yml，一般在resources目录下或者类路径下的/config下，一般放到resources下

默认配置文件位置加载的优先级从高到低：

1. file:/config/，备注：项目根目录下的config文件夹
2. file:/
3. classpath:/config/，备注：resource目录下的config文件夹
4. classpath:/
5. 或者通过配置spring.config.location来改变默认配置（运维）

高级的会覆盖低级的配置，低优先级的也加载，形成**互补配置**

编辑application.properties文件，修改或设置全局配置：

项目中同时有application.properties和application.yml文件，哪个优先？

答：两个配置文件都加载，如果有相同的配置，以application.properties配置为准。——20190607


## 日志

默认使用的是 SLF4J + logback

可以不另外写日志的配置文件，而在application.properties中做配置，当然另外写一个logback.xml也没问题

```properties
#设置日志级别
logging.level.org.springframework=DEBUG

#格式
logging.level.*= # Log levels severity mapping. For instance `logging.level.org.springframework=DEBUG`
```

关于日志，参考笔记《Java-Senior-日志》。


## Profile多环境支持

作用：多环境支持，开发环境、生产环境

1）在resource目录下可以写多个配置文件

application-[profile].properties/yml，如application-dev.properties，application-product.properties

2）激活profile

方式一：在application.properties中激活profile，启动引导类即可

```properties
spring.profiles.active=dev
```

方式二：在idea中配置启动参数`--spring.profiles.active=eureka1`

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708100407.png)

它的原型是`java -jar xxxx.jar --spring.profiles.active=dev`

或配置虚拟机参数`-Dspring.profiles.active=dev`

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708100439.png)

方式三：在linux上，todo...

```

```


## 配置文件占位符

application.properties配置文件中的属性的值可以是占位符

1）随机数

```
person.name=${random.value}

${random.int}
${random.long}
${random.int(10)}
${random.int(1024,65536)}
```

2）占位符获取之前配置的值，如果没有可以用`:`指定默认值

```
person.name=张三
person.dog.name=${person.name:hello}_dog
```


## 写配置文件时有提示

添加一个依赖

```xml
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-configuration-processor</artifactId>
        <optional>true</optional>
    </dependency>
```


## 其它常用配置

- 修改tomcat端口号

```properties
server.port = 8088
```

- 修改项目访问路径

```properties
server.context-path=/springboot-demo
```

- 应用名称？——20191011

这个配置很重要，在分布式微服务中，向外暴露的应用名称 ——20191026

```
spring.application.name=spring-cloud-mq
```

- 修改进入DispatcherServlet的规则为：*.html

```properties
server.servlet-path = *.html
```

参考：

https://blog.csdn.net/m0_37893932/article/details/79665148，

https://blog.csdn.net/qq_33802316/article/details/78267336

---

更多配置：https://docs.spring.io/spring-boot/docs/2.1.4.RELEASE/reference/htmlsingle/#common-application-properties


# Java配置：@Configuration

## @ImportResource：加载xml配置文件

Spring Boot提倡零配置，但在实际项目中，可能有一些特殊要求你必须使用xml，Spring提供@ImportResource来加载xml配置

```java
@ImportResource({"classpath:some-context.xml","classpath:another-context.xml"})
```

## @PropertySource：加载properties文件

```

```

## @Value("字面量/${}/#{SpEL}")


```java
    @Value("${person.username}")
    private String username;
```

相当于<bean>中的<property>的value配置

```xml
    <bean class="">
        <property name="" value=""/>
    </bean>
```

一般配合@PropertySource一起使用，如果没有使用@PropertySource指定文件，默认可以从application.properties中取值


## @ConfigurationProperties(prefix="")

在application.peroperties/yml文件中配置以person为开头的属性，然后在任何类上使用此注解，就会自动将配置的属性值自动注入到对应属性上

```
person.username=张三
person.age=24
```

```java
@Component
@ConfigurationProperties(prefix = "person")
public class MyConfig {

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "MyConfig{" +
                "username='" + username + '\'' +
                '}';
    }
}
```

@ConfigurationProperties与@Value区别

 功能 | @ConfigurationProperties | @Value | 备注
---|---|---|---
批量注入 | 批量注入配置文件中的属性 | 一个个指定 | 
松散绑定（松散语法，大小写） | 支持 | 不支持 | 
SpEL | 不支持 | 支持 | 
JSR303校验 | 支持 | 不支持 | 鸡肋-20190607
复杂类型（如，集合） | 支持 | 不支持 | 

## @ConditionalOnxxx

满足@ConditionalOnxxx指定的条件才进行下一步操作

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708100525.png)

SpringBoot自动配置时加载的文件在哪里？

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708100540.png)

如何知道自动配置了哪些东西？在application.properties文件中开启dubug模式：

```
dubug=true
```

效果如下：

```
============================
CONDITIONS EVALUATION REPORT
============================


Positive matches:
-----------------

   CodecsAutoConfiguration matched:
      - @ConditionalOnClass found required class 'org.springframework.http.codec.CodecConfigurer' (OnClassCondition)

   CodecsAutoConfiguration.JacksonCodecConfiguration matched:
      - @ConditionalOnClass found required class 'com.fasterxml.jackson.databind.ObjectMapper' (OnClassCondition)



Negative matches:
-----------------

   ActiveMQAutoConfiguration:
      Did not match:
         - @ConditionalOnClass did not find required class 'javax.jms.ConnectionFactory' (OnClassCondition)
```



# SSM集成

1)引入jar包

```xml
        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>2.0.0</version>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
        </dependency>
```

2）在application.properties文件配置数据库连接信息

```properties
spring.datasource.url=jdbc.mysql://localhost:3306/test
spring.datasource.username=root
spring.datasource.password=root
spring.datasource.driver-class-name=com.mysql.jdbc.Driver

mybatis.mapper-locations=classpath:mapper/*.xml
```

3）写Mapper接口，接口中写方法名，对应的mapper文件的namespace是Mapper接口的全类名；接口中的方法名跟xml文件中方法的id一致。

```java
    @Mapper
    public interface UserMapper {
        List<User> findUserById(Integer id);
    }
```

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.demo.boot.dao.UserMapper">
    <select id="findUserById" resultType="com.demo.boot.domain.User">
        select * from user where id = #{id}
    </select>
</mapper>
```

# Thymeleaf

前台模板有Thymeleaf/FreeMark/Groovy，全HTML，相当于静态文件。必须放在 src\main\resources\templates 下，文件夹名称**必须**是 templates，否则需要另行配置；

templates目录是安全的，外界不能直接访问该目录下的文件；参考《Java-framework-SpringBoot-3》。

SpringBoot推荐使用 Thymeleaf

```xml
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-thymeleaf</artifactId>
        </dependency>
```

默认的 thymeleaf 版本较低，最好修改 thymeleaf 版本：

```
    <properties>
        <thymeleaf.version>3.0.2.RELEASE</thymeleaf.version>
        <thymeleaf-layout-dialect.version>2.0.4</thymeleaf-layout-dialect.version>
    </properties>
```




# Q & A

## @Controller与@RestController的区别

@Controller比@RestController多了@ResponsBody注解。因此，@Controller返回页面，@RestController返回JSON格式数据。

## jdk版本

spring-boot-starter-web依赖包含了Spring版本，Spring版本以要求JDK的版本。

## Mysql数据库连接报错

Loading class `com.mysql.jdbc.Driver'. This is deprecated.

解决办法;

（1）把com.mysql.jdbc.Driver  替换成com.mysql.cj.jdbc.Driver

（2）另外，给JDBC的URL指定时区。

（3）指定SSL是true或者false。

```properties
jdbc.DriverClassName=com.mysql.cj.jdbc.Driver
jdbc.url =jdbc:mysql://localhost:3306/student?serverTimezone=UTC&useSSL=false
jdbc.username=root
jdbc.password=root
```
参考：https://blog.csdn.net/weixin_42323802/article/details/82589743


# 附件

## SpringBoot中各种starters依赖

https://docs.spring.io/spring-boot/docs/2.0.9.RELEASE/reference/htmlsingle/#using-boot-starter

## 微服务论文

https://martinfowler.com/

