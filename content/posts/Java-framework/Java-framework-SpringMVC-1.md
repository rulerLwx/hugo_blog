---
title: "Java-framework-SpringMVC-1"
date: 2019-08-16T11:10:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]

---

# 概述

SpringMVC通过一套MVC注解，让POJO成为处理请求的控制器，而无须实现任何接口。支持REST风格的URL请求。

MVC，即Model-View-Controller。Model指数据模型，可以是数据查询得到的数据，也可以是第三方接口返回的数据；View指页面，可以是JSP，Html；Controller指使用@Controller标注的类。


# Hello World

1、jar包。

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.demo</groupId>
  <artifactId>SpringMVCTest</artifactId>
  <packaging>war</packaging>
  <version>0.0.1-SNAPSHOT</version>
  <name>SpringMVCTest Maven Webapp</name>
  <url>http://maven.apache.org</url>
  <properties>
		<project.build.sourceEncoding>UTF8</project.build.sourceEncoding>
		<spring.version>4.0.8.RELEASE</spring.version>
		<slf4j.version>1.6.6</slf4j.version>
		<log4j.version>1.2.12</log4j.version>
		<mybatis.version>3.2.4</mybatis.version>
		<aspectj.version>1.8.10</aspectj.version>
	</properties>
	<dependencies>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.11</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>servlet-api</artifactId>
			<version>2.5</version>
		</dependency>
		<!-- springframe start -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-core</artifactId>
			<version>${spring.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-aop</artifactId>
			<version>${spring.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-web</artifactId>
			<version>${spring.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-oxm</artifactId>
			<version>${spring.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-tx</artifactId>
			<version>${spring.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-jdbc</artifactId>
			<version>${spring.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-webmvc</artifactId>
			<version>${spring.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context-support</artifactId>
			<version>${spring.version}</version>
		</dependency>

		<!-- springframe end -->
		
		<dependency>
			<groupId>commons-logging</groupId>
			<artifactId>commons-logging</artifactId>
			<version>1.1.1</version>
		</dependency>

		<!--使用AspectJ方式注解需要相应的包 -->
		<dependency>
			<groupId>org.aspectj</groupId>
			<artifactId>aspectjrt</artifactId>
			<version>${aspectj.version}</version>
		</dependency>
		<!--使用AspectJ方式注解需要相应的包 -->
		<dependency>
			<groupId>org.aspectj</groupId>
			<artifactId>aspectjweaver</artifactId>
			<version>${aspectj.version}</version>
		</dependency>
	</dependencies>
  <build>
    <finalName>SpringMVCTest</finalName>
  </build>
</project>
```

2、在web.xml中配置DispatcherServlet。

```xml
<!DOCTYPE web-app PUBLIC
 "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN"
 "http://java.sun.com/dtd/web-app_2_3.dtd" >

<web-app>
	<servlet>
		<servlet-name>springMVC3</servlet-name>
		<servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
		<init-param>
			<param-name>contextConfigLocation</param-name>
			<param-value>classpath:config/springmvc.xml</param-value>
		</init-param>
		<load-on-startup>1</load-on-startup>
	</servlet>
	<servlet-mapping>
		<servlet-name>springMVC3</servlet-name>
		<url-pattern>/</url-pattern>
	</servlet-mapping>
</web-app>

```

注意`<url-pattern>/</url-pattern>`是`/`而不是`/*`

默认配置文件名称问题：WEB-INF/[servlet的名称]-servlet.xml，[xxx]即为servlet的名称



3、加入SpringMVC配置文件`springmvc.xml`

需要配置两个：

1）<context:component-scan.../>自动扫描controller包。  
2）视图解析器：InternalResourceViewResolver，定义前缀、后缀。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xmlns:context="http://www.springframework.org/schema/context"
	xmlns:mvc="http://www.springframework.org/schema/mvc"
	xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
		http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.0.xsd
		http://www.springframework.org/schema/mvc http://www.springframework.org/schema/mvc/spring-mvc-4.0.xsd">
		
	<context:component-scan base-package="com.demo.controller"></context:component-scan>
	
	<!-- 定义视图文件解析 -->
    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="prefix" value="/WEB-INF/views/"/>
        <property name="suffix" value=".jsp"/>
    </bean>
</beans>
```


4、编写处理器类，并使用@Controller注解标识为处理器。

```java
@Controller
@RequestMapping("/helloworld")
public class HelloWorldController {

	@RequestMapping("/hello")
	public String hello(){
		System.out.println("hello world");
		return "sucess";
	}
}
```

5、编写jsp（省略……）


1、2、3、4、5步完成后，启动服务器测试即可。


注意：

1）如果web.xml中配置DispatcherServlet时不指定`contextConfigLocation`参数，则可使用默认的配置文件。默认的配置的路径和文件命名如下：/WEB-INF/<servlet-name>-servlet.xml。其中能改变是只是<servlet-name>。

2）视图解析器中prefix的值最后带“/”，如“/WEB-INF/views/”。


# 注解

## 入参

### @RequestMapping

@RequestMapping可以指定如下值：
- value：请示的URL。

    支持Ant风格：
    - ？：匹配文件名中的一个字符。
    - *：匹配文件名中的任意字符（无或多个字符）。
    - **：匹配多层路径。
    
    示例：
    
    - /user/*/createUser，匹配/user/aaa/createUser、/user/bbb/createUser等路径。
    - /user/**/createUser，匹配/user/createUser、/user/aaa/bbb/createUser等路径。
    - /user/createUser??，匹配/user/createUseraa、/user/createUserbb等路径。

- method：请求的方式（RequestMethod.GET, POST, HEAD, OPTIONS, PUT, PATCH, DELETE, TRACE.）
- params：请求的参数。支持简单的表达式：

    - param1：表示请求必须包含名为param1的请求参数。
    - !param1：表示请求不能包含名为param1的请求参数。
    - param1 != value1：表示请求包含名为param1，且其值不为value1的参数。
    - {“param1 = value1”,”param2”}：表示请求包含名为param1、param2的参数，且param1的值必须为value1。

- heads：请求头。表达式同params。如：headers={"Accept-Language=zh-CN,zh;q=0.8"}
- consumes：
- produces：。如：produces = "application/json"

### @PathVariable

@PathVariable修饰参数。

目的：为了支持REST风格。

作用：将URL中的占位符参数绑定到处理方法的入参中。

示例：

```java
@RequestMapping("delete/{id}")
public String delete(@PathVariable("id")Integer id){
	userDao.delete(id);
	return "redirect:/user/list.action";
}
```


### @RequestParam

@RequestParam修饰参数，用于把get方式或post方式（如文件上传时）请求的参数绑定到方法入参中。可以设置如下值：

- value：前端的参数名称
- required：是否是必传。
- defaultValue：默认值。

示例：

页面写法：`<a href="requestParamTest/testRequestParam?id=1">测试@RequestParam</a>`

后台写法：

```java
@RequestMapping(value="/testRequestParam")
public String testRequestParam(@RequestParam("id") Integer id){
	System.out.println("testRequestParam,id = "+id);
	return SUCESSED;
}
```

这样就把页面提交的id值绑定到名为id的入参中。当然，**名称相同，就不用写@RequestParam了**。



### @RequestHeader

@RequestHeader访问请求头信息，用法同@RequestParam。

示例：

页面：`<a href="requestParamTest/testRequestHeader>测试@RequestHeader</a>`

后台：

```java
@RequestMapping(value="/testRequestHeader")
public String testRequestHeader(@RequestHeader(value="Accept-Language") String lang){
	System.out.println("testRequestHeader,Accept-Language = "+lang);
	return SUCESSED;
}
```


### @CookieValue

@CookieValue将cookie值绑定到处理方法的入参字段。

其中value指定的是cookie的名称。

如何知道cookie的名称呢？可以打开chrome的开发者工具，查看cookie值的名称。如下的请求头中就能找到cookie的名称：

```java
Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Encoding:gzip, deflate, sdch, br
Accept-Language:zh-CN,zh;q=0.8
Cache-Control:max-age=0
Connection:keep-alive
Cookie:JSESSIONID=26EE22932FF862B5C80D1FF6F94BA188
Host:localhost:8080
Upgrade-Insecure-Requests:1
```

示例：

```java
@RequestMapping(value="/testCookieValue")
public String testCookieValue(@CookieValue(value="JSESSIONID",required=false) String cookieValue){
	System.out.println("testRequestHeader,JSESSIONID = "+cookieValue);
	return SUCESSED;
}
```


### POJO映射请求参数

将请求参数映射到普通类POJO对象中。SpringMVC会将请求参数名与POJO属性名自动匹配，自动为对应的属性赋值。支持级联属性。

页面：

```html
<form action="requestParamTest/testPOJOParam" method="post">
	用户名：<input type="text" name="username"><br>
	邮箱：<input type="text" name="email"><br>
	省：<input type="text" name="addr.province"><br>
	市：<input type="text" name="addr.city">
	<input type="submit" value="测试 testPOJOParam 请求">
</form>
```

后台：

```java
@RequestMapping(value = "/testPOJOParam")
public String testPOJOParam(PersonInfo personInfo) {
	System.out.println("testPOJOParam,personInfo = "+personInfo);
	return SUCESSED;
}
```

这种POJO不用注解修饰参数。

并且 PersonInfo 自动放入到了 requestScope 中，key 为首字母小写的的名称 personInfo。


### 访问Servlet API

处理方法的入参可以访问原生Servlet API，支持访问如下原生的API：

- HttpServletRequest
- HttpServletResponse
- HttpSession
- java.security.Principal
- Locale
- InputStream
- OutputStream
- Reader：
- Writer：向客户端写出信息

```java
@RequestMapping(value = "/testServletAPI")
public String testServletAPI(HttpServletRequest req,HttpServletResponse resp,Writer write,Reader read) {
	return SUCESSED;
}
```


## 域

### Map/Model

SpringMVC在调用方法前会创建一个隐含的模型对象作为数据的存储容器，如果方法入参中声明了Map或Model类型参数，SpringMVC会将隐含模型的引用传递给这些入参，我们就可以在方法体内访问模型中的所有数据，也可以向模型中添加新的属性数据。

下面的示例中，入参中声明一个Map参数，SpringMvc会把它当作model传到目标页面，也就是在目标页面可通过EL表达式取出其值。

A页面：`<a href="requestParamTest/testMap">测试testMap</a>`

后台：

```java
@RequestMapping(value = "/testMap")
public String testMap(Map<String,Object> map) {
	map.put("names",Arrays.asList("Tom","Tony","Wiky"));
	System.out.println(map);
	return SUCESSED;
}
```

B页面：`通过EL表达式取出值names: ${requestScope.names}`


注意：

1）如果web.xml中使用2.3及以下dtd，则需在使用el表达式的页面中增加<%@page isELIgnored="false" %>配置，el表达式才会生效。

2）如果web.xml中使用2.4及以上dtd，则默认isELIgnored="false"。


总结：这个Map，我们一般传Map就行，也可以传如下Map的子类或Model子类：

- Model
- ModelMap
- BindingAwareModelMap


### @SessionAttributes

如果希望在多个请求之间共用某个模型属性（比如方法入参Map中定义的属性名称）数据，则可以在**控制器类**上标注一个@SessionAttributes，SpringMVC会将模型中对应的属性暂存到HttpSession中。

@SessionAttributes可以指定两个属性值：

- value：指定Model中的key名称，将会把此Model放入session中
- types：指定Model中的key对应的value的类型，将会把此Model放入session中

```java
@Controller
@SessionAttributes(value={"person1","person2"})
@RequestMapping(value = "/requestParamTest")
public class RequestParamTest {
	public static String SUCESSED = "sucess";
	
	@RequestMapping(value = "/testSessionAttributes")
	public String testSessionAttributes(Map<String,Object> map) {
		//使用@SessionAttributes(value={"person1","person2"})
		//先将person1、person2的属性放在请求域里，再放到了session中
		//也就是说，请求域、session域中都有
		map.put("person1", new PersonInfo());
		map.put("person2", new PersonInfo());
		return SUCESSED;
	}
```

除了可以放Model中的key外，还可以指定key对应的value的类型。

```java
@Controller
@SessionAttributes(types={String.class})
@RequestMapping(value = "/requestParamTest")
public class RequestParamTest {
	public static String SUCESSED = "sucess";
	
	@RequestMapping(value = "/testSessionAttributes")
	public String testSessionAttributes(Map<String,Object> map) {
		//因为value1是String类型，所以将会把此Model
		//先放入requeest域中，再放入到session域中
		//也就是说，请求域、session域中都有
		map.put("key1", "value1");
		return SUCESSED;
	}
}
```

### @ModelAttribute

1）在更新或修改数据时使用，因为修改数据前在遵循 先查后改

作用：在当前 Controller 的**任何一次请求**之前，都先执行 @ModelAttribute 修饰的方法

![image](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708120216.png)

在 @ModelAttribute 修饰的方法中将属性放到 Model 中后，默认约定，其它 Controller 方法入参的首字母小写后跟 Model 中的key相同则会把值传入 Controller 方法

![image](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708120238.png)

如果名称不一致，则通过 @ModelAttribute 注入

![image](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708120251.png)

资料参考：https://ke.qq.com/course/308755?taid=2321584442619411

总结：此注解慎用。除非一个控制器只对应一个功能，即：所有请求方法之前都需要执行同一个跟入参相关的操作。

2）单独在入参中使用，可以将参数带到页面

```
public String saveUser(@Valid @ModelAttribute User user, BindingResult result){

}
```

可以将 user 带到页面，jsp中可以用 ${user}取值



# REST风格

REST，即 Representational State Transfer，（资源）表现层状态转化。

- 资源（Resources）：网络上的一个实体，或者说是网络上的一个具体信息。它可以是一段文本、一张图片、一首歌曲、一种服务，总之就是一个具体的存在。可以用一个URI（统一资源定位符）指向它，每种资源对应一个特定的 URI 。要获取这个资源，访问它的URI就可以，因此 URI 即为每一个资源的独一无二的识别符。
- 表现层（Representation）：把资源具体呈现出来的形式，叫做它的表现层（Representation）。比如，文本可以用 txt 格式表现，也可以用 HTML 格式、XML 格式、JSON 格式表现，甚至可以采用二进制格式。
- 状态转化（State Transfer）：每发出一个请求，就代表了客户端和服务器的一次交互过程。HTTP协议，是一个无状态协议，即所有的状态都保存在服务器端。因此，如果客户端想要操作服务器，必须通过某种手段，让服务器端发生“状态转化”（State Transfer）。而这种转化是建立在表现层之上的，所以就是 “表现层状态转化”。具体说，就是 HTTP 协议里面，四个表示操作方式的动词：GET、POST、PUT、DELETE。它们分别对应四种基本操作：
    - GET 用来获取资源
    - POST 用来新建资源
    - PUT 用来更新资源
    - DELETE 用来删除资源

示例：

- /order/1 	HTTP GET ：得到 id = 1 的 order
- /order/1 	HTTP DELETE：删除 id = 1的 order
- /order/1 	HTTP PUT：更新id = 1的 order
- /order 	HTTP POST：新增 order

平常我们只使用GET、POST请求（浏览器 form 表单只支持 GET 与 POST 请求，而DELETE、PUT等方法并不支持）

为了支持REST风格，Spring提供了 HiddenHttpMethodFilter **过滤器**，可以将这些请求转换为标准的http方法，使得支持 GET、POST、PUT、DELETE 请求。

![image](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708120311.png)


使用REST风格，步骤如下：

1、在web.xml文件中配置HiddenHttpMethodFilter过滤器：

```xml
    <!--启用REST风格请求-->
    <filter>
        <filter-name>hiddenHttpMethodFilter</filter-name>
        <filter-class>org.springframework.web.filter.HiddenHttpMethodFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>hiddenHttpMethodFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
```

注意：`<url-pattern>/*</url-pattern>` 是 `/*` 而不是 `/`

2、页面的form表单中增加隐藏字段

```html
<body>
    <form action="/test/rest/1" method="post">
        <input type="submit" value="POST-增">
    </form>

    <form action="/test/rest/1" method="post">
        <input type="hidden" name="_method" value="DELETE">
        <input type="submit" value="DELETE-删">
    </form>

    <form action="/test/rest/1" method="post">
        <input type="hidden" name="_method" value="PUT">
        <input type="submit" value="PUT-改">
    </form>

    <form action="/test/rest/1" method="get">
        <input type="submit" value="GET-增">
    </form>
</body>
```

其中 DELETE/PUT 请求时，页面要满足两个条件（GET/POST请求不用）：

- 有隐藏域，并且名称是 _method
- 表单提交方式为 post


3、在Controller方法里接收请求，配置对应的请求方法（GET、POST、PUT、DELETE），并使用@PathVariable修饰参数。

```java
@Controller
@RequestMapping("/test")
public class RestTestController {

    /**
     * 跳转到“增、删、改、查”页面
     */
    @GetMapping("/testRest")
    public String testRest(){
        return "restfull";
    }

    @PostMapping("/rest/{id}")
    public String testPost(@PathVariable("id")String id) {
        System.out.println("rest---POST--"+id);
        return "restfull";
    }

    @DeleteMapping("/rest/{id}")
    public String testDelete(@PathVariable("id")String id) {
        System.out.println("rest---Delete--"+id);
        return "restfull";
    }

    @PutMapping("/rest/{id}")
    public String testPUT(@PathVariable("id")String id) {
        System.out.println("rest---PUT--"+id);
        return "restfull";
    }

    @GetMapping("/rest/{id}")
    public String testGET(@PathVariable("id")String id) {
        System.out.println("rest---GET--"+id);
        return "restfull";
    }

}
```

注意：`@xxxMapping("/rest/{id}")` 中的取值用 `{}` 而不是 `${}`


**总结**：

为什么要用`_method`呢？因为HiddenHttpMethodFilter中会根据_method来包装方法。

疑问：但为什么要这样包装？

答：因为浏览器只支持 get/post 方式，为了支持 DELETE/PUT 请求，所以增加了过滤器。

---

什么是“幂等”？~~大白话：在操作成功的前提下，会不会对数据库造成不好的影响。不会，则幂等；会，则不幂等。~~

~~参考~~：https://www.bilibili.com/video/BV1vE411v7h7?p=5

什么是“幂等”？用通俗的话讲：就是针对一个操作，不管做多少次，产生效果或返回的结果都是一样的

参考：
- https://www.jianshu.com/p/cea3675a590b
- https://blog.csdn.net/garfielder007/article/details/55684420


# 视图解析

## 基本流程

Controller中的方法可以返回String、View、ModelAndView，Spring最终都终都会转为ModelAndView，然后通过ViewResolver进行解析。

![image](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708120339.png)

下面是常用视图解析器：


大类 | 视图类型 | 说明
---|---|---
解析为Bean的名字 | BeanNameViewResolver | 将逻辑视图名解析为一个Bean，Bean的id等于逻辑视图名
解析为URL文件 | InternalResourceViewResolver <br/>  JasperReportsViewResolver | 
模板文件视图 | FreeMarkerViewResolver <br/>  VelocityLayoutViewResolver <br/> VelocityViewResolver | 


## <mvc:view-controller.../>

如果想要直接访问页面而不需要经过Controller处理，则可在springmvc的配置文件中增加如下标签配置：

- <mvc:view-controller path="/sucess" view-name="sucess"/>

    - path表示访问的路径，如http://localhost:8080/myProject/sucess，就会直接跳转到sucess视图。
    - view-name表示视图名称，此视图也需要经SpringMVC解析后的视图，比如要加上前缀与后缀等。

- <mvc:annotation-driven/>

    此配置一般都要配置上。如果配置了<mvc:view-controller.../>，而不配置<mvc:annotation-driven/>，则程序会报错。具体原因，TOD....



## 自定义视图

1、实现View接口来定义自己的视图，并用`@Component`标识此类，让SpringMVC可以扫描到自定义的视图。

```java
@Component
public class HelloView implements View{
	@Override
	public String getContentType() {
		return "text/html";
	}
	@Override
	public void render(Map<String, ?> model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		response.getWriter().write("hello view ,time:"+new Date());
	}
}
```

上面定义了一个id为“helloView”的视图。

2、在springmvc配置文件中配置BeanNameViewResolver解析器，此解析器是根据IOC容器中的bean的id来获取视图解析器。

```xml
<bean class="org.springframework.web.servlet.view.BeanNameViewResolver" >
	<property name="order" value="100"/>
</bean>
```

注：视图解析器的先后顺序用order属性表示，值越小，优先级越高。

InternalResourceViewResolver的order = Integer.MAX_VALUE，所以它会最后执行。

3、在Controller方法中的返回刚刚自定义的视图名字。视图名，默认是类名第一个字母小写后的名字。

```java
@RequestMapping("/testView")
public String testView(){
	System.out.println("testView");
	return "helloView";
}
```

此处返回的就是前面自定义的“helloView”视图。

4、页面测试：`<a href="requestParamTest/testView">测试自定义视图testView</a>`


## 转发与重定向

转发：

重定向：一个web资源收到客户端请求后，通知客户端去访问另一个web资源。特点：地址栏发生变化。

Controller中，如果返回的字符串中带有`forward:`或`redirect:`前缀时，SpringMVC会对他们进行转发或重定向操作：
- forward:/sucess.jsp，会完成一个到sucess.jsp（项目根目录下的）的转发操作
- redirect:/sucess.jsp，会完成一个到sucess.jsp（项目根目录下的）的重定向操作

## EL表达式

1、加入Jar包

```xml
<dependency>
	<groupId>jstl</groupId>
	<artifactId>jstl</artifactId>
	<version>1.2</version>
</dependency>
<!--jstl 1.2 不需要 standard 包-->
<dependency>
	<groupId>taglibs</groupId>
	<artifactId>standard</artifactId>
	<version>1.1.2</version>
</dependency>
```

2、jsp页面引入 c标签

`<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> ` 

在jsp中拿到绝对路径：

```html
<form action="${pageContext.request.contextPath }/emp">
</form>
```

在开发中推荐使用绝对路径。


## 处理静态资源

由于web.xml文件中配置的SpringMVC的拦截器拦截所有的资源（“/”），包括静态资源等，因找不到静态资源对应的处理器，所以会报错。可以通过在springMVC的配置文件中增加如下标签配置：

<mvc:default-servlet-handler/>

此标签表示：将在SpringMVC上下文中定义一个DefaultServletHttpRequestHandler，它会对进入DispatchServlet的请求进行筛查，如果发现是没有经过映射的请求，就将请求交由WEB应用服务器默认的Servlet处理，如果不是静态资源的请求，才由DispatchServlet继续处理。

一般WEB应用服务器默认的Servlet的名称都是default。若所使用的WEB服务器默认的Servlet名称不是default，则需要通过default-servlet-name属性来显式指定默认的服务器名。

- 比如：apache-tomcat-7.0.42\conf\web.xml中就配置有一个名为default的servlet。
- 通过default-servlet-name属性来显式指定：<mvc:default-servlet-handler default-servlet-name="xxxx"/>

注意：配置<mvc:default-servlet-handler/>时，也要配置<mvc:annotation-driven/>。否则报错。



## 国际化（i18n）

参考自己的代码 https://github.com/wolf-lea/spring-mvc-demo/tree/master/spring-mvc-i18n

### 步骤一：写properties文件

在resources下新建 i18n 文件夹，创建以下3个文件：

base.properties

```properties
resource.welcome=WELCOME
resource.exit=EXIST
```

base_en_US.properties

```properties
resource.welcome=welcome
resource.exit=exist
```


i18n_zh_CN.properties

```properties
    resource.welcome=欢迎
    resource.exit=退出
```

国际化文件命名规则，参考：https://wangxingguang.iteye.com/blog/1861160


### 步骤二：配置 ResourceBundleMessageSource

在springmvc配置文件（比如springmvc-servlet.xml）中配置 ResourceBundleMessageSource，用于绑定properties文件

```xml
    <!--加载国际化文件-->
    <bean id="messageSource" class="org.springframework.context.support.ResourceBundleMessageSource">
        <property name="basename" value="i18n.base"/>
        <property name="defaultEncoding" value="UTF-8"/>
    </bean>
```

### 步骤三：写controller

```java
@Controller
@RequestMapping("/test")
public class I18NController {

    @RequestMapping("/i18n")
    public String testI18n(){
        return "i18n";
    }
}
```

### 步骤四：使用JSTL标签实现国际化

1）加入两个依赖：

```xml
<dependency>
  <groupId>jstl</groupId>
  <artifactId>jstl</artifactId>
  <version>1.2</version>
</dependency>

<!--jstl 1.2 不再需要standard -->
<!--<dependency>-->
    <!--<groupId>taglibs</groupId>-->
    <!--<artifactId>standard</artifactId>-->
    <!--<version>1.1.2</version>-->
<!--</dependency>-->
```

2）jsp文件引入jstl，并使用其标签实现国际化

```jsp
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

...
    <fmt:message key="resource.welcome"/>
    <fmt:message key="resource.exit"/>
...

```

测试：访问controller方法，而不是直接访问页面：http://localhost:8080/test/i18n

### 总结

1. jstl 1.2 不再需要 standard 包
2. 页面引入的是`http://java.sun.com/jsp/jstl/fmt`，而不是`<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>`
2. properties的编码要是 UTF-8
![image](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708115559.png)
3. ResourceBundleMessageSource 指定编码，否则中文乱码；id="messageSource"，id名称不能变。

```
 <property name="defaultEncoding" value="UTF-8"/> 
```