---
title: "Java-framework-SpringBoot-6-整合 Redis 哨兵 Sentinel"
date: 2019-08-20T11:17:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---


# SpringBoot 整合 Redis 哨兵 Sentinel

思路：
- Redis 使用 Sentinel 监控后，我们的应用与哨兵打交道，哨兵再访问主库，或从库
- 主库、从库、哨兵的配置文件配置的密码相同 

1）pom

```xml
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-parent</artifactId>
        <version>2.1.4.RELEASE</version>
    </parent>
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-redis</artifactId>
        </dependency>
    </dependencies>
```

2）application.yml

此处连接的是哨兵，而不是主库或从库

```yml
spring:
  redis:
    password: 123456 # 
    sentinel:
      master: mymaster # 哨兵配置文件中配置的名字
      nodes: 192.168.200.129:26379 # 哨兵节点，不是主、从库IP
```

3）启动类

```java
@SpringBootApplication
public class RedisApplication {
    public static void main(String[] args) {
        SpringApplication.run(RedisApplication.class, args);
    }
}
```

4）测试类

```java
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RedisApplication.class)
public class RedisTest {
​
    @Autowired
    private StringRedisTemplate redisTemplate;
​
    @Test
    public void test() {
        redisTemplate.opsForValue().set("test", "redis");
        redisTemplate.opsForValue().get("test");
    }
}
```


# SpringBoot 整合 Redis 集群 Cluster

思路：跟整合哨兵相似

> 此处的集群是redis内置的集群，意思是，还有其它的扩展集群

参考十次方2.0《第6章-Redis分布式缓存.html》

1）pom

跟sentinel相同

2）application.yml

```yml
spring:
  redis:
    cluster:
      nodes: 192.168.200.129:7001,192.168.200.129:7002,192.168.200.129:7003,192.168.200.129:7004,192.168.200.129:7005,192.168.200.129:7006
```

3）测试

```java
/**
 * SpringBoot和Cluster整合测试
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ClusterApplication.class)
public class RedisTest {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Test
    public void test() {
        redisTemplate.opsForValue().set("test", "redis");
        String test = redisTemplate.opsForValue().get("test");
        System.out.println(test);
    }
}
```




