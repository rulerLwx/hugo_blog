---
title: "Java-framework-SpringCloud-1"
date: 2019-08-25T11:17:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---

# 概述

## 单体框架

什么是单体框架？系统中所有的功能、模块耦合在一个应用中的框架。

特点：最终会打包成一个独立的单元（jar/war包），会以一个进程方式进行。

图示：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708104309.png)

优点：易于管理、部署简单

缺点：测试成本高、可伸缩性差、可选性差、系统迭代困难、跨语言程度差、团队协作难

## 微服务框架

什么是微服务？微服务是一种框架风格，一个大型复杂的软件应用，由一个或多个微服务组成，每个微服务可以独立部署，各个微服务之间是松耦合的，每个微服务只关注一件任务并很好地完成。

常见的框架风格：

1. 客户端与服务端
2. 基于组件模型的框架（EJB）
3. 分层框架（MVC）
4. 面向服务（SOA）

微服务特点：

微服务优点：易于测试、伸缩性强、可靠性强（一个服务出问题，不影响其它服务）、跨语言、团队协作容易、系统迭代容易

微服务缺点：运维成本高、部署数量较多、接口兼容多版本、系统的复杂性、分布式事务

## 常见框架：MVC/RPC/SOA/微服务框架

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708104332.png)

MVC：单体框架，代表技术：struts2/springmvc/spring/mybatis

RPC（Remote Procedure Call）：远程过程调用。通过网络从远程计算机程序上请求服务，而不需要了解底层网络技术。代表技术：Thrift/Hessian。注：RPC各个服务之间是没有通信的。

SOA（Service Oriented Architecture）：面向服务框架。ESB（Enterparise Service Bus）：企业服务总线，是一个服务中介，提供服务与服务之间的交互。ESB包含的功能：负载均衡、流量控制、加密处理、服务监控、异常处理等。技术代表：Mule/WSO2

微服务：轻量级的服务治理方案。用zookeper/eureka代替ESB。代表技术：springcloud/dubbo

## 如何设计微服务以及设计原则

1. AKF 拆分原则
2. 前后端分离原则
3. 无状态服务
4. RestFul 的通信风格

其中 AKF 拆分原则 是重点。

### AKF 拆分原则

业界对于可扩展的系统架构设计有一个朴素的理念,就是：

通过加机器就可以解决容量和可用性问题。(如果一台不行那就两台)。

我是个段子：（世界上没有什么事是一顿烧烤不能解决的。如果有，那就两顿。）

这一理念在“云计算”概念疯狂流行的今天，得到了广泛的认可！对于一个规模
迅速增长的系统而言，容量和性能问题当然是首当其冲的。但是随着时间的向前，
系统规模的增长，除了面对性能与容量的问题外，还需要面对功能与模块数量上
的增长带来的系统复杂性问题以及业务的变化带来的提供差异化服务问题。而许
多系统，在架构设计时并未充分考虑到这些问题，导致系统的重构成为常态，从
而影响业务交付能力，还浪费人力财力！对此，《可扩展的艺术》一书提出了一
个更加系统的可扩展模型—— AKF 可扩展立方（Scalability Cube）。这个立方
体中沿着三个坐标轴设置分别为：X、Y、Z。

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708104401.png)

#### Y 轴(功能)

Y 轴扩展会将庞大的整体应用拆分为多个服务。每个服务实现一组相关的功
能，如订单管理、客户管理等。在工程上常见的方案是服务化架构(SOA) 。比
如对于一个电子商务平台，我们可以拆分成不同的服务，组成下面这样的架构：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708104416.png)

但通过观察上图容易发现，当服务数量增多时，服务调用关系变得复杂。为
系统添加一个新功能，要调用的服务数也变得不可控，由此引发了服务管理上的
混乱。所以，一般情况下，需要采用服务注册的机制形成服务网关来进行服务治
理。系统的架构将变成下图所示：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708104507.png)

#### X 轴(水平扩展)

X 轴扩展与我们前面朴素理念是一致的，通过绝对平等地复制服务与数据，
以解决容量和可用性的问题。其实就是将微服务运行多个实例，做集群加负载均
衡的模式。

为了提升单个服务的可用性和容量， 对每一个服务进行X 轴扩展划分。

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708104533.png)

#### Z 轴(数据分区)

Z 轴扩展通常是指基于请求者或用户独特的需求，进行系统划分，并使得划
分出来的子系统是相互隔离但又是完整的。以生产汽车的工厂来举例：福特公司
为了发展在中国的业务，或者利用中国的廉价劳动力，在中国建立一个完整的子
工厂，与美国工厂一样，负责完整的汽车生产。这就是一种Z 轴扩展。

工程领域常见的Z 轴扩展有以下两种方案：

1）单元化架构

在分布式服务设计领域，一个单元（Cell）就是满足某个分区所有业务操作
的自包含闭环。如上面我们说到的Y 轴扩展的SOA 架构，客户端对服务端节点
的选择一般是随机的，但是，如果在此加上Z 轴扩展，那服务节点的选择将不再
是随机的了，而是每个单元自成一体。如下图：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708104547.png)

2）数据分区

为了性能数据安全上的考虑，我们将一个完整的数据集按一定的维度划分出
不同的子集。一个分区（Shard），就是是整体数据集的一个子集。比如用尾
号来划分用户，那同样尾号的那部分用户就可以认为是一个分区。数据分区为一
般包括以下几种数据划分的方式：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708104606.png)

### 前后端分离原则

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708104644.png)

何为前后端分离？前后端本来不就分离么？这要从尴尬的jsp 讲起。分工精细化从来都
是蛋糕做大的原则，多个领域工程师最好在不需要接触其他领域知识的情况下合作，才可能
使效率越来越高，维护也会变得简单。jsp 的模板技术融合了html 和java 代码，使得传统
MVC 开发中的前后端在这里如胶似漆，前端做好页面，后端转成模板，发现问题再找前端，
前端又看不懂java 代码......前后端分离的目的就是将这尴尬局面打破。

前后端分离原则，简单来讲就是前端和后端的代码分离，我们推荐的模式是最好采用物
理分离的方式部署，进一步促使更彻底的分离。如果继续直接使用服务端模板技术，如：jsp，
把java、js、html、css 都堆到一个页面里，稍微复杂一点的页面就无法维护了。

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708104708.png)

这种分离方式有几个好处：

1）前后端技术分离，可以由各自的专家来对各自的领域进行优化，这样前段的用户体
验优化效果更好。

2）分离模式下，前后端交互界面更清晰，就剩下了接口模型，后端的接口简洁明了，
更容易维护。

3）前端多渠道集成场景更容易实现，后端服务无需变更，采用统一的数据和模型，可
以支持多个前端：例如：微信h5 前端、PC 前端、安卓前端、IOS 前端。

### 无状态服务

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708104727.png)

对于无状态服务，首先说一下什么是状态：如果一个数据需要被多个服务共
享，才能完成一笔交易，那么这个数据被称为状态。进而依赖这个“状态”数据的
服务被称为有状态服务，反之称为无状态服务。

那么这个无状态服务原则并不是说在微服务架构里就不允许存在状态，表达
的真实意思是要把有状态的业务服务改变为无状态的计算类服务，那么状态数据
也就相应的迁移到对应的“有状态数据服务”中。

场景说明：例如我们以前在本地内存中建立的数据缓存、Session 缓存，到
现在的微服务架构中就应该把这些数据迁移到分布式缓存中存储，让业务服务变
成一个无状态的计算节点。迁移后，就可以做到按需动态伸缩，微服务应用在运
行时动态增删节点，就不再需要考虑缓存数据如何同步的问题。

### RestFul 的通讯风格

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708104753.png)

作为一个原则来讲本来应该是个“无状态通信原则”，在这里我们直接推荐一
个实践优选的Restful 通信风格，因为他有很多好处：

1) 无状态协议HTTP，具备先天优势，扩展能力很强。例如需要安全加密，有现成的成熟方案HTTPS 即可。

2) JSON 报文序列化，轻量简单，人与机器均可读，学习成本低，搜索引擎友好。

3) 语言无关，各大热门语言都提供成熟的Restful API 框架，相对其他的一些RPC 框架生态更完善。


# 入门

## 什么是SpringCloud

什么是SpringCloud：是一个服务治理平台，提供了一些服务框架。包含了：服务注册与发现、配置中心、消息中心、负载均衡、数据监控等等。

### 概念定义

Spring Cloud 是一个微服务框架，相比Dubbo 等RPC 框架, Spring Cloud 提供的全套的分布式系统解决方案。

Spring Cloud 对微服务基础框架Netflix 的多个开源组件进行了封装，同时又实现了和云端平台以及和Spring Boot 开发框架的集成。

Spring Cloud 为微服务架构开发涉及的配置管理，服务治理，熔断机制，智能路由，
微代理，控制总线，一次性token，全局一致性锁，leader 选举，分布式session，集
群状态管理等操作提供了一种简单的开发方式。

Spring Cloud 为开发者提供了快速构建分布式系统的工具，开发者可以快速的启动
服务或构建应用、同时能够快速和云平台资源进行对接。

### Spring Cloud 的子项目

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708104822.png)
![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708104836.png)

- Spring Cloud Config：配置管理工具，支持使用Git 存储配置内容，支持应
用配置的外部化存储，支持客户端配置信息刷新、加解密配置内容等
- Spring Cloud Bus：事件、消息总线，用于在集群（例如，配置变化事件）中
传播状态变化，可与Spring Cloud Config 联合实现热部署。
- Spring Cloud Netflix：针对多种Netflix 组件提供的开发工具包，其中包括
Eureka、Hystrix、Zuul、Archaius 等。
    - Netflix Eureka：一个基于rest 服务的服务治理组件，包括服务注册中
    心、服务注册与服务发现机制的实现，实现了云端负载均衡和中间层服务器
    的故障转移。
    - Netflix Hystrix：容错管理工具，实现断路器模式，通过控制服务的节点,
    从而对延迟和故障提供更强大的容错能力。
    - Netflix Ribbon：客户端负载均衡的服务调用组件。
    - Netflix Feign：基于Ribbon 和Hystrix 的声明式服务调用组件。
    - Netflix Zuul：微服务网关，提供动态路由，访问过滤等服务。
    - Netflix Archaius：配置管理API，包含一系列配置管理API，提供动
    态类型化属性、线程安全配置操作、轮询框架、回调机制等功能。
- Spring Cloud for Cloud Foundry ： 通过Oauth2 协议绑定服务到
CloudFoundry，CloudFoundry 是VMware 推出的开源PaaS 云平台。
- Spring Cloud Sleuth：日志收集工具包，封装了Dapper,Zipkin 和HTrace
操作。
- Spring Cloud Data Flow：大数据操作工具，通过命令行方式操作数据流。
- Spring Cloud Security：安全工具包，为你的应用程序添加安全控制，主要
是指OAuth2。
- Spring Cloud Consul：封装了Consul 操作，consul 是一个服务发现与配
置工具，与Docker 容器可以无缝集成。
- Spring Cloud Zookeeper ： 操作Zookeeper 的工具包， 用于使用
zookeeper 方式的服务注册和发现。
- Spring Cloud Stream：数据流操作开发包，封装了与Redis,Rabbit、
Kafka 等发送接收消息。
- Spring Cloud CLI：基于Spring Boot CLI，可以让你以命令行方式快速
建立云组件。


## SpringCloud 与Dubbo 的区别

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708104858.png)

dubbo官网：http://dubbo.apache.org


## Spring Cloud 版本说明

### 常见版本号说明

示例：

软件版本号：2.0.2.RELEASE

2：主版本号。当功能模块有较大更新或者整体架构发生变化时，主版本号会更新

0：次版本号。次版本表示只是局部的一些变动。

2：修改版本号。一般是bug 的修复或者是小的变动

RELEASE:希腊字母版本号。次版本号用户标注当前版本的软件处于哪个开发阶段

知识点：

Base：设计阶段。只有相应的设计没有具体的功能实现。

Alpha：软件的初级版本。存在较多的bug

Bate：表示相对alpha 有了很大的进步，消除了严重的bug，还存在一些潜在的bug。

Release：该版本表示最终版。

### Spring Cloud 版本号说明

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708104919.png)

1）为什么Spring Cloud 版本用的是单词而不是数字？

设计的目的是为了更好的管理每个Spring Cloud 的子项目的清单。避免子的版本号与子
项目的版本号混淆。

2）版本号单词的定义规则

采用伦敦的地铁站名称来作为版本号的命名，根据首字母排序，字母顺序靠后的版本号
越大。

3）版本发布计划说明

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708104941.png)

### Spring Cloud 与子项目版本兼容说明

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708104955.png)


## Spring Cloud 与 Spring Boot 的关系

- Spring Boot 做单个应用开发， Spring Cloud 是宏观方面，将各个应用关联起来
- Spring Boot 可以单独开发，Spring Cloud 离不开 Spring Boot


## 文档资料

- 中文文档：https://www.springcloud.cc/
- 中国社区：http://springcloud.cn/


# RabbitMQ

查看《Java-framework-RabbitMQ》

# Eureka注册中心

查看《Java-framework-SpringCloud-2-Eureka》