---
title: "Java-framework-SpringMVC-3-整合开发"
date: 2019-08-16T11:15:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---


# SSM

版本要求：http://www.mybatis.org/spring/zh/index.html

示例：

```xml
    示例1：
    <properties>
        <!--spring整合mybatis时版本要求：http://www.mybatis.org/spring/zh/index.html-->
        <spring.version>4.3.23.RELEASE</spring.version>
        <mybatis.version>3.5.0</mybatis.version>
        <mybatis-spring.version>1.3.2</mybatis-spring.version>
    </properties>
    
    示例2：
    <properties>
        <spring.version>4.0.8.RELEASE</spring.version>
        <mybatis.version>3.2.4</mybatis.version>
        <mybatis-spring.version>1.2.2</mybatis-spring.version>
    </properties> 

```

可以参考官方文档（ http://www.mybatis.org/spring/zh/index.html ），文档知识全但比较零散。下面是自己的整合总结

## Mapper接口方式

1、在web.xml文件中配置启动Spring、SpringMVC

```xml
  <context-param>
    <param-name>contextConfigLocation</param-name>
    <param-value>classpath*:/config/applicationContext.xml</param-value>
  </context-param>
  <listener>
    <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
  </listener>
  
  <servlet>
    <servlet-name>springMVC3</servlet-name>
    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
    <init-param>
      <param-name>contextConfigLocation</param-name>
      <param-value>classpath:config/springMVC-servlet.xml</param-value>
    </init-param>
    <load-on-startup>1</load-on-startup>
  </servlet>
  <servlet-mapping>
    <servlet-name>springMVC3</servlet-name>
    <url-pattern>/</url-pattern>
  </servlet-mapping>
 ```
 
2、SSM配置文件

Spring配置文件applicationContext.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:p="http://www.springframework.org/schema/p"
       xmlns:jdbc="http://www.springframework.org/schema/jdbc"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:jee="http://www.springframework.org/schema/jee" xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:util="http://www.springframework.org/schema/util" xmlns:task="http://www.springframework.org/schema/task"
       xsi:schemaLocation="
		http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
		http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.0.xsd
		http://www.springframework.org/schema/jdbc http://www.springframework.org/schema/jdbc/spring-jdbc-4.0.xsd
		http://www.springframework.org/schema/jee http://www.springframework.org/schema/jee/spring-jee-4.0.xsd
		http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-4.0.xsd
		http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util-4.0.xsd
		http://www.springframework.org/schema/task http://www.springframework.org/schema/task/spring-task-4.0.xsd
		http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-4.0.xsd"
       default-lazy-init="true">

    <context:property-placeholder location="classpath:config.properties"/>

    <context:component-scan base-package="com.demo">
        <context:exclude-filter type="annotation" expression="org.springframework.stereotype.Controller"/>
    </context:component-scan>

    <bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource"
          p:driverClass="${jdbc.mysql.driverClass}"
          p:jdbcUrl="${jdbc.mysql.url}"
          p:user="${jdbc.mysql.user}"
          p:password="${jdbc.mysql.password}"
          p:maxIdleTime="${jdbc.maxIdleTime}"
          p:maxPoolSize="${jdbc.minPoolSize}"
          p:minPoolSize="${jdbc.maxPoolSize}"
          p:initialPoolSize="${jdbc.initialPoolSize}">
    </bean>

    <bean id="sessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean"
          p:dataSource-ref="dataSource"
          p:configLocation="classpath:config/mybatis-setting.xml"
          p:mapperLocations="classpath*:mapper/**/*.xml">
    </bean>

    <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
        <property name="sqlSessionFactoryBeanName" value="sessionFactory"/>
        <property name="basePackage" value="com.demo.dao"/>
    </bean>
</beans>
```

springMVC-servlet.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context" xmlns:jdbc="http://www.springframework.org/schema/jdbc"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:jee="http://www.springframework.org/schema/jee" xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:util="http://www.springframework.org/schema/util" xmlns:task="http://www.springframework.org/schema/task"

       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
		http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.0.xsd
		http://www.springframework.org/schema/jdbc http://www.springframework.org/schema/jdbc/spring-jdbc-4.0.xsd
		http://www.springframework.org/schema/jee http://www.springframework.org/schema/jee/spring-jee-4.0.xsd
		http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-4.0.xsd
		http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util-4.0.xsd
		http://www.springframework.org/schema/task http://www.springframework.org/schema/task/spring-task-4.0.xsd
		http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-4.0.xsd"
       default-lazy-init="true">

    <tx:annotation-driven/>

    <context:component-scan base-package="com.demo.controller" use-default-filters="false">
        <context:include-filter type="annotation" expression="org.springframework.stereotype.Controller"/>
    </context:component-scan>

    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="prefix" value="/WEB-INF/views/"/>
        <property name="suffix" value=".jsp"/>
    </bean>
</beans>
```

mybatis-setting.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>

</configuration>
```

config.properties

```properties
#jdbc.mysql.driverClass=com.mysql.cj.jdbc.Driver
#jdbc.mysql.url=jdbc:mysql://localhost:3306/test?serverTimezone=UTC&useSSL=false
jdbc.mysql.driverClass=com.mysql.jdbc.Driver
jdbc.mysql.url=jdbc:mysql://localhost:3306/test
jdbc.mysql.user=root
jdbc.mysql.password=root
jdbc.maxIdleTime=300
jdbc.minPoolSize=10
jdbc.maxPoolSize=30
jdbc.initialPoolSize=10
```

调用：

```java
@Service("userService")
public class UserServiceImpl  implements UserService {

    @Autowired
    UserMapper userMapper;

    public Integer insertUser(User user) {
        return userMapper.insertUser(user);
    }
}
```


知识点：

为了让Spring容器和SpringMVC容器扫描相同的包时，不重复扫描相同的类，因此在Spring和SpringMVC各自配置文件的自动扫描包的配置中，使用`<context:include-filter.../>`和`<context:exclude-filter.../>`来规定只能扫描的注解。

1）比如SpringMVC自动扫描的配置如下：

```xml
	<context:component-scan base-package="com.demo" use-default-filters="false">
		<context:include-filter type="annotation" expression="org.springframework.stereotype.Controller"/>
		<context:include-filter type="annotation" expression="org.springframework.web.bind.annotation.ControllerAdvice"/>
	</context:component-scan>
```

use-default-filters="false"表示不扫描默认的注解了（如Component、Controller、Service、Repository），只扫描下面include的注解。

2）Spring自动扫描的配置如下：

```xml
	<context:component-scan base-package="com.demo">
		<context:exclude-filter type="annotation" expression="org.springframework.stereotype.Controller"/>
		<context:exclude-filter type="annotation" expression="org.springframework.web.bind.annotation.ControllerAdvice"/>
	</context:component-scan>
```

SpringMVC的IOC容器可以访问Sping的IOC容器中的bean，反之则不行。



## SqlSessionTemplate方式

与mapper接口方式的差别在spring的配置文件（常指applicationContext.xml）

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context" xmlns:p="http://www.springframework.org/schema/p"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">

    <context:property-placeholder location="classpath:config.properties"/>

    <context:component-scan base-package="com.demo">
        <context:exclude-filter type="annotation" expression="org.springframework.stereotype.Controller"/>
    </context:component-scan>

    <bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource"
          p:driverClass="${jdbc.mysql.driverClass}"
          p:jdbcUrl="${jdbc.mysql.url}"
          p:user="${jdbc.mysql.user}"
          p:password="${jdbc.mysql.password}"
          p:maxIdleTime="${jdbc.maxIdleTime}"
          p:maxPoolSize="${jdbc.minPoolSize}"
          p:minPoolSize="${jdbc.maxPoolSize}"
          p:initialPoolSize="${jdbc.initialPoolSize}">
    </bean>

    <bean id="sessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean"
          p:dataSource-ref="dataSource"
          p:configLocation="classpath:mybatis-config.xml"
          p:mapperLocations="classpath*:mapper/**/*.xml">
    </bean>

    <!--mapper接口方式的配置-->
    <!--<bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">-->
        <!--<property name="sqlSessionFactoryBeanName" value="sessionFactory"/>-->
        <!--<property name="basePackage" value="com.demo.dao"/>-->
    <!--</bean>-->

    <bean id="sessionTemplate" class="org.mybatis.spring.SqlSessionTemplate">
        <constructor-arg index="0" ref="sessionFactory"/>
    </bean>
</beans>
```

调用：

```java
@Service("userService")
public class UserServiceImpl  implements UserService {

    @Autowired
    SqlSessionTemplate sessionTemplate;

    public Integer insertUser(User user) {
        return sessionTemplate.insert("com.demo.dao.UserMapper.insertUser", user);
    }
}
```


# Spring MVC + Spring Data Redis

参考笔记《Java-framework-Spring-Data-Redis》，标准配置。

导入依赖

```xml
<properties>
    ...
        <!-- redis 版本 -->
        <redis.version>2.9.0</redis.version>
        <spring.data.redis.version>1.8.4.RELEASE</spring.data.redis.version>
    ...
</properties>
<dependencies>
    <dependency>
        ...
        <!--redis-->
        <dependency>
            <groupId>redis.clients</groupId>
            <artifactId>jedis</artifactId>
            <version>${redis.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.data</groupId>
            <artifactId>spring-data-redis</artifactId>
            <version>${spring.data.redis.version}</version>
        </dependency>            
        ...
    <dependency>
</dependencies>
```

redis.properties

```properties
redis.host=192.168.12.132
redis.port=6379
#redis.password=""
redis.maxIdle=400
redis.maxTotal=6000
redis.maxWaitMillis=1000
redis.blockWhenExhausted=true
redis.testOnBorrow=true
redis.timeout=100000
defaultCacheExpireTime=60
```

spring-redis.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">

    <!-- 加载配置文件 -->
    <context:property-placeholder location="classpath:redis.properties" />
    <!-- redis数据源 -->
    <bean id="poolConfig" class="redis.clients.jedis.JedisPoolConfig">
        <!-- 最大空闲数 -->
        <property name="maxIdle" value="${redis.maxIdle}" />
        <!-- 最大空连接数 -->
        <property name="maxTotal" value="${redis.maxTotal}" />
        <!-- 最大等待时间 -->
        <property name="maxWaitMillis" value="${redis.maxWaitMillis}" />
        <!-- 连接超时时是否阻塞，false时报异常,ture阻塞直到超时, 默认true -->
        <property name="blockWhenExhausted" value="${redis.blockWhenExhausted}" />
        <!-- 返回连接时，检测连接是否成功 -->
        <property name="testOnBorrow" value="${redis.testOnBorrow}" />
    </bean>

    <!-- Spring-redis连接池管理工厂 -->
    <bean id="jedisConnectionFactory" class="org.springframework.data.redis.connection.jedis.JedisConnectionFactory">
        <!-- IP地址 -->
        <property name="hostName" value="${redis.host}" />
        <!-- 端口号 -->
        <property name="port" value="${redis.port}" />
        <!-- 超时时间 默认2000-->
        <property name="timeout" value="${redis.timeout}" />
        <!-- 连接池配置引用 -->
        <property name="poolConfig" ref="poolConfig" />
        <!-- usePool：是否使用连接池 -->
        <property name="usePool" value="true"/>
    </bean>

    <!-- redis template definition -->
    <bean id="redisTemplate" class="org.springframework.data.redis.core.RedisTemplate">
        <property name="connectionFactory" ref="jedisConnectionFactory" />
        <property name="keySerializer">
            <bean class="org.springframework.data.redis.serializer.StringRedisSerializer" />
        </property>
        <property name="valueSerializer">
            <bean class="org.springframework.data.redis.serializer.JdkSerializationRedisSerializer" />
        </property>
        <!--开启事务  -->
        <property name="enableTransactionSupport" value="true"></property>
    </bean>
</beans>
```

在需要的地方注入模块，然后使用它操作数据

```java
@Controller
@RequestMapping("/redis")
public class TestRedisController {

    @Autowired
    public RedisTemplate redisTemplate;

    @ResponseBody
    @RequestMapping("/test")
    public Object testRedis(){

        return null;
    }
}
```

参考：https://www.cnblogs.com/phil_jing/p/7468586.html



# SpringMVC执行流程1

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708124223.png)

另外参考：SpringMVC工作原理-https://www.cnblogs.com/xiaoxi/p/6164383.html


# SpringMVC执行流程2

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708124241.png)

