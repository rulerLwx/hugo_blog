---
title: "Java-framework-SpringBoot-5"
date: 2019-08-20T11:15:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---

# 集成示例参考

https://github.com/xkcoding/spring-boot-demo


# Spring Boot + MyBatis

Mybatis和Spring Boot的整合有两种方式：

- 第一种：使用mybatis官方提供的Spring Boot整合包实现，地址：https://github.com/mybatis/spring-boot-starter

    分为零配置（全注解）方式、xml方式。
    
    可以参考官方示例：https://github.com/mybatis/spring-boot-starter/tree/master/mybatis-spring-boot-samples

- 第二种：使用mybatis-spring整合的方式，也就是我们传统的方式

    这种可以很方便的控制Mybatis的各种配置。——我现在不是这么想，20190625

## 第一种：使用mybatis官方提供的 mybatis-spring-boot-starter

### 零配置方式

添加依赖

```xml
    <dependency>
        <groupId>org.mybatis.spring.boot</groupId>
        <artifactId>mybatis-spring-boot-starter</artifactId>
        <version>2.0.0</version>
    </dependency>
```

书写Dao（Mapper接口）

```java
@Mapper //要求Mybatis版本3.3及以上
public interface IUserMapper {
    @Select("select * from user where username like '%${value}%'")
    public List<User> findUserByName(String name);
}
```

然后，就是controller/service/dao之间的调用了

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708101018.png)

这里最大的不同点是在mapper接口的方法上使用注解写sql

---
以下是mybatis零配置方式mapper接口方法的注解知识点

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708101035.png)


### xml（写mapper.xml）方式

1）在application.properties配置mybatis：

```properties
mybatis.config-location=classpath:mybatis-conf.xml
mybatis.mapper-locations=classpath:mappers/*/*.xml
mybatis.type-aliases-package=com.demo.pojo
```


2）使用@Mapper注解mapper接口

```java
@Mapper
public interface IUserMapper {
    public List<User> findUserByName(String name);
}
```

mapper.xml中的namespace与对应接口全类名一致、方法名一致。



## 第二种：：使用传统 mybatis-spring 整合的方式

首先创建一个Mybatis的配置类MyBatisConfig：

```java
import javax.sql.DataSource;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

@Configuration
public class MyBatisConfig {

    @Bean
    @ConditionalOnMissingBean //当容器里没有指定的Bean的情况下创建该对象
    public SqlSessionFactoryBean sqlSessionFactory(DataSource dataSource) {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        // 设置数据源
        sqlSessionFactoryBean.setDataSource(dataSource);
        // 设置mybatis的主配置文件
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource mybatisConfigXml = resolver.getResource("classpath:mybatis/mybatis-config.xml");
        sqlSessionFactoryBean.setConfigLocation(mybatisConfigXml);
        // 设置别名包
        sqlSessionFactoryBean.setTypeAliasesPackage("com.taotao.cart.pojo");
        return sqlSessionFactoryBean;
    }
}
```

然后，创建Mapper接口的扫描类MapperScannerConfig

```java
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AutoConfigureAfter(MyBatisConfig.class) //保证在MyBatisConfig实例化之后再实例化该类
public class MapperScannerConfig {
    
    // mapper接口的扫描器
    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer() {
        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
        mapperScannerConfigurer.setBasePackage("com.taotao.cart.mapper");
        return mapperScannerConfigurer;
    }
}
```

上面的两个配置相当于以前使用xml整合时spring配置文件application.xml中的两个<bean...>配置

![](https://oscimg.oschina.net/oscnet/3309a88d75841b64f1f5e1aa28d41a88771.jpg)

此时整合使用的mybatis依赖是：

```xml
	<!-- Mybatis -->
	<dependency>
		<groupId>org.mybatis</groupId>
		<artifactId>mybatis</artifactId>
		<version>3.2.8</version>
	</dependency>
	<dependency>
		<groupId>org.mybatis</groupId>
		<artifactId>mybatis-spring</artifactId>
		<version>1.2.2</version>
	</dependency>
```


# Spring Boot + JPA

1）导入依赖

```xml
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-data-jpa</artifactId>
    </dependency>
```

这个依赖自动导入了整合 JPA 所需要的所有包（抽象包、实现包），

包括 hibernate-core/hibernate-entitymanager 两个包，结合《Java-framework-Spring-Data-JPA》比较，更简洁了。

2）配置文件

Spring Boot + JPA 的配置文件只有一个：src/main/resources/application.properties

```properties
spring.datasource.driverClassName=com.mysql.jdbc.Driver
spring.datasource.url=jdbc:mysql://localhost:3306/test
spring.datasource.username=root
spring.datasource.password=root

spring.datasource.type=com.alibaba.druid.pool.DruidDataSource

spring.jpa.hibernate.ddl-auto=update
spring.jpa.show-sql=true
```

跟《Java-framework-Spring-Data-JPA》相比，不需要 spring-jpa.xml 配置文件，或 spring-jpa.xml 对应的“零配置”。

3）写接口、实体类、测试

跟《Java-framework-Spring-Data-JPA》完全相同

参考自己的代码：https://github.com/wolf-lea/spring-boot-v1520-demo/tree/master/22-spring-boot-jpa


# Spring Boot + Ehcache

1）依赖

```xml
    <!-- Spring Boot缓存支持启动器 -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-cache</artifactId>
    </dependency>

    <!-- Ehcache坐标 -->
    <dependency>
        <groupId>net.sf.ehcache</groupId>
        <artifactId>ehcache</artifactId>
    </dependency>
```

2）Ehcache 配置文件 src/main/resources/ehcache.xml

如何配置这个文件？在 ehcache 的 Jar 包里有最简的配置：ehcache-2.10.5.jar!/ehcache-failsafe.xml

下面是示例配置

```xml
<ehcache xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../config/ehcache.xsd">

    <diskStore path="java.io.tmpdir"/>

  <!--defaultCache:echcache的默认缓存策略  -->
    <defaultCache
            maxElementsInMemory="10000"
            eternal="false"
            timeToIdleSeconds="120"
            timeToLiveSeconds="120"
            maxElementsOnDisk="10000000"
            diskExpiryThreadIntervalSeconds="120"
            memoryStoreEvictionPolicy="LRU">
        <persistence strategy="localTempSwap"/>
    </defaultCache>
    <!-- 自定义缓存策略 -->
    <cache name="users"
            maxElementsInMemory="10000"
            eternal="false"
            timeToIdleSeconds="120"
            timeToLiveSeconds="120"
            maxElementsOnDisk="10000000"
            diskExpiryThreadIntervalSeconds="120"
            memoryStoreEvictionPolicy="LRU">
        <persistence strategy="localTempSwap"/>
    </cache>
</ehcache>
```

3）修改 application.properties

 指定 ehcache 的配置文件位置
```properties
spring.cache.ehcache.cofnig=ehcache.xml
```

4）在启动类上添加一个 @EnableCaching 注解

```java
@SpringBootApplication
@EnableCaching
public class App {

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
}
```

5）使用 Ehcache 注解

 ehcache没有学，TODO...

```

```




# Spring Boot + Redis

## 旧方式-JedisPoolConfig + JedisPool 方式

依赖：

```xml
        <dependency>
            <groupId>redis.clients</groupId>
            <artifactId>jedis</artifactId>
            <version>2.6.2</version>
        </dependency>
```

配置类：

```java
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedisPool;

@Configuration
@PropertySource(value = "classpath:redis.properties")
public class RedisSpringConfig {

    @Value("${redis.maxTotal}")
    private Integer redisMaxTotal;

    @Value("${redis.node1.host}")
    private String redisNode1Host;

    @Value("${redis.node1.port}")
    private Integer redisNode1Port;

    private JedisPoolConfig jedisPoolConfig() {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(redisMaxTotal);
        return jedisPoolConfig;
    }

    @Bean
    public ShardedJedisPool shardedJedisPool() {
        List<JedisShardInfo> jedisShardInfos = new ArrayList<JedisShardInfo>();
        jedisShardInfos.add(new JedisShardInfo(redisNode1Host, redisNode1Port));
        return new ShardedJedisPool(jedisPoolConfig(), jedisShardInfos);
    }
}
```

调用：

```java
    @Autowired
    ShardedJedisPool jedisPool;
    
    @GetMapping("/testRedis")
    public String testRedis(){
        ShardedJedis jedis = jedisPool.getResource();
        jedis.set("testRedis", "testRedis-1");
        String testRedis = jedis.get("testRedis");
        System.out.println("----------"+testRedis+"===========");
        return testRedis;
    }
```


## Spring Data Redis 方式

结合《Java-framework-Spring-Data-Redis》一起思考

1）依赖

```xml
    <dependencies>
        <!-- Spring Data Redis的启动器 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-redis</artifactId>
        </dependency>
    </dependencies>
```

自动导入了 jedis 依赖，不需要另外导入。


2）application.properties

```properties
spring.redis.pool.max-idle=10
spring.redis.pool.min-idle=5
spring.redis.pool.max-total=20

spring.redis.hostName=192.168.12.132
spring.redis.port=6379
```

3）redis 的 Java 配置（“零配置”）

```java
@Configuration
public class RedisConfig {

	/**
	 * 1.创建JedisPoolConfig对象。在该对象中完成一些链接池配置
	 * @ConfigurationProperties:会将前缀相同的内容创建一个实体。
	 */
	@Bean
	@ConfigurationProperties(prefix="spring.redis.pool")
	public JedisPoolConfig jedisPoolConfig(){
		JedisPoolConfig config = new JedisPoolConfig();
		/*//最大空闲数
		config.setMaxIdle(10);
		//最小空闲数
		config.setMinIdle(5);
		//最大链接数
		config.setMaxTotal(20);*/
		System.out.println("默认值："+config.getMaxIdle());
		System.out.println("默认值："+config.getMinIdle());
		System.out.println("默认值："+config.getMaxTotal());
		return config;
	}
	
	/**
	 * 2.创建JedisConnectionFactory：配置redis链接信息
	 */
	@Bean
	@ConfigurationProperties(prefix="spring.redis")
	public JedisConnectionFactory jedisConnectionFactory(JedisPoolConfig config){
		System.out.println("配置完毕："+config.getMaxIdle());
		System.out.println("配置完毕："+config.getMinIdle());
		System.out.println("配置完毕："+config.getMaxTotal());
		
		JedisConnectionFactory factory = new JedisConnectionFactory();
		//关联链接池的配置对象
		factory.setPoolConfig(config);
		//配置链接Redis的信息
		//主机地址
		/*factory.setHostName("192.168.70.128");
		//端口
		factory.setPort(6379);*/
		return factory;
	}
	
	/**
	 * 3.创建RedisTemplate:用于执行Redis操作的方法
	 */
	@Bean
	public RedisTemplate<String,Object> redisTemplate(JedisConnectionFactory factory){
		RedisTemplate<String, Object> template = new RedisTemplate<>();
		//关联
		template.setConnectionFactory(factory);
		
		//为key设置序列化器
		template.setKeySerializer(new StringRedisSerializer());
		//为value设置序列化器
		template.setValueSerializer(new StringRedisSerializer());
		
		return template;
	}
}
```

疑问：为什么 redis 需要写 spring-redis.xml（转换为零配置）？而 mybatis/jpa 却不用写 spring-mybatis/jpa.xml ？


Spring Data Redis提供了两个模板：

- RedisTemplate
- StringRedisTemplate





# Spring Boot + HttpClient

依赖：

```xml
		<!-- httpclient -->
		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpclient</artifactId>
		</dependency>
```

配置文件：

```java
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;

import com.taotao.common.httpclient.IdleConnectionEvictor;

@Configuration
@PropertySource(value = "classpath:httpclient.properties")
public class HttpclientSpringConfig {

    @Value("${http.maxTotal}")
    private Integer httpMaxTotal;

    @Value("${http.defaultMaxPerRoute}")
    private Integer httpDefaultMaxPerRoute;

    @Value("${http.connectTimeout}")
    private Integer httpConnectTimeout;

    @Value("${http.connectionRequestTimeout}")
    private Integer httpConnectionRequestTimeout;

    @Value("${http.socketTimeout}")
    private Integer httpSocketTimeout;

    @Value("${http.staleConnectionCheckEnabled}")
    private Boolean httpStaleConnectionCheckEnabled;

    @Autowired
    private PoolingHttpClientConnectionManager manager;

    @Bean
    public PoolingHttpClientConnectionManager poolingHttpClientConnectionManager() {
        PoolingHttpClientConnectionManager poolingHttpClientConnectionManager = new PoolingHttpClientConnectionManager();
        // 最大连接数
        poolingHttpClientConnectionManager.setMaxTotal(httpMaxTotal);
        // 每个主机的最大并发数
        poolingHttpClientConnectionManager.setDefaultMaxPerRoute(httpDefaultMaxPerRoute);
        return poolingHttpClientConnectionManager;
    }

    // 定期关闭无效连接
    @Bean
    public IdleConnectionEvictor idleConnectionEvictor() {
        return new IdleConnectionEvictor(manager);
    }

    // 定义Httpclient对
    @Bean
    @Scope("prototype")
    public CloseableHttpClient closeableHttpClient() {
        return HttpClients.custom().setConnectionManager(this.manager).build();
    }

    // 请求配置
    @Bean
    public RequestConfig requestConfig() {
        return RequestConfig.custom().setConnectTimeout(httpConnectTimeout) // 创建连接的最长时间
                .setConnectionRequestTimeout(httpConnectionRequestTimeout) // 从连接池中获取到连接的最长时间
                .setSocketTimeout(httpSocketTimeout) // 数据传输的最长时间
                .setStaleConnectionCheckEnabled(httpStaleConnectionCheckEnabled) // 提交请求前测试连接是否可用
                .build();
    }

}
```

# Spring Boot + RabbitMQ

# Spring Boot + Servlet/Filter/Listener

需要在启动类中添加一个 @ServletComponetScan 注解，其它的零配置跟《Java-Se-Servlet》一致。

```java
@SpringBootApplication
@ServletComponentScan
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class);
    }
}

```

# Spring Boot + JSP


```xml
	<dependency>
		<groupId>javax.servlet</groupId>
		<artifactId>jstl</artifactId>
	</dependency>
	<dependency>
		<groupId>org.apache.tomcat.embed</groupId>
		<artifactId>tomcat-embed-jasper</artifactId>
		<scope>provided</scope>
	</dependency>
```

参考官方示例：https://github.com/spring-projects/spring-boot/tree/v1.5.20.RELEASE/spring-boot-samples/spring-boot-sample-web-jsp


# Spring Boot + FreeMark/Thymeleaf

文件必须放在 src\main\resources\templates 下，文件夹名称必须是 templates

因为Spring Boot通过 `org.springframework.boot.autoconfigure.freemarker`包对 freemark进行了自动化设置

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708101155.png)

查看 `FreeMarkerProperties` 类就可以看到 `DEFAULT_TEMPLATE_LOADER_PATH = "classpath:/templates/"` 配置。thymeleaf同理。

templates目录是安全的，外界不能直接访问该目录下的文件；

Controller中返回的视图名称跟静态页面名称一致


# 定时任务

有两种方式实现定时任务

- Scheduled 定时任务器
- 整合Quartz 定时任务框架

## Scheduled

Scheduled 是 Spring 3.0以后自带的一个定时任务器。

1）依赖

```xml
    <dependencies>
        <!-- 添加Scheduled坐标 -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context-support</artifactId>
        </dependency>
    </dependencies>
```

2）编写定时任务类

```java
@Component
public class ScheduledDemo {

	/**
	 * 定时任务方法
	 * @Scheduled:设置定时任务
	 * cron属性：cron表达式。定时任务触发是时间的一个字符串表达形式
	 */
	@Scheduled(cron="0/2 * * * * ?")
	public void scheduledMethod(){
		System.out.println("定时器被触发"+new Date());
	}
}
```

3）在启动类中开启定时任务的使用

```java
@SpringBootApplication
@EnableScheduling
public class App {

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
}
```

cron 表达式讲解：

Cron 表达式是一个字符串，分为6 或7 个域，每一个域代表一个含义

Cron 有如下两种语法格式：
- （1） Seconds Minutes Hours Day Month Week Year
- （2）Seconds Minutes Hours Day Month Week

1）结构

corn 从左到右（用空格隔开）：秒 分 小时 月份中的日期 月份 星期中的日期 年份

2）各字段的含义

位置 | 时间域名 | 允许值 | 允许的特殊字符
---|---|---|---
1 |  秒 | 0-59  | , - * /
2 |  分钟 | 0-59 |  , - * /
3 | 小时 | 0-23 |  , - * /
4 |  日 | 1-31  | , - * / L W C
5 |  月 | 1-12 |  , - * /
6 |  星期 | 1-7 |  , - * ? / L C #
7 |  年(可选) |  1970-2099  | , - * /

Cron 表达式的时间字段除允许设置数值外，还可使用一些特殊的字符，提供列表、范围、通配符等功能，细说如下：

- 星号(*)：可用在所有字段中，表示对应时间域的每一个时刻，例如，*在分钟字段时，表示“每分钟”；
- 问号（?）：该字符只在日期和星期字段中使用，它通常指定为“无意义的值”，相当于占位符；
- 减号(-)：表达一个范围，如在小时字段中使用“10-12”，则表示从10 到12 点，即10,11,12；
- 逗号(,)：表达一个列表值，如在星期字段中使用“MON,WED,FRI”，则表示星期一，星期三和星期五；
- 斜杠(/)：x/y 表达一个等步长序列，x 为起始值，y 为增量步长值。如在分钟字段中使用0/15，则表示为0,15,30 和45 秒，而5/15 在分钟字段中表示5,20,35,50，你也可以使用*/y，它等同于0/y；
- L：该字符只在日期和星期字段中使用，代表“Last”的意思，但它在两个字段中意思不同。L 在日期字段中，表示这个月份的最后一天，如一月的31 号，非闰年二月的28 号；如果L 用在星期中，则表示星期六，等同于7。但是，如果L 出现在星期字段里，而且在前面有一个数值X，则表示“这个月的最后X 天”，例如，6L 表示该月的最后星期五；
- W：该字符只能出现在日期字段里，是对前导日期的修饰，表示离该日期最近的工作日。例如15W
表示离该月15 号最近的工作日，如果该月15 号是星期六，则匹配14 号星期五；如果15 日是星期日，
则匹配16 号星期一；如果15 号是星期二，那结果就是15 号星期二。但必须注意关联的匹配日期不能够
跨月，如你指定1W，如果1 号是星期六，结果匹配的是3 号星期一，而非上个月最后的那天。W 字符串
只能指定单一日期，而不能指定日期范围；
- LW 组合：在日期字段可以组合使用LW，它的意思是当月的最后一个工作日；
- 井号(#)：该字符只能在星期字段中使用，表示当月某个工作日。如6#3 表示当月的第三个星期五(6
表示星期五，#3 表示当前的第三个)，而4#5 表示当月的第五个星期三，假设当月没有第五个星期三，
忽略不触发；
- C：该字符只在日期和星期字段中使用，代表“Calendar”的意思。它的意思是计划所关联的日期，
如果日期没有被关联，则相当于日历中所有日期。例如5C 在日期字段中就相当于日历5 日以后的第一天。
1C 在星期字段中相当于星期日后的第一天。

Cron 表达式对特殊字符的大小写不敏感，对代表星期的缩写英文大小写也不敏感。

例子:

- @Scheduled(cron = "0 0 1 1 1 ?") //每年一月的一号的1:00:00 执行一次
- @Scheduled(cron = "0 0 1 1 1,6 ?") //一月和六月的一号的1:00:00 执行一次
- @Scheduled(cron = "0 0 1 1 1,4,7,10 ?") //每个季度的第一个月的一号的1:00:00 执行一次
- @Scheduled(cron = "0 0 1 1 * ?") //每月一号1:00:00 执行一次
- @Scheduled(cron="0 0 1 * * *") //每天凌晨1 点执行一次



## Quartz

### Quartz入门

开源项目：https://github.com/quartz-scheduler/quartz

1）Quartz 的使用思路

- job - 任务- 你要做什么事？
- Trigger - 触发器- 你什么时候去做？
- Scheduler - 任务调度- 你什么时候需要去做什么事？

对 JDK 版本要求：https://github.com/quartz-scheduler/quartz/wiki

使用2.2.x系列 —— 20190524

2）依赖

```xml
    <dependencies>
        <!-- Quartz坐标 -->
        <dependency>
            <groupId>org.quartz-scheduler</groupId>
            <artifactId>quartz</artifactId>
            <version>2.2.1</version>
        </dependency>
    </dependencies>
```

3）创建Job 类

```java
public class QuartzDemo implements Job {

	/**
	 * 任务被触发时所执行的方法
	 */
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("Execute...."+new Date());
	}
}
```

4）测试代码

```java
public class QuartzMain {

	public static void main(String[] args) throws Exception {

		// 1.创建Job对象：你要做什么事？
		JobDetail job = JobBuilder.newJob(QuartzDemo.class).build();

		/**
		 * 简单的trigger触发时间：通过Quartz提供一个方法来完成简单的重复调用 cron
		 * Trigger：按照Cron的表达式来给定触发的时间
		 */
		// 2.创建Trigger对象：在什么时间做？
		/*Trigger trigger = TriggerBuilder.newTrigger().withSchedule(SimpleScheduleBuilder.repeatSecondlyForever())
				.build();*/
		
		
		Trigger trigger = TriggerBuilder.newTrigger().withSchedule(CronScheduleBuilder.cronSchedule("0/2 * * * * ?"))
				.build();

		// 3.创建Scheduler对象：在什么时间做什么事？
		Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
		scheduler.scheduleJob(job, trigger);
		
		//启动
		scheduler.start();
	}
}
```


### Spring Boot + Quartz

1）依赖

 需要3个依赖
 
```xml
    <dependencies>
        <!-- Quartz坐标 -->
        <dependency>
            <groupId>org.quartz-scheduler</groupId>
            <artifactId>quartz</artifactId>
            <version>2.2.2</version>
            <exclusions>
                <exclusion>
                    <artifactId>slf4j-api</artifactId>
                    <groupId>org.slf4j</groupId>
                </exclusion>
            </exclusions>
        </dependency>

        <!-- 添加Scheduled坐标 -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context-support</artifactId>
        </dependency>

        <!-- Sprng tx 坐标 -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-tx</artifactId>
        </dependency>
    </dependencies>
```

2）创建 Job 类

```java
public class QuartzDemo implements Job {
	
	@Autowired
	private UsersService usersService;
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("Execute...."+new Date());
		this.usersService.addUsers();
	}
}
```

3）为了在 QuartzDemo 在可以注入 Spring IOC 容器中的对象 usersService，需要编写一个 MyAdaptableJobFactory

```java
@Component("myAdaptableJobFactory")
public class MyAdaptableJobFactory extends AdaptableJobFactory {

	//AutowireCapableBeanFactory 可以将一个对象添加到SpringIOC容器中，并且完成该对象注入
	@Autowired
	private AutowireCapableBeanFactory autowireCapableBeanFactory;
	
	/**
	 * 该方法需要将实例化的任务对象手动的添加到springIOC容器中并且完成对象的注入
	 */
	@Override
	protected Object createJobInstance(TriggerFiredBundle bundle) throws Exception {
		Object obj = super.createJobInstance(bundle);
		//将obj对象添加Spring IOC容器中，并完成注入
		this.autowireCapableBeanFactory.autowireBean(obj);
		return obj;
	}
}
```



4）编写 quartz 配置类

```java
@Configuration
public class QuartzConfig {

	
	/**
	 * 1.创建Job对象
	 */
	@Bean
	public JobDetailFactoryBean jobDetailFactoryBean(){
		JobDetailFactoryBean factory = new JobDetailFactoryBean();
		//关联我们自己的Job类
		factory.setJobClass(QuartzDemo.class);
		return factory;
	}
	
	/**
	 * 2.创建Trigger对象
	 * 简单的Trigger
	 */
	/*@Bean
	public SimpleTriggerFactoryBean simpleTriggerFactoryBean(JobDetailFactoryBean jobDetailFactoryBean){
		SimpleTriggerFactoryBean factory = new SimpleTriggerFactoryBean();
		//关联JobDetail对象
		factory.setJobDetail(jobDetailFactoryBean.getObject());
		//该参数表示一个执行的毫秒数
		factory.setRepeatInterval(2000);
		//重复次数
		factory.setRepeatCount(5);
		return factory;
	}*/
	
	/**
	 * Cron Trigger
	 */
	@Bean
	public CronTriggerFactoryBean cronTriggerFactoryBean(JobDetailFactoryBean jobDetailFactoryBean){
		CronTriggerFactoryBean factory = new CronTriggerFactoryBean();
		factory.setJobDetail(jobDetailFactoryBean.getObject());
		//设置触发时间
		factory.setCronExpression("0/2 * * * * ?");
		return factory;
	}
	
	/**
	 * 3.创建Scheduler对象
	 */
	@Bean
	public SchedulerFactoryBean schedulerFactoryBean(CronTriggerFactoryBean cronTriggerFactoryBean,MyAdaptableJobFactory myAdaptableJobFactory){
		SchedulerFactoryBean factory = new SchedulerFactoryBean();
		//关联trigger
		factory.setTriggers(cronTriggerFactoryBean.getObject());
		factory.setJobFactory(myAdaptableJobFactory);
		return factory;
	}
}
```

5）启动类

```java
@SpringBootApplication
@EnableScheduling
public class App {

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
}
```

代码：https://github.com/wolf-lea/spring-boot-v1520-demo/tree/master/27-spring-boot-quartz


# FAQ

## Caused by: java.lang.ClassNotFoundException: redis.clients.jedis.GeoUnit

Redis jar包冲突，新旧版同时在Maven中，解决办法：排除（移除）低版本的jar包。

参考 jar包冲突那些事，编译器不报错，启动报错： https://blog.csdn.net/li396864285/article/details/53290354