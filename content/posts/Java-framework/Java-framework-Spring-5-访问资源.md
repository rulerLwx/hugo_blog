---
title: "Java-framework-Spring-5-访问资源"
date: 2019-08-15T11:07:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---

# 访问资源

Spring为所有的资源访问提供了一个Resource接口。主要提供如下几个方法：

- getInputStream()：定位并打开资源，返回资源输入流。每次调用都返回新的输入流。调用者必须负责关闭输入流。
- exists()：返回Resource资源是否存在。
- isOpen()：返回资源是否打开。
- getDescription()：返回资源的描述信息，用于资源处理出错时输出该信息，通常是全限定文件名或实际URL。
- getFile()：返回资源对应的File对象。
- getURL()：返回资源对应的URL对象。

~~最后两个方法通常无须使用~~？？？？？，仅在通过简单方式访问无法实现时，Resource才提供传统的资源访问功能。


## Resource实现类

具体的资源访问由Resource接口的实现类完成。主要提供如下实现类：

- UrlResource：访问网络的实现类。
- ClassPathResource：访问类加载路径里资源的实现类。
- FileSystemResource：访问文件系统里资源的实现类。
- ServletContextResource：访问相对于ServletContext路径下的资源的实现类。
- InputStreamResource：访问输入流资源的实现类。
- ByteArrayResource：访问字节数组资源的实现类。


### UrlResource

URL资源通常应该提供标准的协议前缀。如：file:用于访问文件系统，http:用于通过HTTP协议访问资源，ftp:用于通过FTP协议访问资源等。

示例（将book.xml放在应用的当前路径下）：

```
<?xml version="1.0" encoding="GBK"?>
<计算机书籍列表>
	<书>
		<书名>疯狂Java讲义</书名>
		<作者>李刚</作者>
	</书>
	<书>
		<书名>轻量级Java EE企业应用实战</书名>
		<作者>李刚</作者>
	</书>
</计算机书籍列表>
```

```
public class UrlResourceTest{
	public static void main(String[] args) throws Exception{

		UrlResource ur = new UrlResource("file:book.xml");

		System.out.println(ur.getFilename());
		System.out.println(ur.getDescription());

		SAXReader reader = new SAXReader();
		Document doc = reader.read(ur.getFile());

		Element el = doc.getRootElement();
		List l = el.elements();

		for (Iterator it = l.iterator();it.hasNext() ; ){
			// 每个节点都是<书>节点
			Element book = (Element)it.next();
			List ll = book.elements();
			// 遍历<书>节点的全部子节点
			for (Iterator it2 = ll.iterator();it2.hasNext() ; ){
				Element eee = (Element)it2.next();
				System.out.println(eee.getText());
			}
		}
	}
}
```


file:路径问题，如何解决呢？？？？？？？
答：参看“ApplicationContext访问资源”一节的第4点，file:前缀的用法。


### ClassPathResource

方便访问类加载路径下的资源，尤其对于web应用，ClassPathResource可自动搜索位于WEB-INF/classes下的资源文件，无须使用绝对路径。

示例：

```
public class ClassPathResourceTest{
	public static void main(String[] args) throws Exception{

		ClassPathResource cr = new ClassPathResource("book.xml");

		System.out.println(cr.getFilename());
		System.out.println(cr.getDescription());
		
		//...

	}
}
```

在某个包下的文件也能找到，不用写路径。


### FileSystemResource

要求：资源位于本地文件系统内，资源字符串是完整路径（绝对路径）或位于项目根目录下（跟pom.xml同一目录）。


### ServletContextResource

将book.xml放在WEB-INF路径下，然后在jsp中使用ServletContextResource来访问该文件。

```
<%@ page contentType="text/html; charset=GBK" language="java" errorPage="" %>
<%@ page import="org.springframework.web.context.support.ServletContextResource"%>
<%@ page import="org.dom4j.*,org.dom4j.io.*,java.util.*"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>测试ServletContextResource</title>
</head>
<body>
<h3>测试ServletContextResource</h3>
<%
// 从Web Context下的WEB-INF路径下读取book.xml资源
ServletContextResource src = new ServletContextResource(application , "WEB-INF/book.xml");

//...

%>
</body>
</html>
```

### InputStreamResource

InputStreamResource是访问二进制输入流资源，是一个总被打开的Resource，因此，如果需要多次读取某个流，就不要使用InputStreamResource。

不建议使用InputStreamResource，而应尽量使用ByteArrayResource或FileSystemResource代替它。

ByteArrayResource用于直接访问字节数组，是将字节数组包装成了Resource。

```
public class ByteArrayResourceTest{
	public static void main(String[] args) throws Exception{
		String file = "<?xml version='1.0' encoding='GBK'?>"
			+ "<计算机书籍列表><书><书名>疯狂Java讲义"
			+ "</书名><作者>李刚</作者></书><书><书名>"
			+ "轻量级Java EE企业应用实战</书名><作者>李刚"
			+ "</作者></书></计算机书籍列表>";
			
		byte[] fileBytes = file.getBytes();
		ByteArrayResource bar = new ByteArrayResource(fileBytes);

		System.out.println(bar.getDescription());

		SAXReader reader = new SAXReader();
		Document doc = reader.read(bar.getInputStream());
		// 获取根元素
		Element el = doc.getRootElement();
		List l = el.elements();
		// 遍历根元素的全部子元素
		//...

	}
}
```

## ResourceLoader和ResourceLoaderAware接口

- ResourceLoader：该接口的实现类的实例可以获得一个Resource实例。
- ResourceLoaderAware：该接口的实现类的实例将获得一个ResourceLoader的引用。

ResourceLoader接口有如下方法：

- Resource getResource(String location)：该方法用于返回一个Resource实例。

ApplicationContext的实现类都实现了ResourceLoader接口，因此ApplicationContext可用于直接获取Resource实例。

某个ApplicationContext实例获取Resource实例时，默认采用与ApplicationContext相同的资源访问策略。如下代码：

```
Resource res = ctx.getResource(“some/resource/path/myTemplate.txt”);
```
如果ctx是FileSystemXmlApplicationContext，则res就是FileSystemResource；如果ctx是ClassPathXmlApplicationContext，则res就是ClassPathResource等。

```
public class ResourceLoaderTest{
	public static void main(String[] args) throws Exception{
//		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		ApplicationContext ctx = new FileSystemXmlApplicationContext("beans.xml");
		Resource res = ctx.getResource("book.xml");
		//....
}
```

也可以强制指定实现类：

```
Resource res = ctx.getResource(“classpath:myTemplate.txt”);//ClassPathResource
Resource res = ctx.getResource(“file:myTemplate.txt”);	//UrlResource
Resource res = ctx.getResource(“http:myTemplate.txt”);//UrlResource
```

实现ResourceLoaderAware接口，有如下方法：

- setResourceLoader(ResourceLoader resourceLoader)，resourceLoader就是ResourceLoader的引用，通过resourceLoader.getResource()就可以访问资源了。


## Resource作为属性

上面的使用Resource实现类或ApplicationContext来获取资源资源位置（路径）写在代码里了。这里使用Resource作为属性，Spring负责注入，实现解耦。

（这种方式是通过xml配置好资源位置（路径），并创建好实例，再注入到属性中）

```
<?xml version="1.0" encoding="GBK"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="http://www.springframework.org/schema/beans"
	xmlns:p="http://www.springframework.org/schema/p"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
	http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">
	
	<bean id="test" class="org.crazyit.app.service.TestBean"
		p:res="classpath:book.xml"/>
</beans>
```

```
public class TestBean{
	private Resource res;

	public void setRes(Resource res){
		this.res = res;
	}
	
	public void parse()throws Exception{

		System.out.println(res.getFilename());
		System.out.println(res.getDescription());
		// 创建基于SAX的dom4j的解析器
		SAXReader reader = new SAXReader();
		Document doc = reader.read(res.getFile());
		// 获取根元素
		//...
}
```

示例中要为resource提供setter方法。

但这各方式只适用于xml配置方式，“零配置”方式如何实现呢？？？？


## ApplicationContext访问资源

前面“ResourceLoader和ResourceLoaderAware接口”中已经提到，可以使用ApplicationContext来访问资源。在此需要注意ApplicationContext访问资源的策略：

1、使用ApplicationContext实现类指定访问策略：不同的实现类对应不同的策略。

- ClassPathXmlApplicationContext：对应使用ClassPathResource进行资源访问。
- FileSystemXmlApplicationContext：对应使用FileSystemResource进行资源访问。
- XmlWebApplicationContext：对应使用ServletContextResource进行资源访问。

2、使用前缀指定访问策略

如下代码，虽然使用了FileSystemXmlApplicationContext，但却是从类路径开始搜索文件：

`ApplicationContext ctx = new FileSystemXmlApplicationContext("classpath:beans.xml");`

但这种方式仅仅对当次访问有效，程序后面进行资源访问时，还是会根据	ApplicationContext的实现类来选择对应的资源访问策略。

3、classpath*:前缀的用法

1）`classpath*`:前缀会加载多个xml配置文件。

如`ApplicationContext ctx = new FileSystemXmlApplicationContext("classpath*:beans.xml");`会搜索classpath下所有的beans.xml（就算在不同的包中，只要名称是bean.xml的），不管有多少个，最终都会合并成一个ApplicationContext。


2）`“classpath:beans.xml”`则只加载第一个符合条件的XML文件。

3）classpath*:前缀仅对ApplicationContext有效。

4）另外还有一个加载多个文件的方法：

`ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:beans*.xml");`在类加载路径下搜索beans开关的xml文件。

如果使用FileSystemXmlApplicationContext的话，如下语句也是合法的：`ApplicationContext ctx = new FileSystemXmlApplicationContext("classpath*:beans*.xml");`

4、file:前缀的用法

用file:前缀来区分相对路径与绝对路径。如下：

```
ApplicationContext ctx = new FileSystemXmlApplicationContext("file:bean.xml");
ApplicationContext ctx = new FileSystemXmlApplicationContext("file:/bean.xml");
```

第一条语句是相对路径，以当前工作路径（项目根路径）为路径起点；

第二条语句是绝对路径，以文件系统根路径（如E:/bean.xml）为路径起点。