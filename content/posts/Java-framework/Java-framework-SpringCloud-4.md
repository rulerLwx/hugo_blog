---
title: "Java-framework-SpringCloud-4"
date: 2019-08-25T12:10:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---

# Feign负载均衡

Feign官方文档：http://projects.spring.io/spring-cloud/spring-cloud.html#spring-cloud-feign


## 为什么会有Feign

一句话：只需要创建一个接口，然后在上面添加注解即可

为什么要有Feign？

答：Feign旨在使编写Java Http客户端变得更容易。前面在使用Ribbon+RestTemplate，在Feign的实现下，我们只需创建一个接口并使用注解的方式来配置它(以前是Dao接口上面标注Mapper注解,现在是一个微服务接口上面标注一个Feign注解即可)


## 如何使用Feign

1）参考microservicecloud-consumer-dept-80，新建模块：microservicecloud-consumer-dept-feign

2）pom.xml

除了microservicecloud-consumer-dept-80中的依赖外，还需添加Feign依赖包

```
   <dependency>
       <groupId>org.springframework.cloud</groupId>
       <artifactId>spring-cloud-starter-feign</artifactId>
   </dependency>
```

3）修改microservicecloud-api模块

在microservicecloud-api的pom.xml添加feign依赖

```
   <dependency>
     <groupId>org.springframework.cloud</groupId>
     <artifactId>spring-cloud-starter-feign</artifactId>
   </dependency>
```

新增DeptClientService接口，并用@FeignClient修饰

```java
@FeignClient(value = "MICROSERVICECLOUD-DEPT")
public interface DeptClientService
{
  @RequestMapping(value = "/dept/get/{id}",method = RequestMethod.GET)
  public Dept get(@PathVariable("id") long id);
 
  @RequestMapping(value = "/dept/list",method = RequestMethod.GET)
  public List<Dept> list();
 
  @RequestMapping(value = "/dept/add",method = RequestMethod.POST)
  public boolean add(Dept dept);
}
```

4）修改microservicecloud-consumer-dept-feign模块的controller

```java
@RestController
public class DeptController_Feign
{
  @Autowired
  private DeptClientService service = null;
 
  @RequestMapping(value = "/consumer/dept/get/{id}")
  public Dept get(@PathVariable("id") Long id)
  {
   return this.service.get(id);
  }
 
  @RequestMapping(value = "/consumer/dept/list")
  public List<Dept> list()
  {
   return this.service.list();
  }
 
  @RequestMapping(value = "/consumer/dept/add")
  public Object add(Dept dept)
  {
   return this.service.add(dept);
  }
}
```

5）microservicecloud-consumer-dept-feign模块，主启动类

添加两个注解@EnableFeignClients、@ComponentScan

```java
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages= {"com.atguigu.springcloud"})
public class DeptConsumer80_Feign_App
{
	public static void main(String[] args)
	{
		SpringApplication.run(DeptConsumer80_Feign_App.class, args);
	}
}
```

注意：包名要写对，com.atguigu.springcloud，跟`@FeignClient`所有的包一致

6）总结

重点在第三步，接口+@FeignClient，通过实例名称找到微服务，并隐藏了RestTemplate

# Hystrix断路器

复杂分布式体系结构中的应用程序有数十个依赖关系，每个依赖关系在某些时候将不可避免地失败。

Hystrix能够保证在一个依赖出问题的情况下，不会导致整体服务失败，避免级联故障，以提高分布式系统的弹性。

“断路器”本身是一种开关装置，当某个服务单元发生故障之后，通过断路器的故障监控（类似熔断保险丝），**向调用方返回一个符合预期的、可处理的备选响应（FallBack）**，而不是长时间的等待或者抛出调用方无法处理的异常

官方文档：https://github.com/Netflix/Hystrix/wiki

## 服务容断

熔断机制的注解是@HystrixCommand

代码步骤：

1）参考microservicecloud-provider-dept-8001，新建microservicecloud-provider-dept-hystrix-8001

```xml
  <!-- hystrix -->
   <dependency>
     <groupId>org.springframework.cloud</groupId>
     <artifactId>spring-cloud-starter-hystrix</artifactId>
   </dependency>
```


2）controller的相应方法使用@HystrixCommand，并写回调函数

```java
@RestController
public class DeptController {

    @Autowired
    private DeptService service = null;

    @RequestMapping(value = "/dept/get/{id}", method = RequestMethod.GET)
    //一旦调用服务方法失败并抛出了错误信息后，会自动调用@HystrixCommand标注好的fallbackMethod调用类中的指定方法
    @HystrixCommand(fallbackMethod = "processHystrix_Get")
    public Dept get(@PathVariable("id") Long id)
    {

        Dept dept = this.service.get(id);

        if (null == dept) {
            throw new RuntimeException("该ID：" + id + "没有没有对应的信息");
        }

        return dept;
    }

    public Dept processHystrix_Get(@PathVariable("id") Long id)
    {
        return new Dept().setDeptno(id).setDname("该ID：" + id + "没有没有对应的信息,null--@HystrixCommand")
                .setDb_source("no this database in MySQL");
    }
}
```

3）主启动类上开启Hystrix：@EnableCircuitBreaker

```java
@SpringBootApplication
@EnableEurekaClient // 或使用@EnableDiscoveryClient  不同点：@EnableEurekaClient只适用于Eureka作为注册中心，@EnableDiscoveryClient 可以是其他注册中心
@EnableCircuitBreaker //对hystrixR熔断机制的支持
public class DeptProvider8001_App {
    public static void main(String[] args) {
        SpringApplication.run(DeptProvider8001_App.class, args);
    }
}
```

4）测试

- 启动三个eureka集群
- 启动服务提供者：microservicecloud-provider-dept-hystrix-8001
- 启动服务消费者：microservicecloud-consumer-dept-80

4.1：查看是否正常注册：http://eureka7001.com:7001/
![](https://oscimg.oschina.net/oscnet/up-bba677eca826efec15e11e5ec500ed31715.png)

4.2：查看hystrix：http://localhost/consumer/dept/get/13

没有ID为13的信息，null，返回以下信息
```
{"deptno":13,"dname":"该ID：13没有没有对应的信息,null--@HystrixCommand","db_source":"no this database in MySQL"}
```


## 服务降级

整体资源快不够了，忍痛将某些服务先关掉，待渡过难关，再开启回来。

服务降级处理是**在客户端实现的**，与服务端没有关系

步骤：

1）修改microservicecloud-api工程

根据已经有的DeptClientService接口新建一个实现了FallbackFactory接口的DeptClientServiceFallbackFactory

```java
@Component//不要忘记添加，不要忘记添加
public class DeptClientServiceFallbackFactory implements FallbackFactory<DeptClientService>
{
  @Override
  public DeptClientService create(Throwable throwable)
  {
   return new DeptClientService() {
     @Override
     public Dept get(long id)
     {
       return new Dept().setDeptno(id)
               .setDname("该ID："+id+"没有没有对应的信息,Consumer客户端提供的降级信息,此刻服务Provider已经关闭")
               .setDb_source("no this database in MySQL");
     }
 
     @Override
     public List<Dept> list()
     {
       return null;
     }
 
     @Override
     public boolean add(Dept dept)
     {
       return false;
     }
   };
  }
}
```

注意此类一定要用@Component修饰

2）修改microservicecloud-api工程

在DeptClientService接口的@FeignClient注解中添加fallbackFactory属性

```java
@FeignClient(value = "MICROSERVICECLOUD-DEPT",fallbackFactory = DeptClientServiceFallbackFactory.class)
public interface DeptClientService
{
  @RequestMapping(value = "/dept/get/{id}",method = RequestMethod.GET)
  public Dept get(@PathVariable("id") long id);
 
  @RequestMapping(value = "/dept/list",method = RequestMethod.GET)
  public List<Dept> list();
 
  @RequestMapping(value = "/dept/add",method = RequestMethod.POST)
  public boolean add(Dept dept);
}
```

3）将microservicecloud-api工程重新安装到本地maven目录

```
mvn clean install
```

4）修改microservicecloud-consumer-dept-feign工程的yml

添加以下配置：
```
feign: 
  hystrix: 
    enabled: true
```

5）测试

- 启动3个eureka
- 启动服务提供者：microservicecloud-provider-dept-8001
- 启动服务消息者：microservicecloud-consumer-dept-feign

正常访问：http://localhost/consumer/dept/get/1

故意停止microservicecloud-provider-dept-8001，再访问，则：

```
{"deptno":1,"dname":"该ID：1没有没有对应的信息,Consumer客户端提供的降级信息,此刻服务Provider已经关闭","db_source":"no this database in MySQL"}
```

**服务熔断、降级小结**：

- 服务熔断：需要在每个controller方法上使用@HystrixCommand注解并指定回调函数
- 服务降级：使用切面方式将回调函数写在一个接口中


## 服务监控——hystrixDashboard

除了隔离依赖服务的调用以外，Hystrix还提供了准实时的调用监控（Hystrix Dashboard），Hystrix会持续地记录所有通过Hystrix发起的请求的执行信息，并以统计报表和图形的形式展示给用户，包括每秒执行多少请求多少成功，多少失败等。

Netflix通过hystrix-metrics-event-stream项目实现了对以上指标的监控。Spring Cloud也提供了Hystrix Dashboard的整合，对监控内容转化成可视化界面。

示例-步骤：

1）新建microservicecloud-consumer-hystrix-dashboard项目

2）pom.xml导入依赖

```xml
   <!-- hystrix和 hystrix-dashboard相关-->
   <dependency>
       <groupId>org.springframework.cloud</groupId>
       <artifactId>spring-cloud-starter-hystrix</artifactId>
   </dependency>
   <dependency>
       <groupId>org.springframework.cloud</groupId>
       <artifactId>spring-cloud-starter-hystrix-dashboard</artifactId>
   </dependency> 
```

3）application.yml

```
server:
  port: 9001
```

4）主启动类使用新注解@EnableHystrixDashboard

```
@SpringBootApplication
@EnableHystrixDashboard
public class DeptConsumer_DashBoard_App
{
  public static void main(String[] args)
  {
   SpringApplication.run(DeptConsumer_DashBoard_App.class,args);
  }
}
```

5）所有的服务提供者都添加监控依赖actuator

所有的服务提供者（8001、8002、8003）都添加监控依赖actuator

```
   <!-- actuator监控信息完善 -->
   <dependency>
     <groupId>org.springframework.boot</groupId>
     <artifactId>spring-boot-starter-actuator</artifactId>
   </dependency>
```

6）测试

- 先启动 microservicecloud-consumer-hystrix-dashboard 监控消费端：http://localhost:9001/hystrix
- 启动三个eureka集群
- 启动 microservicecloud-provider-dept-hystrix-8001，访问：http://localhost:8001/hystrix.stream ，将看到很多JSON数据，这不是图形化
- 图形化：
![](https://oscimg.oschina.net/oscnet/up-1c8df240c05753f629efbd2fd737579f3ec.png)

如何看懂上面的图形？7色、1圈、1线
- 7色：
- 实心圆：共有两种含义。它通过颜色的变化代表了实例的健康程度，它的健康度从绿色<黄色<橙色<红色递减。
- 曲线：用来记录2分钟内流量的相对变化，可以通过它来观察到流量的上升和下降趋势。
![](https://oscimg.oschina.net/oscnet/up-2cc1e1f5b151dafa116f812f5fb7ed8b63f.png)

7）总结

1. 监控应用（http://localhost:9001/hystrix ）独立部署，内部代码不需要调用其它服务，只需在页面填写要监控的地址
2. 只能监控开启了熔断机制的应用（主启动类有@EnableCircuitBreaker）


