---
title: "Java-framework-SpringCloud-2-Eureka"
date: 2019-08-25T11:18:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---

# 什么是服务注册中心

服务注册中心是服务实现服务化管理的核心组件,类似于目录服务的作用,主要用来存储服务信息，譬如提供者url 串、路由信息等。

服务注册中心是SOA 架构中最基础的设施之一。

## 服务注册中心的作用

1）作用：

- 服务的注册
- 服务的发现

类比：QQ群，有人加入群就是注册服务，能不能加入群取决于管理员，每个群成员就是已注册的服务。

2）常见的注册中心

- Dubbo 的注册中心 Zookeeper
- SringCloud 的注册中心 Eureka、Consul

3）服务注册中心解决了以下问题：

1. 服务管理
2. 服务的依赖关系管理

<img src="https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708105544.png" style="zoom:80%;" />

4）什么是 Eureka 注册中心

Eureka 是 Netflix 开发的服务发现组件，本身是一个基于 REST 的服务。Spring Cloud 将它集成在其子项目 spring-cloud-netflix 中，以实现Spring Cloud 的服务注册于发现，同时还提供了负载均衡、故障转移等能力。

Eureka 注册中心三种角色

- Eureka Server：通过Register、Get、Renew 等接口提供服务的注册和发现。
- Application Service (Service Provider)：服务提供方，把自身的服务实例注册到 Eureka Server 中
- Application Client (Service Consumer)：服务调用方，通过 Eureka Server 获取服务列表，消费服务。

参考：

<img src="https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708105719.png" style="zoom: 67%;" />


总结：服务提供者需要注册到Eureka，消费者则不必须；可以使用`register-with-eureka: false/true`来控制

# Eureka 入门案例

1）创建项目

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708105739.png)

2）添加依赖

```xml
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>Dalston.SR5</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
        </dependency>
        <!--eureka-->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-config</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-eureka-server</artifactId>
        </dependency>
    </dependencies>
```

小巧门：这些依赖在官方文档很难找到，可以通过官网的Quick start，下载后查看pom文件即可：https://start.spring.io/

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708105758.png)

3）启动类

```java
@SpringBootApplication
@EnableEurekaServer
public class EurekaApp {
    public static void main(String[] args) {
        SpringApplication.run(EurekaApp.class, args);
    }
}
```

4）全局配置文件

```properties
spring.application.name=eureka-server
server.port=8761

#是否将自己注册到eureka中，默认true
eureka.client.register-with-eureka=false
#是否从eureka-server中获取服务注册信息，默认true
eureka.client.fetch-registry=false

eureka.instance.prefer-ip-address=true
```

注意：两个false的设置是重点，作用是“不将自己注册到eureka，而是自己作为服务，以供其它服务注册到自己”

另外，将`eureka.instance.prefer-ip-address=true`配置上，有大用 20200702

5）通过 http://localhost:8761/ 查看eureka-server管理平台

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708105819.png)

# Eureka 集群（高可用的eureka）

防止一个注册中心挂了，其它服务注册不进来，所以需要搭建一个高可用的eureka，即集群。

## 一、创建项目

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708105839.png)

## 二、全局配置文件

知识点：

```properties
#设置eureka实例名称，与配置文件的变量为主
eureka.instance.hostname=eureka2
#设置服务注册中心地址，指向另一个注册中心
eureka.client.service-url.defaultZone=http://eureka1:8761/eureka/
```

在搭建Eureka 集群时，需要添加多个配置文件，并且使用SpringBoot 的多环境配置方
式。集群中需要多少节点就添加多少个配置文件。

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708105919.png)

eureka1：

```properties
spring.application.name=eureka-server
server.port=8761

#设置eureka实例名称，与配置文件的变量为主
eureka.instance.hostname=eureka1
#设置服务注册中心地址，指向另一个注册中心
eureka.client.service-url.defaultZone=http://eureka2:8761/eureka/
```

eureka2：

```properties
spring.application.name=eureka-server
server.port=8761

#设置eureka实例名称，与配置文件的变量为主
eureka.instance.hostname=eureka2
#设置服务注册中心地址，指向另一个注册中心
eureka.client.service-url.defaultZone=http://eureka1:8761/eureka/
```

---

启动类：

```java
@SpringBootApplication
@EnableEurekaServer
public class EurekaApp {
    public static void main(String[] args) {
        SpringApplication.run(EurekaApp.class, args);
    }
}
```


## 三、添加日志文件logback.xml

## 四、集群配置

部署环境：两台linux，都需要安装jdk1.8，正确配置JAVA环境变量，IP分别是 192.168.12.132、192.168.12.133

注意：需要关闭防火墙，或者是开放8761 端口

1）将项目打包

`02-spring-cloud-eureka-server-ha-1.0-SNAPSHOT.jar`

2）将项目jar包上传到两台Linux

将`02-spring-cloud-eureka-server-ha-1.0-SNAPSHOT.jar`上传至 192.168.12.132 /root/temp目录下

将`02-spring-cloud-eureka-server-ha-1.0-SNAPSHOT.jar`上传至 192.168.12.133 /root/temp目录下

3）在linux上操作

192.168.12.132：

```
[root@localhost ~]# cd /usr/local
[root@localhost ~]# mkdir eureka
[root@localhost ~]# cp /root/temp/02-spring-cloud-eureka-server-ha-1.0-SNAPSHOT.jar /usr/local/eureka -f

[root@localhost ~]# cd /usr/local/eureka
[root@localhost ~]# vim server.sh
```

server.sh如下：

```sh
#!/bin/bash
 
cd `dirname $0`
 
CUR_SHELL_DIR=`pwd`
CUR_SHELL_NAME=`basename ${BASH_SOURCE}`
 
JAR_NAME="项目名称"
JAR_PATH=$CUR_SHELL_DIR/$JAR_NAME
 
#JAVA_MEM_OPTS=" -server -Xms1024m -Xmx1024m -XX:PermSize=128m"
JAVA_MEM_OPTS=""
 
SPRING_PROFILES_ACTIV="-Dspring.profiles.active=配置文件变量名称"
#SPRING_PROFILES_ACTIV=""
LOG_DIR=$CUR_SHELL_DIR/logs
LOG_PATH=$LOG_DIR/${JAR_NAME%..log
 
echo_help()
{
    echo -e "syntax: sh $CUR_SHELL_NAME start|stop"
}
 
if [ -z $1 ];then
    echo_help
    exit 1
fi
 
if [ ! -d "$LOG_DIR" ];then
    mkdir "$LOG_DIR"
fi
 
if [ ! -f "$LOG_PATH" ];then
    touch "$LOG_DIR"
fi
 
if [ "$1" == "start" ];then
 
    # check server
    PIDS=`ps --no-heading -C java -f --width 1000 | grep $JAR_NAME | awk '{print $2}'`
    if [ -n "$PIDS" ]; then
        echo -e "ERROR: The $JAR_NAME already started and the PID is ${PIDS}."
        exit 1
    fi
 
    echo "Starting the $JAR_NAME..."
 
    # start
    nohup java $JAVA_MEM_OPTS -jar $SPRING_PROFILES_ACTIV $JAR_PATH >> $LOG_PATH 2>&1 &
 
    COUNT=0
    while [ $COUNT -lt 1 ]; do
        sleep 1
        COUNT=`ps  --no-heading -C java -f --width 1000 | grep "$JAR_NAME" | awk '{print $2}' | wc -l`
        if [ $COUNT -gt 0 ]; then
            break
        fi
    done
    PIDS=`ps  --no-heading -C java -f --width 1000 | grep "$JAR_NAME" | awk '{print $2}'`
    echo "${JAR_NAME} Started and the PID is ${PIDS}."
    echo "You can check the log file in ${LOG_PATH} for details."
 
elif [ "$1" == "stop" ];then
 
    PIDS=`ps --no-heading -C java -f --width 1000 | grep $JAR_NAME | awk '{print $2}'`
    if [ -z "$PIDS" ]; then
        echo "ERROR:The $JAR_NAME does not started!"
        exit 1
    fi
 
    echo -e "Stopping the $JAR_NAME..."
 
    for PID in $PIDS; do
        kill $PID > /dev/null 2>&1
    done
 
    COUNT=0
    while [ $COUNT -lt 1 ]; do
        sleep 1
        COUNT=1
        for PID in $PIDS ; do
            PID_EXIST=`ps --no-heading -p $PID`
            if [ -n "$PID_EXIST" ]; then
                COUNT=0
                break
            fi
        done
    done
 
    echo -e "${JAR_NAME} Stopped and the PID is ${PIDS}."
else
    echo_help
    exit 1
fi
```

将文件中的“项目名称”、“配置文件变量名称”分为改为“02-spring-cloud-eureka-server-ha-1.0-SNAPSHOT.jar”、“eureka1”

更改权限，使文件可执行

```sh
[root@localhost ~]# chmod -R 755 server.sh
```

修改host文件

```shell
[root@localhost ~]# vim /etc/hosts
```

添加以下配置

```tex
192.168.12.132 eureka1
192.168.12.133 eureka2
```

---

192.168.12.133：

也作相同的操作，不同处是server.sh文件中“项目名称”、“配置文件变量名称”配置如下

“02-spring-cloud-eureka-server-ha-1.0-SNAPSHOT.jar”、“eureka2”

4）启动eureka注册中心

```
[root@localhost ~]# cd /usr/local/eureka
[root@localhost ~]# ./server.sh start

[root@localhost ~]# ./server.sh stop
```

在linux上启动不成功 ——20190621

转为在windows上，用idea启动两个应用

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708110007.png)

windows下同样需要配置hsot文件

```
127.0.0.1 eureka1
127.0.0.1 eureka2
```

因在同一电脑上，需要修改 application-eureka1.properties、application-eureka2.properties的端口号

application-eureka1.properties
```properties
spring.application.name=eureka-server
server.port=8761

#设置eureka实例名称，与配置文件的变量为主
eureka.instance.hostname=eureka1
#设置服务注册中心地址，指向另一个注册中心
eureka.client.service-url.defaultZone=http://eureka2:8762/eureka/
```

application-eureka2.properties
```properties
spring.application.name=eureka-server
server.port=8762

#设置eureka实例名称，与配置文件的变量为主
eureka.instance.hostname=eureka2
#设置服务注册中心地址，指向另一个注册中心
eureka.client.service-url.defaultZone=http://eureka1:8761/eureka/
```

参考：https://blog.csdn.net/u011531425/article/details/81713441

5）页面上访问 http://localhost:8761/ ——windwos上集群

项目代码：https://github.com/wolf-lea/spring-cloud/tree/master/02-spring-cloud-eureka-server-ha


# 在高可用的Eureka 注册中心中构建provider 服务

以客户端方式构建服务

1）创建项目

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708110034.png)

2）导入依赖

```xml
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>Dalston.SR5</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
        </dependency>
        <!--eureka-->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-config</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-eureka</artifactId>
        </dependency>
    </dependencies>
```

注意：依赖是`spring-cloud-starter-eureka`

3）引导类

```java
@SpringBootApplication
@EnableEurekaClient
public class EurekaApp {
    public static void main(String[] args) {
        SpringApplication.run(EurekaApp.class, args);
    }
}
```

4）全局配置文件

```properties
spring.application.name=eureka-provider
server.port=9090

#设置服务注册中心地址，指向另一个注册中心
eureka.client.service-url.defaultZone=http://eureka1:8761/eureka/,http://eureka2:8762/eureka/
```

5）修改windows下host文件

```

```

6）编写一个controller、pojo

```java
@RestController
@RequestMapping("/user")
public class UserController {

    @RequestMapping("/listUsers")
    public List<User> listUsers(){
        List<User> list = new ArrayList();
        list.add(new User(1, "zhangsan", 20));
        list.add(new User(2, "lisi", 22));
        list.add(new User(3, "wangwu", 20));
        return list;
    }
}
```

```java
public class User {
    private int userid;
    private String username;
    private int userage;

    public User() {
    }

    public User(int userid, String username, int userage) {
        this.userid = userid;
        this.username = username;
        this.userage = userage;
    }
    
    //get/setter...
```

7）启动引导类

访问 http://localhost:8761/ ，在服务注册中心就多了刚刚的服务

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708110105.png)


# 在高可用的Eureka 注册中心中构建consumer 服务

1）创建项目

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708110129.png)

2）全局配置文件

注意：服务的provider和consumer都需要在eureka注册。

配置文件中的`defaultZone`跟provider一样

```properties
spring.application.name=eureka-consumer
server.port=9091

#设置服务注册中心地址，指向另一个注册中心
eureka.client.service-url.defaultZone=http://eureka1:8761/eureka/,http://eureka2:8762/eureka/
```

3）在Service 中完成服务的调用

```java
@Service
public class UserService {
    @Autowired
    private LoadBalancerClient loadBalancerClient;//ribbon负载均衡器

    public List<User> listUsers() {
        //选择调用的服务名称
        //ServiceInstance封装了服务的IP、端口
        ServiceInstance si = this.loadBalancerClient.choose("eureka-provider");

        //拼接服务的路径，http://localhost:9090/user/listUsers
        StringBuffer sb = new StringBuffer();
        sb.append("http://").append(si.getHost()).append(":").append(si.getPort())
                .append("/user/listUsers");

        RestTemplate rt = new RestTemplate();
        ParameterizedTypeReference<List<User>> type = new ParameterizedTypeReference<List<User>>() {
        };
        ResponseEntity<List<User>> response =
                rt.exchange(sb.toString(), HttpMethod.GET, null, type);
        List<User> userList = response.getBody();
        return userList;
    }
}
```

4）编写controller

```java
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/listUsers")
    public List<User> listUsers(){
        List<User> list = userService.listUsers();
        return list;
    }
}
```

5）测试

启动eureka1、eureka2、eureka-provider、eureka-consumer，访问 http://localhost:9091/user/listUsers

返回以下数据

```
[{"userid":1,"username":"zhangsan","userage":20},{"userid":2,"username":"lisi","userage":22},{"userid":3,"username":"wangwu","userage":20}]
```


# Eureka 注册中心架构原理

Eureka 架构图

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708111852.png)

参考：https://github.com/Netflix/eureka/wiki/Eureka-at-a-glance

- Register(服务注册)：把自己的IP 和端口注册给Eureka。
- Renew(服务续约)：发送心跳包，每30 秒发送一次。告诉Eureka 自己还活着。
- Cancel(服务下线)：当provider 关闭时会向Eureka 发送消息，把自己从服务列表中删除。防止consumer 调用到不存在的服务。
- Get Registry(获取服务注册列表)：获取其他服务列表。
- Replicate(集群中数据同步)：eureka 集群中的数据复制与同步。
- Make Remote Call(远程调用)：完成服务的远程调用。


# Eureka与Zookeeper 的区别

两个框架都是基于分布式CAP 定理

## 什么是CAP 原则

CAP 原则又称CAP 定理，指的是在一个分布式系统中，Consistency（一致性）、
Availability（可用性）、Partition tolerance（分区容错性），三者不可兼得。

CAP 由Eric Brewer 在2000 年PODC 会议上提出。该猜想在提出两年后被证明成
立，成为我们熟知的CAP 定理

分布式系统CAP 定理

名称 | 解释
---|---
C <br> 数据一致性 <br> (Consistency) | 也叫做数据原子性 <br>系统在执行某项操作后仍然处于一致的状态。<br>在分布式系统中，更新操作执行成功后所有的用户都应该读到最新的值，<br>这样的系统被认为是具有强一致性的。等同于所有节点访问同一份最新的数据副本。
A <br> 服务可用性 <br> (Availablity) | 每一个操作总是能够在一定的时间内返回结果，这里需要注意的是"一定时间内"和"返回结果"。<br>一定时间内指的是，在可以容忍的范围内返回结果，结果可以是成功或者是失败。
P <br> 分区容错性 <br> (Partition-torlerance) | 在网络分区的情况下，被分隔的节点仍能正常对外提供服务 <br> (分布式集群，数据被分布存储在不同的服务器上，无论什么情况，服务器都能正常被访问)

定律：任何分布式系统只可同时满足二点，没法三者兼顾。

选择、放弃 | 说明
---|---
CA，放弃P | 如果想避免分区容错性问题的发生，一种做法是将所有的数据(与事务相关的)都放在一台机器上。<br> 虽然无法100%保证系统不会出错，单不会碰到由分区带来的负面效果。当然这个选择会严重的影响系统的扩展性。
CP，放弃A | 相对于放弃"分区容错性"来说，其反面就是放弃可用性。<br> 一旦遇到分区容错故障，那么受到影响的服务需要等待一定时间，因此在等待时间内系统无法对外提供服务。
AP，放弃C | 这里所说的放弃一致性，并不是完全放弃数据一致性，而是放弃数据的强一致性，而保留数据的最终一致性。<br> 以网络购物为例，对只剩下一件库存的商品，如果同时接受了两个订单，那么较晚的订单将被告知商品告罄。

## Zookeeper 与 Eureka 的区别

对比项 | Zookeeper | Eureka | 备注
---|---|---|---
CAP | CP | AP | 
Dubbo 集成 | 已支持 | - | 
Spring Cloud 集成 | 已支持 | 已支持 | 
kv 服务 | 已支持 | - | ZK 支持数据存储，eureka不支持
使用接口(多语言能力) | 提供客户端 | http 多语言 | ZK 的跨语言支持比较弱
watch 支持 | 支持 | 支持 | 什么是Watch 支持？就是客户单监听服务端的变化情况。<br> zk 通过订阅监听来实现，eureka通过轮询的方式来实现
集群监控 | - | metrics | metrics，运维者可以收集并报警这些度量信息达到监控目的


# Eureka自我保护

## 什么是自我保护？

一般情况下，微服务在Eureka 上注册后，会每30 秒发送心跳包，Eureka 通过心跳来
判断服务时候健康，同时会定期删除超过90 秒没有发送心跳服务。

有两种情况会导致Eureka Server 收不到微服务的心跳

- 微服务自身的原因
- 微服务与Eureka 之间的网络故障

通常(微服务的自身的故障关闭)只会导致个别服务出现故障，一般不会出现大面积故障，而(网络故障)通常会导致Eureka Server 在短时间内无法收到大批心跳。

考虑到这个区别，Eureka 设置了一个阀值，当判断挂掉的服务的数量超过阀值时，Eureka Server 认为很大程度上出现了网络故障，将不再删除心跳过期的服务。

那么这个阀值是多少呢？

15 分钟之内是否低于85%；

Eureka Server 在运行期间，会统计心跳失败的比例在15 分钟内是否低于85%，这种算法叫做Eureka Server的自我保护模式。


## 为什么要启动自我保护

1. 因为同时保留"好数据"与"坏数据"总比丢掉任何数据要更好，当网络故障恢复后，
这个Eureka 节点会退出"自我保护模式"。
2. Eureka 还有客户端缓存功能(也就是微服务的缓存功能)。即便Eureka 集群中所有节点
都宕机失效，微服务的Provider 和Consumer
都能正常通信。
3. 微服务的负载均衡策略会自动剔除死亡的微服务节点。

## 如何关闭自我保护

修改 Eureka Server 配置文件（在服务端做，也就是注册中心的两个集群项目全局配置文件，而不是provider/consumer项目的配置文件）

```properties
#关闭自我保护:true 为开启自我保护，false 为关闭自我保护
eureka.server.enableSelfPreservation=false
#清理间隔(单位:毫秒，默认是60*1000)
eureka.server.eviction.interval-timer-in-ms=60000
```

# 如何优雅停服

1）在客户端做，而不需要在 Eureka Server 中配置关闭自我保护

2）需要在服务中添加actuator.jar 包

```xml
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-eureka-server</artifactId>
        </dependency>
```

图示如下：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708111924.png)

3）修改配置文件

```properties
#启用shutdown
endpoints.shutdown.enabled=true
#禁用密码验证
endpoints.shutdown.sensitive=false
```

4）发送一个关闭服务的URL请求

需添加一个依赖

```xml
    <dependency>
        <groupId>org.apache.httpcomponents</groupId>
        <artifactId>httpclient</artifactId>
        <version>4.5</version>
    </dependency>
```

```java
public class HttpClientUtil {

	public static String doGet(String url, Map<String, String> param) {

		// 创建Httpclient对象
		CloseableHttpClient httpclient = HttpClients.createDefault();

		String resultString = "";
		CloseableHttpResponse response = null;
		try {
			// 创建uri
			URIBuilder builder = new URIBuilder(url);
			if (param != null) {
				for (String key : param.keySet()) {
					builder.addParameter(key, param.get(key));
				}
			}
			URI uri = builder.build();

			// 创建http GET请求
			HttpGet httpGet = new HttpGet(uri);

			// 执行请求
			response = httpclient.execute(httpGet);
			// 判断返回状态是否为200
			if (response.getStatusLine().getStatusCode() == 200) {
				resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (response != null) {
					response.close();
				}
				httpclient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return resultString;
	}

	public static String doGet(String url) {
		return doGet(url, null);
	}

	public static String doPost(String url, Map<String, String> param) {
		// 创建Httpclient对象
		CloseableHttpClient httpClient = HttpClients.createDefault();
		CloseableHttpResponse response = null;
		String resultString = "";
		try {
			// 创建Http Post请求
			HttpPost httpPost = new HttpPost(url);
			// 创建参数列表
			if (param != null) {
				List<NameValuePair> paramList = new ArrayList<>();
				for (String key : param.keySet()) {
					paramList.add(new BasicNameValuePair(key, param.get(key)));
				}
				// 模拟表单
				UrlEncodedFormEntity entity = new UrlEncodedFormEntity(paramList,"utf-8");
				httpPost.setEntity(entity);
			}
			// 执行http请求
			response = httpClient.execute(httpPost);
			resultString = EntityUtils.toString(response.getEntity(), "utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				response.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return resultString;
	}

	public static String doPost(String url) {
		return doPost(url, null);
	}
	
	public static String doPostJson(String url, String json) {
		// 创建Httpclient对象
		CloseableHttpClient httpClient = HttpClients.createDefault();
		CloseableHttpResponse response = null;
		String resultString = "";
		try {
			// 创建Http Post请求
			HttpPost httpPost = new HttpPost(url);
			// 创建请求内容
			StringEntity entity = new StringEntity(json, ContentType.APPLICATION_JSON);
			httpPost.setEntity(entity);
			// 执行http请求
			response = httpClient.execute(httpPost);
			resultString = EntityUtils.toString(response.getEntity(), "utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				response.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return resultString;
	}
	
	public static void main(String[] args) {
		String url ="http://127.0.0.1:9090/shutdown";
		//该url必须要使用dopost方式来发送
		HttpClientUtil.doPost(url);
	}
}
```

也就是运行 HttpClientUtil#main方法，修改好ip、端口即可



# Eureka 注册中心的安全认证

这里说的是服务端的安全认证，需要用户名、密码

这样做的目的：防止外部用户可以直接通过浏览器访问集群的后台页面 http://localhost:8761

1）添加security包

```xml
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-security</artifactId>
    </dependency>
```

2）全局配置文件

```properties
#开启http basic 的安全认证
security.basic.enabled=true
security.user.name=user
security.user.password=123456
```

3）修改访问集群节点的url

因为使用了密码，群节点之间的访问需要自带用户名密码

```
eureka.client.service-url.defaultZone=http://user:123456@eureka1:8761/eureka/
```

格式是：`用户名:密码@`

示例：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708111949.png)

eureka1：

```properties
spring.application.name=eureka-server
server.port=8761

#设置eureka实例名称，与配置文件的变量为主
eureka.instance.hostname=eureka1
#设置服务注册中心地址，指向另一个注册中心
eureka.client.service-url.defaultZone=http://user:123456@eureka2:8762/eureka/

#开启http basic 的安全认证
security.basic.enabled=true
security.user.name=user
security.user.password=123456
```

eureka2：

```properties
spring.application.name=eureka-server
server.port=8762

#设置eureka实例名称，与配置文件的变量为主
eureka.instance.hostname=eureka2
#设置服务注册中心地址，指向另一个注册中心
eureka.client.service-url.defaultZone=http://user:123456@eureka1:8761/eureka/

#开启http basic 的安全认证
security.basic.enabled=true
security.user.name=user
security.user.password=123456
```

4）微服务与注册中心之间的通讯也需要自带用户名和密码

如 eureka-provider 微服务

```properties
spring.application.name=eureka-provider
server.port=9090

#设置服务注册中心地址，指向另一个注册中心
eureka.client.service-url.defaultZone=http://user:123456@eureka1:8761/eureka/,user:123456@http://eureka2:8762/eureka/

#启用shutdown
endpoints.shutdown.enabled=true
#禁用密码验证
endpoints.shutdown.sensitive=false
```

其它微服务也需要自动用户名、密码

5）效果

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708112046.png)


# Q & A

## no main manifest attribute

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708112059.png)

原因，需要在子module模块的pom.xml文件中添加插件依赖：

```xml
    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <!-- 指定该Main Class为全局的唯一入口 -->
                    <mainClass>com.swapping.springcloud.ms.eureka.SpringcloudMsEurekaApplication</mainClass>
                    <layout>ZIP</layout>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>repackage</goal><!--可以把依赖的包都打包到生成的Jar包中-->
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
```

参考：https://www.cnblogs.com/sxdcgaq8080/p/10130621.html

---

其它学习资料《重新定义Spring Cloud实战.pdf》