---
title: "Java-framework-ActiveMQ"
date: 2019-08-01T11:01:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---


# 基础知识

## 消息协议/规范

### JMS

>JMS即Java消息服务（Java Message Service）应用程序接口，是一个Java平台中关于面向消息中间件（MOM）的API，用于在两个应用程序之间，或分布式系统中发送消息，进行异步通信。Java消息服务是一个与具体平台无关的API，绝大多数MOM提供商都对JMS提供支持。---百度百科

相关概念

- 消费者/订阅者：接收并处理消息的客户端
- 消息：应用程序之间传递的数据内容
- 消息模式：在客户端之间传递消息的方式，**主题模式**和**队列模式**。

主题模式：

- 客户端包括发布者和订阅者
- 主题中的消息被所有订阅者消费
- 消息者不能消费订阅之前的消息

队列模式：

- 客户端包括生产者和消费者
- 队列中的消息只能被一个消费者消费
- 消息者可以随时消费队列中的消息

JMS编码接口：

- ConnectionFactory:创建连接消息中间件工厂
- Connection:应用程序与消息服务器之间的通信链路
- Destination:消息发布和接收的地点，包括队列和主题
- Session:会话，单线程的上下文，用于发送和接收消息
- MessageConsumer:由会话创建，用于接收发送到目标的消息
- MessageProducer:由会话创建，用于发送消息到目标
- Message:在消息者和生产者之间传送的对象，消息头，一组消息属性，一个消息体

JMS编码接口之间的关系：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708074648.png)

### AMQP

>AMQP，即Advanced Message Queuing Protocol,一个提供统一消息服务的应用层标准高级消息队列协议,是应用层协议的一个开放标准,为面向消息的中间件设计。基于此协议的客户端与消息中间件可传递消息，并不受客户端/中间件不同产品，不同的开发语言等条件的限制。Erlang中的实现有 RabbitMQ等。---百度百科

JMS & AMQP:

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708074751.png)

## 协议/规范实现者

ActiveMQ & RabitMQ & Kafka:

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708074814.png)



## 消息中间件好处

- 解耦
- 异步
- 横向扩展

# ActiveMQ

## ActiveMQ下载安装

[Apache ActiveMQ][1]官网下载，下载ActiveMQ 5.15.1 windows版本，解压，进入MQ的bin目录\bin\win64，

启动服务：

- 非Windows服务方式：双击`\bin\win64\activemq.bat`启动服务
- 以Windows服务方式：双击`\bin\win64\InstallService.bat`安装服务，然后到windows服务中开启服务。若要卸载服务，双击执行`\bin\win64\UninstallService.bat`。

在浏览器访问`http://127.0.0.1:8161/`，测试安装是否成功，

## Demo

maven:

```xml
        <dependency>
            <groupId>org.apache.activemq</groupId>
            <artifactId>activemq-all</artifactId>
            <version>5.15.1</version>
        </dependency>
```

### 消息队列

生产者：

```java
public class AppProducer {
    private static final String url = "tcp://192.168.0.103:61616"; //61616是ActiveMQ默认的端口
    private static final String queueName = "queue-test";

    public static void main(String[] args) throws JMSException {
        //1. 创建ConnectionFaction
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);

        //2. 创建Connection
        Connection connection = connectionFactory.createConnection();

        //3. 启动连接
        connection.start();

        //4. 创建会话
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        //5. 创建一个目标
        Destination destination = session.createQueue(queueName);

        //6. 创建一个生产者
        MessageProducer producer = session.createProducer(destination);

        for(int i = 0;i < 100; i++) {
            //7. 创建消息
            TextMessage textMessage = session.createTextMessage("test " + i);
            //8. 发布消息
            producer.send(textMessage);

            System.out.println("发送消息："+textMessage.getText());
        }

        //9. 关闭连接
        connection.close();
    }
}
```

消费者：

```java
public class AppConsumer {
    private static final String url = "tcp://192.168.0.103:61616"; //61616是ActiveMQ默认的端口
    private static final String queueName = "queue-test";

    public static void main(String[] args) throws JMSException {
        //1. 创建ConnectionFaction
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);

        //2. 创建Connection
        Connection connection = connectionFactory.createConnection();

        //3. 启动连接
        connection.start();

        //4. 创建会话
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        //5. 创建一个目标
        Destination destination = session.createQueue(queueName);

        //6. 创建一个消费者
        MessageConsumer consumer = session.createConsumer(destination);

        //7. 创建一个监听器
        consumer.setMessageListener(new MessageListener() {
            public void onMessage(Message message) {
                TextMessage textMessage = (TextMessage) message;
                try {
                    System.out.println("接收到的消息："+((TextMessage) message).getText());
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        });

        //8. 关闭连接
        //connection.close(); //不能直接关闭，因此上面的监听器是个异步的过程，关闭后就接收不到消息
    }
```

### 主题队列

发布者：

```java
public class AppProducer {
    private static final String url = "tcp://192.168.0.103:61616"; //61616是ActiveMQ默认的端口
    private static final String topicName = "topic-test";

    public static void main(String[] args) throws JMSException {
        //1. 创建ConnectionFaction
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);

        //2. 创建Connection
        Connection connection = connectionFactory.createConnection();

        //3. 启动连接
        connection.start();

        //4. 创建会话
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        //5. 创建一个目标
        Destination destination = session.createTopic(topicName);

        //6. 创建一个生产者
        MessageProducer producer = session.createProducer(destination);

        for(int i = 0;i < 100; i++) {
            //7. 创建消息
            TextMessage textMessage = session.createTextMessage("test " + i);
            //8. 发布消息
            producer.send(textMessage);

            System.out.println("发送消息："+textMessage.getText());
        }

        //9. 关闭连接
        connection.close();
    }
}
```

订阅者：

```java
public class AppConsumer {
    private static final String url = "tcp://192.168.0.103:61616"; //61616是ActiveMQ默认的端口
    private static final String topicName = "topic-test";

    public static void main(String[] args) throws JMSException {
        //1. 创建ConnectionFaction
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);

        //2. 创建Connection
        Connection connection = connectionFactory.createConnection();

        //3. 启动连接
        connection.start();

        //4. 创建会话
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        //5. 创建一个目标
        Destination destination = session.createTopic(topicName);

        //6. 创建一个消费者
        MessageConsumer consumer = session.createConsumer(destination);

        //7. 创建一个监听器
        consumer.setMessageListener(new MessageListener() {
            public void onMessage(Message message) {
                TextMessage textMessage = (TextMessage) message;
                try {
                    System.out.println("接收到的消息："+((TextMessage) message).getText());
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        });

        //8. 关闭连接
        //connection.close(); //不能直接关闭，因此上面的监听器是个异步的过程，关闭后就接收不到消息
    }
}
```

注：发布者、订阅者的代码复制自**消息队列**中的代码，修改的只是`Destination destination = session.createTopic(topicName);`这行代码及涉及到的topicName名称。

## Spring + ActiveMQ

首先，要了解Spring提供的JMS的接口：

- ConnectionFactory:用于管理连接
- JmsTemplate:发送、接收消息的模板
- MessageListener:消息监听器

ConnectionFactory，是Spring提供的JMS连接池，它的子类有：

- SingleConnectionFactory
- CachingConnectionFactory

JmsTemplate，我们只需向Spring容器注删这个类，就可以使用JmsTemplate，它是线程安全的类，可以在整个范围内使用。

MessageListener，我们只需实现它的一个`onMessage`方法，该方法只接收一个Message参考。

### 队列模式

项目结构：

```text
src
    main
        java
            com.demo.jms
                consumer
                    AppConsumer
                    ConsumerMessageListener
                producer
                    AppProducer
                    ProducerService
                    ProducerServiceImpl
        resources
            common.xml
            consumer.xml
            producer.xml
pom.xml
```

依赖之pom.xml：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.demo.jms</groupId>
    <artifactId>jms-spring</artifactId>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <spring.version>4.0.8.RELEASE</spring.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.11</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <!--本次demo的主角-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jms</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.activemq</groupId>
            <artifactId>activemq-core</artifactId>
            <version>5.7.0</version>
            <exclusions>
                <exclusion>
                    <groupId>org.springframework</groupId>
                    <artifactId>spring-context</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
    </dependencies>

</project>
```

生产者接口：

```java
public interface ProducerService {
    void setMessage(String message);
}
```

生产者接口实现类：

```java
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import javax.annotation.Resource;
import javax.jms.*;

/**
 * Created by lwx on 2017/10/7.
 */
public class ProducerServiceImpl implements ProducerService {

    @Autowired
    JmsTemplate jmsTemplate;
    @Resource(name = "queueDistination")//第一处修改，将name = "topicDistination"时，则为主题模式
    Destination destination;

    public void setMessage(final String message) {
        jmsTemplate.send(destination, new MessageCreator() {
            public Message createMessage(Session session) throws JMSException {
                TextMessage textMessage = session.createTextMessage(message);
                return textMessage;
            }
        });
        System.out.println("发送消息："+message);
    }
}
```

Spring配置文件之common.xml:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">

    <context:annotation-config/>

    <!--ActiveMQ为我们提供的ConnectionFactory-->
    <bean id="targetConnectionFactory" class="org.apache.activemq.ActiveMQConnectionFactory">
        <property name="brokerURL" value="tcp://192.168.0.103:61616"/>
    </bean>

    <!--Spring为我们提供的连接池-->
    <bean id="connectionFactory" class="org.springframework.jms.connection.SingleConnectionFactory">
        <property name="targetConnectionFactory" ref="targetConnectionFactory"/>
    </bean>

    <!--一个队列目的地，点对点-->
    <bean id="queueDistination" class="org.apache.activemq.command.ActiveMQQueue">
        <!--队列的名称-->
        <constructor-arg value="queue"/>
    </bean>

    <!--一个主题目的地，发布订阅模式-->
    <bean id="topicDistination" class="org.apache.activemq.command.ActiveMQTopic">
        <constructor-arg value="topic"/>
    </bean>
</beans>
```

Spring配置文件之生产者producer.xml:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

    <import resource="common.xml"/>

    <!--配置JmsTemplate，用于发送消息-->
    <bean id="jmsTemplate" class="org.springframework.jms.core.JmsTemplate">
        <property name="connectionFactory" ref="connectionFactory"/>
    </bean>

    <bean id="producerService" class="com.demo.jms.producer.ProducerServiceImpl"/>
</beans>
```

生产者测试类AppProducer.java:

```java
public class AppProducer {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("producer.xml");
        ProducerService service = context.getBean(ProducerService.class);
        for(int i = 0; i<100; i++) {
            service.setMessage("test "+i);
        }
        context.close();
    }
}
```

消费者消息监听器ConsumerMessageListener.java:

```java
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * Created by lwx on 2017/10/7.
 */
public class ConsumerMessageListener implements MessageListener {
    public void onMessage(Message message) {
        TextMessage textMessage = (TextMessage) message;
        try {
            System.out.println("接收到的消息："+textMessage.getText());
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
```

Spring配置文件之消费者consumer.xml:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

    <import resource="common.xml"/>

    <!--配置消息监听器-->
    <bean id="consumerMessageListener" class="com.demo.jms.consumer.ConsumerMessageListener"/>

    <!--配置消息容器-->
    <bean id="jmsContainer" class="org.springframework.jms.listener.DefaultMessageListenerContainer">
        <property name="connectionFactory" ref="connectionFactory"/>

        <!--第二处修改，ref="topicDistination"时，则为主题模式-->
        <property name="destination" ref="queueDistination"/>

        <property name="messageListener" ref="consumerMessageListener"/>
    </bean>
</beans>
```

消费者之测试类AppConsumer.java:

```java
public class AppConsumer {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("consumer.xml");
    }
}
```

注意：要开启ActiveMQ服务。

### 主题模式

将**队列模式**中的**第一处修改**，**第二处修改**改好即可。

# FAQ

- 什么是中间件

    非底层操作系统软件，非业务应用软件，不直接给最终用户使用，不直接给客户带来价值的软件统称为中间件。

[1]:[http://activemq.apache.org/activemq-5151-release.html]
[2]:http://www.imooc.com/learn/856