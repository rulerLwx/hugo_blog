---
title: "Java-framework-Jedis"
date: 2019-08-01T11:03:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---


# 概述

官网：https://github.com/xetorthio/jedis

使用Jedis分几种情况：

- 单机版，可以直接`Jedis jedis = new Jedis("192.168.12.132", 6379)`
- 使用 JedisPool
- 使用 ShardedJedis，与集群相关，数据分片 ——20190916

另外，Java的Redis客户端有哪些？
- Jedis
- lettuce
- Redisson（官方推荐）


# 使用示例

## 事务（Transaction）

```
jedis.watch (key1, key2, ...);
Transaction t = jedis.multi();
t.set("foo", "bar");
t.exec();
```

上面代码参考：https://github.com/xetorthio/jedis/wiki/AdvancedUsage

示例代码：

```java
public class TestTX {
    public boolean transMethod() throws InterruptedException {
        Jedis jedis = new Jedis("127.0.0.1", 6379);
        jedis.auth("pass123");
        jedis.select(11);
        int balance;// 可用余额
        int debt;// 欠额
        int amtToSubtract = 10;// 实刷额度

        jedis.watch("balance");
        //jedis.set("balance","5");//此句不该出现，讲课方便。模拟其他程序已经修改了该条目
        Thread.sleep(7000); // 模拟高并发，阻塞了7s
        balance = Integer.parseInt(jedis.get("balance"));
        if (balance < amtToSubtract) {
            jedis.unwatch();
            System.out.println("modify");
            return false;
        } else {
            System.out.println("***********transaction");
            Transaction transaction = jedis.multi();
            transaction.decrBy("balance", amtToSubtract);
            transaction.incrBy("debt", amtToSubtract);
            transaction.exec();
            balance = Integer.parseInt(jedis.get("balance"));
            debt = Integer.parseInt(jedis.get("debt"));

            System.out.println("*******" + balance);
            System.out.println("*******" + debt);
            return true;
        }
    }

    /**
     * 通俗点讲，watch命令就是标记一个键，如果标记了一个键，
     * 在提交事务前如果该键被别人修改过，那事务就会失败，这种情况通常可以在程序中
     * 重新再尝试一次。
     * 首先标记了键balance，然后检查余额是否足够，不足就取消标记，并不做扣减；
     * 足够的话，就启动事务进行更新操作，
     * 如果在此期间键balance被其它人修改， 那在提交事务（执行exec）时就会报错，
     * 程序中通常可以捕获这类错误再重新执行一次，直到成功。
     *
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        TestTX test = new TestTX();
        boolean retValue = test.transMethod();
        System.out.println("main retValue-------: " + retValue);
    }
}
```

上面代码参考：https://github.com/huangsanyeah/redis1122/blob/master/src/com/atguigu/redis/test/TestTX.java


## 主从复制（Replication-Master/Slave）

```java
public class TestMS {
	public static void main(String[] args) {
		Jedis jedis_M = new Jedis("127.0.0.1",6379);
		Jedis jedis_S = new Jedis("127.0.0.1",6380);
		
		jedis_S.slaveof("127.0.0.1",6379);
		
		jedis_M.set("class","1122V2");
		
		String result = jedis_S.get("class");
		System.out.println(result);
	}
}
```

以上是通过代码方式实现主从复制，重启应用后，如果不执行此 main 方法，主从关系将丢失。**生产环境，不会用代码来实现主从关系**。

## JedisPool

单例，双重锁模式，创建线程池

```java
public class JedisPoolUtil {
    private static volatile JedisPool jedisPool = null;

    private JedisPoolUtil(){}
    
    // 单例，双重锁模式
    public static JedisPool getJedisPoolInstance()
    {
        if(null == jedisPool)
        {
            synchronized (JedisPoolUtil.class)
            {
                if(null == jedisPool)
                {
                    JedisPoolConfig poolConfig = new JedisPoolConfig();
                    poolConfig.setMaxActive(1000);
                    poolConfig.setMaxIdle(32);
                    poolConfig.setMaxWait(100*1000);
                    poolConfig.setTestOnBorrow(true);

                    jedisPool = new JedisPool(poolConfig,"127.0.0.1",6379);
                }
            }
        }
        return jedisPool;
    }

    public static void release(JedisPool jedisPool, Jedis jedis)
    {
        if(null != jedis)
        {
            jedisPool.returnResourceObject(jedis);
        }
    }
}
```

使用 JedisPool

```java
public class TestPool {
	public static void main(String[] args) {
		JedisPool jedisPool = JedisPoolUtil.getJedisPoolInstance();
		JedisPool jedisPool2 = JedisPoolUtil.getJedisPoolInstance();
		
		System.out.println(jedisPool == jedisPool2);
		
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.set("aa","bb");
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			JedisPoolUtil.release(jedisPool, jedis);
		}
	}
}
```

JedisPoolConfig常用配置：——参考《尚硅谷-Redis视频》

---

疑问：ShardedJedis、JedisPool、ShardedJedisPool 区别与联系 ——20190916

答：使用Jedis分几种情况：
- 单机版，可以直接`Jedis jedis = new Jedis("192.168.12.132", 6379)`
- 使用 JedisPool
- 使用 ShardedJedis，与集群相关，数据分片 ——20190916

参考：Jedis下的ShardedJedis https://www.cnblogs.com/alter888/p/8878855.html


# Jedis工具类

封装了Jedis连接池以及常用的操作

参考：https://github.com/sishen0718/JedisUtils