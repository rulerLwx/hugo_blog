---
title: "Java-framework-Spring-8"
date: 2019-08-15T11:10:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---


# Java Mail

```
public class MailTask implements Runnable {

    private String code;
    private String email;
    private JavaMailSender javaMailSender;
    private int operation;

    public MailTask(String code, String email, JavaMailSender javaMailSender,int operation) {
        this.code = code;
        this.email = email;
        this.javaMailSender = javaMailSender;
        this.operation = operation;
    }

    @Override
    public void run() {
        javaMailSender.send(new MimeMessagePreparator() {
            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                System.out.println("开始发邮件...");
                MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage,true,"UTF-8");
                mimeMessageHelper.setFrom(MyConstant.MAIL_FROM);
                mimeMessageHelper.setTo(email);
                mimeMessageHelper.setSubject("一封激活邮件");
                StringBuilder sb  = new StringBuilder();
                sb.append("<html><head></head><body>");

                if(operation==1){
                    sb.append("<a href="+MyConstant.DOMAIN_NAME+"activate.do?code=");
                    sb.append(code);
                    sb.append(">点击激活</a></body>");
                }else{
                    sb.append("是否将您的密码修改为:");
                    sb.append(code.substring(0,8));
                    sb.append("，<a href="+MyConstant.DOMAIN_NAME+"verify.do?code="+code+">");
                    sb.append("点击是</a></body>");
                }

                mimeMessageHelper.setText(sb.toString(),true);

                System.out.println("结束发邮件...");
            }
        });
    }
}
```
参考：https://github.com/fanchaoo/forum/blob/master/src/main/java/com/fc/async/MailTask.java



# Test

## Spring Test

```java
package com.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by lwx on 2019/6/5.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class SpringTest {

    @Autowired
    private UserService userService;

    @Test
    public void test(){
        System.out.println("------>SpringTest");
        System.out.println(userService.getName());
    }
}
```




## MVC Test

示例：

```
import com.demo.controller.UserController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * Created by lwx on 2017/8/6.
 */
@RunWith(SpringJUnit4ClassRunner.class)	//①
@WebAppConfiguration		//②
@ContextConfiguration(locations = {"classpath:config/applicationContext.xml",	//③
        "classpath:config/mybatis-config.xml",
        "classpath:config/springMVC-servlet.xml"})
public class UserControllerTest {

    @Autowired
    UserController userController;	//④

    MockMvc mockMvc;

    @Before
    public void before(){
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build(); //⑤
    }

    @Test
    public void testSayHi(){
        try {
            this.mockMvc.perform(MockMvcRequestBuilders.get("/user/sayHi?id=1").accept(MediaType.ALL_VALUE)); //⑥
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

①、②、③，一个都不能少；④是要测试的Controller；⑤是把④中的controller包装成一个模拟的controller；⑥是开始测试controller，模拟http请求和接收返回参数，在此处可以写请求的路径、请求方式（GET/POST/PUT/DELETE）、设置请求参数等。

要求servlet3.0及以上。



# 附件

## 命令空间大全

```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context" xmlns:jdbc="http://www.springframework.org/schema/jdbc"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:jee="http://www.springframework.org/schema/jee" xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:util="http://www.springframework.org/schema/util" xmlns:task="http://www.springframework.org/schema/task" 

xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
		http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.0.xsd
		http://www.springframework.org/schema/jdbc http://www.springframework.org/schema/jdbc/spring-jdbc-4.0.xsd
		http://www.springframework.org/schema/jee http://www.springframework.org/schema/jee/spring-jee-4.0.xsd
		http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-4.0.xsd
		http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util-4.0.xsd
		http://www.springframework.org/schema/task http://www.springframework.org/schema/task/spring-task-4.0.xsd
		http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-4.0.xsd"
       default-lazy-init="true">
</beans>
```

## web.xml

```
<!DOCTYPE web-app PUBLIC
 "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN"
 "http://java.sun.com/dtd/web-app_2_3.dtd" >

<web-app>
	<context-param>
		<param-name>contextConfigLocation</param-name>
		<param-value>classpath*:/config/applicationContext.xml</param-value>
	</context-param>
	<filter>
		<filter-name>encodingFilter</filter-name>
		<filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
		<init-param>
			<param-name>encoding</param-name>
			<param-value>UTF-8</param-value>
		</init-param>
		<init-param>
			<param-name>forceEncoding</param-name>
			<param-value>true</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>encodingFilter</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>
	<listener>
		<listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
	</listener>
</web-app>
```



## 配置线程池

```
    <!--线程池-->
    <bean id="taskExecutor" class="org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor">
        <property name="corePoolSize" value="10"/>
        <property name="maxPoolSize" value="20"/>
        <property name="queueCapacity" value="20"/>
    </bean>
```
