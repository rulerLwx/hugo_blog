---
title: "Java-framework-Hibernate"
date: 2019-08-01T11:02:30+08:00
draft: true
tags: []
categories: ["Java-framework"]
---

# 如何使用Hibernate

```
1、加载配置文件
2、从配置文件获得session工厂
3、人session工厂获得session
4、session开启事务
5、调用session里面的增、删、改、查方法
6、session提交事务
7、关闭session
```

session关闭之后，事务就没有了，注意持久化的变化。

cascade描述的是对象跟对象之间的操作，inverse描述是的对象对关系之间的操作。

sessionFactory.getCurrentSession();之后不需要关闭session。这种的增删改查都需要事务环境。

曾经用过的数据就会放到缓存中。

懒加载：
    1、配置文件：lazy = "xxxx"
    2、代码：session.load(xxx.class,id);


# 几种状态：临时、持久化、托管

临时、持久化、托管

![image](https://oscimg.oschina.net/oscnet/43a0605ebddc0f151b9a6d6d2f9dfdc95f4.jpg)

自我总结：以数据库为界，来判断；id+session。

经典图：

![image](https://oscimg.oschina.net/oscnet/e304a5391c2b7be42266d9872bc1cd57303.jpg)

比喻：单身、结婚、离婚



# hibernate.ddl-auto

- create 启动时删数据库中的表，然后创建，退出时不删除数据表
- create-drop 启动时删数据库中的表，然后创建，退出时删除数据表 如果表不存在报错
- update 如果启动时表格式不一致则更新表，原有数据保留
- validate 项目启动表结构进行校验 如果不一致则报错

参考：https://blog.csdn.net/fengyuhan123/article/details/80264795



# Q & A

## 几种方法的区别

- load与get

如果配置了延迟加载，load()方法会延迟加载，get()方法不会。

- merge与update

merge()不会持久化给定的对象，update()则会。共同点是：都将数据同步到数据库了。PS：merge()把游离状态同步到数据库。