---
title: "Java-framework-MyBatis-4-整合Spring"
date: 2019-08-05T11:06:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---


# Spring整合MyBatis

参考 **Java-framework-SpringMVC-3** 

spring整合mybatis有两种方式

- 一种是使用SqlSessionTemplate
- 另一种是使用mapper接口

在一个项目里面，可以选用一种或两种方式都使用。

```
spring整合mybatis——使用Mapper接口

spring配置技巧：
1、配置数据源
2、配置SqlSessionFactoryBean
    2.1数据源
    2.2mapper.xml
    2.3mybatis-config.xml
3、配置MapperScannerConfigurer
    3.1sqlSessionFactoryBeanName
    3.2basePackage
    3.3annotationClass

在业务层注入basePackage对应的DAO。
```

```
spring整合mybatis——使用SqlSessionTemplate

spring配置技巧：
1、配置数据源
2、配置SqlSessionFactoryBean
    2.1数据源
    2.2mapper.xml
    2.3mybatis-config.xml
3、配置MapperScannerConfigurer
    3.1SqlSessionTemplate

在业务层注入SqlSessionTemplate。
```

## 示例——使用Mapper接口

Mapper接口开发需要遵循以下规范：

- 1、Mapper.xml文件中的namespace与mapper接口的类路径相同。
- 2、	Mapper接口方法名和Mapper.xml中定义的每个statement的id相同 
- 3、Mapper接口方法的输入参数类型和mapper.xml中定义的每个sql 的parameterType的类型相同
- 4、Mapper接口方法的输出参数类型和mapper.xml中定义的每个sql的resultType的类型相同

## 示例——使用SqlSessionTemplate


# 其它基于MyBatis的框架（或说是插件）

## pagehelper

https://github.com/pagehelper/Mybatis-PageHelper


# Q & A

## #{}和${}的区别是什么？

正确的答案是：#{}是预编译处理，${}是字符串替换。

参考：http://www.mybatis.cn/archives/70.html，https://www.cnblogs.com/kangyun/p/5881531.html

## MyBatis中主要类的生命周期和应用范围

MyBatis中常用的类就要数SqlSessionFactoryBuilder、SqlSessionFactory、SqlSession、SqlMapper了。那么下面对他们的应用范围和生命周期进行一下说明： 

1、SqlSessionFactoryBuilder：在应用中该类实例的主要作用是创建SqlSessionFactory实例，故任务完成后就可以消失了。因此该类实例的最佳应用范围和生命周期应为“方法范围”。 

2、SqlSessionFactory：在应用的整个周期中会有众多地方需要利用其实例打开某个SqlSession，因此最佳范围是“应用生命周期范围”。故此，可以使用单例与工厂模式，在官方文档中最佳建议是IoC容器，如Spring来生成该实例。 

3、SqlSession：该类是非线程安全的，其实例是不能共享的，所以应该每个线程具有自己的SqlSession实例。因此最佳建议是“请求或方法范围”。例如：收到一个Http请求后，创建一个实例，进行某些操作，之后关闭。确保将关闭放于finally中。 

4、SqlMapper：是创建绑定映射语句的接口。其实例从SqlSession获得，所以其最宽生命周期与SqlSession相同，因此其实例的执行范围也是“方法范围”，而且其不需要明确的关闭。

来自：https://www.cnblogs.com/tv151579/p/3295763.html