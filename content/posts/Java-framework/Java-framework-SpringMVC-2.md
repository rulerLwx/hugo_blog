---
title: "Java-framework-SpringMVC-2"
date: 2019-08-16T11:11:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---

# 数据格式转换

## 入参

数据格式化，如把日期格式化为 yyyy-MM-dd。

### <mvc:annotation-driven/>配置的作用

<mvc:annotation-driven/>会自动注册如下三个bean：
- RequestMappingHandlerMapping
- RequestMappingHandlerAdapter
- ExceptionHandlerExceptionResolver

除此外，还提供如下支持：

- 支持使用ConversionService实例（默认是FormattingConversionServiceFactoryBean）对表单参数进行类型转换
- 支持使用 @NumberFormatannotation、@DateTimeFormat 注解完成数据类型的格式化
- 支持使用 @Valid 注解对JavaBean实例进行JSR303验证
- 支持使用 @RequestBody 和 @ResponseBody 注解


### 格式化

<mvc:annotation-driven/>默认创建的ConversionService实例为FormattingConversionServiceFactoryBean，其内部已经注册了如下转换器：

- NumberFormatAnnotationFormatterFactory，支持对数字类型的属性使用 @NumberFormat 注解
- JodaDateTimeFormatAnnotationFormatterFactory，支持对日期类型的属性使用 @DateTimeFormat 注解

#### @DateTimeFormat

可对java.util.Date、java.util.Calendar、java.long.Long 时间类型进行标注以进行格式化。

@DateTimeFormat 可设置如下属性：

- pattern 属性：类型为字符串。指定解析/格式化字段数据的模式，如：yyyy-MM-dd hh:mm:ss。
- iso 属性：类型为 DateTimeFormat.ISO。指定解析/格式化字段数据的ISO模式，包括四种：

    - ISO.NONE（不使用） -- 默认
    - ISO.DATE(yyyy-MM-dd)
    - SO.TIME(hh:mm:ss.SSSZ)
    - ISO.DATE_TIME(yyyy-MM-dd hh:mm:ss.SSSZ)

- style 属性：字符串类型。通过样式指定日期时间的格式，由两位字符组成，第一位表示日期的格式，第二位表示时间的格式：S：短日期/时间格式、M：中日期/时间格式、L：长日期/时间格式、F：完整日期/时间格式、-：忽略日期或时间格式

示例：

1）页面：

2）后台字段：

```java
@DateTimeFormat(pattern="yyyy-MM-dd")
private Date birth;
```

也就是说，页面的格式跟pattern="yyyy-MM-dd"一致，才能调试通过。

此注解使用的前提是在springmvc配置文件中配置<mvc:annotation-driven/>注解。


#### @NumberFormat

@NumberFormat拥有两个互斥的属性：

- style：类型为 NumberFormat.Style。用于指定样式类型，包括三种：
    - Style.NUMBER（正常数字类型）
    - Style.CURRENCY（货币类型）
    - Style.PERCENT（百分数类型）
- pattern：类型为 String，自定义样式， 如patter="#,###"；

示例

```java
@NumberFormat(pattern="#,###,###.#")
private Float salary;
```

用#表示数字。

此注解使用的前提是在springmvc配置文件中配置<mvc:annotation-driven/>注解。


### 类型转换出错时，如何捕捉呢？

```java
@RequestMapping(value="/testFormatDate",method=RequestMethod.POST)
public String testFormatDate(PersonInfo person,BindingResult result){
	System.out.println(person.getBirth());
	if(result.getErrorCount()>0	){
		System.out.println("出错了");
		for(FieldError err:result.getFieldErrors()){
			System.out.println(err.getField()+":"+err.getDefaultMessage());
		}
	}
	return SUCESSED;
}
```



### 自定义类型转换器

1）实现接口，自定义类型转换器

Spring 定义了 3 种类型的转换器接口，实现任意一个转换器接口都可以作为自定义转换器注册到ConversionServiceFactroyBean 中

- Converter<S,T>：将 S 类型对象转为 T 类型对象
- ConverterFactory：将相同系列多个“同质”Converter 封装在一起。如果希望将一种类型的对象转换为另一种类型及其子类的对象（例如将 String 转换为 Number 及 Number 子类（Integer、Long、Double 等）对象）可使用该转换器工厂类
- GenericConverter：会根据源类对象及目标类对象所在的宿主类中的上下文信息进行类型转换

```java
public class MyConversion implements Converter<String, Date> {
	public String datePattern;
	
	public MyConversion(String datePattern) {
		this.datePattern = datePattern;
		System.out.println("dataPattern:"+datePattern);
	}

	@Override
	public Date convert(String source) {
		try {
			System.out.println("source:"+source);
			SimpleDateFormat sf = new SimpleDateFormat(datePattern);
			sf.setLenient(false);
			return sf.parse(source);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
}
```

2）将类型转换器注册到ConversionServiceFactroyBean中，也可以是FormattingConversionServiceFactoryBean

```xml
	<bean id="conversionService"
		class="org.springframework.context.support.ConversionServiceFactoryBean">
		<property name="converters">
			<set>
				<bean class="com.demo.conversion.MyConversion">
					<constructor-arg type="java.lang.String" value="yyyy-MM-dd" />
				</bean>
			</set>
		</property>
	</bean>
```

3）注册类型转换器

```xml
<mvc:annotation-driven conversion-service="conversionService"/>
```

将自定义的 ConversionService 注册到Spring MVC 的上下文中。conversionService是ConversionServiceFactroyBean的id名称。

4）页面

```html
<a href="test/testConversion?apply_time=08/01/2017">测试自定义转换器testConversion</a>
```

5） Controller

```java
@Controller
@RequestMapping("/test")
public class TestController {
    @RequestMapping("/testConversion")
    public String testConversion(@RequestParam("apply_time")Date apply_time){
        System.out.println(apply_time);
        return "addUser";
    }
}
```

此时 apply_time=08/01/2017 是不能转换的

```txt
source:08/01/2017
java.text.ParseException: Unparseable date: "08/01/2017"
	at java.text.DateFormat.parse(DateFormat.java:366)
```


## 出参

需要导入 jackson 依赖包（参考 JSON 章节）

### @JsonFormat

```java
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date birthday;
```

https://blog.csdn.net/qq_28483283/article/details/81326365


## 总结

在字段上添加注解，其实都是springmvc在做工作，如果在controller中打印从数据库中获取的数据，是不按"yyyy-MM-dd"格式出现的，在终端才会显示，所以是springmvc在做工作。

其原理是 HttpMessageConverter，请参考 JSON 章节。

user.java
```java
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date birthday;
```

controller
```java
    @ResponseBody
    @RequestMapping("/getUser")
    public List<User> getUser(String name, Model model){
        List<User> userList = userService.getUser(name);

        for(User user: userList) {
            System.out.println(user.getName()+","+user.getBirthday());
        }

        return userList;
    }
```

控制台

```txt
wangwu,Sun May 12 00:00:00 GMT+08:00 2019
wangwu,Sun May 12 00:00:00 GMT+08:00 2019
wangwu,Sun May 12 00:00:00 GMT+08:00 2019
```

浏览器

```json
[{"id":47,"name":"wangwu","birthday":"2019-05-12","email":"wangwu@163.com"},{"id":48,"name":"wangwu","birthday":"2019-05-12","email":"wangwu@163.com"},{"id":49,"name":"wangwu","birthday":"2019-05-12","email":"wangwu@163.com"}]
```




# 表单校验

## 传统（官方）校验

在 Spring MVC 中，可直接通过注解驱动的方式进行数据校验。

Spring的 LocalValidatorFactroyBean 既实现了 Spring 的 Validator 接口，也实现了JSR 303 的 Validator 接口。

只要在 Spring 容器中定义了一个 LocalValidatorFactoryBean，即可将其注入到需要数据校验的 Bean 中。

Spring 本身并没有提供 JSR303 的实现，所以必须将JSR303 的实现者的 jar 包放到类路径下。

<mvc:annotation-driven/> 会默认装配好一个 LocalValidatorFactoryBean，通过在处理方法的入参上标注 @valid 注解即可让 Spring MVC 在完成数据绑定后执行数据校验的工作。

Spring MVC 是通过对处理方法签名的规约来保存校验结果的：前一个表单/命令对象的校验结果保存到随后的入参中，这个保存校验结果的入参必须是 BindingResult 或 Errors 类型，这两个类都位于 org.springframework.validation 包中。

JSR303

- @Null    被注释的元素必须为 null
- @NotNull    被注释的元素必须不为 null
- @AssertTrue    被注释的元素必须为 true
- @AssertFalse    被注释的元素必须为 false
- @Min(value)    被注释的元素必须是一个数字，其值必须大于等于指定的最小值
- @Max(value)    被注释的元素必须是一个数字，其值必须小于等于指定的最大值
- @DecimalMin(value)    被注释的元素必须是一个数字，其值必须大于等于指定的最小值
- @DecimalMax(value)    被注释的元素必须是一个数字，其值必须小于等于指定的最大值
- @Size(max, min)    被注释的元素的大小必须在指定的范围内
- @Digits (integer, fraction)    被注释的元素必须是一个数字，其值必须在可接受的范围内
- @Past    被注释的元素必须是一个过去的日期
- @Future    被注释的元素必须是一个将来的日期
- @Pattern(value)    被注释的元素必须符合指定的正则表达式

Hibernate Validator 附加的 constraint：

- @Email    被注释的元素必须是电子邮箱地址
- @Length    被注释的字符串的大小必须在指定的范围内
- @NotEmpty    被注释的字符串的必须非空
- @Range    被注释的元素必须在合适的范围内


校验步骤：

1）加入Hibernate Validator验证框架Jar包。

```xml
<dependency>
	<groupId>org.hibernate</groupId>
	<artifactId>hibernate-validator</artifactId>
	<version>5.3.5.Final</version>
</dependency>
```

2）在springmvc配置文件中添加 <mvc:annotation-driven/>。

3）在bean的对应属性上添加对应的注解。

```java
public class PersonInfo {
	@NotEmpty
	private String username;
	@Email
	private String email;
```

4）在目标方法Bean类型的前面加上 @Valid 注解。

```java
@RequestMapping(value="/testValidator",method=RequestMethod.POST)
public String testValidator(@Valid PersonInfo person,BindingResult result){
	System.out.println("testValidator:");
	if(result.getErrorCount()>0	){
		System.out.println("出错了");
		for(FieldError err:result.getFieldErrors()){
			System.out.println(err.getField()+":"+err.getDefaultMessage());
		}
	}
	return SUCESSED;
}
```

注意：BindingResult 必须在被校验字段或对象的后面，中间不能有其它参数。


5）页面

```html
<form action="requestParamTest/testValidator" method="post">
	用户名：<input type="text" name="username"><br>
	邮箱：<input type="text" name="email"><br>
	<input type="submit" value="测试  testValidator 请求">
</form>
```

增加map入参，还可以回显数据（需要使用springmvc标签）

参考：https://www.cnblogs.com/myinspire/articles/7649027.html


## 自定义校验

需求：定义一个校验手机号码的注解

```java
public class IsMobileValidator implements ConstraintValidator<IsMobile ,String >{

	private boolean  required = false;
	
	public void initialize(IsMobile constraintAnnotation) {
		required = constraintAnnotation.required();
	}

	public boolean isValid(String value, ConstraintValidatorContext context) {
		if(required) {
			return ValidatorUtil.isMobile(value);
		}else {
			if(StringUtils.isEmpty(value)) {
				return true;
			}else {
				return ValidatorUtil.isMobile(value);
			}
		}
	}

}
```

```java
@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD, ElementType.FIELD})  
@Retention(RetentionPolicy.RUNTIME)  
@Documented  
@Constraint(validatedBy = {IsMobileValidator.class})
public @interface IsMobile {
    String message() default "手机号码格式有误";  
    Class<?>[] groups() default {};  
    Class<? extends Payload>[] payload() default {};  
    boolean required() default true;
}
```

总结：这个需求，能否用`@Pattern(regexp = "1\\d{10}",message = "手机号码格式有误")`来解决



# JSON

## 使用JSON

### 步骤一：加入JSON的Jar包

注意：Spring默认使用  JackSon，所以加入jackson依赖包，分1.x和2.x版本

2.x版本：

```xml
    <properties>
        ...
        <jackson.version>2.9.8</jackson.version>
        ...
    </properties>
    
    <!--jackson-->
    <!--自动包含了 jackson-core/jackson-databind/jackson-annotation-->
    <dependency>
        <groupId>com.fasterxml.jackson.core</groupId>
        <artifactId>jackson-databind</artifactId>
        <version>${jackson.version}</version>
    </dependency>
```

1.x版本（tecsun）：

```xml
<dependency>
	<groupId>org.codehaus.jackson</groupId>
	<artifactId>jackson-mapper-asl</artifactId>
	<version>1.9.13</version>
</dependency>
<dependency>
	<groupId>org.codehaus.jackson</groupId>
	<artifactId>jackson-core-asl</artifactId>
	<version>1.9.13</version>
</dependency>
```

1.x 参考：https://github.com/FasterXML/jackson-1

结合笔记《Java-framework-JSON》进行学习比较。

### 步骤二：用@ResponseBody修饰处理方法中，并返回要生成的JSON数据。

```java
@RequestMapping(value="/testJson")
@ResponseBody
public Object testJson(){
	ArrayList<PersonInfo> list = new ArrayList<PersonInfo>();
	list.add(new PersonInfo("张三1", "zs@163.com"));
	list.add(new PersonInfo("张三2", "zs@163.com"));
	list.add(new PersonInfo("张三3", "zs@163.com"));
	return list;
}
```



## 底层原理

之所以可以生成JSON，底层使用的是HttpMessageConverter<T>，其 是 Spring3.0 新添加的一个接口，负责将请求信息转换为一个对象（类型为 T），将对象（类型为 T）输出为响应信息。

HttpMessageConverter<T>接口定义的方法：

- Boolean canRead(Class<?> clazz,MediaType mediaType): 指定转换器可以读取的对象类型，即转换器是否可将请求信息转换为 clazz 类型的对象，同时指定支持 MIME 类型(text/html,applaiction/json等)。
- Boolean canWrite(Class<?> clazz,MediaType mediaType):指定转换器是否可将 clazz 类型的对象写到响应流中，响应流支持的媒体类型在MediaType 中定义。
- LIst<MediaType> getSupportMediaTypes()：该转换器支持的媒体类型。
- T read(Class<? extends T> clazz,HttpInputMessage inputMessage)将请求信息流转换为 T 类型的对象。
- void write(T t,MediaType contnetType,HttpOutputMessgae outputMessage):将T类型的对象写到响应流中，同时指定相应的媒体类型为 contentType。

入参、出参的转换图示：

![image](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708120508.png)


使用 HttpMessageConverter<T> 将请求信息转化并绑定到处理方法的入参中或将响应结果转为对应类型的响应信息，Spring 提供了两种途径：
- 使用 @RequestBody / @ResponseBody – 对处理方法进行标注
- 使用 HttpEntity<T> / ResponseEntity<T> 作为处理方法的入参或返回值
当控制器处理方法使用到@RequestBody/@ResponseBody或
HttpEntity<T> /ResponseEntity<T> 时, Spring 首先根据请求头或响应头的Accept 属性选择匹配的 HttpMessageConverter, 进而根据参数类型或泛型类型的过滤得到匹配的 HttpMessageConverter, 若找不到可用的HttpMessageConverter 将报错。

如何找到对应的 HttpMessageConverter ？—— 根据入参、返回值类型

![image](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708120525.png)


@RequestBody和@ResponseBody不需要成对出现。

## @RequestBody、@ResponseBody使用示例

1）页面：

```html
<form action="requestParamTest/testHttpMessageConverter" method="post" 
	enctype="multipart/form-data">
	文件：<input type="file" name="file">
	描述：<input type="text" name="desc"><br>
	<input type="submit" value="测试  testJson 请求">
</form>
```

2）后台：

```java
@ResponseBody
@RequestMapping(value="/testHttpMessageConverter")
public String testHttpMessageConverter(@RequestBody String txt){
	System.out.println(txt);
	return "hello world! "+new Date();
}
```

使用了@ResponseBody后，就算返回的是String，也不跳转到页面了，而是直接向页面输出。


## HttpEntity<T>、ResponseEntity<T>使用示例

ResponseEntity使用示例（相当于下载一个文件）：

1）页面

`<a href="requestParamTest/testResponseEntity">testResponseEntity</a>`

2）后台

```java
@RequestMapping(value="/testResponseEntity")
public ResponseEntity<byte[]> testResponseEntity(HttpSession session){
	byte[] body = null;
	try {
		ServletContext context = session.getServletContext();
		InputStream is = context.getResourceAsStream("/files/pom.xml");
		body = new byte[is.available()];
		is.read(body);
	} catch (IOException e) {
		e.printStackTrace();
	}
	
	HttpHeaders headers = new HttpHeaders();
	headers.add("Content-Disposition", "attachment;filename=pom.xml");
	
	HttpStatus statusCode = HttpStatus.OK;
	
	ResponseEntity<byte[]> responseEntity = 
			new ResponseEntity<byte[]>(body, headers, statusCode);
	return responseEntity;
}
```

# 文件上传

Spring MVC 为文件上传提供了直接的支持，这种支持是通过即插即用的 MultipartResolver实现的。Spring用Jakarta Commons FileUpload技术实现了一个MultipartResolver 实现类：CommonsMultipartResolver。

Spring MVC 上下文中默认没有装配 MultipartResovler，因此默认情况下不能处理文件的上传工作，如果想使用 Spring 的文件上传功能，需现在上下文中配置 MultipartResolver。


1）加入Jar包

```xml
<dependency>
	<groupId>commons-fileupload</groupId>
	<artifactId>commons-fileupload</artifactId>
	<version>1.2.2</version>
</dependency>
<dependency>
	<groupId>commons-io</groupId>
	<artifactId>commons-io</artifactId>
	<version>2.0.1</version>
</dependency>
```

1.4版本自动包含了common-io

```xml
<dependency>
  <groupId>commons-fileupload</groupId>
  <artifactId>commons-fileupload</artifactId>
  <version>1.4</version>
</dependency>
```

问：版本对jdk有要求吗？

答：Apache Commons FileUpload 1.4 (requires Java 1.6 or later)

参考：https://commons.apache.org/proper/commons-fileupload/download_fileupload.cgi


2）在SpringMVC配置文件中配置MultipartResolver

```xml
<bean id="multipartResolver" class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
	<property name="defaultEncoding" value="UTF-8"/>
	<property name="maxUploadSize" value="1024000"/>
</bean>
```

注意：此处的property是根据setXxxx方法来确定的，不是字段了。

3）页面

```html
<form action="requestParamTest/testFileUpload" method="post" enctype="multipart/form-data">
	文件：<input type="file" name="file">
	描述：<input type="text" name="desc"><br>
	<input type="submit" value="测试  testFileUpload 请求">
</form>
```

4）后台处理方法

```java
@RequestMapping(value="/testFileUpload")
public String testFileUpload(@RequestParam("desc")String desc,@RequestParam("file")MultipartFile file) throws IOException{
	System.out.println("desc:"+desc);
	System.out.println("file:"+file.getOriginalFilename());
	System.out.println("InputStream:"+file.getInputStream());
	return "sucess.jsp";
}
```

由此看到，@RequestParam可以作用于post方式的参数。




# 拦截器

## 自定义拦截器

Spring MVC也可以使用拦截器对请求进行拦截处理，用户可以自定义拦截器来实现特定的功能，自定义的拦截器必须实现HandlerInterceptor接口。该接口有如下方法：

- preHandle()：这个方法在业务处理器处理请求之前被调用，在该方法中对用户请求 request 进行处理。如果程序员决定该拦截器对请求进行拦截处理后还要调用其他的拦截器，或者是业务处理器去进行处理，则返回true；如果程序员决定不需要再调用其他的组件去处理请求，则返回false。
- postHandle()：这个方法在业务处理器处理完请求后，但是DispatcherServlet 向客户端返回响应前被调用，在该方法中对用户请求request进行处理。
- afterCompletion()：这个方法在 DispatcherServlet 完全处理完请求后被调用，可以在该方法中进行一些资源清理的操作。

步骤：

1、定义一个类，实现HandlerInterceptor接口。

2、在springmvc的配置文件中增加拦截器配置：

```xml
<mvc:interceptors>
	<bean class="com.demo.interceptor.MyInterceptor"/>
</mvc:interceptors>
```

可以配置多个<bean.../>。

日志可以在preHandle()方法里面写。



## 拦截器配置

配置拦截器时，可以配置（不）拦截的路径，如下：

```xml
<mvc:interceptors>
	<bean class="com.demo.interceptor.MyInterceptor"/>
	<mvc:interceptor>
		<mvc:mapping path="/user"/>
		<bean class="com.demo.interceptor.MyInterceptor2"/>
	</mvc:interceptor>
</mvc:interceptors>
```


## 拦截器顺序

遵循先进后出原则。顺序跟<mvc:interceptors .../>中定义的顺序一致，即定义在前面的排序在前面。

正常执行的情况：

![image](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708120551.png)

异常的情况：

如果SecondInterceptor#preHandle时返回flase，则不执行下面的过程，直接执行到FirstInterceptor#afterCompletion。


## 拦截器原理

springmvc拦截器使用和原理理解：https://blog.csdn.net/jiangyie000/article/details/80691186

# 异常处理

## 概述

Spring MVC 通过 HandlerExceptionResolver 处理程序的异常，包括 Handler 映射、数据绑定以及目标方法执行时发生的异常。

如果使用了<mvc:annotation-driven.../>配置，则DispatcherServlet 默认装配的异常处理如下：

- ExceptionHandlerExceptionResolver
    - Controller类中使用 @ExceptionHandler 定义的方法将被用来处理当前controller的异常（**controller级别**），可以声明多个。
    - @ExceptionHandler 注解定义的方法优先级问题：例如发生的是NullPointerException，但是声明的异常有RuntimeException 和 Exception，此时会根据异常的最近继承关系找到继承深度最浅的那个@ExceptionHandler注解方法。
    - @ControllerAdvice 用于注解类（**全局异常**），如果在当前Controller类中找不到@ExceptionHandler注解的方法的话，会在Spring容器中查找使用 @ControllerAdvice 注解的类中的用 @ExceptionHandler 注解的方法来处理异常。

- ResponseStatusExceptionResolver，使用@ResponseStatus自定义异常类或方法。

    @ResponseStatus可指定以下两个值：
    - value，指定一个HttpStatus的常量值。
    - reason，异常文本消息。

- DefaultHandlerExceptionResolver，SpringMVC默认实现并提供的异常处理类。


## ExceptionHandlerExceptionResolver

ExceptionHandlerExceptionResolver，常用@ExceptionHandler和@ControllerAdvice来处理异常。

### @ExceptionHandler

在Controller方法上使用@ExceptionHandler注解处理异常，其value属性接收一个异常类组成的数组。

注意：@ExceptionHandler 处理的是当前 **controller 级别**的异常，不是全局的异常。

示例：

```java
@ExceptionHandler({ArithmeticException.class})
public ModelAndView handleException(Exception ex){
	System.out.println("出错了："+ex);
	ModelAndView mv = new ModelAndView("error");
	mv.addObject("exception", ex);
	return mv;
}
```
注意：
1. 入参增加 Exception ex ，即是发生异常时所封装的异常对象；
2. 如果要将错误信息传到页面，则返回值必须是ModelAndView ，如 new ModelAndView("error") 就可以传到“error.jsp”。
3. 也可以返回JSON数据，这里只是举例

```java
    @ResponseBody
    @ExceptionHandler({Exception.class})
    public List<User> handleException(Exception ex){
        System.out.println("出错了："+ex);
        return userService.listUsers();
    }
```


### @ControllerAdvice + @ExceptionHandler

定义一个类，用@ControllerAdvice修饰，其中的方法用@ExceptionHandler修饰，这个类就是**全局**的异常处理了（记得把此类交给Spring管理）。

```java
@ControllerAdvice
public class MyControllerExceptionHandler {

    @ResponseBody
    @ExceptionHandler({Exception.class})
    public String handleException(Exception ex) {
        System.out.println("出错了："+ex);
        String message = ex.getMessage();
        return "error";
    }
}
```

注意：Spring MVC 配置文件需要配置 <mvc:annotation-driven/>，否则此异常类不起作用。

参考：https://blog.csdn.net/x6582026/article/details/76400422

入参可以有哪些？

- HttpServletRequest request
- Throwable ex
- Exception ex ——20190817

（@ControllerAdvice标记的类，就相当于一个controller，入参、返回值相同）


## ResponseStatusExceptionResolver

ResponseStatusExceptionResolver的使用是通过@ResponseStatus注解来实现的，@ResponseStatus可以修饰自定义的异常类，也可以修饰Controller里面的方法。

1） 作用在自定义异常类上

在自定义的类上加上@ResponseStatus修饰，指定value、reason属性。然后在Controller处理方法中抛出自定义的异常。

2） 作用在controller方法上



## DefaultHandlerExceptionResolver

SpringMVC默认就定义了很多异常处理器。在DefaultHandlerExceptionResolver#doResolveException()方法中可以看到。


## SimpleMappingExceptionResolver

当出现异常时，可以通过SimpleMappingExceptionResolver来将异常映射跳转到哪个页面上。

示例：

1）在springmvc配置文件上，配置异常映射

```xml
<bean class="org.springframework.web.servlet.handler.SimpleMappingExceptionResolver">
	<property name="exceptionMappings">
		<props>
			<prop key="java.lang.ArrayIndexOutOfBoundsException">error</prop>
		</props>
	</property>
</bean>
```

2）后台方法，当数组索引越界时，将出现异常

```java
@RequestMapping(value="/testSimpleMappingExceptionResolver")
public String testSimpleMappingExceptionResolver(int i){
	String[] arr = {"a","b","c","d"};
	System.out.println("arr["+i+"]:"+arr[i] );
	return SUCESSED;
}
```

3）调用方法的页面

```html
<a href="requestParamTest/testSimpleMappingExceptionResolver?i=1">
测试 testSimpleMappingExceptionResolver 
</a>
```

4）error页面

${requestScope.exception }

## 总结

ExceptionHandlerExceptionResolver 提供的 @ExceptionHandler 和 @ControllerAdvice 分别对应 controller级别、全局级别的异常处理方式。

那么如何处理service/dao/converter等等出现的异常呢？(下面是我的总结-2019)

1. controller/service/dao中的异常都可以往外抛，然后在controller中用 @ExceptionHandler 定义方法来处理相应的异常
2. converter中的异常，TODO...



如下面的by zero异常可以处理：

```java
@Controller
@RequestMapping("/test")
public class TestController {
    @RequestMapping("/testConversion")
    public String testConversion(@RequestParam("apply_time")Date apply_time){
        System.out.println(apply_time);

        int i =10/0;    //这里异常

        return "addUser";
    }

    @ExceptionHandler({Exception.class})
    public ModelAndView handleException(Exception ex){
        System.out.println("出错了："+ex);
        ModelAndView mv = new ModelAndView("error");
        mv.addObject("exception", ex);
        return mv;
    }
}
```

但在 @RequestParam("apply_time")Date apply_time 对应的处理器转换时出现异常，controller中的用@ExceptionHandler标记的方法却处理不了

---

总结——异常处理方式：

1）在 controller 层调用service 层方法，service 层使用 throw 抛出异常，然后使用全局异常类捕捉异常

注：全局异常是指`ControllerAdvice + @ExceptionHandler`定义的类，其中使用`instanceof`判断异常种类

```java
@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {
	
	@ExceptionHandler(value=Exception.class)
	public  Result<String> exceptionHandler(HttpServletRequest request,Exception e){
		e.printStackTrace();
		if(e instanceof GlobalException) {
			GlobalException ex = (GlobalException) e;
			return Result.error(ex.getCm());
		}else if(e instanceof BindException) {
			BindException ex = (BindException) e;
			List<ObjectError> errors = ex.getAllErrors();
			ObjectError error = errors.get(0);
			String msg = error.getDefaultMessage();
			
			return Result.error(CodeMsg.BIND_ERROR.fillArgs(msg));
		}else {
			return Result.error(CodeMsg.SERVER_ERROR);
		}
	}
}
```

这样写越来，controller、service 层非常简洁。

参考：https://github.com/keeperLee/miaosha_1/blob/master/src/main/java/com/imooc/miaosha/exception/GlobalExceptionHandler.java
