---
title: "Java-tool-Maven-2"
date: 2019-08-03T11:01:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---

# BOM

博客参考：https://blog.csdn.net/LoveJavaYDJ/article/details/86594226

官方示例：https://github.com/spring-projects/spring-data-examples/tree/master/bom

官方示例也就是看：https://github.com/spring-projects/spring-data-examples/blob/master/bom/pom.xml

这是自己的一个示例：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.demo</groupId>
    <artifactId>spring-data-demo</artifactId>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <java.version>1.8</java.version>
        <spring.version>4.3.24.RELEASE</spring.version>
        <spring-data.version>Ingalls-SR22</spring-data.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-framework-bom</artifactId>
                <version>${spring.version}</version>
                <scope>import</scope>
                <type>pom</type>
            </dependency>
            <dependency>
                <groupId>org.springframework.data</groupId>
                <artifactId>spring-data-releasetrain</artifactId>
                <version>${spring-data.version}</version>
                <scope>import</scope>
                <type>pom</type>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>org.springframework.data</groupId>
            <artifactId>spring-data-jpa</artifactId>
        </dependency>
    </dependencies>
</project>
```

这是在一个pom中，其实 <dependencyManagement> 可以定义在父pom中，子模块中定义<dependencies>且不需要写版本。

# ${project.version}

${project.version} 表示当前项目的版本



