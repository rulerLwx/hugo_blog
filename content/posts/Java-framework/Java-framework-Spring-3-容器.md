---
title: "Java-framework-Spring-3-容器"
date: 2019-08-15T11:05:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---

# Spring容器

## Bean作用域、生命周期

### 作用域

Bean的作用域有：singleton、prototype、request、session、global session。默认是singleton。

可以通过scope属性指定作用域：

<bean id="p2" class="org.crazyit.app.service.Person" scope="prototype"/>

### 生命周期

Spring**只管理singleton作用域**的bean的生命周期。**不能管理prototype作用域**的bean。

对于 prototype 作用域的 Bean，Spring 只负责创建。每次客户端请求 prototype 域的 Bean 时，Spring 都产生一个新的实例，Spring 无从知道创建了多少个，不追踪它的生命周期。

对于 singleton 作用域的 Bean，管理生命周期的时机有二：

- 注入依赖关系之后
- 即将销毁Bean之前

注：init-method与@PostConstruct注释等同；destory-method与@PreDestroy注释等同。


#### 注入依赖关系之后

注入依赖关系之后，有两种方法管理初始化：

- 使用init-method属性（在xml中）；
- 实现InitializingBean接口，重写该接口的afterPropertiesSet()方法。


示例：

```
public interface Person{
	public void useAxe();
}
```

```
public interface Axe{
	public String chop();
}
```

```
public class Chinese implements Person,InitializingBean,BeanNameAware, ApplicationContextAware
{
	private Axe axe;
	
	public void setBeanName(String beanName){
		System.out.println("===setBeanName===");
	}
	
	public void setApplicationContext(ApplicationContext ctx){
		System.out.println("===setApplicationContext===");
	}
	
	public Chinese(){
		System.out.println("Spring实例化主调bean：Chinese实例...");
	}
	
	public void setAxe(Axe axe){
		System.out.println("Spring调用setAxe()执行依赖注入...");
		this.axe = axe;
	}
	
	public void useAxe(){
		System.out.println(axe.chop());
	}
	
	// 测试用的初始化方法
	public void init()	{
		System.out.println("正在执行初始化方法 init...");
	}
	
	// 实现InitializingBean接口必须实现的方法
	public void afterPropertiesSet() throws Exception{
		System.out.println("正在执行初始化方法 afterPropertiesSet...");
	}
}
```

```
public class SteelAxe implements Axe{

	public SteelAxe()	{
		System.out.println("Spring实例化依赖bean：SteelAxe实例...");
	}
	
	public String chop(){
		return "钢斧砍柴真快";
	}
}
```

```
<?xml version="1.0" encoding="GBK"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="http://www.springframework.org/schema/beans"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
	http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">
	
	<bean id="steelAxe" class="org.crazyit.app.service.impl.SteelAxe"/>
	
	<bean id="chinese" class="org.crazyit.app.service.impl.Chinese" init-method="init">
		<property name="axe" ref="steelAxe"/>
	</bean>
</beans>
```

```
public class BeanTest{

	public static void main(String[] args)throws Exception{
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		Person p = ctx.getBean("chinese" , Person.class);
		p.useAxe();
	}
}
```

上面的示例中的Chinese.java同时实现了InitializingBean、BeanNameAware、ApplicationContextAware接口，可以看到容器在创建Bean实例、调用setter方法执行依赖注入、执行完setBeanName()、setApplicationContext()方法之后，自动执行Bean的初始化行为（包括init-method指定的方法和afterPropertiesSet()方法）。

如果既配置了init-method，又实现了InitializingBean接口，则Spring先调用afterPropertiesSet()，再调用init-method属性指定的方法进行初始化。

通常没有必要这样做。因实现InitializingBean接口污染了代码，是侵入式的，建议使用init-method方法方式。


#### 即将销毁Bean之前

即将销毁Bean之前，有两种方法：

- 使用destroy-method属性；
- 实现DisposableBean接口，重写destroy()方法。

实现DisposableBean接口污染了代码，一般建议使用destroy-method属性方式。



问题：Spring什么时候创建Bean？



#### 所有bean生命周期？？？

对<beans.../>设置defalut-init-method属性和default-destroy-method属性，它们将使容器中的所有Bean生效。（注意：是beans元素，不是bean元素）

对应哪个类中的方法呢？？？？？？？

（这种情况下如果bean有对应的方法则会执行对应的初始化和销毁方法）

参考：https://www.cnblogs.com/moonlightpoet/p/5537284.html




## 基于XML Schema的简化配置方式

### p:命名空间

目的是为了简化设值注入（简化属性property的书写）。

1、首先要引入p:命名空间：xmlns:p="http://www.springframework.org/schema/p"。

2、对于引用类型，可以使用“p:属性名-ref”，如：p:axe-ref="stoneAxe"。

以前的写法：

```
<bean id="chinese" class="org.crazyit.app.service.impl.Chinese">
	<property name="axe" ref="stoneAxe"></property>
	<property name="age" value="29"></property>
</bean>
```

现在的写法：

```
<bean p:axe-ref="stoneAxe" p:age="29" class="org.crazyit.app.service.impl.Chinese" id="chinese"/>
```


### c:命名空间

目的是为了简化构造注入（简化属性constructor的书写）。

1、首先要引入c:命名空间：xmlns:c="http://www.springframework.org/schema/c"。

2、对于引用类型，可以使用“c:属性名-ref”，如：c:axe-ref="stoneAxe"。

以前的写法：

```
<bean id="chinese" class="org.crazyit.app.service.impl.Chinese">
	<constructor-arg ref="steelAxe" type="org.crazyit.app.service.impl.SteelAxe"/>
	<constructor-arg name="age" value="29"/>
</bean>
```

现在的写法：

```
<bean id="chinese" class="org.crazyit.app.service.impl.Chinese" c:axe-ref="steelAxe" c:age="29"/>
```


### util:命名空间

引入命名空间：

```
xmlns:util="http://www.springframework.org/schema/util"
http://www.springframework.org/schema/util/spring-util-4.0.xsd
```

util Schema提供了如下几个元素：

- constant：用于获取指定类的静态Field的值。
- property-path：用于获取指定对象的getter方法的返回值。
- list：用于定义一个List Bean，支持使用<value.../>、<ref.../>、<bean.../>等子元素来定义List集合元素。该标签支持如下三个属性：

    - id：指定一个id名。
    - list-class：使用哪个List实现类来创建Bean。默认ArrayList。
    - scope：指定该List Bean实例的作用域。

- set：用于定义一个Set Bean，支持使用<value.../>、<ref.../>、<bean.../>等子元素来定义Set集合元素。该标签支持如下三个属性：

    - id：指定一个id名。
    - list-class：使用哪个Set实现类来创建Bean。默认HashSet。
    - scope：指定实例的作用域。

- map：用于定义一个Map Bean，支持使用<entry.../>来定义Map的key-value对。该标签支持如下三个属性：

    - id：指定一个id名。
    - list-class：使用哪个Map实现类来创建Bean。默认HashMap。
    - scope：指定实例的作用域。

- property：用于加载一分资源文件，并根据加载的资源文件创建一个Properties Bean实例。该标签支持如下三个属性：

    - id：指定一个id名。
    - location：指定资源文件位置。
    - scope：指定该实例的作用域。


示例：

```
public class Chinese implements Person{
	private Axe axe;
	private int age;
	private List schools;
	private Map scores;
	private Set axes;

	// 此处省略了字段的setter方法

	// 实现Person接口的useAxe()方法
	public void useAxe(){
		System.out.println(axe.chop());
		System.out.println("age属性值：" + age);
		System.out.println(schools);
		System.out.println(scores);
		System.out.println(axes);
	}
}
```

```
<?xml version="1.0" encoding="GBK"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:p="http://www.springframework.org/schema/p"
	xmlns:util="http://www.springframework.org/schema/util"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
	http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
	http://www.springframework.org/schema/util
	http://www.springframework.org/schema/util/spring-util-4.0.xsd">
	
	<bean id="chinese" class="org.crazyit.app.service.impl.Chinese"
		p:age-ref="chin.age" p:axe-ref="stoneAxe"
		p:schools-ref="chin.schools"
		p:axes-ref="chin.axes"
		p:scores-ref="chin.scores"/>
	
	<util:constant id="chin.age" static-field="java.sql.Connection.TRANSACTION_SERIALIZABLE"/>
	
	<util:properties id="confTest" location="classpath:test_zh_CN.properties"/>
	
	<util:list id="chin.schools" list-class="java.util.LinkedList">
		<value>小学</value>
		<value>中学</value>
		<value>大学</value>
	</util:list>
	
	<util:set id="chin.axes" set-class="java.util.HashSet">
		<value>字符串</value>
		<bean class="org.crazyit.app.service.impl.SteelAxe"/>
		<ref bean="stoneAxe"/>
	</util:set>
	
	<util:map id="chin.scores" map-class="java.util.TreeMap">
		<entry key="数学" value="87"/>
		<entry key="英语" value="89"/>
		<entry key="语文" value="82"/>
	</util:map>

	<bean id="steelAxe" class="org.crazyit.app.service.impl.SteelAxe"/>

	<bean id="stoneAxe" class="org.crazyit.app.service.impl.StoneAxe"/>
</beans>
```

```
public class BeanTest{
	public static void main(String[] args){

		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		Person p = ctx.getBean("chinese" , Person.class);
		p.useAxe();
		System.out.println(ctx.getBean("confTest"));
	}
}
```