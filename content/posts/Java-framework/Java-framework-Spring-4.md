---
title: "Java-framework-Spring-4"
date: 2019-08-15T11:06:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---


# 后处理器

允许通过两个后处理器对IoC容器进行扩展。常用的后处理器有两种：

- Bean后处理器，对容器中的Bean进行后处理，对Bean进行额外加强。
- 容器后处理器，对IoC容器进行后处理，增加容器功能。


## Bean后处理器

实现BeanPostProcessor接口，BeanPostProcessor接口有两个方法：

- Object postProcessBeforeInitialization(Object bean, String beanName)，bean初始化之前被回调。
- Object postProcessAfterInitialization(Object bean, String beanName)，bean初始化之后被回调。

这两个方法，第一参数是系统即将进行后处理的bean实例，第二个参数是该bean的id。

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708091535.png)

上图是Bean后处理器两个方法的回调时机

BeanPostProcessor通常用来检查标记接口，或做如将Bean包装成一个Proxy的事情。

示例：

```
public class MyBeanPostProcessor implements BeanPostProcessor
{
	/**
	 * 对容器中的Bean实例进行后处理
	 * @param bean 需要进行后处理的原Bean实例
	 * @param beanName 需要进行后处理的Bean的配置id
	 * @return 返回后处理完成后的Bean
	 */
	public Object postProcessBeforeInitialization (Object bean , String beanName){
		System.out.println("Bean后处理器在初始化之前对"+ beanName + "进行增强处理...");
		// 返回的处理后的Bean实例，该实例就是容器中实际使用的Bean
		// 该Bean实例甚至可与原Bean截然不同
		return bean;
	}
	
	public Object postProcessAfterInitialization (Object bean , String beanName){
		System.out.println("Bean后处理器在初始化之后对"+ beanName + "进行增强处理...");
		// 如果该Bean是Chinese类的实例
		if (bean instanceof Chinese)	{
			// 修改其name成员变量
			Chinese c = (Chinese)bean;
			c.setName("疯狂iOS讲义");
		}
		return bean;
	}
}
```

```
public class Chinese implements Person,InitializingBean{
	private Axe axe;
	private String name;
	public Chinese(){
		System.out.println("Spring实例化主调bean：Chinese实例...");
	}
	public void setAxe(Axe axe){
		this.axe = axe;
	}
	public void setName(String name){
		System.out.println("Spring执行setName()方法注入依赖关系...");
		this.name = name;
	}
	public void useAxe(){
		System.out.println(name + axe.chop());
	}
	
	// 下面是两个生命周期方法
	public void init(){
		System.out.println("正在执行初始化方法 init...");
	}
	public void afterPropertiesSet() throws Exception{
		System.out.println("正在执行初始化方法 afterPropertiesSet...");
	}
}
```

```
<?xml version="1.0" encoding="GBK"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="http://www.springframework.org/schema/beans"
	xmlns:p="http://www.springframework.org/schema/p"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
	http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">

	<bean id="steelAxe" class="org.crazyit.app.service.impl.SteelAxe"/>
	<bean id="chinese" class="org.crazyit.app.service.impl.Chinese" init-method="init" p:axe-ref="steelAxe" p:name="依赖注入的值"/>

	<bean id="bp" class="org.crazyit.app.util.MyBeanPostProcessor"/>
</beans>
```

```
public class BeanTest{
	public static void main(String[] args)throws Exception{
		// 以类加载路径下的beans.xml文件来创建Spring容器
//		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
//		Person p = (Person)ctx.getBean("chinese");

		Resource isr = new ClassPathResource("beans.xml");
		DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
		// 让默认的BeanFactory容器加载isr对应的XML配置文件
		new XmlBeanDefinitionReader(beanFactory).loadBeanDefinitions(isr);
		// 获取容器中的Bean后处理器
		BeanPostProcessor bp = (BeanPostProcessor)beanFactory.getBean("bp");
		// 注册Bean后处理器
		beanFactory.addBeanPostProcessor(bp);
		Person p = (Person)beanFactory.getBean("chinese");
		p.useAxe();
	}
}
```


## 容器后处理器

实现BeanFactoryPostProcessor接口，该接口有如下方法：

void postProcessBeanFactory( ConfigurableListableBeanFactory beanFactory)，通过此方法可以对容器进行自定义扩展。

Spring提供的其它几个常用的后处理器：

- PropertyPlaceholderConfigurer：属性点位符配置器。
- PropertyOverrideConfigurer：重写占位符配置器。
- CustomAutowireConfigurer：自定义自动装配的配置器。
- CustomScopeConfigurer：自定义作用域的配置器。

主要用于配置用的？？？？


### 属性占位符配置器

PropertyPlaceholderConfigurer：属性占位符配置器。

负责读取Properties属性文件里的属性值，并将这些属性值设置成spring配置文件中数据。

```
jdbc.driverClassName=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3306/spring
jdbc.username=root
jdbc.password=32147
```

```
<?xml version="1.0" encoding="GBK"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="http://www.springframework.org/schema/beans"
	xmlns:p="http://www.springframework.org/schema/p"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
	http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">

	<bean class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
		<property name="locations">
			<list>
				<value>dbconn.properties</value>
				<!-- 如果有多个属性文件,依次在下面列出来 -->
				<!--value>wawa.properties</value-->
			</list>
		</property>
	</bean>

	<bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource"
		destroy-method="close"
		p:driverClass="${jdbc.driverClassName}"
		p:jdbcUrl="${jdbc.url}"
		p:user="${jdbc.username}"
		p:password="${jdbc.password}"/>
</beans>
```

如果引入了context:命名空间，则可采用如下方式来配置属性占位符：

<context:property-placeholder location="classpath:config.properties" />

<context:property-placeholder.../>元素是PropertyPlaceholderConfigurer的简化配置。


### 重写占位符配置器

PropertyOverrideConfigurer：重写占位符配置器。

PropertyOverrideConfigurer的功能比上面PropertyPlaceholderConfigurer的功能更加强大：PropertyOverrideConfigurer的属性文件的信息可以覆盖Spring配置文件中的无数据。

PropertyPlaceholderConfigurer的属性文件，每条属性格式如下：beanId.property=value

```
dataSource.driverClass=com.mysql.jdbc.Driver
dataSource.jdbcUrl=jdbc:mysql://localhost:3306/spring
dataSource.user=root
dataSource.password=32147
```

```
<?xml version="1.0" encoding="GBK"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="http://www.springframework.org/schema/beans"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
	http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">

	<bean class="org.springframework.beans.factory.config.PropertyOverrideConfigurer">
		<property name="locations">
			<list>
				<value>dbconn.properties</value>
				<!-- 如果有多个属性文件,依次在下面列出来 -->
			</list>
		</property>
	</bean>
	<!-- 定义数据源Bean，使用C3P0数据源实现,配置该Bean时没有指定任何信息，但Properties文件里的信息将会直接覆盖该Bean的属性值 -->
	<bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource" destroy-method="close"/>
</beans>
```

如果引入了context:命名空间，则可采用如下方式来配置属性占位符：

<context:property-override location="classpath:config.properties" />

<context:property-override.../>元素是PropertyOverrideConfigurer的简化配置。


# “零配置”支持

“零配置”，指用Annotation代替xml的配置。


## 搜索Bean

Spring提供如下几个Annotation来标注Bean：

- @Component：标注一个普通的Bean。
- @Controller：标注一个控制器组件类。
- @Service：标注一个业务逻辑组件类。
- @Repository：标注一个DAO组件类。

自动扫描的配置：

1、引入context:命名空间

```
xmlns:context="http://www.springframework.org/schema/context
http://www.springframework.org/schema/context
http://www.springframework.org/schema/context/spring-context-4.0.xsd
```

2、配置自动扫描的包路径

```
<context:component-scan base-package="org.crazyit.app.service"/>
```

可以通过为<context:component-scan.../>元素添加<include-filter.../>或<exclude-filter.../>子元素来指定是否加载或不加载某些元素。

<include-filter.../>或<exclude-filter.../>可以指定以下两个属性：

- type：指定过滤器类型。
- expression：指定过滤器所需要的表达式。

其中type可以指定如下四种过滤器：

- annotation：Annotation过滤器，指定一个用Annotation名，如<context:exclude-filter type="annotation" expression="org.springframework.stereotype.Controller"/>
- assignable：类名过滤器，指定一个java类。
- regex：正式表达式过滤器，指定一个正则表达式，如org\.example\.Default.*。
- aspectj：AspectJ过滤器，如org.example..*Service+。


示例：

```
<?xml version="1.0" encoding="GBK"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:context="http://www.springframework.org/schema/context"
	xsi:schemaLocation="http://www.springframework.org/schema/beans 
	http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
	http://www.springframework.org/schema/context
	http://www.springframework.org/schema/context/spring-context-4.0.xsd">

	<context:component-scan base-package="org.crazyit.app.service">
		<!-- 只将以Chinese、Axe结尾的类当成Spring容器中的Bean -->
		<context:include-filter type="regex" expression=".*Chinese"/>
		<context:include-filter type="regex" expression=".*Axe"/>
	</context:component-scan>
</beans>
```

## @Configuration 和 @Bean

Spring的Java配置方式是通过 @Configuration 和 @Bean 这两个注解实现的：

1、@Configuration 作用于类上，相当于一个xml配置文件；

2、@Bean 作用于方法上，相当于xml配置中的<bean>；

```
@Configuration //通过该注解来表明该类是一个Spring的配置，相当于一个xml文件
@ComponentScan(basePackages = "cn.itcast.springboot.javaconfig") //配置扫描包
public class SpringConfig {
    
    @Bean // 通过该注解来表明是一个Bean对象，相当于xml中的<bean>
    public UserDAO getUserDAO(){
        return new UserDAO(); // 直接new对象做演示
    }
    
}
```


## Bean作用域

@Scope(“prototype”)，在引号中提供bean的作用域名称即可。

Bean的作用域：singleton、prototype、request、session、global session。默认是singleton。


## Bean生命周期

@PostConstruct、@PreDestroy用于修饰方法，分别对应init-method、destory-method。


## @Resource配置依赖

@Resource有一个name属性，在默认情况下，Spring将这个值解释为需要被注入的Bean实例的id，与<property .../>元素的ref属性相同。

@Resource不仅可以修饰setter方法，也可以直接修饰实例变量。修饰实例变量时，可以连setter方法都可以不要。

@Resource可以省掉name属性，分两种情况：

1、修饰setter方法时，name属性默认为该setter方法去掉前面的set子串、首字母小写后得到的子串。如setName方法，子串为name。

2、修饰实例变量字段时，name属性默认与实例变量同名。


## @PropertySource 读取外部的资源配置文件

通过@PropertySource可以指定读取的配置文件，通过@Value注解获取值，具体用法：

```
@Configuration //通过该注解来表明该类是一个Spring的配置，相当于一个xml文件
@ComponentScan(basePackages = "cn.itcast.springboot.javaconfig") //配置扫描包
@PropertySource(value= {"classpath:jdbc.properties"})
public class SpringConfig {
    
    @Value("${jdbc.url}")
    private String jdbcUrl;
    
    @Bean // 通过该注解来表明是一个Bean对象，相当于xml中的<bean>
    public UserDAO getUserDAO(){
        return new UserDAO(); // 直接new对象做演示
    }
    
}
```

## @ImportResource

Spring提供@ImportResource来加载xml配置

```
@ImportResource({"classpath:some-context.xml","classpath:another-context.xml"})
```

## 其它注解

- @DependsOn，强制初始化其它Bean，如@DependsOn({"steelAxe","abc"})。
- @Lazy，是否取消预初始化，如@Lazy(true)。
- @Autowired，自动装配，可以修饰setter方法、普通方法、实例变量和构造器等。可以根据泛型来装配，可以装配数组、集合。默认按byType装配，可以一起使用@Qualifier("id")指定id进行装配（还不如使用@Resource呢）。（注：@Autowired功能很强大）

## @Resource与@Autowired的区别

1、@Autowired与@Resource都可以用来装配bean. 

都可以写在字段上,或写在setter方法上。其中@Autowired还可以修饰构造器、普通方法、实例变量和构造器等。可以根据泛型来装配，可以装配数组、集合。

2、@Autowired按byType自动注入（若想byName，则结合@Qualifier("id")），而@Resource默认按 byName自动注入。

3、@Resource有两个属性是比较重要的，分是name和type，Spring将@Resource注解的name属性解析为bean的名字，而type属性则解析为bean的类型。所以如果使用name属性，则使用byName的自动注入策略，而使用type属性时则使用byType自动注入策略。如果既不指定name也不指定type属性，这时将通过反射机制使用byName自动注入策略。？？

@Resource默认按名称装配，当找不到与名称匹配的bean才会按类型装配。