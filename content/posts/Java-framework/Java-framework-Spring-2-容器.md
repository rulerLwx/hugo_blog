---
title: "Java-framework-Spring-2-容器"
date: 2019-08-15T11:03:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---

# Spring容器

## 抽象Bean与子Bean

为了重用配置信息，在<bean .../>元素中增加abstract=”true”属性，就是抽象Bean，作为一个模板。spring不会实例化抽象Bean。

通过为一个<bean .../>元素指定parent属性即可指定该Bean是一个子Bean，parent=“id”属性指定该Bean所继承的父Bean的id。

子Bean无法从父Bean继承如下属性：depends-on、autowire、singleton、scope、lazy-init，这些属性将总是从子Bean定义中获得，或采用默认值。

示例：

```
<?xml version="1.0" encoding="GBK"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="http://www.springframework.org/schema/beans"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
	http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">
	
	<bean id="steelAxe" class="org.crazyit.app.service.impl.SteelAxe"/>
	
	<bean id="personTemplate" abstract="true">
		<property name="name" value="crazyit"/>
		<property name="axe" ref="steelAxe"/>
	</bean>
	
	<bean id="chinese" class="org.crazyit.app.service.impl.Chinese" parent="personTemplate"/>
	<bean id="american" class="org.crazyit.app.service.impl.American" parent="personTemplate"/>
</beans>
```

## 容器中的工厂Bean

与前面的静态工厂Bean、实例工厂Bean不同，此处的工厂Bean必须实现FactoryBean接口。

FactoryBean有如下三个方法：

- T getObject()：实现该方法负责返回该工厂Bean生成的Java实例。
- Class<?> getObjectType()：实现该方法返回该工厂Bean生成的Java实例的实现类型。
- boolean isSingleton()：实现该方法表示该工厂Bean生成的Java实例是否是单例模式。

配置FactoryBean与配置普通Bean的定义没有区别，但当程序向Spring容器请求获取该Bean时，容器返回的是FactoryBean的产品，而不是返回该FactoryBean本身。

工厂的实现类是由开发人员写，getObject()方法返回产品。

示例：

```
public class GetFieldFactoryBean implements FactoryBean<Object>
{
	private String targetClass;
	private String targetField;
	
	public void setTargetClass(String targetClass){
		this.targetClass = targetClass;
	}
	public void setTargetField(String targetField){
		this.targetField = targetField;
	}
	
	public Object getObject() throws Exception{
		Class<?> clazz = Class.forName(targetClass);
		Field field = clazz.getField(targetField);
		return field.get(null);
	}
	
	public Class<? extends Object> getObjectType(){
		return Object.class;
	}
	
	public boolean isSingleton(){
		return false;
	}
}
```

```
<?xml version="1.0" encoding="GBK"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="http://www.springframework.org/schema/beans"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
	http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">
	
	<bean id="north" class="org.crazyit.app.factory.GetFieldFactoryBean">
		<property name="targetClass" value="java.awt.BorderLayout"/>
		<property name="targetField" value="NORTH"/>
	</bean>
	
	<bean id="theValue" class="org.crazyit.app.factory.GetFieldFactoryBean">
		<property name="targetClass" value="java.sql.ResultSet"/>
		<property name="targetField" value="TYPE_SCROLL_SENSITIVE"/>
	</bean>
</beans>

```

```
public class SpringTest
{
	public static void main(String[] args)throws Exception
	{
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		
		System.out.println(ctx.getBean("north"));
		System.out.println(ctx.getBean("theValue"));
		
		System.out.println(ctx.getBean("&theValue"));
	}
}
```

## 获取Bean本身的id

实现BeanNameAware接口，重写setBeanName(String name)方法，name就是Bean的id。

## 强制初始化Bean

<bean .../>中增加depends-on=”id”属性。