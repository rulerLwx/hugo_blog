---
title: "Java-framework-Spring-Data-Redis"
date: 2019-08-17T11:11:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---

# 整合开发

依赖

```xml
    <properties>
        <spring.version>4.3.24.RELEASE</spring.version>
        <!-- redis 版本 -->
        <jedis.version>2.9.0</jedis.version>
        <spring-data-redis.version>1.8.4.RELEASE</spring-data-redis.version>
        <jackson.version>2.9.8</jackson.version>
    </properties>
    <dependencies>
        ...
        <!--redis-->
        <dependency>
            <groupId>redis.clients</groupId>
            <artifactId>jedis</artifactId>
            <version>${jedis.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.data</groupId>
            <artifactId>spring-data-redis</artifactId>
            <version>${spring-data-redis.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        ...
    </dependencies>
```


spring-redis.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/context
       https://www.springframework.org/schema/context/spring-context.xsd">

    <context:property-placeholder location="classpath:redis.properties"/>

    <!-- Jedis 连接池-->
    <bean id="poolConfig"
          class="redis.clients.jedis.JedisPoolConfig">
        <property name="maxTotal" value="${redis.pool.maxTotal}"/>
        <property name="maxIdle" value="${redis.pool.maxIdle}"/>
        <property name="minIdle" value="${redis.pool.minIdle}"/>
    </bean>
    <!-- Jedis 的连接工厂-->
    <bean id="jedisConnectionFactory"
          class="org.springframework.data.redis.connection.jedis.JedisConnectionFactory">
        <property name="hostName" value="${redis.conn.hostName}"/>
        <property name="port" value="${redis.conn.port}"/>
        <property name="poolConfig" ref="poolConfig"/>
    </bean>
    <!-- Redis 模板对象-->
    <bean id="redisTemplate"
          class="org.springframework.data.redis.core.RedisTemplate">
        <property name="connectionFactory" ref="jedisConnectionFactory"/>
        <!-- 序列化器：能够把我们储存的key 与value 做序列化处理的对象-->
        <!-- 配置默认的序列化器-->
        <!-- keySerializer、valueSerializer 配置 Redis 中的 String 类型 key与 value 的序列化器-->
        <!-- HashKeySerializer、HashValueSerializer 配置 Redis 中的 Hash型 key 与value 的序列化器-->
        <property name="keySerializer">
            <bean class="org.springframework.data.redis.serializer.StringRedisSerializer"/>
        </property>
        <property name="valueSerializer">
            <bean class="org.springframework.data.redis.serializer.StringRedisSerializer"/>
        </property>
    </bean>
</beans>
```

上面配置了默认的序列化器。

Spring Data Redis **提供了很多序列化器**，参考`org.springframework.data.redis.serializer`包。

操作 Redis 的存、取时，如果用的不是默认的序列化器，则需要根据实际情况配置 key 或 value 的序列化器。

另外可以参考笔记 Java-framework-SpringMVC-3 ，也做了一个配置，大致相同。



操作 Redis ：

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class RedisTest {
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 添加键值对
     */
    @Test
    public void test1(){
        this.redisTemplate.opsForValue().set("key", "test");
    }

    /**
     * 获取redis中的数据
     */
    @Test
    public void test2(){
        String str = (String)this.redisTemplate.opsForValue().get("key");
        System.out.println(str);
    }

    /**
     * 添加Users
     */
    @Test
    public void test3(){
        User user = new User();
        user.setAge(30);
        user.setId(1);
        user.setName("张三");
        //更换序列化器
        this.redisTemplate.setValueSerializer(new JdkSerializationRedisSerializer());
        this.redisTemplate.opsForValue().set("user", user);
    }

    /**
     * 获取Users
     *
     */
    @Test
    public void test4(){
        //更换序列化器
        this.redisTemplate.setValueSerializer(new JdkSerializationRedisSerializer());
        User user = (User)this.redisTemplate.opsForValue().get("user");
        System.out.println(user);
    }

    /**
     * 添加Users JSON格式
     */
    @Test
    public void test5(){
        User user = new User();
        user.setAge(23);
        user.setId(2);
        user.setName("李四");

        this.redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<>(User.class));
        this.redisTemplate.opsForValue().set("usersjson", user);
    }

    /**
     * 获取Uesrs JSON格式
     */
    @Test
    public void test6(){
        this.redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<>(User.class));
        User user = (User)this.redisTemplate.opsForValue().get("usersjson");
        System.out.println(user);
    }
}
```

总结：

1）存和取键值对时需要指定序列化器，**要么使用默认的，要么存取之前指定序列化器**；

2）操作字符串使用 redisTemplate.opsForValue().xxx，操作Hash使用：redisTemplate.opsForHash().xxx，....

3）开发中推荐使用 json 序列化器，因为 redis 存储时空间占用更小；

参考自己的代码：https://github.com/wolf-lea/spring-data-demo