---
title: "Java-framework-SpringCloud-5"
date: 2019-08-25T12:11:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---


# zuul-路由网关

## zuul概述

官方文档：https://github.com/Netflix/zuul/wiki/Getting-Started

Zuul包含了对请求的路由和过滤两个最主要的功能：
- 路由：负责将外部请求转发到具体的微服务实例上，是实现外部访问统一入口的基础
- 过滤：负责对请求的处理过程进行干预，是实现请求校验、服务聚合等功能的基础

Zuul和Eureka进行整合，将Zuul自身注册为Eureka服务治理下的应用，同时从Eureka中获得其他微服务的消息，也即以后的访问微服务都是通过Zuul跳转后获得。

注意：Zuul服务最终还是会注册进Eureka

提供=代理+路由+过滤三大功能


## zuul路由基本配置

1）新建模块 microservicecloud-zuul-geteway-9527

2）依赖

```xml
    <dependency>
     <groupId>org.springframework.cloud</groupId>
     <artifactId>spring-cloud-starter-eureka</artifactId>
   </dependency>
   <dependency>
     <groupId>org.springframework.cloud</groupId>
     <artifactId>spring-cloud-starter-zuul</artifactId>
   </dependency>
```

3）yml

```yml
server: 
  port: 9527
 
spring: 
  application:
    name: microservicecloud-zuul-gateway
 
eureka: 
  client: 
    service-url: 
      defaultZone: http://eureka7001.com:7001/eureka,http://eureka7002.com:7002/eureka,http://eureka7003.com:7003/eureka  
  instance:
    instance-id: gateway-9527.com
    prefer-ip-address: true 
 
info:
  app.name: atguigu-microcloud
  company.name: www.atguigu.com
  build.artifactId: $project.artifactId$
  build.version: $project.version$
```

4）修改host

```
127.0.0.1 myzuul9527.com
```

5）主启动类使用注解@EnableZuulProxy

```java
@SpringBootApplication
@EnableZuulProxy
public class Zuul_9527_StartSpringCloudApp
{
  public static void main(String[] args)
  {
   SpringApplication.run(Zuul_9527_StartSpringCloudApp.class, args);
  }
}
```

6）测试

- 启动三个eureka
- 启动一个服务提供者：microservicecloud-provider-dept-8001
- 启动一个路由：microservicecloud-zuul-geteway-9527

不用路由，访问：http://localhost:8001/dept/get/2

使用路由，访问：http://myzuul9527.com:9527/microservicecloud-dept/dept/get/2


## zuul路由规则

优化 microservicecloud-zuul-geteway-9527 模块，用别名代替实例名，application.yml添加以下配置：

```
zuul: 
  prefix: /atguigu
  ignored-services: "*"
  routes: 
    mydept.serviceId: microservicecloud-dept
    mydept.path: /mydept/**
```

解释：
- prefix：设置公共统一前缀
- ignored-services：忽略原真实服务名，忽略多个时用 *

测试：
- 通过路由访问：http://myzuul9527.com:9527/atguigu/mydept/dept/get/2
- 通过原路径访问：http://myzuul9527.com:9527/atguigu/microservicecloud-dept/dept/get/2
 ，（设置 ignored-services: "*" 后访问不到，若要看效果，需去掉）

注意：ignored-services: "*"，要写 ""


# SpringCloud config 分布式配置中心

## 概述

![](https://oscimg.oschina.net/oscnet/up-c2a8ad21b8fb7dc6abfc36ddaa92d2174c1.png)

SpringCloud Config 为微服务架构中的微服务提供集中化的外部配置支持，中心化的外部配置。

SpringCloud Config 分为客户端、服务端，服务端即是**分布式配置中心**

## SpringCloud Config 服务端与github通信

步骤：

1）在github上创建一个公共的新的仓库：microservicecloud-config

2）在本地电脑上clone新建的仓库，在克隆好的目录中新建一个application.yml，内容如下：

```
spring:
  profiles:
    active: -dev
---

spring:
  profiles: dev #开发环境
  application:
    name: microservicecloud-config-atguigu-dev

---
spring:
  profiles: test # 测试环境
  application:
    name: microservicecloud-config-atguigu-test

# 文件需要用 UTF-8 编码保存
```

注意：仓库中的**application.yml必须用UTF-8编码保存**，否则其它服务读取时将会报错

3）将新建的application.yml推送到github

```
git clone https://github.com/wolf-lea/microservicecloud-config.git
git add .
git commit -m "add" .
git push https://github.com/wolf-lea/microservicecloud-config.git 或 git push origin master
```

4）新建模块：microservicecloud-config-3344

pom.xml依赖

```
		<!-- springCloud Config -->
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-config-server</artifactId>
		</dependency>
```

application.yml

```
server:
  port: 3344

spring:
  application:
    name:  microservicecloud-config
  cloud:
    config:
      server:
        git:
          uri: https://github.com/wolf-lea/microservicecloud-config.git
```

主启动类使用注解 @EnableConfigServer

```java
@SpringBootApplication
@EnableConfigServer
public class Config_3344_StartSpringCloudApp
{
	public static void main(String[] args)
	{
		SpringApplication.run(Config_3344_StartSpringCloudApp.class, args);
	}
}
```

5）修改host

```
127.0.0.1 config-3344.com
```

6）测试

- 启动 microservicecloud-config-3344
- 访问 http://config-3344.com:3344/application-dev.yml
- 访问 http://config-3344.com:3344/application-test.yml
- 访问 http://config-3344.com:3344/application-abcdefghijklmnopqrstuvwzyx.yml

知识：配置读取规则

![](https://oscimg.oschina.net/oscnet/up-d9099bd4d0669688fb7d7f96d658244ea30.png)

7）总结

这个示例是单独的工程，不需要注册到eureka（以后的开发需要），不需要调用其它服务，只需要提供一个git地址

8）技巧

将配置文件放进文件夹，服务端根据客户端需要的配置文件名搜索配置的文件夹列表

多了 search-paths ，示例：

```yml
server:
  port: 3344

spring:
  application:
    name:  microservicecloud-config
  cloud:
    config:
      server:
        git:
          uri: https://github.com/wolf-lea/microservicecloud-config.git
          search-paths: config # 可以将配置文件放进文件夹，多个文件夹可以用逗号分隔
```

参考：https://blog.csdn.net/myth_g/article/details/90344374


## SpringCloud Config 客户端通过服务端获得github上的配置

https://www.bilibili.com/video/av22623176?p=49

步骤：

1）在本地git仓库新建 microservicecloud-config-client.yml 文件，内容如下，并提交到github

```
spring:
  profiles:
    active: -dev
---
server:
  port: 8201
spring:
  profiles: dev
  application:
    name: microservicecloud-config-client
eureka:
  client:
    service-url:
      defaultZone: http://eureka-dev.com:7001/eureka/
---
server:
  port: 8202
spring:
  profiles: test
  application:
    name: microservicecloud-config-client
eureka:
  client:
    service-url:
      defaultZone: http://eureka-test.com:7001/eureka/
```

2）新建模块 microservicecloud-config-client-3355

pom

```
        <!--config 客户端-->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-config</artifactId>
        </dependency>
```

bootstrap.yml，系统级，优先级更高

```
spring:
  cloud:
    config:
      name: microservicecloud-config-client #需要从github上读取的资源名称，注意没有yml后缀名
      profile: test   #本次访问的配置项
      label: master   
      uri: http://config-3344.com:3344  #本微服务启动后先去找3344号服务，通过SpringCloudConfig获取GitHub的服务地址
```


application.yml，用户级

```
spring:
  application:
    name: microservice-config-client
```

3）修改host

这一步不是必须的

```
127.0.0.1 config-client.com
```

4）编写rest风格 controller

```java
@RestController
public class ConfigClientRest
{

    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${eureka.client.service-url.defaultZone}")
    private String eurekaServers;

    @Value("${server.port}")
    private String port;

    @RequestMapping("/config")
    public String getConfig()
    {
        String str = "applicationName: " + applicationName + "\t eurekaServers:" + eurekaServers + "\t port: " + port;
        System.out.println("******str: " + str);
        return "applicationName: " + applicationName + "\t eurekaServers:" + eurekaServers + "\t port: " + port;
    }
}
```

总结：此处@Value的值是从中心服务器中获取，中心服务器又从github上获取

5）测试

- 启动3344服务端，自测：http://config-3344.com:3344/application-dev.yml
- 启动3355客户端，测试：

如果bootstrap.yml的profiles:dev，则访问的是：http://config-client.com:8201/config

如果bootstrap.yml的profiles:test，则访问的是：http://config-client.com:8202/config



## SpringCloud Config 配置实战示例

https://www.bilibili.com/video/av22623176?p=50

步骤：

1）本地git仓库新建如下两个yml配置文件，并提交到github

microservicecloud-config-eureka-client.yml：

```yml
spring:
  profiles:
    active:
      - dev
---
server:
  port: 7001
spring:
  profiles: dev
  application:
    name: microservicecloud-config-eureka-client
eureka:
  instance:
    hostname: eureka7001.com
  client:
    register-with-eureka: false # 自己不注册进 eureka
    fetch-registry: false # 不通过 eureka 获取注册信息
    service-url:
      defaultZone: http://eureka7001:7001/eureka/
---
server:
  port: 7001
spring:
  profiles: test
  application:
    name: microservicecloud-config-eureka-client
eureka:
  instance:
    hostname: eureka7001.com
  client:
    register-with-eureka: false
    fetch-registry: false
    service-url:
      defaultZone: http://eureka7001:7001/eureka/
```

microservicecloud-config-dept-client.yml：

```yml
spring:
  profiles:
    active:
      - dev
---
server:
  port: 8001
spring:
  profiles: dev
  application:
    name: microservicecloud-config-dept-client
  datasource:
    type: com.alibaba.druid.pool.DruidDataSource            # 当前数据源操作类型
    driver-class-name: org.gjt.mm.mysql.Driver              # mysql驱动包
    url: jdbc:mysql://localhost:3306/cloudDB01              # 数据库名称
    username: root
    password: root
    dbcp2:
      min-idle: 5                                           # 数据库连接池的最小维持连接数
      initial-size: 5                                       # 初始化连接数
      max-total: 5                                          # 最大连接数
      max-wait-millis: 200                                  # 等待连接获取的最大超时时间
mybatis:
  config-location: classpath:mybatis/mybatis.cfg.xml        # mybatis配置文件所在路径
  type-aliases-package: com.atguigu.springcloud.entity    # 所有Entity别名类所在包
  mapper-locations:
    - classpath:mybatis/mapper/**/*.xml                       # mapper映射文件

eureka:
  client:
    service-url:
      defaultZone:http://eureka7001.com:7001/eureka/
  instance:
    instance-id: dept-8001.com
    prefer-ip-address: true
---
server:
  port: 8001
spring:
  profiles: dev
  application:
    name: microservicecloud-config-dept-client
  datasource:
    type: com.alibaba.druid.pool.DruidDataSource            # 当前数据源操作类型
    driver-class-name: org.gjt.mm.mysql.Driver              # mysql驱动包
    url: jdbc:mysql://localhost:3306/cloudDB02              # 数据库名称
    username: root
    password: root
    dbcp2:
      min-idle: 5                                           # 数据库连接池的最小维持连接数
      initial-size: 5                                       # 初始化连接数
      max-total: 5                                          # 最大连接数
      max-wait-millis: 200                                  # 等待连接获取的最大超时时间
mybatis:
  config-location: classpath:mybatis/mybatis.cfg.xml        # mybatis配置文件所在路径
  type-aliases-package: com.atguigu.springcloud.entity    # 所有Entity别名类所在包
  mapper-locations:
    - classpath:mybatis/mapper/**/*.xml                       # mapper映射文件

eureka:
  client:
    service-url:
      defaultZone:http://eureka7001.com:7001/eureka/
  instance:
    instance-id: dept-8001.com
    prefer-ip-address: true
```

2）新建模块 microservicecloud-config-eureka-client-7001

pom

```xml
        <!-- SpringCloudConfig配置 -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-config</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-eureka-server</artifactId>
        </dependency>
```

bootstrap.yml

```yml
spring: 
  cloud: 
    config: 
      name: microservicecloud-config-eureka-client     #需要从github上读取的资源名称，注意没有yml后缀名
      profile: dev 
      label: master 
      uri: http://config-3344.com:3344      #SpringCloudConfig获取的服务地址


```

application.yml

```yml
spring:
  application:
    name: microservicecloud-config-eureka-client
```

主启动类：

```java
/**
 * EurekaServer服务器端启动类,接受其它微服务注册进来
 */
@SpringBootApplication
@EnableEurekaServer
public class Config_Git_EurekaServerApplication
{
	public static void main(String[] args) {
		SpringApplication.run(Config_Git_EurekaServerApplication.class, args);
	}
}
```

模块自测：
- 先启动 microservicecloud-config-3344
- 再启动 microservicecloud-config-eureka-client-7001
- 访问：http://eureka7001.com:7001

总结：
1. 这是一个注册进eureka的config客户端
2. 它的配置文件是从3344服务端获取，而3344又从git仓库获取


3）新建模块：microservicecloud-config-dept-client-8001 （参考之前的8001工程）

pom

```xml

```

bootstrap.yml

```yml
spring:
  cloud:
    config:
      name: microservicecloud-config-dept-client #需要从github上读取的资源名称，注意没有yml后缀名
      #profile配置是什么就取什么配置dev or test
      #profile: dev
      profile: test
      label: master
      uri: http://config-3344.com:3344  #SpringCloudConfig获取的服务地址
```

application.yml

```yml
spring:
  application:
    name: microservicecloud-dept
```


启动类：

```java
@SpringBootApplication
@EnableEurekaClient // 自动注册进 eureka 服务中
@EnableDiscoveryClient // 服务发现
public class DeptProvider8001_App {
    public static void main(String[] args) {
        SpringApplication.run(DeptProvider8001_App.class, args);
    }
}
```

测试
- 启动配置中心： microservicecloud-config-3344
- 启动 eureka 服务中心：microservicecloud-config-eureka-client-7001
- 启动服务提供者：microservicecloud-config-dept-client-8001
- 访问 http://localhost:8001/dept/list ，看数据库是clouddb01还是clouddb02，然后切换 服务提供者 的bootstrap.yml中的profile: test/dev，再测


# 总结回顾

架构图：

![](https://oscimg.oschina.net/oscnet/up-077ba04cfe2835406a2a9258b46e97ec38a.png)


