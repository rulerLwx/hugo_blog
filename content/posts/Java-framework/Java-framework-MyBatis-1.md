---
title: "Java-framework-MyBatis-1"
date: 2019-08-05T11:02:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---


# Mybatis架构

github https://github.com/mybatis/mybatis-3

英文文档 http://www.mybatis.org/mybatis-3/

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708080338.png)

1、mybatis配置

SqlMapConfig.xml，此文件作为mybatis的全局配置文件，配置了mybatis的运行环境等信息。

mapper.xml文件即sql映射文件，文件中配置了操作数据库的sql语句。此文件需要在SqlMapConfig.xml中加载。

2、通过mybatis环境等配置信息构造SqlSessionFactory即会话工厂

3、由会话工厂创建sqlSession即会话，操作数据库需要通过sqlSession进行。

4、mybatis底层自定义了Executor执行器接口操作数据库，Executor接口有两个实现，一个是基本执行器、一个是缓存执行器。

5、Mapped Statement也是mybatis一个底层封装对象，它包装了mybatis配置信息及sql映射信息等。mapper.xml文件中一个sql对应一个Mapped Statement对象，sql的id即是Mapped statement的id。

6、Mapped Statement对sql执行输入参数进行定义，包括HashMap、基本类型、pojo，Executor通过Mapped Statement在执行sql前将输入的java对象映射至sql中，输入参数映射就是jdbc编程中对preparedStatement设置参数。

7、Mapped Statement对sql执行输出结果进行定义，包括HashMap、基本类型、pojo，Executor通过Mapped Statement在执行sql后将输出结果映射至java对象中，输出结果映射过程相当于jdbc编程中对结果的解析处理过程。

# 知识点

## mysql自增主键返回

```
    <!-- 添加用户 -->
    <insert id="insertUser" parameterType="cn.itcast.mybatis.po.User">
		<!-- selectKey将主键返回，需要再返回 -->
		<selectKey keyProperty="id" order="AFTER" resultType="java.lang.Integer">
			select LAST_INSERT_ID()
		</selectKey>
	   insert into user(username,birthday,sex,address)values(#{username},#{birthday},#{sex},#{address})
	</insert>	
```

说明：

- 添加selectKey实现将主键返回
- keyProperty：返回的主键存储在pojo中的哪个属性
- order：selectKey的执行顺序，是相对与insert语句来说，由于mysql的自增原理执行完insert语句之后才将主键生成，所以这里selectKey的执行顺序为after
- resultType：返回的主键是什么类型
- LAST_INSERT_ID()：是mysql的函数，返回auto_increment自增列新记录id值
- 这里没有用到`useGeneratedKeys="true" keyColumn="id"`，有人说用到，参考：https://www.cnblogs.com/quan-coder/p/8728410.html


后台通过`getId`获取自增键

```
    public Integer insertUser(User user) {
        userDao.insertUser(user);
        return user.getId();
    }
```



## Oracle使用序列生成主键

首先自定义一个序列且用于生成主键，selectKey使用如下：

```
<insert  id="insertUser" parameterType="cn.itcast.mybatis.po.User">
    <selectKey resultType="java.lang.Integer" order="BEFORE" keyProperty="id">
        SELECT 自定义序列.NEXTVAL FROM DUAL
    </selectKey>
    insert into user(id,username,birthday,sex,address) 
    		 values(#{id},#{username},#{birthday},#{sex},#{address})
</insert>
```

注意这里使用的order是“BEFORE”

如何返回主键？TODO...参考：https://blog.csdn.net/lcw785376995/article/details/82497048



## Mysql使用 uuid实现主键

```
<insert  id="insertUser" parameterType="cn.itcast.mybatis.po.User">
    <selectKey resultType="java.lang.String" order="BEFORE" keyProperty="id">
        select uuid()
    </selectKey>
    insert into user(id,username,birthday,sex,address)values(#{id},#{username},#{birthday},#{sex},#{address})
</insert>
```

注意这里使用的order是“BEFORE”


# SqlMapConfig.xml配置文件

SqlMapConfig.xml中配置的内容和顺序如下：

```
properties（属性）
settings（全局配置参数）
typeAliases（类型别名）
typeHandlers（类型处理器）
objectFactory（对象工厂）
plugins（插件）
environments（环境集合属性对象）
    environment（环境子属性对象）
        transactionManager（事务管理）
        dataSource（数据源）
mappers（映射器）
```


## properties（属性）

SqlMapConfig.xml可以引用java属性文件中的配置信息如下：

在classpath下定义db.properties文件，

```
jdbc.driver=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3306/mybatis
jdbc.username=root
jdbc.password=mysql
```

SqlMapConfig.xml引用如下：

```
<properties resource="db.properties"/>
	<environments default="development">
		<environment id="development">
			<transactionManager type="JDBC"/>
			<dataSource type="POOLED">
				<property name="driver" value="${jdbc.driver}"/>
				<property name="url" value="${jdbc.url}"/>
				<property name="username" value="${jdbc.username}"/>
				<property name="password" value="${jdbc.password}"/>
			</dataSource>
		</environment>
	</environments>
```

注意： MyBatis 将按照下面的顺序来加载属性：

- 在 properties 元素体内定义的属性首先被读取。 
- 然后会读取properties 元素中resource或 url 加载的属性，它会覆盖已读取的同名属性。 
- 最后读取parameterType传递的属性，它会覆盖已读取的同名属性。

因此，通过parameterType传递的属性具有最高优先级，resource或 url 加载的属性次之，最低优先级的是 properties 元素体内定义的属性。


## settings（配置）

mybatis全局配置参数，全局参数将会影响mybatis的运行行为。

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708080421.png)
![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708080443.png)

可以去官网参考：http://www.mybatis.org/mybatis-3/zh/configuration.html


## typeAliases（类型别名）

mybatis支持别名：

别名 | 映射的类型
---|---
_byte  | byte 
_long  | long 
_short  | short 
_int  | int 
_integer | int 
_double  | double 
_float  | float 
_boolean  | boolean 
string  | String 
byte  | Byte 
long  | Long 
short  | Short 
int  | Integer 
integer  | Integer 
double  | Double 
float  | Float 
boolean  | Boolean 
date  | Date 
decimal  | 	BigDecimal 
bigdecimal | BigDecimal 

自定义别名：

在SqlMapConfig.xml中配置：

```
<typeAliases>
	<!-- 单个别名定义 -->
	<typeAlias alias="user" type="cn.itcast.mybatis.po.User"/>
	<!-- 批量别名定义，扫描整个包下的类 -->
	<package name="domain.blog"/>
	<package name="其它包"/>
</typeAliases>
```

每一个在包 domain.blog 中的 Java Bean，在没有注解的情况下，会使用 Bean 的首字母小写的非限定类名来作为它的别名。 比如 domain.blog.Author 的别名为 author；若有注解，则别名为其注解值。见下面的例子：

```
@Alias("author")
public class Author {
    ...
}
```


## typeHandlers（类型处理器）

类型处理器用于java类型和jdbc类型映射，如下：

```
<select id="findUserById" parameterType="int" resultType="user">
		select * from user where id = #{id}
</select>
```

mybatis自带的类型处理器基本上满足日常需求，不需要单独定义。

mybatis支持类型处理器：

类型处理器 | 	Java类型 | 	JDBC类型
---|---|---
BooleanTypeHandler |  	Boolean，boolean  | 	任何兼容的布尔值
ByteTypeHandler  | 	Byte，byte  | 	任何兼容的数字或字节类型
ShortTypeHandler  | 	Short，short  | 	任何兼容的数字或短整型
IntegerTypeHandler  | 	Integer，int  | 	任何兼容的数字和整型
LongTypeHandler  | 	Long，long  | 	任何兼容的数字或长整型
FloatTypeHandler  | 	Float，float  | 	任何兼容的数字或单精度浮点型
DoubleTypeHandler  | 	Double，double 	 | 任何兼容的数字或双精度浮点型
BigDecimalTypeHandler  | 	BigDecimal 	 | 任何兼容的数字或十进制小数类型
StringTypeHandler  | 	String  | 	CHAR和VARCHAR类型
ClobTypeHandler  | 	String  | 	CLOB和LONGVARCHAR类型
NStringTypeHandler  | 	String  | 	NVARCHAR和NCHAR类型
NClobTypeHandler  | 	String  | 	NCLOB类型
ByteArrayTypeHandler  | 	byte[] 	 | 任何兼容的字节流类型
BlobTypeHandler  | 	byte[]  | 	BLOB和LONGVARBINARY类型
DateTypeHandler  | 	Date（java.util） | 	TIMESTAMP类型
DateOnlyTypeHandler  | 	Date（java.util） | 	DATE类型
TimeOnlyTypeHandler  | 	Date（java.util） | 	TIME类型
SqlTimestampTypeHandler  | 	Timestamp（java.sql） | 	TIMESTAMP类型
SqlDateTypeHandler  | 	Date（java.sql） | 	DATE类型
SqlTimeTypeHandler  | 	Time（java.sql） | 	TIME类型
ObjectTypeHandler  | 	任意 | 	其他或未指定类型
EnumTypeHandler  | 	Enumeration类型 | 	VARCHAR-任何兼容的字符串类型，作为代码存储（而不是索引）。


## mappers（映射器）

Mapper配置的几种方法：

- `<mapper resource=" " />`，相对于类路径的资源，如：`<mapper resource="sqlmap/User.xml" />`
- `<mapper url=" " />`，使用完全限定路径，如：`<mapper url="file:///D:\workspace_spingmvc\mybatis_01\config\sqlmap\User.xml" />`
- `<mapper class=" " />`，使用mapper接口类路径，如：`<mapper class="cn.itcast.mybatis.mapper.UserMapper"/>`，注意：此种方法要求mapper接口名称和mapper映射文件名称相同，且放在同一个目录中
- `<package name=""/>`，注册指定包下的所有mapper接口，如：`<package name="cn.itcast.mybatis.mapper"/>`，注意：此种方法要求mapper接口名称和mapper映射文件名称相同，且放在同一个目录中




# Mapper.xml映射文件

Mapper.xml映射文件中定义了操作数据库的sql，每个sql是一个statement，映射文件是mybatis的核心。


## parameterType(输入类型)

### #{}与${}

#{}实现的是向prepareStatement中的预处理语句中设置参数值，sql语句中#{}表示一个占位符，即?

使用占位符#{}可以有效防止sql注入

${}是进行字符串的接连




### 传递pojo对象

```
	<select id="findUserByUser" parameterType="user" resultType="user">
	   select * from user where id=#{id} and username like '%${username}%'
	</select>
```

### 传递pojo包装对象

定义包装对象

```
public class QueryVo {
	
	private User user;
	
	//自定义用户扩展类
	private UserCustom userCustom;
```

mapper.xml映射文件

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708080541.png)

说明：mybatis底层通过ognl从pojo中获取属性值：#{user.username}，user即是传入的包装对象的属性。queryVo是别名，即上边定义的包装对象类型


### 传递hashmap

```
    <!-- 传递hashmap综合查询用户信息 -->
	<select id="findUserByHashmap" parameterType="hashmap" resultType="user">
	   select * from user where id=#{id} and username like '%${username}%'
	</select>
```

测试：

```
Public void testFindUserByHashmap()throws Exception{
		//获取session
		SqlSession session = sqlSessionFactory.openSession();
		//获限mapper接口实例
		UserMapper userMapper = session.getMapper(UserMapper.class);
		//构造查询条件Hashmap对象
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("id", 1);
		map.put("username", "管理员");
		
		//传递Hashmap对象查询用户列表
		List<User>list = userMapper.findUserByHashmap(map);
		//关闭session
		session.close();
	}
```



## resultType(输出类型)

### 输出pojo对象

```
	<!-- 根据id查询用户信息 -->
	<select id="findUserById" parameterType="int" resultType="user">
		select * from user where id = #{id}
	</select>
```

```
public User findUserById(int id) throws Exception;
```

### 输出pojo列表

```
    <!-- 根据名称模糊查询用户信息 -->
	<select id="findUserByUsername" parameterType="string" resultType="user">
	   select * from user where username like '%${value}%'
	</select>
```

```
public List<User> findUserByUsername(String username) throws Exception;
```

### 输出hashmap

输出pojo对象可以改用hashmap输出类型，将输出的字段名称作为map的key，value为字段值。



## resultMap（输出类型）

resultType可以指定pojo将查询结果映射为pojo，但需要pojo的属性名和sql查询的列名一致方可映射成功。

如果sql查询字段名和pojo的属性名不一致，可以通过resultMap将字段名和属性名作一个对应关系 ，resultMap实质上还需要将查询结果映射到pojo对象中。

resultMap可以实现将查询结果映射为复杂类型的pojo，比如在查询结果映射对象中包括pojo和list实现一对一查询和一对多查询。

1、Mapper定义

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708080615.png)

使用resultMap指定上边定义的personmap。

2、定义resultMap

由于上边的mapper.xml中sql查询列和Users.java类属性不一致，需要定义resultMap：userListResultMap将sql查询列和Users.java类属性对应起来

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708080704.png)

- <id />：此属性表示查询结果集的唯一标识，非常重要。如果是多个字段为复合唯一约束则定义多个<id />。
- Property：表示person类的属性。
- Column：表示sql查询出来的字段名。
- Column和property放在一块儿表示将sql查询出来的字段映射到指定的pojo类属性上。
- <result />：普通结果，即pojo的属性。

3、Mapper接口定义

```
public List<User> findUserListResultMap() throws Exception;
```




## 动态sql(重点)

通过mybatis提供的各种标签方法实现动态拼接sql。

### If

```
	<select id="findUserList" parameterType="user" resultType="user">
		select * from user 
		where 1=1 
		<if test="id!=null and id!=''">
		and id=#{id}
		</if>
		<if test="username!=null and username!=''">
		and username like '%${username}%'
		</if>
	</select>
```

### Where

上边的sql也可以改为：

```
    <select id="findUserList" parameterType="user" resultType="user">
		select * from user 
		<where>
		<if test="id!=null and id!=''">
		and id=#{id}
		</if>
		<if test="username!=null and username!=''">
		and username like '%${username}%'
		</if>
		</where>
	</select>
```

<where />可以自动处理第一个and


### foreach

向sql传递数组或List，mybatis使用foreach解析

#### 通过pojo传递list

- 需求

    传入多个id查询用户信息，用下边两个sql实现：

```
    SELECT * FROM USERS WHERE username LIKE '%张%' AND (id =10 OR id =89 OR id=16)
    SELECT * FROM USERS WHERE username LIKE '%张%' and id IN (10,89,16)
```

- 在pojo中定义list属性ids存储多个用户id，并添加getter/setter方法

    ![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708080728.png)

- mapper.xml

```
    <if test="ids!=null and ids.size > 0">
    	<foreach collection="ids" open=" and id in(" close=")" item="id" separator="," >
    		#{id}
    	</foreach>
    </if>
```

- 测试

```
    List<Integer> ids = new ArrayList<Integer>();
    ids.add(1);//查询id为1的用户
    ids.add(10); //查询id为10的用户
    queryVo.setIds(ids);
    List<User> list = userMapper.findUserList(queryVo);
```

#### 传递单个List

传递List类型在编写mapper.xml没有区别，唯一不同的是只有一个List参数时它的参数名为list。

- Mapper.xml

```
    <select id="selectUserByList" parameterType="java.util.List" resultType="user">
		select * from user 
		<where>
		<!-- 传递List，List中是pojo -->
		<if test="list!=null">
		<foreach collection="list" item="item" open="and id in("separator=","close=")">
		    #{item.id} 
		</foreach>
		</if>
		</where>
	</select>
```

- Mapper接口

```
    public List<User> selectUserByList(List userlist) throws Exception;
```

- 测试

```
    Public void testselectUserByList()throws Exception{
    		//获取session
    		SqlSession session = sqlSessionFactory.openSession();
    		//获限mapper接口实例
    		UserMapper userMapper = session.getMapper(UserMapper.class);
    		//构造查询条件List
    		List<User> userlist = new ArrayList<User>();
    		User user = new User();
    		user.setId(1);
    		userlist.add(user);
    		user = new User();
    		user.setId(2);
    		userlist.add(user); 
    		//传递userlist列表查询用户列表
    		List<User>list = userMapper.selectUserByList(userlist);
    		//关闭session
    		session.close();
    }
```

#### 传递单个数组（数组中是pojo）

- Mapper.xml

```
	<select id="selectUserByArray" parameterType="Object[]" resultType="user">
		select * from user 
		<where>
		<!-- 传递数组 -->
		<if test="array!=null">
		<foreach collection="array" index="index" item="item" open="and id in("separator=","close=")">
		    #{item.id} 
		</foreach>
		</if>
		</where>
	</select>
```

sql只接收一个数组参数，这时sql解析参数的名称mybatis固定为array，

如果数组是通过一个pojo传递到sql则参数的名称为pojo中的属性名。

    - index：为数组的下标。
    - item：为数组每个元素的名称，名称随意定义
    - open：循环开始
    - close：循环结束
    - separator：中间分隔输出


- Mapper接口

```
public List<User> selectUserByArray(Object[] userlist) throws Exception;
```

- 测试

```
    Public void testselectUserByArray()throws Exception{
		//获取session
		SqlSession session = sqlSessionFactory.openSession();
		//获限mapper接口实例
		UserMapper userMapper = session.getMapper(UserMapper.class);
		//构造查询条件List
		Object[] userlist = new Object[2];
		User user = new User();
		user.setId(1);
		userlist[0]=user;
		user = new User();
		user.setId(2);
		userlist[1]=user;
		//传递user对象查询用户列表
		List<User>list = userMapper.selectUserByArray(userlist);
		//关闭session
		session.close();
	}
```


#### 传递单个数组（数组中是字符串类型）

- Mapper.xml

```
	<select id="selectUserByArray" parameterType="Object[]" resultType="user">
		select * from user 
		<where>
		<!-- 传递数组 -->
		<if test="array!=null">
		<foreach collection="array"index="index"item="item"open="and id in("separator=","close=")">
		    #{item} 
		</foreach>
		</if>
		</where>
	</select>
```

如果数组中是简单类型则写为#{item}，不用再通过ognl获取对象属性值了。


- Mapper接口

```
    public List<User> selectUserByArray(Object[] userlist) throws Exception;
```

- 测试：

```
    Public void testselectUserByArray()throws Exception{
		//获取session
		SqlSession session = sqlSessionFactory.openSession();
		//获限mapper接口实例
		UserMapper userMapper = session.getMapper(UserMapper.class);
		//构造查询条件List
		Object[] userlist = new Object[2];
		userlist[0]=”1”;
		userlist[1]=”2”;
		//传递user对象查询用户列表
		List<User>list = userMapper.selectUserByArray(userlist);
		//关闭session
		session.close();
	}
```


### Sql片段

Sql中可将重复的sql提取出来，使用时用include引用即可，最终达到sql重用的目的，如下：

```
	<select id="findUserList" parameterType="user" resultType="user">
		select * from user 
		<where>
		<if test="id!=null and id!=''">
		and id=#{id}
		</if>
		<if test="username!=null and username!=''">
		and username like '%${username}%'
		</if>
		</where>
	</select>
```

将where条件抽取出来：

```
    <sql id="query_user_where">
    	<if test="id!=null and id!=''">
    		and id=#{id}
    	</if>
    	<if test="username!=null and username!=''">
    		and username like '%${username}%'
    	</if>
    </sql>
```

使用include引用：

```
    <select id="findUserList" parameterType="user" resultType="user">
		select * from user 
		<where>
		    <include refid="query_user_where"/>
		</where>
	</select>
```

注意：如果引用其它mapper.xml的sql片段，则在引用时需要加上namespace，如下：

```
<include refid="namespace.sql片段”/>
```