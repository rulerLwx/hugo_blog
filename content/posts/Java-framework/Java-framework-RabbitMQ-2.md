---
title: "Java-framework-RabbitMQ-2"
date: 2019-08-09T11:03:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---


# 使用RabbitMQ实现松耦合设计

## 需求

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708084225.png)

## 代码实现

只需要在上一个项目的基础上，在consumer方面，增加一个红包服务的队列就行，

这样其它的代码都不用动，让这个队列订阅消息就行，这就是解耦。

# RabbitMQ 消息处理

## 消息持久化

1）创建两个项目

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708084316.png)

2）autoDelete

```java
@RabbitListener(
        bindings =@QueueBinding(
                value = @Queue(value = "${mq.config.queue.error}",autoDelete ="true"),//配置队列名称、是否是一个可删除的临时队列
                exchange = @Exchange(value = "${mq.config.exchange}",type = ExchangeTypes.DIRECT),//配置交换器名称、指定交换器类型
                key = "${mq.config.queue.error.routing.key}"
        )
)
```

有两处可以设置是否自动删除：
- @Queue#autoDelete：当**所有消费客户**端断开连接后，是否自动删除队列。true：删除，false：不删除。
- @Exchange#autoDelete：当**所有绑定队列**都不使用时，是否自动删除交换器。true：删除，false：不删除。

将 autoDelete 设置成 false 即可。

> 疑问

问：如果此时rabbitmq所在的服务器挂了，如何保证数据安全？

答：可以在调用 rabbitmq 的代码中做判断，不成功则给用户反馈服务器出问题了（我的总结）。todo... ——20191012


## 消息确认ACK 机制

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708084340.png)

为了不出现致命的“内存泄漏”，添加以下配置：

```
#开启重试
spring.rabbitmq.listener.retry.enabled=true
#重试次数，默认为5次
spring.rabbitmq.listener.retry.max-attempts=5
```

# 参考书籍

## 《RabbitMQ实战指南》

RabbitMQ实战指南.pdf