---
title: "Java-framework-Spring-6-AOP"
date: 2019-08-15T11:08:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---

# AOP

AOP(Aspect Orient Programming)，面向切面编程。

我的理解：把公共的方法抽取出来，大家共用。

Spring默认使用JDK动态代理来创建AOP代理，这样就可以为任何接口实例创建代理了。也可以使用cglib代理，在需要代理类而不是代理接口的时候，spring会自动切换到cglib代理。

JDK动态代理，被代理的类必须要实现有接口。cglib则不用。

Spring的AOP有两种实现方式：

- XML配置方式
- Annotation注解方式



## 基本概念

- 切面（Aspect）：用于组织多个Advice。可以理解为我们自定义的切面类。
- 连接点（Joinpoint）：可以理解为一串表达式，如`"execution(* org.crazyit.app.servie.impl.*.*(..))"`。
- 增强处理（Advice）：在特定的切入点执行的增强处理。如“before”、“after”。
- 切入点（Pointcut）：可以插入增强处理的连接点。当被增强处理时，连接点就变成切入点。

可以理解为：连接点是一种抽象描述，切入点是一个具体描述。

即：连接点是一串表达式，其中可以匹配很多方法；切入点，就是被增强时，只匹配一个方法。




## 注解方式实现AOP

JDK1.5才提供的注解，所以，低于1.5的就不能使用注解方式。

### 基本使用步骤

1、添加jar包

```
<dependency>
	<groupId>org.springframework</groupId>
	<artifactId>spring-aop</artifactId>
	<version>${spring.version}</version>
</dependency>
<dependency>
	<groupId>org.aspectj</groupId>
	<artifactId>aspectjrt</artifactId>
	<version>${aspectj.version}</version>
</dependency>
<dependency>
	<groupId>org.aspectj</groupId>
	<artifactId>aspectjweaver</artifactId>
	<version>${aspectj.version}</version>
</dependency>
```

2、定义切面，并在切面里写好切入点、增强处理等等

```
// 定义一个切面
@Aspect
public class AuthAspect{
	// 匹配org.crazyit.app.service.impl包下所有类的、
	// 所有方法的执行作为切入点
	@Before("execution(* org.crazyit.app.service.impl.*.*(..))")
	public void authority(){
		System.out.println("模拟执行权限检查");
	}
}
```

3、配置applicationContext.xml

```
<?xml version="1.0" encoding="GBK"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:context="http://www.springframework.org/schema/context"
	xmlns:aop="http://www.springframework.org/schema/aop"
	xsi:schemaLocation="http://www.springframework.org/schema/beans 
	http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
	http://www.springframework.org/schema/context
	http://www.springframework.org/schema/context/spring-context-4.0.xsd
	http://www.springframework.org/schema/aop
	http://www.springframework.org/schema/aop/spring-aop-4.0.xsd">
	<!-- 指定自动搜索Bean组件、自动搜索切面类 -->
	<context:component-scan base-package="org.crazyit.app.service,org.crazyit.app.aspect">
		<context:include-filter type="annotation" expression="org.aspectj.lang.annotation.Aspect"/>
	</context:component-scan>
	
	<!-- 启动@AspectJ支持 -->
	<aop:aspectj-autoproxy/>
</beans>

```

在配置文件中做两个配置：

1）开启aspectj注解功能：`<aop:aspectj-autoproxy/>`

2）自动扫描业务bean和切面（可以扫描多个包，用逗号隔开）：`<context:component-scan base-package="org.crazyit.app.service, org.crazyit.app.aspect">`，默认并不会加载Aspect注解，因此，要include这个Aspect注解。


4、测试

```
public class BeanTest{
	public static void main(String[] args){
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		Hello hello = ctx.getBean("hello" , Hello.class);
		hello.foo();
		hello.addUser("孙悟空" , "7788");
		World world = ctx.getBean("world" , World.class);
		world.bar();

	}
}
```

注意：要用接口来接收代理对象（面向接口编程）。





### 各种增强处理的配置

先要清楚各种通知的执行顺序或时机，如下：

```
	//此处：前置通知	@Before
	
	public String getPersonName(){
		try {
			
			//此处执行业务方法
			
		} catch (Exception e) {
			
			//此处：异常通知	@AfterThrowing
			
			e.printStackTrace();
		}
		return null;
		
		//此处：返回通知	@AfterReturning
	}
	
	//此处：后置通知	@After
```


#### Before增强处理

`@Before("execution(* org.crazyit.app.service.impl.*.*(..))")`

使用@Before修饰时，通常需要指定一个value属性值，该属性值指定一个切入点表达式（既可以是一个已有的切入点，也可以直接定义切入点表达式），用于指定该增强处理将被织入哪些切入点。

如果是已有的切入点，则写法如下：

```
@Aspect
public class MyAspect {
	@Pointcut("execution(* com.demo.service.impl.*.*(..))")
	private void myPointCut(){}
	
	@Before("myPointCut()")
	public void authority(){
		System.out.println("权限检查");
	}

}
```


#### AfterReturning增强处理

@AfterReturning注解可指定如下两个常用属性：

- pointcut/value：这两个属性的作用是一样的，都用于指定该切入点对应的切入表达式（既可以是一个已有的切入点，也可以直接定义切入点表达式）。当指定了pointcut值后，vlaue值将被覆盖。
- returning：该属性指定一个形参名，用于表示Advice方法中可定义与此同名的形参，该形参可用于访问目标方法（业务bean中定义的方法）的返回值。除此之外，在Adice方法中定义该形参（代表上档方法的返回值）时指定的类型，会限制目标方法必须返回指定类型的值或没有返回值。

示例：

```
public interface Hello{
	void foo();
	int addUser(String name , String pass);
}
```

```
public interface World{
	public void bar();
}
```

```
@Component("hello")
public class HelloImpl implements Hello{
	public void foo(){
		System.out.println("执行Hello组件的foo()方法");
	}
	public int addUser(String name , String pass){
		System.out.println("执行Hello组件的addUser添加用户：" + name);
		if(name.length() < 3 || name.length() > 10){
			throw new IllegalArgumentException("name参数的长度必须大于3，小于10！");
		}
		return 20;
	}
}
```

```
@Component("world")
public class WorldImpl implements World{
	public void bar()	{
		System.out.println("执行World组件的bar()方法");
	}
}
```

```
// 定义一个切面
@Aspect
public class LogAspect{
	// 声明rvt时指定的类型会限制目标方法必须返回指定类型的值或没有返回值
	// 此处将rvt的类型声明为Object，意味着对目标方法的返回值不加限制
	@AfterReturning(returning="rvt", pointcut="execution(* org.crazyit.app.service.impl.*.*(..))")
	public void log(Object rvt){
		System.out.println("获取目标方法返回值:" + rvt);
		System.out.println("模拟记录日志功能...");
	}
}
```

```
<?xml version="1.0" encoding="GBK"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:context="http://www.springframework.org/schema/context"
	xmlns:aop="http://www.springframework.org/schema/aop"
	xsi:schemaLocation="http://www.springframework.org/schema/beans 
	http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
	http://www.springframework.org/schema/context
	http://www.springframework.org/schema/context/spring-context-4.0.xsd
	http://www.springframework.org/schema/aop
	http://www.springframework.org/schema/aop/spring-aop-4.0.xsd">

	<context:component-scan base-package="org.crazyit.app.service,org.crazyit.app.aspect">
		<context:include-filter type="annotation" expression="org.aspectj.lang.annotation.Aspect"/>
	</context:component-scan>
	
	<aop:aspectj-autoproxy/>
</beans>
```

```
public class BeanTest{
	public static void main(String[] args){
		// 创建Spring容器
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		Hello hello = ctx.getBean("hello" , Hello.class);
		hello.foo();
		hello.addUser("悟空" , "7788");
		World world = ctx.getBean("world" , World.class);
		world.bar();
	}
}
```


#### AfterThrowing增强处理

@AfterThrowing主要用于处理程序中未处理的异常。可指定如下两个注解：

- pointcut/value：同上。
- throwing：该属性指定一个形参名，用于表示Advice方法中可定义与此同名的形参，该形参可用于访问目标方法抛出的异常。除此外，会限制目标方法必须抛出指定类型的异常。

```
@Aspect
public class RepairAspect{
	@AfterThrowing(throwing="ex", pointcut="execution(* org.crazyit.app.service.impl.*.*(..))")
	// 声明ex时指定的类型会限制目标方法必须抛出指定类型的异常
	// 此处将ex的类型声明为Throwable，意味着对目标方法抛出的异常不加限制
	public void doRecoveryActions(Throwable ex){
		System.out.println("目标方法中抛出的异常:" + ex);
		System.out.println("模拟Advice对异常的修复...");
	}
}
```


#### After增强处理

@After跟@Before对应，是必然会执行的（如果定义了此增强处理），不论一个方法是如何结束的（正常或异常）。

@After注解需要指定一个value属性，表示一个切入点（既可以是一个已有的切入点，也可以直接定义切入点表达式）。

```
@Aspect
public class ReleaseAspect{
	@After("execution(* org.crazyit.app.service.*.*(..))")
	public void release(){
		System.out.println("模拟方法结束后的释放资源...");
	}
}
```


#### Around增强处理

@Around注解需要指定一个vlue属性，表示一个切入点。

@Around注解修饰的增强方法的第一个形参必须是ProceedingJoinPoint（至少包含一个形参），在Advice方法体内，调用ProceedingJoinPoint形参的processed()方法后才会执行目标方法。如下：

```
	@Around("myPointCut()")
	public Object doBasicProfiling(ProceedingJoinPoint pjp) throws Throwable {
		
//		return pjp.proceed();
		
		return null;
		
	}
```

环绕通知相当于自定义通知。格式是固定的：Object ，ProceedingJoinPoint 。

特点：

1、如果 return null;则不会执行之后的方法；

2、如果 return pjp.proceed();则会执行之后的方法

3、我们可以通过pjp对象来判断让不让它执行之后的方法，以达到权限控制。

调用ProceedingJoinPoint形参的processed()方法时，还可以传一个Object[]对象作为参数，该数组中的值将被传入目标方法作为执行方法的实参。

```
@Aspect
public class TxAspect{
	@Around("execution(* org.crazyit.app.service.impl.*.*(..))")
	public Object processTx(ProceedingJoinPoint jp) throws java.lang.Throwable {
	
		System.out.println("执行目标方法之前，模拟开始事务...");
		
		// 获取目标方法原始的调用参数
		Object[] args = jp.getArgs();
		if(args != null && args.length > 1){
			// 修改目标方法的第一个参数
			args[0] = "【增加的前缀】" + args[0];
		}
		
		// 以改变后的参数去执行目标方法，并保存目标方法执行后的返回值
		Object rvt = jp.proceed(args);
		System.out.println("执行目标方法之后，模拟结束事务...");
		
		// 如果rvt的类型是Integer，将rvt改为它的平方
		if(rvt != null && rvt instanceof Integer)
			rvt = (Integer)rvt * (Integer)rvt;
		return rvt;
	}
}
```


### 其它知识点

#### 访问目标方法的参数

访问目标方法最简单的做法是定义增强处理方法时将第一个参数定义为JoinPoint类型。当增强处理方法被调用时，该JoinPoint参数就代表了织入增强处理的连接点。

JoinPoint里包含如下几个常用的方法：

- Object[] getArgs()：返回执行目标方法时的参数。
- Signature getSignature()：返回被增加方法的相关信息。
- Object getTarget()：返回被织入增强处理的目标对象。
- Object getThis()：返回AOP框架为目标对象生成的代理对象。

注：@Around注解修饰的增强方法的第一个形参ProceedingJoinPoint是JoinPoint的子类。


示例：

```
@Aspect
public class FourAdviceTest{

	@Around("execution(* org.crazyit.app.service.impl.*.*(..))")
	public Object processTx(ProceedingJoinPoint jp) throws java.lang.Throwable {
		System.out.println("Around增强：执行目标方法之前，模拟开始事务...");
		// 访问执行目标方法的参数
		Object[] args = jp.getArgs();
		// 当执行目标方法的参数存在，
		// 且第一个参数是字符串参数
		if (args != null && args.length > 0
			&& args[0].getClass() == String.class)
		{
			// 修改目标方法调用参数的第一个参数
			args[0] = "【增加的前缀】" + args[0];
		}
		//执行目标方法，并保存目标方法执行后的返回值
		Object rvt = jp.proceed(args);
		System.out.println("Around增强：执行目标方法之后，模拟结束事务...");
		// 如果rvt的类型是Integer，将rvt改为它的平方
		if(rvt != null && rvt instanceof Integer)
			rvt = (Integer)rvt * (Integer)rvt;
		return rvt;
	}
	
	// 定义Before增强处理
	@Before("execution(* org.crazyit.app.service.impl.*.*(..))")
	public void authority(JoinPoint jp) {
		System.out.println("Before增强：模拟执行权限检查");
		// 返回被织入增强处理的目标方法
		System.out.println("Before增强：被织入增强处理的目标方法为：" + jp.getSignature().getName());
		// 访问执行目标方法的参数
		System.out.println("Before增强：目标方法的参数为：" + Arrays.toString(jp.getArgs()));
		// 访问被增强处理的目标对象
		System.out.println("Before增强：被织入增强处理的目标对象为：" + jp.getTarget());
	}
	
	//定义AfterReturning增强处理
	@AfterReturning(pointcut="execution(* org.crazyit.app.service.impl.*.*(..))", returning="rvt")
	public void log(JoinPoint jp , Object rvt){
		System.out.println("AfterReturning增强：获取目标方法返回值:"+ rvt);
		System.out.println("AfterReturning增强：模拟记录日志功能...");
		// 返回被织入增强处理的目标方法
		System.out.println("AfterReturning增强：被织入增强处理的目标方法为："+ jp.getSignature().getName());
		// 访问执行目标方法的参数
		System.out.println("AfterReturning增强：目标方法的参数为：" + Arrays.toString(jp.getArgs()));
		// 访问被增强处理的目标对象
		System.out.println("AfterReturning增强：被织入增强处理的目标对象为："+ jp.getTarget());
	}

	// 定义After增强处理
	@After("execution(* org.crazyit.app.service.impl.*.*(..))")
	public void release(JoinPoint jp) {
		System.out.println("After增强：模拟方法结束后的释放资源...");
		// 返回被织入增强处理的目标方法
		System.out.println("After增强：被织入增强处理的目标方法为：" + jp.getSignature().getName());
		// 访问执行目标方法的参数
		System.out.println("After增强：目标方法的参数为：" + Arrays.toString(jp.getArgs()));
		// 访问被增强处理的目标对象
		System.out.println("After增强：被织入增强处理的目标对象为：" + jp.getTarget());
	}
}
```


另外一个访问目标方法的参数的方法是：在程序中使用args切入点表达式来绑定目标方法的参数。args中声明有多少个参数，则目标方法中也要声明有多少个参数才能正确被匹配。如：`"execution(* org.crazyit.app.service.impl.*.*(..)) && args(arg0,arg1)"`，arg0和arg1的类型由增强方法决定，如增强方式的入参是`access(String arg0 , String arg1)`，则arg0和arg1的类型都是String。

示例：

```
@Aspect
public class AccessArgAspect{
	// 下面的args(arg0,arg1)会限制目标方法必须有2个形参
	@AfterReturning(returning="rvt" , pointcut="execution(* org.crazyit.app.service.impl.*.*(..)) && args(arg0,arg1)")
	// 此处指定arg0、arg1为String类型
	// 则args(arg0,arg1)还要求目标方法的两个形参都是String类型
	public void access(Object rvt, String arg0 , String arg1)
	{
		System.out.println("调用目标方法第1个参数为:" + arg0);
		System.out.println("调用目标方法第2个参数为:" + arg1);
		System.out.println("获取目标方法返回值:" + rvt);
		System.out.println("模拟记录日志功能...");
	}
}
```

如何控制同一连接点织入的优先顺序呢？方法有二：

1）让切面实现org.springframework.core.Ordered接口，int getOrder()返回的值越小，则优先级越高。

2）直接使用@Order注解来修饰一个切面类，指定一个int型的vlaue值，值越小，优先级越高。


#### 定义切入点

定义切入点的意义是，让不同的增强处理共用一个切入点。

切入点的定义通常包括两个部分：

1）一个切入点表达式。

2）一个包含名字和任意参数的方法签名。

注意：切入点的方法签名的方法体通常为空，返回值类型必须是void；切入点表达式用@Pointcut来标注。如下：

```
	@Pointcut("execution(* com.demo.service.impl.*.*(..))")
	private void myPointCut(){}
```

可以引用其它类中定义好的切入点，格式：类名.切入点方法名()，如下：pointcut="SystemArchitecture.myPointcut()"。

示例：

```
@Aspect
public class SystemArchitecture{
	@Pointcut("execution(* org.crazyit.app.service.impl.*.*(..))")
	public void myPointcut(){}
}
```

```
@Aspect
public class LogAspect{
	// 直接使用SystemArchitecture切面类的myPointcut()切入点
	@AfterReturning(returning="rvt", pointcut="SystemArchitecture.myPointcut()")
	// 声明rvt时指定的类型会限制目标方法必须返回指定类型的值或没有返回值
	// 此处将rvt的类型声明为Object，意味着对目标方法的返回值不加限制
	public void log(Object rvt)
	{
		System.out.println("获取目标方法返回值:" + rvt);
		System.out.println("模拟记录日志功能...");
	}
}
```


#### 切入指示符

前面大量使用到的execution表达式，就是一个切入点指示符。除execution外，spring还支持如下几种切入点指示符：

- within：todo....
- this：
- target：
- args：对连接点的参数类型进行限制
- bean：


#### 组合切入点表达式

spring支持如下切入点表达式：

- &&：要求切入同时匹配两个切入点表达式
- ||：匹配任意一个
- !：不匹配给出的切入点表达式

前面有过下面这种写法，就是组合切入点表达式了：

`pointcut= "execution(* org.crazyit.app.service.impl.*.*(..)) && args(arg0,arg1)"`。



## XML方式实现AOP

所有的切面、切入点和增强处理都必须定义在<aop:config.../>元素内部。<beans.../>元素下可以包含多个<aop:config.../>元素，一个<aop:config.../>元素可以包含pointcut、advisor、aspect元素（且这三个元素必须按此顺序来定义），其中aspect下可以包含多个子元素（且元素无序）——通过使用这些元素就可以在XML文件中配置切面、切入点和增强处理了。一个<aop:config.../>元素可以有多个<aop:aspectj.../>元素。

<aop:config.../>元素所包含的子元素如下：

```xml
<aop:config>
	<aop:pointcut expression="" id=""/>	// a	全局切入点
	<aop:advisor advice-ref=""/>	// b	全局增强处理，配置事务时用到
	<aop:aspect id="" ref="">	// c
		<aop:pointcut expression="" id=""/>
		<aop:declare-parents types-matching="" implement-interface=""/>
		<aop:before method=""/>
		<aop:after method=""/>
		<aop:after-throwing method=""/>
		<aop:after-returning method=""/>
		<aop:around method=""/>
	</aop:aspect>
</aop:config>
```

注意：a、b、c的顺序不能颠倒。a、b是全局切入点、增强处理，可以不定义a、b，但要在c中定义。

另外，使用<aop:config.../>方式进行配置时，可能与Spring的自动代理方式相冲突。例如使用<aop:aspectj-autoproxy.../>或类似方式显式启用自动代理，则可能出现此问题。因此建议：要么全部使用<aop:config.../>配置方式，要么全部使用自动代理方式，不要两者混合使用。


示例：

```
public class FourAdviceTest{
	public Object processTx(ProceedingJoinPoint jp) throws java.lang.Throwable {
		System.out.println("Around增强：执行目标方法之前，模拟开始事务...");
		// 访问执行目标方法的参数
		Object[] args = jp.getArgs();
		// 当执行目标方法的参数存在，
		// 且第一个参数是字符串参数
		if (args != null && args.length > 0
			&& args[0].getClass() == String.class)
		{
			// 修改目标方法调用参数的第一个参数
			args[0] = "【增加的前缀】" + args[0];
		}
		//执行目标方法，并保存目标方法执行后的返回值
		Object rvt = jp.proceed(args);
		System.out.println("Around增强：执行目标方法之后，模拟结束事务...");
		// 如果rvt的类型是Integer，将rvt改为它的平方
		if(rvt != null && rvt instanceof Integer)
			rvt = (Integer)rvt * (Integer)rvt;
		return rvt;
	}
	
	public void authority(JoinPoint jp) {
		System.out.println("②Before增强：模拟执行权限检查");
		// 返回被织入增强处理的目标方法
		System.out.println("②Before增强：被织入增强处理的目标方法为：" + jp.getSignature().getName());
		// 访问执行目标方法的参数
		System.out.println("②Before增强：目标方法的参数为：" + Arrays.toString(jp.getArgs()));
		// 访问被增强处理的目标对象
		System.out.println("②Before增强：被织入增强处理的目标对象为：" + jp.getTarget());
	}
	
	public void log(JoinPoint jp , Object rvt) {
		System.out.println("AfterReturning增强：获取目标方法返回值:" + rvt);
		System.out.println("AfterReturning增强：模拟记录日志功能...");
		// 返回被织入增强处理的目标方法
		System.out.println("AfterReturning增强：被织入增强处理的目标方法为：" + jp.getSignature().getName());
		// 访问执行目标方法的参数
		System.out.println("AfterReturning增强：目标方法的参数为：" + Arrays.toString(jp.getArgs()));
		// 访问被增强处理的目标对象
		System.out.println("AfterReturning增强：被织入增强处理的目标对象为：" + jp.getTarget());
	}
	public void release(JoinPoint jp){
		System.out.println("After增强：模拟方法结束后的释放资源...");
		// 返回被织入增强处理的目标方法
		System.out.println("After增强：被织入增强处理的目标方法为："+ jp.getSignature().getName());
		// 访问执行目标方法的参数
		System.out.println("After增强：目标方法的参数为："+ Arrays.toString(jp.getArgs()));
		// 访问被增强处理的目标对象
		System.out.println("After增强：被织入增强处理的目标对象为："+ jp.getTarget());
	}
}
```

```
public class SecondAdviceTest{
	public void authority(String aa){
		System.out.println("目标方法的参数为：" + aa);
		System.out.println("①号Before增强：模拟执行权限检查");
	}
}
```

```
<?xml version="1.0" encoding="GBK"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:aop="http://www.springframework.org/schema/aop"
	xsi:schemaLocation="http://www.springframework.org/schema/beans 
	http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
	http://www.springframework.org/schema/aop
	http://www.springframework.org/schema/aop/spring-aop-4.0.xsd">
	<aop:config>
		<!-- 将fourAdviceBean转换成切面Bean 切面Bean的新名称为：fourAdviceAspect 指定该切面的优先级为2 -->
		<aop:aspect id="fourAdviceAspect" ref="fourAdviceBean" order="2">
			<!-- 定义一个After增强处理， 直接指定切入点表达式 以切面Bean中的release()方法作为增强处理方法 -->
			<aop:after pointcut="execution(* org.crazyit.app.service.impl.*.*(..))" method="release"/>
			<!-- 定义一个Before增强处理，直接指定切入点表达式 以切面Bean中的authority()方法作为增强处理方法 -->
			<aop:before pointcut="execution(* org.crazyit.app.service.impl.*.*(..))" method="authority"/>
			<!-- 定义一个AfterReturning增强处理，直接指定切入点表达式 以切面Bean中的log()方法作为增强处理方法 -->
			<aop:after-returning pointcut="execution(* org.crazyit.app.service.impl.*.*(..))" method="log" returning="rvt"/>
			<!-- 定义一个Around增强处理，直接指定切入点表达式 以切面Bean中的processTx()方法作为增强处理方法 -->
			<aop:around pointcut="execution(* org.crazyit.app.service.impl.*.*(..))"  method="processTx"/>
		</aop:aspect>

		<!-- 将secondAdviceBean转换成切面Bean 切面Bean的新名称为：secondAdviceAspect 指定该切面的优先级为1，该切面里的增强处理将被优先织入 -->
		<aop:aspect id="secondAdviceAspect" ref="secondAdviceBean" order="1">
			<!-- 定义一个Before增强处理， 直接指定切入点表达式 以切面Bean中的authority()方法作为增强处理方法  且该参数必须为String类型（由authority方法声明中msg参数的类型决定） -->
			<aop:before pointcut= "execution(* org.crazyit.app.service.impl.*.*(..)) and args(aa)"  method="authority"/>
		</aop:aspect>
	</aop:config>

	<bean id="fourAdviceBean" class="org.crazyit.app.aspect.FourAdviceTest"/>
	<bean id="secondAdviceBean" class="org.crazyit.app.aspect.SecondAdviceTest"/>
	<bean id="hello" class="org.crazyit.app.service.impl.HelloImpl"/>
	<bean id="world" class="org.crazyit.app.service.impl.WorldImpl"/>
</beans>
```

总结：其实<aop:aspect ref=””.../>就是我们定义好的切面（也就是注解方式时使用@Aspect的那个类），其子元素（如before）中的method指向我们定义的切面中的方法名。