---
title: "Java-framework-Spring-1-容器"
date: 2019-08-15T11:02:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---

# 依赖注入

程序中不用new对象，从spring容器中获取。


## 设值注入

为类中的字段注入值，通过setter方法设置值（所以要为字段定义setXxx方法）。

1、引用类型

```
<bean id="chinese" class="org.crazyit.app.service.impl.Chinese">
	<property name="axe" ref="stoneAxe"/>
</bean>

<bean id="stoneAxe" class="org.crazyit.app.service.impl.StoneAxe"/>
```

2、普通属性值

```
<bean id="exampleBean" class="org.crazyit.app.service.ExampleBean">
	<!-- 指定int型的参数值 -->
	<property name="integerField" value="1"/>
	<!-- 指定double型的参数值 -->
	<property name="doubleField" value="2.3"/>
</bean>
```

3、注入集合值

```
<?xml version="1.0" encoding="GBK"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="http://www.springframework.org/schema/beans"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
	http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">
	
	<bean id="stoneAxe" class="org.crazyit.app.service.impl.StoneAxe"/>
	<bean id="steelAxe" class="org.crazyit.app.service.impl.SteelAxe"/>
	
	<bean id="chinese" class="org.crazyit.app.service.impl.Chinese">
		<property name="schools">
			<!-- 为调用setSchools()方法配置List集合作为参数值 -->
			<list>
				<!-- 每个value、ref、bean...都配置一个List元素 -->
				<value>小学</value>
				<value>中学</value>
				<value>大学</value>
			</list>
		</property>
		<property name="scores">
			<!-- 为调用setScores()方法配置Map集合作为参数值 -->
			<map>
				<!-- 每个entry配置一个key-value对 -->
				<entry key="数学" value="87"/>
				<entry key="英语" value="89"/>
				<entry key="语文" value="82"/>
			</map>
		</property>
		<property name="phaseAxes">
			<!-- 为调用setPhaseAxes()方法配置Map集合作为参数值 -->
			<map>
				<!-- 每个entry配置一个key-value对 -->
				<entry key="原始社会" value-ref="stoneAxe"/>
				<entry key="农业社会" value-ref="steelAxe"/>
			</map>
		</property>
		<property name="health">
			<!-- 为调用setHealth()方法配置Properties集合作为参数值 -->
			<props>
				<!-- 每个prop元素配置一个属性项，其中key指定属性名 -->
				<prop key="血压">正常</prop>
				<prop key="身高">175</prop>
			</props>
			<!-- 
			<value>
				pressure=normal
				height=175
			</value> -->
		</property>
		<property name="axes">
			<!-- 为调用setAxes()方法配置Set集合作为参数值 -->
			<set>
				<!-- 每个value、ref、bean..都配置一个Set元素 -->
				<value>普通的字符串</value>
				<bean class="org.crazyit.app.service.impl.SteelAxe"/>
				<ref bean="stoneAxe"/>
				<!-- 为Set集合配置一个List集合作为元素 -->
				<list>
					<value>20</value>
					<!-- 再次为List集合配置一个Set集合作为元素 -->
					<set>
						<value type="int">30</value>
					</set>
				</list>
			</set>
		</property>
		<property name="books">
			<!-- 为调用setBooks()方法配置数组作为参数值 -->
			<list>
				<!-- 每个value、ref、bean...都配置一个数组元素 -->
				<value>疯狂Java讲义</value>
				<value>疯狂Android讲义</value>
				<value>轻量级Java EE企业应用实战</value>
			</list>
		</property>
	</bean>
</beans>
```

4、组合属性

`<property name="person.name" value="孙悟空"/>`


为属性设计setter方法，是个不错的选择。（新的spring版本好像不用？？？？）

答：“零配置”则不用写setter方法，xml的配置方式，则要写。


## 构造注入

通过构造器参数为字段注入值。

1、引用类型：

```
<bean id="chinese" class="org.crazyit.app.service.impl.Chinese">
	<constructor-arg ref="steelAxe" type="org.crazyit.app.service.Axe"/>
</bean>

<bean id="steelAxe" class="org.crazyit.app.service.impl.SteelAxe"/>
```

2、基本类型或String类型，可以直接赋值，不用ref属性了：

```
<bean id="chinese" class="org.crazyit.app.service.impl.Chinese">
	<constructor-arg value=”hello”/>
	<constructor-arg value=”123”/>
</bean>
```

也可以为上面的元素指定type属性，指定类型。


# Spring容器

- Java Se常用的容器：

    FileSystemXmlApplicationContext、ClassPathXmlApplicationContext、AnnotationConfigApplicationContext。

- Java Web常用的容器：

    XmlWebApplicationContext、AnnotationConfigWebApplicationContext。

创建容器示例：

```
ApplicationContext ctx = new FileSystemXmlApplicationContext("beans.xml","service.xml");
ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml","service.xml");
```

在容器启动时，默认自动创建singleton bean。

## ApplicationContext事件机制

ApplicationContext事件机制是观察者设计模式的实现。有如下两个重要成员：

- ApplicationEvent：容器事件，必须由ApplicationContext发布。
- ApplicationListener：监听器，可由容器中任意监听器Bean担任。

每当ApplicationContext发布ApplicationEvent时，ApplicationListener Bean将自动触发（onApplicationEvent(ApplicationEvent event)被调用）。

事件的三个要素：

- 事件源：ApplicationContext
- 事件：ApplicationEvent
- 监听器：ApplicationListener


示例：

```
public class EmailEvent extends ApplicationEvent
{
	private String address;
	private String text;

	public EmailEvent(Object source)
	{
		super(source);
	}
	
	// 初始化全部成员变量的构造器
	public EmailEvent(Object source , String address , String text)
	{
		super(source);
		this.address = address;
		this.text = text;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}
	public String getAddress()
	{
		return this.address;
	}

	public void setText(String text)
	{
		this.text = text;
	}
	public String getText()
	{
		return this.text;
	}
}
```

```
public class EmailNotifier implements ApplicationListener
{
	// 该方法会在容器发生事件时自动触发
	public void onApplicationEvent(ApplicationEvent evt)
	{
		// 只处理EmailEvent，模拟发送email通知...
		if (evt instanceof EmailEvent)
		{
			EmailEvent emailEvent = (EmailEvent)evt;
			System.out.println("需要发送邮件的接收地址 " + emailEvent.getAddress());
			System.out.println("需要发送邮件的邮件正文 " + emailEvent.getText());
		}
		else
		{
			// 其他事件不作任何处理
			System.out.println("其他事件：" + evt);
		}
	}
}
```

beans.xml

```
<?xml version="1.0" encoding="GBK"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="http://www.springframework.org/schema/beans"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
	http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">
	<!-- 配置监听器 -->
	<bean class="org.crazyit.app.listener.EmailNotifier"/>
</beans>
```

```
public class SpringTest
{
	public static void main(String[] args)
	{
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		// 创建一个ApplicationEvent对象
		EmailEvent ele = new EmailEvent("test" ,"spring_test@163.com" , "this is a test");
		// 发布容器事件
		ctx.publishEvent(ele);
	}
}
```

首先要实现ApplicationListener接口，并交给spring管理（相当于注册一个监听器）。


## 让Bean获取Spring容器

在web开发中，不可能手动创建spring容器，有如下两种方法获取spring容器：

- 实现BeanFactoryAware接口，重写setBeanFactory(BeanFactory beanFactory)方法，其中beanFactory就是spring容器了
- 实现ApplicationContextAware接口，重写setApplicationContext(ApplicationContext applicationContext)方法，其中applicationContext就是spring容器了。


## 创建Bean的三种方式

### 构造器创建Bean实例

这种方式就是前面介绍的构造注入的方式。class属性指定的就是Bean实例的实现类。

### 静态工厂方法创建Bean实例

使用静态工厂方法创建bean时，<bean .../>元素的需要指定class属性、factory-method属性：

- class：该属性的值为静态工厂类的类名
- factory-method：该属性指定静态工厂方法来生产Bean实例

如果静态工厂方法需要参数，则使用<constructor-arg.../>元素传入。


示例：

```
public interface Being
{
	public void testBeing();
}
```

```
public class Dog implements Being
{
	private String msg;
	public void setMsg(String msg)
	{
		this.msg = msg;
	}
	
	public void testBeing()
	{
		System.out.println(msg +	"，狗爱啃骨头");
	}
}
```

```
public class Cat implements Being
{
	private String msg;
	public void setMsg(String msg)
	{
		this.msg = msg;
	}
	
	public void testBeing()
	{
		System.out.println(msg +	"，猫喜欢吃老鼠");
	}
}
```

```
public class BeingFactory
	public static Being getBeing(String arg)
	{
		if (arg.equalsIgnoreCase("dog"))
		{
			return new Dog();
		}else
		{
			return new Cat();
		}
	}
}
```

```
<?xml version="1.0" encoding="GBK"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="http://www.springframework.org/schema/beans"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
	http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">

    <!--下面代码相当于 dog = org.crazyit.app.factory.BeingFactory.getBeing("dog");-->
    
	<bean id="dog" class="org.crazyit.app.factory.BeingFactory" factory-method="getBeing">
		<constructor-arg value="dog"/>
		<property name="msg" value="我是狗"/>
	</bean>
	
	<bean id="cat" class="org.crazyit.app.factory.BeingFactory" factory-method="getBeing">
		<constructor-arg value="cat"/>
		<property name="msg" value="我是猫"/>
	</bean>
</beans>
```

```
public class SpringTest
{
	public static void main(String[] args)
	{
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		
		Being b1 = ctx.getBean("dog" , Being.class);
		b1.testBeing();
		
		Being b2 = ctx.getBean("cat" , Being.class);
		b2.testBeing();
	}
}
```

### 实例工厂方法创建Bean实例


使用实例工厂方法创建bean时，<bean .../>元素需要指定如下两个属性：

- factory-bean：该属性值为工厂Bean的id。
- factory-method：该属性指定实例工厂的工厂方法。

如果实例工厂方法需要参数，则使用<constructor-arg.../>元素传入。

示例：

```
public interface Person
{
	public String sayHello(String name);
	public String sayGoodBye(String name);
}
```

```
public class American implements Person
{
	public String sayHello(String name){
		return name + ",Hello!";
	}
	public String sayGoodBye(String name){
		return name + ",Good Bye!";
	}
}
```

```
public class Chinese implements Person
{
	public String sayHello(String name){
		return name + "，您好";
	}
	public String sayGoodBye(String name)	{
		return name + "，下次再见";
	}
}
```

```
public class PersonFactory
{
	public Person getPerson(String ethnic)
	{
		if (ethnic.equalsIgnoreCase("chin")){
			return new Chinese();
		}else{
			return new American();
		}
	}
}
```

```
<?xml version="1.0" encoding="GBK"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="http://www.springframework.org/schema/beans"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
	http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">
	
	<bean id="personFactory" class="org.crazyit.app.factory.PersonFactory"/>
	
	<bean id="chinese" factory-bean="personFactory"	factory-method="getPerson">
		<constructor-arg value="chin"/>
	</bean>
	
	<bean id="american" factory-bean="personFactory" factory-method="getPerson">
		<constructor-arg value="ame"/>
	</bean>
</beans>
```

```
public class SpringTest
{
	public static void main(String[] args)
	{
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		
		Person p1 = ctx.getBean("chinese", Person.class);
		System.out.println(p1.sayHello("Mary"));
		System.out.println(p1.sayGoodBye("Mary"));
		
		Person p2 = ctx.getBean("american", Person.class);
		System.out.println(p2.sayHello("Jack"));
		System.out.println(p2.sayGoodBye("Jack"));
	}
}
```

总结：实例工厂方法与静态工厂方法的不同之处是，生产实例的不指向一个工厂类，而是指向一个工厂的实例（因为在配置工厂方法的前已经配置了一个工厂bean，直接使用这个bean）。