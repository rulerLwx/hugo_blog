---
title: "Java-framework-Shiro"
date: 2019-08-10T11:02:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---


# 初识 Shiro

1）什么是shiro

Apache Shiro 是Java 的一个安全框架。Shiro 可以非常容易的开发出足够好的应用，其
不仅可以用在JavaSE 环境，也可以用在JavaEE 环境。Shiro 可以帮助我们完成：认证、
授权、加密、会话管理、与Web 集成、缓存等。

2）为什么要学shiro

既然shiro 将安全认证相关的功能抽取出来组成一个框架，使用shiro 就可以非常快速的
完成认证、授权等功能的开发，降低系统成本。

shiro 使用广泛，shiro 可以运行在web 应用，非web 应用，集群分布式应用中越来越多
的用户开始使用shiro。

java 领域中spring security(原名Acegi)也是一个开源的权限管理框架，但是spring security
依赖spring 运行，而shiro 就相对独立，最主要是因为shiro 使用简单、灵活，所以现在越来
越多的用户选择shiro。

3）基本功能

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708084523.png)

Authentication：身份认证/登录，验证用户是不是拥有相应的身份；

Authorization：授权，即权限验证，验证某个已认证的用户是否拥有某个权限；即判断用
户是否能做事情，常见的如：验证某个用户是否拥有某个角色。或者细粒度的验证某个用
户对某个资源是否具有某个权限；

Session Manager：会话管理，即用户登录后就是一次会话，在没有退出之前，它的所有信
息都在会话中；会话可以是普通JavaSE 环境的，也可以是如Web 环境的；

Cryptography：加密，保护数据的安全性，如密码加密存储到数据库，而不是明文存储；

- Web Support：Web 支持，可以非常容易的集成到Web 环境；
- Caching：缓存，比如用户登录后，其用户信息、拥有的角色/权限不必每次去查，这样可以
提高效率；
- Concurrency：shiro 支持多线程应用的并发验证，即如在一个线程中开启另一个线程，能
把权限自动传播过去；
- Testing：提供测试支持；
- Run As：允许一个用户假装为另一个用户（如果他们允许）的身份进行访问；
- Remember Me：记住我，这个是非常常见的功能，即一次登录后，下次再来的话不用登录
了。

Shiro 不会去维护用户、维护权限；这些需要我们自己去设计/提供；然后通过
相应的接口注入给Shiro 即可。

4）Shiro 架构

从外部看：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708084556.png)

从内部看：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708085430.png)

Subject：即主体，用于获取主体信息（principals/credentials），外部应用与subject 进行交互，subject 记录了当前操作用户，将用户的
概念理解为当前操作的主体，可能是一个通过浏览器请求的用户，也可能是一个运行的程序。
Subject 在shiro 中是一个接口，接口中定义了很多认证授相关的方法，外部程序通过
subject 进行认证授，而subject 是通过SecurityManager 安全管理器进行认证授权

SecurityManager：即安全管理器，对全部的subject 进行安全管理，它是shiro 的核心，
负责对所有的subject 进行安全管理。通过SecurityManager 可以完成subject 的认证、授权
等，实质上SecurityManager 是通过Authenticator 进行认证，通过Authorizer 进行授权，通
过SessionManager 进行会话管理等。
SecurityManager 是一个接口，继承了Authenticator, Authorizer, SessionManager 这三个接
口。

Authenticator：即认证器，对用户身份进行认证，Authenticator 是一个接口，shiro 提供
ModularRealmAuthenticator 实现类，通过ModularRealmAuthenticator 基本上可以满足大多数
需求，也可以自定义认证器。

Authorizer：即授权器，用户通过认证器认证通过，在访问功能时需要通过授权器判断用
户是否有此功能的操作权限。

Realm：即领域，相当于datasource 数据源，securityManager 进行安全认证需要通过Realm
获取用户权限数据，比如：如果用户身份数据在数据库那么realm 就需要从数据库获取用户
身份信息。
注意：不要把realm 理解成只是从数据源取数据，在realm 中还有认证授权校验的相关
的代码。

sessionManager：即会话管理，shiro 框架定义了一套会话管理，它不依赖web 容器的session，
所以shiro 可以使用在非web 应用上，也可以将分布式应用的会话集中在一点管理，此特性
可使它实现单点登录。

SessionDAO：即会话dao，是对session 会话操作的一套接口，比如要将session 存储到数据库，
可以通过jdbc 将会话存储到数据库。

CacheManager：即缓存管理，将用户权限数据存储在缓存，这样可以提高性能。

Cryptography：即密码管理，shiro 提供了一套加密/解密的组件，方便开发。比如提供常
用的散列、加/解密等功能。

# 认证

## 入门-ini认证（即：不连数据库）

1）基本概念

Authentication，身份验证
即在应用中谁能证明他就是他本人。一般提供如他们的身份ID 一些标识信息来
表明他就是他本人，如提供身份证，用户名/密码来证明。
在shiro 中，用户需要提供 principals （身份）和 credentials（证明）给shiro，从而应用能
验证用户身份

principals：身份，即主体的标识属性，可以是任何东西，如用户名、邮箱等，唯一即可。
一个主体可以有多个principals，但只有一个Primary principals，一般是用户名/密码/手机号。

credentials：证明/凭证，即只有主体知道的安全值，如密码/数字证书等。

最常见的principals 和credentials 组合就是用户名/密码了。

2）认证流程

![](https://i.loli.net/2020/07/08/7tlMDSauIT3fcqF.png)

3）代码实现

依赖

```xml
    <dependencies>
        <dependency>
            <groupId>org.apache.shiro</groupId>
            <artifactId>shiro-all</artifactId>
            <version>1.4.1</version>
            <type>pom</type>
        </dependency>
        <dependency>
            <groupId>commons-logging</groupId>
            <artifactId>commons-logging</artifactId>
            <version>1.2</version>
        </dependency>
        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>1.2.17</version>
        </dependency>
    </dependencies>
```

resource目录下新建 shiro.ini 文件

```
[users]
zhangsan=1111
lisi=1111
```

认证的简单实现

```java
public class AuthenticationDemo {
    public static void main(String[] args) {
        // 构建SecurityManager 工厂，IniSecurityManagerFactory 可以从ini文件中初始化SecurityManager环境
        Factory<SecurityManager> factory = new
                IniSecurityManagerFactory("classpath:shiro.ini");
        //通过工厂获得SecurityManager 实例
        SecurityManager securityManager = factory.getInstance();
        //将securityManager 设置到运行环境中
        SecurityUtils.setSecurityManager(securityManager);
        //获取subject 实例
        Subject subject = SecurityUtils.getSubject();
        //创建用户名,密码身份验证Token
        UsernamePasswordToken token = new
                UsernamePasswordToken("zhangsan", "1111");
        try {
            //登录，即身份验证
            subject.login(token);
            if (subject.isAuthenticated()) {
                System.out.println("用户授权成功");
            }
        } catch (AuthenticationException e) {
            e.printStackTrace();
            //身份认证失败
            //...
        }
        //退出
        subject.logout();
    }
}
```

认证流程：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708090100.png)

## 自定义 realm

自定realm的目的：用户认证的信息不用硬编码，比如写在 ini 文件中。

一般需要实现 AuthorizingRealm

```java
public class MyRealm extends AuthorizingRealm {
    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        return null;
    }

    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        return null;
    }
}
```

示例：

1）继承 AuthorizingRealm，重写3个方法

```java
public class MyRealm extends AuthorizingRealm {

    //当前realm名字
    @Override
    public String getName() {
        return "MyRealm";
    }

    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        return null;
    }

    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        //入参token：登录时包装的UsernamePasswordToken

        String username = (String) token.getPrincipal();

        String credential = "123456";//模拟数据库中获取的密码是123456

        SimpleAuthenticationInfo info =
                new SimpleAuthenticationInfo(username, credential, getName());
        return info;
    }
}
```

2）增加配置文件 shiro-realm.ini

```
#自定义一个realm
myRealm=com.demo.shiro.realm.MyRealm
securityManager.realms=$myRealm
```

3）测试

```java
    @Test
    public void testMyRealm()
    {
        Factory<SecurityManager> factory =
                new IniSecurityManagerFactory("classpath:shiro-realm.ini");
        SecurityManager securityManager = factory.getInstance();
        SecurityUtils.setSecurityManager(securityManager);
        Subject subject = SecurityUtils.getSubject();

        UsernamePasswordToken token =
                new UsernamePasswordToken("zhangsan", "123456");
        try {
            //6、登录
            subject.login(token);

        } catch (Exception ex) {
            //异常处理
            throw ex;
        }

        System.out.println("是否登录成功："+subject.isAuthenticated());

        //登出
        subject.logout();

        System.out.println("是否登录成功："+subject.isAuthenticated());
    }
```


## 在realm中对密码加密

散列算法：一般用于生成数据的摘要信息，是一种不可逆的算法，一般适合存储密码之类的数据，常见的散列算法如MD5、SHA等。一般进行散列时最好提供一个salt（盐），比如加密密码“admin”，产生的散列值是“21232f297a57a5a743894a0e4a801fc3”，可以到一些md5解密网站很容易的通过散列值得到密码“admin”，即如果直接对密码进行散列相对来说破解更容易，此时我们可以加一些只有系统知道的干扰数据，如用户名和ID（即盐）；这样散列的对象是“密码+用户名+ID”，这样生成的散列值相对来说更难破解。

1）自定义realm

```java
public class PasswordRealm extends AuthorizingRealm {
    //当前realm名字
    @Override
    public String getName() {
        return "PasswordRealm";
    }

    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        return null;
    }

    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        //入参token：登录时包装的UsernamePasswordToken

        String username = (String) token.getPrincipal();

        if (!username.equals("zhangsan")) {//模拟用户不存在
            return null;
        }

        String credential = "5cb022bfe7cab9f9f6b043d32e58e617";//模拟数据库中获取的密码是密文

        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(
                username, credential, ByteSource.Util.bytes("zhangsan"),getName());//记得加盐值
        return info;
    }
}
```

注意：使用 ByteSource.Util.bytes("zhangsan") 加盐值

2）编写 resources/shiro-cryptography.ini

```
[main]
#定义凭证匹配器
credentialsMatcher= org.apache.shiro.authc.credential.HashedCredentialsMatcher
#散列算法
credentialsMatcher.hashAlgorithmName=md5
#散列次数
credentialsMatcher.hashIterations=5

#将凭证匹配器设置到realm
myRealm= com.demo.shiro.realm.PasswordRealm
myRealm.credentialsMatcher=$credentialsMatcher
securityManager.realms=$myRealm
```

3）测试

```java
    @Test
    public void testPasswordRealm()
    {
        Factory<SecurityManager> factory =
                new IniSecurityManagerFactory("classpath:shiro-cryptography.ini");
        SecurityManager securityManager = factory.getInstance();
        SecurityUtils.setSecurityManager(securityManager);
        Subject subject = SecurityUtils.getSubject();

        UsernamePasswordToken token =
                new UsernamePasswordToken("zhangsan", "123456");
        try {
            //6、登录
            subject.login(token);

        } catch (Exception ex) {
            //异常处理
            throw ex;
        }

        System.out.println("是否登录成功："+subject.isAuthenticated());

        //登出
        subject.logout();

        System.out.println("是否登录成功："+subject.isAuthenticated());
    }
```

4）附件：测试时对密码加密

```java
public class Md5Test {
    @Test
    public void testMd5(){
        String password = "123456";//明文

        Md5Hash md5Hash = new Md5Hash(password);//md5加密

        md5Hash = new Md5Hash(password, "zhangsan");//md5 + 盐值

        md5Hash = new Md5Hash(password, "zhangsan", 5);//md5 + 盐值 + 散列次数

        System.out.println(md5Hash);//5cb022bfe7cab9f9f6b043d32e58e617
    }
}
```



# 授权

授权模式：RBAC，是基于角色的权限管理。

## 授权方式

1）编程方式，通过写if/else授权代码块完成

```java
Subject subject = SecurityUtils.getSubject();  
if(subject.hasRole(“admin”)) {  
    //有权限  
} else {  
    //无权限  
}  

```

2）注解方式，通过在执行的Java方法上放置相应的注解完成

```java
@RequiresRoles("admin")  
@RequiresPermission(“employee:save”)
public void hello() {  
    //有权限  
} 
```

3）jsp标签方式，在JSP页面通过相应的标签完成

```jsp
<shiro:hasRole name="admin">  
         <!— 有权限 —>  
</shiro:hasRole> 
```

## ini授权

1）添加 resources/shiro-

```
[users]
#用户zhang的密码是123，此用户具有role1和role2两个角色
zhangsan=666,role1,role2
lisi=888,role2

[roles]
#角色role1对资源user拥有create、update权限
role1=user:create,user:update
#角色role2对资源user拥有create、delete权限
role2=user:create,user:delete
#角色role3对资源user拥有create权限
role3=user:create
```

在ini文件中用户、角色、权限的配置规则是：“用户名=密码，角色1，角色2...” “角色=权限1，权限2...”，首先根据用户名找角色，再根据角色找权限，角色是权限集合。

权限字符串的规则是：“资源标识符：操作：资源实例标识符”，意思是对哪个资源的哪个实例具有什么操作，“:”是资源/操作/实例的分割符，权限字符串也可以使用*通配符。

例子：
- 用户创建权限：user:create，或user:create:*
- 用户修改实例001的权限：user:update:001
- 用户实例001的所有权限：user：*：001

2）测试 角色

```java
    @Test
    public void testHashRoles() {
        //1、登录
        Factory<SecurityManager> factory =
                new IniSecurityManagerFactory("classpath:shiro-permission.ini");
        SecurityManager securityManager = factory.getInstance();
        SecurityUtils.setSecurityManager(securityManager);
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token =
                new UsernamePasswordToken("zhangsan", "123456");
        subject.login(token);

        //2、判断是否拥有角色：方式一
        System.out.println(subject.hasRole("role1"));
        System.out.println(subject.hasAllRoles(Arrays.asList("role1","role2","role3")));
        System.out.println(Arrays.toString(subject.hasRoles(Arrays.asList("role1","role2","role3"))));

        //2、判断是否拥有角色：方式二
        subject.checkRole("role2");//如果没有角色，将抛出 UnauthorizedException

        //登出
        //subject.logout();
    }
```

3）测试 权限

```java
    @Test
    public void testHashPermission() {
        //1、登录
        Factory<SecurityManager> factory =
                new IniSecurityManagerFactory("classpath:shiro-permission.ini");
        SecurityManager securityManager = factory.getInstance();
        SecurityUtils.setSecurityManager(securityManager);
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token =
                new UsernamePasswordToken("zhangsan", "123456");
        subject.login(token);

        //2、是否有权限，方式一
        System.out.println(subject.isPermitted("user:create"));
        System.out.println(subject.isPermitted("create"));
        System.out.println(subject.isPermittedAll("user:create","user:update"));

        //2、是否有权限，方式二
        subject.checkPermission("user:create");//如果没有权限，将抛出 UnauthorizedException
        
        //登出
        //subject.logout();
    }
```

## 自定义 realm

1）自定义realm

```java
public class PermissionRealm extends AuthorizingRealm {
    //当前realm名字
    @Override
    public String getName() {
        return "PermissionRealm";
    }

    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        String username = (String) principals.getPrimaryPrincipal();//SimpleAuthenticationInfo的第一个参数：username

        //模拟从数据库中查询出来的数据
        ArrayList<String> roles = new ArrayList<>();//从数据库中查到的角色集合
        ArrayList<String> permissions = new ArrayList<>();//从数据库中查到的权限集合
        roles.add("role1");
        permissions.add("user:delete");

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.addRoles(roles);
        info.addStringPermissions(permissions);

        return info;
    }

    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        //入参token：登录时包装的UsernamePasswordToken

        String username = (String) token.getPrincipal();

        if (!username.equals("zhangsan")) {//模拟用户不存在
            return null;
        }

        String credential = "123456";

        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(
                username, credential,getName());
        return info;
    }
}
```

2）配置resources/shiro-permission-realm.ini

```
[main]
#声明一个realm
myReal=com.demo.shiro.realm.PermissionRealm
#指定securityManager的realms实现
securityManager.realms=$myReal
```

3）

```java
    @Test
    public void testRolePermissionByRealm() {
        //1、登录
        Factory<SecurityManager> factory =
                new IniSecurityManagerFactory("classpath:shiro-permission-realm.ini");
        SecurityManager securityManager = factory.getInstance();
        SecurityUtils.setSecurityManager(securityManager);
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token =
                new UsernamePasswordToken("zhangsan", "123456");
        subject.login(token);

        //是否拥有角色
        System.out.println(subject.hasRole("role1"));

        //是否有权限
        System.out.println(subject.isPermitted("user:create"));

        //登出
        //subject.logout();
    }
```

## 授权流程分析

 跟认证类似

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708090153.png)

1. 首先调用Subject.isPermitted*/hasRole*接口，其会委托给SecurityManager，而SecurityManager接着会委托给Authorizer
2. Authorizer是真正的授权者，如果我们调用如isPermitted(“user:view”)，其首先会通过PermissionResolver把字符串转换成相应的Permission实例；
3. 在进行授权之前，其会调用相应的Realm获取Subject相应的角色/权限用于匹配传入的角色/权限；
4. Authorizer会判断Realm的角色/权限是否和传入的匹配，如果有多个Realm，会委托给ModularRealmAuthorizer进行循环判断，如果匹配如isPermitted*/hasRole*会返回true，否则返回false表示授权失败。


# Servlet + Shiro

Shiro与Web集成，主要是通过配置一个ShiroFilter拦截所有URL，其中ShiroFilter类似于如Strut2/SpringMVC这种web框架的前端控制器，是所有请求入口点，负责根据配置（如ini配置文件），判断请求进入URL是否需要登录/权限等工作。

1）导入依赖

```xml
<dependencies>
        <dependency>
            <groupId>commons-logging</groupId>
            <artifactId>commons-logging</artifactId>
            <version>1.1.3</version>
        </dependency>
        <!-- shiro核心的依赖 -->
        <dependency>
            <groupId>org.apache.shiro</groupId>
            <artifactId>shiro-core</artifactId>
            <version>${shiro.previousVersion}</version>
        </dependency>
        <!-- shiro对web支持的依赖 -->
        <dependency>
            <groupId>org.apache.shiro</groupId>
            <artifactId>shiro-web</artifactId>
            <version>${shiro.previousVersion}</version>
        </dependency>
        <!-- serlvet-api -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>3.0.1</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>
```

2）在web.xml中配置 ShiroFilter

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://java.sun.com/xml/ns/javaee"
         xmlns:web="http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd"
         xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd" id="WebApp_ID" version="3.0">

    <!--初始化securityManager对象所需要的环境配置-->
    <context-param>
        <param-name>shiroEnvironmentClass</param-name>
        <param-value>org.apache.shiro.web.env.IniWebEnvironment</param-value>
    </context-param>
    <context-param>
        <param-name>shiroConfigLocations</param-name>
        <param-value>classpath:shiro.ini</param-value>
    </context-param>
    <!--
    	从Shiro 1.2开始引入了Environment/WebEnvironment的概念，即由它们的实现提供相应的SecurityManager及其相应的依赖。
    	ShiroFilter会自动找到Environment然后获取相应的依赖。
    	底层:返回反射创建shiroEnvironmentClass对象,调用其init方法.
    		shiroEnvironmentClass中的init方法创建SecurityManager实例并绑定到当前运行环境
     -->
    <listener>
        <listener-class>org.apache.shiro.web.env.EnvironmentLoaderListener</listener-class>
    </listener>


    <filter>
        <filter-name>shiroFilter</filter-name>
        <filter-class>org.apache.shiro.web.servlet.ShiroFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>shiroFilter</filter-name>
        <!-- 拦截所有的请求 -->
        <url-pattern>/*</url-pattern>
    </filter-mapping>
</web-app>
```

3）编写 resources/shiro.ini

```
[main]
#默认是/login.jsp
authc.loginUrl=/login
#用户无需要的角色时跳转的页面
roles.unauthorizedUrl=/nopermission.jsp
#用户无需要的权限时跳转的页面
perms.unauthorizedUrl=/nopermission.jsp
#登出之后重定向的页面
logout.redirectUrl=/login
[users]
admin=666,admin
zhangsan=666,deptMgr
[roles]
admin=employee:*,department:*
deptMgr=department:view
[urls]
#静态资源可以匿名访问
/static/**=anon
#访问员工列表需要身份认证及需要拥有admin角色
/employee=authc,roles[admin]
#访问部门列表需要身份认证及需要拥有department:view的权限
/department=authc,perms["department:view"]
#当请求loginOut,会被logout捕获并清除session
/loginOut=logout
#所有的请求都需要身份认证
/**=authc
```

过滤器简称 | 对应的java类
---|---
anon	|	org.apache.shiro.web.filter.authc.AnonymousFilter
authc	|	org.apache.shiro.web.filter.authc.FormAuthenticationFilter
authcBasic	|	org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter
roles	|	org.apache.shiro.web.filter.authz.RolesAuthorizationFilter
perms	|	org.apache.shiro.web.filter.authz.PermissionsAuthorizationFilter
user	|	org.apache.shiro.web.filter.authc.UserFilter
logout	|	org.apache.shiro.web.filter.authc.LogoutFilter
port	|	org.apache.shiro.web.filter.authz.PortFilter
rest	|	org.apache.shiro.web.filter.authz.HttpMethodPermissionFilter
ssl	|	org.apache.shiro.web.filter.authz.SslFilter


- anon：匿名拦截器，即不需要登录即可访问；一般用于静态资源过滤；示例“/static/**=anon”
- authc：表示需要认证(登录)才能使用;示例“/**=authc”主要属性：
    - usernameParam：表单提交的用户名参数名（ username）；
    - passwordParam：表单提交的密码参数名（password）；
    - rememberMeParam：表单提交的密码参数名（rememberMe）；
    - loginUrl：登录页面地址（/login.jsp）；
    - successUrl：登录成功后的默认重定向地址；
    - failureKeyAttribute：登录失败后错误信息存储key（shiroLoginFailure）；
- authcBasic：Basic HTTP身份验证拦截器，主要属性：
    - applicationName：弹出登录框显示的信息（application）；
- roles：角色授权拦截器，验证用户是否拥有资源角色；示例“/admin/**=roles[admin]”
- perms：权限授权拦截器，验证用户是否拥有资源权限；示例“/user/create=perms["user:create"]”
- user：用户拦截器，用户已经身份验证/记住我登录的都可；示例“/index=user”
- logout：退出拦截器，主要属性：
    - redirectUrl：退出成功后重定向的地址（/）;示例“/logout=logout”
- port：端口拦截器，主要属性：
    - port（80）：可以通过的端口；示例“/test= port[80]”，如果用户访问该页面是非80，将自动将请求端口改为80并重定向到该80端口，其他路径/参数等都一样
- rest：rest风格拦截器，自动根据请求方法构建权限字符串（GET=read, POST=create,PUT=update,DELETE=delete,HEAD=read,TRACE=read,OPTIONS=read, MKCOL=create）构建权限字符串；
       示例“/users=rest[user]”，会自动拼出“user:read,user:create,user:update,user:delete”权限字符串进行权限匹配（所有都得匹配，isPermittedAll）；
- ssl：SSL拦截器，只有请求协议是https才能通过；否则自动跳转会https端口（443）；其他和port拦截器一样；

注：

- anon，authcBasic，auchc，user是认证过滤器，
- perms，roles，ssl，rest，port是授权过滤器

参考：org.apache.shiro.web.filter.mgt.DefaultFilter 枚举类

---
![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708090515.png)

---


这些默认的拦截器会自动注册，可以直接在ini配置文件中通过“拦截器名.属性”设置其属性：

```
#如果身份验证没有通过,就跳转到loginUrl指定的页面
authc.loginUrl=/login  
#如果用户没有角色,就跳转到unauthorizedUrl指定的页面
roles.unauthorizedUrl=/nopermission.jsp 
#如果用户没有权限,就跳转到unauthorizedUrl指定的页面
perms.unauthorizedUrl=/nopermission.jsp
```

另外如果某个拦截器不想使用了可以直接通过如下配置直接禁用：
perms.enabled=false

---
authc登录拦截器工作原理

authc拦截器有2个作用：

1）登录认证

请求进来时，拦截并判断当前用户是否登录了，如果已经登录了放行， 如果没有登录，跳转到authc.loginUrl属性配置的路径，注意：默认是/login.jsp

2）执行登录认证

请求进来时，如果**请求的路径为authc.loginUrl属性配置的路径**（没配置，默认是/login.jsp）时，如果当前用户没有登录，authc这个拦截器会尝试获取请求中的账号跟密码值，然后比对ini配置文件或者realm中的用户列表，如果比对正确，直接执行登录操作，反之，抛异常，跳转到authc.loginUrl指定的路径。

注意：请求中账号与密码必须固定为username 跟password， 如果需要改动必须额外指定，authc.usernameParam=xxx   authc.passwordParam=xxxx

authc登录成功之后处理逻辑：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708090541.png)

authc登录失败之后处理逻辑：

```java
@Controller
public class LoginController {

    @RequestMapping("/login")
    public String login(Model model, HttpServletRequest req) throws  Exception{
        //如果登陆失败从request中获取认证异常信息，shiroLoginFailure就是shiro异常类的全限定名
        String exceptionClassName = (String) req.getAttribute("shiroLoginFailure");
        //根据shiro返回的异常类路径判断，抛出指定异常信息
        if(exceptionClassName!=null){
            if (UnknownAccountException.class.getName().equals(exceptionClassName)) {
                //最终会抛给异常处理器

                model.addAttribute("errorMsg", "账号不存在");
            } else if (IncorrectCredentialsException.class.getName().equals(
                    exceptionClassName)) {
                model.addAttribute("errorMsg", "用户名/密码错误");
            } else {
                //最终在异常处理器生成未知错误.
                model.addAttribute("errorMsg", "其他异常信息");
            }
        }
        //此方法不处理登陆成功（认证成功），shiro认证成功会自动跳转到上一个请求路径
        //登陆失败还到login页面
        return "forward:/login.jsp";
    }
}
```



# Spring MVC + Shiro

## 整合Shiro

参考官方示例：https://github.com/apache/shiro/tree/master/samples/spring-mvc

依赖（从官方示例抄的）：

```xml
    <dependencies>
        <!--<dependency>-->
            <!--<groupId>org.apache.shiro.samples</groupId>-->
            <!--<artifactId>samples-spring-client</artifactId>-->
        <!--</dependency>-->
        <dependency>
            <groupId>org.apache.shiro</groupId>
            <artifactId>shiro-core</artifactId>
            <version>${shiro.previousVersion}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.shiro</groupId>
            <artifactId>shiro-ehcache</artifactId>
            <version>${shiro.previousVersion}</version>
        </dependency>
        <dependency>
            <groupId>net.sf.ehcache</groupId>
            <artifactId>ehcache-core</artifactId>
            <version>${ehcache.version}</version>
            <optional>false</optional>
        </dependency>
        <dependency>
            <groupId>org.apache.shiro</groupId>
            <artifactId>shiro-spring</artifactId>
            <version>${shiro.previousVersion}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.shiro</groupId>
            <artifactId>shiro-web</artifactId>
            <version>${shiro.previousVersion}</version>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>3.1.0</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>${slf4j.version}</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>jcl-over-slf4j</artifactId>
            <version>${slf4j.version}</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>${log4j.version}</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>hsqldb</groupId>
            <artifactId>hsqldb</artifactId>
            <version>${hsqldb.version}</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jstl</artifactId>
            <version>1.2</version>
            <scope>runtime</scope>
        </dependency>
    </dependencies>
```

整合 Shiro 之前，先搭建 Spring/SpringMVC 环境，此步省略

在此基础上开始整合 shiro

1）在 web.xml 添加 shiro 过滤器

```xml
    <!--
    DelegatingFilterProxy 实际上是 Filter 的一个代理对象. 默认情况下, Spring 会到 IOC 容器中查找和 
	<filter-name> 对应的 filter bean. 也可以通过 targetBeanName 的初始化参数来配置 filter bean 的 id.
    -->
    <filter>
        <filter-name>shiroFilter</filter-name>
        <filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
        <init-param>
            <param-name>targetFilterLifecycle</param-name>
            <param-value>true</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>shiroFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
```

2）配置 applicationContext.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd">
    <!--1. 配置 SecurityManager!-->
    <bean id="securityManager" class="org.apache.shiro.web.mgt.DefaultWebSecurityManager">
        <property name="cacheManager" ref="cacheManager"/>
        <property name="authenticator" ref="authenticator"></property>

        <property name="realms">
            <list>
                <ref bean="jdbcRealm"/>
                <ref bean="secondRealm"/>
            </list>
        </property>

        <!--<property name="rememberMeManager.cookie.maxAge" value="10"></property>-->
    </bean>

    <!--2. 配置 CacheManager.-->
    <!--2.1 需要加入 ehcache 的 jar 包及配置文件.-->
    <bean id="cacheManager" class="org.apache.shiro.cache.ehcache.EhCacheManager">
        <property name="cacheManagerConfigFile" value="classpath:ehcache.xml"/>
    </bean>

    <bean id="authenticator"
          class="org.apache.shiro.authc.pam.ModularRealmAuthenticator">
        <property name="authenticationStrategy">
            <bean class="org.apache.shiro.authc.pam.AtLeastOneSuccessfulStrategy"></bean>
        </property>
    </bean>

    	<!--3. 配置 Realm-->
    	<!--3.1 直接配置实现了 org.apache.shiro.realm.Realm 接口的 bean-->
    <bean id="jdbcRealm" class="com.demo.shiro.realm.ShiroRealm">
        <property name="credentialsMatcher">
            <bean class="org.apache.shiro.authc.credential.HashedCredentialsMatcher">
                <property name="hashAlgorithmName" value="MD5"></property>
                <property name="hashIterations" value="1024"></property>
            </bean>
        </property>
    </bean>

    <bean id="secondRealm" class="com.demo.shiro.realm.SecondRealm">
        <property name="credentialsMatcher">
            <bean class="org.apache.shiro.authc.credential.HashedCredentialsMatcher">
                <property name="hashAlgorithmName" value="SHA1"></property>
                <property name="hashIterations" value="1024"></property>
            </bean>
        </property>
    </bean>

    <!--4. 配置 LifecycleBeanPostProcessor. 可以自定的来调用配置在 Spring IOC 容器中 shiro bean 的生命周期方法.-->
    <bean id="lifecycleBeanPostProcessor" class="org.apache.shiro.spring.LifecycleBeanPostProcessor"/>

    <!--5. 启用 IOC 容器中使用 shiro 的注解. 但必须在配置了 LifecycleBeanPostProcessor 之后才可以使用.-->
    <bean class="org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator"
          depends-on="lifecycleBeanPostProcessor"/>
    <bean class="org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor">
        <property name="securityManager" ref="securityManager"/>
    </bean>

    <!--6. 配置 ShiroFilter.-->
    <!--
    
    6.1 id 必须和 web.xml 文件中配置的 DelegatingFilterProxy 的 <filter-name> 一致.
    若不一致, 则会抛出: NoSuchBeanDefinitionException. 
    因为 Shiro 会来 IOC 容器中查找和 <filter-name> 名字对应的 filter bean.
    
    -->
    <bean id="shiroFilter" class="org.apache.shiro.spring.web.ShiroFilterFactoryBean">
        <property name="securityManager" ref="securityManager"/>
        <property name="loginUrl" value="/login.jsp"/>
        <property name="successUrl" value="/list.jsp"/>
        <property name="unauthorizedUrl" value="/unauthorized.jsp"/>

        <property name="filterChainDefinitionMap" ref="filterChainDefinitionMap"></property>

        <!--
        	配置哪些页面需要受保护.
        	以及访问这些页面需要的权限.
        	1). anon 可以被匿名访问
        	2). authc 必须认证(即登录)后才可能访问的页面.
        	3). logout 登出.
        	4). roles 角色过滤器
        -->
        <!--
        <property name="filterChainDefinitions">
            <value>
                /login.jsp = anon
                /shiro/login = anon
                /shiro/logout = logout

                /user.jsp = roles[user]
                /admin.jsp = roles[admin]

                # everything else requires authentication:
                /** = authc
            </value>
        </property>
        -->
    </bean>

    <!-- 配置一个 bean, 该 bean 实际上是一个 Map. 通过实例工厂方法的方式 -->
    <bean id="filterChainDefinitionMap"
          factory-bean="filterChainDefinitionMapBuilder" factory-method="buildFilterChainDefinitionMap"></bean>

    <bean id="filterChainDefinitionMapBuilder"
          class="com.demo.shiro.factory.FilterChainDefinitionMapBuilder"></bean>

    <bean id="shiroService"
          class="com.demo.shiro.services.ShiroService"></bean>
</beans>
```

注意点：<bean id="shiroFilter" class="org.apache.shiro.spring.web.ShiroFilterFactoryBean">的名称（id）跟web.xml中的<filter-name>要一致。


## 多 realm 和 认证策略

为什么要多个 realm ？

AuthenticationStrategy接口的默认实现：
- FirstSuccessfulStrategy：只要有一个Realm 验证成功即可，只返回第一个Realm 身份验证成功的认证信息，其他的忽略；
- AtLeastOneSuccessfulStrategy：只要有一个Realm验证成功即可，和FirstSuccessfulStrategy不同，将返回所有Realm身份验证成功的认证信息；
- AllSuccessfulStrategy：所有Realm验证成功才算成功，且返回所有Realm身份验证成功的认证信息，如果有一个失败就失败了。

ModularRealmAuthenticator默认是AtLeastOneSuccessfulStrategy策略

参考：

```xml
    <!--1. 配置 SecurityManager!-->
    <bean id="securityManager" class="org.apache.shiro.web.mgt.DefaultWebSecurityManager">
        <property name="cacheManager" ref="cacheManager"/>
        <property name="authenticator" ref="authenticator"></property>

        <property name="realms">
            <list>
                <ref bean="jdbcRealm"/>
                <ref bean="secondRealm"/>
            </list>
        </property>
    </bean>

    <!--认证策略-->
    <bean id="authenticator"
          class="org.apache.shiro.authc.pam.ModularRealmAuthenticator">
        <property name="authenticationStrategy">
            <bean class="org.apache.shiro.authc.pam.AtLeastOneSuccessfulStrategy"></bean>
        </property>
    </bean>
    
    <bean id="jdbcRealm" class="com.demo.shiro.realm.ShiroRealm">
        <property name="credentialsMatcher">
            <bean class="org.apache.shiro.authc.credential.HashedCredentialsMatcher">
                <property name="hashAlgorithmName" value="MD5"></property>
                <property name="hashIterations" value="1024"></property>
            </bean>
        </property>
    </bean>

    <bean id="secondRealm" class="com.demo.shiro.realm.SecondRealm">
        <property name="credentialsMatcher">
            <bean class="org.apache.shiro.authc.credential.HashedCredentialsMatcher">
                <property name="hashAlgorithmName" value="SHA1"></property>
                <property name="hashIterations" value="1024"></property>
            </bean>
        </property>
    </bean>
```

realms 属性建议配置给 securityManager，因为授权时用到。

## 权限注解

- @RequiresAuthentication：表示当前Subject已经通过login 进行了身份验证；即Subject. isAuthenticated() 返回true
- @RequiresUser：表示当前Subject 已经身份验证或者通过记住我登录的。
- @RequiresGuest：表示当前Subject没有身份验证或通过记住我登录过，即是游客身份。
- @RequiresRoles(value={“admin”, “user”}, logical= Logical.AND)：表示当前Subject 需要角色admin 和user
- @RequiresPermissions(value={“user:a”, “user:b”}, logical= Logical.OR)：表示当前Subject 需要权限user:a或user:b。

注解可以在 controller层、service层，但当service层有事务时就不要注解在servie层，不能让service是代理的代理。

## 从数据表中查出资源权限

目的：不用xml配置方式

```xml
    <bean id="shiroFilter" class="org.apache.shiro.spring.web.ShiroFilterFactoryBean">
        <property name="securityManager" ref="securityManager"/>
        <property name="loginUrl" value="/login.jsp"/>
        <property name="successUrl" value="/list.jsp"/>
        <property name="unauthorizedUrl" value="/unauthorized.jsp"/>
        <property name="filterChainDefinitionMap" ref="filterChainDefinitionMap"></property>
        <!--
        <property name="filterChainDefinitions">
            <value>
                /login.jsp = anon
                /shiro/login = anon
                /shiro/logout = logout

                /user.jsp = roles[user]
                /admin.jsp = roles[admin]

                # everything else requires authentication:
                /** = authc
            </value>
        </property>
        -->
    </bean>

    <!-- 配置一个 bean, 该 bean 实际上是一个 Map. 通过实例工厂方法的方式 -->
    <bean id="filterChainDefinitionMap"
          factory-bean="filterChainDefinitionMapBuilder" factory-method="buildFilterChainDefinitionMap"></bean>

    <bean id="filterChainDefinitionMapBuilder"
          class="com.demo.shiro.factory.FilterChainDefinitionMapBuilder"></bean>
```

```java
/**
 * 从数据表中查出资源权限（这里是用静态模拟数据）
 *
 * 注意配置的顺序，因为有第一位置优先原则 —— 20190528
 */
public class FilterChainDefinitionMapBuilder {
    public LinkedHashMap<String, String> buildFilterChainDefinitionMap(){
        LinkedHashMap<String, String> map = new LinkedHashMap<>();

        map.put("/login.jsp", "anon");
        map.put("/shiro/login", "anon");
        map.put("/shiro/logout", "logout");
        map.put("/user.jsp", "authc,roles[user]");
        map.put("/admin.jsp", "authc,roles[admin]");
        map.put("/list.jsp", "user");

        map.put("/**", "authc");

        return map;
    }
}
```

## 在realm中跟数据库交互

1）在配置realm时增加dao属性

```xml
	<!--自定义realm-->
	<bean id="userRealm" class="cn.wolfcode.shiro.realm.UserRealm">
		<property name="userDAO" ref="userDAOImpl"/>
	</bean>
```

2）在自定义的realm中增加dao的set方法

```java
public class UserRealm extends AuthorizingRealm {

    private IUserDAO userDAO;

    public void setUserDAO(IUserDAO userDAO) {
        this.userDAO = userDAO;
    }

    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

        return null;
    }

    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

        String username = (String) token.getPrincipal();

        //从数据库查用户信息
        User user = userDAO.getUserByUsername(username);

        if (user == null) {
            return null;
        }

        //第一个参数是 user，不是username ——20190529
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(user, user.getPassword(), getName());

        return info;
    }

    @Override
    public String getName() {
        return "UserRealm";
    }
}
```

## 静态授权

1）修改spring-shiro.xml配置文件

```xml
	<!-- 开启aop，对类代理 -->
	<aop:config proxy-target-class="true"></aop:config>
	<!-- 开启shiro注解支持 -->
	<bean class="org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor">
		<property name="securityManager" ref="securityManager" />
	</bean>
	<!-- 定义需要特殊处理的异常，用类名或完全路径名作为key，异常页名作为值 -->
	<!--shiro权限异常处理-->
	<bean class="org.springframework.web.servlet.handler.SimpleMappingExceptionResolver">
		<property name="exceptionMappings">
			<props>
				<prop key="org.apache.shiro.authz.UnauthorizedException">redirect:/nopermission.jsp</prop>
			</props>
		</property>
	</bean>
```

2）controller方法上添加权限注解

```java
@Controller
@RequestMapping("/employee")
public class EmployeeController {
    @RequestMapping("")
    public String index() throws  Exception{
        System.out.println("执行了员工列表....");
        return "employee";
    }
    @RequestMapping("/save")
    @RequiresPermissions("employee:save")
    public String save() throws  Exception{
        System.out.println("执行了员工保存....");
        return "employee";
    }

    @RequestMapping("/edit")
    @RequiresPermissions("employee:edit")
    public String edit() throws  Exception{
        System.out.println("执行了员工编辑....");
        return "employee";
    }

    @RequestMapping("/delete")
    @RequiresPermissions("employee:delete")
    public String delete() throws  Exception{
        System.out.println("执行了员工删除....");
        return "employee";
    }
}
```

3）在自定义realm中添加权限

```java
    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

        User user = (User) principals.getPrimaryPrincipal();

        ArrayList<String> permissionList = new ArrayList<>();

        //静态授权
        if ("zhangsan".equals(user.getUsername())) {
            permissionList.add("employee:edit");
        } else if ("admin".equals(user.getUsername())) {
            permissionList.add("*:*");
        }

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.addStringPermissions(permissionList);

        return info;
    }
```


## 资源授权（动态）

user/role/permission，它们之间的多对多关系

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708090640.png)

对应的表

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708090656.png)


### 加载权限表达式到数据库

1）在controller方法上添加两个标签：@RequiresPermissions、@PermissionName

```java
    @RequestMapping("/save")
    @RequiresPermissions("employee:save")
    @PermissionName("员工保存")
    public String save() throws  Exception{
        System.out.println("执行了员工保存....");
        return "employee";
    }
```

其中 @PermissionName 是自定义的注解

```java
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface PermissionName {
    String value();
}
```

2）添加加载权限表达式的controller

```java
@Controller
public class PermissionController {

    //springmvc在启动时将所有标记有@RequestMapping的方法收集起来并封装到此对象
    @Autowired
    private RequestMappingHandlerMapping rmhm;

    @Autowired
    private IPermissionDAO permissionDAO;

    //将系统中所有的权限添加到数据库中
    @RequestMapping("/reload")
    public String reload() throws  Exception{
        //0、从数据库中查出所有权限表达式，然后对比，如果已经存在则跳过，如果不存在则添加
        List<String> resourcesList = permissionDAO.getAllResources();

        //1、获取所有标记有@RequestMapping的方法
        Map<RequestMappingInfo, HandlerMethod> handlerMethods = rmhm.getHandlerMethods();
        Collection<HandlerMethod> methods = handlerMethods.values();
        //2、遍历所有方法，判断是否有@RequiresPermissions标签
        for (HandlerMethod method : methods) {
            RequiresPermissions annotation = method.getMethodAnnotation(RequiresPermissions.class);
            if (annotation != null) {
                String permission = annotation.value()[0];//假设只有一个权限表达式

                //如果权限表达式已经存在，则跳过（不重复添加）
                if (resourcesList.contains(permission)) {
                    continue;
                }

                //3、如果有，解析权限表达式，封装成permission对象保存到permission表中
                Permission p = new Permission();
                p.setResource(permission);
                p.setName(method.getMethodAnnotation(PermissionName.class).value());

                //保存到数据表中
                permissionDAO.save(p);
            }
        }
        return "main";
    }
}
```

看点：RequestMappingHandlerMapping、IPermissionDAO

3）请求 /reload 方法就可以动态加载权限表达式到数据库permission表中

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708090727.png)

参考自己的代码：https://github.com/wolf-lea/shiro-v1.4.1-demo/tree/master/shiro-wolfcode-mvc

### 角色权限

上面只是做了资源权限，跟角色还没有搭上关系，需要手动添加关系，或开发相关的功能来完善。


## 动态授权——数据库

```java
public class UserRealm extends AuthorizingRealm {
    @Setter
    private IUserDAO userDAO;
    @Setter
    private IRoleDAO roleDAO;
    @Setter
    private IPermissionDAO permissionDAO;

    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

        User user = (User) principals.getPrimaryPrincipal();

        List<String> permissionList = new ArrayList<>();
        List<String> roleList = new ArrayList<>();

        //静态授权
//        if ("zhangsan".equals(user.getUsername())) {
//            permissionList.add("employee:edit");
//        } else if ("admin".equals(user.getUsername())) {
//            permissionList.add("*:*");
//        }

        //动态授权
        if ("admin".equals(user.getUsername)) {
            //拥有所有权限
            permissionList.add("*:*");
            //拥有所有角色
            roleList = roleDAO.getAllRoleSn();
        } else {
            //根据用户id查询角色
            roleList = roleDAO.getRoleSnByUserId(user.getId());
            //根据用户id查询资源权限
            permissionList = permissionDAO.getPermissionResourceByUserId(user.getId());
        }

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.addRoles(roleList);
        info.addStringPermissions(permissionList);
        return info;
    }

    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

        String username = (String) token.getPrincipal();

        //从数据库查用户信息
        User user = userDAO.getUserByUsername(username);

        if (user == null) {
            return null;
        }

        //第一个参数是 user，不是username ——20190529
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(user, user.getPassword(), getName());

        return info;
    }

    @Override
    public String getName() {
        return "UserRealm";
    }
}
```

记得在spring-shiro.xml中注入依赖

```xml
	<!--自定义realm-->
	<bean id="userRealm" class="cn.wolfcode.shiro.realm.UserRealm">
		<property name="userDAO" ref="userDAOImpl"/>
		<property name="roleDAO" ref="roleDAOImpl"/>
		<property name="permissionDAO" ref="permissionDAOImpl"/>
	</bean>
```

跟 静态授权 对比，角色和权限不是写死的，是从数据库是查出的


## 对密码加密

1）在 spring-shiro.xml 中添加 credentialsMatcher

```xml
	<!--密码加密算法-->
	<bean id="credentialsMatcher"
		  class="org.apache.shiro.authc.credential.HashedCredentialsMatcher">
		<property name="hashAlgorithmName" value="md5" />
		<property name="hashIterations" value="3" />
	</bean>
```

记得在自定义的realm的bean中引入上面的credentialsMatcher

```xml
	<!--自定义realm-->
	<bean id="userRealm" class="cn.wolfcode.shiro.realm.UserRealm">
		<property name="userDAO" ref="userDAOImpl"/>
		<property name="roleDAO" ref="roleDAOImpl"/>
		<property name="permissionDAO" ref="permissionDAOImpl"/>
		<property name="credentialsMatcher" ref="credentialsMatcher"/>
	</bean>
```

2）在认证方法中添加盐值（如果需要）

3）数据库中的密码是加密后的密文，可以使用 MD5Test 对密码事先修改为密文


## 缓存管理

1）依赖

```xml
<dependency>
	<groupId>org.apache.shiro</groupId>
	<artifactId>shiro-ehcache</artifactId>
	<version>1.2.2</version>
</dependency>
<dependency>
		<groupId>net.sf.ehcache</groupId>
		<artifactId>ehcache-core</artifactId>
		<version>2.6.8</version>
</dependency>
```

2）修改spring-shiro.xml

添加配置：

```xml
	<!--如果导入的ehcache版本在2.5.0以上,需要配置如下.-->
	<!-- 缓存管理器开始 -->
	<bean id="cacheManager" class="org.apache.shiro.cache.ehcache.EhCacheManager">
		<property name="cacheManager" ref="ehCacheManager"/>
	</bean>
	<bean id="ehCacheManager" class ="org.springframework.cache.ehcache.EhCacheManagerFactoryBean">
		<property name="configLocation" value="classpath:shiro-ehcache.xml" />
		<property name="shared" value="true"></property>
	</bean> 
```

在securityManager中添加cacheManager

```
<!-- 安全管理器 -->
<bean id="securityManager" class="org.apache.shiro.web.mgt.DefaultWebSecurityManager">
	<property name="realm" ref="myRealm"></property>
	<property name="cacheManager" ref="cacheManager"></property>
</bean>
```

3）shiro-ehcache.xml配置文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ehcache>
    <defaultCache
            maxElementsInMemory="1000"
            eternal="false"
            timeToIdleSeconds="120"
            timeToLiveSeconds="120"
            memoryStoreEvictionPolicy="LRU">
    </defaultCache>
</ehcache>
```

清空缓存：

- 如果用户正常退出，缓存自动清空。
- 如果用户非正常退出，缓存自动清空。
- 如果修改了用户的权限，而用户不退出系统，修改的权限无法立即生效。

当用户权限修改后，用户再次登陆shiro会自动调用realm从数据库获取权限数据，如果在修改权限后想立即清除缓存则可以调用realm的clearCache方法清除缓存。

在realm中定义该方法:

```java
//清除缓存
public void clearCached() {
     PrincipalCollection principals = SecurityUtils.getSubject().getPrincipals();
     super.clearCache(principals);
}
```

在角色或权限service中,delete或者update方法去调用realm的清除缓存方法。注意：是手动调用。


## 会话管理

Shiro提供了完整的企业级会话管理功能，不依赖于底层容器（如web容器tomcat），不管JavaSE还是JavaEE环境都可以使用，提供了会话管理、会话事件监听、会话存储/持久化、容器无关的集群、失效/过期支持、对Web 的透明支持、SSO 单点登录的支持等特性。

会话相关的API：

- Subject.getSession()：即可获取会话；其等价于Subject.getSession(true)，即如果当前没有创建Session 对象会创建一个；Subject.getSession(false)，如果当前没有创建Session 则返回null
- session.getId()：获取当前会话的唯一标识
- session.getHost()：获取当前Subject的主机地址
- session.getTimeout() & session.setTimeout(毫秒)：获取/设置当前Session的过期时间
- session.getStartTimestamp() & session.getLastAccessTime()：获取会话的启动时间及最后访问时间；如果是JavaSE应用需要自己定期调用session.touch() 去更新最后访问时间；如果是Web 应用，每次进入ShiroFilter都会自动调用session.touch() 来更新最后访问时间。
- session.touch() & session.stop()：更新会话最后访问时间及销毁会话；当Subject.logout()时会自动调用stop 方法来销毁会话。如果在web中，调用HttpSession. invalidate() 也会自动调用ShiroSession.stop方法进行销毁Shiro的会话
- session.setAttribute(key, val) & session.getAttribute(key) & session.removeAttribute(key)：设置/获取/删除会话属性；在整个会话范围内都可以对这些属性进行操作

Shiro提供的session可以访问到web中session域的数据：

```java
public class ShiroService {
    @RequiresRoles({"admin"})
    public void testMethod(){
        System.out.println("testMethod, time: " + new Date());

        Session session = SecurityUtils.getSubject().getSession();
        Object val = session.getAttribute("key");

        System.out.println("Service SessionVal: " + val);
    }
}
```
也就是可以在service层访问session域数据，开发时可能用到。

# FQA

## Shiro内置Realm之JdbcRealm

https://www.jianshu.com/p/c2ca70edfae6

## Shiro缓存使用Redis、Ehcache、自带的MpCache实现的三种方式实例

https://www.cnblogs.com/zfding/p/8536480.html

