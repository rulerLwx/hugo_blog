---
title: "Java-framework-SpringCloud-3"
date: 2019-08-25T11:19:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]

---


# 概述

子模块介绍：
- microservicecloud-api API公共模块，如：公共实体类
- microservicecloud-consumer-dept-80
- microservicecloud-eureka-7001 服务注册中心
- microservicecloud-eureka-7002
- microservicecloud-eureka-7003
- microservicecloud-provider-dept-8001 服务提供者
- microservicecloud-provider-dept-8002
- microservicecloud-provider-dept-8003


# Rest 客户端

```java
    /**
     * 使用 使用restTemplate访问restful接口非常的简单粗暴无脑。 (url, requestMap,
     * ResponseBean.class)这三个参数分别代表 REST请求地址、请求参数、HTTP响应转换被转换成的对象类型。
     */
    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping(value = "/consumer/dept/add")
    public boolean add(Dept dept)
    {
        return restTemplate.postForObject(REST_URL_PREFIX + "/dept/add", dept, Boolean.class);
    }
```

# Eureka 服务中心

1）新建 microservicecloud-eureka-7001 模块

2）maven 导入依赖

```xml
        <!--eureka-server服务端 -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-eureka-server</artifactId>
        </dependency>
```

3）yml 配置文件

```yml
server: 
  port: 7001
 
eureka: 
  instance:
    hostname: eureka7001.com #eureka服务端的实例名称
  client: 
    register-with-eureka: false     #false表示不向注册中心注册自己。
    fetch-registry: false     #false表示自己端就是注册中心，我的职责就是维护服务实例，并不需要去检索服务
    service-url: 
      #单机 defaultZone: http://${eureka.instance.hostname}:${server.port}/eureka/       #设置与Eureka Server交互的地址查询服务和注册服务都需要依赖这个地址（单机）。
      defaultZone: http://eureka7002.com:7002/eureka/,http://eureka7003.com:7003/eureka/
```

4）入口类开启 eureka 服务

```java
@SpringBootApplication
@EnableEurekaServer // EurekaServer服务器端启动类,接受其它微服务注册进来
public class EurekaServer7001_App
{
	public static void main(String[] args)
	{
		SpringApplication.run(EurekaServer7001_App.class, args);
	}
}
```


# 修改 microservicecloud-provider-dept-8001

让 microservicecloud-provider-dept-8001 作为 eureka 客户端，注册到上面的7001中


# 微服务信息完善

以下配置是优化微服务，显示实例名、IP、相关服务信息：

1）添加依赖包：

```xml
    <!-- actuator监控信息完善 -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>
```

2）完善`microservicecloud-provider-dept-8001`的application.yml配置

```properties
eureka:
  client: #客户端注册进eureka服务列表内
    service-url:
      #defaultZone: http://localhost:7001/eureka
      defaultZone: http://eureka7001.com:7001/eureka/,http://eureka7002.com:7002/eureka/,http://eureka7003.com:7003/eureka/
# 以下信息是优化微服务，显示实例名、IP、相关服务信息
  instance:
    instance-id: microservicecloud-dept8001
    prefer-ip-address: true     #访问路径可以显示IP地址
info:
  app.name: atguigu-microservicecloud
  company.name: www.atguigu.com
  build.artifactId: $project.artifactId$
  build.version: $project.version$
```

3）在总的父工程pom中添加以下构建信息：

```xml
<build>
   <finalName>microservicecloud</finalName>
   <resources>
     <resource>
       <directory>src/main/resources</directory>
       <filtering>true</filtering>
     </resource>
   </resources>
   <plugins>
     <plugin>
       <groupId>org.apache.maven.plugins</groupId>
       <artifactId>maven-resources-plugin</artifactId>
       <configuration>
         <delimiters>
          <delimit>$</delimit>
         </delimiters>
       </configuration>
     </plugin>
   </plugins>
  </build>
```

图解：

![image](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708113552.png)


# Eureka集群

新建`microservicecloud-eureka-7002`和`microservicecloud-eureka-7003`模块

1）依赖

参考`microservicecloud-eureka-7001`的pom

2）修改7002、7003启动类开启注册服务

3）修改7002、7003的yml配置文件

```yml
server: 
  port: 7002
 
eureka: 
  instance:
    hostname: eureka7002.com #eureka服务端的实例名称
  client: 
    register-with-eureka: false     #false表示不向注册中心注册自己。
    fetch-registry: false     #false表示自己端就是注册中心，我的职责就是维护服务实例，并不需要去检索服务
    service-url:
      #设置与Eureka Server交互的地址，查询服务和注册服务都需要依赖这个地址（单机）。
      #defaultZone: http://${eureka.instance.hostname}:${server.port}/eureka/
      defaultZone: http://eureka7001.com:7001/eureka/,http://eureka7003.com:7003/eureka/
```

4）修改host文件

```
127.0.0.1 eureka7001.com
127.0.0.1 eureka7002.com
127.0.0.1 eureka7003.com
```

5）修改其它微服务提供者、消费者的yml，使其指向上面的3个服务注册中心

```yml
eureka:
  client: #客户端注册进eureka服务列表内
    service-url:
      #defaultZone: http://localhost:7001/eureka
      defaultZone: http://eureka7001.com:7001/eureka/,http://eureka7002.com:7002/eureka/,http://eureka7003.com:7003/eureka/

```


程序员级别：

码神》码龙》码牛》码工》码农》码畜》码渣

p3-p9



# Ribbon负载均衡

## 概述

Spring Cloud Ribbon是基于Netflix Ribbon实现的一套**客户端**负载均衡的工具

LB，即负载均衡(Load Balance)，在微服务或分布式集群中经常用的一种应用。

负载均衡简单的说就是将用户的请求平摊的分配到多个服务上，从而达到系统的HA。

常见的负载均衡有软件Nginx，LVS，硬件F5等。

官网：https://github.com/Netflix/ribbon


## 入门——改造`microservicecloud-consumer-dept-80`模块

1）pom

```xml
    <!-- Ribbon相关 -->
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-eureka</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-ribbon</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-config</artifactId>
    </dependency>
```

2）@LoadBalanced

在配置类的bean上使用`@LoadBalanced`注解

因为，Spring Cloud Ribbon是基于Netflix Ribbon实现的一套**客户端**负载均衡的工具

```java
@Configuration
public class ConfigBean {
    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate()
    {
        return new RestTemplate();
    }
}
```

3）启动类

```java
@SpringBootApplication
@EnableEurekaClient
//在启动该微服务的时候就能去加载我们的自定义Ribbon配置类，从而使配置生效
//@RibbonClient(name="MICROSERVICECLOUD-DEPT",configuration=MySelfRule.class)
//@RibbonClient(name="MICROSERVICECLOUD-DEPT",configuration=MySelfRule.class)
public class DeptConsumer80_App
{
	public static void main(String[] args)
	{
		SpringApplication.run(DeptConsumer80_App.class, args);
	}
}
```

4）控制层根据微服务实例名称调用

```java
@RestController
public class DeptController_Consumer {
    //private static final String REST_URL_PREFIX = "http://localhost:8001";
    private static final String REST_URL_PREFIX = "http://MICROSERVICECLOUD-DEPT";
    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping(value = "/consumer/dept/list")
    public List<Dept> list()
    {
        return restTemplate.getForObject(REST_URL_PREFIX + "/dept/list", List.class);
    }
```

说明：微服务实例名称指的是yml的`spring:application:name: microservicecloud-dept`，也就是下面的服务名

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708142229.png)

5）测试

启动以下服务：
- microservicecloud-eureka-7001
- microservicecloud-eureka-7002
- microservicecloud-provider-dept-8001
- microservicecloud-consumer-dept-80

浏览器访问：http://localhost/consumer/dept/list

6）总结

使用微服务实例名访问就不用记住服务的地址、端口


## 负载均衡

![image](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708113751.png)

1）基于8001新建模块

- microservicecloud-provider-dept-8002
- microservicecloud-provider-dept-8003


2）新建8002、8003对应的数据库

```sql
DROP DATABASE IF EXISTS cloudDB02;
 
CREATE DATABASE cloudDB02 CHARACTER SET UTF8;
 
USE cloudDB02;
 
CREATE TABLE dept
(
  deptno BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  dname VARCHAR(60),
  db_source   VARCHAR(60)
);
 
INSERT INTO dept(dname,db_source) VALUES('开发部',DATABASE());
INSERT INTO dept(dname,db_source) VALUES('人事部',DATABASE());
INSERT INTO dept(dname,db_source) VALUES('财务部',DATABASE());
INSERT INTO dept(dname,db_source) VALUES('市场部',DATABASE());
INSERT INTO dept(dname,db_source) VALUES('运维部',DATABASE());
 
SELECT * FROM dept;
```

```sql
DROP DATABASE IF EXISTS cloudDB03;
 
CREATE DATABASE cloudDB03 CHARACTER SET UTF8;
 
USE cloudDB03;
 
CREATE TABLE dept
(
  deptno BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  dname VARCHAR(60),
  db_source   VARCHAR(60)
);
 
INSERT INTO dept(dname,db_source) VALUES('开发部',DATABASE());
INSERT INTO dept(dname,db_source) VALUES('人事部',DATABASE());
INSERT INTO dept(dname,db_source) VALUES('财务部',DATABASE());
INSERT INTO dept(dname,db_source) VALUES('市场部',DATABASE());
INSERT INTO dept(dname,db_source) VALUES('运维部',DATABASE());
 
SELECT * FROM dept;
```

3）pom

8002、8003跟8001保持一致

4）yml

备忘

- 端口：8002、8003
- 数据库链接：jdbc:mysql://localhost:3306/cloudDB01 或 cloudDB02 或 cloudDB03
- 对外暴露的统一的服务实例名`microservicecloud-dept`

![image](77766EDE8F9543F7B85D86E7122BD43A)

5）测试

启动
- microservicecloud-eureka-7001
- microservicecloud-eureka-7002
- microservicecloud-provider-dept-8001
- microservicecloud-provider-dept-8002

> 由于只有8G内存，只启动了以上4个

测试8001、8002是否可以访问：
- http://localhost:8001/dept/list
- http://localhost:8002/dept/list

最后，启动消费者：`microservicecloud-consumer-dept-80`，测试消费接口：http://localhost/consumer/dept/list

可以看到数据会轮流从cloudDB01、cloudDB02库中获取


6）总结

Ribbon负载均衡**默认采用轮询模式**（轮流来）

自我总结：三个服务提供者的实例名相同，都是`microservicecloud-dept`，但各自对应的数据库不同（cloudDB01、cloudDB02、cloudDB03），我认为是为了测试用，不具有真实项目作用 —— 20191116


# Ribbon的负载均衡策略

Ribbon的负载均衡的核心是IRule，IRule提供以下策略：

![IRule](https://oscimg.oschina.net/oscnet/up-e6e347d5bb2483af9678ea774e7f7ad70b7.png)

如何使用以上策略？

```java
@Configuration
public class ConfigBean {

    // 指定策略
    @Bean
    public IRule myRule()
    {
        //return new RoundRobinRule();
        //return new RandomRule();//达到的目的，用我们重新选择的随机算法替代默认的轮询。
        return new RetryRule();
    }
}
```


# 自定义Ribbon——IRule策略

步骤：

1）在主启动类添加`@RibbonClient`注解，指定自定义Ribbon的配置类

```java
@SpringBootApplication
@EnableEurekaClient
//在启动该微服务的时候就能去加载我们的自定义Ribbon配置类，从而使配置生效
@RibbonClient(name="MICROSERVICECLOUD-DEPT",configuration=MySelfRule.class)
public class DeptConsumer80_App
{
	public static void main(String[] args)
	{
		SpringApplication.run(DeptConsumer80_App.class, args);
	}
}
```

2）新建MySelfRule类

注意：MySelfRule类**不能跟启动类放在同一个包及子包**中（官方文档：https://cloud.spring.io/spring-cloud-static/Greenwich.SR4/single/spring-cloud.html#_customizing_the_ribbon_client）

总结：MySelfRule配置类中的@Bean要继承AbstractLoadBalancerRule类
