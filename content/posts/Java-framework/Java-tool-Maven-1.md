---
title: "Java-tool-Maven-1"
date: 2019-08-03T11:01:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---

# Maven最佳配置

- 将M2_HOME/config/setting.xml复制到~/.m2/setting.xml（最佳实战）  
- 设置MAVEN_OPTS环境变量，值为 -Xms128m -Xmx512m ，（因生成项目站点时需要大量的内存）
- 不使用IDE内嵌的Maven

# 配置依赖

## xml标签——基本概念

- groupId
- artifactId
- version
- type，依赖类型（jar、war、pom、bundle），默认为jar。
- scope，依赖范围，默认compile。
- optional，标记依赖是否可选。
- exclusions，用来排除传递性依赖。

下图是依赖范围（scope）：
![dependent-scope](https://oscimg.oschina.net/oscnet/95b5baa6a88baff55ac75cf7d88bbb5f27f.jpg "dependent-scope")

![maven的传递性依赖](https://oscimg.oschina.net/oscnet/7b31f892a114a2b4af267394ad7e89208b5.jpg "maven的传递性依赖")

## 依赖最佳实践

- 排除不稳定的依赖，使用`exclusions`元素，如下

```xml
    <dependency>
        <groupId>xxxx</groupId>
        <artifactId>xxx</artifactId>
        <version>0.9.1.2</version>
        <exclusions>
            <exclusion>
                <groupId></groupId>
                <artifactId></artifactId>
            </exclusion>
        </exclusions>
    </dependency>
```

- 归类依赖

也就是统一定义版本

```xml
    <properties>
        <mybatis.version>3.2.4</mybatis.version>
    </properties>
    <dependencies>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <version>${spring.version}</version>
        </dependency>
    </dependencies>
```

# 仓库：本地、远程、中央仓库

仓库分类：
![maven-repo-catalog](https://oscimg.oschina.net/oscnet/a5919292f7d5125db3c046e21d1b6bcc2bc.jpg "maven-repo-catalog")

- 本地仓库

本地仓库默认在C:\Users\lwx\.m2\repository（windows),/home/xxx/.m2/repository(linux)
可以在setting.xml文件中修改仓库位置：

```xml
<settings>
    <localRepository>D:\java\maven\repo</localRepository>
</settings>
```

- 远程仓库

经常用于配置公司的公用仓库

```xml
<project>
    ....
    <repositories>
        <repository>
            <id>xxx</id>
            <name>xxx</name>
            <url>http://xxx.xxx</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <layout>default</layout>
        </repository>
    </repositories>
</project>
```

仓库（repository子元素）可以配置一个或多个；

releases > enabled 表示是否开启仓库的发布版本下载支持；snapshots > enable 表示是否下载快照版本。

- 仓库搜索服务

    - Sonatype Nexus:[https://repository.sonatype.org][1]
    - MVNrepository:[http://mvnrepository.com/][2]
    - search.maven:[http://search.maven.org/][3]



# Maven生命周期：清理、初始化、编译、测试、打包、集成测试、验证、部署、站点

maven的生命周期包括：清理、初始化、编译、测试、打包、集成测试、验证、部署、站点。

maven拥有三套相互独立的生命周期，分别是：  

- clean，清理项目
    - pre-clean 执行一些清理前需要完成的工作  
    - clean 清理上一次构建生成的文件  
    - post-clean 执行一些清理后需要完成的工作  
- default，构建项目  
    - validate  
    - initialize  
    - generate-sources  
    - process-sources  
    - generate-resources
    - process-resources  
    - compile  编译主代码到主输出目录
    - process-class  
    - generate-test-sources  
    - process-tset-sources  
    - generate-test-resources  
    - process-test-resources  
    - test-complite  
    - process-test-classes  
    - test  执行测试用例
    - prepare-package  
    - package  创建项目jar/war包
    - pre-integration-test  
    - integration-test  
    - post-integration-test  
    - vrify  
    - install  将项目输出构件安装到本地仓库
    - deploy  将项目输出构件部署到远程仓库
- site，建立站点  
    - pre-site  
    - site  
    - post-site  
    - site-deploy  

生命周期对应的命令行：

```sh
$ mvn clean  
$ mvn test  
$ mvn clean install  
$ mvn clean deploy site-deploy
```

maven生命周期所做的事由插件完成。  


# 常用插件 code snippets

- 指定编译、打包的 JDK 版本

```xml
    <build>
        <plugins>
            <!--指定编译的Java版本-->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>
        </plugins>
    </build>
```

- 跳过所有测试

```xml
  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>2.5</version>
        <!--跳过所有的测试-->
        <configuration>
          <skipTests>true</skipTests>
        </configuration>
      </plugin>
    </plugins>
  </build>
```


# 聚合与继承  

## 聚合  

首先要明白聚合的目的是**分模块，方便快速构建应用**。  

聚合主要是`packaging`和`modules`元素：  

- packaging的打包方式一定要是pom（默认是jar）  
```
<packaging>pom</packaging>
```
- module是待聚合的项目的artifactId  
```
<modules>
    <module>SSM-DAO</module>
    <module>SSM-Mapper</module>
    <module>SSM-Template</module>
</modules>
```

>![maven-module](https://oscimg.oschina.net/oscnet/b2fd1c37d3846da77df003900c8f7e2d269.jpg "maven-module")

聚合后，只需运行最外边的pom就可以构建整个项目了。  

## 继承  

首先要明白，继承的目的是为了**简化配置、共用配置**。使用继承的步骤和注意点如下：  

- 父pom的`packaging`打包方式必须是pom  
- 父模块只是为了消除配置的重复，因此它本身不包含除pom之处的项目文件，也就是不需要src/main/java之类的文件  
- 子模块中使用`parent`元素继承父pom  
```
<parent>
    <artifactId>MybatisTest</artifactId>
    <groupId>com.demo</groupId>
    <version>1.0-SNAPSHOT</version>
    <relativePath>../parent-project/pom.xml</relativePath>
</parent>
```
注意：在`parent`元素中可以定义`relativePath`子元素来指定父pom的位置，如上所示。`relativePath`默认值是../pom.xml。  

- 子模块的**坐标**只需要定义`artifactId`,不用定义`groupId`和`version`  

完整的子模块定义示例如下：
```
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <parent>
        <artifactId>MybatisTest</artifactId>
        <groupId>com.demo</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>
    <artifactId>SSM-Test</artifactId>
    <packaging>war</packaging>
    <name>SSM-Test Maven Webapp</name>
    <url>http://maven.apache.org</url>
    <dependencies>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>3.1.0</version>
        </dependency>
    </dependencies>
    <build>
        <finalName>SSM-Test</finalName>
    </build>
</project>
```

### 可继承的pom元素  
- groupId  
- version  
- descrption，项目描述信息  
- organization：项目的组织信息  
- inceptionYear：项目他妈年份  
- url：项目url地址  
- developers：项目开发者信息  
- contributors：项目贡献者信息  
- distributionManagement：项目的部署配置  
- issueManagement：项目的缺陷跟踪系统信息  
- ciManagement：项目的持续集成系统信息  
- scm：项目的版本控制信息  
- mailingLists：项目的邮件列表信息  
- properties：自定义的maven属性  
- dependencies：项目依赖配置  
- dependencyManagement：项目依赖管理配置  
- repositories：项目的仓库配置  
- build：包括项目的源码目录配置、输入目录配置、插件配置、插件管理配置等  
- reporting：包括项目的输入目录配置、报告插件配置等

### 继承中的依赖管理

**问题**：上面使用`dependencies`元素时，子模块会完全继承父模块的一切可以继承的依赖，这样不太合理，因为并不是所有的子模块都完全需要父类的依赖。  
**解决办法**:父模块使用`dependencyManagement`元素来包裹`dependencies`元素，子模块在`dependencies`元素中声明要继承父模块的依赖，但不用指定版本，即只需定义`groupId`和`artifactId`，不用定义`version`。这样做的好处是版本统一。  

父pom的示例：  
```
<project >
    ...
    <properties>
        <springframework.version>2.5.6</springframework.version>
        <junit.version>4.7</junit.version>
    </properties>
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-core</artifactId>
                <version>${springframework.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-beans</artifactId>
                <version>${springframework.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>
</project>
```
子pom的示例：  
```
<project >
    ...
    <dependencies>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>        
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>           
        </dependency>
    </dependencies>
</project>
```

当然了，子模块完全可以定义自己的依赖。  

### 继承中的插件管理

**问题**：子模块和父模块需要同样的插件时，如何管理插件？  
**解决办法**:父模块使用`pluginManagement`元素来包裹`plugins`元素，子模块在`plugins`元素中声明要继承父模块的插件，但不用指定版本，即只需定义`groupId`和`artifactId`，不用定义`version`。这样做的好处是版本统一。  

父pom插件示例：

```xml
    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <configuration>
                        <source>1.5</source>
                        <target>1.5</target>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <configuration>
                        <encoding>UTF-8</encoding>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>
```

子pom插件示例：

```xml
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
```
子pom中的插件，只有`groupId`和`artifactId`与父pom的一致才会继承父pom的插件，否则将是子pom所特有的。  

## 聚合与继承的关系

![aggregation-extends](https://oscimg.oschina.net/oscnet/129a49fbe8c40f33111e17333d29097fd77.jpg "aggregation-extends")

其实它们之间没有什么关系，如果非要说有什么共同点，那么它们的`packaging`必须是pom。  

另外，读者往往发现一个pom既是聚合pom，又是父pom，这么做其实是为了方便。（IntelliJ里面就是这样做）  

## 约定优于配置

>任何一个maven项目都隐式地继承超级POM。

- maven3，超级pom在 $ MAVEN_HOME/lib/maven-model-builder-x.x.x.jar中的org/apache/maven/model/pom-4.0.0.xml。  
- maven2，超级pom在 $ MAVEN_HOME/lib/maven-x.x.x-uber.jar中的org/apache/maven/project/pom-4.0.0.xml。  

超级pom中定义了仓库及插件的仓库，地址都是[http://repo1.maven.org/maven2][4]，并且都关闭了SNAPSHOT的支持，这就是为什么maven默认就可以按需要从中央仓库下载插件的原因。  

超级pom中定义了项目的主输入目录、主代码输出目录、最终构件的名称格式、测试代码输出目录、主源码目录、测试源码目录、主资源目录和测试目录。  


# 单元测试

在maven的生命周期中，test阶段与maven-surefire-plugin插件绑定。

maven构建项目时，默认会执行所有的单元测试（以Test结尾的类），那如何跳过全部/部分测试类，或指定特定的测试类呢？

- 命令方式

  - $ mvn package-DskipTests  跳过所有的测试
  - $ mvn package-Dmaven.test.skip=true 跳过测试，并跳过测试代码的编译

- pom配置插件方式

```
    - 跳过所有测试  
  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>2.5</version>
        <!--跳过所有的测试-->
        <configuration>
          <skipTests>true</skipTests>
        </configuration>
      </plugin>
    </plugins>
  </build>
```

```
   - 跳过测试，并跳过测试代码的编译 
  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.0</version>
        <configuration>
          <skip>true</skip>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>2.5</version>
        <configuration>
          <skip>true</skip>
        </configuration>
      </plugin>
    </plugins>
  </build>
```

```
    - 排除测试类
  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>2.5</version>
        <configuration>
          <excludes>
            <exclude>**/*ServiceTest.java</exclude>
            <exclude>**/TempDaoTest.java</exclude>
          </excludes>
        </configuration>
      </plugin>
    </plugins>
  </build>
```

```
    - 包含以Tests结尾的测试类
  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>2.5</version>
        <configuration>
          <includes>
            <include>**/*Tests.java</include>
          </includes>
        </configuration>
      </plugin>
    </plugins>
  </build>
```

- 测试报告  
使用maven-surefire-plugin插件时默认的测试报告在target/surefire-reports目录，包含两种错误格式的报告：  
    - 简单文本格式  
    - 与junit兼容的xml格式  
- 测试测试覆盖率报告  
集成cobertura-maven-plugin插件  
- 重用测试代码  
使用 maven-jar-plugin将测试类打包：  
```
  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-jar-plugin</artifactId>
        <version>3.0.2</version>
        <executions>
          <execution>
            <goals>
              <goal>test-jar</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
```
maven-jar-plugin有两个目标，分别是jar和test-jar。  
    1）jar，内置绑定在packaging阶段执行，主要是对项目主代码进行打包  
    2）test-jar，没有内置绑定，需要显式声明，主要是对项目的测试代码进行打包  



# Web开发

## 目录结构

```
+ project  
    |
    + pom.xml  
    |
    + src/
        + main/
        | + java/
        | | + ServletA.java
        | | + ...
        | |
        | + resources/
        | | + config.properties
        | | + ...
        | |
        | + webapp/
        |   +WEB-INF/
        |   | + web.xml
        |   |
        |   + img/
        |   |
        |   + css
        |   |
        |   + js
        |   |
        |   + index.jsp
        |   |
        + test/
            + java/
            + resources/
```

- pom  

几乎所有的web所需的两个依赖： 

```
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <version>2.4</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.servlet.jsp</groupId>
            <artifactId>jsp-api</artifactId>
            <version>2.0</version>
            <scope>provided</scope>
        </dependency>
```

- 指定打包的名字

```
    <build>
        <finalName>SSM-Test</finalName>
    </build>
```

打包的名字就是 SSM-Test.war，不配置的话，默认是xxxx-1.0.0-SNAPSHOT.war

## -DarchetypeCatalog=internal

添加一个参数，maven构建时不用每次去下载插件

![](https://gitee.com/rulerLwx/PicGo/raw/master/img-07/20200708124730.png)


## jetty-maven-plugin

web页面的测试应该仅限于页面层次，如jps/css/javascript的修改，其它代码修改（如数据库访问），请编写单元测试。

通常情况下，我们只需要直接在IDE中修改源码，IDE能够执行自动编译，jetty-maven-plugin发现编译后的文件变化后，自动将其更新到jetty容器，这时就可以直接测试web页面了。  

使用步骤如下：

- 在pom中配置

```xml
    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.mortbay.jetty</groupId>
                    <artifactId>jetty-maven-plugin</artifactId>
                    <version>7.1.6.v20100715</version>
                    <configuration>
                        <scanIntervalSeconds>10</scanIntervalSeconds>
                        <webAppConfig>
                            <contextPath>/account</contextPath>
                        </webAppConfig>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>
```

*注意*：这个jetty-maven-plugin并不是官方插件，
scanIntervalSeconds，表示隔多少秒扫描一次；
contextPath，表示context path,例如 http://hostname:port/account/

- 修改setting.xml文件

```
<settings>
    ....
    <pluginGroup>org.mortbay.jetty</pluginGroup>
    ....
</setting>
```

- 启动

```bash
$ mvn jetty:run

$ mvn jetty:run -Djetty.port=9999   指定了端口
```

**注意**：在intellij中，通过Maven Project视图中的Run Configurations来配置执行的命令



# 私服：Nexus

>Nexus是典型的JavaWeb项目，它有两种安装包，一种是包含Jetty窗口的Buddle包，另一种是不包含web容器的war包。  

Nexus分开源版本和专业版本，专业版本收费。  

搭建私服：

1）下载

  [http://www.sonatype.org/nexus/downloads/][5]

2）安装

- 实验用版本：nexus-2.14.1-01-bundle（windows)
- 解压，目录结构如下

  ![maven-nexus-unzip](https://oscimg.oschina.net/oscnet/0877fb0fba9a6324b232908ac9c7b2ccc23.jpg "maven-nexus-unzip")

- 以管理员身份启动dos窗口，cd进入解压后的nexus-2.14.1-01-bundle\nexus-2.14.1-01\bin目录，执行以下命令：

```sh
  > nexus install
```

此命令是把nexus安装成windows的服务，如果不想安装此服务，可以执行`>nexus uninstall`。

- 到windows服务列表窗口启动`nexus`服务。
- 启动服务报错了？？？？？TODO....



# FAQ

## omitted for duplicate（依赖冲突）

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
    <exclusions>
        <exclusion>
            <groupId>org.springframework.boot</groupId>//产生omitted for duplicate的依赖的ja对应的g
            <artifactId>spring-boot</artifactId>//产生omitted for duplicate的依赖jar对应的a
        </exclusion>
        <exclusion>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-autoconfigure</artifactId>
        </exclusion>
    </exclusions>
</dependency>
```

http://www.cnblogs.com/brxHqs/p/10307787.html


# 附件

## pom.xml 最简配置

```pom
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.demo</groupId>
  <artifactId>mybatis</artifactId>
  <version>1.0-SNAPSHOT</version>

  <packaging>jar</packaging>

  <name>mybatis</name>
  <url>http://maven.apache.org</url>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
  </properties>

  <dependencies>
  </dependencies>

</project>

```

## SSM常用依赖

- hibernate-validator

```xml
    <dependency>
        <groupId>org.hibernate</groupId>
        <artifactId>hibernate-validator</artifactId>
        <version>5.3.5.Final</version>
    </dependency>
```

## 国内镜像

在setting.xml文件配置。

```xml
<mirrors>
    <mirror>
        <id>alimaven</id>
        <name>aliyun maven</name>
        <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
        <mirrorOf>central</mirrorOf>
    </mirror>
    <mirror>
        <id>central</id>
        <name>Maven Repository Switchboard</name>
        <url>http://repo1.maven.org/maven2/</url>
        <mirrorOf>central</mirrorOf>
    </mirror>
    <mirror>
        <id>repo2</id>
        <mirrorOf>central</mirrorOf>
        <name>Human Readable Name for this Mirror.</name>
        <url>http://repo2.maven.org/maven2/</url>
    </mirror>
    <mirror>
        <id>ibiblio</id>
        <mirrorOf>central</mirrorOf>
        <name>Human Readable Name for this Mirror.</name>
        <url>http://mirrors.ibiblio.org/pub/mirrors/maven2/</url>
    </mirror>
    <mirror>
        <id>jboss-public-repository-group</id>
        <mirrorOf>central</mirrorOf>
        <name>JBoss Public Repository Group</name>
        <url>http://repository.jboss.org/nexus/content/groups/public</url>
    </mirror>
    <!-- 中央仓库在中国的镜像 -->
    <mirror>
        <id>maven.net.cn</id>
        <name>oneof the central mirrors in china</name>
        <url>http://maven.net.cn/content/groups/public/</url>
        <mirrorOf>central</mirrorOf>
    </mirror>
</mirrors>
```

[1]:https://repository.sonatype.org
[2]: http://mvnrepository.com/
[3]: http://search.maven.org/
[4]: http://repo1.maven.org/maven2
[5]: http://www.sonatype.org/nexus/downloads/
[6]: http://maven.apache..org/archetype/maven-archetype-plugin
[7]: http://repo1.maven.org/maven2/archetype-catalog.xml