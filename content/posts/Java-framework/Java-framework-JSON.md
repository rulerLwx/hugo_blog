---
title: "Java-framework-JSON"
date: 2019-08-02T11:01:30+08:00
draft: true
tags: [ ]
categories: ["Java-framework"]
---


# 什么是JSON

主要是对象和数组两种类型：

- 花括号保存对象
- 方括号保存数组

对象表示为键/值对，数据由逗号分隔

键/值对组合中的键名写在前面并用双引号 "" 包裹，使用冒号 : 分隔，然后紧接着值

键名可以使用整数和字符串来表示，值的类型可以是任意类型



[JSON官网][1] 有各种语言的json客户端，以下是java的json客户端：

```
Java:
	JSON-java.
	JSONUtil.
	jsonp.
	Json-lib.
	Stringtree.
	SOJO.
	json-taglib.
	Flexjson.
	JON tools.
	Argo.
	jsonij.
	fastjson.      # alibaba
	mjson.
	jjson.
	json-simple.
	json-io.
	JsonMarshaller.
	google-gson.   # google
	Json-smart.
	FOSS Nova JSON.
	Corn CONVERTER.
	Apache johnzon.
	Genson.
	JSONUtil.
	cookjson.

    jackson         # 官网没列出，是开源的 https://github.com/FasterXML/jackson-databind
```



# jackson

官网地址（@github）：[FasterXML/jackson][3]

## 1.x 版本

tecsun:

```
        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-mapper-asl</artifactId>
            <version>1.9.13</version>
        </dependency>

        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-core-asl</artifactId>
            <version>1.9.13</version>
        </dependency>
```

**疑问**：1）包过期了？  2）这两个包都是必须的吗？

答：

1.x版本是 https://github.com/codehaus/jackson 写的，现在（2019）是 FasterXML 在维护，官方推荐使用2.x

参考：https://github.com/FasterXML/jackson-1

1.x版本maven依赖官方没有给出，参考：https://search.maven.org/search?q=g:org.codehaus.jackson （我搜索过了）

## 2.x 版本 

新的包依赖写法（参考：[FasterXML/jackson-databind][2]之Maven）：

```xml
<properties>
  ...
  <jackson.version>2.9.0</jackson.version>
  ...
</properties>

<dependencies>
    ...
  <dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-databind</artifactId>
    <version>${jackson.version}</version>
  </dependency>
  ...
</dependencies>
```

## 使用（1.x/tecsun)

```java
package com.tecsun.sisp.iface.server.util;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Sandwich on 2015/12/11.
 */
public class JsonHelp {
    private static final ObjectMapper mapper = new ObjectMapper();

    public static ObjectMapper getInstance() {

        return mapper;
    }

    public static <T> Object jsonToJavaBean(String jsonString, Class<T> t) {
        Object result = null;
        try {
            result = mapper.readValue(jsonString, t);
        } catch (Exception e) {
        }
        return result;
    }

    public static String javaBeanToJson(Object o) {
        String json = null;
        try {
            json = mapper.writeValueAsString(o);
        } catch (Exception e) {
        }
        return json;
    }

    @SuppressWarnings("unchecked")
    public static <T> Map<String, Object> jsonToMap(String jsonStr) {
        try {
            return mapper.readValue(jsonStr, Map.class);
        } catch (Exception e) {
            return null;
        }
    }

    @SuppressWarnings("rawtypes")
    public static <T> T mapToJavaBean(Map map, Class<T> clazz) {
        return mapper.convertValue(map, clazz);
    }

    public static <T> Map<String, T> jsonToMap(String jsonStr, Class<T> clazz)
            throws Exception {
        Map<String, Map<String, Object>> map = mapper.readValue(jsonStr,
                new TypeReference<Map<String, T>>() {
                });
        Map<String, T> result = new HashMap<String, T>();
        for (Map.Entry<String, Map<String, Object>> entry : map.entrySet()) {
            result.put(entry.getKey(), mapToJavaBean(entry.getValue(), clazz));
        }
        return result;
    }

    public static <T> List<T> jsonToList(String jsonArrayStr, Class<T> clazz)
            throws Exception {
        List<Map<String, Object>> list = mapper.readValue(jsonArrayStr,
                new TypeReference<List<T>>() {
                });
        List<T> result = new ArrayList<T>();
        for (Map<String, Object> map : list) {
            result.add(mapToJavaBean(map, clazz));
        }
        return result;
    }
}
```

注意：新的依赖包也可以使用上面的代码 ，只是要更改`ObjectMapper`和`TypeReference`的包(intellij自动导入：ctrl+alt+o)：

```
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
```

也可参考：[FasterXML/jackson-databind][2]，加深认识。


# gson

@github：[google/gson][4]



# fastjson

@github：[alibaba/fastjson][5]

- 开发注意事项

    + 内部类不能作为JSON的转化对象：`SecretKeyVO vo = JSON.parseObject(str,SecretKeyVO.class);`,`SecretKeyVO`为内部类。



[1]:http://www.json.org/
[2]: https://github.com/FasterXML/jackson-databind/
[3]: https://github.com/FasterXML/jackson
[4]:https://github.com/google/gson 
[5]:https://github.com/alibaba/fastjson 