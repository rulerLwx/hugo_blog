---
title: "MySQL-1-基础"
date: 2018-06-01T10:01:47+08:00
draft: true
categories: ["数据库"]
---


# 数据类型

## 整数类型

数据类型 | 字节数 | 无符号数的取值范围 | 有符号数的取值范围
---|---|---|---
TINYINT | 1 | 0 ~ 255 | -128 ~ 127
SMALLINT | 2 | 0 ~ 65 535 | -32 768 ~ 32 768
MEDIUMINT | 3 | 0 ~ 16 777 215 | -8 388 608 ~ 8 388 608
INT | 4 | 0 ~ 4 294 967 295 | -2 147 483 648 ~ 2 147 483 648
BIGINT | 8  | 0 ~ 18 446 744 073 709 551 615 |

取值范围可以根据字节数来算，如 TINYINT 占1个字节，无符号数最大值就是 2^8-1，即255，有符号取值范围 -2^7 ~ 2^7-1

## 浮点数、定点数类型

数据类型 | 字节数 | 无符号数的取值范围 | 有符号数的取值范围
---|---|---|---
FLOAT | 4 |  | 
DOUBLE | 8 |  | 
DECIMAL(M,D) | M+2 | | 

DECIMAL(M,D)，M表示数据长度，D表示小数点后的长度，如将 3.1415 插入 DECIMAL(6,2) 后，显示结果为 3.14

## 日期、时间类型

数据类型 | 字节数 | 取值范围 | 日期格式 | 零值
---|---|---|---|---
YEAR | 1 | 1901 ~ 2155 | YYYY | 0000
DATE | 4 | 1000-01-01 ~ 9999-12-31 | YYYY-MM-DD | 0000-00-00
TIME | 3 | -838:59:59 ~ 838:59:59 | HH:MM:SS | 00:00:00
DATETIME | 8 | 1000-01-01 00:00:00 ~ 9999-12-31 00:00:00 | YYYY-MM-DD HH:MM:SS | 0000-00-00 00:00:00
TIMESTAMP | 4 | 1970-01-01 00:00:01 ~ 2038-01-19 03:13:07 | YYYY-MM-DD HH:MM:SS | 0000-00-00 00:00:00

now() 和 current_timestamp 均表示系统当前日期和时间

```sql
select now() from dual;
select current_timestamp  from dual;
```


## 字符、二进制类型

数据类型 | 说明
---|---
CHAR | 固定长度的字符串
VARCHAR() | 可变长度的字符串
TEXT | 大文本数据，如文章内容、评论等
ENUM() | 表示枚举类型，只能存储一个枚举字符串值
SET() | 表示字符串对象，可以有零或多个值
BINARY | 表示固定长度的二进制数据
VARBINARY() | 表示可变长度的二进制数据
BIT | 表示位字段类型，二进制

为更好的理解CHAR与VARCHAR的区别，以 CHAR(4) 和 VARCHAR(4) 对比进行说明

插入值 | CHAR(4) | 存储需求 | VARCHAR(4) | 存储需求
---|---|---|---|---
'' | '' | 4个字节 | '' | 1个字节
'ab' | 'ab' | 4个字节 | 'ab' | 3个字节
'abc' | 'abc' | 4个字节 | 'abc' | 4个字节
'abcd' | 'abcd' | 4个字节 | 'abcd' | 5个字节
'abcdef' | 'abcde' | 4个字节 | 'abcd' | 5个字节

当数据为 CHAR(4) 时，不管插入值的长度为多少，所占用存储空间都是4个字节，而 VARCHAR(4) 所对应的数据所占用的字节数为实际长度加1

TEXT类型有以下几种：

数据类型 | 存储范围
---|---
TINYTEXT | 0 ~ 255 字节
TEXT | 0 ~ 65 535 字节
MEDIUMTEXT | 0 ~ 16 777 215 字节
LONGTEXT | 0 ~ 4 294 967 295 字节

- 共享锁（读锁）、排他锁（写锁）：读锁是共享的，或者说是相互不阻塞的；写锁则是排他，也就是说一个写锁会阻塞其它的写锁和读锁。

# 表的约束

## 主键约束

1）单字段主键

2）多字段主键

注意：每个数据表中最多能有一个主键约束，定义为 PRIMARY KEY 的字段不能重复、且不能为NULL

## 非空约束

通过 NOT NULL 定义

## 唯一约束

通过 UNIQUE 定义。表中此字段的值不能重复出现

## 默认约束

通过 DEFAULT 关键字定义。如果没有给这个字段赋值，数据库自动为这个字段插入默认值

## 外键约束

通过 Foreign Key 定义

如果想删除主表记录时，从表记录也会随之删除，则需要在建立外键约束后添加 `on delete cascade`或`on delete set null`，第一条是级联删除，第二条是将参照主表记录的从表记录的外键设为null。如：

```sql
create table student_table4
(
    # 为本表建立主键
    student_id int auto_increment primary key,
    student_name varchar(255),
    java_teacher int,
    # 建立外键，级联删除
    foreign key(java_teacher) references teacher_table4(teacher_id) on delete cascade # 也可以用 on delete set null
)
```


# 索引

## 索引的分类

1）普通索引

普通索引是由 KEY 或 INDEX 定义的索引，是MySQL中基本索引类型

2）唯一性索引

唯一性索引是由 UNIQUE 定义的索引

3）全文索引

全文索引是由 FULLTEXT 定义的索引，它只能创建在 CHAR/VARCHAR/TEXT 类型的字段上，现在只有 MyISAM 引擎支持

4）单列索引

5）多列索引

6）空间索引

总结：虽然索引可以提高数据的查询速度，但索引会占用一定的磁盘空间，并且在创建和维护索引时，其消耗的时间是随着数据量的增加而增加的。因此，使用索引时，应该综合考虑索引的优点和缺点。

## 索引用使用

可以使用 `EXPLAIN` 语句查看索引的使用情况


# 表的操作

## 创建表

方法一：常规姿势

方法二：使用select

```
---1 mysql 
   create table `新数据表名`
     select  *  from `旧数据表名`;
--------------------------------
---2 oracle
  create table 新数据表名 as select * from 旧数据表名
--------------------------------
--3 mssql 
  select * into [新数据表名] from [旧数据表名]
```

https://www.cnblogs.com/lairui1232000/p/9206465.html


# 查询

## 关系运算符

1）比较运算符

关系运算符 | 说明 | 关系运算符 | 说明 
---|---|---|---
= | 等于 | <= | 小于等于
<> | 不等于 | > | 大于
!= | 不等于 | >= | 大于等于
< | 小于

2）between and 包含边界

3）distinct可以作用于单个、多个字段，作用于多个字段的语法：`select distinct 字段1,字段2,...`

作用于多个字段的示例：`select distinct gender,name from student`，只有gender和name**都相同**才会被认作是重复记录

4）like 的通配符有百分号（%）和下划线（_）

`%`匹配任意长度的字符串，包括空字符串；`_`匹配单个字符，如果有多个下划线，则下划线之间不能有空格

如果被匹配的字符中有`%`或`_`则需要使用`\`转义，如 `like %\%%`

5）and 和 or

如果 and 和 or 关键字一起使用，**and 的优先级高于 or**

6）order by / group by

group by 通常结合聚合函数一样使用，也常常跟 having 一起使用

7）limit {[offset,] row_count}

从第 offset 行开始，返回 row_count 条记录

## 系统函数

### 1）聚合函数：
函数名称 | 作用 | 函数名称 | 作用
---|---|---|---
count() | 返回某列的行数 | max() | 返回某列的最大值
sum() | 返回某列值的和 | min() | 返回某列的最小值
avg() | 返回某列的平均值

### 2）数学函数：
函数名称 | 作用 | 函数名称 | 作用
---|---|---|---
abs(x) | 返回 x 的绝对值 | floor(x) | 返回不大于 x 的最大整数 <br> 举例：`floor(8.3)`=8，`floor(-8.3)`=-9
sqrt(x) | 返回 x 的非负2次方根 | round(x,y) | 对 x 进行四舍五入操作，小数点后保留 y 位 <br> 举例：`round(5.345,2)` = 5.35
mod(x,y) | 返回 x 被 y 除后的余数，相当于 x/y 的余数 | truncate(x,y) | 舍去 x 中小数点 y 位后面的数，<br/> 举例：`truncate(5.010104,2)` = 5.01
ceiling(x) | 返回不小于 x 的最小整数 <br> 举例：`ceiling(8.3)`=9，`ceiling(-8.3)`=-8 | sign(x) | 返回 x 的符号，-1、0或1 <br> 举例：`sign(-7)`=-1，`sign(7)`=1，`sign(-0)`=0

### 3）字符串函数：
函数名称 | 作用 | 函数名称 | 作用
---|---|---|---
length(str) | 返回字符串 str 的长度 | substring(str,n.len) | 返回字符串 str 的子串，<br> 起始位置为n，长度为len <br> `substring('string',1,3)`=`str`
concat(s1,s2,..) | 返回一个或者多个字符串连接产生的新的字符串 | reverse(str) | 返回字符串反转后的结果
trim(str) | 删除字符串两侧的空格 | locate(s1,str) | 返回子串 s1 在字符串 str 中的 <br> 起始位置
replace(str,s1,s2) | 使用字符串 s2 替换字符串 str 中所有的字符串 s1

### 4）日期和时间函数
函数名称 | 作用 | 函数名称 | 作用
---|---|---|---
curdate() | 获取系统当前日期，`2019-07-16` | adddate() | 执行日期的加运算，<br> `adddate('2019-02-01',31)` = `2019-03-04`
curtime() | 获取系统当前时间，`01:19:20` | subdate() | 执行日期的减运算
sysdate() | 获取系统当前日期和时间，<br> `2019-07-16 01:19:20` | date_format() | 格式化输出日期和时间值
time_to_sec() | 返回将时间转换成秒的结果 | datediff() | todo...

总结：date 的格式就是 yyyy-mm-dd

### 5）条件判断函数：
函数名称 | 作用
---|---
if(expr,v1,v2) | 如果 expr 表达式为 true 返回 v1，否则返回 v2
ifnull(v1,v2) | 如果 v1 为 null 返回 v1，否则返回 v2
case expr when v1 then r1 [when v2 then r2...] [else rn] end |

### 6）加密函数：
函数名称 | 作用
---|---
md5(str) | 对字符串 str 进行 MD5 加密
encode(str,pwd_str) | 使用 pwd 作为密码加密字符串 str
decode(str,pwd_str) | 使用 pwd 作为密码解密字符串 str


## 连接查询

### 交叉连接

语法：select * from 表1 cross join 表2

交叉连接返回的结果是被连接的两个表中所有数据行的笛卡儿积

如 department 表中有4个部门，employee表中有4个员工，那么交叉连接的结果就有 4 * 4 = 16 条数据

实际开发很少用这种连接查询方式！


### 内连接

语法：select 查询字段 from 表1 [inner] join 表2 on 表1.关系字段 = 表2.关系字段  where 条件

这种方式跟使用 where 条件判断效果相同：

```sql
select a.* from table a,talbe b where a.id = b.id
```


### 外连接

语法：select 所查字段 from  表1 left|right [outer] join 表2 on 表1.关系字段 = 表2.关系字段 where 条件

left/right称为关键字，关键字左边的表称为左表，关键字右边的表称为右表

1）left join ：返回包括左表中的所有记录和右表中符合连接条件的记录，不符合的字段显示为 null

2）right join ：返回包括右表中的所有记录和左表中符合连接条件的记录，不符合的字段显示为 null


## 子查询

常用关键字：in/exists/any/all

```sql
select * from department where did > all (select did from employee)
```


## 分页

```sql
select * from test.user limit 0,4 # 记录索引从0开始，表示第一条记录
```


```sql
select id, name, cert_num as certnum from basic_person_info limit 3 # 第1行开始，3条数据
select id, name, cert_num as certnum from basic_person_info limit 3,3 # 第4行开始，3条数据

# 疑问：
# 为了检索从某一个偏移量到记录集的结束所有的记录行，可以指定第二个参数为 -1：   
mysql> SELECT * FROM table LIMIT 95,-1; # 检索记录行 96-last.  

```

在Java中第几行是 （当前页码-1）* 每页行数

```
        //每页记录数，从哪开始
        int limit = 8;
        int offset = (curPage-1) * limit;
        
        //分页得到数据列表
        List<Post> postList = postMapper.listPostByTime(offset,limit);
        
    <select id="listPostByTime" resultMap="postMap">
        select u.uid,u.username,u.head_url,p.pid,p.title,p.publish_time,p.reply_time,p.reply_count,p.like_count,p.scan_count from post p
        join user u on p.uid = u.uid
        order by p.reply_time desc limit #{offset},#{limit}
    </select>
```

## 分组查询

```
select sum(grade),gender from student group by gender having sum(grade) < 300;
```


# 事务

## 事务的概念

事务：针对数据库的一组操作，它可以由一条或多条SQL语句组成，这些语句要么都执行，要么都不执行。它必须满足4个特性：原子性（Atomicity）、一致性（Consistency）、隔离性（Isolation）、持久性（Durability）。

- 原子性：一个最小的工作单元
- 一致性：如唯一性约束
- 隔离性：多个用户并发访问数据库时，数据库为每个用户开启事务，事务之间的操作数据相互隔离
- 持久性：事务一旦提交，其所做的修改就会永久保存到数据库中


## 事务的开启、提交、回滚

```sql
start transaction;#开启事务

rollback;#回滚事务

commit;#提交事务
```


## 事务的隔离级别

数据库是多线程并发访问的，所以很容易出现多个线程同时开启事务的情况，容易出现脏读、重复读、幻读

解决方法：为事务设置隔离级别。

MySQL有四种隔离级别：

事务级别 | 说明
---|---
read uncommitted | 可以读取到另一个事务未提交的数据，也称为脏读（Dirty Read）
read committed | 可以读取其它事务已提交的内容，避免了脏读，避免不了重复读、幻读。**oracle使用此级别**
repeatable read | 也称为可重复读，**MySQL默认的事务隔离级别**，可以避免 脏读、重复读、幻读
serializable | 也称可串行化，最高隔离级别，在每个读的数据行上加锁，解决了脏读、重复读、幻读，<br> 但可能导致大量超时现象和锁竞争，在实际应用中很少使用

搞清楚两个概念：

- 重复读：在事务内重复读取了别的线程已经提交的数据，但两次读取的结果不一致，原因是查询的过程中其它事务做了更新操作。
- 幻读：在一个事务内两次查询中数据条数不一致，原因是查询的过程中其它的事务做了添加操作。

如何修改事务隔离级别：

```sql
set session transaction isolation level read uncommitted;
```
