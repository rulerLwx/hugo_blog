---
title: "Oracle-2-QA"
date: 2018-06-04T10:02:47+08:00
draft: true
categories: ["数据库"]
---


# Q & A

## 什么是数据字典

数据字典（Data dictionary）是一种用户可以访问的记录数据库和应用程序元数据的目录。


## between and 

包含边界


## Oracle密码过期


查询密码是否过期

1、--查看用户的proifle是哪个，一般是default：

```
SELECT USERNAME, PROFILE FROM DBA_USERS;
```

2、查看指定概要文件(如default)的密码有效期

```
select * from dba_profiles where profile='DEFAULT' and resource_name='PASSWORD_LIFE_TIME';
```


### window系统

1、打开cmd

2、输入：sqlplus /nolog，回车

```
C:\Users\liweixin>sqlplus /nolog
SQL*Plus: Release 11.2.0.1.0 Production on 星期三 11月 16 09:53:00 2016
Copyright (c) 1982, 2010, Oracle.  All rights reserved.
SQL>
```

3、输入命令：conn /as sysdba

```
SQL> conn /as sysdba 
已连接。
```

4、修改用户密码

```
SQL> alter user system identified by password;
用户已更改。
```

5、把密码改为永久不过期

```
sql> ALTER PROFILE DEFAULT LIMIT PASSWORD_LIFE_TIME UNLIMITED;
```

6、解锁用户

```
SQL> alter user sisp_public ACCOUNT UNLOCK;
用户已更改。
```

7、提交事务

```
SQL> commit;
提交完成。
```


### linux系统

1、用secureCRT连接到数据库所在的linux机器，切换到oracle用户模式下

```
[root@oracle-server bin]# su - oracle
[oracle@oracle-server ~]$
```

2、步骤2-7与window系统相同。


## truncate 、delete、drop区别

delete是dml语句，truncate、drop是ddl语句。

速度，一般来说: drop > truncate > delete

想删除部分数据行用 delete，注意带上where子句；想删除表,当然用 drop；想保留表而将所有数据删除，如果和事务无关，用truncate即可。

什么时候不能truncate/delete/drop？答：有外键时，不能对 从表 执行这些操作。

参考：https://www.cnblogs.com/zhoufangcheng04050227/p/7991759.html


# 面试题

## scott/tiger模式面试题


- 名称为'SMITH'的管理者信息

```
SELECT *
  FROM EMP E
 WHERE E.ENAME = 'SMITH'
   AND EXISTS (SELECT 1 FROM EMP E2 WHERE E2.Empno = e.mgr);
```

- 平均工资高于800的部门ID、名称、平均工资

```
SELECT E.DEPTNO,d.dname, AVG(E.SAL) AS avg_SAL
  FROM EMP E, DEPT D
 WHERE E.DEPTNO = D.DEPTNO
 GROUP BY E.DEPTNO,d.dname
HAVING AVG(E.SAL) > 800;
```

- 工资最低的员工信息

```
SELECT e.empno,e.sal,ROWNUM rn FROM emp e WHERE ROWNUM = 1 ORDER BY e.sal ASC;
SELECT e.empno,e.sal FROM emp e WHERE e.sal = (SELECT MIN(sal) FROM emp);
```

- 查询平均工资最低的部门信息

```
SELECT *
  FROM (SELECT E.DEPTNO, D.DNAME, AVG(E.SAL) AS AVG_SAL
          FROM EMP E, DEPT D
         WHERE E.DEPTNO = D.DEPTNO
         GROUP BY E.DEPTNO, D.DNAME
         ORDER BY AVG_SAL ASC)
 WHERE ROWNUM = 1;
```

- 查询平均工资最低的部门信息和该部门的平均工资（同5）


- 查询平均工资最高的job信息

```
SELECT *
FROM emp e,jobs j;
-- 8、查询平均工资高于公司平均工资的部门有哪些
SELECT *
  FROM (SELECT E.DEPTNO, D.DNAME, AVG(E.SAL) AVG_SAL
          FROM EMP E, DEPT D
         WHERE E.DEPTNO = D.DEPTNO
         GROUP BY E.DEPTNO, D.DNAME)
 WHERE AVG_SAL > (SELECT AVG(SAL) FROM EMP)
```

## 为“省、市、县、街道”设计数据表

```
CREATE TABLE t_amap_city (
id bigint(20) NOT NULL COMMENT '区域ID',
pid bigint(20) NOT NULL COMMENT '上级区域ID, 如深圳市的上级是广东省',
level tinyint(4) NOT NULL DEFAULT '0' COMMENT '国家:1,省份:2,市:3,区:4,街道:5',
name varchar(128) NOT NULL DEFAULT '' COMMENT '行政区名称',
citycode varchar(16) NOT NULL DEFAULT '0' COMMENT '城市编码',
adcode varchar(16) NOT NULL DEFAULT '0' COMMENT '区域编码',
center varchar(32) NOT NULL DEFAULT '' COMMENT '城市中心经纬度',
polyline text COMMENT '边界点经纬度',
create_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
modify_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=TokuDB DEFAULT CHARSET=utf8
```
参考：https://segmentfault.com/q/1010000011373811

另一种行数较多，参考：https://blog.csdn.net/qq_32915215/article/details/81483795

php sql语句 二级、三级、四级联动：https://blog.csdn.net/qq_30202073/article/details/83538222

我写的sql：

```
use test;
select a.area_id,a.area_code,a.area_name,b.area_id,b.area_name,c.area_id,c.area_name
from area a,area b,area c
where a.area_id = b.parent_id and b.area_id = c.parent_id
```
