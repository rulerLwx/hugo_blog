---
title: "Redis-3-事务、持久化"
date: 2018-06-05T10:03:47+08:00
draft: true
categories: ["数据库"]
---

# 事务

## 概述

- 事务：在一个队列（QUEUE）中，一次性、顺序性、排他性地执行一系列命令
- redis中的事务是一组命令集合，multi ..... exec。
- redis不支持事务回滚
- watch命令：监控一个或多个键，一旦其中有一个键被修改（或删除），之后的事务就不会被执行。

## 常用命令

命令 | 说明
---|---
MULTI | 标记一个事务块的开始
DISCARD | 取消事务，放弃执行事务块内的**所有命令**
EXEC | 执行事务块内的所有命令
WATCH key [key ...] | 监视一个或多个key，如果在事务执行之间这个（或这些）key被其它命令所改动，那么事务将被打断
UNWATCH | 取消 WATCH命令对**所有key**的监视

## 示例

1）正常执行`multi ..... exec`

```
127.0.0.1:6379> MULTI
OK
127.0.0.1:6379> set k1 v1
QUEUED
127.0.0.1:6379> set k2 v2
QUEUED
127.0.0.1:6379> set k3 v3
QUEUED
127.0.0.1:6379> EXEC
1) OK
2) OK
3) OK
127.0.0.1:6379> 
```

2）放弃事务

```
127.0.0.1:6379> MULTI
OK
127.0.0.1:6379> set k1 11
QUEUED
127.0.0.1:6379> set k2 22
QUEUED
127.0.0.1:6379> DISCARD
OK
127.0.0.1:6379> 
```

3）全体连坐

如果在加入队列时，有一条语句执行错误，则全部事务将失效，即：要么全成功，要么全部放弃。

```
127.0.0.1:6379> MULTI
OK
127.0.0.1:6379> set k1 aa
QUEUED
127.0.0.1:6379> set k2 bb
QUEUED
127.0.0.1:6379> setget k3 cc
(error) ERR unknown command `setget`, with args beginning with: `k3`, `cc`, 
127.0.0.1:6379> set k4 dd
QUEUED
127.0.0.1:6379> EXEC
(error) EXECABORT Transaction discarded because of previous errors.
127.0.0.1:6379> 
```

4）冤头债主

EXEC 时，INCR k1 产生错误，但不影响后面的执行。跟上面的“全体连坐”不同，“全体连坐”是在加入 QUEUE 时就错误了。

这里可以看到，Redis不支持事务回滚。

```
127.0.0.1:6379> MULTI
OK
127.0.0.1:6379> INCR k1
QUEUED
127.0.0.1:6379> set k2 gg
QUEUED
127.0.0.1:6379> set k3 hh
QUEUED
127.0.0.1:6379> EXEC
1) (error) ERR value is not an integer or out of range
2) OK
3) OK
127.0.0.1:6379> 
```

5）WATCH 监控

先监控，再开启事务
```
127.0.0.1:6379> set balance 100
OK
127.0.0.1:6379> set debt 0
OK


127.0.0.1:6379> WATCH balance
127.0.0.1:6379> MULTI
OK
127.0.0.1:6379> DECRBY balance 20
QUEUED
127.0.0.1:6379> INCRBY debt 20
QUEUED
127.0.0.1:6379> EXEC
1) (integer) 80
2) (integer) 20
127.0.0.1:6379> 
```

总结：修改重要数据字段时，先`WATCH`该字段，再执行`MULTI...EXEC`，事务执行成功后，`WATCH` 才失效，此时不需要再执行`UNWATCH`命令。

## 悲观锁、乐观锁

- 悲观锁，以数据表为例
- 乐观锁，`WATCH`命令就是乐观锁，可以理解为用版本控制
- CAS，

![](https://gitee.com/leafsing/pic/raw/master/img/20200709091442.png)

![](https://gitee.com/leafsing/pic/raw/master/img/20200709091525.png)

# 持久化

## RDB（Redis DataBase）方式

当符合一定条件时Redis会自动将内存中的所有数据生成一份副本并存储在硬盘上，这个过程即为“快照”（snapshotting）

RDB方式默认是开启的，文件的**存储位置、名称**在redis.conf中配置。

Redis执行快照的时机：

- 快照规则配置
- 手动执行save/bgsave 命令
- 手动执行flushall 命令
- 执行复制（replication）时。

1、快照规则配置

```
语法：save <seconds> <changes>

save 900 1      # save 时间（秒） 改动的键数，可以配置多个条件，条件之间是‘或’关系
save 300 10
save 60 10000

官方解释：
#   In the example below the behaviour will be to save:
#   after 900 sec (15 min) if at least 1 key changed
#   after 300 sec (5 min) if at least 10 keys changed
#   after 60 sec if at least 10000 keys changed
```

2、`sava/bgsave`命令

```
此处是手动进行快照（如服务器关闭前）

save，同步快照，会阻塞请求，生产环境不建议使用
bgsave，异步快照
```

3、`flushall/shutdown`命令

```
flushall命令会清除数据库中的所有数据（0~15的数据库），此时会触发快照条件（快照规则不为空）

注意：此时内存中的数据全部清空了，然后触发快照生成了新的dump.db文件，此时的dump.db是空的，没有任何数据。——20190914
```

4、执行复制（master/slave）时

```
执行复制时都会生成快照文件（不管快照规则是否为空，有没有手动执行快照）
```

总结：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709091545.png)


## AOF（Append Only File）方式

aof方式，可以防止数据丢失：redis的每一条**写命令**都会追加到硬盘中，但读命令不记录；只允许追加文件，不可以改写文件。

注意：`FLUSHALL`命令也会记录在`appendonly.aof`文件中。

aof方式以纯文本形式记录redis执行的命令。

aof文件保存位置和rdb文件位置相同，默认文件名是appendonly.aof，可以通过以下参数配置：
```
appendonly yes      # 启用aof方式
dir ./              # 文件位置
appendfilename appendfilename   # 文件名称
```

命令执行得越多，aof的文件越大，且有冗余的命令，如何优化？—— 配置aof重写

```
auto-aof-rewrite-percentage 100    # 当前aof文件大小超过上次重写时的aof文件大小的百分之几时时会再次进行重写
auto-aof-rewrite-min-size 64mb  重写的最小aof文件大小
```

重写aof的目的是**去除冗余的命令**

redis允许同时开启aof和rdb

常用配置（redis.conf）：
- `appendonly yes` ：启用aof方式
- `dir ./` ：文件位置
- `appendfilename appendfilename`：文件名称
- `appendfsync always/everysec/no`：默认用everysec，也是推荐配置
- `no-appendfsync-on-rewrite no`：重写时是否可以运用appendfsync，用默认的no即可，保证数据安全性
- `auto-aof-rewrite-percentage 100`：重写基准值，当前aof文件大小超过上次重写时的aof文件大小的百分之几时时会再次进行重写
- `auto-aof-rewrite-min-size 64mb`：重写基准值，重写的最小aof文件大小

总结：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709091609.png)

![](https://gitee.com/leafsing/pic/raw/master/img/20200709091629.png)


## 如何恢复rdb或aof数据到内存？

将`dump.rdb`或`appendonly.aof`放到安装目录，重新启动redis服务即可。客户端可以使用`config get dir`获取安装目录。——20190914

问：redis允许同时开启aof和rdb，数据恢复时，以谁为主？优先执行谁？

答：如果开启了aof方式，则加载的是`appendonly.aof`；如果没有开启aof，则加载`dump.rdb`。

---

检查、修复aof文件：`./redis-check-aof --fix appendonly` 

---

参考：

- redis执行了flushdb或者flushall之后的“后悔药”操作：https://blog.csdn.net/qq_25551295/article/details/48103245#commentBox
- Redis 禁用FLUSHALL FLUSHDB KEYS 命令：https://blog.csdn.net/u011250882/article/details/48580003

## 同步硬盘数据

```
# appendfsync always
appendfsync everysec    # 每秒同步一次到硬盘
# appendfsync no        # 操作系统默认每30秒将硬盘缓存的数据写入硬盘
```

[1]:http://www.cnblogs.com/panjun-Donet/archive/2010/08/10/1796873.html
[2]: http://blog.csdn.net/gxw19874/article/details/51992125
[3]: http://blog.csdn.net/aitangyong/article/details/52055932