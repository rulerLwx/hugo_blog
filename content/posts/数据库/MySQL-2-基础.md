---
title: "MySQL-2-基础"
date: 2018-06-01T10:02:47+08:00
draft: true
categories: ["数据库"]
---

# 存储过程

## 预备知识：MySQL中的变量

- 局部变量：declare
- 用户变量：set/select
- 会话变量 
- 全局变量 

参考：https://www.cnblogs.com/EasonJim/p/7966918.html ， https://www.cnblogs.com/gavin110-lgy/p/5772577.html

查看定义的变量：`select 变量名,变量名,..` 或 `select @变量名,@变量名,..`

## 创建存储过程

储存过程就是一条或多条sql语句的集合。

如果使用命令行创建存储过程，可以使用 delimiter 定义结束符，如`delimiter //`，注意：delimiter与结束符之间要有一个空格，否则无效

1）语法定义

```sql
CREATE
    [DEFINER = { user | CURRENT_USER }]
    PROCEDURE sp_name ([proc_parameter[,...]])
    [characteristic ...] routine_body

CREATE
    [DEFINER = { user | CURRENT_USER }]
    FUNCTION sp_name ([func_parameter[,...]])
    RETURNS type
    [characteristic ...] routine_body

proc_parameter:
    [ IN | OUT | INOUT ] param_name type

func_parameter:
    param_name type

type:
    Any valid MySQL data type

characteristic:
    COMMENT  'string'
  | LANGUAGE  SQL
  | [NOT] DETERMINISTIC
  | { CONTAINS  SQL | NO  SQL | READS  SQL  DATA | MODIFIES  SQL  DATA }
  | SQL  SECURITY { DEFINER | INVOKER }

routine_body:
    Valid SQL routine statement
```

说明：

- LANGUAGE SQL ：说明 routine_body 部分是由 sql 组成的，sql 是 LANGUAGE 的唯一值
- [NOT] DETERMINISTIC ：指明存储过程执行结果是否确定，默认为 NOT DETERMINISTIC
- { CONTAINS SQL | NO  SQL | READS SQL DATA | MODIFIES SQL DATA } ：默认 CONTAINS SQL
- SQL SECURITY { DEFINER | INVOKER } ：指明谁有权限来执行，默认 DEFINER

---

```sql
CREATE PROCEDURE `存储过程名` (参数列表)
BEGIN

存储过程体（一组合法的SQL语句）

END
```

参数列表包含三个部分：参数模式 参数名 参数类型，例：`IN stuname VARCHAR(20)`

参数模式：

- IN ：输入参数，调用方需要传入参数
- OUT ：输出参数，该参数可以作为返回值
- INOUT ：输入输出参数，该参数即可输入也可输出


2）变量的定义、赋值

变量定义、赋值在 begin...end 之间

语法：DECLARE var_name [, var_name] ... data_type [DEFAULT value]

为变量赋值，语法：set var_name = expr[,var_name = expr]...

官方文档：https://dev.mysql.com/doc/refman/5.5/en/stored-program-variables.html

示例：

```sql
declare var1,var2,var3 int;
set var1 = 10,var2 = 20;
set var3 = var1 + var2;

declare s_grade float;
declare s_gender char(2);
select grade,gender into s_grade,s_gender from student where name = 'rose';
```

## 定义条件和处理程序

**定义条件**是事先定义程序执行过程中遇到的问题，**处理程序**定义了在遇到这些问题时应采取的处理方式，并且保证存储过程在遇到警告或错误时能继续执行。



## 示例

### OUT参数的存储过程

1）一个输出参数：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709022603.png)

`SET @bname` 也可以省略不写

2）两个输出参数：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709022630.png)

3）自我总结：输出参数是存放在自定义的变量中


### INOUT参数的存储过程

![](https://gitee.com/leafsing/pic/raw/master/img/20200709023227.png)

总结：调用时传入的是两个自定义的变量，这些变量既用于传入值也用于接收值。

`select @m,@n;`是取出存储过程的返回值。

---

其它示例：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709023245.png)

![](https://gitee.com/leafsing/pic/raw/master/img/20200709023300.png)



## 调用存储过程

```sql
CALL sp_name([parameter[,...]]);
```

入参有时是常量，有时是用户变量（当储存过程有输出参数时）


# 函数

存储过程与函数区别：

- 存储过程：可以有0个或多个返回，适合批量插入、批量更新
- 函数：有且仅有1个返回，适合对数据处理后返回一个结果

## 创建函数

1）语法

```sql
CREATE FUNCTION `函数名` (参数列表) RETURNS 返回类型
BEGIN

RETURN xxx;
END
```

2）示例

无参有返回：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709023324.png)

有参有返回：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709023337.png)


## 调用函数

```sql
select 函数名（参数列表）;
```


# 流程控制

流程控制有三种：

- 顺序结构
- 分支结构
- 循环结构

## 分支结构

### IF

1）if()函数

if(expr,v1,v2)，如果 expr 表达式为 true，返回 v1，否则返回 v2

2）条件控制

语法：

```sql
IF search_condition THEN statement_list
    [ELSEIF search_condition THEN statement_list] ...
    [ELSE statement_list]
END  IF
```

使用位置：只能应用在 BEGIN AND 中

示例：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709023409.png)

### CASE

情况1：类似于java中的switch语句，一般用于实现等值判断

```sql
CASE 变量|表达式|字段 
WHEN 要判断的值1 THEN 返回的值1或语句1;
WHEN 要判断的值2 THEN 返回的值2或语句2;
...
ELSE 要返回的值n或语句n;
END CASE;
```

情况2：类似于java中的多重IF语句，一般用于实现区间判断

```sql
CASE
WHEN 要判断的条件1 THEN 返回的值1或语句1;
WHEN 要判断的条件2 THEN 返回的值2或语句2;
...
ELSE 要返回的值n或语句n;
END CASE;
```

- CASE 作为表达式，嵌套在其它语句中使用，可以放在任何地方：BEGIN AND 或 BEGIN AND 外面
- CASE 作为独立语句使用，只能放在 BEGIN AND 中
- 如果 WHEN 中条件成立，则执行 THEN 中的语句，并结束 CASE ;如果者不满足，则执行 ELSE 中的语句
- ELSE 可以省略，如果省略了，并且所有 WHEN 都不满足，则返回 NULL

示例：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709023426.png)


## 循环结构

分类：

- WHILE
- LOOP
- REPEAT

循环控制：

- iterate，类似于 continue，结束本次循环，继续下一次循环。语法：ITERATE lable 
- leave，类似于 break ，跳出，结束当前所有循环。语法：LEAVE lable 

### WHILE

语法：

```sql
[begin_label:] WHILE search_condition DO
    statement_list
END  WHILE [end_label]
```

1）没有添加循环控制语句

![](https://gitee.com/leafsing/pic/raw/master/img/20200709023449.png)

2）添加 leave 语句

![](https://gitee.com/leafsing/pic/raw/master/img/20200709023502.png)

3）添加 iterate 语句

![](https://gitee.com/leafsing/pic/raw/master/img/20200709023518.png)

4）综合示例

![](https://gitee.com/leafsing/pic/raw/master/img/20200709023527.png)


### LOOP

### REPEAT

# 数据库管理

## 数据备份与还原

MySQL提供了一个 mysqldump 命令，可以备份单个数据库、多个数据库、所有数据库

1）单个数据库备份

语法：mysqldump -uusername -ppassword dbname [tbname1 [tbname2...]] > filename.sql

使用 mysqldump 命令备份数据库时，直接在 DOS 容器执行即可

2）多个数据库备份

语法：mysqldump -uusername -ppassword --database dbname1 [dbname2 dbname3...] > filename.sql

3）备份所有数据库

语法：mysqldump -uusername -ppassword --all-database > filename.sql

总结：在实际工作过程中，我会用界面工具，如 mysql workbench

如何还原数据？

![](https://gitee.com/leafsing/pic/raw/master/img/20200709023553.png)

`source`语句，后面不能有分号


## 用户管理

安装MySQL后，会自动安装一个名为 mysql 的数据库，该数据库中的表都是权限表，如 user/db/host/tables_priv/column_priv/procs_priv

user 表是最重要的一个权限表，MySQL中的用户分为 root 用户和普通用户

### 创建普通用户

使用 grant 语句是创建用户最常用的方法

1）使用 grant 语句创建用户

语法：

```sql
GRANT privileges ON database.table
    TO 'username'@'hostname' [IDENTIFIED BY [PASSWORD] 'password']
    [,'username'@'hostname' [IDENTIFIED BY [PASSWORD] 'password']
```

示例：新建一个名为 user1，密码为 123，并授予该用户对chapter08.student 表有查询权限 的用户

```sql
GRANT SELECT ON chapter08.student TO 'user1'@'localhost' IDENTIFIED BY '123';
```

2）使用 create user 语句创建用户

通常，应该先创建用户，再授予权限：

```sql
CREATE USER 'jeffrey'@'localhost' IDENTIFIED BY 'password';
GRANT ALL ON db1.* TO 'jeffrey'@'localhost';
GRANT SELECT ON db2.invoice TO 'jeffrey'@'localhost';
GRANT USAGE ON *.* TO 'jeffrey'@'localhost' WITH MAX_QUERIES_PER_HOUR 90;
```

参考：https://dev.mysql.com/doc/refman/5.5/en/grant.html

3）使用 insert 语句创建用户

不管使用 CREATE USER 语句还是 GRANT 语句，在创建用户时，实际上都是在 user 表中添加一条新的记录

步骤一：直接在mysql.user表中插入一条记录

```sql
INSERT INTO mysql.user(Host,User,Password,ss1_cipher,x509_issuer,x509_subject)
VALUES('localhost','user3',PASSWORD('123'),'','','');
```

user表其它字段都有默认值，但 ss1_cipher、x509_issuer、x509_subject 字段没有默认值，所以要设置初始值

步骤二：手动刷新权限表

```
flush privileges;
```

### 删除普通用户

方式一：使用 DROP USER 语句删除用户

```sql
DROP USER 'test'@'localhost';
```

DROP USER 跟 DROP TABLE 类似

方式二：使用 DELETE 语句删除语句

```sql
DELETE FROM mysql.user WHERE Host = 'localhost' AND User = 'test';
```

### 修改用户密码

root 用户具有最高权限，不仅可以修改自己的密码，还可以修改普通用户的密码，而普通用户只能修改自己的密码

---

修改 root 用户密码

1）方式一：使用 mysqladmin 命令 修改 root 用户密码

```sql
mysqladmin -u username [-h hostname] -p password new_password
```

-p 后面的 password 为关键字

上面语句执行时，会提示输入旧密码

2）方式二：使用 UPDATE 语句修改 root 用户密码，此方式需要手动刷新权限表

```sql
UPDATE mysql.user SET Password = PASSWORD('mypwd2') WHERE User = 'root' and Host = 'localhost'

FLUSH PRIVILEGES;
```

3）方式三：使用 SET 语句修改 root 用户的密码

```
SET PASSWORD = password('123456');
```

要使用 PASSWORD()函数加密，并且新密码需要使用引号括起来

---

root 用户修改普通用户密码

1）方式一：使用 GRANT 语句修改普通用户密码

为不影响当前用户的权限，可以使用 GRANT USAFE 语句

```sql
GRANT USAFE ON *.* TO 'username'@'localhost' IDENTIFIED BY [PASSWORD] 'new_password'
```

2）方式二：使用 UPDATE 语句修改普通用户密码

跟root用户的相同

3）方式三：使用 SET 语句修改普通用户密码

使用 SET 语句修改普通用户密码时，需要使用 FOR 指定要修改哪个用户

```sql
SET PASSWORD FOR 'username'@'hostname'=PASSWORD('new_password');
```


### 如何找回 root 用户密码？（Windwos）

步骤一：在“运行”对话框中，使用 net 命令停止 MySQL 服务

```
net stop mysql
```

步骤二：在“运行”对话框中，使用`--skip-grant-tables`启动MySQL服务

```
--skip-grant-tables
```

步骤三：重新开启一个“运行”对话框，登录MySQL服务器

```sql
mysql -u root
```

步骤四：使用 UPDATE 语句设置root用户密码

```sql

```

步骤五：加载权限表

```sql
FLUSH PRIVILEGES;
```
