---
title: "Redis-6-应用场景"
date: 2018-06-05T10:06:47+08:00
draft: true
categories: ["数据库"]
---


# 概述

![](https://gitee.com/leafsing/pic/raw/master/img/20200709091834.png)

# String

## 分布式锁（简单版）

使用`SETNX key value`

![](https://gitee.com/leafsing/pic/raw/master/img/20200709091907.png)

## 文章阅读量

![](https://gitee.com/leafsing/pic/raw/master/img/20200709091924.png)

redis是单线程的，可以保证原子性

# Hash

## 对象缓存

![](https://gitee.com/leafsing/pic/raw/master/img/20200709091943.png)

## 电商购物车

![](https://gitee.com/leafsing/pic/raw/master/img/20200709092037.png)

![](https://gitee.com/leafsing/pic/raw/master/img/20200709092057.png)


# List

![](https://gitee.com/leafsing/pic/raw/master/img/20200709092138.png)

![](https://gitee.com/leafsing/pic/raw/master/img/20200709092207.png)


# Set

## 点赞、收藏

![](https://gitee.com/leafsing/pic/raw/master/img/20200709092219.png)


## 抽奖

![](https://gitee.com/leafsing/pic/raw/master/img/20200709092229.png)


## 集合操作

![](https://gitee.com/leafsing/pic/raw/master/img/20200709092240.png)

![](https://gitee.com/leafsing/pic/raw/master/img/20200709092250.png)

![](https://gitee.com/leafsing/pic/raw/master/img/20200709092304.png)


# SortedSet-Zset

![](https://gitee.com/leafsing/pic/raw/master/img/20200709092318.png)


# 更多应用场景

![](https://gitee.com/leafsing/pic/raw/master/img/20200709092338.png)


# 图形界面工具

- https://redisdesktop.com/
- https://gitee.com/qishibo/AnotherRedisDesktopManager

