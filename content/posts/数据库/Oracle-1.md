---
title: "Oracle-1"
date: 2018-06-04T10:01:47+08:00
draft: true
categories: ["数据库"]
---

# 用户

## 新建用户

使用系统用户创建新用户时，一般赋予3个权限即可

- connect
- dba
- resource


## 修改用户密码

```sql
sqlplus/ as sysdba

SQL> alter user sys identified by admin;
用户已更改。
SQL> conn sys/admin as sysdba;
已连接。
```

## 删除一个用户及其相关数据

```sql
drop user CHAXUN_CMP_ZCFG cascade
```

# 表空间

## 查看表空间（所在目录）

```sql
SELECT * FROM Dba_Data_Files;
```


## 创建表空间（win)

```sql
CREATE TABLESPACE CSSP_TBS
         DATAFILE  'D:\DataBase\oracle\oradata\orcl\CSSP_TBS.DBF'
         SIZE 100M
         AUTOEXTEND ON
         NEXT 32M MAXSIZE UNLIMITED
         EXTENT MANAGEMENT LOCAL;
```

## 查看表空间

- 查询所有的表空间

```sql
select distinct tablespace_name from dba_free_space
```

- 查询用户或表所用表空间

```sql
select owner,table_name,tablespace_name from dba_tables;
```


## 删除表空间

```
-删除空的表空间，但是不包含物理文件
drop tablespace tablespace_name;
--删除非空表空间，但是不包含物理文件
drop tablespace tablespace_name including contents;
--删除空表空间，包含物理文件
drop tablespace tablespace_name including datafiles;
--删除非空表空间，包含物理文件
drop tablespace tablespace_name including contents and datafiles;
--如果其他表空间中的表有外键等约束关联到了本表空间中的表的字段，就要加上CASCADE CONSTRAINTS
drop tablespace tablespace_name including contents and datafiles CASCADE CONSTRAINTS;
```

# 数据库导入导出

windows下在cmd窗口输入如下命令

```
导出：
exp sisp_public/sisp_public@10.165.0.13/testdb owner='sisp_public'  file=D:\tecsun\sisp_public_20161012.dmp   log=D:\tecsun\sisp_public_20161012.log
exp sisp/sisp@10.165.0.13/testdb owner='sisp'  file=D:\tecsun\sisp_20161012.dmp   log=D:\tecsun\sisp_20161012.log

导入：
imp zzgs/j2yx@192.168.5.71/ZZWATER100  file=E:\zzgs20160306.dmp full=y  log=E:\impzzgs_2016-03-06.log
```


# 查询



## 分页

子查询利用rownum进行分页，做模板代码用

```
select * from (
    select rownum rn, b.* from (
        select * from a 
    ) b where rownum <= 40
) where rn >= 30;
```

在rownum的基础上，另外还可以用rowid加快查询速度？？？

```
select * from t_xiaoxi where rowid in (
    select rid from (
        select rownum rn,rid from (
            select rowid rid,cid from t_xiaoxi order by cid desc
        )
        where rownum < 10000
    )
    where rn > 9980
) order by cid desc;
```

资料参考：

![image](https://oscimg.oschina.net/oscnet/f5a7f557679d1d798faf4cbc989c37afc20.jpg)

## where、group by、having

同时使用时，where用于筛选由from指定的数据对象；group by 用于对where的结果进行分组；having则是对group by 子句以后的分组数据进行过滤。

## 左右连接

（一）非标准外连接

![image](https://oscimg.oschina.net/oscnet/febc3e6bfdf55862a84dfa3368f46aeca8a.jpg)

（二）标准外连接

![image](https://oscimg.oschina.net/oscnet/1ff28545956738bf1d0a6db965173cfbef5.jpg)

（三）自连接

![image](https://oscimg.oschina.net/oscnet/25a2db5e08c92772e1da74a72e6b7ad5908.jpg)

## 使用union

1、使用 union 组合两个查询的结果集的基本规则如下：

1）所有查询中的列数和列的顺序必须相同；2）数据类型必须兼容。（默认去除重复列）
    
2、having与where：

1）having子句可以使用统计函数，where子句不可以；2）where,group by ,having同时使用时，注意顺序。
    
3、内、外连接

1）系统默认内连接；2）外连接：left join 左边的全出来； right join 右边的全出来

![image](https://oscimg.oschina.net/oscnet/ebf36eb69e72a41900a96045ac2f90ff5c9.jpg)

## 视图

- 可更新视图

    没有使用连接函数、集合函数、组函数。
    
- 插入数据

    单表视图可插入，多表不可以。
    
- 修改数据

    1）……；2）多表，一次只能修改一个表
    
- 删除数据

![image](https://oscimg.oschina.net/oscnet/c2bab9fe829cb78ce625a86c05673d6e4ba.jpg)

# 序列

```
创建序列，删除序列，得到序列的例子 

序列的创建
create sequence seq_newsId
increment by 1
start with 1
maxvalue 999999999;

得到序列的SQL语句

select seq_newsid.nextval from sys.dual;

删除序列的SQL

   DROP SEQUENCE seq_newsId；
```

语法：
```
    CREATE SEQUENCE [user.]sequence_name
    [increment by n]
    [start with n]
    [maxvalue n | nomaxvalue]
    [minvalue n | nominvalue];
```

MAXVALUE最大值是28个9，用科学计数法将最大值记做1E28，换句话说，即使设置NOMAXVALUE，也是有这个最大值的。

NOCYCLE是默认值。

修改序列：
```
    ALTER SEQUENCE [user.]sequence_name
    [INCREMENT BY n]
    [MAXVALUE n| NOMAXVALUE ]
    [MINVALUE n | NOMINVALUE]；
```

# 函数

## 系统函数

### 常用函数

- decode()

    if...then...else
    
    ![decode](06B37BB7BEEA44FF882A5CF04DB70B17)

- nvl(expr1,expr2)
- nv2(expr1,expr2,expr3)
- nullif(expr1,expr2)
- coalesce(expr1,expr2,...,exprn)


### 字符串函数

- length(char)，如`select * from user where length(name)=5`
- upper(char)，转换成大写
- lower(char)，转换成小写
- substr(char,m,n)，截取字符串

    以首字母大写的方式显示所有员工的姓名

    `select upper(sbustr(ename,1,1)) || lower(substr(ename,2,length(ename)-1)) from emp;`

### 数字函数

- trunc(n,[m])
- ceil(n)
- floor(n)
- mod(m,n)
- round(n,[m])，四舍五入，round(3.222,2)=3.22，round(3.222)=3

### 日期函数

- add_month(日期，月数)

    `select * from emp where sysdate > add_month(hiredate,300);`

- trunc

    `select trunc(sysdate-hiredate) "入职天数",ename from emp;`
    
    ![trunc](C6E249B0D8F44E8089D727D9AF417865)

### 转换函数

- to_char()

    日期 to_char(hiredate,'yyyy-mm-dd hh24:mi:ss')，货币 to_char(sal,'L99999.99')，如 RMB999.00

- to_date()

    ![exchage](1177CD3B3F5C423C84994218299396E1)
    ![exchage2](C031BB88B3224C91873267FDECD44FB3)
    ![exchage3](5E3DF279148644C6A18E6682FF695C6E)





## 创建、调用函数

函数的参数只能是in模式

附参数模式：[image](https://oscimg.oschina.net/oscnet/9d0cef25223d1ffa08edcbd362a42f32996.jpg)

firstFun：

![image](https://oscimg.oschina.net/oscnet/941b129a55ec06e76c1a0fbc80d35a378d4.jpg)

调用firstFun：

![image](https://oscimg.oschina.net/oscnet/184482caf00c1370bea506b857906f5d924.jpg)

有参数的函数：

![image](https://oscimg.oschina.net/oscnet/03ce5dbb349b5f1927235d0bd49f9196eba.jpg)

调用函数：

![image](https://oscimg.oschina.net/oscnet/0cb016aee31f712f376d239957a5ce750b8.jpg)

# 储存过程

储存过程与函数的唯一区别：函数有返回值，储存过程没有。？？？

无参的储存过程：
![image](https://oscimg.oschina.net/oscnet/4626f1e664258344fbe6bd81cf740385be7.jpg)

有参的储存过程：
![image](https://oscimg.oschina.net/oscnet/1faa668d62f6be660f1de05a9e6c6a2226c.jpg)

## 游标

（一）没有使用循环

![image](https://oscimg.oschina.net/oscnet/be895080586624707e56fb2de84766acdbf.jpg)

（二）使用循环

![image](https://oscimg.oschina.net/oscnet/e0bcb3c26ef1aec101193076840f4e8fd43.jpg)

（三）有内循环的

![image](https://oscimg.oschina.net/oscnet/659435cf024248d11f12fb266c1a3cfc2bc.jpg)
![image](https://oscimg.oschina.net/oscnet/79221ee25798ae969dfc6f2d7a697fb05b9.jpg)

<a href="#查询">首页</a>