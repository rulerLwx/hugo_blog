---
title: "Redis-2-过期、安全、排序、消息、QA"
date: 2018-06-05T10:02:47+08:00
draft: true
categories: ["数据库"]
---


# 过期时间

```sh
redis> expire key seconds   # 设置键的过期时间，单位是秒

redis> ttl key      # 键剩余时间，秒。键不存在时，返回-2；永久（默认），返回-1。

redis> pexpire key milliseconds # 设置过期时间，单位毫秒

redis> pttl key     # 键剩余时间，毫秒。

redis> persist key  # 将键设为永久，或调用 set/getset 等命令为键重新赋值时，会清除过期时间
```

实现缓存：

- 设置redis能够使用的最大内存：maxmemory    (单位字节)
- maxmemory-policy xxx

# 排序

- 有序集合本身有排序功能 
- sort命令

    - `sort key [alpha|desc] [limit offset count]`
    - sort命令可以对列表、集合、有序集合类型键进行排序
    - 对有序集合类型排序时会忽略元素的分数，只对元素自身排序
    - by参数
    - get参数
    - store参数

# 消息通知

- 任务队列

    - redis> brpop key [key ...] timeout   # 取出第一个不为空的key的右边第一个元素。timeout，阻塞时间，timeout=0表示一直阻塞
    - redis> blpop key [key ...] timeout   # 取出第一个不为空的key的左边第一个元素。
    - 注：上述中的key可以多个，排在前面的优先级高
- 发布/订阅
    - redis> publish channel message    # 发布者发布消息，发出的消息不会被持久化，之前发送的就收不到了
    - redis> subcribe channel [channel ...]    # 订阅，之后就不能使用除subscribe、unsubscribe、psubscribe、punsubscribe之外的命令，否则报错
    - redis> unsubscribe [channel [channel ...]] # 取消订阅

# 管道

redis客户端和服务端的通信是通过TCP协议连接，网络传输会花费时间。通过管道将一组都不依赖之前命令的执行结果的命令发送和返回结果，可以降低往返时延。

# 安全（密码）

- 可信环境：在生产环境不允许外界直接连接到redis服务器上，而应该通过应用程序进行中转。因此建议配置 `bind 127.0.0.1`。
- 数据库密码
```
# 配置文件中配置数据库密码：
requirepass tafk(@~!ji

# 设置访问主服务器密码
masterauth 123456
```
> 两个密码保持一致

每次连接时，都需要发送密码，如：
```
redis> get foo
(error)
redis> auth tafk(@~!ji
ok
```

可以在登录时输入密码：

```
redis-cli -h 192.168.12.132 -p 6379 -a pass
```
参考 https://www.cnblogs.com/suanshun/p/7699084.html

# 通信协议

- 简单协议
- 统一请求协议

通信协议主要用于写redis客户端

# Q & A

##  一台机器上启动多个redis实例

    + 思路：用同一个redis安装文件，指定不同的端口、数据保存位置、pidfile
    + 1)复制一个redis配置文件，重命名
    + 2)修改三个配置：pidfile、port、rdbfile
    + 3)redis-server指定配置文件：`redis-server redis_6380.conf`
    + 参考：[在一台linux机器上启动多个redis实例:学习redis的master-slave功能][3]

## xshell命令行中文是"\xe5\xa5\xb3"

启动redis-cli时增加一个参数`--row`

```
 $ redis-cli -h 192.168.12.132 -p 6379 --row
```

[1]:http://www.cnblogs.com/panjun-Donet/archive/2010/08/10/1796873.html
[2]: http://blog.csdn.net/gxw19874/article/details/51992125
[3]: http://blog.csdn.net/aitangyong/article/details/52055932