---
title: "Redis-5-性能测试"
date: 2018-06-05T10:05:47+08:00
draft: true
categories: ["数据库"]
---

# Redis性能测试

## redis-benchmark 工具简介

redis-benchmark是官方自带的Redis性能测试工具，用来测试Redis在当前环境下的读写性能。我们在使用Redis的时候，服务器的硬件配置、网络状况、测试环境都会对Redis的性能有所影响，我们需要对Redis实时测试以确定Redis的实际性能。

使用语法：`redis-benchmark [参数] [参数值]`

参数说明：

选项 | 描述 | 默认值
---|---|---
-h |	指定服务器主机名	127.0.0.1
-p | 指定服务器端口	6379
-s |	指定服务器 socket	 
-c |	指定并发连接数	50
-n |	指定请求数	10000
-d |	以字节的形式指定 SET/GET 值的数据大小	2
-k |	1=keep alive 0=reconnect	1
-r |	SET/GET/INCR 使用随机 key, SADD 使用随机值	 
-P |	通过管道传输 请求	1
-q |	强制退出 redis。仅显示 query/sec 值	 
--csv |	以 CSV 格式输出	 
-l |	生成循环，永久执行测试	 
-t |	仅运行以逗号分隔的测试命令列表。	 
-I |	Idle 模式。仅打开 N 个 idle 连接并等待。	 

示例：`./redis-benchmark -t set,get -n 100000`

```
...
99.98% <= 67 milliseconds
100.00% <= 68 milliseconds
100.00% <= 68 milliseconds
16952.03 requests per second
...
```

在上面的测试结果中，我们关注GET结果最后一行 52110.47 requests per second ，即每秒GET命令处理52110.47个请求，即**QPS**5.2万。但这里的数据都只是理想的测试数据，测出来的QPS不能代表实际生产的处理能力。


## TPS、QPS、RT

在描述系统的高并发能力时，吞吐量（TPS）、QPS、响应时间（RT）经常提到，我们先了解这些概念：

- 响应时间RT
- 吞吐量TPS：T - Throughput，吞吐量
- 每秒查询率QPS


## 测试 redis 性能

1. 使用 redis-benchmark 工具

```
./redis-benchmark -t get -n 100000 -c 100 -d 2048
```

2. 使用 redis 自带的 info 命令

```
# 在客户端中执行info命令
127.0.0.1:6379> info
```

查看结果（摘取部分结果）：

```
connected_clients:101 #redis连接数
​
used_memory:8367424 # Redis 分配的内存总量 
used_memory_human:7.98M
used_memory_rss:11595776 # Redis 分配的内存总量(包括内存碎片) 
used_memory_rss_human:11.06M
used_memory_peak:8367424
used_memory_peak_human:7.98M #Redis所用内存的高峰值
used_memory_peak_perc:100.48%
```





