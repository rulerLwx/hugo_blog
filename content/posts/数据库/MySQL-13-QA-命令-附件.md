---
title: "MySQL-4-QA-命令-附件"
date: 2018-06-03T10:03:47+08:00
draft: true
categories: ["数据库"]
---

# 常用命令

```sql
select version();#查看mysql版本
show engines;#查看支持哪些引擎
show global variables like "%datadir%";#查看存储文件的位置
desc logs;#查看表结构
```


# 附件

## maven

```
<!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java -->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>5.1.19</version>
</dependency>

```

## 链接符

```xml
# mysql database connection info
jdbc.mysql.driverClassName=com.mysql.jdbc.Driver
jdbc.mysql.url=jdbc:mysql://localhost:3306/test
jdbc.mysql.username=root
jdbc.mysql.password=root

```

## 测试MySql是否连通了

```java
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/test", "root", "root");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt
                    .executeQuery("select * from basic_person_info ");
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String certNum = rs.getString(3);
                System.out.println("id:" + id + ",name:" + name + ",certNum:"
                        + certNum);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

```

## mysql的连接java代码测试

```
    @Test
    public void testJdbc(){
        String driver="com.mysql.jdbc.Driver";   //获取mysql数据库的驱动类
        String url="jdbc:mysql://localhost:3306/test"; //连接数据库（kucun是数据库名）
        String name="root";//连接mysql的用户名
        String pwd="root";//连接mysql的密码
        try{
            Class.forName(driver);
            Connection conn= DriverManager.getConnection(url,name,pwd);//获取连接对象
            Statement statement = conn.createStatement();
            String sql = "select * from user";
            ResultSet rs = statement.executeQuery(sql);
            while(rs.next()) {
                System.out.println(rs.getString("id")+"");
            }
            conn.close();

        }catch(Exception e){
            e.printStackTrace();
        }
    }
```

## 默认编码

查看默认编码： 

```
show variables like 'character%';
```

修改默认编码：
修改my.ini文件，

![clipboard](972DE1DAB6164A17A4D3AE1497FBE43A)

修改数据库默认编码：

```
alter database hibernate0909 character set utf8;
```

## mysql 日期加减天数

date_add()

参考：https://blog.csdn.net/asdkwq/article/details/77881850

## mysql计算连续天数，mysql连续登

https://fanshuyao.iteye.com/blog/2341455

# 错误

## Error Code: 1093

Error Code: 1093. You can't specify target table 'score' for update in FROM clause

```
当执行以下sql语句时会出现 Error Code：1093 错误：

update car set tag = 1 where id in (select id from car where brand_id=182 and tag=0);
1
出现错误的原因是因为修改的表和查询的表是同一个表，MySQL是不允许这样做的，我们可以通过中间再查询一次来解决：

update car set tag = 1 where id in (select id from (select id from car where brand_id=182 and tag=0) As temp);
--------------------- 
原文：https://blog.csdn.net/u010657094/article/details/64439486 
```

## Error Code: 1093. You can't specify target table 'student_info' for update in FROM clause

这个是我们在使用update或者delete语句时，在where条件里面加入的子查询导致的。例如如下的update语句：

```
update table set type = 'static' where id in (
select id from ws_product where first_name ='superman'
);
```

修改上述语句为下面这样，该问题可以解决：

```
update ws_product set type = 'static' where id in (
select id form (
 select id from ws_product where first_name ='superman'
) xx
);
```

注意，这样一定要给最里面的子查询定义一个别名，不然会报另外一个错误：

原文：https://blog.csdn.net/xuehuagongzi000/article/details/77825579


# FAQ

## MySQL界面工具

- MySQL Workbeanch
- HeidiSQL

## com.mysql.jdbc.Driver 和 com.mysql.cj.jdbc.Driver 的区别 

```txt
com.mysql.jdbc.Driver 是 mysql-connector-java 5.x版本的，
com.mysql.cj.jdbc.Driver 是 mysql-connector-java 6.x版本中的
```

参考：https://blog.csdn.net/superdangbo/article/details/78732700

## DELETE/TRUNCATE/DROP 的区别


## 复合索引

 1、对于复合索引,在查询使用时,最好将条件顺序按找索引的顺序,这样效率最高;     select * from table1 where col1=A AND col2=B AND col3=D     如果使用 where col2=B AND col1=A 或者 where col2=B 将不会使用索引
 
 https://www.cnblogs.com/summer0space/p/7247778.html