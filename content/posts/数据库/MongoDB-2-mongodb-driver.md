---
title: "MonggoDB-2"
date: 2020-07-03T18:56:47+08:00
draft: true
categories: ["数据库"]
---


mongodb-driver是mongo官方推出的java连接mongoDB的驱动包，相当于JDBC驱动。我们现在来使用mongodb-driver完成对Mongodb的操作。

# 环境准备

创建工程，并添加以下依赖：

```
<dependency>
    <groupId>org.mongodb</groupId>
    <artifactId>mongodb-driver</artifactId>
    <version>3.10.1</version>
</dependency>
```

# 代码示例

## 查询所有

```java
@Test
public void test1() {
    //创建连接
    MongoClient client = new MongoClient("192.168.200.128");
    //打开数据库
    MongoDatabase commentdb = client.getDatabase("commentdb");
    //获取集合
    MongoCollection<Document> comment = commentdb.getCollection("comment");
​
    //查询
    FindIterable<Document> documents = comment.find();
​
    //查询记录获取文档集合
    for (Document document : documents) {
        System.out.println("_id：" + document.get("_id"));
        System.out.println("内容：" + document.get("content"));
        System.out.println("用户ID:" + document.get("userid"));
        System.out.println("点赞数：" + document.get("thumbup"));
    }
    //关闭连接
    client.close();
}
```

## 根据_id查询

每次使用都要用到MongoCollection，进行抽取：

```java
private MongoClient client;
private MongoCollection<Document> comment;
​
@Before
public void init() {
    //创建连接
    client = new MongoClient("192.168.200.128");
    //打开数据库
    MongoDatabase commentdb = client.getDatabase("commentdb");
    //获取集合
    comment = commentdb.getCollection("comment");
}
​
@After
public void after() {
    client.close();
}
```

 

测试根据_id查询：

```java
@Test
public void test2() {
    //查询
    FindIterable<Document> documents = comment.find(new BasicDBObject("_id", "1"));
​
    //查询记录获取文档集合
    for (Document document : documents) {
        System.out.println("_id：" + document.get("_id"));
        System.out.println("内容：" + document.get("content"));
        System.out.println("用户ID:" + document.get("userid"));
        System.out.println("点赞数：" + document.get("thumbup"));
    }
}
```

## 新增

```java
@Test
public void test3() {
    Map<String, Object> map = new HashMap();
    map.put("_id", "6");
    map.put("content", "很棒！");
    map.put("userid", "9999");
    map.put("thumbup", 123);
​
    Document document = new Document(map);
​
    comment.insertOne(document);
}
```

## 修改

修改注意 $set

```java
@Test
public void test4() {
    //修改的条件
    Bson filter = new BasicDBObject("_id", "6");
    //修改的数据
    Bson update = new BasicDBObject("$set", new Document("userid", "8888"));
​
    comment.updateOne(filter, update);
}
```

但，在使用SpringDateMongoDB时，修改使用的是 save() 方法，此时不用注意 $set


##  删除

```java
@Test
public void test5() {
    //删除的条件
    Bson filter = new BasicDBObject("_id", "6");
​
    comment.deleteOne(filter);
}
```



