---
title: "Redis-1-安装、结构、命令、配置"
date: 2018-06-05T10:01:47+08:00
draft: true
categories: ["数据库"]
---

# NoSql

## 数据库模型

1）传统数据库模型

一对一，一对多，多对多，主键、外键等

2）NoSQL数据模型

BSON（Binary JSON）模型：是一种类JSON的二进制的存储格式，支持内嵌的文档对象和数组对象。

理解成 JSON 即可。——20190916

3）两者对比

- 高并发操作不太建议有关联查询
- 互联网公司用冗余数据来避免关联查询
- 分布式事务支持不了太多的并发

NoSQL不需要像传统数据库那样 关联 查询，一个 JSON 就可以存储大量的不同类型的数据。——20190916

## 四大分类

- K/V键值：Redis
- 文档型数据库（bson）：MongoDB、CouchDB
- 列储存数据库：HBase、Cassandra、分布式文件系统
- 图关系数据库：

四者对比：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709090027.png)

## CAP原理

什么是CAP？
- C：Consistency，强一致性
- A：Availability，可用性
- P：Partition tolerance，分区容错性

CAP的3选2：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709090101.png)

## 分布式 VS 集群

- 分布式：不同的多台服务器上面部署**不同的服务模块**（工程），他们之间通过RPC/RMI通信和调用， 对外提供服务和组内协作。
- 集群  ：不同的多台服务器上面部署**相同的服务模块**，通过分布式调度软件进行统一的调度，对外提供服务和访问。




# 存储结构

Redis，Remote Dictionary Server，远程字典服务器。

Redis支持的键值数据类型：

- 字符串 string
- 散列 hash
- 列表 list
- 集合 set
- 有序集合 zset

# 安装

## linux-生产环境-源码安装方式

注意：需要先安装 gcc

```
$ wget http://download.redis.io/releases/redis-3.2.10.tar.gz
$ tar zxvf redis-3.2.10.tar.gz
$ cd redis-3.2.10
$ make

$ sudo make install     # 将redis-3.2.10/src下面的redis-cli、redis-server等复制到 /usr/local/bin/

$ sudo cp utils/redis_init_script /etc/init.d/redis_6379    # 将启动脚本复制到 /etc/init.d/，并改名为 redis_6379
$ sudo mkdir /etc/redis         # 创建文件夹，用于存放redis配置文件
$ sudo mkdir -p /var/redis/6379 # 创建文件夹，用于存放redis持久化文件
$ sudo cp redis.conf /etc/redis/6379.conf # 将配置文件模板复制到/etc/redis，并改名为6379.conf

$ vim /etc/redis/6379.conf      # 修改配置，设为以下值：
-----------------------------------------------------------------
1、在6379.conf的头部注释中（可以是最开始位置，或中间位置）添加两行配置：
# Comments to support chkconfig on RedHat Linux
# chkconfig:   2345 90 10
# description:  Redis is a persistent key-value database

2、
daemonize yes                       # 设为后台进程
pidfile /var/run/redis_6379.pid     # redis的PID文件位置
port 6379                           # redis监听端口号
dir /var/redis/6379                 # 持久化文件存放位置
-----------------------------------------------------------------

$ sudo chkconfig --add redis_6379
```

注意：配置开机自启时，要做三件事

1. 将启动脚本放在/etc/init.d目录下，脚本名称可以随意
2. 在启动脚本头部注释中，增加两行RHT系列特有的注释
3. sudo chkconfig --add redis_6379 命令将脚本注册到开机启动入口中

参考资料：

- [vLinux下chkconfig命令详解][1]
- [service redis does not support chkconfig的解决办法][2]

### 各种配置

- 开启远程访问

类似“本机客户端连接虚拟机Redis服务”，修改redis.conf文件

1、绑定Redis所在服务器的IP

```
# bind 127.0.0.1
bind 192.168.12.132
```

2、关闭受保护模式

```
protected-mode no
```

### 启动服务端

- 上面安装时，已经将redis服务端配置为开机启动。
- 若需手动启动服务端，则执行：`redis-server`或`redis-server --port 6379`。
- 若需关闭服务端，则执行`redis-cli SHUTDOWN`。

### 启动客户端

上面开机启动时，绑定了IP，因此启动客户端时，要连接上IP和端口，执行`redis-cli -h 192.168.12.132 -p 6379`

## 安装2（linux）

- yum install gcc-c++ # 需要编译器环境
- tar -zxf redis-3.0.0.tar.gz
- cd redis-3.0.0
- make
- make PREFIX=/usr/local/redis install # 将Redis 安装到指定目录
- cp redis.conf /usr/local/redis/bin # 先将redis.conf 文件拷贝到redis 的安装目录
- 编辑redis.conf 文件修改：daemonize yes
- ./redis-server redis.conf # 后台启动 redis

参考尚学堂《Spring Data》之《Spring Data Redis》视频

## 安装（windows）

客户端下载：https://github.com/MicrosoftArchive/redis/releases

以 Redis-x64-3.2.100 压缩包安装方式 为例，启动redis服务：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709090143.png)

redis中的文件（如redis-server redis-cli）是独立的文件，可以复制到任何地方单独启动，Linux下也一样

# 卸载（Linux）

卸载安装目录中的文件，如`/usr/local/bin`下的`redis-*`文件，如`rm -rf redis-*`。——20190913


# 客户端常用命令

可参考在线文档（API）：
- http://doc.redisfans.com/
- http://redisdoc.com/index.html

## Key

- redis> keys *       # 查询所有的键值，，redis命令不区分大小写，keys 与 KEYS相同
- redis> keys string*          # 可以找出相同前缀的键
- redis> select 1     # 选择数据库
- redis> exists bar   # 键是否存在，1_是，0_否
- redis> del key [key ...]    # 删除键
- redis> type foo     # 键的类型（string/hash/list/set/zset）

文档大多命令来自《Redis入门指南（第2版）》——李子骅


## 字符串(string)

```sh
redis> set foo 1    # 设置键及值

redis> get foo      # 取值

redis> incr num     # 数字递增

redis> incrby num 2 # 每次递增2

redis> decr num     # 数字递减

redis> decrby num 2    # 数字递减，每次2

redis> incrbyfloat key increment    # 增加指定浮点数

redis> append key " world"      # 向尾部追加值

redis> strlen key   # 返回键对应的值的长度

redis> mget key [key ...]               # 同时获得多个值 mget/mset与set/get用法相同，只是可以获取/设置多个值

redis> mset key value [,key value ...]  # 同时设置多个值
```

- getset key value，自动将key对应到value并且返回原来key对应的value
- SETNX key value ，只在键 key 不存在的情况下， 将键 key 的值设置为 value 。若键 key 已经存在， 则 SETNX 命令不做任何动作。命令在设置成功时返回 1 ， 设置失败时返回 0 。


## 列表(list)

双向队列（double linked list）。

![](https://gitee.com/leafsing/pic/raw/master/img/20200709090227.png)

*与key对应的是一个链表，链表具有索引*，一个列表类型键最多包含2^32-1个元素

```sh
redis> lpush key value [value ...]      # 向列表左边增加元素

redis> rpush key value [value ...]      # 向列表右边增加元素

redis> lpop key     # 弹出左边的元素，原理：1）将列表左边的元素移除，2）返回被移除的元素值

redis> rpop key     # 弹出右边的元素

注：lpush和lpop或rpush和rpop配合，可以将列表当成栈；lpush和rrop或rpush和lpop配合，可以将列表当成队列。

redis> llen key     # 列表中元素的个数

redis> lrange key start stop  # 返回索引从start到stop之间的所有元素（包括两端元素），索引从0开始。
                              # 此方式跟`lrop`命令不同，不移除元素。
                              # 支持负索引，-1表示最右边的第一个元素，-2表示最右边的第二个元素，依次类推。
                              # 因此，`lrange key 0 -1`表示列表的所有元素 。

redis> lrem key count value     # 删除列表中前count个值为value的元素。
                                # count > 0，从列表左边开始删除前count个值为value的元素
                                # count < 0，从列表右边开始删除前count个值为value的元素
                                # count = 0，删除列表中所有值为value的元素

redis> lindex key index         # 获取指定索引的值,索引从0开始，支持负索引，-1表示最右边的第一个元素，依次类推。

redis> lset key index value     # 设置指定索引值的键的值

注：列表可以存放相同key的元素，因此要指定key和index（key表示名为key的一个list，然后设置list中指定索引的值）


redis> ltrim key start end      # 删除指定索引范围之外的所有元素（注：只删除指定key的的元素）

redis> linsert key before|after pivot value     # 在（列表中）值为pivot元素的前或后插入value值

redis> rpoplpush source destination     # rpop命令和lpush命令的组合，将元素从一个列表转到另一个列表中
```



## 散列(hash)

散列存储的键（key)对应的值（value)是字段与字段值，其中字段值只能是字符串（即不支持类型嵌套），其它数据类型同理。

*与key对应的是一个键值对（字段、字段值）*，一个散列类型键最多有2^32-1个字段。

```
redis> hset key field value

redis> hget key field

redis> hmset key field value [field value...]

redis> hmget key field [field ...]

redis> hgetall key      # 返回 key 指定的哈希集中所有的字段和值

redis> hexists key field        # 判断键是否存在 

redis> hsetnx key field value   # 当前字段不存在时赋值

redis> hincby key field increment # 增加数字，注意：没有 hinc 命令

    > hincby drinks amount 5    # 增加5
    > hincby drinks amount -3   # 减少3

redis> hdel key field [field ...]

redis> hkeys key    # 只获取字段名

redis> hvals key    # 只猎取字段值

redis> hlen key     # 获取字段数量
```

自我总结：key 相当于 一个分类（文件夹），后面的 file:value 才是主角——20190427


## 集合(set)

集合(set)，无序，不可重复

```sh
redis> sadd key member [member ...]     # 增加一个元素，无则添加，有则忽略

redis> srem key member [member ...]     # 删除一个元素，有则删除，无没忽略

redis> smembers key     # 返回集合中的所有元素（key表示一个名为‘key’的集合）

redis> sismember key member     # 判断元素是否在集合中

redis> sdiff key [key ...]      # 集合间的差集运算，如sdiff a b， 表示所有属于a且不属性b的元素构成的集合

redis> sinter key [key ...]      # 集合间交集运算，如sinter a b， 表示所有属于a且属于b的元素构成的集合

redis> sunion key [key ...]      # 集合间并集运算，如sunion a b， 表示所有属于a或属于b的元素构成的集合（重复的元素算一个元素）

redis> scard key        # 获取集合中元素个数

redis> sdiffstore|sinterstore|sunionstore destination key [key ...]     # 进行集合运算并将结果存储

redis> srandmember key [count]  # 从集合中随机获取集合中的元素，元素不从key中删除

redis> spop key     # 从集合中随机弹出一个元素，元素从key中删除
```

## 有序集合(zset)

在集合类型的基础上，有序集合为集合中的每个元素都关联了一个分数。

排序的时候是按分数排序。——20190913

```sh
redis> zadd key score member [score member]     # 增加元素，分数（score）可以是整数、双精度浮点数

redis> zscore key member    # 获取元素的分数

redis> zrange key start stop [withscores]       # 按元素分数从小到大顺序返回索引从start到stop之间的所有元素（包含两端）

redis> zrevrange key start stop [withscores]    # 按元素分数从大一小顺序返回索引从start到stop之间的所有元素（包含两端）

redis> zrangebyscore key min max [withscores]  [limit offset count] # 获取指定分数范围的元素(从小到大)

redis> zrevrangebyscore key min max [withscores]  [limit offset count] # 获取指定分数范围的元素（从大到小）

redis> zincrby key increment member     # 增加一个元素的分数（increment是要增加的数值）

redis> zcard key    # 获得集合中元素的数量

redis> zcount key min max   # 获得指定分数范围内的元素个数

redis> zrem key member [member ...]     # 删除一个或多个元素

redis> zremrangebyrank key start stop   # 按排名范围删除元素（分数从小到大） ？？？

redis> zremrangbyscore key min max      # 按分数范围删除元素 ？？？

redis> zrank key member         # 获取元素排名（分数小到大）

redis> zrevrank key member      # 获取元素排名（分数大到小）

redis> zinterstore destination numkeys key [key ...]  [weight weight  [weight ...]]  [aggrecate]

```

总结：set 和 zset 都是 set 。只不过 zset 多了 一个分数（在key和value之间添加一个分数），用于排序、区间划分。


# 配置文件（redis.conf）

## 目录位置

执行`make`命令后，配置文件在`redis-5.0.5/redis.conf`，一般要将其复制到其它文件夹，而不是直接修改它。

redis.conf的配置文件分为以下几个大部分：
- INCLUDES：可以包含其它配置文件
- MODULES
- NETWORK
- GENERAL：通用配置
- SNAPSHOTTING：快照，持久化
- REPLICATION：复制
- SECURITY
- CLIENTS
- MEMORY MANAGEMENT
- LAZY FREEING
- APPEND ONLY MODE
- LUA SCRIPTING
- REDIS CLUSTER
- SLOW LOG
- ACTIVE DEFRAGMENTATION

![](https://gitee.com/leafsing/pic/raw/master/img/20200709090312.png)


## 常用用配置

`k/kb/m/MB/g/BG` 单位大小写不敏感：`units are case insensitive so 1GB 1Gb 1gB are all the same.`

`port 6379`：

`daemonize no`：是否以后台方式启动

`bind 127.0.0.1`：只能本机访问。若要跨网络访问，配置成 0.0.0.0 （为安全，要配置密码登录）

`pidfile /var/run/redis_6379.pid`：进程管道ID文件

`tcp-backlog 511`：

![](https://gitee.com/leafsing/pic/raw/master/img/20200709090349.png)

`timeout 0`：

`tcp-keepalive 300`：单位为秒，如果设置为0，则不会进行keepalive检测，建议设成60

`loglevel notice`：

`logfile ""`：

`syslog-enabled no`：

`syslog-ident redis`：

`syslog-facility local0`：

RDB保存时间：
```
save 900 1
save 300 10
save 60 10000
```

`dir ./`：持久化文件存储位置，不建议是`./`，因为在不同的目录启动redis，存储文件就在不同目录。

`appendonly no`：

`appendfilename "appendonly.aof"`：

## 通过`redis-cli`修改配置

```
127.0.0.1:6379>config set requirepass 123456
```