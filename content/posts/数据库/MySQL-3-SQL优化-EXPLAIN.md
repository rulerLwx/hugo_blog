---
title: "MySQL-3-SQL-优化-EXPLAIN"
date: 2018-06-03T10:01:47+08:00
draft: true
categories: ["数据库"]
---


# EXPLAIN

## 各字段介绍

字段 | 说明
---|---
id | 编号；若编号相同，顺序从上往下执行，数据小的表先执行；若编号不同（有子查询时），编号越大越优先执行
select_type | 查询类型：<br> PRIMARY：主查询（最外层） <br> SUBQUERY：子查询（非最外层）<br> SIMPLE：简单查询（不包含子查询、union的查询） <br> DERIVED：衍生查询（使用到了临时表）
table | 当前查询的表
type | 索引类型：system > const > eq_ref > ref > range > index > all，一般能达到  ref 或 range 就行
possible_keys | 预测用到的索引，不准确
key | 实际用到的索引
key_len | 实际用到的索引长度，作用：用于判断复合索引是否被完全使用（a,b,c）
ref | 指明当前表所参照的字段
rows | 优化器预估的记录扫描行数
Extra |


## 关于 select_type

- PRIMARY：主查询（最外层）
- SUBQUERY：子查询（非最外层）
- SIMPLE：简单查询（不包含子查询、union的查询） 
- DERIVED：衍生查询（使用到了临时表），分两种情况
    - a）在`from 子查询`中，子查询只有一张表
    - b）在`from 子查询`中，子查询有table1 union table2，则table1就是derived，table2是union

```sql
select temp.name from (select * from  course where id = 1 union select * from course where id = 2) temp
```


## 关于 type

- system（忽略）: 只有一条数据的系统表 ；或 衍生表只有一条数据的主查询
- const：仅仅能查到一条数据的SQL ,用于Primary key 或unique索引  （类型 与索引类型有关）
- eq_ref：唯一性索引：对于每个索引键的查询，返回匹配唯一行数据（有且只有1个，不能多 、不能0）
- ref：非唯一性索引，对于每个索引键的查询，返回匹配的所有行（0，多）
- range：检索指定范围的行 ,where后面是一个范围查询(between,> < >=, 特殊：in有时候会失效，从而转为 无索引all)
- index：查询全部索引中数据
- all：查询全部表中的数据

总结

- system/const: 结果只有一条数据
- eq_ref:结果多条；但是每条数据是唯一的 ；
- ref：结果多条；但是每条数据是是0或多条 ；


## 关于 key_len

key_len 表示索引长度，也可理解为字节数

- utf8：1个字符占3个字节
- gbk：1个字符占2个字节
- latin：1个字符占1个字节

如果索引字段可以为Null，则会使用1个字节用于标识，+1

- char(20)：20*3=60 +  1(null) = 61
- varchar(20)：20*3=60 +  1(null)  +2(用2个字节 标识可变长度)  = 63
- int类型，长度固定为 4


## 关于 ref

指明当前表所参照的字段

示例：`select ....where a.c = b.x ;`，如果b.x是常量时，ref则是const

注意与type中的ref值区分


## 关于 rows

实际通过索引而查询到的数据个数


## 关于 Extra

- using filesort：性能消耗大，需要“额外”的一次排序（查询）。常见于 order by 语句中（排序之前得先查询）。
    - 对于单索引， 如果排序和查找是同一个字段，则不会出现using filesort；如果排序和查找不是同一个字段，则会出现using filesort；如何避免： where哪些字段，就order by那些字段2
    - 复合索引：不能跨列（最佳左前缀）；如何避免： where和order by 按照复合索引的顺序使用，不要跨列或无序使用
- using temporary：性能损耗大 ，用到了临时表。一般出现在group by 语句中。如何避免：查询那些列，就根据那些列 group by

- using index：性能提升，也称为索引覆盖（覆盖索引）。

  原因：不读取原文件，只从索引文件中获取数据（不需要回表查询），即，要查询的列全部都在索引中，就是 using index

- using where：需要回表查询

  什么是【回表查询】：无法从索引文件中查询到需要的列，需要到原表中查询

  假设age是索引列，但查询语句select age,name from ...where age =...,此语句中必须回原表查Name，因此会显示using where

- impossible where：where子句永远为false

  explain select * from test02 where a1='x' and a1='y'  ;


# 优化示例

## 单表

💃💃💃💃💃💃💃💃💃💃💃💃💃💃

## 两表

1）小表驱动大表

```
小表：10条数据
大表：300条数据
where 小表.x = 大表.y;
```

2）索引往哪张表加？   

- 小表驱动大表  
- 索引建立经常使用的字段上

比如：t.cid=c.cid可知，t.cid字段使用频繁，因此给该字段加索引。

一般情况对于左外连接，给左表加索引；右外连接，给右表加索引

## 三表

1. 小表驱动大表
2. 索引建立在经常查询的字段上


# 避免索引失效的一些原则

1）复合索引

- 不要跨列或无序使用（最佳左前缀）
- 尽量使用全索引匹配（复合索引全用上）

2）不要在索引上进行任何操作（计算、函数、类型转换），否则索引失效

对于**复合索引**，如果左边失效，右侧全部失效。比如复合索引 (a,b,c)，如果 b失效，则b c同时失效。

3）**复合索引**不能使用不等于（!=  <>）或is null (is not null)，否则自身以及右侧所有全部失效。

复合索引中如果有>，则自身和右侧索引全部失效

疑问：索引一样生效 ？？？？

![](https://gitee.com/leafsing/pic/raw/master/img/20200709085605.png)

解答：

> SQL优化，是一种概率层面的优化。原因是服务层中有SQL优化器，可能会影响我们的优化。一般而言， 范围查询（> <  in），之后的索引失效。

另：随机读与顺序读的优化，不是概率。可以了解下mrr

怎么办？使用索引覆盖（using index），即，使用全索引

4）like尽量以“常量”开头，不要以'%'开头，否则索引失效

```sql
select * from xx where name like '%x%' ; --name索引失效
```

5）尽量不要使用or，否则索引失效

```sql
explain select * from teacher where tname ='' or tcid >1 ; --将or左侧的tname 失效
```

6）尽量不要使用类型转换（显示、隐式），否则索引失效

```sql
explain select * from teacher where tname = 'abc' ;
explain select * from teacher where tname = 123 ;--程序底层将 123 -> '123'，即进行了类型转换，因此索引失效
```

# 其他的优化方法

## exist和in

- 如果主查询的数据量大，则使用In,效率高。
- 如果子查询的数据量大，则使用exist,效率高。

## order by 优化

using filesort 有两种算法：双路排序、单路排序 （根据IO的次数）

提高order by查询的策略：

1. 选择使用单路、双路 ；调整buffer的容量大小；
2. 避免select * ...  
3. 复合索引 不要跨列使用 ，避免using filesort
4. 保证全部的排序字段 排序的一致性（都是升序 或 降序）

## SQL排查 - 慢查询日志

1）慢查询日志默认是关闭的；

2）检查是否开启了 慢查询日志：   

```sql
show variables like '%slow_query_log%' ;
```

3）开启慢查询日志

a） 临时开启

```sql
set global slow_query_log = 1 ;  #在内存种开启
```

 重启mysql后失效

```shell
service mysql restart; #重启 mysql 服务
```


b） 永久开启

 /etc/my.cnf 中追加配置

```sql
[mysqld]
slow_query_log=1
slow_query_log_file=/var/lib/mysql/localhost-slow.log
```

 备注：原my.cnf文件没有相关配置，在 [mysqld] 下追加即可；配置后重启服务。


4）修改慢查询阈值

a） 临时修改

```sql
set global long_query_time = 5 ;
```

b） 永久修改

 /etc/my.cnf 中追加配置

```shell
vi /etc/my.cnf 

[mysqld]
long_query_time=3
```

5）模拟慢查询

```
select sleep(4);
```

6）查看慢查询

方式一：

```shell
cat /var/lib/mysql/localhost-slow.log
```

方式二：


# 锁机制

按操作类型：

1. 读锁（共享锁）： 对同一个数据（衣服），多个读操作可以同时进行，互不干扰。
2. 写锁（互斥锁）： 如果当前写操作没有完毕（买衣服的一系列操作），则无法进行其他的读操作、写操作

按操作范围：

1. 表锁 ：一次性对一张表整体加锁。如MyISAM存储引擎使用表锁，开销小、加锁快；无死锁；但锁的范围大，容易发生锁冲突、并发度低。
2. 行锁 ：一次性对一条数据加锁。如InnoDB存储引擎使用行锁，开销大，加锁慢；容易出现死锁；锁的范围较小，不易发生锁冲突，并发度高（很小概率 发生高并发问题：脏读、幻读、不可重复度、丢失更新等问题）。
3. 页锁	

```sql
SHOW OPEN TABLES;
```

# 主从同步

如何实现主从同步（原理）

![](https://gitee.com/leafsing/pic/raw/master/img/20200709085652.png)

对上图的文字解释：

1. master将改变的数据 记录在本地的 二进制日志中（binary log） ；该过程 称之为：二进制日志件事
2. slave将master的binary log拷贝到自己的 relay log（中继日志文件）中
3. 中继日志事件，将数据读取到自己的数据库之中

