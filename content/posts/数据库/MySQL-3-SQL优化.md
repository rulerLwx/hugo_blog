---
title: "MySQL-3-SQL-优化"
date: 2018-06-01T10:02:47+08:00
draft: true
categories: ["数据库"]
---

# 概念

## 逻辑分层

![](https://gitee.com/leafsing/pic/raw/master/img/20200709082955.png)

## 存储引擎

- InnoDB：事务优先（适合高并发操作，行锁）
- MyISAM：性能优先（表锁）

sql优化原因：sql语句欠佳、索引失效、服务器参数设置不合理（缓存、线程数）

## sql解析过程

1）编写顺序

```
select ... from ... join ... on ... where ... group by ... having ... order by ... limit ...
```

2）解析顺序

```
from ... on ... join ... where ... group by ... havint ... select ... order by ... limit ...
```

索引的数据结构

树：B+树（默认）、Hash树


# 存储引擎

存储引擎 = 算法 + IO

## ISAM

读非常快，但不支持事务

## MyISAM

特点：

- MyISAM是ISAM扩展格式，不支持事务
- 数据越多，写操作效率越低，因为要维护数据表索引

使用此引擎，生成三个文件：

- .frm：表结构信息
- .MYD：数据文件
- .MYI：表的索引信息

## InnoDB

InnoDB 存储引擎，5.7版本后，生成两个文件：

- .frm：表结构信息
- .idb：表的索引信息

特点：

- 支持事务
- 数据多版本读取
- 锁定机制的改进
- 实现外键


### InnoDB与MyISAM的区别

1）InnoDB支持事务，MyISAM不支持事务

对于 InnoDB ，每一条sql都默认封装成事务，自动提交，但这位会影响速度，所以最好将多条SQL语言放在 begin transaction 和 commit 之间，组成一个事务

2）InnoDB 支持外键，而 MyISAM 不支持。对一个包含外键的 InnoDB转换成 MyISAM 会失败

3）InnoDB 是聚集引擎，而  MyISAM 是非聚集引擎

InnoDB 是聚集引擎，数据文件和索引绑定在一起，必须要有主键，通过主键索引效率很高。但是辅助索引需要两次查询，先查询到主键，再通过主键查询到数据。因此，主键不应该过大，主键太大，其它索引也会很大。

MyISAM 是非聚集引擎，数据文件是分离的，索引文件保存的是数据文件的指针，主键索引和辅助索引是独立的。

> 注：聚集索引并不是在物理存储上是连续的，而是在逻辑上连续

4）InnoDB 不保存表的具体行数，执行 select count(*) from table 时需要全表扫描，而 MyISAM 会用一个变量保存了整个表的行数。

5）InnoDB 不支持全文检索，而 MyISAM 支持，查询效率上 MyISAM 更高。


总结：MySQL 可以实现同一个数据库中，不同的表应用不同的存储引擎；主从数据库里同一张表，数据引擎也可以不一样；

参考：https://zhidao.baidu.com/question/396976850982201885.html


## Memory

将数据存储在内存中的引擎


## 引擎常用命令

```sql
show engines;#查看支持的引擎
show variables like '%storage_engine%';#查看当前使用的存储引擎
show create table table_name;#查看table_name表使用的引擎
create table table_name(column_name,column_type) engine = engine_name#创建表时指定引擎
alter table table_name engine = engine_name;#修改表的引擎
```

如何修改默认的引擎？

在MySQL配置文件中修改`default-storage-engine=INNODB`

- windows在：MySQL安装目录/my.ini
- linux在：/etc/my.cnf


# 索引

## 什么字段适合创建索引

- 在经常需要搜索的列上
- 在作为主键的列上
- 在经常用在连接的列上，这些列主要是一些外键
- 在经常需要根据范围搜索的列上，此范围是连续的
- 在经常需要排序的列上
- 在经常用在where子句的列上

## MySQL索引种类

### B-Tree索引

B-Tree，balance tree

B-Tree中，每个结点包含：

1. 本结点所含 关键字 的个数
2. 指向父结点的指针
3. 关键字
4. 指向子结点的指针

对于一棵m 阶B-tree，每个结点至多可以拥有m 个子结点。各结点的关键字和可以拥
有的子结点数都有限制，规定m 阶B-tree 中，根结点至少有2 个子结点，除非根结点为叶
子节点，相应的，根结点中关键字的个数为1~m-1；非根结点至少有[m/2]（[]，向上取整）
个子结点，相应的，关键字个数为[m/2]-1~m-1。

B-tree 有以下特性：

1. 关键字集合分布在整棵树中；
2. 任何一个关键字出现且只出现在一个结点中；
3. 搜索有可能在非叶子结点结束；
4. 其搜索性能等价于在关键字全集内做一次二分查找；
5. 自动层次控制；

### Full-Tree索引

Full-text 索引就是我们常说的全文索引，他的存储结构也是b-tree。主要是为了解决在
我们需要用like 查询的低效问题。只能解决’xxx%’的like 查询。如：字段数据为ABCDE，索
引建立为 A、AB、ABC、ABCD、ABCDE 五个。

## 索引管理

在MySQL中，对索引的查看和删除操作是所有索引类型通用的，但创建方式则不一样。

### 普通索引

这是最基本的索引，它没有任何限制，也是我们大多数情况下用到的索引。

1）创建普通索引

```sql
CREATE INDEX index_name ON table_name (column(length));

ALTER TABLE table_name ADD INDEX index_name (column(length));

CREATE TABLE table_name (id int not null auto_increment,title varchar(30) ,PRIMARY
KEY(id) , INDEX index_name (title(5)));
```

2）查看索引

```sql
SHOW INDEX FROM [table_name];

SHOW KEYS FROM [table_name] # 只在MySQL 中可以使用keys 关键字。
```

3）删除索引

```sql
DROP INDEX index_name ON talbe_name;

ALTER TABLE table_name DROP INDEX index_name;

ALTER TABLE table_name DROP PRIMARY KEY;
```

### 唯一索引

与普通索引类似，不同的就是：索引列的值必须唯一，但允许有空值（注意和主键不同）。

如果是组合索引，则列值的组合必须唯一，创建方法和普通索引类似

创建索引：

```sql
CREATE UNIQUE INDEX index_name ON table_name (column(length));

ALTER TABLE table_name ADD UNIQUE index_name (column(length));

CREATE TABLE table_name (id int not null auto_increment,title varchar(30) ,PRIMARY
KEY(id) , UNIQUE index_name (title(length)));
```

### 全文索引（FULLTEXT）

MySQL 从3.23.23 版开始支持全文索引和全文检索，FULLTEXT 索引仅可用于MyISAM
表；他们可以从CHAR、VARCHAR 或TEXT 列中作为CREATE TABLE 语句的一部分被创建，或
是随后使用ALTER TABLE 或CREATE INDEX 被添加。

对于较大的数据集，将你的资料输入一个没有FULLTEXT 索引的表中，然后创建索引，
其速度比把资料输入现有FULLTEXT 索引的速度更为快。不过切记对于大容量的数据表，生
成全文索引是一个非常消耗时间非常消耗硬盘空间的做法。

创建索引：

```sql
CREATE FULLTEXT INDEX index_name ON table_name(column(length));

ALTER TABLE table_name ADD FULLTEXT index_name( column);

CREATE TABLE table_name (id int not null auto_increment,title varchar(30) ,PRIMARY
KEY(id) , FULLTEXT index_name (title));
```

### 组合索引（最左前缀）

```sql
CREATE TABLE article(id int not null, title varchar(255), time date);
```

平时用的SQL 查询语句一般都有比较多的限制条件，所以为了进一步榨取MySQL 的效
率，就要考虑建立组合索引。例如上表中针对title 和time 建立一个组合索引：ALTER TABLE
article ADD INDEX index_title_time (title(50),time(10))。建立这样的组合索引，其实是相当于分
别建立了下面两组组合索引：

- title,time
- title

为什么没有time 这样的组合索引呢？这是因为MySQL 组合索引“最左前缀”的结果。

简单的理解就是只从最左面的开始组合。并不是只要包含这两列的查询都会用到该组合索引，

如下面的几个SQL 所示：

1，使用到上面的索引

```sql
SELECT * FROM article WHERE title='测试' AND time=1234567890;
SELECT * FROM article WHERE title='测试';
```

2，不使用上面的索引

```sql
SELECT * FROM article WHERE time=1234567890;
```

参考：https://segmentfault.com/a/1190000008131735#articleHeader5

创建索引：

```sql
CREATE INDEX index_name ON table_name (column_list)
```

## MySQL 中的索引优化

虽然索引大大提高了查询速度，同时却会降低更新表的速度，如对表进行INSERT、UPDATE
和DELETE 次数大于查询次数时，放弃索引。

因为更新表时，MySQL 不仅要保存数据，还要
保存一下索引文件。建立索引会占用磁盘空间的索引文件。一般情况这个问题不太严重，但
如果你在一个大表上创建了多种组合索引，索引文件的会膨胀很快。索引只是提高效率的一
个因素，如果你的MySQL 有大数据量的表，就需要花时间研究建立最优秀的索引，或优化
查询语句。

### 索引不会包含有NULL 值的列

只要列中包含有NULL 值都将不会被包含在索引中，组合索引中只要有一列含有NULL
值，那么这一列对于此组合索引就是无效的。所以我们在数据库设计时不要让字段的默认值
为NULL。

```sql
create table table_name(c1 varchar(32) default ‘0’)
```

### 使用短索引

对串列进行索引，如果可能应该指定一个前缀长度。例如，如果有一个CHAR(255)的列，
如果在前10 个或20 个字符内，多数值是惟一的，那么就不要对整个列进行索引。短索引不
仅可以提高查询速度而且可以节省磁盘空间和I/O 操作。

```sql
CREATE INDEX index_name ON table_name (column(length))
```

### 索引列排序

MySQL 查询只使用一个索引。因此如果where 子句中已经使用了索引的话，那么order
by 中的列是不会使用索引的。因此数据库默认排序可以符合要求的情况下不要使用排序操
作；尽量不要包含多个列的排序，如果需要最好给这些列创建复合索引。

### like 语句操作

一般情况下不鼓励使用like 操作，如果非使用不可，如何使用也是一个问题。

like “%aaa%”不会使用索引，而like “aaa%”可以使用索引。

### 不要在列上进行运算

例如：`select * from users where YEAR(adddate) < 2007`，将在每个行上进行运算，这将导
致索引失效而进行全表扫描， 因此我们可以改成： `select * from users where adddate < '2007-01-01'`

## 索引总结

MySQL 只对以下操作符才使用索引：<,<=,=,>,>=,between,in,以及某些时候的like(不以通配符%或_开头的情形)。而理论上每张表里面最多可创建16 个索引，不过除
非是数据量真的很多，否则过多的使用索引也不是那么好玩的。

建议：一个表的索引数最好不要超过6 个，若太多则应考虑一些不常使用到的列上建的索引是否有必要。

# MySQL 中的SQL 的常见优化策略

## 避免全表扫描

对查询进行优化，应尽量避免全表扫描，首先应考虑在where 及order by 涉及的列上建立索引。

## 避免判断null 值

应尽量避免在where 子句中对字段进行null 值判断，否则将导致引擎放弃使用索引而进行全表扫描，如：

```sql
select id from t where num is null
```

可以在num 上设置默认值0，确保表中num 列没有null 值，然后这样查询：

```sql
select id from t where num=0
```

## 避免不等值判断

应尽量避免在where 子句中使用!=或<>操作符，否则引擎将放弃使用索引而进行全表扫描。

## 避免使用or 逻辑

应尽量避免在where 子句中使用or 来连接条件，否则将导致引擎放弃使用索引而进行全表扫描，如：

```sql
select id from t where num=10 or num=20
```

可以这样查询：

```sql
select id from t where num=10
union all
select id from t where num=20
```

## 慎用in 和not in 逻辑

in 和not in 也要慎用，否则会导致全表扫描，如：

```sql
select id from t1 where num in(select id from t2 where id > 10)
```

此时外层查询会全表扫描，不使用索引。可以修改为：

```sql
select id from t1,(select id from t1 where id > 10)t2 where t1.id = t2.id
```

此时索引被使用，可以明显提升查询效率。

## 注意模糊查询

下面的查询也将导致全表扫描：

```sql
select id from t where name like '%abc%'
```

模糊查询如果是必要条件时，可以使用

```sql
select id from t where name like 'abc%'
```

来实现模糊查询，此时索引将被使用。如果头匹配是必要逻辑，建议使用全文搜索引擎（Elastic search、Lucene、Solr 等）。

## 避免查询条件中字段计算

应尽量避免在where子句中对字段进行表达式操作，这将导致引擎放弃使用索引而进行全表扫描。如：

```sql
select id from t where num/2=100
```

应改为:

```sql
select id from t where num=100*2
```

## 避免查询条件中对字段进行函数操作

应尽量避免在where 子句中对字段进行函数操作，这将导致引擎放弃使用索引而进行全表扫描。如：

```sql
select id from t where substring(name,1,3)='abc'
```

--name以abc开头<应改为:

```sql
select id from t where name like 'abc%'
```

## WHERE 子句“=”左边注意点

不要在where子句中的“=”左边进行函数、算术运算或其他表达式运算，否则系统将可能无法正确使用索引。

## 组合索引使用

在使用索引字段作为条件时，如果该索引是复合索引，那么必须使用到该索引中的第一个字段作为条件时才能保证系统使用该索引，否则该索引将不会被使用，并且应尽可能的让字段顺序与索引顺序相一致。

## 不要定义无意义的查询

不要写一些没有意义的查询，如需要生成一个空表结构：

```sql
select col1,col2 into #t from t where 1=0
```

这类代码不会返回任何结果集，但是会消耗系统资源的，应改成这样：

```sql
create table #t(...)
```

## exists

很多时候用exists 代替in 是一个好的选择：

```sql
select num from a where num in(select num from b)
```

用下面的语句替换：

```sql
select num from a where exists(select 1 from b where num=a.num)
```

## 索引也可能失效

并不是所有索引对查询都有效，SQL 是根据表中数据来进行查询优化的，当索引列有大量数据重复时，SQL 查询可能不会去利用索引，如一表中有字段sex，male、female 几乎各一半，那么即使在sex 上建了索引也对查询效率起不了作用。

## 表格字段类型选择

尽量使用数字型字段，若只含数值信息的字段尽量不要设计为字符型，这会降低查询和连接的性能，并会增加存储开销。这是因为引擎在处理查询和连接时会逐个比较字符串中每一个字符，而对于数字型而言只需要比较一次就够了。

尽可能的使用varchar 代替char ，因为首先可变长度字段存储空间小，可以节省存储空间，其次对于查询来说，在一个相对较小的字段内搜索效率显然要高些。

## 查询语法中的字段

任何地方都不要使用select * from t ，用具体的字段列表代替“*”，不要返回用不到的任何字段。

## 索引无关优化

不使用*、尽量不使用union，union all等关键字、尽量不使用or 关键字、尽量使用等值判断。

表连接建议不超过5 个。如果超过5 个，则考虑表格的设计。（互联网应用中）

表连接方式使用外联优于内联。

外连接有基础数据存在。如：A left join B，基础数据是A。A inner join B，没有基础数据的，先使用笛卡尔积完成全连接，在根据连接条件得到内连接结果集。

大数据量级的表格做分页查询时，如果页码数量过大，则使用子查询配合完成分页逻辑。

```sql
Select * from table limit 1000000, 10
Select * from table where id in (select pk from table limit 100000, 10)
```
