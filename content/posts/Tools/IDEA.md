---
title: "IDEA"
date: 2020-07-01T18:58:47+08:00
draft: true
tags: [ ""]
categories: ["Tools"]
---

# 安装

1） 下载

地址：http://www.jetbrains.com/idea/download/#section=windows](http://www.jetbrains.com/idea/download/#section=windows)

```
版本：Ultimate
Version: 2017.1.5
Build: 171.4694.70
Released: July 4, 2017
```

2） 安装

![install](https://oscimg.oschina.net/oscnet/1ab7d485449315ecd7ea427be34a6432cf1.jpg "install")

3） 注册

1. 2017年之前
   	- 服务器激活方式：选择菜单Help->Register->License server，填上http://idea.iteblog.com/key.php，参考：http://idea.iteblog.com/
      	- 激活码方式： 参考网站http://idea.lanyus.com/

2. 现在国家加强知识产权保护，注册码或破解文件不好找。不过，如果你加入某个群还是可以找到的……


# 常用设置

## Java代码模板

File-->settings-->Editor-->File and Code Templates-->Files

![img](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200707132124.png)

（1）${NAME}：设置类名，与下面的${NAME}一样才能获取到创建的类名

（2）TODO：代办事项的标记，一般生成类或方法都需要添加描述

（3）${USER}、${DATE}、${TIME}：设置创建类的用户、创建的日期和时间，这些事IDEA内置的方法，还有一些其他的方法在绿色框标注的位置，比如你想添加项目名则可以使用${PROJECT_NAME}

（4）1.0：设置版本号，一般新创建的类都是1.0版本，这里写死就可以了

参考：https://blog.csdn.net/xiaoliulang0324/article/details/79030752


## 字体

![img](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200707131322.png)

## 跨系统-回车问题

![image](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200707131445.png)

## git提交时忽略文件设置

![image](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200707131526.png)

参考：https://blog.csdn.net/weixin_42617398/article/details/81807448

这样然后项目干净多了

# 快捷键

| 快捷键                                  | 说明                              |
| --------------------------------------- | --------------------------------- |
| Ctrl+Shift+↑/↓  或者  Alt+Shift+↑/↓     | 上下移动一行代码                  |
| Ctrl + Shift + F12                      | 代码窗口最大化                    |
| Ctrl + F12                              | 弹窗方式显示大纲                  |
| F11                                     | 书签                              |
| ctrl+f                                  | 查找                              |
| ctrl+r                                  | 查找和替换                        |
| Alt+鼠标选区                            | 选择方块                          |
| Ctrl+P                                  | 显示参数信息                      |
| Ctrl+Q                                  | 显示方法参数信息                  |
| Ctrl+Alt+O                              | 优化导入的类和包                  |
| CTRL+SHIFT+T                            | 新建测试类                        |
| Ctrl+Shift+Alt+T                        | 重构一切                          |
| Ctrl-Alt-B 或 ctrl + alt + 左键移动上去 | 也可以看到**方法的实现类**        |
| Ctrl + Shilf + U                        | 大小写切换                        |
| Ctrl + Alt + T                          | try catch包裹                     |
| Ctrl + H                                | 查看所有子类                      |
| Alt + 句点                              | 新建类、包、文件....              |
| Ctrl + Shift + Enter                    | 类似Eclipse的tab跳到右括号外      |
| Ctrl+Alt+V                              | 类似于Eclipse中的ctrl+2+L的快捷键 |
| sout                                    | eclipse中的syso                   |
| psvm                                    | eclipse中的main方法               |
| Ctrl+Alt+L                              | 一键格式化代碼                    |
| Ctrl + O                                | 打开可以重写的方法                |
| Ctrl + I                                |                                   |


参考：https://www.cnblogs.com/aashui/p/7657803.html

AndroidStudio和Eclipse常用快捷键对比：

![key-word-2](https://oscimg.oschina.net/oscnet/432b41423a2766283d616127be478a2e21f.jpg "key-word-2")

# FAQ

## IntelliJ中getResourceAsStream为null的问题

IntelliJ中的资源和java文件是分开放的。在src下，intellij在编译时只编译.java后缀的文件并放到classpath目录下，其它文件会忽略；而在resource下的文件会按包名（文件夹）放到对应的classpath目录下。

![getResourceAsStream-null](https://oscimg.oschina.net/oscnet/9d0f81a1cdb6d87c40d8f5a2e524faabc90.jpg "getResourceAsStream-null")

## IDEA中java文件出现小黄色的 J 

https://jingyan.baidu.com/article/c910274b5c23a1cd361d2d3b.html

## IntelliJ换行CRLF, LF, CR的解释和默认设置

https://blog.csdn.net/echo_follow_heart/article/details/48314523




