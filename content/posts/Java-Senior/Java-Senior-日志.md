---
title: "Java-Senior-日志"
date: 2017-07-01T12:09:47+08:00
draft: true
tags: [ ""]
categories: ["Java-Senior"]
---

# 日志级别

日志的级别，从高到低依次为：OFF、FATAL、ERROR、WARN、INFO、DEBUG、TRACE、 ALL。

官方建议使用四个级别：ERROR、WARN、INFO、DEBUG。


# SLF4J + Log4j2

官方文档：https://logging.apache.org/log4j/2.x/articles.html


1）依赖

```xml
    <dependencies>
        <!--slf4j-->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>1.7.26</version>
        </dependency>
        <!--桥接器-->
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-slf4j-impl</artifactId>
            <version>2.11.2</version>
        </dependency>
        <!--log4j2依赖的两个包-->
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-api</artifactId>
            <version>2.11.2</version>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-core</artifactId>
            <version>2.11.2</version>
        </dependency>
    </dependencies>
```

http://logging.apache.org/log4j/2.x/maven-artifacts.html#Using_Log4j_in_your_Apache_Maven_build

2）log4j.xml

官方文档：https://logging.apache.org/log4j/2.x/manual/configuration.html#XML

最简配置：

```xml
    <?xml version="1.0" encoding="UTF-8"?>  
    <configuration status="OFF">  
        <appenders>  
            <Console name="Console" target="SYSTEM_OUT">  
                <PatternLayout pattern="%d{HH:mm:ss.SSS} [%t] %-5level %logger{36} - %msg%n"/>  
            </Console>  
        </appenders>  
        <loggers>  
            <root level="error">  
                <appender-ref ref="Console"/>  
            </root>  
        </loggers>  
    </configuration>
```

复杂点配置：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="INFO">
    <!--先定义所有的appender-->
    <Appenders>
        <Console name="Console" target="SYSTEM_OUT">
            <!--控制台只输出level及以上级别的信息（onMatch），其他的直接拒绝（onMismatch）-->
            <ThresholdFilter level="trace" onMatch="ACCEPT" onMismatch="DENY"/>
            <PatternLayout pattern="%d{HH:mm:ss.SSS} %-5level %class{36} %L %M - %msg%xEx%n"/>
        </Console>
        <!--每次运行程序会自动清空，适合临时测试用-->
        <File name="log" fileName="log/test.log" append="false">
            <PatternLayout pattern="%d{HH:mm:ss.SSS} %-5level %class{36} %L %M - %msg%xEx%n"/>
        </File>
        <!-- 这个会打印出所有的信息，每次大小超过size，则这size大小的日志会自动存入按年份-月份建立的文件夹下面并进行压缩，作为存档-->
        <RollingFile name="RollingFile" fileName="logs/app.log"
                     filePattern="log/$${date:yyyy-MM}/app-%d{MM-dd-yyyy}-%i.log.gz">
            <PatternLayout pattern="%d{yyyy-MM-dd 'at' HH:mm:ss z} %-5level %class{36} %L %M - %msg%xEx%n"/>
            <SizeBasedTriggeringPolicy size="50MB"/>
            <!-- 则默认为最多同一文件夹下7个文件，这里设置了20 -->
            <DefaultRolloverStrategy max="20"/>
        </RollingFile>
    </Appenders>

    <!--然后定义logger，只有定义了logger并引入的appender，appender才会生效-->
    <Loggers>
        <root level="INFO">
            <appender-ref ref="RollingFile"/>
            <appender-ref ref="Console"/>
        </root>
    </Loggers>

</Configuration>
```


自我总结：<Configuration status="INFO">与<root level="INFO">日志级别尽量保持一致 ——20190604


3）使用

```java
public class LogTest {

    private static Logger logger = LoggerFactory.getLogger(LogTest.class);

    public static void main(String[] args) {
        logger.error("error");

    }
}
```

---

死循环：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708012718.png)




# SLF4J + Logback 

## SLF4J

知识点：log4j/logback/slf4j 都是同一个人写的，https://github.com/qos-ch

官方手册：https://logback.qos.ch/manual/introduction.html

### 日志框架都需要哪些依赖-图解

教程：https://ke.qq.com/course/403245?taid=3394046366459693

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708012832.png)

另外，第三方框架使用不同的日志框架，如何统一呢？使用桥接：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708012913.png)


### 开源框架默认使用的日志框架

开源框架 | 默认使用的框架
---|---
Spring | JCL
MyBatis | 
Hibernate | 
Spring Boot | SFL4J + logback


Spring框架默认使用JCL(Jakarta Commons Logging)

SpringBoot底层是Spring，对Spring的日志进行了包装，选用 SFL4J + logback


## SLF4J + Logback

spring boot 中的默认日志依赖：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708012943.png)

spring boot 默认配置好了日志，可以直接使用：

```java
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootLogTest {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void test(){
        logger.trace("这是trace日志");
        logger.debug("这是debug日志");
        logger.info("这是info日志");
        logger.warn("这是warn日志");
        logger.error("这是error日志");
    }
}
```

默认是info级别的日志

可以在application.properties文件中修改配置：

```properties
# log
logging.level.com.demo=trace
```

或在类路径（resources）下放上日志配置文件，SpringBoot就不使用默认的配置了。

Logging System | Customization
---|---
Logback | logback-spring.xml, logback-spring.groovy, logback.xml or logback.groovy
Log4j2 | log4j2-spring.xml or log4j2.xml
JDK (Java Util Logging) | logging.properties

参考：https://docs.spring.io/spring-boot/docs/1.5.21.RELEASE/reference/htmlsingle/#boot-features-logging

### 占位符 {}

```java
    public static void test1() {
        logger.error("aaa{}ccc{}eee{}ggg{}iii{}kkk{}lll", "bbb", "ddd", "fff", "hhh", "jjj", "mmm");
    }
```

参考：https://blog.csdn.net/Dongguabai/article/details/83720885


### 附件：logback.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
 <configuration>
<!--定义日志文件的存储地址 勿在 LogBack 的配置中使用相对路径-->  
    <property name="LOG_HOME" value="${catalina.base}/logs/" />  
    <!-- 控制台输出 -->   
    <appender name="Stdout" class="ch.qos.logback.core.ConsoleAppender">
       <!-- 日志输出编码 -->  
        <layout class="ch.qos.logback.classic.PatternLayout">   
             <!--格式化输出：%d表示日期，%thread表示线程名，%-5level：级别从左显示5个字符宽度%msg：日志消息，%n是换行符--> 
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{50} - %msg%n   
            </pattern>   
        </layout>   
    </appender>   
    <!-- 按照每天生成日志文件 -->   
    <appender name="RollingFile"  class="ch.qos.logback.core.rolling.RollingFileAppender">   
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <!--日志文件输出的文件名-->
            <FileNamePattern>${LOG_HOME}/server.%d{yyyy-MM-dd}.log</FileNamePattern>   
            <MaxHistory>30</MaxHistory>
        </rollingPolicy>   
        <layout class="ch.qos.logback.classic.PatternLayout">  
            <!--格式化输出：%d表示日期，%thread表示线程名，%-5level：级别从左显示5个字符宽度%msg：日志消息，%n是换行符--> 
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{50} - %msg%n   
            </pattern>   
       </layout> 
        <!--日志文件最大的大小-->
       <triggeringPolicy class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
         <MaxFileSize>10MB</MaxFileSize>
       </triggeringPolicy>
    </appender>     

    <!-- 日志输出级别 -->
    <root level="info">   
        <appender-ref ref="Stdout" />   
        <appender-ref ref="RollingFile" />   
    </root> 



<!--日志异步到数据库 -->  
<!--     <appender name="DB" class="ch.qos.logback.classic.db.DBAppender">
        日志异步到数据库 
        <connectionSource class="ch.qos.logback.core.db.DriverManagerConnectionSource">
           连接池 
           <dataSource class="com.mchange.v2.c3p0.ComboPooledDataSource">
              <driverClass>com.mysql.jdbc.Driver</driverClass>
              <url>jdbc:mysql://127.0.0.1:3306/databaseName</url>
              <user>root</user>
              <password>root</password>
            </dataSource>
        </connectionSource>
  </appender> -->

</configuration>
```


# FAQ

## SLF4J/Logback/Log4j/Logging的区别与联系

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708020216.png)

https://blog.csdn.net/qq_32625839/article/details/80893550