---
title: "Java-Senior-设计模式"
date: 2017-07-01T12:09:48+08:00
draft: true
tags: [ ""]
categories: ["Java-Senior"]
---

# 工厂模式

## 工厂方法模式

定义：一个用于创建对象的接口，让子类决定实例化哪一个类。Factory Method使一个类的实例化延迟到其子类。

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708020450.png)

我的理解：子类实现抽象方法时可以有不同的实现（new不同的对象，让子类决定创建对象）。这样就不用修改已经写的类中的逻辑了。

## 抽象工厂模式

定义：提供一个创建一系列或相互依赖对象的接口，而无须指定它们具体的类。

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708020519.png)

## 两种模式的区别

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708020534.png)

# 观察者模式

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708020608.png)

我的理解：在主题方有集合，用于保存订阅者列表。主题方有更新时，就会循环遍历集合中的观察者，并调用观察者的更新方法（update()）。

# 装饰模式

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708020628.png)

作用：在不修改类的情况下给类增加新的功能。

具体组件和装饰者都是抽象组件的子类，因此装饰者本身也可以作为具体组件（即“被装饰者”）。装饰者中有具体组件的引用。

它的核心思想是：装饰者调用被装饰者的方法后，再执行装饰者自己的代码。

# 命令模式

会看（画）类图，理解它们之间引用。

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708020646.png)

我的理解：命令模式就是一种引用之间的调用关系。Invoker调用ConcreteCommad中的方法，ConcreteCommand再调用Receiver中的方法。

用面向对象方式来思考：指挥官，命令，士兵。指挥官不直接跟士兵接触，而是通过命令对象。

# 迭代器模式

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708020740.png)

我的理解：集合本身不迭代元素，而是交给一个专门的对象来执行，这个对象就是Iterator。因此Iterator就要有集合的引用，以便迭代集合。

# 适配器模式

## 单向适配器

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708020803.png)

三个角色：  
1）目标（Target）  
2）被适配者（Adaptee）  
3）适配器（Adapter）：实现了目标接口，并有被适配者的引用

## 双向适配器

如果Adapter角色同时实现目标接口和被适配器接口，并包含目标和被适配者的引用，那么该适配器就是一个双向适配器。

# FAQ

113）适配器模式和装饰器模式有什么区别？(答案)  
虽然适配器模式和装饰器模式的结构类似，但是每种模式的出现意图不同。适配器模式被用于桥接两个接口，而装饰模式的目的是在不修改类的情况下给类增加新的功能。

114）适配器模式和代理模式之前有什么不同？(答案)  
这个问题与前面的类似，适配器模式和代理模式的区别在于他们的意图不同。由于适配器模式和代理模式都是封装真正执行动作的类，因此结构是一致的，但是适配器模式用于接口之间的转换，而代理模式则是增加一个额外的中间层，以便支持分配、控制或智能访问。


作者：Java团长
链接：http://www.jianshu.com/p/ca185fff7ccb
來源：简书
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

# 附录A：两个易混概念

面向对象中依赖、关联的实践概念：  
依赖：以方法入参或返回值形式。  
关联：以字段形式。

# 附录B：程序设计的几个原则

1）面向抽象原则  
2）开-闭原则：让设计对扩展开放，对修改关闭。  
3）多用组合少用继承原则  
4）高内聚-低耦合原则



# 附录C：单例模式代码实现

## 饿汉方式

```java
public class HungryManSingleInstance {

    private HungryManSingleInstance(){}

    private static final HungryManSingleInstance instance = new HungryManSingleInstance();

    public HungryManSingleInstance getInstance(){
        return instance;
    }
}
```

## 懒汉方式

```java
public class LazyLoadSingleInstance {

    private static LazyLoadSingleInstance instance;

    private LazyLoadSingleInstance(){}

    public static LazyLoadSingleInstance getInstance() {
        if(instance == null){
            instance = new LazyLoadSingleInstance();
        }
        return instance;
    }
}
```

**懒汉方式线程不安全**。若要安全，则使用双重锁方式（懒汉方式的安全版）。

## 双重锁方式（多线程环境）

```java
public class DoubleLockSingleInstance {

    private volatile static DoubleLockSingleInstance singleton;

    private DoubleLockSingleInstance() {
    }

    public static DoubleLockSingleInstance getSingleton() {
        if (singleton == null) {
            synchronized (DoubleLockSingleInstance.class) {
                if (singleton == null) {
                    singleton = new DoubleLockSingleInstance();
                }
            }
        }
        return singleton;
    }
}
```

为什么要用双重锁？为了提高线程的访问效率：只要有一个线程实例化了一个对象，外面的线程就不需要再等待锁了。

![image](226C87252F2347C28B145A2145873F87)

## 静态内部类方式

做到了延迟加载，因为调用getInstance()时才创建实例。（有疑问）。

经测试，是可以延迟的。结论：静态内部类的静态变量只有在所在的内部类被调用时才初始化。

```java
public class StaticSingleInstance {

    private StaticSingleInstance(){
        System.out.println("静态方式创建单例的外部类无参构造器被调用了");
    }

    public static class SingleInnerHolder{
        public static StaticSingleInstance instance = 
                new StaticSingleInstance();
    }

    public static StaticSingleInstance getInstance(){
        return SingleInnerHolder.instance;
    }
}
```

## 枚举类方式

单例的定义：

```java
public enum EnumSingleInstance {

    INSTANCE;

    private EnumSingleInstanceClass instance;

    EnumSingleInstance() {
        instance = new EnumSingleInstanceClass();
    }

    public EnumSingleInstanceClass getInstance() {
        return instance;
    }
}
```

客户端的调用：

```java
public class EnumSingleInstanceTest {
    @Test
    public void testGetInstance() {
        for (int i = 0; i < 10; i++) {
            EnumSingleInstanceClass instance = EnumSingleInstance.INSTANCE
                    .getInstance();
            System.out.println(instance);
        }
    }
}
```