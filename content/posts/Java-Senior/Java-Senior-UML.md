
---
title: "Java-Senior-UML"
date: 2017-07-01T12:09:45+08:00
draft: true
tags: [ ""]
categories: ["Java-Senior"]
---

# 基本概念

## 聚合aggregation、组合composition

聚合与对象之间的关系，即整体对象和部分对象之间的关系。

聚合：是两个类之间的一个二元关系，它表示一个类的对象实例以另一个类的对象实例作为其组成部分。

- 方式一：用部分对象作为整体对象的一个属性。
- 方式二：独立地定义和创建整体对象和部分对象，并在整体对象（或部分对象）中设置一个属性，它的值是一个指向部分对象（或整体对象）的指针，从而在逻辑上表明一个对象是另一个对象的组成部分。

紧密而固定的聚合关系称为组合（是聚合的一种）


## 关联association

定义：如果A类中成员变量是用B类（接口）来声明的变量，那么A和B的关系是关联关系，称为A关联B。


## 消息 message

狭义：在面向对象方法中把向对象发出的服务请求称为消息。

广义：对象之间在一次交互中所传送的消息。

# 类图

1、类实现接口

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708022518.png)


2、泛化

指的是继承关系（类继承类，或接口继承接口）

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708022533.png)


3、聚合与组合

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708022547.png)


4、关联

定义：如果A类中成员变量是用B类（接口）来声明的变量，那么A和B的关系是关联关系，称为A关联B。

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708022623.png)


5、依赖

定义：如果A类中某个方法的参数是用B类（接口）来声明的变量或某个方法返回的数据类型是B类型的，那么A和B的关系是依赖关系，称为A依赖B。

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708022654.png)



# 顺序图

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708022715.png)



# 活动图

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708022727.png)



# 附件

## UML画图工具

- staruml 地址：https://staruml.en.softonic.com/
- processon 在线画图工具 https://www.processon.com/