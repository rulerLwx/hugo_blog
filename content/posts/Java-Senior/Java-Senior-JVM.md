---
title: "Java-Senior-JVM"
date: 2017-07-01T12:12:49+08:00
draft: true
tags: ["jvm"]
categories: ["Java-Senior"]
---


# JVM体系结构

## JVM的位置

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708021336.png)

JVM运行在操作系统之上，与硬件没有直接交互


## JVM体系结构

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708021356.png)

灰色代表线程独有，亮色代表线程共享

绿色的就相当于Java中的**java.lang.Runtime**类


### 类加载器ClassLoader

负责加载class文件，转换成**方法区**中运行时数据结构，并且ClassLoader**只负责class文件的加载**，至于它是否可以运行，则由Execution Engine决定

class文件在文件开头有特定的文件标识`cafe babe`（用文本编辑器打开class文件就能看到）

类加载有以下4类：
- Bootstrap 根类加载器，C++语言
- Extension 扩展类加载器，Java语言
- System 系统类加载器
- 自定义类加载器，java.lang.ClassLoader的子类

Bootstrap 负责加载Java的核心类（jdk1.7.0_79\jre\lib\rt.jar），String、System等类就在这个包中。

Extension负责加载jdk1.7.0_79\jre\lib\ext\\*（或java.ext.dirs系统属性指定）的目录下的jar包类。

System负责加载 CLASSPATH环境变量（或-classpath、或java.class.path属性）所指定的jar包和类路径。

```java
public static void main(String[] args) {
    Object o = new Object();
    System.out.println(o.getClass().getClassLoader());//null

    MyClass myClass = new MyClass();
    System.out.println(myClass.getClass().getClassLoader().getParent().getParent());//null
    System.out.println(myClass.getClass().getClassLoader().getParent());//sun.misc.Launcher$ExtClassLoader@776ec8df
    System.out.println(myClass.getClass().getClassLoader());//sun.misc.Launcher$AppClassLoader@18b4aac2
}
```

类加载遵从**父类委托机制**（又称“双亲委托机制”）


为什么要用父类委托机制？可以起到**沙箱安全**的作用。

如下，自己编写一个java.lang.String类，运行会报错，起到：用户写的类不会污染java提供的类 ——20191108

```java
package java.lang;

public class String {
    //错误: 在类 java.lang.String 中找不到 main 方法
    public static void main(String[] args) {
        System.out.println("abc");
    }
}
```

一张有趣的图：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708021419.png)


### 本地接口（方法）native

- Native Interface：本地方法接口
- Native Method Stack：用于登记native方法，在Execution Engine执行时加载本地方法库


### PC寄存器

Program Counter Register 程序计算器

每个线程都有一个程序计数器，是一个指针，存储下一条指令的地址。是线程私有的，占用内存较少。


### 方法区Method Area

方法区不是存放方法的地方

存储了**每个类的结构信息**（可以理解为模板），如：常量池、字段（静态、非静态）、方法数据、构造函数、普通方法字节码

方法区在不同虚拟机里实现是不一样的，最典型的是永久代（PermGen space）和元空间（Metaspace）

- 永久代（PermGen space）
- 元空间（Metaspace）


### Java栈Java Stack

方法进栈，对象进堆

- 面试：程序 = 算法 + 数据结构
- 工作：程序 = 框架 + 业务逻辑

栈存储什么？
- 本地变量（Local Variables）：输入、输出参数，方法内的变量
- 栈操作（Operand Stack）：记录入栈、出栈操作
- 栈帧数据（Frame Data）：类文件、方法等

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708021442.png)

1）方法递归如果没有边界，就会java.lang.StackOverflowError（SOF），StackOverflowError属于Error，不是Exception。

2）栈、堆、方法区如何交互？

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708021505.png)

总结：
- 值传递、引用传递
- 方法区相当于类的模板 —— 20191108


### 堆Heap

堆内存大小可以调节

- 新生代
    - 伊甸区（Eden Space）：GC = YGC = Minor GC
    - 幸存0区（Survivor 0 Space），又称S0 = from
    - 幸存1区（Survivor 1 Space），又称S1 = to
- 老年代（Tenure Generation Space）：Full GC = FGC = Major GC
- 元空间（永久区）

参考：https://www.bilibili.com/video/av70166821?p=25

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708021537.png)

如果出现java.lang.OutOfMemoryError异常，说明Java虚拟机堆内存不够，原因有二：
- 堆内存设置不够，可以通过参数-Xms、-Xmx来调整
- 代码中创建了大量大对象，且长时间不能被回收（有引用）

对象生命周期和GC：from区和to区，它们的位置和名份不是固定的，每次GC后会交换，谁空谁是to，如图：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708021559.png)

物理上的堆内存：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708021648.png)

永久区：

永久区是一个常驻内存区域，用于存放JDK自身所携带的Class，Interface的元数据（运行环境必须的类信息），不会被垃圾回收，关闭JVM才释放内存。


# 堆体系结构

# 堆参数调优

JDK7：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708021713.png)

JDK8：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708021726.png)

java8以后，元空间并不在虚拟机中而是使用本机物理内存，仅受本地内存限制，不再受MaxPermSize控制

参数解释：
- -Xms：设置初始堆内存大小，即堆最小内存，默认为物理内存的1/64
- -Xmx：设置最大堆内存大小，默认为物理内存的1/4
- -XX:+PrintGCDetails：输出详细的GC处理日志
- -XXSurvivorRatio=3：年轻代中Eden区与Survivor区的大小比值（这里示例，eden占3，survivor占2，总共5份）
- -XX:MaxDirectMemorySize：
- -XX:MaxTenuringThreshold：设置对象在新生代中存活的次数，默认15岁

如：-Xmx100m -Xms100m -XX:+PrintGCDetails

```tex
Heap
 PSYoungGen      total 29696K, used 4102K [0x00000000fdf00000, 0x0000000100000000, 0x0000000100000000)
  eden space 25600K, 16% used [0x00000000fdf00000,0x00000000fe301a70,0x00000000ff800000)
  from space 4096K, 0% used [0x00000000ffc00000,0x00000000ffc00000,0x0000000100000000)
  to   space 4096K, 0% used [0x00000000ff800000,0x00000000ff800000,0x00000000ffc00000)
 ParOldGen       total 68608K, used 0K [0x00000000f9c00000, 0x00000000fdf00000, 0x00000000fdf00000)
  object space 68608K, 0% used [0x00000000f9c00000,0x00000000f9c00000,0x00000000fdf00000)
 Metaspace       used 3501K, capacity 4496K, committed 4864K, reserved 1056768K
  class space    used 382K, capacity 388K, committed 512K, reserved 1048576K

```

```java
    public static void main(String[] args) {
        long maxMemory = Runtime.getRuntime().maxMemory();//Java虚拟机试图使用的最大内存
        long totalMemory = Runtime.getRuntime().totalMemory();//Java虚拟机中的内存问题
        System.out.println("-Xmx:"+maxMemory+"字节，"+(double)maxMemory/1024/1024+"M");
        System.out.println("-Xms:"+totalMemory+"字节，"+(double)totalMemory/1024/1024+"M");
    }
```

问：生产环境，如何配置最大、最小内存？

答：最大、最小配置相同，避免GC应用程序时产生卡顿。

问：写一个应用程序，测试OOM：

答：

1）先设置运行的JVM：-Xmx10m -Xms10m -XX:+PrintGCDetails

2）代码：

```java
    public static void main(String[] args) {
        byte[] bytes = new byte[40 * 1024 * 1024];
    }
```

# MinorGC/MajorGC执行时机

Minor GC触发条件：当Eden区满时，触发Minor GC。

Full GC触发条件：

（1）调用System.gc时，系统建议执行Full GC，但是不必然执行

（2）老年代空间不足

（3）方法去空间不足

（4）通过Minor GC后进入老年代的平均大小大于老年代的可用内存

（5）由Eden区、From Space区向To Space区复制时，对象大小大于To Space可用内存，则把该对象转存到老年代，且老年代的可用内存小于该对象大小

参考：
- https://blog.csdn.net/yhyr_ycy/article/details/52566105
- https://blog.csdn.net/summerZBH123/article/details/81131945


# GC收集日志信息

```tex
[GC (Allocation Failure) [PSYoungGen: 2040K->488K(2560K)] 2040K->904K(9728K), 0.0051501 secs] [Times: user=0.00 sys=0.00, real=0.01 secs] 
[GC (Allocation Failure) [PSYoungGen: 1318K->504K(2560K)] 1734K->1008K(9728K), 0.0020450 secs] [Times: user=0.00 sys=0.00, real=0.00 secs] 
[GC (Allocation Failure) [PSYoungGen: 504K->504K(2560K)] 1008K->1048K(9728K), 0.0006964 secs] [Times: user=0.05 sys=0.00, real=0.00 secs] 
[Full GC (Allocation Failure) [PSYoungGen: 504K->0K(2560K)] [ParOldGen: 544K->846K(7168K)] 1048K->846K(9728K), [Metaspace: 3473K->3473K(1056768K)], 0.0088245 secs] [Times: user=0.00 sys=0.00, real=0.01 secs] 
[GC (Allocation Failure) [PSYoungGen: 0K->0K(2560K)] 846K->846K(9728K), 0.0003925 secs] [Times: user=0.00 sys=0.00, real=0.00 secs] 
[Full GC (Allocation Failure) [PSYoungGen: 0K->0K(2560K)] [ParOldGen: 846K->828K(7168K)] 846K->828K(9728K), [Metaspace: 3473K->3473K(1056768K)], 0.0093239 secs] [Times: user=0.02 sys=0.00, real=0.01 secs] 
Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
```

GC：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708021751.png)

FullGC：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708021803.png)



# GC四大算法

https://www.bilibili.com/video/av70166821?p=31

## GC是什么

分代收集算法

- 次数上频繁收集Yong区
- 次数上较少收集Old区
- 基本上不动Perm区（又叫元空间）

## 算法概述：

Major GC与Minor GC区别：
- Minor GC发生在新生代，Major GC发生在老年代
- Major GC比Minor GC慢10倍以上

## 4大算法

- 引用计数法
- 复制算法（Copying）
- 标记清除（Mark-Sweep）
- 标记压缩（Mark-Compact）


### 引用计数法

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708021950.png)

引用说的是：此对象上还有多少个引用正在使用

手动调用`System.gc();`并不会立即执行垃圾回收


### 复制算法（Copying）

复制算法（Copying）发生在**新生代**

新生代使用的是Minor GC，这种GC使用就是复制算法

From区、to区复制来复制去，使用的就是这种算法

复制算法的**基本思想**是将内存分为2块，每次只用一块，~~当一块用完~~，就将活着的对象复制到另一块上

**优缺点**：复制算法不会产生内存碎片，但会浪费内存（没有使用的那块）

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708022013.png)

如何复制：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708022025.png)


### 标记清除（Mark-Sweep）

标记清除（Mark-Sweep）发生在**老年代**

原理：分为标记和清除两部分，先标记出要回收的对象，然后统一回收这些对象

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708022044.png)

优缺点：不需要额外空间，但两次扫描，耗时严重，会产生内存碎片。


### 标记压缩（Mark-Compact）

标记压缩（Mark-Compact）发生在**老年代**

也可以叫标记-清除-压缩（Mark-Sweep-Compact）

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708022102.png)

优缺点：没有内存碎片，但耗时长


### 总结

有没有最好的垃圾回收算法？

答：没有。应该分代使用相应的算法，简称“分代算法”。

新生代的特点是区域相对老年代小、对象存活率低

老年代的特点是区域大、对象存活率高

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200708022133.png)



# JMM 

JMM，Java Memory Model，Java内存模型

## 什么是Java内存模型

物理内存 --> 工作内存 --> 缓存 --> CPU


## 

Java内存模型是高并发开发的基础

高并发编程需要满足以下3点：
- 可见性
- 原子性
- 有序性





