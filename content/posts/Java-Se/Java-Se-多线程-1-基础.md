
---
title: "Java-Se-多线程-1-基础"
date: 2017-07-01T10:13:47+08:00
draft: true
tags: ["多线程"]
categories: ["Java-Se"]
---


# 进程 & 线程

进程：是系统进行资源分配的和调度的一个独立单位。举例：将公司的一个部门看作一个进程，公司分配项目奖金的时候就是以部门为基本单位分配1万块钱，部门下面的十几个人就分这1万块钱，这时就可以将部门中的每个人看作是一个线程。这就是进程与线程之间的关系。

线程：线程是进程的执行单元，一个进程可以有多个线程。

进程间可以并发，线程间也可以并发。

什么是并发，什么是并行？？？？？
>并发（concurrency）和并行（parallellism）是：
>解释一：并行是指两个或者多个事件在同一时刻发生；而并发是指两个或多个事件在同一时间间隔发生。
>解释二：并行是在不同实体上的多个事件，并发是在同一实体上的多个事件。
>解释三：在一台处理器上“同时”处理多个任务，在多台处理器上同时处理多个任务。如hadoop分布式集群
>所以并发编程的目标是充分的利用处理器的每一个核，以达到最高的处理性能。-- [并发编程网][ifeve]

因此，多线程编程解决的是并发问题，并行的问题可以通过分布式部署来解决。？？？

# 创建线程的3种方式

## 继承Thread类

步骤：  
1、 定义Thread类的子类，并重写run()方法。  
2、 创建Thread子类的实现  
3、 调用线程对象的start()方法启动线程  

代码片段：
```java

// 通过继承Thread类来创建线程类
public class FirstThread extends Thread
{
    // 重写run方法，run方法的方法体就是线程执行体
    public void run()
    {

    }
    public static void main(String[] args)
    {
        // 创建、并启动第一条线程
        new FirstThread().start();
        // 创建、并启动第二条线程
        new FirstThread().start();
    }
}

```

## 实现Runnable接口

步骤：  
1、定义Runnable接口的实现类，并重写该接口的run()方法  
2、创建该实现类的实例，并以此实例作为Thread的target来创建Thread对象  
3、调用该线程对象的start()方法来启动线程  

代码片段：
```java

// 通过实现Runnable接口来创建线程类
public class SecondThread implements Runnable
{
    // run方法同样是线程执行体
    public void run()
    {
       
    }

    public static void main(String[] args)
    {
       SecondThread st = new SecondThread();     // ①
        // 通过new Thread(target , name)方法创建新线程
        new Thread(st , "新线程1").start();
        new Thread(st , "新线程2").start();
    }
}
```
## Callable和Future

步骤：  
1、创建Callable接口实现类，并实现call()方法  
2、使用FutureTask类来包装Callable对象  
3、使用FutureTask对象作为Thread对象的target创建并启动线程  
4、通过FutureTask对象的get()方法获取线程的返回值  

代码片段：
```java
    @Test
    public void test() throws Exception {
        FutureTask<Integer> task = new FutureTask<Integer>(new Callable<Integer>() {

            public Integer call() throws Exception {
                return 1;
            }
        });

        new Thread(task, "新的线程1").start();

        Integer i = task.get();
        System.out.println("i:" + i);
    }
```

# 线程生命周期

线程的生命周期有：新建、就绪、运行、阻塞、死亡。

线程对象调用start()方法后就处于就绪状态。

线程生命周期的状态转换图如下：
![线程生命周期状态转换图](https://oscimg.oschina.net/oscnet/8ce3055225ac17367c264516865b450b4f9.jpg "线程生命周期状态转换图")

**总结：**

- wait()，进入阻塞状态，释放同步监视器
- sleep()，进入阻塞状态，给其它进程（不管线程优先级）执行的机会，不会释放同步监视器
- join()，进入阻塞状态，直到加入的线程执行完成为止
- yield()，不进入阻塞状态，给其它线程（只给优先级高）执行的机会，不会释放同步监视器



# 控制线程

## join

join：让一个线程等待另一个线程完成。

## setDaemon(true)

设置为后台线程，调用线程实例本身的setDaemon()方法：
```java
        MyThread myThread = new MyThread();
        myThread.setDaemon(true);
```

## sleep

线程睡眠，并进入阻塞状态。

调用线程Thread类的静态sleep()方法，而不是某个线程的实例方法：
```java
Thread.sleep(1000);
```

## yield

线程暂停，但不进入阻塞状态。

调用线程Thread类的静态yield()方法，而不是某个线程的实例方法：
```
Thread.yield();
```

## sleep()与yield()区别

- sleep()与yield()都会让当前线程暂停，sleep()会给其它线程执行的机会，不理会其它线程的优先级；但yield()只给优先级相同或更高的线程执行的机会。
- sleep()会让线程进入阻塞状态，yield()则不会进入阻塞状态。

## 线程优先级

priority，1-10，数值越大优先级越高：

- MAX_PRIORITY:10
- MIN_PRIORITY:1
- NORM_PRIORITY:5

```
    myThread.setPriority(Thread.MAX_PRIORITY);
```

*注：servlet中的 load-on-startup 参数，值越大优先级越低*

# 线程同步

## 传统线程同步

### 同步方法

同步一个易发生线程安全的方法，**这里的锁就是this，即当前类对象**：

```java
    // 提供一个线程安全draw()方法来完成取钱操作
    public synchronized void draw(double drawAmount)
    {
        
    }
```

### 同步代码块

同步一个易发生线程安全的字段，**这里的锁就是此可能发生线程安全的字段**：

```java

public class DrawThread extends Thread
{
    // 模拟用户账户
    private Account account;

    // 当多条线程修改同一个共享数据时，将涉及数据安全问题。
    public void run()
    {
        // 使用account作为同步监视器，任何线程进入下面同步代码块之前，
        // 必须先获得对account账户的锁定——其他线程无法获得锁，也就无法修改它
        // 这种做法符合：“加锁 → 修改 → 释放锁”的逻辑
        synchronized (account)
        {
           
        }
        // 同步代码块结束，该线程释放同步锁
    }
}

```

### 何时释放同步监视器

- wait()方法会释放同步监视器
- Thread.sleep(),Thread.yield()不会释放同步监视器

## 同步锁（Lock）

java5开始提供了同步锁，常用的同步锁是ReentrantLock。

同步锁继承关系如下：

```
- Lock
    |-ReentrantLock
- ReadWriteLock
    |-ReentrantReadWriteLock
```

同步锁使用代码片段：
```java

import java.util.concurrent.locks.*;

public class Account
{
    // 定义锁对象
    private final ReentrantLock lock = new ReentrantLock();

    // 提供一个线程安全draw()方法来完成取钱操作
    public void draw(double drawAmount)
    {
        // 加锁
        lock.lock();
        try
        {

        }
        finally
        {
            // 修改完成，释放锁
            lock.unlock();
        }
    }
}
```

## ThreadLocal

原理：为每一个使用该变量的线程都提供一个变量值的副本，使每一个线程都可以独立改变自己的副本，而不会和其他线程的副本冲突，就好像每一个线程都完全拥有该变量一样。代码内部使用ThreadLocal.ThreadLocalMap来存储变量的键和值，也就是内部使用一个类似Map的结构来存储name和name对应的值。

ThreadLocal只提供三个public方法：

- T get()：返回此线程局部变量中当前线程副本中的值。
- void remove()：删除此线程局部变量中当前线程副本中的值。
- void set(T value)：设置此线程局部变量中当前线程副本中的值。

示例：

```java
class Acount
{
    private ThreadLocal<String> name = new ThreadLocal<>();
    public Acount(String str){
        this.name.set(str);
    }
    public String getName(){
        return name.get();
    }
    public void setName(String str){
        this.name.set(str);
    }
    //......
}
```

# 线程间通信

## 传统线程间通信

适用条件：如果程序中使用的是同步代码块、同步方法来控制线程安全，则线程间的通信是使用Object类的 wait(),notify(),notifyAll()控制线程间通信。

- wait() ：当前线程等待
- notify() ：唤醒在此同步监视器上的一个线程（或随机选择一个）
- notifyAll() ：唤醒在此同步监视器上的所有线程，但只有一个获得执行机会

```java

public class Account
{
    // 封装账户编号、账户余额的两个成员变量
    private String accountNo;
    private double balance;
    // 标识账户中是否已有存款的旗标
    private boolean flag = false;

    public Account(){}
    // 构造器
    public Account(String accountNo , double balance)
    {
        this.accountNo = accountNo;
        this.balance = balance;
    }

    // accountNo的setter和getter方法
    public void setAccountNo(String accountNo)
    {
        this.accountNo = accountNo;
    }
    public String getAccountNo()
    {
        return this.accountNo;
    }
    // 因此账户余额不允许随便修改，所以只为balance提供getter方法，
    public double getBalance()
    {
        return this.balance;
    }

    public synchronized void draw(double drawAmount)
    {
        try
        {
            // 如果flag为假，表明账户中还没有人存钱进去，取钱方法阻塞
            if (!flag)
            {
                wait();
            }
            else
            {
                // 执行取钱
                System.out.println(Thread.currentThread().getName()
                    + " 取钱:" +  drawAmount);
                balance -= drawAmount;
                System.out.println("账户余额为：" + balance);
                // 将标识账户是否已有存款的旗标设为false。
                flag = false;
                // 唤醒其他线程
                notifyAll();
            }
        }
        catch (InterruptedException ex)
        {
            ex.printStackTrace();
        }
    }
    public synchronized void deposit(double depositAmount)
    {
        try
        {
            // 如果flag为真，表明账户中已有人存钱进去，则存钱方法阻塞
            if (flag)             //①
            {
                wait();
            }
            else
            {
                // 执行存款
                System.out.println(Thread.currentThread().getName()
                    + " 存款:" +  depositAmount);
                balance += depositAmount;
                System.out.println("账户余额为：" + balance);
                // 将表示账户是否已有存款的旗标设为true
                flag = true;
                // 唤醒其他线程
                notifyAll();
            }
        }
        catch (InterruptedException ex)
        {
            ex.printStackTrace();
        }
    }
}
```

## 使用Condition控制线程间通信

适用条件：如果程序中使用的是同步锁（Lock）来控制线程安全，则线程间通信是使用Condition来控制线程间通信。

获取Condiion实例代码片段：通过锁去获取
```java
        private final Lock lock = new ReentrantLock();
        private final Condition condition = lock.newCondition();
```

Condition类提供如下方法以实现线程间通信：

- await()
- signal(),唤醒单个线程
- signalAll()，唤醒此Lock上的所有线程

示例：
```java

import java.util.concurrent.*;
import java.util.concurrent.locks.*;

public class Account
{
    // 显式定义Lock对象
    private final Lock lock = new ReentrantLock();
    // 获得指定Lock对象对应的Condition
    private final Condition cond  = lock.newCondition();

    public void draw(double drawAmount)
    {
        // 加锁
        lock.lock();
        try
        {
            if ()
            {
                
            }
            else
            {
                // 唤醒其他线程
                cond.signalAll();
            }
        }
        catch (InterruptedException ex)
        {
            ex.printStackTrace();
        }
        // 使用finally块来释放锁
        finally
        {
            lock.unlock();
        }
    }
}
```

## 使用阻塞队列（BockingQueue）控制线程间通信

todo...

# 线程组与异常处理

todo...

# 线程池

## 普通线程池

java 5 新增了一个 Executors 工厂类来产生线程池，常用以下几个静态方法：

- ExecutorService newCachedThreadPool()：系统根据需要创建一个具有缓存功能的线程池。
- ExecutorService newFixedThreadPool(int nThreads)：创建一个可重用的、固定线程数的线程池。
- ExecutorService newSingleThreadExector()：创建一个只有单个线程的线程池。
- ScheduledExecutorService newScheduledThreadPool(int corePoolSize)：创建指定线程数的线程池，它可以指定延迟后执行线程任务。
- ScheduledExecutorService newSingleThreadScheduledExecutor()：创建一个只有一个线程和线程池，它可以在指定延迟后执行线程任务。
- ExecutorService newWorkStealingPool(int parallelism)：创建持有足够的线程的线程池来支持给定的并行级别，该方法还会使用多个队列来减少竞争。（java 8）
- ExecutorService newWorkStealingPool()：该方法是前一个方法的简化版本。如果当前机器有4个CPU，则目标并行级别被设置为4，也就是相当于前一个方法传入4作为参数。（java 8）

ExecutorService 代表尽快执行线程的线程池，程序只需把一个 Runnable 对象或 Callable 对象提交给指定的线程池即可。

ExecutorService 常用以下方法：

- Future<?> submit(Runnable task)：将一个线程任务提交给线程池，Future代表返回值。
- <T> Future<T> submit(Runnable task,T result)：同上，其中result显式指定线程执行执行结束后返回的值。
- <T> Future<T> submit(Callable<T> task)：将一个Calable对象提交给指定的线程池，Future代表返回值。

线程池使用步骤：  
1、调用Executors类的静态工厂方法创建一个ExecutorService对象。  
2、创建Runnable实现类或Callable实现类的实例。  
3、调用ExecutorService对象的submit()方法提交Runnable或Callable实例。  
4、调用ExecutorService对象的shutdown()关闭线程池。

示例代码片段：
```java

import java.util.concurrent.*;

public class ThreadPoolTest
{
    public static void main(String[] args)
        throws Exception
    {
        // 创建足够的线程来支持4个CPU并行的线程池
        // 创建一个具有固定线程数（6）的线程池
        ExecutorService pool = Executors.newFixedThreadPool(6);
        // 使用Lambda表达式创建Runnable对象
        Runnable target = () -> {
            for (int i = 0; i < 100 ; i++ )
            {
                System.out.println(Thread.currentThread().getName()
                    + "的i值为:" + i);
            }
        };
        // 向线程池中提交两个线程
        pool.submit(target);
        pool.submit(target);
        // 关闭线程池
        pool.shutdown();
    }
}
```

注意：《阿里巴巴Java开发手册》中强制线程池不允许使用 Executors 去创建，而是通过 ThreadPoolExecutor 的方式

参考：https://snailclimb.gitee.io/javaguide/#/java/Multithread/JavaConcurrencyAdvancedCommonInterviewQuestions?id=_44-%e5%a6%82%e4%bd%95%e5%88%9b%e5%bb%ba%e7%ba%bf%e7%a8%8b%e6%b1%a0


```java
        //构造一个线程池
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(
                1, 
                1, 
                10, 
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(1),
                new ThreadPoolExecutor.DiscardOldestPolicy());
        threadPool.execute(() -> {
            try {
                //执行 方法
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                threadPool.shutdown();// 关闭线程池
            }
        });
```

参考：https://blog.csdn.net/qq_31615049/article/details/80756781


参考资料：

- ThreadPoolExecutor线程池解析及Executor创建线程常见四种方式：https://cloud.tencent.com/developer/article/1442947


## ForkJoinPool

jdk 7 提供了ForkJoinPool来支持将一个任务拆分成多个“小任务”并行计算，再把多个“小任务”的结果合并成总的计算结果。

```
java.util.concurrent.Executor
                    |-ExecutorService
                        |-AbstractExecutorService
                            |-ForkJoinPool
```

创建ForkJoinPool实例后，就可以调用ForkJoinPool的submit(ForkJoinTask task)或invoke(ForkJoinTask task)方法来提交任务。

其中ForkJoinTask有两个抽象子类：
```
java.util.concurrent.Future
                    |-ForkJoinTask          表示一个可执行、合并的任务
                        |-RecursiveAction   表示没有返回值的任务
                        |-RecursiveTask     表示有返回值的任务
```

完整示例：
```java

import java.util.concurrent.*;

// 继承RecursiveAction来实现"可分解"的任务
class PrintTask extends RecursiveAction
{
    // 每个“小任务”只最多只打印50个数
    private static final int THRESHOLD = 50;
    private int start;
    private int end;
    // 打印从start到end的任务
    public PrintTask(int start, int end)
    {
        this.start = start;
        this.end = end;
    }
    @Override
    protected void compute()
    {
        // 当end与start之间的差小于THRESHOLD时，开始打印
        if(end - start < THRESHOLD)
        {
            for (int i = start ; i < end ; i++ )
            {
                System.out.println(Thread.currentThread().getName()
                    + "的i值：" + i);
            }
        }
        else
        {
            // 如果当end与start之间的差大于THRESHOLD时，即要打印的数超过50个
            // 将大任务分解成两个小任务。
            int middle = (start + end) / 2;
            PrintTask left = new PrintTask(start, middle);
            PrintTask right = new PrintTask(middle, end);
            // 并行执行两个“小任务”
            left.fork();
            right.fork();
        }
    }
}
public class ForkJoinPoolTest
{
    public static void main(String[] args)
        throws Exception
    {
        ForkJoinPool pool = new ForkJoinPool();
        // 提交可分解的PrintTask任务
        pool.submit(new PrintTask(0 , 300));
        pool.awaitTermination(2, TimeUnit.SECONDS);
        // 关闭线程池
        pool.shutdown();
    }
}


```

```java

import java.util.concurrent.*;
import java.util.*;

// 继承RecursiveTask来实现"可分解"的任务
class CalTask extends RecursiveTask<Integer>
{
    // 每个“小任务”只最多只累加20个数
    private static final int THRESHOLD = 20;
    private int arr[];
    private int start;
    private int end;
    // 累加从start到end的数组元素
    public CalTask(int[] arr , int start, int end)
    {
        this.arr = arr;
        this.start = start;
        this.end = end;
    }
    @Override
    protected Integer compute()
    {
        int sum = 0;
        // 当end与start之间的差小于THRESHOLD时，开始进行实际累加
        if(end - start < THRESHOLD)
        {
            for (int i = start ; i < end ; i++ )
            {
                sum += arr[i];
            }
            return sum;
        }
        else
        {
            // 如果当end与start之间的差大于THRESHOLD时，即要累加的数超过20个时
            // 将大任务分解成两个小任务。
            int middle = (start + end) / 2;
            CalTask left = new CalTask(arr , start, middle);
            CalTask right = new CalTask(arr , middle, end);
            // 并行执行两个“小任务”
            left.fork();
            right.fork();
            // 把两个“小任务”累加的结果合并起来
            return left.join() + right.join();    // ①
        }
    }
}
public class Sum
{
    public static void main(String[] args)
        throws Exception
    {
        int[] arr = new int[100];
        Random rand = new Random();
        int total = 0;
        // 初始化100个数字元素
        for (int i = 0 , len = arr.length; i < len ; i++ )
        {
            int tmp = rand.nextInt(20);
            // 对数组元素赋值，并将数组元素的值添加到sum总和中。
            total += (arr[i] = tmp);
        }
        System.out.println(total);
        // 创建一个通用池
        ForkJoinPool pool = ForkJoinPool.commonPool();
        // 提交可分解的CalTask任务
        Future<Integer> future = pool.submit(new CalTask(arr , 0 , arr.length));
        System.out.println(future.get());
        // 关闭线程池
        pool.shutdown();
    }
}

```

[ifeve]:http://ifeve.com/parallel_and_con/

## ThreadPoolExecutor

构造方法：

```java
    public ThreadPoolExecutor(int corePoolSize,
                              int maximumPoolSize,
                              long keepAliveTime,
                              TimeUnit unit,
                              BlockingQueue<Runnable> workQueue,
                              ThreadFactory threadFactory,
                              RejectedExecutionHandler handler) 
```

参数说明：
- corePoolSize => 线程池核心线程数量
- maximumPoolSize => 线程池最大数量
- keepAliveTime => 空闲线程存活时间
- unit => 时间单位
- workQueue => 线程池所使用的缓冲队列
- threadFactory => 线程池创建线程使用的工厂
- handler => 线程池对拒绝任务的处理策略

线程池执行任务逻辑和线程池参数的关系：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200707171346.png)

总结：
- FixedThreadPool和SingleThreadExecutor => 允许的请求队列长度为Integer.MAX_VALUE，可能会堆积大量的请求，引起OOM异常
- CachedThreadPool => 允许创建的线程数为Integer.MAX_VALUE，可能会创建大量的线程，从而引起OOM异常

这就是为什么**禁止使用Executors去创建线程池**，而是推荐自己去创建ThreadPoolExecutor的原因

建议：
- CPU密集型 => 线程池的大小推荐为CPU数量 + 1，CPU数量可以根据Runtime.availableProcessors方法获取
- IO密集型 => CPU数量 * CPU利用率 * (1 + 线程等待时间/线程CPU时间)
- 混合型 => 将任务分为CPU密集型和IO密集型，然后分别使用不同的线程池去处理，从而使每个线程池可以根据各自的工作负载来调整
- 阻塞队列 => 推荐使用有界队列，有界队列有助于避免资源耗尽的情况发生
- 拒绝策略 => 默认采用的是AbortPolicy拒绝策略

拒绝策略推荐以下几种：
- 在程序中捕获RejectedExecutionException异常，在捕获异常中对任务进行处理。针对默认拒绝策略
- 使用CallerRunsPolicy拒绝策略，该策略会将任务交给调用execute的线程执行【一般为主线程】，此时主线程将在一段时间内不能提交任何任务，从而使工作线程处理正在执行的任务。此时提交的线程将被保存在TCP队列中，TCP队列满将会影响客户端，这是一种平缓的性能降低
- 自定义拒绝策略，只需要实现RejectedExecutionHandler接口即可
- 如果任务不是特别重要，使用DiscardPolicy和DiscardOldestPolicy拒绝策略将任务丢弃也是可以的

参考：https://mp.weixin.qq.com/s?__biz=MzIzMzgxOTQ5NA==&mid=2247489522&idx=1&sn=e56c83eae436e177306a4d818d2ef73c&chksm=e8fe8bfbdf8902eda75dd7afe069cf8d7a70b9b183b95035859a5ded5609956393a73a2f8b39&scene=0&xtrack=1&key=e44a2e70ef44435d01bde76d941a30b6506a00995ec8cf19fa95c3d5ee8b57544d830087e7ab29c8434eb07794c21bde261b18a28e1858b1291c85bae22ccf792f586479406a61c561b5e93554fa90c0&ascene=1&uin=MjY0NjE5NjUzNQ%3D%3D&devicetype=Windows+10&version=62070158&lang=zh_CN&pass_ticket=r8eJN8itmJD5vwKgZydZdBltEvSKH06rU7D%2FNO6Ri%2F1h4CI188V3eAcJuyFqEFCZ

