---
title: "Java-Se-面向对象"
date: 2017-07-01T11:12:47+08:00
draft: true
tags: [""]
categories: ["Java-Se"]
---

# 构造器

Java类 必须包含一个或一个以上的构造方法

如果程序员没有为一个类编写构造方法，则系统会为该类提供一个默认的且无参的构造方法。

一旦程序员为一个类提供了构造方法，系统将不再为该类提供构造方法。


# 面向对象的特点

封装、继承、多态。

## 封装

访问控制符（针对方法、变量）：

![decorate](https://oscimg.oschina.net/oscnet/e6e63f78b1654f8fce88ba82473aced3b9b.jpg "decorate")

## 继承

子类不能获得父类的构造器。

多继承：接口与接口之间可以多继承。

继承后，子类可以获得父类中除private修饰外的所有方法（静态、非静态）、属性（静态、非静态）。如果子类的静态方法、静态属性跟父类一致，则父类相应的静态方法、静态属性被隐藏，子类可以通过“父类.静态XXX”来访问。

## 多态

一句话：只有非静态方法具有多态性，常量、静态方法等不具有多态。

### 方法重写与重载

重写：两同两小一大  
1）两同：方法名相同、形参列表相同  
2）两小：返回值类型比父类更小或相同、抛出的异常比父类更小或相等  
3）一大：访问修饰符比父类更大或相等  

重载：两同一不同  
1）两同：同一个类同方法名  
2）参数列表不同

重载发生在同一个类中，重写发生在子父类的继承中。两者之间没有可比性。

# 接口

## 通用

接口的成员特点：  
1）成员变量 只能是常量。默认修饰符 public static final  
2）成员方法 只能是抽象方法。默认修饰符 public abstract

注：这些修饰符可以省略不写。

## JDK8

接口可以定义类（static）方法、默认（default）方法。

# 类

类的修饰符有public/abstract/final

## 命名

Java 标识符有如下命名规则： 
- 由26个英文字母大小写，数字：0-9，符号：_ $ 组成。（简记：字母、数字、两个符号）
- 标识符应以 **字母、_ 、$** 开头。（简记：除数字外，其它两种可以开关）
- 标识符不能是关键字

Java中严格区分大小写 

关于 javac xxx ，其中 xxx 不区分大小写，也就是 `javac MyTest.java` 与 `javac mytest.java` 都正常编译通过。 —— 20191104


## 内部类

### 分类

内部类分为

- 成员内部类，包括：静态内部类和非静态内部类
- 非成员内部类，包括：局部内部类和匿名内部类

非静态内部类可以访问外部类的成员变量（**实例变量、类变量**），包括**private修饰的实例变量**。

不允许外部类的静态成员中直接使用非静态内部类，不允许在非静态内部类里定义静态成员。（重点 ——20191025）

静态内部类，可以包含静态成员，也可以包含非静态成员。

局部内部类和匿名内部类访问局部变量时，局部变量要用final修饰。java8则会为局部变量自动添加final修饰（effectively final）。

```java
private Animator createAnimatorView(final View view, final int position) {
    MyAnimator animator = new MyAnimator();
    animator.addListener(new AnimatorListener() {
        @Override
        public void onAnimationEnd(Animator arg0) {
            Log.d(TAG, "position=" + position); 
        }
    });
    return animator;
}
```

### 图解

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200707183348.png)

参考：https://www.nowcoder.com/test/question/done?tid=25024105&qid=252898#summary

### 陷阱

下面的程序会报错：

```java
public class Outer {

    class Inner {}

    public static void foo() { new Inner(); }//编译错误

    public void bar() { new Inner(); }

    public static void main(String[] args) {
        new Inner();//编译错误
    }
}
```

可以这样理解：Inner内部类看作Outer 的非静态成员，按照“静态成员不能访问非静态成员”，所以报错了。
如果非要在静态方法中访问非静态的内部类，可以这样做：`new Outer().new Inner();`

由此看出，调用非静态内部类时，要包含有外部类的引用（this）。

问：非静态的方法实例化非静态内部类时，为什么不用传外部类的this？  
答：非静态的方法是实例方法，实例方法被调用的前提是已经创建好了外部类的实例。所以，此时不用传this。


# 流程控制

## 三目运算

- `System.out.println(1 < 2 ? 1:2);`

## 针对循环进行优化

死代码删除，代码外提，强度削弱，删除归纳变量，复写传播

https://www.nowcoder.com/test/question/done?tid=28827378&qid=304509#summary


# 值传递 & 引用传递

总结：在方法的形参中，都是值传递，也就是副本传递，不同的是：“值传递”传递的是值的副本，“引用传递”传递的是地址。在此特别注意，对于“引用传递”，如果在方法中只是对地址进行运算或处理，则不会影响原引用中的值；如果**对地址对应的对象中的值进行操作**，则会改变原引用中的值。——20191016

一句话，只有对引用的对象里面的值进行操作才会改变原引用。——20191016

```java
public class MyTest2 {
    public static void main(String[] args) {
        StringBuffer a = new StringBuffer("A");
        StringBuffer b = new StringBuffer("B");
        MyTest2 t = new MyTest2();
        t. caculate(a, b);
        System.out.println(a + "," + b);
    }

    private void caculate(StringBuffer a, StringBuffer b) {
        // 第一种，输出 AB,B
//        a.append(b);
//        b = a;

        // 第二种，输出 AB,BAB
        a.append(b);
        b.append(a);
    }
}
```

另一个经典题：

```java
public class Example {
    String str = new String("good");
    char[] ch = {'a', 'b', 'c'};

    public static void main(String[] args) {
        Example ex = new Example();
        ex.change(ex.str, ex.ch);
        System.out.print(ex.str + " and ");
        System.out.println(ex.ch);
    }

    public void change(String str, char[] ch) {
        str = "test ok";
        ch[0] = 'g';
    }
}
```

答案：good and gbc


# Q & A

## ==与equal的区别

1、如果是基本数据类型，用 ==，如果是对应的包装类型，强烈建议用 equals，因为缓存问题（-128~127） ——20191119

2、如果是引用类型

==：比较的是两个对象的地址，equal：比较的是两个值

```
public class test {
 public static void main(String[] args) {
      Person p1 = new Person();
      Person p2 = new Person();
      p1.setId(100);
      p2.setId(100);
      System.out.println(p1.equals(p2));//false
      System.out.println(p1 == p2);//false
 }
}
```

String字符串的缓存问题？？？？

答：JVM使用常量池来管理字符串，当使用`new String("hello")`时，JVM会使用常理池管理“hello”直接量，再创建一个String对象保存在堆内存中。一个在常量池，一个在堆内存。

示例代码：

```java
public class StringCompareTest
{
	public static void main(String[] args)
	{
		// s1直接引用常量池中的"疯狂Java"
		String s1 = "疯狂Java";
		String s2 = "疯狂";
		String s3 = "Java";
		// s4后面的字符串值可以在编译时就确定下来
		// s4直接引用常量池中的"疯狂Java"
		String s4 = "疯狂" + "Java";
		// s5后面的字符串值可以在编译时就确定下来
		// s5直接引用常量池中的"疯狂Java"
		String s5 = "疯" + "狂" + "Java";
		// s6后面的字符串值不能在编译时就确定下来，
		// 不能引用常量池中的字符串
		String s6 = s2 + s3;
		// 使用new调用构造器将会创建一个新的String对象，
		// s7引用堆内存中新创建的String对象
		String s7 = new String("疯狂Java");
		System.out.println(s1 == s4); // 输出true
		System.out.println(s1 == s5); // 输出true
		System.out.println(s1 == s6); // 输出false
		System.out.println(s1 == s7); // 输出false
	}
}
```

## has-a & is-a & like-a

- is-a：继承
- has-a：组合

参考：https://cloud.tencent.com/developer/ask/195015


[1]:http://www.cnblogs.com/DarrenChan/p/5738957.html
[2]:http://blog.csdn.net/molu_chase/article/details/52270746