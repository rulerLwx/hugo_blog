---
title: "Java-Se-集合"
date: 2017-07-01T10:19:47+08:00
draft: true
tags: [""]
categories: ["Java-Se"]
---

# Collection

## 集合体系、家族

常用的集合类：

```other
java.util.Collection
        |- Set
            |- HashSet
            |- LinkedHashSet
            |- TreeSet
            |- EnumSet
        |- List
            |- ArrayList
            |- LinkedList
            |- Vector
        |- Queue
            |- PriorityQueue
            |- Deque
                |- ArrayDeque
            |- LinkedList

java.util.Map
        |- HashMap
        |- Hashtable
        |- LinkedHashMap
        |- TreeMap
        |- EnumMap
        |- Properties

```

## 关于迭代

问：在迭代过程中能否增加、删除元素？

答：最好在迭代过程中不操作元素，将触发`java.util.ConcurrentModificationException`异常。如果真有此需求，有以下方案：
- 使用Iterator遍历集合，使用`it.remove()`方法删除集合中的元素，删除的是上一次`next()`方法返回的元素；
- 使用“写时复制”代替原生的集合，如使用`CopyOnWriteArrayList`替换`ArrayList`，适合“读多写少”的情况

`java.util.ConcurrentModificationException`异常，示例：

```java
    public static void main(String[] args) {
        HashSet<String> books = new HashSet<>();
        books.add("疯狂Java讲义");
        books.add("疯狂Androroid讲义");
        books.add("轻量级JavaEE企业应用实战");

        Iterator<String> it = books.iterator();
        while (it.hasNext()) {
            String book = it.next();
            if ("疯狂Androroid讲义".equals(book)) {
                it.remove();// 此处的删除，不报错
                //books.remove(book);// 此处的删除，报错
            }
        }
    }
```
参考《疯狂JAVA讲义（第三版）》P286

# 以前的笔记

```t
set:无序、不重复
list:有序、可重复

set:
只要不重复就可以存，用的是equals()方法,而不是==
   |-- equals()返回true，hashCode()返回true,------> 一定不存
   |--- equals()返回true，hashCode()返回false,------> 可存（因为是通过hashCode值来计算存储的位置，所以hashCode值不同，存在不同的位置。）
实际开发中，尽量保证：equals()返回true时，hashCode()也返回true

treeSet：
    |---subSet(),headSet(),tailSet()这三个方法理解成：treeSet是按小-->大（低-->高）排（升序）
    |---添加到treeSet中的对象所对应的类必须实现Comparable接口，否则抛ClassCastException
    |---添加到treeSet听的对象应该属于同一个类，否则抛ClassCastException
    
    treeSet是根据红黑树来计算存储的位置
    treeSet判断对象的唯一标准是：compareTo(Object obj)返回的是否是0,是则不存。

集合中应存放不可变的对象作为key

EnumSet不可以插入null值


在所有的Set集合中：
1、EnumSet性能最好，但所有元素必须是指定枚举类型的枚举值
2、其次是HashSet,
3、treeSet、LinkedHashSet可以记住插入的顺序

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~List集合
    |-----判断对象是否相等的标准是：equals()返回true;
    |-----listIteretor比Iterator灵活，可以前后迭代；

ArrayList，是不线程安全的
Vactor，是线程安全的，
Stack，是Vector子类，性能更差，推荐用LinkedList\LinkedDeque

Arrays.asList(Object ....a)返回的是一个ArrayList,这个ArrayList，程序只能遍历，不能增、删该集合元素。
---------------------------------------------------------------------------------------------------------------------------------Queue集合
    |----PriorityQueue，不是按队列的“先进先出”的方式，而是按由小到大的顺序排序和输出。

队列用poll，栈用pop

双端队列（队列和栈），进行操作的时候，如果用到是队列的方法，就把它看成是队列；如果用的是栈的方法，就把它看成是栈。
（队列的头就是栈的栈顶，队列的尾就是栈的底）

---------------------------------------------------------------------------------------------------------------------------------Map集合
|-----所有的key组成的是Set集合，所以具有无序、不可重复特点。判断key相等的标准同Set，即equals()、hashCode().
|-----所有的value组成的是List集合，所以具有有序、可重复特点。

Hashtable：古老，线程安全，key、value不可以为null
HashMap：1.2开始，线程不安全，key、value可以为null

Hashtable、HashMap判断两个key是否相等的标准是：两个key通过equals()方法比较返回true时，两个key通过HashCode()方法返回也是true。（把key当作set集合来看，符合set的标准）

HashMap、Hashtable不保证插入的顺序，LinkedHashMap保持插入时的顺序。

Properties是Hashtable的子类。

SortedMap接口和TreeMap实现类：
TreeMap：红黑树，判断key相等的标准与TreeSet同：两个key通过compareTo()方法比较返回0，

WeakHashMap:弱引用

IdentityHashMap:当且仅当两个key严格相等（key1==key2）时，IdetityHashMap才认为这两个key相等。

EnumMap：key不可以为null,key必须是单个枚举类的枚举值。

总结
可以维护顺序的Map有：
LinkedHashMap保持插入时的顺序，
TreeMap有自然顺序、定制顺序，
EnumMap自然顺序，即枚举类中定义的顺序，
注：HashMap是无序的

Enumeration接口：jdk1.0开始，方法命名太长，建议用Iterator替代，

操作集合的工具类：Collections

=============================================================================
Map集合的containsKey()方法，要想bean中的某个实体是否相等，这个实体必须要重写hashCode()、equals()方法。

iterator it = xxx.iterator;
每次it.next(),这个”iterator表格“都会向下移动一行，即不能操作上一行数据了，甚至会抛NoSuchElementException异常。
```

# Map

常用的Map有
- HashMap
- LinkedHashSet
- ConcurrentHashMap

## HashMap

### 存数据`put(K key, V value) `

+ 以“数组+链表”结构实现存储。
+ 以hash算法计算存储位置。
+ 默认数组大小：`int DEFAULT_INITIAL_CAPACITY = 1 << 4; // aka 16`，一定要是2的次方。
+ 扩展数组策略：`resize(2 * table.length);`
+ 用Entry类来封装一个key-value对，其中next即是用链表方式保存另一个key-value对，也就是“通过hash相同，但equals()方法返回false”的键值对。

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200707182216.png)

JDK8，当链表长度大于8时，链表就转换为红黑树，提高查询效率。

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200707182248.png)

### 取数据`get(key)`

步骤：
- 获取key的hashcode，通过hash()算法获得hash值，进而定位到数组位置
- 如果有多个元素，在链表上挨个比较key对象，调用的是equals()方法

Java中规定：两个equals()为true的对象必须具有相同的hashcode。

---

HashMap之put、get源码：

```java
    public V put(K key, V value) {
        if (table == EMPTY_TABLE) {
            inflateTable(threshold);
        }
        if (key == null)
            return putForNullKey(value);
        int hash = hash(key);
        int i = indexFor(hash, table.length);
        //下面的for循环的作用是：对于key，hash相同时，判断equals，注意这里的next，跟Entry中的next一致
        //也就是链表关联元素
        for (Entry<K,V> e = table[i]; e != null; e = e.next) {      
            Object k;
            if (e.hash == hash && ((k = e.key) == key || key.equals(k))) {
                V oldValue = e.value;
                e.value = value;
                e.recordAccess(this);
                return oldValue;
            }
        }

        modCount++;
        addEntry(hash, key, value, i);
        return null;
    }

    public V get(Object key) {
        if (key == null)
            return getForNullKey();
        Entry<K,V> entry = getEntry(key);

        return null == entry ? null : entry.getValue();
    }
    final Entry<K,V> getEntry(Object key) {
        if (size == 0) {
            return null;
        }

        int hash = (key == null) ? 0 : hash(key);
        // 这个for循环作用：循环查找同一个key下的不同值
        for (Entry<K,V> e = table[indexFor(hash, table.length)];
             e != null;
             e = e.next) {
            Object k;
            if (e.hash == hash &&
                ((k = e.key) == key || (key != null && key.equals(k))))
                return e;
        }
        return null;
    }

```

总结：关键是要理解Entry类中的next实例变量的作用——作链表结构用，才能理解key的hash相同而equal()返回false的存储情况。

### 扩容

HashMap初始大小是16，当数组中的元素达到 0.75*数组length 时，就重新调整数组大小为原来的2倍。

扩容的本质是重新定义更大的数组，将原来数组中的元素拷贝到新数据中。

问：HashMap扩容后，元素位置如何变化？

答：HashMap 扩容后，原来的元素，要么在原位置，要么在原位置+原数组长度 那个位置上。

参考：https://blog.csdn.net/wohaqiyi/article/details/81448176


## HashMap和ConcurrentHashMap区别

Hashtable线程安全，但效率低


# Set

- HashSet

  1. 内部是HashMap。
  2. add(key)方法，其实是调用HashMap的put(key,new Object())，所有的value用一个Object对象代替。


# List

- ArrayList

    + 以数组结构实现List.
    + 默认的数组大小是10，`DEFAULT_CAPACITY = 10;`.
    + 当数组装满了，扩展的策略：`int newCapacity = oldCapacity + (oldCapacity >> 1);`，也就是增加原来数组长度的一半，另外，如果增加的是集合，则需要另外判断扩展的大小，则按最后复制数组`elementData = Arrays.copyOf(elementData, newCapacity);`

- LinkedList

    + 以链表结构实现List。
    + Node静态内部类，是实现链表的关键。

# FAQ

## 56、TreeMap和TreeSet在排序时如何比较元素？Collections工具类中的sort()方法如何比较元素？

答：TreeSet要求存放的对象所属的类必须实现Comparable接口，该接口提供了比较元素的compareTo()方法，当插入元素时会回调该方法比较元素的大小。TreeMap要求存放的键值对映射的键必须实现Comparable接口从而根据键对元素进行排序。Collections工具类的sort方法有两种重载的形式，第一种要求传入的待排序容器中存放的对象比较实现Comparable接口以实现元素的比较；第二种不强制性的要求容器中的元素必须可比较，但是要求传入第二个参数，参数是Comparator接口的子类型（需要重写compare方法实现元素的比较），相当于一个临时定义的排序规则，其实就是通过接口注入比较元素大小的算法，也是对回调模式的应用（Java中对函数式编程的支持）。

参考：http://blog.csdn.net/jackfrued/article/details/44921941


## java.util.ConcurrentModificationException

集合在add/set/remove时，都会使modCount++，for(XXX xx : Collection)循环内部使用的就是iterator进行遍历，调用next()方法时会判断modCount != expectedModCount时，就抛此异常。

总结：遍历集合过程中不能添加元素，但可以删除元素（使用iterator.remove）。


## 一个ArrayList在循环过程中删除，会不会出问题，为什么。

List 循环遍历中删除元素问题一：https://blog.csdn.net/weixin_36759405/article/details/82767430

List 循环遍历中删除元素问题二：https://blog.csdn.net/weixin_36759405/article/details/83904452



## HashMap底层为什么一定用数组

1）数组效率高

在HashMap中，定位桶的位置是利用元素的key的哈希值对数组长度取模得到。此时，我们已得到桶的位置。显然数组的查找效率比LinkedList大。

2）可自定义扩容机制

采用基本数组结构，扩容机制可以自己定义，HashMap中数组扩容刚好是2的次幂，在做取模运算的效率高。

（如：ArrayList底层也是数组，但是扩容机制是1.5倍扩容）

https://www.cnblogs.com/MWCloud/p/11379775.html