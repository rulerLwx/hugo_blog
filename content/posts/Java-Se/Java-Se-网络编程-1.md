---
title: "Java-Se-网络编程-1"
date: 2017-07-01T11:13:47+08:00
draft: true
tags: ["网络编程"]
categories: ["Java-Se"]
---

# 网络分层

![osi-tcp-ip](https://oscimg.oschina.net/oscnet/94f7613301801bab487d0342afe2f1ac07a.jpg "osi-tcp-ip")

![tcp-ip](https://oscimg.oschina.net/oscnet/ab08b17d8d50d151a1adc405cfdce62b6ac.jpg "tcp-ip")

# GET/POST

## 类与API

### InetAddress

将IP地址封装成一个类，

### URLDecoder、URLEncoder

普通字符串与application/x-www-form-urlencoded MIME转换

### URL、URLConnection、URLPermission

URI是一种抽象，URL是URI的一种具体实现。URL是一种绝对定位，必须能找到资源。
通过URL能获取URLConnection，URLPermission是JDK8提供的用于管理URLConnection权限问题。

# GET/POST示例

```java
import java.io.*;
import java.net.*;
import java.util.*;

public class GetPostTest
{
    /**
    * 向指定URL发送GET方法的请求
    * @param url 发送请求的URL
    * @param param 请求参数，格式满足name1=value1&name2=value2的形式。
    * @return URL所代表远程资源的响应
    */
    public static String sendGet(String url , String param)
    {
        String result = "";
        String urlName = url + "?" + param;
        try
        {
            URL realUrl = new URL(urlName);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent"
                , "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");
            // 建立实际的连接
            conn.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = conn.getHeaderFields();
            // 遍历所有的响应头字段
            for (String key : map.keySet())
            {
                System.out.println(key + "--->" + map.get(key));
            }
            try(
                // 定义BufferedReader输入流来读取URL的响应
                BufferedReader in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream() , "utf-8")))
            {
                String line;
                while ((line = in.readLine())!= null)
                {
                    result += "\n" + line;
                }
            }
        }
        catch(Exception e)
        {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
        }
        return result;
    }
    /**
    * 向指定URL发送POST方法的请求
    * @param url 发送请求的URL
    * @param param 请求参数，格式应该满足name1=value1&name2=value2的形式。
    * @return URL所代表远程资源的响应
    */
    public static String sendPost(String url , String param)
    {
        String result = "";
        try
        {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent",
            "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            try(
                // 获取URLConnection对象对应的输出流
                PrintWriter out = new PrintWriter(conn.getOutputStream()))
            {
                // 发送请求参数
                out.print(param);
                // flush输出流的缓冲
                out.flush();
            }
            try(
                // 定义BufferedReader输入流来读取URL的响应
                BufferedReader in = new BufferedReader(new InputStreamReader
                    (conn.getInputStream() , "utf-8")))
            {
                String line;
                while ((line = in.readLine())!= null)
                {
                    result += "\n" + line;
                }
            }
        }
        catch(Exception e)
        {
            System.out.println("发送POST请求出现异常！" + e);
            e.printStackTrace();
        }
        return result;
    }
    // 提供主方法，测试发送GET请求和POST请求
    public static void main(String args[])
    {
        // 发送GET请求
        String s = GetPostTest.sendGet("http://localhost:8888/abc/a.jsp"
            , null);
        System.out.println(s);
        // 发送POST请求
        String s1 = GetPostTest.sendPost("http://localhost:8888/abc/login.jsp"
            , "name=crazyit.org&pass=leegang");
        System.out.println(s1);
    }
}
```

# TCP

tcp/ip，两端各建立一个Socket，并通过Socket产生IO流来进行网络通信。

tcp是一种可靠的网络协议（三次握手）。

TCP分客户端、服务端，分别用Socket、ServiceSocket表示。Socket也称“套接字”。

## 示例

Server.java

```java

import java.net.*;
import java.io.*;

public class Server
{
    public static void main(String[] args) throws IOException
    {
        // 创建一个ServerSocket，用于监听客户端Socket的连接请求
        ServerSocket ss = new ServerSocket(30000);
        // 采用循环不断接受来自客户端的请求
        while (true)
        {
            // 每当接受到客户端Socket的请求，服务器端也对应产生一个Socket
            Socket s = ss.accept();
            // 将Socket对应的输出流包装成PrintStream
            PrintStream ps = new PrintStream(s.getOutputStream());
            // 进行普通IO操作
            ps.println("您好，您收到了服务器的新年祝福！");
            // 关闭输出流，关闭Socket
            ps.close();
            s.close();
        }
    }
}
```

Client.java

```java

import java.net.*;
import java.io.*;

public class Client
{
    public static void main(String[] args)
        throws IOException
    {
        Socket socket = new Socket("127.0.0.1" , 30000);   // ①
        // 将Socket对应的输入流包装成BufferedReader
        BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        // 进行普通IO操作
        String line = br.readLine();
        System.out.println("来自服务器的数据：" + line);
        // 关闭输入流、socket
        br.close();
        socket.close();
    }
}
```

## 加入多线程的TCP通信

服务端为接收到的每一个客户端开启一个线程进行相应数据处理。

### 示例

MyServer.java

```java

import java.net.*;
import java.io.*;
import java.util.*;

public class MyServer
{
    // 定义保存所有Socket的ArrayList，并将其包装为线程安全的
    public static List<Socket> socketList
        = Collections.synchronizedList(new ArrayList<>());
    public static void main(String[] args)
        throws IOException
    {
        ServerSocket ss = new ServerSocket(30000);
        while(true)
        {
            // 此行代码会阻塞，将一直等待别人的连接
            Socket s = ss.accept();
            socketList.add(s);
            // 每当客户端连接后启动一条ServerThread线程为该客户端服务
            new Thread(new ServerThread(s)).start();
        }
    }
}
```

ServerThread.java

```java

import java.io.*;
import java.net.*;

// 负责处理每个线程通信的线程类
public class ServerThread implements Runnable
{
    // 定义当前线程所处理的Socket
    Socket s = null;
    // 该线程所处理的Socket所对应的输入流
    BufferedReader br = null;
    public ServerThread(Socket s)
    throws IOException
    {
        this.s = s;
        // 初始化该Socket对应的输入流
        br = new BufferedReader(new InputStreamReader(s.getInputStream()));
    }
    public void run()
    {
        try
        {
            String content = null;
            // 采用循环不断从Socket中读取客户端发送过来的数据
            while ((content = readFromClient()) != null)
            {
                // 遍历socketList中的每个Socket，
                // 将读到的内容向每个Socket发送一次
                for (Socket s : MyServer.socketList)
                {
                    PrintStream ps = new PrintStream(s.getOutputStream());
                    ps.println(content);
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    // 定义读取客户端数据的方法
    private String readFromClient()
    {
        try
        {
            return br.readLine();
        }
        // 如果捕捉到异常，表明该Socket对应的客户端已经关闭
        catch (IOException e)
        {
            // 删除该Socket。
            MyServer.socketList.remove(s);      // ①
        }
        return null;
    }
}
```

MyClient.java

```java

import java.io.*;
import java.net.*;

public class MyClient
{
    public static void main(String[] args)throws Exception
    {
        Socket s = new Socket("127.0.0.1" , 30000);
        // 客户端启动ClientThread线程不断读取来自服务器的数据
        new Thread(new ClientThread(s)).start();   // ①
        // 获取该Socket对应的输出流
        PrintStream ps = new PrintStream(s.getOutputStream());
        String line = null;
        // 不断读取键盘输入
        BufferedReader br = new BufferedReader(
            new InputStreamReader(System.in));
        while ((line = br.readLine()) != null)
        {
            // 将用户的键盘输入内容写入Socket对应的输出流
            ps.println(line);
        }
    }
}
```

ClientThread.java

```java

import java.io.*;
import java.net.*;

public class ClientThread implements Runnable
{
    // 该线程负责处理的Socket
    private Socket s;
    // 该线程所处理的Socket所对应的输入流
    BufferedReader br = null;
    public ClientThread(Socket s)
        throws IOException
    {
        this.s = s;
        br = new BufferedReader(
            new InputStreamReader(s.getInputStream()));
    }
    public void run()
    {
        try
        {
            String content = null;
            // 不断读取Socket输入流中的内容，并将这些内容打印输出
            while ((content = br.readLine()) != null)
            {
                System.out.println(content);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
```

# 半关闭Socket

- shutdownInput()：关闭该Socket的输入流，程序还可通过该Socket的输出流输出数据。
- hutdownOutput()：关闭该Socket的输出流，程序还可通过该Socket的输入流读取数据。

“半关闭”，指关闭了输出流或输入流，但Socket还没有完全关闭。
半关闭的目的：表示输入或输出流已经传输结束。
如果一个Socket先后调用了shutdownInput()、shutdownOutput()方法，该Socket实例依然没有被关闭，只是该Socket既不能输出数据，也不能读取数据而已。

当调用shutdownInput()或shutdownOutput()方法关闭了输入或输出流之后，该Socket无法再次打开输入或输出流。因此这种做法通常不适合保持持久通信状态的交互式应用，只适用于一站式的通信协议，例如HTTP协议——客户端连接到服务器后，开始发送请求数据，发送完成后无须再次发送数据，只需读取服务端响应数据即可，当读取完成后，该Socket连接也被关闭了。

# 使用BIO实现阻塞通信

BIO（block input output），这是传统的IO操作。

# 使用NIO实现非阻塞通信

NIO（New IO）是JDK1.4开始提供的API。

主要是使用Selector类，用于监控SelectableChannel的IO状况。只有当IO操作准备好时（通道注册时设置不同的状态，如SelectionKey.OP_READ）才会把IO交给线程处理。

所有的Channel默认都是阻塞模式。

## 示例

NServer.java

```java

import java.net.*;
import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.charset.*;

public class NServer
{
    // 用于检测所有Channel状态的Selector
    private Selector selector = null;
    static final int PORT = 30000;
    // 定义实现编码、解码的字符集对象
    private Charset charset = Charset.forName("UTF-8");
    public void init()throws IOException
    {
        selector = Selector.open();
        // 通过open方法来打开一个未绑定的ServerSocketChannel实例
        ServerSocketChannel server = ServerSocketChannel.open();
        InetSocketAddress isa = new InetSocketAddress("127.0.0.1", PORT);
        // 将该ServerSocketChannel绑定到指定IP地址
        server.bind(isa);
        // 设置ServerSocket以非阻塞方式工作
        server.configureBlocking(false);
        // 将server注册到指定Selector对象
        server.register(selector, SelectionKey.OP_ACCEPT);
        while (selector.select() > 0)
        {
            // 依次处理selector上的每个已选择的SelectionKey
            for (SelectionKey sk : selector.selectedKeys())
            {
                // 从selector上的已选择Key集中删除正在处理的SelectionKey
                selector.selectedKeys().remove(sk);      // ①
                // 如果sk对应的Channel包含客户端的连接请求
                if (sk.isAcceptable())        // ②
                {
                    // 调用accept方法接受连接，产生服务器端的SocketChannel
                    SocketChannel sc = server.accept();
                    // 设置采用非阻塞模式
                    sc.configureBlocking(false);
                    // 将该SocketChannel也注册到selector
                    sc.register(selector, SelectionKey.OP_READ);
                    // 将sk对应的Channel设置成准备接受其他请求
                    sk.interestOps(SelectionKey.OP_ACCEPT);
                }
                // 如果sk对应的Channel有数据需要读取
                if (sk.isReadable())     // ③
                {
                    // 获取该SelectionKey对应的Channel，该Channel中有可读的数据
                    SocketChannel sc = (SocketChannel)sk.channel();
                    // 定义准备执行读取数据的ByteBuffer
                    ByteBuffer buff = ByteBuffer.allocate(1024);
                    String content = "";
                    // 开始读取数据
                    try
                    {
                        while(sc.read(buff) > 0)
                        {
                            buff.flip();
                            content += charset.decode(buff);
                        }
                        // 打印从该sk对应的Channel里读取到的数据
                        System.out.println("读取的数据：" + content);
                        // 将sk对应的Channel设置成准备下一次读取
                        sk.interestOps(SelectionKey.OP_READ);
                    }
                    // 如果捕捉到该sk对应的Channel出现了异常，即表明该Channel
                    // 对应的Client出现了问题，所以从Selector中取消sk的注册
                    catch (IOException ex)
                    {
                        // 从Selector中删除指定的SelectionKey
                        sk.cancel();
                        if (sk.channel() != null)
                        {
                            sk.channel().close();
                        }
                    }
                    // 如果content的长度大于0，即聊天信息不为空
                    if (content.length() > 0)
                    {
                        // 遍历该selector里注册的所有SelectionKey
                        for (SelectionKey key : selector.keys())
                        {
                            // 获取该key对应的Channel
                            Channel targetChannel = key.channel();
                            // 如果该channel是SocketChannel对象
                            if (targetChannel instanceof SocketChannel)
                            {
                                // 将读到的内容写入该Channel中
                                SocketChannel dest = (SocketChannel)targetChannel;
                                dest.write(charset.encode(content));
                            }
                        }
                    }
                }
            }
        }
    }
    public static void main(String[] args)
        throws IOException
    {
        new NServer().init();
    }
}
```

NClient.java

```java

import java.util.*;
import java.net.*;
import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.charset.*;

public class NClient
{
    // 定义检测SocketChannel的Selector对象
    private Selector selector = null;
    static final int PORT = 30000;
    // 定义处理编码和解码的字符集
    private Charset charset = Charset.forName("UTF-8");
    // 客户端SocketChannel
    private SocketChannel sc = null;
    public void init()throws IOException
    {
        selector = Selector.open();
        InetSocketAddress isa = new InetSocketAddress("127.0.0.1", PORT);
        // 调用open静态方法创建连接到指定主机的SocketChannel
        sc = SocketChannel.open(isa);
        // 设置该sc以非阻塞方式工作
        sc.configureBlocking(false);
        // 将SocketChannel对象注册到指定Selector
        sc.register(selector, SelectionKey.OP_READ);
        // 启动读取服务器端数据的线程
        new ClientThread().start();
        // 创建键盘输入流
        Scanner scan = new Scanner(System.in);
        while (scan.hasNextLine())
        {
            // 读取键盘输入
            String line = scan.nextLine();
            // 将键盘输入的内容输出到SocketChannel中
            sc.write(charset.encode(line));
        }
    }
    // 定义读取服务器数据的线程
    private class ClientThread extends Thread
    {
        public void run()
        {
            try
            {
                while (selector.select() > 0)    // ①
                {
                    // 遍历每个有可用IO操作Channel对应的SelectionKey
                    for (SelectionKey sk : selector.selectedKeys())
                    {
                        // 删除正在处理的SelectionKey
                        selector.selectedKeys().remove(sk);
                        // 如果该SelectionKey对应的Channel中有可读的数据
                        if (sk.isReadable())
                        {
                            // 使用NIO读取Channel中的数据
                            SocketChannel sc = (SocketChannel)sk.channel();
                            ByteBuffer buff = ByteBuffer.allocate(1024);
                            String content = "";
                            while(sc.read(buff) > 0)
                            {
                                sc.read(buff);
                                buff.flip();
                                content += charset.decode(buff);
                            }
                            // 打印输出读取的内容
                            System.out.println("聊天信息：" + content);
                            // 为下一次读取作准备
                            sk.interestOps(SelectionKey.OP_READ);
                        }
                    }
                }
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }
    }
    public static void main(String[] args)
        throws IOException
    {
        new NClient().init();
    }
}
```

也可以参考笔记《Java-NIO》的阻塞式、非阻塞式。

# 使用AIO实现非阻塞通信

AIO（Asynchronous IO），异步IO，是JDK7提供的API。

## 如何区分IO的同步、异步和阻塞、非阻塞？

```txt
答：IO的操作可以分两步：
1）程序发出IO请求；
2）完成实际的IO操作
如果按第一步来划分：发出IO请求会阻塞线程，则是阻塞IO，如果发出IO请求没有阻塞线程，则是非阻塞式IO。
如果按第二步来划分：实际的IO操作由操作系统来完成，再将结果返回给应用程序，则是异步IO；实际的IO操作由应用程序本身去执行，会阻塞IO，是同步IO。
```

## 示例

```txt
使用AsynchronousServerSocketChannel只要三步：
1、调用自身的open()静态方法创建AsynchronousServerSocketChannel。
2、调用AsynchronousServerSocketChannel的bind()方法指定IP地址、端口。
3、调用AsynchronousServerSocketChannel的accept()方法接受连接请求。

使用AsynchronousSocketChannel也只要三步：
1、调用自身的open()静态方法创建AsynchronousSocketChannel。
2、调用AsynchronousSocketChannel的connect()方法连接到指定IP、端口。
调用AsynchronousSocketChannel的read()、write()方法进行读写。
```

下面是最简示例：

SimpleAIOServer.java

```java

import java.net.*;
import java.nio.*;
import java.nio.channels.*;
import java.util.concurrent.*;

public class SimpleAIOServer
{
    static final int PORT = 30000;
    public static void main(String[] args)
        throws Exception
    {
        try(
            // ①创建AsynchronousServerSocketChannel对象。
            AsynchronousServerSocketChannel serverChannel =
                AsynchronousServerSocketChannel.open())
        {
            // ②指定在指定地址、端口监听。
            serverChannel.bind(new InetSocketAddress(PORT));
            while (true)
            {
                // ③采用循环接受来自客户端的连接
                Future<AsynchronousSocketChannel> future
                    = serverChannel.accept();
                // 获取连接完成后返回的AsynchronousSocketChannel
                AsynchronousSocketChannel socketChannel = future.get();
                // 执行输出。
                socketChannel.write(ByteBuffer.wrap("欢迎你来自AIO的世界！"
                    .getBytes("UTF-8"))).get();
            }
        }
    }
}
```

SimpleAIOClient.java

```java

import java.net.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.charset.*;

public class SimpleAIOClient
{
    static final int PORT = 30000;
    public static void main(String[] args)
        throws Exception
    {
        // 用于读取数据的ByteBuffer。
        ByteBuffer buff = ByteBuffer.allocate(1024);
        Charset utf = Charset.forName("utf-8");
        try(
            // ①创建AsynchronousSocketChannel对象
            AsynchronousSocketChannel clientChannel
                = AsynchronousSocketChannel.open())
        {
            // ②连接远程服务器
            clientChannel.connect(new InetSocketAddress("127.0.0.1"
                , PORT)).get();     // ④
            buff.clear();
            // ③从clientChannel中读取数据
            clientChannel.read(buff).get();     // ⑤
            buff.flip();
            // 将buff中内容转换为字符串
            String content = utf.decode(buff).toString();
            System.out.println("服务器信息：" + content);
        }
    }
}
```

直接创建AsynchronousServerSocketChannel的代码片段如下：

```java
AsynchronousServerSocketChannel serverSocketChannel = 
		AsynchronousServerSocketChannel.open().bind(new InetSocketAddress(PORT));
```

使用AsynchronousChannelGroup创建AsynchronousServerSocketChannel的代码片段如下：

```java
// 创建一个线程池
ExecutorService executor = Executors.newFixedThreadPool(20);
// 以指定线程池来创建一个AsynchronousChannelGroup
AsynchronousChannelGroup channelGroup = AsynchronousChannelGroup
	.withThreadPool(executor);
// 以指定线程池来创建一个AsynchronousServerSocketChannel
AsynchronousServerSocketChannel serverChannel
	= AsynchronousServerSocketChannel.open(channelGroup)
	// 指定监听本机的PORT端口
	.bind(new InetSocketAddress(PORT));
```

```txt
AsynchronousServerSocketChannel的accept()方法有两个版本，有Future返回值的会阻塞（必须调用get()方法才有返回值），另外一个方法是异步的：

- Future<AsynchronousSocketChannel> accept()，接受客户端请求，如果程序需要获得连接成功后返回的AsynchronousSocketChannel，则应调用该方法返回的Future对象的get()方法——但get()方法会阻塞线程，因此这种方式依然会阻塞当前线程。
- <Object> void accept(Object attachment, CompletionHandler<AsynchronousSocketChannel, ? super Object> handler)，接受客户端请求，连接成功或失败都会触发CompletionHandler对象里相应的方法。其中AsynchronousSocketChannel就代表连接成功后返回的AsynchronousSocketChannel。

CompletionHandler是个接口，该接口定义了两个方法：
- completed(AsynchronousSocketChannel result, Object attachment)，当IO操作成功时触发该方法。第一个参数代表IO操作成功后返回的对象，第二参数代表发起IO操作时传入的附加参数。
- failed(Throwable exc, Object attachment)，当IO操作失败时触发的方法。

AsynchronousSocketChannel的connect()、read()、write()方法都有两个版本，同上。
```

完整的示例如下：

AIOServer.java

```java

import java.net.*;
import java.io.*;
import java.util.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.charset.*;
import java.util.concurrent.*;

public class AIOServer
{
    static final int PORT = 30000;
    final static String UTF_8 = "utf-8";
    static List<AsynchronousSocketChannel> channelList
        = new ArrayList<>();
    public void startListen() throws InterruptedException,
        Exception
    {
        // 创建一个线程池
        ExecutorService executor = Executors.newFixedThreadPool(20);
        // 以指定线程池来创建一个AsynchronousChannelGroup
        AsynchronousChannelGroup channelGroup = AsynchronousChannelGroup
            .withThreadPool(executor);
        // 以指定线程池来创建一个AsynchronousServerSocketChannel
        AsynchronousServerSocketChannel serverChannel
            = AsynchronousServerSocketChannel.open(channelGroup)
            // 指定监听本机的PORT端口
            .bind(new InetSocketAddress(PORT));
        // 使用CompletionHandler接受来自客户端的连接请求
        serverChannel.accept(null, new AcceptHandler(serverChannel));  // ①
        Thread.sleep(5000);
    }
    public static void main(String[] args)
        throws Exception
    {
        AIOServer server = new AIOServer();
        server.startListen();
    }
    }
    // 实现自己的CompletionHandler类
    class AcceptHandler implements
    CompletionHandler<AsynchronousSocketChannel, Object>
    {
    private AsynchronousServerSocketChannel serverChannel;
    public AcceptHandler(AsynchronousServerSocketChannel sc)
    {
        this.serverChannel = sc;
    }
    // 定义一个ByteBuffer准备读取数据
    ByteBuffer buff = ByteBuffer.allocate(1024);
    // 当实际IO操作完成时候触发该方法
    @Override
    public void completed(final AsynchronousSocketChannel sc
        , Object attachment)
    {
        // 记录新连接的进来的Channel
        AIOServer.channelList.add(sc);
        // 准备接受客户端的下一次连接
        serverChannel.accept(null , this);
        sc.read(buff , null
            , new CompletionHandler<Integer,Object>()  // ②
        {
            @Override
            public void completed(Integer result
                , Object attachment)
            {
                buff.flip();
                // 将buff中内容转换为字符串
                String content = StandardCharsets.UTF_8
                    .decode(buff).toString();
                // 遍历每个Channel，将收到的信息写入各Channel中
                for(AsynchronousSocketChannel c : AIOServer.channelList)
                {
                    try
                    {
                        c.write(ByteBuffer.wrap(content.getBytes(
                            AIOServer.UTF_8))).get();
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }
                }
                buff.clear();
                // 读取下一次数据
                sc.read(buff , null , this);
            }
            @Override
            public void failed(Throwable ex, Object attachment)
            {
                System.out.println("读取数据失败: " + ex);
                // 从该Channel读取数据失败，就将该Channel删除
                AIOServer.channelList.remove(sc);
            }
        });
    }
    @Override
    public void failed(Throwable ex, Object attachment)
    {
        System.out.println("连接失败: " + ex);
    }
}
```

AIOClient.java

```java

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.charset.*;
import java.util.concurrent.*;

public class AIOClient
{
    final static String UTF_8 = "utf-8";
    final static int PORT = 30000;
    // 与服务器端通信的异步Channel
    AsynchronousSocketChannel clientChannel;
    JFrame mainWin = new JFrame("多人聊天");
    JTextArea jta = new JTextArea(16 , 48);
    JTextField jtf = new JTextField(40);
    JButton sendBn = new JButton("发送");
    public void init()
    {
        mainWin.setLayout(new BorderLayout());
        jta.setEditable(false);
        mainWin.add(new JScrollPane(jta), BorderLayout.CENTER);
        JPanel jp = new JPanel();
        jp.add(jtf);
        jp.add(sendBn);
        // 发送消息的Action,Action是ActionListener的子接口
        Action sendAction = new AbstractAction()
        {
            public void actionPerformed(ActionEvent e)
            {
                String content = jtf.getText();
                if (content.trim().length() > 0)
                {
                    try
                    {
                        // 将content内容写入Channel中
                        clientChannel.write(ByteBuffer.wrap(content
                            .trim().getBytes(UTF_8))).get();    //①
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }
                }
                // 清空输入框
                jtf.setText("");
            }
        };
        sendBn.addActionListener(sendAction);
        // 将Ctrl+Enter键和"send"关联
        jtf.getInputMap().put(KeyStroke.getKeyStroke('\n'
            , java.awt.event.InputEvent.CTRL_MASK) , "send");
        // 将"send"和sendAction关联
        jtf.getActionMap().put("send", sendAction);
        mainWin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainWin.add(jp , BorderLayout.SOUTH);
        mainWin.pack();
        mainWin.setVisible(true);
    }
    public void connect()
        throws Exception
    {
        // 定义一个ByteBuffer准备读取数据
        final ByteBuffer buff = ByteBuffer.allocate(1024);
        // 创建一个线程池
        ExecutorService executor = Executors.newFixedThreadPool(80);
        // 以指定线程池来创建一个AsynchronousChannelGroup
        AsynchronousChannelGroup channelGroup =
            AsynchronousChannelGroup.withThreadPool(executor);
        // 以channelGroup作为组管理器来创建AsynchronousSocketChannel
        clientChannel = AsynchronousSocketChannel.open(channelGroup);
        // 让AsynchronousSocketChannel连接到指定IP、指定端口
        clientChannel.connect(new InetSocketAddress("127.0.0.1"
            , PORT)).get();
        jta.append("---与服务器连接成功---\n");
        buff.clear();
        clientChannel.read(buff, null
            , new CompletionHandler<Integer,Object>()   //②
        {
            @Override
            public void completed(Integer result, Object attachment)
            {
                buff.flip();
                // 将buff中内容转换为字符串
                String content = StandardCharsets.UTF_8
                    .decode(buff).toString();
                // 显示从服务器端读取的数据
                jta.append("某人说：" + content + "\n");
                buff.clear();
                clientChannel.read(buff , null , this);
            }
            @Override
            public void failed(Throwable ex, Object attachment)
            {
                System.out.println("读取数据失败: " + ex);
            }
        });
    }
    public static void main(String[] args)
        throws Exception
    {
        AIOClient client = new AIOClient();
        client.init();
        client.connect();
    }
}
```

# UDP

udp是一种不可靠的网络协议，只负责发送或接收（不管对方的状态）。

udp没有服务端、客户端之分，都用DatagramSocket表示，不维护状态、不产生IO流，只负责接收和发送数据报。而数据报用DatagramPacket表示。

虽然没有服务端、客户端之分，但通常固定IP、固定端口的DatagramSocket对象所在的程序称为服务器，可以用它来接收客户端的数据。

## 原理示例

![udp-example](https://oscimg.oschina.net/oscnet/07922bfb36f8dbcd98cf58aa9cd1f3adf77.jpg "udp-example")

```java
class UDPServer{
    public static void main(String[] args)throws IOException{
        DatagramSocket  server = new DatagramSocket(5050);
        byte[] recvBuf = new byte[100];
        DatagramPacket recvPacket 
            = new DatagramPacket(recvBuf , recvBuf.length);
        server.receive(recvPacket);
        String recvStr = new String(recvPacket.getData() , 0 , recvPacket.getLength());
        System.out.println("Hello World!" + recvStr);
        int port = recvPacket.getPort();
        InetAddress addr = recvPacket.getAddress();
        String sendStr = "Hello ! I'm Server";
        byte[] sendBuf;
        sendBuf = sendStr.getBytes();
        DatagramPacket sendPacket 
            = new DatagramPacket(sendBuf , sendBuf.length , addr , port );
        server.send(sendPacket);
        server.close();
    }
}
class UDPClient{
    public static void main(String[] args)throws IOException{
        DatagramSocket client = new DatagramSocket();
        
        String sendStr = "Hello! I'm Client";
        byte[] sendBuf;
        sendBuf = sendStr.getBytes();
        InetAddress addr = InetAddress.getByName("127.0.0.1");
        int port = 5050;
        DatagramPacket sendPacket 
            = new DatagramPacket(sendBuf ,sendBuf.length , addr , port);
        client.send(sendPacket);
        byte[] recvBuf = new byte[100];
        DatagramPacket recvPacket
            = new DatagramPacket(recvBuf , recvBuf.length);
        client.receive(recvPacket);
        String recvStr = new String(recvPacket.getData() , 0 ,recvPacket.getLength());
        System.out.println("收到:" + recvStr);
        client.close();
    }
}
```

from [http://blog.csdn.net/wintys/article/details/3525643/][1]

## MulticastSocket

DatagramSocket只允许数据发送给指定的目标地址，而MulticastSocket可以将数据报以广播方式发送到多个客户端。

# TCP/UDP比较

tcp协议：可靠，传输大小无限制，但是需要建立连接，差错控制开销大。

udp协议：不可靠，传输大小限制在64KB以下，不需要建立连接，差错控制开销小。

[1]:http://blog.csdn.net/wintys/article/details/3525643/

# Q & A

## TCP与Http协议的区别

![image](https://oscimg.oschina.net/oscnet/5858a792ec3ba4e2077737a712de9f12877.jpg)