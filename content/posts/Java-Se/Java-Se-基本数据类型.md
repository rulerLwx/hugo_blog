---
title: "Java-Se-基本数据类型"
date: 2017-07-01T10:18:47+08:00
draft: true
tags: [""]
categories: ["Java-Se"]
---

# 基本数据类型

## 二进制

8位二进制码有2^8=256个状态.如果用来表示无符号数,就可以表示0~255恰好256个二进制数；

而如果表示带符号数,则最高位就是符号位,0表示"+",1表示"-"


## 八大数据类型

- 如何表示八/十六进制：八进制以0开头，十六进制以0x开头


# 运算

## 算术运算符

- ++
    - i++
    - ++i



## 位运算

- &，按位与，当两位同时为1才返回1。
- |，按位或，只要有一位是1即返回1。
- ~，按位非，单目运算符，将操作数的每个位（包括符号位）取反。
- ^，按位异或，两位相同时返回0，不同时返回1。
- `<<`，左移，右边空出来的以0填充。
- `>>`，右移，符号位跟原来一致。（如果原来是正数，则以0填充；如果原来是负数，则以1填充）
- `>>>`，无符号右移，左边总以0填充。（如果原来是负数，则`>>>`后可能会变得很大，如`-5>>>2`得到`1073741822`）

## 逻辑运算符

- &&：与，前后两个操作数必须都是true才返回true，否则返回false
- &：使用与 && 相同，但不会短路
- ||：或，只要有一个操作数中有一个true，就返回true，否则返回false
- |：使用与 || 相同，但不会短路
- !：非
- ^：异或，当两个操作数不同是返回true，如果两个操作数相同返回false

## %运算

```
0%4=0
1%4=1
2%4=2    取余运算不能约分后再除
3%4=3
4%4=0
5%4=1
```
两个数是整数，第一个数小于第二个数时，直接不除，结果就是第一个数

## 三目运算

三目运算符只有一个：`?:` 

示例：true ? 1:2

## -=  +=  *=  /=

不单有-=  +=

还有 *=  /=

```
public class MyClass {
     public int constInt = 5;
     public void method() {
          constInt *=  5;
     }
}
```


https://www.nowcoder.com/test/question/done?tid=29118956&qid=14090#summary


# Math

## Math.floor/cell/round(x)

问题：Math.floor(-8.5)=( )

答：-9.0

Math.floor(x) 返回小于等于x的最接近整数，类型为double

```tex
floor: 求小于等于参数的最大整数。返回double类型-----n. 地板，地面
         例如：Math.floor(-4.2) = -5.0
-----------------------------------------------------------
ceil:   求大于等于参数的最小整数。返回double类型-----vt. 装天花板；
         例如：Math.ceil(5.6) = 6.0
-----------------------------------------------------------
round: 对小数进行四舍五入后的结果。返回int类型
         例如：Math.round(-4.6) = -5
         Math.round(-4.5) = -4
         
         总结：round函数是取最接近整数，如果遇到一样近，则取最大值。
```

参考：https://www.nowcoder.com/questionTerminal/6a7a47502af94cb98d4e005ddaf35f33?source=relative


# 自动类型转换

![image](https://oscimg.oschina.net/oscnet/cb2c782553087d3e0829e7b526177695c7e.jpg)

注意：类型自动转换，指的是在运算时，而不是赋值时，如：`String str = 'c';`报错，其它同理。—— 20191104


# Q & A

## java 中怎样将 bytes 转换为 long 类型??

参考 [java 中怎样将 bytes 转换为 long 类型][1]


## 在没有使用临时变量的情况如何交换两个整数变量的值??

```
网络上的人们给出了两种方法：

 一、异或法

        a=a^b;
         
        b=b^a;
         
        a=a^b;

 二、加减法

        a = a + b; 
         
        b = a - b; 
         
        a = a - b;
```

## 什么是自动装箱、拆箱

自动装箱：将一个基本类型变量直接赋给对应的包装类变量，或赋给Object变量，如`Integer inObj = 1;`.

自动拆箱：将一个包装类对象直接赋值给一个对应的基本类型变量，如`int i = new Integer();`.

参考：[深入剖析Java中的装箱和拆箱][4]

- Float型数据32位，与int相同，但float的范围 远比int大，为什么？

 [https://zhidao.baidu.com/question/228609328.html][2]

 [https://www.cnblogs.com/findumars/p/5452526.html][3] 注意第三点

**注意：浮点数基于二进制存储。即，XX进制先转换成二进制，然后才分 符号位 指数位 有效数位（尾数位）**

[1]:https://zhidao.baidu.com/question/563107903798073804.html
[2]:https://zhidao.baidu.com/question/228609328.html
[3]:https://www.cnblogs.com/findumars/p/5452526.html
[4]:https://www.cnblogs.com/dolphin0520/p/3780005.html