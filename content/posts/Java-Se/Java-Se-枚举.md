
---
title: "Java-Se-枚举"
date: 2017-07-01T11:11:47+08:00
draft: true
tags: [""]
categories: ["Java-Se"]
---

# 实例

实例必须定义在第一行，且它默认的省略了的修饰都是 `public static final T`

# 构造函数

分无参、有参构造函数，且修饰符只能是private（可省略）。

若枚举实例中没有参数，则调用无参的构造函数；若枚举实例中有参数，则调用有参的构造函数。

```java
public enum Gender
{
	// 此处的枚举值必须调用对应构造器来创建
	MALE("男"),FEMALE("女");
	private final String name;
	// 枚举类的构造器只能使用private修饰
	private Gender(String name)
	{
		this.name = name;
	}
	public String getName()
	{
		return this.name;
	}
}
```
参考：《疯狂JAVA讲义（第三版）》P224

# 实现接口的枚举类

```java
//情况一、情况二都用到的接口
public interface MyEnumInterface {
    void foo();
    boolean too();
}
```

分两种情况：

情况一：实现接口，并在枚举类中实现接口中的方法（作为枚举类的实例成员）

```java
public enum MyEnum implements MyEnumInterface{
    SPRING,MYBATIS;
    public void foo() {

    }
    public boolean too() {
        return false;
    }
}
```

情况二：实现接口，并在各个实例中实现接口中的方法（各个实例可对接口的方法有不同的实现）。

```java
public enum MyEnum implements MyEnumInterface{
    SPRING{
        public void foo() {

        }
        public boolean too() {
            return false;
        }
    },
    MYBATIS{
        public void foo() {

        }
        public boolean too() {
            return false;
        }
    };
}
```

# 有抽象方法的枚举类

步骤一：在枚举类中定义抽象方法；

步骤二：在各个实例中实现抽象方法；

```java
public enum MyEnumClass {
    SPRING{
        @Override
        public double eval(double x, double y) {
            return 0;//各自有不同的实现
        }

    },STRUTS2{
        @Override
        public double eval(double x, double y) {
            return 0;//各自有不同的实现
        }
    };
    public abstract double eval(double x,double y);
}
```

# 总结

有接口或抽象方法的枚举类都是匿名内部类的实例（有多少个枚举实例，就创建多少个匿名内部类实例），而不是枚举类的实例。