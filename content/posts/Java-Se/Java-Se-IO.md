---
title: "Java-Se-IO"
date: 2017-07-01T12:16:47+08:00
draft: true
tags: [""]
categories: ["Java-Se"]
---


# 输入 & 输出 体系

- 分类：

    - 按流的方向分：输入流、输出流
    - 按字节和字符分：字节流、字符流
    - 按流的角色分：节点流（低级流）、处理流（高级流）

分类|字节输入流|字节输出流|字符输入流|字符输出流
---|---|---|---|---
抽象基类|*InputStream*|*OutputStream*|*Reader*|*Writer*
访问文件|**FileInputStream**|**FileOutStream**|**FileRead**|**FileWriter**
访问数组|**ByteArrayInputStream**|**ByteArrayOutputStream**|**CharArrayReader**|**CharArrayWriter**
访问管道|**PipedInputStream**|**PipedOutputStream**|**PipedReader**|**PipedWriter**
访问字符串|||**StringReader**|**StringWriter**
缓冲流|BufferedInputStream|BufferedOutputStream|BufferedReader|BufferedWriter
转换流|||InputStreamReader|OutputStramWriter
对象流|ObjectInputStream|ObjectOutputStream||

*注：粗体标出的类代表节点流，必须直接与指定物理节点关联；斜体标出的类代表抽象基类，无法创建实例*



# 文件读入、写出

使得JDK7的Files

![](https://oscimg.oschina.net/oscnet/8be07c83ec50282c135280da31bf032cb3d.png)


# Files遍历文件、目录

```java
public class FileVisitorTest
{
	public static void main(String[] args)
		throws Exception
	{
		// 遍历g:\publish\codes\15目录下的所有文件和子目录
		Files.walkFileTree(Paths.get("g:", "publish" , "codes" , "15")
			, new SimpleFileVisitor<Path>()
		{
			// 访问文件时候触发该方法
			@Override
			public FileVisitResult visitFile(Path file
				, BasicFileAttributes attrs) throws IOException
			{
				System.out.println("正在访问" + file + "文件");
				// 找到了FileInputStreamTest.java文件
				if (file.endsWith("FileInputStreamTest.java"))
				{
					System.out.println("--已经找到目标文件--");
					return FileVisitResult.TERMINATE;
				}
				return FileVisitResult.CONTINUE;
			}
			// 开始访问目录时触发该方法
			@Override
			public FileVisitResult preVisitDirectory(Path dir
				, BasicFileAttributes attrs) throws IOException
			{
				System.out.println("正在访问：" + dir + " 路径");
				return FileVisitResult.CONTINUE;
			}
		});
	}
}

```


# File

File即可表示文件，也可以表示目录，看具体情况而定。

- boolean isFile()，判断是否是文件
- boolean isDirectory()，判断是否是文件夹

## File类的两个常量

- File.separator，windows下是 \ 
- File.pathSeparator，windows下是 ；

## File的路径问题

File的相对路径：相对于当前项目的根路径，`File file = new File("pom.xml");`，可以拿到**项目根路径**下的pom.xml文件



# 附件

## 递归删除文件及文件夹
```java
    public void deleteAll(File file) {
        if(file.isFile() || file.list().length == 0)
        {
            file.delete();
        }
        else
        {
            File[] files = file.listFiles();
            for(File f : files)
            {
                deleteAll(f);//递归删除每一个文件
                f.delete();//删除该文件夹
            }
        }
    }
```

建议使用commons-io的FileUtils.deleteDirectory(File dir)方法。或使用jdk7的FileVisitor来实现递归删除。

## IntelliJ中获取类路径下的文件
```
    + src /
    |   + main/
    |   |   + java/
    |   |   |    + com.demo/
    |   |   |    |   + TraditionIODemo.java
    |   |   + resource/
    |   |   |   + com.demo/ 
    |   |   |   |   + c.txt 
```

```java
    public static void main(String[] args) throws Exception {
        InputStream is = TraditionIODemo.class.getResourceAsStream("c.txt");
        System.out.println(is);
    }
```
要把resource标记为`Resources`，这样，因为包名相同，c.txt和TraditionIODemo.class文件就会放在同一个目录下。

## 加载properties文件

FileUtilsTest.class.getClassLoader().getResourceAsStream 默认加载的路径是编译后class目录（不包括子目录）的文件
若要加载子目录中的文件，则要写上相对于class目录的路径。

```java
    /**
     * FileUtilsTest.class.getClassLoader().getResourceAsStream 默认加载的路径是编译
     * 后class目录（不包括子目录）的文件
     */
    @Test
    public void testLoadProperties(){
        try {
            InputStream is = FileUtilsTest.class.getClassLoader().getResourceAsStream("project.properties");

            Properties properties = new Properties();
            properties.load(is);

            String pass = properties.getProperty("user.model.pass");
            System.out.println("user.model.pass:"+pass);

            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
```

## 一个英文和汉字分别占几个字节

```
一个英文字母是一个字节
中国字比较复杂,1字节=8位,8位从0000 0000到1111 1111只能表示256个字符 (2^8),2个字节是2*8=16位,从0000 0000 0000 0000到1111 1111 1111 1111可以表示65535个字符 (2^16).
6万多个足以表示我们经常用的那些字了.所以我们用2字节表示汉字.
如果用3个字节有很多不常用的字又占了很多不必要的存储空间,所以我们用2个字节而不是1个或者3个表示汉字的国际码 (2^24).

```

参考：[一个英文和汉字分别占几个字节][1]


[1]:https://www.zybang.com/question/05e64717f8964188b2113bcd52e44771.html