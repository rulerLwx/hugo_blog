---
title: "Java-Se-多线程-2-高级"
date: 2017-07-01T10:14:47+08:00
draft: true
tags: ["多线程"]
categories: ["Java-Se"]
---

# CAS 

## CAS 是什么

compareAndSet()方法，也就是 compareAndSwap ，比较并且交换。

使用示例：
```
    AtomicInteger atomicInteger = new AtomicInteger(5);
    //当前值是5，如果主内存中的值还是5则设置当前值为2019
    System.out.println(atomicInteger.compareAndSet(5, 2019)+" current value:"+ atomicInteger.get());
    
    atomicInteger.getAndIncrement();//代替 i++
```

## 底层原理

Unsafe 类 是 CAS 的核心类，由于Java方法无法直接访问底层系统，需要通过本地（native）方法来访问，Unsafe相当于一个后门，基于该类可以直接操作特定内存的数据。Unsafe类存在于sun.misc包中，其内部方法操作可以像C的指针一样直接操作内存，因为Java中CAS操作的执行依赖于Unsafe类的方法。**注意Unsafe类中的所有方法都是native修饰的，也就是说Unsafe类中的方法都直接调用操作系统底层资源执行相应任务**。

变量valueOffset，**表示该变量在内存中的偏移地址**（类似于坐位的第三排第2位的说法），因为Unsafe就是根据内存偏移地址获取数据的。

变量value用volatile修饰，保证了多线程之间的内存可见性。

AtomicInteger：
```java
    /**
     * Atomically increments by one the current value.
     *
     * @return the previous value
     */
    public final int getAndIncrement() {
        return unsafe.getAndAddInt(this, valueOffset, 1);
    }
```

Unsafe：
```
    public final int getAndAddInt(Object var1, long var2, int var4) {
        int var5;
        do {
            var5 = this.getIntVolatile(var1, var2);// 底层代码，C 语言
        } while(!this.compareAndSwapInt(var1, var2, var5, var5 + var4));// 底层代码，C 语言

        return var5;
    }
```

以上方法是交换的核心方法。

## 缺点

- 循环时间长时开销大
- 只能保证一个共享变量的原子操作
- 引出ABA问题？？？

## ABA问题

A是头尾的值，只要头尾值相同就交换。但在交换中间的时间里，有可能被其它的线程修改过A的值，只不过最后的值又恰巧修改回了A。

解决办法：使用原子引用，常用API：`AtomicReference`、`AtomicStampedReference`

原理：原子引用 + 版本号"stamp"

以下是ABA问题的出现与解决：

```java
public class ABADemo {

    static AtomicReference<Integer> atomicReference = new AtomicReference<Integer>(100);
    // AtomicStampedReference(V initialRef, int initialStamp)，initialRef 初始值，initialStamp 版本或时间戳
    static AtomicStampedReference<Integer> atomicStampedReference = new AtomicStampedReference<>(100, 1);

    public static void main(String[] args) {
        System.out.println("--------------以下是ABA问题的产生----------------");
        new Thread(()->{
            atomicReference.compareAndSet(100, 101);
            atomicReference.compareAndSet(101, 100);
        },"t1").start();
        new Thread(()->{
            // 暂停1秒仲，目的是让t1执行一次ABA操作
            try {TimeUnit.SECONDS.sleep(1);} catch (InterruptedException e) {e.printStackTrace();}
            boolean b = atomicReference.compareAndSet(100, 2019);
            System.out.println(b);
        },"t2").start();

        // 暂停2秒仲，目的是让上面的ABA问题操作执行完成
        try {TimeUnit.SECONDS.sleep(1);} catch (InterruptedException e) {e.printStackTrace();}

        System.out.println("--------------以下是ABA问题的解决----------------");

        new Thread(()->{
            int stamp = atomicStampedReference.getStamp();
            System.out.println(Thread.currentThread().getName()+"\t 第1次版本号："+stamp);
            // 暂停1秒仲t3线程
            try {TimeUnit.SECONDS.sleep(1);} catch (InterruptedException e) {e.printStackTrace();}
            atomicStampedReference.compareAndSet(100, 101,
                    atomicStampedReference.getStamp(), atomicStampedReference.getStamp() + 1);
            System.out.println(Thread.currentThread().getName()+"\t 第2次版本号："+atomicStampedReference.getStamp());
            atomicStampedReference.compareAndSet(101, 100,
                    atomicStampedReference.getStamp(), atomicStampedReference.getStamp() + 1);
            System.out.println(Thread.currentThread().getName()+"\t 第3次版本号："+atomicStampedReference.getStamp());
        },"t3").start();
        new Thread(()->{
            int stamp = atomicStampedReference.getStamp();
            System.out.println(Thread.currentThread().getName()+"\t 第1次版本号："+stamp);
            // 暂停3秒仲t4线程，目的是让t3执行完成
            try {TimeUnit.SECONDS.sleep(3);} catch (InterruptedException e) {e.printStackTrace();}
            boolean b = atomicStampedReference.compareAndSet(100, 2019,
                    stamp, stamp + 1);
            System.out.println(Thread.currentThread().getName()+"\t修改结果："+ b
                    +"\t当前的实际版本号："+atomicStampedReference.getStamp());
        },"t4").start();
    }
}
```

输出：

```
--------------以下是ABA问题的产生----------------
true
--------------以下是ABA问题的解决----------------
t3	 第1次版本号：1
t4	 第1次版本号：1
t3	 第2次版本号：2
t3	 第3次版本号：3
t4	修改结果：false	当前的实际版本号：3
```

# 集合类不安全的问题

## 为什么集合不安全

ArrayList 默认大小：10
HashMap 默认大小：16

为什么ArrayList线程不安全？

因为它的写方法没有加synchronized，ArrayList源码：

```java
    /**
     * Appends the specified element to the end of this list.
     *
     * @param e element to be appended to this list
     * @return <tt>true</tt> (as specified by {@link Collection#add})
     */
    public boolean add(E e) {
        ensureCapacityInternal(size + 1);  // Increments modCount!!
        elementData[size++] = e;
        return true;
    }
```

手写不安全示例：

```java
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,8));
                System.out.println(list);
            },String.valueOf(i)).start();
        }
    }
```

输出是随机的，以下是其中的两次输出：

```
[null, null, 3e48b54b]
[null, null, 3e48b54b]
[null, null, 3e48b54b]

[null, c1bbced9]
[null, c1bbced9, 7da1e5a3]
[null, c1bbced9]
```

线程多时，可能还出现`java.util.ConcurrentModificationException`异常

## 导致的原因

一个人在写，另一个人过来抢夺，导致数据不一致。


## 解决方案

- `new Vector<>();`使用线程安全的 Vector 替换 ArrayList
- `Collections.synchronizedList(new ArrayList<>());` 将 ArrayList 包装成线程安全的
- `new CopyOnWriteArrayList<>()`

---

CopyOnWriteArrayList 是“写时复制”，读写分离思想:

![](https://gitee.com/leafsing/pic/raw/master/img/20200709233122.png)

CopyOnWriteArrayList源码：

```java
    /**
     * Appends the specified element to the end of this list.
     *
     * @param e element to be appended to this list
     * @return {@code true} (as specified by {@link Collection#add})
     */
    public boolean add(E e) {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            Object[] elements = getArray();
            int len = elements.length;
            Object[] newElements = Arrays.copyOf(elements, len + 1);// 扩容1个元素
            newElements[len] = e;
            setArray(newElements);
            return true;
        } finally {
            lock.unlock();
        }
    }
```

## 优化建议

使用“写时复制”，如：
- `CopyOnWriteArrayList`替换`ArrayList`
- `CopyOnWriteArraySet`替换`ArraySet`
- `ConcurrentHashMap` 替换`HashMap`
- `ConcurrentSkipListMap`替换`TreeMap`

# synchronized和lock有什么区别

- synchronized属于JVM层面，是java的关键字；lock是具体类，是api层面
- synchronized不需要手动释放锁，lock需要手动释放锁
- synchronized不可中断，除非抛出异常或正常运行完成；lock可以中断，方法一：设置超时时间`tryLock(long timeout, TimeUnit unit)`，方法二：`lockInterruptibly()`放代码块中，调用`interrupt()`方法可中断
- synchronized非公平锁；ReentrantLock两者都可以，默认是非公平锁，构造方法可以传入boolean值，true为公平锁，false为非公平锁
- ReentrantLock可以实现精确唤醒，而synchronized要么随机唤醒一个或全部唤醒
- synchronized 锁住的是 this 或 某个字段，lock 锁住的是它内部的一个对象？？？？——20191024 https://www.iteye.com/problems/95328


> 面试题目<br><br>
> 多线程之间按顺序调用，实现A->B-C三个线程启动，要求如下：<br>
> AA打印5次，BB打印10次，CC打印15次，<br>
> 紧接着，<br>
> AA打印5次，BB打印10次，CC打印15次，<br>
> ...<br>
> 
> 来10轮


代码实现：

```java
class ShareResource{
    private int number = 1;// A:1,B:2,C:3
    private Lock lock = new ReentrantLock();
    private Condition c1 = lock.newCondition();
    private Condition c2 = lock.newCondition();
    private Condition c3 = lock.newCondition();

    void print5(){
        lock.lock();
        try {
            // 1 判断
            while (number != 1) {
                c1.await();
            }
            // 2 干活
            for (int i = 1; i <= 5; i++) {
                System.out.println(Thread.currentThread().getName()+"\t"+i);
            }
            // 3 通知
            number = 2;
            c2.signal();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }
    void print10(){
        lock.lock();
        try {
            // 1 判断
            while (number != 2) {
                c2.await();
            }
            // 2 干活
            for (int i = 1; i <= 10; i++) {
                System.out.println(Thread.currentThread().getName()+"\t"+i);
            }
            // 3 通知
            number = 3;
            c3.signal();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }
    void print15(){
        lock.lock();
        try {
            // 1 判断
            while (number != 3) {
                c3.await();
            }
            // 2 干活
            for (int i = 1; i <= 15; i++) {
                System.out.println(Thread.currentThread().getName()+"\t"+i);
            }
            // 3 通知
            number = 1;
            c1.signal();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }
}
public class SyncAndReentrantLockDemo {
    public static void main(String[] args) {
        ShareResource shareResource = new ShareResource();
        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                shareResource.print5();
            }
        },"A").start();
        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                shareResource.print10();
            }
        },"B").start();
        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                shareResource.print15();
            }
        },"C").start();
    }
}
```
锁绑定多个Condition


# 公平锁、非公平锁、乐观锁、悲观锁

- 公平锁：加锁前先查看是否有**排队**等待的线程，有的话优先处理排在前面的线程，**先来先得**
- 非公平锁：线程加锁时直接尝试获取锁，获取不到就自动到队尾等待
- 乐观锁
- 悲观锁

总结：
- 更多的是直接使用非公平锁：非公平锁比公平锁性能高5-10倍，因为公平锁需要在多核情况下维护一个队列，如果当前线程不是队列的第一个无法获取锁，增加了线程切换次数 ——20191111

参考：https://www.cnblogs.com/darrenqiao/p/9211178.html


# 多线程下的单例

单线程下的懒汉模式：
```java
public class SingleInstanceTest {
    private SingleInstanceTest(){
        System.out.println("这是SingleInstanceTest()");
    }
    public static SingleInstanceTest instance;

    public static SingleInstanceTest getInstance() {
        if (instance == null) {
            instance = new SingleInstanceTest();
        }
        return instance;
    }
}
```

解决办法：
- 获取实例的`getInstance()`方法使用`synchronized`修饰，不推荐
- 双重锁方式（DCL，double check lock）并使用`volatile`关键字，推荐

```java
public class DoubleCheckLockSingleInstanceTest {
    private volatile static DoubleCheckLockSingleInstanceTest instance;

    private DoubleCheckLockSingleInstanceTest() {
        System.out.println("这是DoubleCheckLockSingleInstanceTest()");
    }

    public static DoubleCheckLockSingleInstanceTest getSingleton() {
        if (instance == null) {
            synchronized (DoubleCheckLockSingleInstanceTest.class) {
                if (instance == null) {
                    instance = new DoubleCheckLockSingleInstanceTest();
                }
            }
        }
        return instance;
    }
}
```
另外，可参考笔记《Java-Senior-设计模式》

**注意**：DCL（双重锁）机制不一定线程安全，原因是有指令重排序的存在，加入`volatile`可禁止指令重排。


# ThreadLocal

示例：并发下的时间格式化

```java
    private static final ThreadLocal<DateFormat> df = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };
```

参考《阿里巴巴java开发手册(最终版)》并发处理

Java并发编程——ThreadLocal：https://blog.csdn.net/ghw15221836342/article/details/100136753

# volatile

参考笔记《Java-Se-关键字》


# String & StringBuffer & StringBuilder

- String 是不可变类，StringBuffer、StringBuilder是可变
- String、StringBuffer 是线程安全的（所有不可变类都是线程安全的，线程安全的类不一定是不可变类，如StringBuffer是可变类，靠方法上都加了synchronized锁实现线程安全；String内部存储字符串的char数组以及和char数组相关的信息都是final的来实现线程安全）
