---
title: "Java-Se-异常"
date: 2017-07-01T11:16:47+08:00
draft: true
tags: [""]
categories: ["Java-Se"]
---

# 概述

异常类的继承体系：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200707201229.png)

Java异常机制主要依赖try、catch、finally、throw、throws一个关键字

Java将异常分为两种：Checked 异常和 Runtime 异常

- Checked 异常：
    - 不是 RuntimeException 类及其子类的实例被称为 Checked 异常 ——20190819《疯狂Java讲义》
    - 可以在编译阶段被处理的异常，程序员必须显式处理所有的Checked异常（否则程序不能编译通过）
    - 要么`try...catch`捕捉，要么`throws`声明异常
- Runtime 异常：
    - 所有 RuntimeException 类及其子类的实例被称为 Runtime 异常
    - 可以不使用try...catch进行处理，但是如果有异常产生，则异常将由JVM进行处理。

总结：Runtime 异常具有 Checked 异常的所有处理异常方式，也就是可以try，也可以throws，除此外，还可以不处理异常

# 两大常见Error

1. 方法区溢出：OutOfMemoryError 
2. 栈溢出：StackOverflowError


# 分类：检查异常、运行异常

参考：https://www.nowcoder.com/test/question/done?tid=26827681&qid=22460#summary