---
title: "Java-Se-反射"
date: 2017-07-01T10:15:47+08:00
draft: true
tags: [""]
categories: ["Java-Se"]
---

# Class对象

```txt
如何获取Class对象？有以下三种方式：
1）Class.forName(String clazzName)
2）类.class
3）对象.getClass()，此方法是Object类的方法
```

# 创建对象

```java
Class<?> clazz = Class.forName(clazzName);
clazz.newInstance();
```

# 调用方法

重载的方法通过方法名+参数列表来确定。反射后得到的方法封装在了Method类，其有个方法的签名如下：Object invoke(Object obj,Object ...args)，obj是执行该方法的主调，args是obj执行mtd方法时的入参。

```java
Method mtd = clazz.getMethod(“info”,String.class);
Method mtd = clazz.getMethod(“info”,String.class,Integer.class);
mtd.invoke(对象,args）;
```

# 访问成员变量

通过Class对象的getFields()或getFile()方法可以获取类中的全部成员变量或指定成员变量（不管是private还是public，都可以获取）。

```txt
读取或设置成员变量值有如下方法：
getXxx(Object obj)：此处的Xxx对应8各基本类型，如果是引用类型，则去掉Xxx。
setXxx(Object obj)：此处的Xxx对应8各基本类型，如果是引用类型，则去掉Xxx。

例：f.get(test.newInstance())，注意：这里传对象实例。
```

```java
Field nameField = class.getDeclaredField(“name”);
nameField.setAccessible(true);
nameField.set(对象,”abc”);
```

# 操作数组

程序可以通过Array类的对象来动态创建数组。Array类有如下方法签名：

```txt
static Object newInstance(Class<?>componentType,int...length)：创建一个指定元素类型、维度的数组
static xxx getXxx(Object array,int index)：Xxx对应8各基本类型，如果是引用类型，则去掉Xxx。
static xxx setXxx(Object array,int index,xxx val)：Xxx对应8各基本类型，如果是引用类型，则去掉Xxx。
```

```java
Object arr = Array.newInstance(String.class,10);
Array.set(arr,0,”a”);
Array.set(arr,1,”b”);
Array.set(arr,2,”c”);
```

# 查看泛型

```java
声明一个成员变量：
public ArrayList<String> list;

Field f = clazz.getDeclaredField("list");
Class<?> type = f.getType();	//只得到成员变量的类型，java.util.ArrayList

Type gtype = f.getGenericType();
ParameterizedType ptype = (ParameterizedType) gtype;	//强转为ParameterizedType
Type[] targs = ptype.getActualTypeArguments();
for(Type t:targs){
	System.out.println(t);	//得到泛型，java.lang.String
}
```

# 解析注解

只能解析作用范围是RetentionPolicy.RUNTIME的注解。

## 解析类上的注解

```java
Class c = 类.class;
if(c.isAnnotationPresent(注解类.class)){
    注解类 注解类对象 = (注解类) c.getAnnotation(注解类.class);
    System.out.println(注解类对象.desc());
    System.out.println(注解类对象.author());
    System.out.println(注解类对象.age());
}
```

## 解析方法上的注解

```java
Class c = 类.class;
Method[] ms = c.getMethods();
for (Method method : ms) {
    if(method.isAnnotationPresent(注解类.class)){
        注解类 注解类对象 = (注解类)method.getAnnotation(注解类.class);
        System.out.println(注解类对象.desc());
        System.out.println(注解类对象.author());
        System.out.println(注解类对象.age());
    }
}
```

## 解析成员变量上的注解

```java
Class c = Test1.class;  
Field[] fields = c.getDeclaredFields();
for(Field f:fields){
    if(f.isAnnotationPresent(注解类.class)){
        Annotation[] arr = f.getAnnotations();
        //...
    }
}
```


# 实体非空判断

```
for (Field f : obj.getClass().getDeclaredFields()) {
    f.setAccessible(true);
    if (f.get(obj) == null) { //判断字段是否为空，并且对象属性中的基本都会转为对象类型来判断
        ......
    }
}
```