---
title: "Java-EE-Servlet"
date: 2017-07-01T10:11:47+08:00
draft: true
tags: [ ]
categories: ["Java-Se"]
---


# JSP

JSP的本质是Servlet，JSP页面由系统编译成Servlet，Servlet再负责响应用户请求。

## 语法

1、注释

<%--注释内容 --%>

2、声明

<%! 声明部分 %>

```jsp
</head>
<!-- 下面是JSP声明部分 -->
<%!
    //声明一个整形变量
    public int count;

    //声明一个方法
    public String info() {
        return "hello";
    }
%>
<body>
<%
    //将count的值输出后再加1
    out.println(count++);
%>
<br/>
<%
    //输出info()方法的返回值
    out.println(info());
%>
</body>
```

3、输出

<%=表达式 %>

4、脚本

<%  脚本 %>

```
<table bgcolor="#9999dd" border="1" width="300px">
    <!-- Java脚本，这些脚本会对HTML的标签产生作用 -->
    <%
        for (int i = 0; i < 10; i++) {
    %>
    <!-- 上面的循环将控制<tr>标签循环 -->
    <tr>
        <td>循环值:</td>
        <td><%=i%></td>
    </tr>
    <%
        }
    %>
</table>
```

以上四点都是在jsp页面中写，会将五jsp中的java代码和html标签编译成一个servlet。

## 编译指令

- page：针对当前页面的指令。
- include：包含另一个页面。
- taglib：定义和访问自定义标签。

语法：<%@编译指令 属性名=”属性值” ....%>

page指令的属性：

![image](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200707160349.png)

taglib指令：`<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> `

include指令：`<%@ include file=”relativeURLSpec”%>`

这个是静态include，**它会把目标页面的其它编译指令也包含进来，但动态include则不会**。

特点：包含页面在编译时将完全包含了被包含的页面的代码，如果两个页面的编译指令冲突，则页面报错。

注：动态include，使用<jsp:include>

## 7个动作指令

- jsp:forward  执行页面转向，将请求的处理转发到下一个页面。
- jsp:param  用于传递参数，必须与其他支持参数的标签一起使用

```
<%@ page contentType="text/html; charset=GBK" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title> forward的原始页 </title>
	<meta name="website" content="http://www.crazyit.org" />
</head>
<body>
<h3>forward的原始页</h3>
<jsp:forward page="forward-result.jsp">
	<jsp:param name="age" value="29"/>
</jsp:forward>
</body>
</html>
```

- jsp:include  用于动态引入一个JSP页面

    它不会导入被include页面的编译指令，仅仅将**被导入页面的body内容**插入本页面。

```
<body>
<jsp:include page="forward-result.jsp" >
	<jsp:param name="age" value="32"/>
</jsp:include>
</body>
```

- jsp:plugin 用于下载JavaBean或者Applet到客户端执行
- jsp:useBean 创建一个Javabean实例
- jsp:setProperty  设置JavaBean实例的属性值
- jsp:getProperty  获取JavaBean实例的属性值

## 9个内置对象

request、response、session、application、out、pagecontext、config、page、exception。

![image](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200707160459.png)

pageContext可以访问page、request、session、application范围的变量。

## EL表达式

语法：${expression}

JSP2才开始默认支持EL表达式。

（如果web.xml中使用2.3及以下dtd，则需在使用el表达式的页面中增加`<%@page isELIgnored="false" %>`配置，el表达式才会生效。如果web.xml中使用2.4及以上dtd，则默认isELIgnored="false"）


### 11个内置对象

- pageContext，与JSP的pageContext内置对象相同；
- pageScope，page范围内的属性值；
- requestScope，request范围内的属性值；
- sessionScope，session范围内的属性值；
- applicationScope，application范围的属性值；
- param，请求参数
- paramValues，以数组形式表示的请求参数；
- header，获取请求头；
- headerValues，获取以数组形式表示的请求头；
- initParam，获取Web应用初始化参数；
- cookie，获取Cookie值。



### 使用示例

1、依赖的包

```
<dependency>
	<groupId>jstl</groupId>
	<artifactId>jstl</artifactId>
	<version>1.2</version>
</dependency>
<dependency>
	<groupId>taglibs</groupId>
	<artifactId>standard</artifactId>
	<version>1.1.2</version>
</dependency>
```

2、jsp页面引入 c标签

`<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>`

在jsp中拿到绝对路径：

```
<form action="${pageContext.request.contextPath }/emp">

</form>
```
在开发中推荐使用绝对路径


# Servlet

Servlet是个特殊的Java类，必须继承HttpServlet。

```
public class MyServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
    }
    
}
```

Servlet中没有内置对象。


## 入门Demo

servlet3.0开始，servlet的配置有两种方式：

- web.xml。
- 在servlet类中使用@WebServlet注解。



### web.xml方式

1）创建Maven工程，导入依赖；

2）创建的Servlet，继承HttpServlet，重写doGet/doPost方法；

3）在web.xml配置servlet和servlet-mapping，其中`<url-pattern>/servletTest</url-pattern>`要有`/`

4）将工程发布到Tomcat测试访问



### 零配置方式


1）创建Maven工程，导入依赖；

2）创建的Servlet，继承HttpServlet，重写doGet/doPost方法；

3）用@WebServlet注解此Servlet类，`@WebServlet(name = "testServlet",urlPatterns = {"/testServlet"})`；

4）将工程发布到Tomcat测试访问


@WebServlet支持的常用属性：

![image](E:\backup\hogo_blog\content\posts\Java-Se\1CD4254B5F8048B0B830065C5C82E9AD)

loadOnStartup属性只接收一个整型值，值越小，优先级越高。



注意：Tomcat容器自动加载有@WebServlet注解的类

容器是如何加载@WebServlet类的？如何发现servlet的？？TODO....



### 两种方式比较

使用的不同之处是第三步，其它的相同。

一个工程中两种方式同时使用时，Servlet的name属性不能相同；若相同，web.xml中的优先，零配置的访问不到。



## Servlet生命周期

servelt的创建、销毁都不是由程序员来决定的，而是由Web容器进行控制。

创建servlet实例有两个时机：

- 客户端第一次访问某个servlet时
- web容器启动时，需要配置load-on-startup属性

servlet生命周期：

1、创建servlet实例。

2、调用servlet的init方法。

3、servlet初始化后，将一直存在容器中，用于响应客户端请求。一般处理的是GET/POST请求，分别调用doGet/doPost方法响应请求。

4、容器决定销毁servlet时，先调用servlet的destroy方法。

注意：在整个Servlet的生命周期过程中，创建Servlet实例、调用实例的init()和destroy()方法都只进行一次，当初始化完成后，Servlet容器会将该实例保存在内存中，通过调用它的service()方法，为接收到的请求服务。换句话说，**容器中的Servlet是单例的**。


## 访问servlet配置参数

容器在初始化servlet时，将调用servlet的`init(ServletConfig config)`方法，config就封装好了servlet的初始化参数，通过它就可以访问配置参数了。

`config.getInitParameter("")`;


## Filter

filter的流程：对用户请求进行预处理，然后将请求交给servlet进行处理并生成响应，最后再对服务响应进行后处理。

filter的使用：

1、创建filter实现类

必须实现javax.servlet.Filter接口，此接口有如下三个方法：

- void init(FilterConfig config)，config是Filter的配置参数。
- void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)：在chain.doFilter(req,resp)之前的是预处理，之后的代码是后处理。如果不调用chain.doFilter(req,resp)方法，则此此用户请求不会交给servlet处理。
- void destroy()

2、配置filter

- web.xml配置方式
- @WebFilter注解方式

@WebFilter支持的常用属性

![image](E:\backup\hogo_blog\content\posts\Java-Se\194B76B103724412A5B854C74D43919E)

filter跟servlet具有相同的生命周期行为。



## Listener

用于监听web应用的内部事件，如web应用的启动、停止，用户session的开始、结束，用户请求到过等。

不同的监听器接口具有不同的作用：

- ServletContextListener，监听Web应用的启动和关闭。
- ServletContextAttributeListener，监听ServletContext范围（application）内属性的改变。
- ServletRequestListener，监听用户请求。
- ServletRequestAttributeListener，监听ServletRequest范围（request）内属性的改变。
- HttpSessionListener，监听Session的开始、结束。
- HttpSessionActivationListener，监听HttpSession范围（session）内属性的改变。


listener的使用：

1、创建listener的实现类

2、@WebListener注解或web.xml方式配置listener。


# 附件

## Servlet依赖的jar包

```
3.1
<dependency>
	<groupId>javax.servlet</groupId>
	<artifactId>javax.servlet-api</artifactId>
	<version>3.1.0</version>
</dependency>

2.4
<dependency>
	<groupId>javax.servlet</groupId>
	<artifactId>servlet-api</artifactId>
	<version>2.4</version>
	<scope>provided</scope>
</dependency>
```

## EL表达式依赖的jar包

```
<dependency>
	<groupId>jstl</groupId>
	<artifactId>jstl</artifactId>
	<version>1.2</version>
</dependency>
<dependency>
	<groupId>taglibs</groupId>
	<artifactId>standard</artifactId>
	<version>1.1.2</version>
</dependency>
```

据说 jstl 1.2 不再需要 standard 这个包了：https://blog.csdn.net/z69183787/article/details/38066239



## web.xml配置头

```
3.1
<?xml version="1.0" encoding="GBK"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee
	http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
	version="3.1">

2.4
<!DOCTYPE web-app PUBLIC
 "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN"
 "http://java.sun.com/dtd/web-app_2_3.dtd" >
<web-app>

2.5
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://java.sun.com/xml/ns/javaee"
           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
           xsi:schemaLocation="http://java.sun.com/xml/ns/javaee
		  http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd"
           version="2.5">

</web-app>
```




# Q & A

## attribute与parameter区别

1、request.getParameter取得Web客户端到web服务端的http请求数据(get/post)，只能是string类型的

2、request.getAttribute():当两个web组件为转发关系时，通过getAttribute()和setAttribute()来共享request范围内的数据。attrubute中的数据是Object类型的，通过attribute传递的数据只会存在于web容器内部，仅仅是请求处理阶段。

3、小结：request.getAttribute()方法返回request范围内存在的对象，request.getParameter()获取http请求提交过来的数据（字符串）。

## JSP内置对象 & EL表达式内置对象

JSP内置对象：
```
application

config
exception
out

page

pageContext

request
response
session
```

EL表达式内置对象：
```
pageContext

pageScope
requestScope
sessionScope
applicationScope

param
paramValues
header
headerValues
cookie
initParam
```

## 转发、重定向区别

转发（forward)：将请求forward到指定jsp资源

重定向（redirect）：比喻：你找我借钱，我没有，叫你去找他

区别：重定向会丢失所有的请求参数、请求属性、action的处理结果，url会改变。发送再次请求。


## 默认跳转到index.jsp

Java Web项目，启动tomcat后默认到跳转到webapp下的index.jsp，除非web.xml中配置其它欢迎页面。

## Java Web中有几个域，分别是什么，怎么用

page/request/session/application域

## <%!%>和<%%>的区别

<%!%>只是进行方法的定义和属性的定义

<%%>这个里面定义的会**转换成方法内的局部变量，或者代码**

<%!%>是用来定义成员变量属性和方法的,<%%>主要是用来输出内容的,因此如果涉及到了成员变量的操作,那么我们就应该使用<!%%>,而如果是涉及到了输出内容的时候,就使用<%%>。

https://zhidao.baidu.com/question/253471013.html