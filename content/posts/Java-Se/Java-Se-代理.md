---
title: "Java-Se-代理"
date: 2017-07-01T10:12:47+08:00
draft: true
tags: [""]
categories: ["Java-Se"]
---


# JDK代理

## 静态代理

可以理解为 1v1 关系。即一个代理类只能代理一个类（或对象）。

即：代理类中有被代理类的引用，`private IUserService uservice;`

## 动态代理

可以理解为 1v多 关系。即一个代理类（工厂类）可以代理n个类（或对象）

条件：代理类与被代理类，都要实现共同的接口

示例：

![dynamic-1](https://oscimg.oschina.net/oscnet/ff258173d0102daef86471db95316338598.jpg "dynamic-1")

```java
package com.tecsun.service;

public interface PersonService {

	String getPersonName();
}
```

```java
package com.tecsun.service.impl;

import com.tecsun.service.PersonService;

public class PersonServiceImpl implements PersonService {

	@Override
	public String getPersonName() {
		System.out.println("我是getPersonName()方法");
		return "xxxx";
	}

}
```

![dynamic-2](https://oscimg.oschina.net/oscnet/eadb67c918eb0b05ba757c719385567ee7e.jpg "dynamic-2")

原理：代理类与被代理类，都要实现共同的接口，然后在代理类中调用被代码类的方法。

![dynamic-3](https://oscimg.oschina.net/oscnet/2978bf52909b72f32ee06d7e2b86f400fbc.jpg "dynamic-3")

缺点：JDK动态代理，被代理类必须要实现有接口（一个或多个）。如果没有，则使用CGLIB代理。

---

最简写法

```java
public class JDKProxyFactory {

    public Object createProxyInstance(final Object target) {
        return Proxy.newProxyInstance(
                target.getClass().getClassLoader(),
                target.getClass().getInterfaces(),
                (proxy, method, args) -> {
                    //before
                    Object invoke = method.invoke(target, args);
                    //after
                    return invoke;
                }
        );
    }
}
```


# CGLib代理

CGLIB(Code Generation Library)是一个开源项目。请查看百度百科。

### HelloWorld

![cglib-1](https://oscimg.oschina.net/oscnet/472873393adc73ca2bcf04342e04167584c.jpg "cglib-1")

```java
public class PersonServiceImpl {
    public String getPersonName() {
        System.out.println("我是getPersonName()方法");
        return "xxxx";
    }
}
```

![cglib-2](https://oscimg.oschina.net/oscnet/734c53443e5126fcb287793ddf926245c4e.jpg "cglib-2")

代理工厂使用到Enhancer类，并设置父类、回调函数，其中回调函数是实现MethodInterceptor接口方法intercept()。

代码在：gitee/demo。

