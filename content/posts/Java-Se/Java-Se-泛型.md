---
title: "Java-Se-泛型"
date: 2017-07-01T10:16:47+08:00
draft: true
tags: [""]
categories: ["Java-Se"]
---

# 上边界 & 下边界

上边界：
```
正确：Vector<? extends Number> x = new Vector<Integer>();
错误：Vector<? extends Number> x = new Vector<String>();
```

下边界：
```
正确：Vector<? super Integer> x = new Vector<Number>();
错误：Vector<? super Integer> x = new Vector<Byte>();
```

# 泛型方法

语法：
```
    修饰符 <T,S> 返回值 方法名(形参列表){

    }
```
注：T、S等，可以是任意的字母，表示先定义以便后面引用它。此时的“返回值”也可以用T或S来代替。

代码片段：

```java
    static <T> void fromArrayToCollection(T[] a, Collection<T> c)
    {
        for (T o : a)
        {
            c.add(o);
        }
    }
```

# 泛型擦除

## 哪些泛型信息会保留到运行时

>Java 的泛型擦除是有范围的，即类定义中的泛型是不会被擦除的。---[Java 的泛型擦除和运行时泛型信息获取][1]

[1]:http://www.iteye.com/news/32333