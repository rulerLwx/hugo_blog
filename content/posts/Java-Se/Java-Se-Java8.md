---
title: "Java-Se-Java8"
date: 2017-07-01T12:10:47+08:00
draft: true
tags: [""]
categories: ["Java-Se"]
---


# Lambda 表达式（重点）

JDK8文档：https://docs.oracle.com/javase/8/docs/

## 为什么使用 Lambda

Lambda是一个匿名函数，我们可以将 lambda 表达式理解为**一段可以传递的代码**（将代码像数据一样传递）

```java
    //原来的匿名内部类
    @Test
    public void test1(){
        Comparator<Integer> comparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer x, Integer y) {
                return Integer.compare(x,y);
            }
        };
        TreeSet<Integer> ts = new TreeSet<>(comparator);
    }

    //使用 Lambda 表达式
    public void test2(){
        Comparator<Integer> comparator = (x, y) -> Integer.compare(x, y);
        TreeSet<Integer> ts = new TreeSet<>(comparator);
    }
```

PS：java8-foreach

```java
        ArrayList<Object> list = new ArrayList<>();
        list.forEach(System.out::println);
```


## 基础语法

Lambda 表达式由三部分组成：

- 形参列表：0个或多个
- 箭头：`->`
- 代码块：一条语句，或多条语句（用花括号）

示例：

```java
    public static void main(String[] args) {
        //无参、无返回值
        Runnable r1 = () -> System.out.println("");
        r1.run();

        //有一个参数、无返回值
        Consumer<String> consumer = (x) -> System.out.println(x);
        consumer.accept("这是传给x的参数");

        //若只有一个参数，括号可以不写
        Consumer<String> consume2 = x -> System.out.println(x);
        consumer.accept("一个参数");

        //两个以上参数，有返回值，Lambda 体中有多条语句
        Comparator<Integer> comparator = (x,y) -> {
            System.out.println("多参、返回值，Lambda体多条语句");
            return Integer.compare(x,y);
        };
        comparator.compare(1, 2);
    }
```

疑问：接口只能有一个抽象方法？

答：并不是所有的接口可以使用`Lambda`表达式，只**有函数式接口**才能使用`Lambda`表达式，即，有`@FunctionalInterface`修饰的接口才能使用`Lambda`，原则上接口只有一个未实现的方法。 ——20190904

参数列表类型可以不写，因为JVM可以通过上下文推断，判断数据类型

## 应用示例

1）调用`Collections.sort()`方法，通过定制排序比较两个`Employee`（先按年龄，年龄相同再按姓名），使用Lambda作为参数传递。

```java
public class MyTest {
    List<Employee> list = Arrays.asList(
            new Employee(101, "张三", 19, 3333.33),
            new Employee(102, "李四", 39, 5555.77),
            new Employee(103, "王五", 29, 9999.88),
            new Employee(104, "李二狗", 49, 3333.33),
            new Employee(105, "秦二春", 59, 9999.99)
    );

    @Test
    public void test1(){
        //使用Lambda
        Collections.sort(list,(e1,e2) -> {
            if (e1.getAge() == e2.getAge()) {
                return e1.getName().compareTo(e2.getName());
            }else {
                return Integer.compare(e1.getAge(),e2.getAge());
            }
        });

        //使用匿名内部类
        Collections.sort(list, new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                if (o1.getAge() == o2.getAge()) {
                    return o1.getName().compareTo(o2.getName());
                }else {
                    return Integer.compare(o1.getAge(),o2.getAge());
                }
            }
        });
        
        for (Employee e : list) {
            System.out.println(e);
        }
    }
}
```

## 四大内置函数式接口

函数式接口 | 参数类型 | 返回类型 | 方法 | 用途
---|---|---|---|---
Consumer<T>，消费型接口 | T | void | `void accept(T t);` | 对类型为T的对象应用操作
Supplier<T>，供给型接口 |  | T | `T get();` | 返回类型为T的对象
Function<T, R>，函数型接口 | T | R | `R apply(T t);` | 对类型为T的对象应用操作，并返回R类型的对象
Predicate<T>，断定型接口 | T | boolean | `boolean test(T t);` | 确定类型为T的对象是否满足某约束

示例：

```java
public class FunctionInterfaceTest {
    /**
     * Consumer<T>，消费型示例
     */
    @Test
    public void test1(){
        happy(10000,(x) -> System.out.println("花费了："+x+"元"));
    }

    public void happy(double money, Consumer consumer) {
        consumer.accept(money);
    }

    /**
     * Supplier<T>，供给型示例
     */
    @Test
    public void test2(){
        List<Integer> numList = getNumList(10, () -> (int)(Math.random() * 100));
        for (Integer i : numList) {
            System.out.println(i);
        }
    }

    public List<Integer> getNumList(int num, Supplier<Integer> supplier) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            Integer n = supplier.get();
            list.add(n);
        }
        return list;
    }

    /**
     * Function<T, R>，函数型接口
     */
    @Test
    public void test3(){
        String newStr = strHandler("ABC", (str) -> str.trim());
        System.out.println(newStr);
    }

    //处理字符串
    public String strHandler(String str, Function<String,String> fun){
        return fun.apply(str);
    }

    /**
     * Predicate<T>，断定型接口
     */
    @Test
    public void test(){
        List<String> list = Arrays.asList("abc", "www.baidu.com", "www.oschina.com", "ok");
        List<String> stringList = filterStr(list, (s -> s.length() > 3));//字符长度大于3
        for (String str : stringList) {
            System.out.println(str);
        }
    }

    public List<String> filterStr(List<String> list, Predicate<String> pre){
        ArrayList<String> strList = new ArrayList<>();
        for (String str : list) {
            if (pre.test(str)) {//判断
                strList.add(str);
            }
        }
        return strList;
    }
}
```

总结：核心思想是面向接口编程，可以不管具体实现，用到时再写实现。不会写的时候，就用匿名内部类代替，然后再转为lambda。——20190905

# 方法引用、构造器引用

语法格式：

种类 | 示例 | 对应的Lambda
---|---|---
引用类方法 | 类名::类方法 | (a,b,..) -> 类名.类方法(a,b,...)
引用特定对象的实例方法 | 对象::实例方法 | (a,b,..) -> 对象.实例方法(a,b,...)
引用某类对象的实例方法 | 类名::实例方法 | (a,b,..) -> a.实例方法(a,b,...)
引用构造器 | 类名::new | (a,b,..) -> new 类名(a,b,...)

引用与Lambda之间可以转换！

总结：当 Lambda 只有一行语句时，可以用`::`的写法，连入参都省了，前提是两者的**形参列表、返回值**相同。

有现成的方法，就引用；没有，就自己用 Lambda 表达式写。

1）引用类方法

```java
@FunctionalInterface
interface Converter{
	Integer convert(String from);
}

Converter converter1 = from -> Integer.valueOf(from);

Converter converter1 = Integer::valueOf;
```

2）引用特定对象的实例方法

```java
Converter converter2 = from -> "fkit.org".indexOf(from);

Converter converter2 = "fkit.org"::indexOf;

Integer value = converter2.convert("it");
```

3）引用某类对象的实例方法

```java
@FunctionalInterface
interface MyTest
{
	String test(String a , int b , int c);
}

MyTest mt = (a , b , c) -> a.substring(b , c);//第一个参数作为调用者，后面的参数全部传给该方法作为参数。

MyTest mt = String::substring;

String str = mt.test("Java I Love you" , 2 , 9);
```

4）引用构造器

```java
@FunctionalInterface
interface YourTest
{
	JFrame win(String title);
}

YourTest yt = (String a) -> new JFrame(a);

YourTest yt = JFrame::new;

JFrame jf = yt.win("我的窗口");
System.out.println(jf);
```

总结：如何知道调用的是哪个构造函数？**构造函数入参**跟接口中**抽象方法的入参**对应。

5）数组引用

```java
@Test
public void test2(){
    Function<Integer, String[]> fun = (x) -> new String[x];
    String[] arr = fun.apply(10);
    System.out.println(arr.length);

    Function<Integer,String[]> fun2 = String[]::new;
    String[] arr2 = fun2.apply(20);
    System.out.println(arr2.length);
}
```

# 接口中的默认方法、静态方法

接口中可以定义哪些？

```
[修饰符] interface 接口名 extends 父接口1,父接口2...
{
    零个到多个常量定义...
    零个到多个抽象方法定义...
    零个到多个内部类、接口、枚举定义...
    零个到多个默认方法或静态方法（类方法）定义...
}
```

为什么要有默认方法？

在 java 8 之前，接口与其实现类之间的 耦合度 太高了（tightly coupled），当需要为一个接口添加方法时，所有的实现类都必须随之修改。默认方法解决了这个问题，它可以为接口添加新的方法，而不会破坏已有的接口的实现。这在 lambda 表达式作为 java 8 语言的重要特性而出现之际，为升级旧接口且保持向后兼容（backward compatibility）提供了途径。

参考：https://www.cnblogs.com/sidesky/p/9287710.html

接口默认方法的“类优先”原则：若一个接口中定义了一个默认方法，而另一个父类或接口中又定义了一个同名的方法时
- 选择父类中的方法，如果一个父类提供了具体的实现，那么接口中具有相同名称和参数的默认方法会被忽略
- 接口冲突，如果父接口提供一个默认方法，而另一个接口也提供一个相同名称和参数列表的方法（不管是否是默认方法），那么必须覆盖该方法来解决冲突

总结：
- 接口中定义的变量，如果省略了修改符，则默认是 `public static final`；
- 接口中定义的普通方法，不管是否使用`public abstract`修饰，接口中的普通方法总是使用`public abstract`来修饰
- 接口中普通方法不能有实现体，但类方法、默认方法必须要有实现体

示例：

```java
public interface Output
{
	// 接口里定义的成员变量只能是常量
	int MAX_CACHE_LINE = 50;
	// 接口里定义的普通方法只能是public的抽象方法
	void out();
	void getData(String msg);
	// 在接口中定义默认方法，需要使用default修饰
	default void print(String... msgs)
	{
		for (String msg : msgs)
		{
			System.out.println(msg);
		}
	}
	// 在接口中定义默认方法，需要使用default修饰
	default void test()
	{
		System.out.println("默认的test()方法");
	}
	// 在接口中定义类方法，需要使用static修饰
	static String staticTest()
	{
		return "接口里的类方法";
	}
}
```

如何调用接口中的默认方法、类方法？静态方法直接接口名调用，默认方法用`InterfaceA.super.foo();`或实现类实例调用。


# 新日期、时间 API

新的API所在包：

```
java.time //日期、时间
java.time.chrono //年代，日本年号、民国、佛教
java.time.format //格式化
java.time.temporal //日期、时间运算
java.time.zone //时区
```

新的API解决了线程安全问题。

## 传统日期格式化方法

原来的`SimpleDateFormat`存在线程安全问题：

```java
    @Test
    public void test1(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Callable<Date> task = new Callable<Date>() {
            @Override
            public Date call() throws Exception {
                return sdf.parse("20190906");
            }
        };

        ExecutorService pool = Executors.newFixedThreadPool(1000);

        ArrayList<Future<Date>> result = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            result.add(pool.submit(task));
        }

        for (Future<Date> future : result) {
            try {
                System.out.println(future.get());
            } catch (Exception e) {
            } 
        }
    }
```

可以使用 ThreadLocal 来控制线程安全：

```java
public class DateFormatThradLocal {
    private static final ThreadLocal<DateFormat> df = new ThreadLocal<DateFormat>() { //此处是重点
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };
    
    public static Date convertToDate(String source) throws ParseException {
        return df.get().parse(source);
    }
    
    public static String convertToString(Date date) throws ParseException {
        return df.get().format(date);
    }
}
```

新的日期格式化方法：

```java
    /**
     * 使用java8的日期格式化API
     */
    @Test
    public void test3(){
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        System.out.println(LocalDate.parse("2019-09-06",df));
    }
```

## API使用示例

```java
    // LocalDate
    // LocalTime 
    // LocalDateTime 它们使用方式相似
    @Test
    public void test1(){
        LocalDateTime dt = LocalDateTime.now();//返回当前时期时间：2019-09-06T22:41:40.890
        System.out.println(dt);

        System.out.println(dt.plusDays(2));//加上两天
        System.out.println(dt.minusDays(2));//减两天，其它类推

        System.out.println(dt.getYear());
        System.out.println(dt.getMonthValue());
        System.out.println(dt.getDayOfMonth());//依次类推...
    }

    // Instant，时间戳（以 Unix 元年，即1970年1月1日零时到某个时间之间的毫秒值）
    @Test
    public void test2(){
        Instant ins = Instant.now();//默认是 UTC 时区
        System.out.println(ins.atOffset(ZoneOffset.ofHours(8)));// UTC + 8 时区（北京时间）
    }

    // Duration，计算两个“时间”之间的间隔
    // Period，计算两个“日期”之间的间隔
    @Test
    public void test3(){
        LocalDateTime ldt1 = LocalDateTime.now();
        //Thread.sleep(1000);//需要try-catch
        LocalDateTime ldt2 = LocalDateTime.now();
        Duration duration = Duration.between(ldt1, ldt2);
        System.out.println(duration);//默认是秒
        System.out.println(duration.toMinutes());//转换成分钟

        LocalDate ld1 = LocalDate.of(2015, 12, 9);
        LocalDate ld2 = LocalDate.now();
        Period period = Period.between(ld1, ld2);
        System.out.println(period);
        System.out.println(period.toTotalMonths());
    }
    
    // TemporalAdjuster，时间校正器
    // TemporalAdjusters，工具类
    @Test
    public void test4(){
        LocalDateTime ldt = LocalDateTime.now();
        System.out.println(ldt);
        System.out.println(ldt.withDayOfMonth(20));//2019-09-20T10:37:44.230
    }

    // DateTimeFormatter，格式化时间、日期
    @Test
    public void test5(){
        LocalDateTime ldt = LocalDateTime.now();
        DateTimeFormatter df1 = DateTimeFormatter.ISO_DATE;//使用API提供的格式
        DateTimeFormatter df2 = DateTimeFormatter.ofPattern("yyyy年MM月dd日");//自定义格式

        System.out.println(ldt.format(df1));
        System.out.println(ldt.format(df2));
    }
    
    // ZonedDateTime
    @Test
    public void test6(){
        Set<String> availableZoneIds = ZoneId.getAvailableZoneIds();// 获取所有的时区ID
        availableZoneIds.forEach(System.out::println);

        ZoneId shanghai = ZoneId.of("Asia/Shanghai");
        System.out.println(shanghai.toString());

        LocalDateTime ldt = LocalDateTime.now(ZoneId.of("Europe/Monaco"));//指定时区创建时间
        System.out.println(ldt);
    }
```

# Stream API（重点）

Stream（流） 是 Java8 中处理集合的关键抽象概念。Stream API 提供了一种高效且易于使用的处理数据的方式。

“集合讲的是数据，流讲的是计算！”。

注意：
- Stream 自己不会存储数据。
- Stream 不会改变源对象。相反，他们会返回一个持有结果的新 Stream；
- Stream 操作是延迟的。这意味着他们会等到需要结果时才执行；

操作步骤：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200707202443.png)

## 创建流

示例：

```java
    // 创建 Stream
    @Test
    public void test1(){
        // 方式一：通过 Collection 系列集合提供的 stream() 或 parallelstream()
        ArrayList<String> list = new ArrayList<>();
        Stream<String> stream1 = list.stream();

        // 方式二：通过 Arrays 中的静态方法 stream
        Employee[] empArr = new Employee[10];
        Stream<Employee> stream2 = Arrays.stream(empArr);

        // 方式三：通过 Stream 类中的静态方法 of()
        Stream<String> stream3 = Stream.of("ab", "cd", "ef");

        // 方式四：创建无限流
        Stream<Integer> stream4 = Stream.iterate(0, (x) -> x + 2);
        stream4.limit(30).forEach(System.out::println);
        Stream.generate(() -> Math.random()).limit(10).forEach(System.out::println);
    }
```

## 中间操作

多个中间操作连接起来就是一个流水线，除非流水线上触发终止操作，否则中间操作不会执行任何的处理，而在终止操作时一次性全部处理，称为“惰性求值”。

- 筛选、切片
- 映射
- 排序

筛选、切片
方法 | 描述
---|---
filter(Predicate<? super T> predicate) | 筛选，接收 Lambda，从流中排除某些元素
distinct() | 筛选，通过流所生成元素的 hashCode() 和 equals() 去除重复元素
limit(long maxSize) | 截断流，使其元素不超过给定数量
skip(long n) | 跳过元素，返回一个扔掉了前 n 个元素的流。若流中元素不足 n 个，则返回一个空流。<br/> 与 limit(n) 互补。

映射
方法 | 描述
---|---
map(Function f) | 接收一个函数作为参数，该函数会被应用到每个元素上，并将其映射成一个新的元素
mapToDouble(ToDoubleFunction f) | 接收一个函数作为参数，该函数会被应用到每个元素上，产生一个新的 DoubleStream
mapToInt(ToIntFunction f) | 接收一个函数作为参数，该函数会被应用到每个元素上，产生一个新的 IntStream
mapToLong(ToLongFunction f) | 接收一个函数作为参数，该函数会被应用到每个元素上，产生一个新的 LongStream
flatMap(Function f) | 接收一个函数作为参数，将流中的每个值都换成另一个流，然后将所有流连接成一个流

总结：map() 和 flatMap() 的区别有点像集合中的 add() addAll()的区别

排序
方法 | 描述
---|---
sorted() | 产生一个新流，其中按自然顺序排序（Comparable）
sorted(Comparator comp) | 产生一个新流，其中按比较器顺序排序（Comparator）

示例：

```java
    List<Employee> employeeList = Arrays.asList(
            new Employee(101, "张三", 19, 3333.33),
            new Employee(102, "李四", 39, 5555.77),
            new Employee(103, "王五", 29, 9999.88),
            new Employee(104, "李二狗", 49, 3333.33),
            new Employee(105, "秦二春", 59, 9999.99)
    );
    // 中间操作
    @Test
    public void test2(){
        Stream<Employee> stream = employeeList.stream().filter(e -> e.getAge() > 20).limit(1);
        stream.forEach(System.out::println);// 终止操作
        
        List<String> stringList = Arrays.asList("aaa", "bbb", "ccc");
        stringList.stream().map(str -> str.toUpperCase()).forEach(System.out::println);// map
        employeeList.stream().map(Employee::getName).forEach(System.out::println);// 注意Employee::getName写法
        
        stringList.stream().sorted().forEach(System.out::println);// 自然排序
        employeeList.stream().sorted((e1,e2)->{// 定制排序
            if (e1.getAge().equals(e2.getAge())) {
                return e1.getName().compareTo(e2.getName());
            }else {
                return e1.getAge().compareTo(e2.getAge());
            }
        }).forEach(System.out::println);
    }
```

## 终止操作

终止操作会从流的流水生成结果。其结果可以是任何不是流的值，例如：List、Integet，甚至是 void。

查找与匹配
方法 | 描述
---|---
allMatch(Predicate p) | 检查是否匹配所有元素
anyMatch(Predicate p) | 检查是否至少匹配一个元素
noneMatch(Predicate p) | 检查是否没有匹配所有元素
findFirst() | 返回第一个元素
findAny() | 返回当前流中任意元素

归约
方法 | 描述
---|---
reduce(T identity, BinaryOperator<T> accumulator) | 将流中元素反复结合起来，得到一个值。
reduce(BinaryOperator<T> accumulator) | 

收集
方法 | 描述
---|---
collect(Collector collector) | `Collectors`工具类提供了很多静态方法来操作集合
collect(Supplier supplier, BiConsumer accumulator,BiConsumer combiner) | 

示例：

```java
// 终止操作
@Test
public void test3(){
    long count = employeeList.stream().count();
    Optional<Employee> op1 = employeeList.stream()
            .max((e1, e2) -> Double.compare(e1.getSalary(), e2.getSalary()));
    System.out.println(op1.get().getSalary());
    Optional<Double> op2 = employeeList.stream().map(Employee::getSalary).min(Double::compareTo);
    System.out.println(op2.get());
    //归约
    List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    // identity 是起始值，开始时，x=identity=0，然后x作为和，y是list中的下一个元素，相当于是 x += y 操作
    Integer sum = list.stream().reduce(0, (x, y) -> x + y);
    System.out.println(sum);
    Optional<Double> opt3 = employeeList.stream().map(Employee::getSalary).reduce(Double::sum);
    System.out.println(opt3.get());
    //收集
    List<String> list2 = employeeList.stream().map(Employee::getName).collect(Collectors.toList());
    list2.forEach(System.out::println);
    employeeList.stream().map(Employee::getName).collect(Collectors.toSet()).forEach(System.out::print);//set可以去重
    employeeList.stream().map(Employee::getName).collect(Collectors.toCollection(HashSet::new)).forEach(System.out::print);//收集到 hashset
    Long count1 = employeeList.stream().collect(Collectors.counting());//总数
    Double avg1 = employeeList.stream().collect(Collectors.averagingDouble(Employee::getSalary));//求平均
    System.out.println(avg1);
    Optional<Double> opt4 = employeeList.stream().map(Employee::getSalary).collect(Collectors.maxBy(Double::compare));//最大值
    Optional<Double> opt5 = employeeList.stream().map(Employee::getSalary).collect(Collectors.minBy(Double::compare));//最小值
    System.out.println(opt4.get());
    Map<Integer, List<Employee>> map1 = employeeList.stream().collect(Collectors.groupingBy(Employee::getAge));//分组
    System.out.println(map1);
    Map<String, Map<Integer, List<Employee>>> map2 = employeeList.stream()
            .collect(Collectors.groupingBy(Employee::getName, Collectors.groupingBy(Employee::getAge)));//多级分组，先按姓名，再按年龄
    System.out.println(map2);
    Map<Boolean, List<Employee>> map3 = employeeList.stream()
            .collect(Collectors.partitioningBy((e) -> e.getSalary() > 5000));//分区，按工资大于5000分区
    System.out.println(map3);
    DoubleSummaryStatistics dss = employeeList.stream()
            .collect(Collectors.summarizingDouble(Employee::getSalary));//汇总，跟上面求最大、最小、数量功能相同
    System.out.println(dss.getMax());
    System.out.println(dss.getMin());
    System.out.println(dss.getCount());
    String str1 = employeeList.stream().map(Employee::getName).collect(Collectors.joining(","));//连接，名字的拼接
    System.out.println(str1);
}
```

map 和 reduce 的连接通常称为 map-reduce 模式，因Google用它来进行网络搜索而出名。

## API练习

1）给定一个数字列表，如何返回一个由每个数的平方构成的列表？如，给定【1，2，3，4，5】，返回【1，4，9，16，25】

```java
    @Test
    public void test4(){
        Integer[] intArr = {1, 2, 3, 4, 5};
        Arrays.stream(intArr).map((x) -> x * x).forEach(System.out::print);
    }
```

2）怎样用 map 和 reduce 方法统计流中有多少个 Employee ？

```java
    @Test
    public void test5() {
        Optional<Integer> opt1 = employeeList.stream().map(e -> 1).reduce(Integer::sum);//亮点：map(e -> 1)
        System.out.println(opt1.get());
    }
```

3）练习题

```java
//交易员类
public class Trader {

	private String name;
	private String city;

	public Trader() {
	}

	public Trader(String name, String city) {
		this.name = name;
		this.city = city;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "Trader [name=" + name + ", city=" + city + "]";
	}
}
```

```java
//交易类
public class Transaction {

	private Trader trader;
	private int year;
	private int value;

	public Transaction() {
	}

	public Transaction(Trader trader, int year, int value) {
		this.trader = trader;
		this.year = year;
		this.value = value;
	}

	public Trader getTrader() {
		return trader;
	}

	public void setTrader(Trader trader) {
		this.trader = trader;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Transaction [trader=" + trader + ", year=" + year + ", value="
				+ value + "]";
	}
}
```

练习题：

```java
public class TransactionTest {
    List<Transaction> transactions = null;

    @Before
    public void before(){
        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario", "Milan");
        Trader alan = new Trader("Alan", "Cambridge");
        Trader brian = new Trader("Brian", "Cambridge");

        transactions = Arrays.asList(
                new Transaction(brian, 2011, 300),
                new Transaction(raoul, 2012, 1000),
                new Transaction(raoul, 2011, 400),
                new Transaction(mario, 2012, 710),
                new Transaction(mario, 2012, 700),
                new Transaction(alan, 2012, 950)
        );
    }
    //1. 找出2011年发生的所有交易， 并按交易额排序（从低到高）
    //2. 交易员都在哪些不同的城市工作过？
    //3. 查找所有来自剑桥的交易员，并按姓名排序
    //4. 返回所有交易员的姓名字符串，按字母顺序排序
    //5. 有没有交易员是在米兰工作的？
    //6. 打印生活在剑桥的交易员的所有交易额
    //7. 所有交易中，最高的交易额是多少
    //8. 找到交易额最小的交易
}
```

参考：https://github.com/wangpw2016/java8-day02/blob/master/src/com/atguigu/exer/TestTransaction.java

```java
public class TransactionTest {
    List<Transaction> transactions = null;

    @Before
    public void before() {
        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario", "Milan");
        Trader alan = new Trader("Alan", "Cambridge");
        Trader brian = new Trader("Brian", "Cambridge");

        transactions = Arrays.asList(
                new Transaction(brian, 2011, 300),
                new Transaction(raoul, 2012, 1000),
                new Transaction(raoul, 2011, 400),
                new Transaction(mario, 2012, 710),
                new Transaction(mario, 2012, 700),
                new Transaction(alan, 2012, 950)
        );
    }

    //1. 找出2011年发生的所有交易， 并按交易额排序（从低到高）
    @Test
    public void test1() {
        transactions.stream()
                .filter(e -> e.getYear() == 2011)
                .sorted((e1, e2) -> Integer.compare(e1.getValue(), e2.getValue()))
                .forEach(System.out::println);
    }

    //2. 交易员都在哪些不同的城市工作过？
    @Test
    public void test2() {
        transactions.stream()
                .map(Transaction::getTrader)
                .map(Trader::getCity)
                .collect(Collectors.toSet())
                .forEach(System.out::println);
    }
    //3. 查找所有来自剑桥的交易员，并按姓名排序
    @Test
    public void test3(){
        transactions.stream()
                .map(Transaction::getTrader)
                .filter(e -> e.getCity().equals("Cambridge"))
                .sorted((e1,e2) -> e1.getName().compareTo(e2.getName()))
                .distinct()// 去重
                .forEach(System.out::println);
    }
    //4. 返回所有交易员的姓名字符串，按字母顺序排序
    @Test
    public void test4(){
        transactions.stream()
                .map(Transaction::getTrader)
                .map(Trader::getName)
                .sorted()
                .distinct()
                .forEach(System.out::println);
        String str = transactions.stream()
                .map(Transaction::getTrader)
                .map(Trader::getName)
                .sorted().distinct()
                .reduce("", String::concat);//重点，归约
        System.out.println(str);
    }

    //5. 有没有交易员是在米兰工作的？
    @Test
    public void test5(){
        boolean b = transactions.stream()
                .anyMatch(e -> "Milan".equals(e.getTrader().getCity()));
        System.out.println(b);
    }
    //6. 打印生活在剑桥的交易员的所有交易额
    @Test
    public void test6(){
        // 方式一
        IntSummaryStatistics valSum = transactions.stream()
                .filter(t -> "Cambridge".equals(t.getTrader().getCity()))
                .collect(Collectors.summarizingInt(Transaction::getValue));
        System.out.println(valSum.getSum());
        // 方式二
        Optional<Integer> valSum2 = transactions.stream()
                .filter(t -> "Cambridge".equals(t.getTrader().getCity()))
                .map(Transaction::getValue)
                .reduce(Integer::sum);
        System.out.println(valSum2.get());
    }
    //7. 所有交易中，最高的交易额是多少
    @Test
    public void test7(){
        // 方式一
        Optional<Transaction> opt = transactions.stream()
                .collect(Collectors.maxBy((e1, e2) -> Integer.compare(e1.getValue(), e2.getValue())));
        System.out.println(opt.get().getValue());
        // 方式二
        Optional<Integer> opt2 = transactions.stream()
                .map(Transaction::getValue)
                .max(Integer::compare);
        System.out.println(opt2.get());
    }
    //8. 找到交易额最小的交易（记录）
    @Test
    public void test8(){
        Optional<Transaction> opt = transactions.stream()
                .min((e1, e2) -> Integer.compare(e1.getValue(), e2.getValue()));
        System.out.println(opt.get());
    }


    //1. 找出2011年发生的所有交易， 并按交易额排序（从低到高）
    //2. 交易员都在哪些不同的城市工作过？
    //3. 查找所有来自剑桥的交易员，并按姓名排序
    //4. 返回所有交易员的姓名字符串，按字母顺序排序
    //5. 有没有交易员是在米兰工作的？
    //6. 打印生活在剑桥的交易员的所有交易额
    //7. 所有交易中，最高的交易额是多少
    //8. 找到交易额最小的交易
}
```


# Optional

java.util.Optional 是一个容器类，代表一个值存在或不存在，可以避免空指针异常。

常用方法：
- `Optional.of(T t);`创建一个 Optional 实例
- `Optional.empty();`创建一个空的 Optional 实例
- `Optional.ofNullable(T t);`若 T 不为 null ，创建 Optional 实例，否则创建空实例
- `isPresent();`判断是否包含值
- `orElse(T t);`如果调用对象包含值，返回该值，否则返回 t
- `orElseGet(Supplier<? extends T> s);`如果调用对象包含值，返回该值，否则返回 s 获取的值
- `map(Function<? super T, ? extends U> mapper);`如果有值，对其处理并返回处理后的 Optional，否则返回`Optional.empty();`
- `flatMap(Function<? super T, Optional<U>> mapper);`与 map 类似，要求返回值必须是 Optional

使用示例：https://www.cnblogs.com/zhangboyu/p/7580262.html


# 并行流 & 顺序流

并行流就是把一个内容分成多个数据块，并用不同的线程分别处理每个数据块的流。

Java8中将并行流进行了优化，Stream API 可以声明性通过 parallel() 与 sequential() 在并行流与顺序流之间切换。

了解 Fork/Join 框架，大任务分为小任务，可以将结果汇总或没有返回值；

java7中关于 Fork/Join 的写法，参考《Java-Se-多线程-1》。

采用“工作窃取”模式（work-stealing）：当执行新的任务时它可以将其拆分成更小的任务执行，并将小任务加到线程队列中，然后再从一个随机的队列中偷一个并把他放在自己的队列中。

```java
    // java8
    @Test
    public void test(){
        Instant start = Instant.now();

        LongStream.rangeClosed(0, 100000000000L)
                .parallel()
                .reduce(0, Long::sum);

        Instant end = Instant.now();
        System.out.println("耗费时间："+Duration.between(start,end).toMillis()+" 毫秒");// 34737
    }
```

# 可重复注解、类型注解

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200707202602.png)

解析注解：

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200707202621.png)