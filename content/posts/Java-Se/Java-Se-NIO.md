---
title: "Java-Se-NIO"
date: 2017-07-01T12:11:47+08:00
draft: true
tags: [""]
categories: ["Java-Se"]
---


# 基本概念

## 通道

通道（Channel）是现代计算机的一个特性，用于源节点与目标节点之间的连接。它不能单独地使用，必须结合缓冲区（Buffer）一起使用。通道就像一条条高速公路，而缓冲区就是在高速公路上跑的一输输车。通道是双向的，既可读，也可写。

常用的通道有：

- FileChannel
- SocketChannel
- ServerSocketChannel
- DatagramChannel

---

获取通道的方式有：

- RandomAccessFile/FileInputStream/FileOutputStream对象上调用getChannel方法来获取。
- FileChannel/SocketChannel/ServerSocketChannel/DatagramChannel等调用自身的静态方法open()来获取。

---

Channel中最常用的方法:

- map()，将Channel对应的部分或全部数据映射成ByteBuffer（MappedByteBuffer）.也叫内存映射。
- read()，读取数据。
- wirte()，写入数据。

## 缓冲区

缓冲区（Buffer）可以理解成一个容器（本质上是个数组），可以保存多个类型相同的数据。

常用的缓冲区有：

- ByteBuffer
- CharBuffer
- ShortBuffer
- IntBuffer
- LongBuffer
- FloadBuffer
- DoubleBuffer
- MappedByteBuffer

---
创建缓冲区的方式：

- 调用缓冲区本身的静态方法 `static XxxBuffer allocate(int capacity)`.将缓冲区建立在JVM内存中。
- 调用缓冲区本身的静态方法 `static XxxBuffer allocateDirect(int capacity)`（只限ByteBuffer，其它的不能使用此方法）.将缓冲区建立在物理内存中，提高效率，但不安全、资源消耗大。

---

缓冲区中，要搞清楚四个常量及它们之间的关系：

`0 <= mark <= position <= limit <= capacity `

Buffer中有两个重要的方法：

- flip()，将写状态转为读状态  （limit=position,limit~capacity之间的数据被封印起来）
- clear()，将读状态转为写状态 （position=0，limit = capacity，但前一个缓冲区的数据并没有立即被清除。只有当前存入的位置的值被替换）

Buffer的其它常用方法：

- put()：将数据放入Buffer。
- get()：从Buffer中取出数据。
- Buffer mark()：设置mark位置，mark = position;。
- Buffer reset()：将position转到mark所在位置，position = mark;。
- Buffer rewind()：将position设为0，取消设置mark。

## 选择器

选择器（Selector）是 SelectableChannel对象的多路复用器，所有希望采用非阻塞方式进行通信的Channel都应该注册到Selector对象。

创建 Selector实例的方法：调用Selector类的`open()`静态方法

## Charset

二进制与字符之间的转换

```
Charset cn = Charset.forName("GBK");
ByteBuffer byteBuffer = cn.encode(""); //将字符串转换成二进制
CharBuffer charBuffer = cn.decode(java.nio.ByteBuffer bb); //将二进制转换成字符

```

# 通道、缓冲区的使用

## FileChannel

1、Channel的map()方法是将对应的部分或全部数据映射成ByteBuffer（MappedByteBuffer），即内存映射，此种方式对大文件的操作可能会影响系统性能。

Channel的map()方法跟Buffer的allocateDirect(int capacity)原理一样。

```
public class FileChannelTest{
    public static void main(String[] args){
        File f = new File("FileChannelTest.java");
        try(
            // 创建FileInputStream，以该文件输入流创建FileChannel
            FileChannel inChannel = new FileInputStream(f).getChannel();
            // 以文件输出流创建FileBuffer，用以控制输出
            FileChannel outChannel = new FileOutputStream("a.txt").getChannel())
        {
            // 将FileChannel里的全部数据映射成ByteBuffer
            MappedByteBuffer buffer = inChannel.map(FileChannel.MapMode.READ_ONLY , 0 , f.length());   // ①
            // 使用GBK的字符集来创建解马器
            Charset charset = Charset.forName("GBK");
            // 直接将buffer里的数据全部输出
            outChannel.write(buffer);     // ②
            // 再次调用buffer的clear()方法，复原limit、position的位置
            buffer.clear();
            // 创建解马器(CharsetDecoder)对象
            CharsetDecoder decoder = charset.newDecoder();
            // 使用解马器将ByteBuffer转换成CharBuffer
            CharBuffer charBuffer =  decoder.decode(buffer);
            // CharBuffer的toString方法可以获取对应的字符串
            System.out.println(charBuffer);
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
    }
}

```

2、Channel的read()、write()方法可以实现“竹筒重复取水”方式，大小文件都可以操作。
```
public class ReadFile{
    public static void main(String[] args) throws IOException {
        try(
            // 创建文件输入流
            FileInputStream fis = new FileInputStream("ReadFile.java");
            // 创建一个FileChannel
            FileChannel fcin = fis.getChannel())
        {
            // 定义一个ByteBuffer对象，用于重复取水
            ByteBuffer bbuff = ByteBuffer.allocate(256);
            // 将FileChannel中数据放入ByteBuffer中
            while( fcin.read(bbuff) != -1 )
            {
                // 锁定Buffer的空白区
                bbuff.flip();
                // 创建Charset对象
                Charset charset = Charset.forName("GBK");
                // 创建解马器(CharsetDecoder)对象
                CharsetDecoder decoder = charset.newDecoder();
                // 将ByteBuffer的内容转码
                CharBuffer cbuff = decoder.decode(bbuff);
                System.out.print(cbuff);
                // 将Buffer初始化，为下一次读取数据做准备
                bbuff.clear();
            }
        }
    }
}

```


# 选择器的使用

选择器（Selector）可以同时监控多个 SelectableChannel 的IO状况，是非阻塞式的核心。

```
java.nio.channels.Channel
                   |-SelectableChannel
                        |-SocketChannel
                        |-ServerSocketChannel
                        |-DatagramChannel

                        |-Pipe.SinkChannel, 
                        |-Pipe.SourceChannel
```

---

Selector实例通过 Selector.open()获取，其实例的常用如下方法：

- keys(),返回SelectionKey集合，代表所有注册在该 Selector上的所有Channel
- selectedKey,返回SelectionKey集合，代表该 Selector上需要进行IO处理的Channel
- select()，监控Channel,当有需要处理的IO操作的Channel时，返回这此Channel的数量
- selectNow()，同select()，只是不阻塞线程

---

SelectableChannel 常用如下方法：

- register()，将其注册到指定的Selector上
- configureBlocking(boolean block)，是否采用阻塞模式（所有的 Channel默认都是阻塞式）
- isBlocking()，是否是阻塞模式
- vilidOps，返回一个整数值，表示所支持的IO操作

---

阻塞与非阻塞

- FileChannel不可用于非阻塞式，不可注册到Selector

通过通道（Channel）来控制阻塞与非阻塞：
```java
        channel.configureBlocking(false);
```

## 阻塞 && 非阻塞、异步 && 同步

参看《Java-Se-网络编程.docx》

---

## Server

地址和端口包装成了一个单独的对象：InetSocketAddress。

创建一个ServerSocketChannel的代码片段如下：

```
        ServerSocketChannel server = ServerSocketChannel.open();
        InetSocketAddress isa = new InetSocketAddress("127.0.0.1", 30000);
        server.bind(isa);
```

将该Channel注册到指定Selector上的代码片段如下：
```
        Selector selector = Selector.open();

        server.configureBlocking(false);
        server.register(selector,SelectionKey.OP_ACCEPT);
```

Server完整示例如下：
```java
public class NServer
{
    // 用于检测所有Channel状态的Selector
    private Selector selector = null;
    static final int PORT = 30000;
    // 定义实现编码、解码的字符集对象
    private Charset charset = Charset.forName("UTF-8");
    public void init()throws IOException
    {
        selector = Selector.open();
        // 通过open方法来打开一个未绑定的ServerSocketChannel实例
        ServerSocketChannel server = ServerSocketChannel.open();
        InetSocketAddress isa = new InetSocketAddress("127.0.0.1", PORT);
        // 将该ServerSocketChannel绑定到指定IP地址
        server.bind(isa);
        // 设置ServerSocket以非阻塞方式工作
        server.configureBlocking(false);
        // 将server注册到指定Selector对象
        server.register(selector, SelectionKey.OP_ACCEPT);
        while (selector.select() > 0)
        {
            // 依次处理selector上的每个已选择的SelectionKey
            for (SelectionKey sk : selector.selectedKeys())
            {
                // 从selector上的已选择Key集中删除正在处理的SelectionKey
                selector.selectedKeys().remove(sk);      // ①
                // 如果sk对应的Channel包含客户端的连接请求
                if (sk.isAcceptable())        // ②
                {
                    // 调用accept方法接受连接，产生服务器端的SocketChannel
                    SocketChannel sc = server.accept();
                    // 设置采用非阻塞模式
                    sc.configureBlocking(false);
                    // 将该SocketChannel也注册到selector
                    sc.register(selector, SelectionKey.OP_READ);
                    // 将sk对应的Channel设置成准备接受其他请求
                    sk.interestOps(SelectionKey.OP_ACCEPT);
                }
                // 如果sk对应的Channel有数据需要读取
                if (sk.isReadable())     // ③
                {
                    // 获取该SelectionKey对应的Channel，该Channel中有可读的数据
                    SocketChannel sc = (SocketChannel)sk.channel();
                    // 定义准备执行读取数据的ByteBuffer
                    ByteBuffer buff = ByteBuffer.allocate(1024);
                    String content = "";
                    // 开始读取数据
                    try
                    {
                        while(sc.read(buff) > 0)
                        {
                            buff.flip();
                            content += charset.decode(buff);
                        }
                        // 打印从该sk对应的Channel里读取到的数据
                        System.out.println("读取的数据：" + content);
                        // 将sk对应的Channel设置成准备下一次读取
                        sk.interestOps(SelectionKey.OP_READ);
                    }
                    // 如果捕捉到该sk对应的Channel出现了异常，即表明该Channel
                    // 对应的Client出现了问题，所以从Selector中取消sk的注册
                    catch (IOException ex)
                    {
                        // 从Selector中删除指定的SelectionKey
                        sk.cancel();
                        if (sk.channel() != null)
                        {
                            sk.channel().close();
                        }
                    }
                    // 如果content的长度大于0，即聊天信息不为空
                    if (content.length() > 0)
                    {
                        // 遍历该selector里注册的所有SelectionKey
                        for (SelectionKey key : selector.keys())
                        {
                            // 获取该key对应的Channel
                            Channel targetChannel = key.channel();
                            // 如果该channel是SocketChannel对象
                            if (targetChannel instanceof SocketChannel)
                            {
                                // 将读到的内容写入该Channel中
                                SocketChannel dest = (SocketChannel)targetChannel;
                                dest.write(charset.encode(content));
                            }
                        }
                    }
                }
            }
        }
    }
    public static void main(String[] args)
        throws IOException
    {
        new NServer().init();
    }
}

```

## Client

```
public class NClient {
    // 定义检测SocketChannel的Selector对象
    private Selector selector = null;
    static final int PORT = 30000;
    // 定义处理编码和解码的字符集
    private Charset charset = Charset.forName("UTF-8");
    // 客户端SocketChannel
    private SocketChannel sc = null;
    public void init()throws IOException
    {
        selector = Selector.open();
        InetSocketAddress isa = new InetSocketAddress("127.0.0.1", PORT);
        // 调用open静态方法创建连接到指定主机的SocketChannel
        sc = SocketChannel.open(isa);
        // 设置该sc以非阻塞方式工作
        sc.configureBlocking(false);
        // 将SocketChannel对象注册到指定Selector
        sc.register(selector, SelectionKey.OP_READ);
        // 启动读取服务器端数据的线程
        new ClientThread().start();
        // 创建键盘输入流
        Scanner scan = new Scanner(System.in);
        while (scan.hasNextLine())
        {
            // 读取键盘输入
            String line = scan.nextLine();
            // 将键盘输入的内容输出到SocketChannel中
            sc.write(charset.encode(line));
        }
    }
    // 定义读取服务器数据的线程
    private class ClientThread extends Thread
    {
        public void run()
        {
            try
            {
                while (selector.select() > 0)    // ①
                {
                    // 遍历每个有可用IO操作Channel对应的SelectionKey
                    for (SelectionKey sk : selector.selectedKeys())
                    {
                        // 删除正在处理的SelectionKey
                        selector.selectedKeys().remove(sk);
                        // 如果该SelectionKey对应的Channel中有可读的数据
                        if (sk.isReadable())
                        {
                            // 使用NIO读取Channel中的数据
                            SocketChannel sc = (SocketChannel)sk.channel();
                            ByteBuffer buff = ByteBuffer.allocate(1024);
                            String content = "";
                            while(sc.read(buff) > 0)
                            {
                                sc.read(buff);
                                buff.flip();
                                content += charset.decode(buff);
                            }
                            // 打印输出读取的内容
                            System.out.println("聊天信息：" + content);
                            // 为下一次读取作准备
                            sk.interestOps(SelectionKey.OP_READ);
                        }
                    }
                }
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }
    }
    public static void main(String[] args)
        throws IOException
    {
        new NClient().init();
    }
}

```

# 分散与聚集

分散读取（Scattering Reads）：将通道中的数据分散到多个缓冲区中

聚集写入（Gathering Writes）：将多个缓冲区中的数据聚集到通道中

## 分散读取

```java
    public void tset() throws Exception {

        RandomAccessFile raf1 = new RandomAccessFile("a.txt", "rw");

        FileChannel channel1 = raf1.getChannel();

        ByteBuffer buf1 = ByteBuffer.allocate(1024);
        ByteBuffer buf2 = ByteBuffer.allocate(1024);

        ByteBuffer[] bufs = {buf1,buf2};
        channel1.read(bufs);

        for(ByteBuffer b:bufs){
            b.flip();
        }

        System.out.println(new String(bufs[0].array(),0,bufs[0].limit()));
        System.out.println("----------------------");
        System.out.println(new String(bufs[1].array(),0,bufs[0].limit()));
    }
```

## 聚集写入

```java
        RandomAccessFile raf2 = new RandomAccessFile("a.txt", "rw");
        FileChannel channel2 = raf2.getChannel();
        channel2.write(bufs);
```

# Java-Se-NIO2

jdk7提供： Path,Paths,Files,SimpleFileVisitor,WatchService

- `Path path = Paths.get("pom.xml");`获取项目根路径下的文件，获取项目下的文件，如`Path path = Paths.get("log","pom.xml");`，可以可以指定路径，如`Path path = Paths.get("e:", "project.properties");`.
- `SimpleFileVisitor`递归删除指定目录下的文件和文件，**参考jdk中`FileVisitor`文档示例**。
- `Files`实现文件的复制和删除。
