---
title: "Java-Se-关键字"
date: 2017-07-01T10:17:47+08:00
draft: true
tags: [""]
categories: ["Java-Se"]
---

# synchronized

可以修饰方法、代码块

不能修饰类，如：

```java
public synchronized class Test {
}
```

# assert

https://www.cnblogs.com/wardensky/p/4307848.html

```java
    public static Integer valueOf(int i) {
        assert IntegerCache.high >= 127;
        if (i >= IntegerCache.low && i <= IntegerCache.high)
            return IntegerCache.cache[i + (-IntegerCache.low)];
        return new Integer(i);
    }
```

为了提高性能，在软件发布后，assertion检查通常是关闭的。开发工具默认关闭断言，开启需要设置相关项。

此处说的是关键字 assert，而不是在有些源代码中看到的 Assert。比如 junit 中提供了 Assert.assertEquals(int expected, int actual)。


# final

final有“不可更改”的意思，具体视语义而定。

final修饰字段（实例、类、局部）时，一旦赋值后不能再更改。（注意：实例、类变量初始化时必须赋值，如果定义时没有赋值，可以代码块、静态代码块、构造器中赋值；局部变量则必须在使用前赋值，即，如果定义时没有赋值，使用前必须赋值，且只能赋值一次）。如果在编译时就可以把值确定下来，那么就会执行“宏替换”。

final修饰方法时，方法不能被子类重写。

final修饰类时，此类不能再派生出子类。

# finalize

finalize() 是 Object 提供的的实例方法

# static

static 修饰的变量不能定义在方法内

```java
public class Test {
    public void atest(){
        static int i = 0;//编译报错
    }
}
```


# private、default、protected、public

![keyword-decorate](https://oscimg.oschina.net/oscnet/ab5947f0800ac567b38eb684e5c18e6db42.jpg "keyword-decorate")

# package

# import/import static

# this/super

# instanceof

```t
if(实例对象 instanceof 类或接口){

}
```

instanceof运算符，前一个参数是个引用类型变量（a)，后一个参数是类或接口(b)。要求a与b类型相同或具有父子关系。

# volatile

底层实现原理？
答题思路：1、先说操作系统的内存模型，2、再说并发编程的三个概念（原子性、可见性、有序性），3、说java的内存模型（原子性的类，volatile保存可见性和有序性）。

答：volatile关键字在多线程编程时使用到，它可以保存可见性、有序性，**但不能保存原子性**。
可见性即工作内存中的内容会及时刷新到主存中，即可见性是指当多个线程访问同一个变量时，一个线程修改了这个变量的值，其他线程能够立即看得到修改的值；
有序性即程序执行的顺序按照代码的先后顺序执行。什么是指令重排序，一般来说，处理器为了提高程序运行效率，可能会对输入代码进行优化，它不保证程序中各个语句的执行先后顺序同代码中的顺序一致，但是它会保证程序最终执行结果和代码顺序执行的结果是一致的。指令重排序不会影响单个线程的执行，但是会影响到线程并发执行的正确性。（完）

volatile关键字禁止指令重排序有两层意思：
1）当程序执行到volatile变量的读操作或者写操作时，在其前面的操作的更改肯定全部已经进行，且结果已经对后面的操作可见；在其后面的操作肯定还没有进行；
2）在进行指令优化时，不能将在对volatile变量访问的语句放在其后面执行，也不能把volatile变量后面的语句放到其前面执行。

volatile关键字在某些情况下性能要优于synchronized，但是要注意volatile关键字是无法替代synchronized关键字的，因为volatile关键字无法保证操作的原子性。
java.util.concurrent.atomic包下的原子变量 使用了CAS（compare-and-swap）算法，具有原子性。

注意：可见性、有序性、原子性是多线程安全的特点，缺一不可。

# transient

变量修饰符，如果用transient声明一个实例变量，当对象存储时，它的值不需要维持。换句话来说就是，用transient关键字标记的成员变量不参与序列化过程。

# native

native关键字说明其修饰的方法是一个原生态方法，方法对应的实现不是在当前文件，而是在用其他语言（如C和C++）实现的文件中。Java语言本身不能对操作系统底层进行访问和操作，但是可以通过JNI接口调用其他语言来实现对底层的访问。
