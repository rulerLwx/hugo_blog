---
title: "Java-Se-序列化"
date: 2017-07-01T11:15:47+08:00
draft: true
tags: ["网络编程"]
categories: ["Java-Se"]
---

# 什么是序列化

将实现序列化的Java对象转换成字节序列，这些字节序列可以保存到磁盘上，或通过网络传输，以备以后重新恢复成原来的对象。

# 为什么要序列化

保存对象或将保存的对象在网络中传输，以重新恢复对象。

## 怎样实现序列化、反序列化

```java
public class WriteObject {
    public static void main(String[] args) {
        try (ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream("object.txt"))){
            Person person = new Person("张三", 500);
            oos.writeObject(person);
        }catch (Exception e) {
        }
    }
}
```

```java
public class ReadObject {
    public static void main(String[] args) {
        try (ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream("object.txt"))) {
            Person per = (Person) ois.readObject();
            System.out.println(per.getName());
        } catch (Exception e) {
        }
    }
}
```

# 序列化的注意点

当一个可序列化类有多个父类时（包括直接父类和间接父类），这些父类要么有无参的构造器，要么也是可序列化的——否则反序列化时将招聘InvalidClassException异常。如果父类是不可序列的，只是带有无参的构造器，则该父类中定义的成员变量值不会序列化到二进制流中。

如果某个类的成员变量的不是基本类型或String类型，而是另一个引用类型，那么这个引用类必须是可序列化的的，否则拥有该类型成员变量的类也是不可序列化的。