---
title: "Java-Se-正则表达式"
date: 2017-07-01T11:17:47+08:00
draft: true
tags: [""]
categories: ["Java-Se"]
---

# 正则表达式

正则表达式，Regular Expression。

```
	java.util.regex.Pattern
	java.util.regex.Matcher

```

## 1. 定义正则表达式

可以查看Pattern的api。

字符
正则 | 解释
---|---
x  | 字符 x
`\\ ` | 反斜线字符 
\0n  | 带有八进制值 0 的字符 n (0 <= n <= 7) 
\0nn | 带有八进制值 0 的字符 nn (0 <= n <= 7) 
\0mnn | 带有八进制值 0 的字符 mnn（0 <= m <= 3、0 <= n <= 7） 
\xhh | 带有十六进制值 0x 的字符 hh 
\uhhhh | 带有十六进制值 0x 的字符 hhhh 
\t | 制表符 ('\u0009') 
\n | 新行（换行）符 ('\u000A') 
\r | 回车符 ('\u000D') 
\f | 换页符 ('\u000C') 
\a | 报警 (bell) 符 ('\u0007') 
\e | 转义符 ('\u001B') 
\cx | 对应于 x 的控制符 

字符类
正则 | 解释
---|---
[abc] | a、b 或 c（简单类） 
[^abc] | 任何字符，除了 a、b 或 c（否定） 
[a-zA-Z] | a 到 z 或 A 到 Z，两头的字母包括在内（范围） 
[a-d[m-p]] | a 到 d 或 m 到 p：[a-dm-p]（并集） 
[a-z&&[def]] | d、e 或 f（交集） 
[a-z&&[^bc]] | a 到 z，除了 b 和 c：[ad-z]（减去） 
[a-z&&[^m-p]] | a 到 z，而非 m 到 p：[a-lq-z]（减去） 

预定义字符类
正则 | 解释
---|---
. | 任何字符（与行结束符可能匹配也可能不匹配） <br> 字母或单词 ——20190709
\d | 数字：[0-9] 
\D | 非数字： [^0-9] 
\s | 空白字符：[ \t\n\x0B\f\r] 
\S | 非空白字符：[^\s] 
\w | 单词字符：[a-zA-Z_0-9] 
\W | 非单词字符：[^\w] 

边界匹配器
正则 | 解释
---|---
^ | 行的开头 
$ | 行的结尾 
\b | 单词边界 
\B | 非单词边界 
\A | 输入的开头 
\G | 上一个匹配的结尾 
\Z | 输入的结尾，仅用于最后的结束符（如果有的话） 
\z | 输入的结尾 

Greedy 数量词
正则 | 解释
---|---
X? | X，一次或一次也没有 
X* | X，零次或多次 
X+ | X，一次或多次 
X{n} | X，恰好 n 次 
X{n,} | X，至少 n 次 
X{n,m} | X，至少 n 次，但是不超过 m 次 
  
Reluctant 数量词
正则 | 解释
---|---
X?? | X，一次或一次也没有 
X*? | X，零次或多次 
X+? | X，一次或多次 
X{n}? | X，恰好 n 次 
X{n,}? | X，至少 n 次 
X{n,m}? | X，至少 n 次，但是不超过 m 次 
  
Possessive 数量词 
正则 | 解释
---|---
X?+ | X，一次或一次也没有 
X*+ | X，零次或多次 
X++ | X，一次或多次 
X{n}+ | X，恰好 n 次 
X{n,}+ | X，至少 n 次 
X{n,m}+ | X，至少 n 次，但是不超过 m 次 

Greedy、Reluctant、Possessive数量词的区别：https://blog.csdn.net/qq_35917800/article/details/78420278

正则表达式里面 .* 和 .*? 有什么区别？：https://zhidao.baidu.com/question/297472922.html

## 2. 使用正则表达式

典型应用如下：
```
        Pattern p = Pattern.compile("a*b");
        Matcher m = p.matcher("aaaab");
        boolean b = m.matches();
        System.out.println(b);
```