---
title: "Java-Se-注解"
date: 2017-07-01T11:18:47+08:00
draft: true
tags: [""]
categories: ["Java-Se"]
---

# 概述

注解（Annotation）是代码里的特殊标记，这些标记在编译、类加载、运行时被读取，并执行相应的处理。

注解可以修饰包、类、构造器、方法、成员变量、参数、局部变量。

# 基本注解

基本注解：

- [@Override](https://my.oschina.net/u/1162528)
- [@Deprecated](https://my.oschina.net/jianhuaw)
- @SuppressWarnings
- @SafeVarargs
- @FunctionalInteface

@SuppressWarnings的值有：@SuppressWarnings("unchecked")

# JDK元注解

元注解（Meta Annotation)，是修饰注解的注解。常用的有：

- @Retention
- @Target
- @Documented
- @Inherited

@Retention指此注解可以保留多长时间，常用如下：
`@Retention(RetentionPolicy.SOURCE/RetentionPolicy.CLASS/RetentionPolicy.RUNTIME)`

@Target指此注解能修饰谁，常用的value值在ElementType枚举类中（8个）

@Documented被此注解修饰的元素会生成API文档

@Inherited批被修饰的annotation具有继承性

# 自定义注解

## 增加属性

![annotation-diy-add-attr](https://oscimg.oschina.net/oscnet/090c55a396b7404b1d130ff8a32393f12ea.jpg "annotation-diy-add-attr")

# 解析Annotation

定义annotation的目的一是为了让编译器识别，二是为了其它程序解析这些annotation。因此如何解析注解呢？
参看笔记《Java-Se-反射》。

# 附录

SuppressWarnings的value值
![annotation-diy-add-appendix](https://oscimg.oschina.net/oscnet/7094e84ae4ce9d07e3b35bf04b44d097747.jpg "annotation-diy-add-appendix")