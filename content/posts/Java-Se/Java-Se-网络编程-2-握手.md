---
title: "Java-Se-网络编程-2"
date: 2017-07-01T11:14:47+08:00
draft: true
tags: ["网络编程"]
categories: ["Java-Se"]
---

# TCP三次握手及原因

![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200707183830.png)

参考：https://www.cnblogs.com/GuoXinxin/p/11657889.html


# 四次挥手

TCP共有6个标志位，分别是：

- SYN(synchronous),建立联机。
- ACK(acknowledgement),确认。
- PSH(push),传输。
- FIN(finish),结束。
- RST(reset),重置。
- URG(urgent),紧急。


![](https://gitee.com/rulerLwx/PicGo/raw/master/img/20200707184122.png)

（1） TCP客户端发送一个FIN报文，用来关闭客户到服务器的数据传送。

（2） 服务器收到这个FIN报文，它发回一个ACK报文，确认序号为收到的序号加1。和SYN一样，一个FIN报文将占用一个序号。

（3） 服务器关闭客户端的连接，发送一个FIN给客户端。

（4） 客户端发回ACK报文确认，并将确认序号设置为收到序号加1。

https://www.jianshu.com/p/cd801d1b3147

