---
title: "自己动手（一）——批量移动、重命名文件"
date: 2020-07-27T17:50:02+08:00
draft: true
categories: ["技术杂烩"] 
---


# 实际需求

在B站（bilibili.com）找视频学习时，在浏览器上播放视频会让电脑发烫，cpu会占用30~40%，风扇转个不停。

想将视频下载至本地，然后用PotPlayer播放，想必能让电脑不发烫。使用win10自带的软件商店store安装bilibili客户端，将视频下载到硬盘`E:\B站视频`，但文件是分散的，且没有文章、章节标题：

![](https://gitee.com/leafsing/pic/raw/master/img20200714/20200727164343.png)

![](https://gitee.com/leafsing/pic/raw/master/img20200714/20200727164427.png)

这样的话，即使用PotPlayer播放视频也不好找对应的章节来学习，于是就有下面的需求：

需求：能不能将下载好的视频批量重命名，并移动到指定的目录，因为视频章节有几百节，不可能手动操作几百次`ctrl c/x/v`。


# 着手分析

1）打开`668060810.info`文件，里面以JSON格式记录视频的基本信息，包括视频的名称、章节的名称等等

```json
{
	"Type": 0,
	"Aid": "668060810",
	"Bid": "BV1Ma4y1i7QH",
	"Cid": "186351071",
	"SeasonId": null,
	"EpisodeId": null,
	"Title": "黑马 - web前端Vue基础到高级教程205集 完整版",
	"Uploader": "蓝精灵永恒",
	"Description": "（带课件，11天）\r\nVue基础到高级教程",
	"CoverURL": "http://i0.hdslb.com/bfs/archive/7e0013f2d2f83841e23011d0ab4c48383156704b.jpg",
	"Tag": null,
	"From": "vupload",
	"PartNo": "8",
	"PartName": "8.Day1_08.讲解v-cloak、v-text、v-html的基本使用(Av50680998,P8)",
	"Format": 2,
	"TotalParts": 1,
	"DownloadTimeRelative": 183734991,
	"CreateDate": "2020-05-03 11:53",
	"TotalTime": "00:12:20.1120000",
	...
}
```

2）我能不能从这个文件中提取标题信息，并重命名同一目录下的mp4文件，并将视频移动到指定的目录？

答：我会用Java读取这个文件，转换成JSON对象，提取里面的标题，使用IO重命名，并移动它到其它地方。

3）应该可以遍历这个目录并按以上逻辑批量做吧？

答：(⊙v⊙)嗯


# 动手写代码

1）好吧，一动手就是用 Spring Boot 创建项目（会不会是本能反应）

pom.xml

```xml
   <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.2.2.RELEASE</version>
    </parent>

    <properties>
        <java.version>8</java.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.7</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>
```

2）思路分析

- 使用`jdk7`提供的NIO中的工具类`Files`中的`walkFileTree`静态方法遍历目录
- 使用`commons-io`的`FileUtils`工具类对文件的读写，可以简化 IO 操作
- 使用 `spring-boot-starter-web`包中的`jackson`中的`ObjectMapper`进行JSON转换

3）代码实现

```java
/**
 * @author lwx
 * @date 2020/7/27
 */
public class App {
    public static void main(String[] args) throws IOException {
        Files.walkFileTree(
                Paths.get("E:", "B站视频", "668060810"),
                new SimpleFileVisitor<Path>(){
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        String extension = FilenameUtils.getExtension(file.toString());
                        if ("info".equals(extension)) {
                            // 从 xxx.info 文件（JSON格式）中解析出文件名
                            String json = FileUtils.readFileToString(file.toFile(), "UTF-8");
                            ObjectMapper mapper = new ObjectMapper();
                            Map<String,String> map = mapper.readValue(json, Map.class);
                            String title = map.get("Title");
                            String partName = map.get("PartName");

                            // 重命名同一目录下的.mp4，并移动到指定的目录
                            Iterator<File> it = FileUtils.iterateFiles(file.getParent().toFile(), new String[]{"mp4"},false);
                            String distDir = file.getParent().getParent().getParent()+"/"+title+"/"+partName+".mp4"; // 文件目录、名称
                            if (it.hasNext()) {
                                File mp4 = it.next();
                                FileUtils.moveFile(mp4, new File(distDir));
                            }
                            return FileVisitResult.SKIP_SIBLINGS; // 结束此目录的递归
                        }
                        return super.visitFile(file, attrs);
                    }
                });
    }
}
```

代码仓库：https://gitee.com/rulerLwx/bilibili_video_rename.git

4）效果验证

![](https://gitee.com/leafsing/pic/raw/master/img20200714/20200727173139.png)


# 题外话

- 对不方便之处，能写一些自己工具提高效率
- 写工具固然需要时间，但总比重复性的`ctrl c/v/x`要好，且有成就感