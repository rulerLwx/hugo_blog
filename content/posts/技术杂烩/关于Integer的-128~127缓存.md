---
title: "关于Integer的 -128~127缓存"
date: 2020-07-11T16:56:21+08:00
draft: true
categories: ["技术杂烩"]
---

先说结论：
1. 凡是通过`new Integer(xx)`得到的，`==`是false。因为是不同的对象。
2. 数字在`-128~127`之间，若是通过`Integer.valueOf(xx)`或直接装箱赋值的，则有缓存，`==`是true；但超出`-128~127`范围，`==`是false。

一句话：`-128~127`之间有缓存，但`new Integer(xx)`除外。

测试代码：

```java
public class Test {
    public static void main(String[] args) {
        Integer s = new Integer(9);
        Integer t = new Integer(9);
        System.out.println(s==t); // false

        Integer i = Integer.valueOf(9);
        Integer j = Integer.valueOf(9);
        System.out.println(i==j); // true

        Integer a = 127;
        Integer b = 127;
        System.out.println(a == b); // true

        Integer c = 128;
        Integer d = 128;
        System.out.println(c == d); // false
    }
}
```

Integer有一个内部类IntegerCache以及valueOf()源码如下：

```java
    /**
     * Cache to support the object identity semantics of autoboxing for values between
     * -128 and 127 (inclusive) as required by JLS.
     *
     * The cache is initialized on first usage.  The size of the cache
     * may be controlled by the {@code -XX:AutoBoxCacheMax=<size>} option.
     * During VM initialization, java.lang.Integer.IntegerCache.high property
     * may be set and saved in the private system properties in the
     * sun.misc.VM class.
     */

    private static class IntegerCache {
        static final int low = -128;
        static final int high;
        static final Integer cache[];

        static {
            // high value may be configured by property
            int h = 127;
            String integerCacheHighPropValue =
                sun.misc.VM.getSavedProperty("java.lang.Integer.IntegerCache.high");
            if (integerCacheHighPropValue != null) {
                try {
                    int i = parseInt(integerCacheHighPropValue);
                    i = Math.max(i, 127);
                    // Maximum array size is Integer.MAX_VALUE
                    h = Math.min(i, Integer.MAX_VALUE - (-low) -1);
                } catch( NumberFormatException nfe) {
                    // If the property cannot be parsed into an int, ignore it.
                }
            }
            high = h;

            cache = new Integer[(high - low) + 1];
            int j = low;
            for(int k = 0; k < cache.length; k++)
                cache[k] = new Integer(j++);

            // range [-128, 127] must be interned (JLS7 5.1.7)
            assert IntegerCache.high >= 127;
        }

        private IntegerCache() {}
    }

    /**
     * Returns an {@code Integer} instance representing the specified
     * {@code int} value.  If a new {@code Integer} instance is not
     * required, this method should generally be used in preference to
     * the constructor {@link #Integer(int)}, as this method is likely
     * to yield significantly better space and time performance by
     * caching frequently requested values.
     *
     * This method will always cache values in the range -128 to 127,
     * inclusive, and may cache other values outside of this range.
     *
     * @param  i an {@code int} value.
     * @return an {@code Integer} instance representing {@code i}.
     * @since  1.5
     */
    public static Integer valueOf(int i) {
        if (i >= IntegerCache.low && i <= IntegerCache.high)
            return IntegerCache.cache[i + (-IntegerCache.low)];
        return new Integer(i);
    }
```

于是，我在Byte、Short、Charactor、Long的源码中也看到有 xxCache，如 ByteCache、ShortCache、CharacterCache、LongCache，原理跟 Integer 一样。

而Float、Double则没有看到。
