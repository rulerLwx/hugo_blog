---
title: "跨域问题"
date: 2020-07-08T22:41:12+08:00
draft: true
categories: ["技术杂烩"] 
---


# 为什么出现“跨域”问题

现在是**前后端分离**开发大行其道的时候，一不小心就容易出现跨域问题。比如在本地编写的页面请求后台接口数据时，容易出现以下问题：

![](https://gitee.com/leafsing/pic/raw/master/img/20200708232126.png)

这是因为浏览器有**同源策略**，是浏览器搞的鬼。是浏览器根据服务器返回的http头来判断的（参考：https://www.v2ex.com/t/203693）

但另一方面，**同源策略**是浏览器最基本的安全机制。

所谓同源（即指在同一个域）就是两个具有相同的协议（protocol），主机（host）和端口号（port），缺一不可。

# 解决办法

## 方法一：jsonp

- 在页面中引入一段`<script src="http://localhost:91?callback=f">`代码，其中`f`是回调函数名称；后台接口返回的数据可能是：`f([{},{},...])`。
![](https://gitee.com/leafsing/pic/raw/master/img/20200708232800.png)
- 缺点：只能是 get 请求

**jsonp的原理**：只有通过ajax方式时才会有跨域问题，而通过 href 或 src 请求的 .js/css/img 等文件时，不会存在跨域，所以此处使用`src`属性来解决跨域。


## 方法二：CROS

在后台服务器端 response 对象上增加响应头`Access-Control-Allow-Origin`：

```
res.header("Access-Control-Allow-Origin","*");
```

在 Spring 4.x 中提供了 `@CrossOrigin` 注解，可以注解在类或方法上，原理也是添加响应头。


## 方法三：nginx 反向代理

使用 nginx 反向代理，因为此时后台接口、前台页面都在同一个nginx里。

跨域参考文章：
- https://www.jianshu.com/p/7ce1f8c7a04c
- https://blog.csdn.net/weixin_33743880/article/details/91423231
- https://blog.csdn.net/qq_38128179/article/details/84956552
- https://segmentfault.com/a/1190000015597029?utm_source=tag-newest

